# Welcome to Thor!

Welcome to Thor source code repository!

In the following sections, you may find how to build
Thor from sources and license information.

# Prerequisities

Before you start to build Thor, you'll need to prepare
the following packages ready. The annotated version number
were tested. Any version after or before them are not
guaranteed to work.

1. GCC 4.8  
For Ubuntu users, you may find them in the Ubuntu toolchain test PPA:
`https://launchpad.net/~ubuntu-toolchain-r/+archive/test`  
After GCC 4.8 is installed, please make it default choice of gcc via
update-alternatives or simply modify the soft link target of /usr/bin/gcc.

1. CMake at least 2.8.11  
Get the latest version here:
http://www.cmake.org/cmake/resources/software.html

1. LLVM 3.2  
You may build from sources:
    1. Download LLVM 3.2 src http://llvm.org/releases/
    1. `./configure --enable-assertions --disable-optimized --enable-debug-runtime --prefix=<XXX>`
    1. `make REQUIRES_RTTI=1 -j4 && sudo make install`  
  
    Or directly install the package `sudo apt-get install llvm-3.2-dev`

1. `sudo apt-get install zlib1g-dev`

1. Boost 1.55  
    1. Get the source
    1. `./bootstrap.sh && ./b2 --without-python stage && sudo ./b2 --without-python install`

1. TBB 4.1 Update3  
    Since the pre-built version didn't provide the one building with GCC 4.8, you may need
to build yourself.  
    1. Grab the source https://www.threadingbuildingblocks.org/download
    1. `make`
    1. TBB didn't have install script at the time this document was written, so we are going to install it manually: 
        1. copy all shared objects to your library path:   
`cp linux_XX_gcc_cc4.8_libcXX_kernel_XX_/.so* /usr/local/lib/`  
    All the above shared objects should contained a trailing version strings.
    Make symbolic links without the version string of them: ex. `ln -s libtbb_debug.so.2 libtbb_debug.so`.
        1. Copy include/tbb to /usr/local/include/tbb

1. msgpack  
`sudo apt-get install libmsgpack-dev`

1. websocketpp  
Please use our mirror:
https://github.com/chitat/websocketpp  
`make SHARED=1 && sudo make SHARED=1 install`

1. rstm  
Please use our mirror and modified version of rstm:  
https://github.com/zillians/rstm  
`cmake . && make && sudo make install`

1. log4cxx  
`sudo apt-get install liblog4cxx10-dev`

1. xmllint  
`sudo apt-get install libxml2-utils`

1. xsltproc  
`sudo apt-get install xsltproc`

1. nVidia CUDA Toolkit 5.5 (optional)  
https://developer.nvidia.com/cuda-downloads

# Build Steps

1. Clone the source
`git clone https://bitbucket.org/zillians/thor.git`

1. Create a build directory next to the checked-out source. Ex. build.

1. Do cmake configuration:  
`cd build`  
`cmake ../thor/ -DENABLE_FEATURE_CUDA=OFF -DLLVM_CONFIG_EXECUTABLE=<path to llvm-config>`  
If you install llvm 3.2 via APT, path to your llvm-config utility is likely to be `/usr/lib/llvm-3.2/bin/llvm-config`.  
If you have installed CUDA toolkit, `-DENABLE_FEATURE_CUDA=OFF` can be removed.
1. `make thor-collection`

Now you should have all toolchain binaries ready in bin/ directory
under your build directory.  

Enjoy!

# License

Thor, Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>,
released under GNU Affero General Public License, version 3.

If you want to develop any commercial services or closed-source products
with Thor, to adapt sources of Thor in your own projects without
disclosing sources, purchasing a commercial license is mandatory and can be
accomplished by contacting Zillians, Inc. at <thor@zillians.com>

For contributing to Thor, the contribution agreement can be reached at
<thor@zillians.com>.

Some 3rd libraries with Thor are attributed to respective authors
under other FOSS licenses. Please read the accompanied license information files for details.

