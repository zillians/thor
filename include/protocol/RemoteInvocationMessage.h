/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_REMOTEINVOCATIONMESSAGE_H_
#define ZILLIANS_REMOTEINVOCATIONMESSAGE_H_

#include "core/Common.h"
#include "core/IntTypes.h"
#include "core/Buffer.h"

namespace zillians {

struct RemoteInvocationMessage
{
	static const uint32 TYPE = 1;

	int64 function_id;
	Buffer buffer;

	template <typename Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		UNUSED_ARGUMENT(version);

		ar & function_id;
		ar & buffer;
	}
};

}

#endif /* ZILLIANS_REMOTEINVOCATIONMESSAGE_H_ */
