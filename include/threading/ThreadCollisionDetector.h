/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_THREADCOLLISIONDETECTOR_H_
#define ZILLIANS_THREADCOLLISIONDETECTOR_H_

#include <stdexcept>

#ifdef NDEBUG
    #define DETECTOR(obj)
    #define SCOPED_WATCH(obj)
    #define WATCH(obj)
#else
    #define DETECTOR(obj)       ThreadCollisionDetector _##obj;
    #define SCOPED_WATCH(obj)   ThreadCollisionDetector::ScopedWatcher _scoped_watcher_##obj(_##obj);
    #define WATCH(obj)          ThreadCollisionDetector::Watcher _watcher_##obj(_##obj);
#endif

namespace zillians {

/**
 * ThreadCollisionDetector is used to detect whether two thread accessing the same region of code or variable
 */
class ThreadCollisionDetector
{
public:
    ThreadCollisionDetector() : mActiveThread(0)
    { }

    ~ThreadCollisionDetector()
    { }

    class Watcher
    {
    public:
        Watcher(ThreadCollisionDetector& _v) : v(_v)
        { v.enterSelf(); }

        ~Watcher()
        { }

    private:
        ThreadCollisionDetector& v;
    };

    class ScopedWatcher {
    public:
        ScopedWatcher(ThreadCollisionDetector& _v) : v(_v)
        { v.enter(); }

        ~ScopedWatcher()
        { v.leave(); }

    private:
        ThreadCollisionDetector& v;
    };

private:
    void enterSelf()
    {
        if(!__sync_bool_compare_and_swap(&mActiveThread, 0, pthread_self()))
        {
            if(!__sync_bool_compare_and_swap(&mActiveThread, pthread_self(), mActiveThread))
            {
                throw std::runtime_error("thread collision detected");
            }
        }
    }

    void enter()
    {
        if(!__sync_bool_compare_and_swap(&mActiveThread, 0, pthread_self()))
        {
            throw std::runtime_error("thread collision detected");
        }
    }

    void leave()
    {
        __sync_fetch_and_xor(&mActiveThread, mActiveThread);
    }

    pthread_t mActiveThread;
};

}
#endif /* ZILLIANS_THREADCOLLISIONDETECTOR_H_ */
