/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_DISPATCHER_DISPATCHERTHREADSIGNALER_H_
#define ZILLIANS_DISPATCHER_DISPATCHERTHREADSIGNALER_H_

#include "core/Semaphore.h"
#include "core/Atomic.h"

namespace zillians { namespace threading {

class DispatcherThreadSignaler
{
public:
    DispatcherThreadSignaler() : mWaitSignal(sizeof (uint64) * 8 - 1)
    { mBitmap = 0; }

    ~DispatcherThreadSignaler()
    { }

public:
    void signal(uint32 signal)
    {
        if(atomic::bitmap_btsr(mBitmap, signal, mWaitSignal))
            mSemaphore.post();
    }

    uint64 poll(uint32 id)
    {
        uint64 result = atomic::bitmap_izte(mBitmap, uint64(1) << mWaitSignal, 0);

        if(!result)
        {
            mSemaphore.wait();
            result = atomic::bitmap_xchg (mBitmap, 0);
        }

        return result;
    }

    uint64 check()
    { return atomic::bitmap_xchg(mBitmap, 0); }

    uint64 bitOr(uint64 bitmap)
    { return atomic::bitmap_or(mBitmap, bitmap); }

    void bitReset(uint32 bit)
    {
        atomic::bitmap_btsr(mBitmap, bit, bit);
    }

private:
    Semaphore mSemaphore;
    uint64 mBitmap;
    const int mWaitSignal;
};

} }

#endif /* ZILLIANS_DISPATCHER_DISPATCHERTHREADSIGNALER_H_ */
