/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_ATOMIC_ATOMICQUEUE_H_
#define ZILLIANS_ATOMIC_ATOMICQUEUE_H_

#include <cstdlib>

#include <tbb/atomic.h>

#include "core/Atomic.h"

namespace zillians { namespace atomic {

/**
 * AtomicQueue is based on ZeroMQ y-suite.
 */
template <typename T, int N> class AtomicQueue
{
public:
    inline AtomicQueue()
    {
         begin_chunk = (chunk_t*) malloc (sizeof (chunk_t));
         BOOST_ASSERT (begin_chunk);
         begin_pos = 0;
         back_chunk = NULL;
         back_pos = 0;
         end_chunk = begin_chunk;
         end_pos = 0;
         spare_chunk = NULL;
    }

    inline ~AtomicQueue()
    {
        while (true)
        {
            if (begin_chunk == end_chunk)
            {
                free (begin_chunk);
                break;
            }
            chunk_t *o = begin_chunk;
            begin_chunk = begin_chunk->next;
            free (o);
        }

        chunk_t *sc = spare_chunk.fetch_and_store(NULL);
        if (sc)
            free (sc);
    }

    inline T &front()
    {
         return begin_chunk->values [begin_pos];
    }

    inline T &back()
    {
        return back_chunk->values [back_pos];
    }

    inline void push ()
    {
        back_chunk = end_chunk;
        back_pos = end_pos;

        if (++end_pos != N)
            return;

        chunk_t *sc = spare_chunk.fetch_and_store(NULL);
        if (sc) {
            end_chunk->next = sc;
            sc->prev = end_chunk;
        } else {
            end_chunk->next = (chunk_t*) malloc (sizeof (chunk_t));
            BOOST_ASSERT (end_chunk->next);
            end_chunk->next->prev = end_chunk;
        }
        end_chunk = end_chunk->next;
        end_pos = 0;
    }

    inline void unpush ()
    {
        if (back_pos)
            --back_pos;
        else
        {
            back_pos = N - 1;
            back_chunk = back_chunk->prev;
        }

        if (end_pos)
            --end_pos;
        else
        {
            end_pos = N - 1;
            end_chunk = end_chunk->prev;
            free (end_chunk->next);
            end_chunk->next = NULL;
        }
    }

    inline void pop ()
    {
        if (++ begin_pos == N)
        {
            chunk_t *o = begin_chunk;
            begin_chunk = begin_chunk->next;
            begin_chunk->prev = NULL;
            begin_pos = 0;

            chunk_t *cs = spare_chunk.fetch_and_store(o);
            if (cs)
                free(cs);
        }
    }

private:
    struct chunk_t
    {
         T values [N];
         chunk_t *prev;
         chunk_t *next;
    };

    chunk_t *begin_chunk;
    int begin_pos;
    chunk_t *back_chunk;
    int back_pos;
    chunk_t *end_chunk;
    int end_pos;

    tbb::atomic<chunk_t*> spare_chunk;

private:
    AtomicQueue (const AtomicQueue&);
    void operator = (const AtomicQueue&);
};


/**
 * AtomicPipe is based on ZeroMQ y-suite
 */
template <typename T, int N> class AtomicPipe
{
public:
    inline AtomicPipe ()
    {
        queue.push ();

        r = w = f = &queue.back ();
        c = &queue.back();
    }

    inline void write (const T &value_, bool incomplete_)
    {
        queue.back () = value_;
        queue.push ();

        if (!incomplete_)
            f = &queue.back ();
    }

    inline bool unwrite (T *value_)
    {
        if (f == &queue.back ())
            return false;
        queue.unpush ();
        *value_ = queue.back ();
        return true;
    }

    inline bool flush ()
    {
        if (w == f)
            return true;

        if(c.compare_and_swap(f, w) != w)
        {
            c = f;
            w = f;
            return false;
        }

        w = f;
        return true;
    }

    inline bool check_read ()
    {
        if ((&queue.front () != r) && r)
             return true;

        r = c.compare_and_swap(NULL, &queue.front());

        if (&queue.front () == r || !r)
            return false;

        return true;
    }

    inline bool read (T *value_)
    {
        if (!check_read ())
            return false;

        *value_ = queue.front ();
        queue.pop ();
        return true;
    }

protected:
    AtomicQueue <T, N> queue;

    T *w;
    T *r;
    T *f;
    tbb::atomic<T*> c;

private:
    AtomicPipe (const AtomicPipe&);
    void operator = (const AtomicPipe&);
};

} }

#endif /* ATOMICQUEUE_H_ */
