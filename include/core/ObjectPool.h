/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_OBJECTPOOL_H_
#define ZILLIANS_OBJECTPOOL_H_

#include <queue>

#include <tbb/concurrent_queue.h>

#include "core/Common.h"

#define ZILLIANS_OBJPOOL_PREFERABLE_POOL_SIZE     0
#define ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER  0

namespace zillians {

/**
 * @brief ObjectPool is a simple object pooling template.
 *
 * @note ObjectPool does not support concurrent new/delete on the same
 * type of object, use ConcurrentObjectPool for that case.
 *
 * @see ConcurrentObjectPool
 */
template <typename T>
class ObjectPool
{
public:
    ObjectPool()
    { }

    virtual ~ObjectPool()
    { }

public:
    static void* operator new(size_t size)
    {
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
        ++mPool.allocationCount;
#endif
        if(mPool.allocations.empty())
        {
            return ::operator new(size);
        }
        else
        {
            void* obj = mPool.allocations.front();
            mPool.allocations.pop();
            return obj;
        }
    }

    static void operator delete(void* p)
    {
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
        --mPool.allocationCount;
#endif
        if(ZILLIANS_OBJPOOL_PREFERABLE_POOL_SIZE > 0 && mPool.allocations.size() > ZILLIANS_OBJPOOL_PREFERABLE_POOL_SIZE)
        {
            ::operator delete(p);
        }
        else
        {
            mPool.allocations.push(p);
        }
    }

    static void purge()
    {
        while(!mPool.allocations.empty())
        {
            void* obj = mPool.allocations.front();
            ::operator delete(obj);
            mPool.allocations.pop();
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
            --mPool.allocationCount;
#endif
        }
    }

protected:
    /**
     * This is used to ensure the ObjectPool<T>::purge() is called before
     * destroying the internal object pool container.
     */
    struct AutoPoolImpl
    {
        AutoPoolImpl() { }
        ~AutoPoolImpl() { ObjectPool<T>::purge(); }
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
        tbb::atomic<long> allocationCount;
#endif
        std::queue<void*> allocations;
    };
    static AutoPoolImpl mPool;
};

template<typename T> typename ObjectPool<T>::AutoPoolImpl ObjectPool<T>::mPool;

/**
 * ConcurrentObjectPool is a simple object pooling template supporting
 * concurrent allocations and deallocations.
 *
 * @note Since ConcurrentObjectPool does support concurrent new/delete on
 * the same type of object, it also brings some overhead while managing
 * the internal queue. Use it with performance caution.
 *
 * @see ObjectPool
 */
template <typename T>
class ConcurrentObjectPool
{
public:
    ConcurrentObjectPool()
    { }

    virtual ~ConcurrentObjectPool()
    { }

public:
    static void* operator new(size_t size)
    {
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
        ++mPool.allocationCount;
#endif
        void* obj = NULL;
        mPool.allocations.try_pop(obj);

        if(obj)
        {
            return obj;
        }
        else
        {
            return ::operator new(size);
        }
    }

    static void operator delete(void* p)
    {
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
        --mPool.allocationCount;
#endif
        if(ZILLIANS_OBJPOOL_PREFERABLE_POOL_SIZE > 0 && mPool.allocations.size() > ZILLIANS_OBJPOOL_PREFERABLE_POOL_SIZE)
        {
            ::operator delete(p);
        }
        else
        {
            mPool.allocations.push(p);
        }
    }

    static void purge()
    {
        while(!mPool.allocations.empty())
        {
            void* obj = NULL;
            if(mPool.allocations.try_pop(obj))
            {
                ::operator delete(obj);
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
                --mPool.allocationCount;
#endif
            }
        }
    }

protected:
    /**
     * This is used to ensure the ObjectPool<T>::purge() is called before
     * destroying the internal object pool container.
     */
    struct AutoPoolImpl
    {
        AutoPoolImpl() { }
        ~AutoPoolImpl() { ConcurrentObjectPool<T>::purge(); }
#if ZILLIANS_OBJPOOL_ENABLE_OBJ_POOL_COUNTER
        tbb::atomic<long> allocationCount;
#endif
        tbb::concurrent_bounded_queue<void*> allocations;
    };
    static AutoPoolImpl mPool;
};

template<typename T> typename ConcurrentObjectPool<T>::AutoPoolImpl ConcurrentObjectPool<T>::mPool;

}

#endif/*ZILLIANS_OBJECTPOOL_H_*/
