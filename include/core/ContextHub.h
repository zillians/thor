/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_CONTEXTHUB_H_
#define ZILLIANS_CONTEXTHUB_H_

#include <cstddef>

#include <string>
#include <vector>
#include <map>
#include <utility>
#include <unordered_map>

#include <boost/assert.hpp>

#include "core/Common.h"
#include "core/IntTypes.h"
#include "core/SharedPtr.h"
#include "core/Atomic.h"

/**
 * By allowing arbitrary context placement for different ContextHub instance,
 * the atomic context indexer can be stored as a member variable in ContextHub
 * to avoid link dependency (so we have a pure header class.) However this makes
 * it harder to debug and make the ContextHub object larger (memory space waste.)
 */
#define ZILLIANS_SERVICEHUB_ALLOW_ARBITRARY_CONTEXT_PLACEMENT_FOR_DIFFERENT_INSTANCE  0

namespace zillians {

struct ContextOwnership
{
    enum type
    {
        keep        = 0,
        transfer    = 1,
    };
};

/**
 * ContextHub is a universal storage for arbitrary class with constant access time.
 *
 * ContextHub utilized static template method trick to give different template type
 * a different class index, which will be used to locate the stored instance. The
 * access time is basically a small constant, since we don't use map or hash but a
 * dead simple dynamic array (std::vector).
 *
 * @note There's only one slot for each different type of object. Suppose you have
 * three different types named A, B, and C. You can only save one instance of A into
 * one instance of ContextHub. Same for B and C.
 */
template<ContextOwnership::type TransferOwnershipDefault = ContextOwnership::transfer>
class ContextHub
{
private:
    struct NullDeleter
    {
        void operator() (void const *) const
        { }
    };

public:
    /**
     * Init object through default constructor
     */
    template <typename T, typename ...ArgTypes>
    inline T* init(ArgTypes&&... args)
    {
        auto shared_ctx = make_shared<T>(std::forward<ArgTypes>(args)...);

        refSharedContext<T>() = shared_ctx;

        return shared_ctx.get();
    }

    /**
     * Save an object of type T into the universal storage.
     *
     * @note If the TransferOwnership template parameter is set, the ownership of the given object is transferred to this ContextHub instance.
     *
     * @param ctx The given object of type T
     */
    template <typename T, ContextOwnership::type TransferOwnership/* = TransferOwnershipDefault*/>// NOTE 20101015 Nothing - Default template argument in template function is a C++0x feature, not supported in C++03 standard.
    inline void set(T* ctx)
    {
        if(TransferOwnership == ContextOwnership::transfer)
        {
            refSharedContext<T>() = shared_ptr<T>(ctx);
        }
        else
        {
            refSharedContext<T>() = shared_ptr<T>(ctx, NullDeleter());
        }
    }
    template <typename T>
    inline void set(T* ctx)
    {
        if(TransferOwnershipDefault == ContextOwnership::transfer)
        {
            refSharedContext<T>() = shared_ptr<T>(ctx);
        }
        else
        {
            refSharedContext<T>() = shared_ptr<T>(ctx, NullDeleter());
        }
    }

    /**
     * Retrieve the object according to the given type T.
     *
     * @return The pointer to the stored object. Return null pointer if it's not set previously.
     */
    template <typename T>
    inline T* get() const
    {
        const auto index = refIndex<T>();

        if (index >= mSharedContextObjects.size())
            return nullptr;

        return static_cast<T*>(mSharedContextObjects[index].get());
    }

    /**
     * Remove the previously stored object instance of type T.
     *
     * @note If the TransferOwnership template parameter is set, ContextHub will automatically destroy the object; otherwise
     */
    template <typename T>
    inline void reset()
    {
        const auto index = refIndex<T>();

        if (index >= mSharedContextObjects.size())
            return;

        mSharedContextObjects[index] = nullptr;
    }

    inline void resetAll()
    {
        mSharedContextObjects.clear();
    }

private:
    /**
     * The magic trick to store and access context object by using static
     * initialization to identify the index of a specific type.
     *
     * @return The reference to the shared pointer
     */
    template <typename T>
    inline std::vector< shared_ptr<void> >::reference refSharedContext() const
    {
        const auto index = refIndex<T>();

        if(UNLIKELY(index >= mSharedContextObjects.size()))
        {
            while(index >= mSharedContextObjects.size())
            {
                mSharedContextObjects.push_back(shared_ptr<void>());
            }
        }

        BOOST_ASSERT(index < mSharedContextObjects.size());
        return mSharedContextObjects[index];
    }

    template <typename T>
    inline uint32 refIndex() const
    {
        static uint32 index = atomic::add<uint32>(&msContextIndexer, 1);
        return index;
    }

    mutable std::vector< shared_ptr<void> > mSharedContextObjects;
#if ZILLIANS_SERVICEHUB_ALLOW_ARBITRARY_CONTEXT_PLACEMENT_FOR_DIFFERENT_INSTANCE
    uint32 msContextIndexer;
#else
    static uint32 msContextIndexer;
#endif
};

#if ZILLIANS_SERVICEHUB_ALLOW_ARBITRARY_CONTEXT_PLACEMENT_FOR_DIFFERENT_INSTANCE
#else
template<ContextOwnership::type TransferOwnershipDefault> uint32 ContextHub<TransferOwnershipDefault>::msContextIndexer;
#endif


/**
 * NamedContextHub provides name to context pointer mapping.
 *
 * NamedContextHub is an alternative version of ContexHub in which
 * an optional key (string) can be provided as the identifier of the
 * context pointer. By default, the key is the name (typeid) of the
 * given type object.
 */
template<ContextOwnership::type TransferOwnershipDefault>
class NamedContextHub
{
private:
    struct NullDeleter
    {
        void operator() (void const *) const
        { }
    };

public:
    NamedContextHub()
    { }

    virtual ~NamedContextHub()
    { }

public:
    /**
     * Save an object of type T into the universal storage.
     *
     * @note If the TransferOwnership template parameter is set, the ownership of the given object is transferred to this ContextHub instance.
     *
     * @param ctx The given object of type T
     */
    template <typename T, ContextOwnership::type TransferOwnership/* = TransferOwnershipDefault*/>// NOTE 20101015 Nothing - Default template argument in template function is a C++0x feature, not supported in C++03 standard.
    inline void set(T* ctx, const std::string& name = std::string(typeid(T).name()))
    {
        if(TransferOwnership == ContextOwnership::transfer)
        {
            refSharedContext<T>(name) = shared_ptr<T>(ctx);
        }
        else
        {
            refSharedContext<T>(name) = shared_ptr<T>(ctx, NullDeleter());
        }
    }
    template <typename T>
    inline void set(T* ctx, const std::string& name = std::string(typeid(T).name()))
    {
        if(TransferOwnershipDefault == ContextOwnership::transfer)
        {
            refSharedContext<T>(name) = shared_ptr<T>(ctx);
        }
        else
        {
            refSharedContext<T>(name) = shared_ptr<T>(ctx, NullDeleter());
        }
    }

    /**
     * Retrieve the object according to the given type T.
     *
     * @return The pointer to the stored object. Return null pointer if it's not set previously.
     */
    template <typename T>
    inline T* get(const std::string& name = std::string(typeid(T).name()))
    {
        return static_pointer_cast<T>(refSharedContext<T>(name)).get();
    }

    /**
     * Remove the previously stored object instance of type T.
     *
     * @note If the TransferOwnership template parameter is set, ContextHub will automatically destroy the object; otherwise
     */
    template <typename T>
    inline void reset(const std::string& name = std::string(typeid(T).name()))
    {
        refSharedContext<T>(name).reset();
    }

    inline std::size_t size()
    {
        return mSharedContextObjects.size();
    }

private:
    /**
     * The magic trick to store and access context object by using static
     * initialization to identify the index of a specific type.
     *
     * @return The reference to the shared pointer
     */
    template <typename T>
    inline shared_ptr<void>& refSharedContext(const std::string& name)
    {
        std::map< std::string, shared_ptr<void> >::iterator it = mSharedContextObjects.find(name);
        if(UNLIKELY(it == mSharedContextObjects.end()))
        {
            //mSharedContextObjects[name] = shared_ptr<void>();
            mSharedContextObjects.insert(std::make_pair(name, shared_ptr<void>()));
            return mSharedContextObjects[name];
        }
        else
        {
            return it->second;
        }
    }

    std::map< std::string, shared_ptr<void> > mSharedContextObjects;
};

/**
 * HashedContextHub provides index to context pointer mapping.
 *
 * IndexedContextHub is an alternative version of ContexHub in which
 * an key can be provided as the identifier of the context pointer.
 * By default, the key is the name (typeid) of the given type object.
 */
template<ContextOwnership::type TransferOwnershipDefault = ContextOwnership::transfer, typename KeyType = uint32>
class HashedContextHub
{
private:
    struct NullDeleter
    {
        void operator() (void const *) const
        { }
    };

public:
    HashedContextHub()
    { }

    virtual ~HashedContextHub()
    { }

public:
    /**
     * Save an object of type T into the universal storage.
     *
     * @note If the TransferOwnership template parameter is set, the ownership of the given object is transferred to this ContextHub instance.
     *
     * @param ctx The given object of type T
     */
    template <typename T, ContextOwnership::type TransferOwnership/* = TransferOwnershipDefault*/>// NOTE 20101015 Nothing - Default template argument in template function is a C++0x feature, not supported in C++03 standard.
    inline void set(T* ctx, const KeyType& key)
    {
        if(TransferOwnership == ContextOwnership::transfer)
        {
            refSharedContext<T>(key) = shared_ptr<T>(ctx);
        }
        else
        {
            refSharedContext<T>(key) = shared_ptr<T>(ctx, NullDeleter());
        }
    }
    template <typename T>
    inline void set(T* ctx, const KeyType& key)
    {
        if(TransferOwnershipDefault == ContextOwnership::transfer)
        {
            refSharedContext<T>(key) = shared_ptr<T>(ctx);
        }
        else
        {
            refSharedContext<T>(key) = shared_ptr<T>(ctx, NullDeleter());
        }
    }

    /**
     * Retrieve the object according to the given type T.
     *
     * @return The pointer to the stored object. Return null pointer if it's not set previously.
     */
    template <typename T>
    inline T* get(const KeyType& key)
    {
        return static_pointer_cast<T>(refSharedContext<T>(key)).get();
    }

    /**
     * Remove the previously stored object instance of type T.
     *
     * @note If the TransferOwnership template parameter is set, ContextHub will automatically destroy the object; otherwise
     */
    template <typename T>
    inline void reset(const KeyType& key)
    {
        refSharedContext<T>(key).reset();
    }

    inline std::size_t size()
    {
        return mSharedContextObjects.size();
    }

//  template<typename F>
//  void foreach(F functor)
//  {
//      std::for_each(mSharedContextObjects.begin(), mSharedContextObjects.end(), functor);
//  }

private:
    /**
     * The magic trick to store and access context object by using static
     * initialization to identify the index of a specific type.
     *
     * @return The reference to the shared pointer
     */
    template <typename T>
    inline shared_ptr<void>& refSharedContext(const KeyType& name)
    {
        typename std::unordered_map<KeyType, shared_ptr<void> >::iterator it = mSharedContextObjects.find(name);
        if(UNLIKELY(it == mSharedContextObjects.end()))
        {
            mSharedContextObjects[name] = shared_ptr<void>();
            return mSharedContextObjects[name];
        }
        else
        {
            return it->second;
        }
    }

    std::unordered_map<KeyType, shared_ptr<void> > mSharedContextObjects;
};

template <typename Trait, typename T, T DefaultValue>
struct SimpleContextTrait
{
    SimpleContextTrait(const T& _value = DefaultValue) : value(_value)
    { }
    T value;
};

}
#endif/*ZILLIANS_CONTEXTHUB_H_*/
