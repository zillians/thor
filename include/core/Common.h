/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_COMMON_H_
#define ZILLIANS_COMMON_H_

//////////////////////////////////////////////////////////////////////////
#include <boost/assert.hpp>

#ifdef _WIN32
#define MSVC_VERSION    _MSC_VER
#else
#define GCC_VERSION     (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#endif

#ifdef _WIN32
#include <xutility>
#endif

//////////////////////////////////////////////////////////////////////////
#define ZN_OK       0
#define ZN_ERROR    -1

//////////////////////////////////////////////////////////////////////////
/// Macros to help safely free memory and nullize afterwards
#ifndef NULL
#define NULL ((void*)0)
#endif

#define SAFE_NULL(x)   { x = NULL; }
#define SAFE_FREE(x)   { if(!!(x)) ::free(x); x = NULL; }
#define SAFE_DELETE(x) { if(!!(x)) delete (x); x = NULL; }
#define SAFE_DELETE_ARRAY(x) { if(!!(x)) delete[] (x); x = NULL; }

//////////////////////////////////////////////////////////////////////////
#define UNUSED_ARGUMENT(x) \
    (void)x

#define UNREACHABLE_CODE() \
    BOOST_ASSERT(false && "reaching unreachable code")

#define UNIMPLEMENTED_CODE() \
    BOOST_ASSERT(false && "not yet implemented")

#define NOT_NULL(expr) \
    BOOST_ASSERT((expr) != nullptr && "null pointer exception")

//////////////////////////////////////////////////////////////////////////
/// Macros to support C++ visibility (supported after GCC 4.0 to reduce DSO performance)
#if defined _WIN32 || defined __CYGWIN__
    #ifdef BUILD_SHARED_LIBS
        #ifdef __GNUC__
            #define ZILLIANS_API __attribute__((dllexport))
        #else
            #define ZILLIANS_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
        #endif
    #elif defined __GNUC__
        #define ZILLIANS_API __attribute__((dllimport))
    #else
        #define ZILLIANS_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
    #endif
    #define DLL_LOCAL
#else
    #if __GNUC__ >= 4
        #define ZILLIANS_API    __attribute__ ((visibility("default")))
        #define ZILLIANS_LOCAL  __attribute__ ((visibility("hidden")))
    #else
        #define ZILLIANS_API
        #define ZILLIANS_LOCAL
    #endif
#endif

#ifdef __GNUC__
#define pure_function __attribute__((const))
#else
#define pure_function
#endif

#define restricted_pointer __restrict

// Generic helper definitions for shared library support
//#if defined _WIN32 || defined __CYGWIN__
//  #define ZILLIANS_HELPER_DLL_IMPORT __declspec(dllimport)
//  #define ZILLIANS_HELPER_DLL_EXPORT __declspec(dllexport)
//  #define ZILLIANS_HELPER_DLL_LOCAL
//#else
//  #if __GNUC__ >= 4
//      #define ZILLIANS_HELPER_DLL_IMPORT __attribute__ ((visibility("default")))
//      #define ZILLIANS_HELPER_DLL_EXPORT __attribute__ ((visibility("default")))
//      #define ZILLIANS_HELPER_DLL_LOCAL  __attribute__ ((visibility("hidden")))
//  #else
//      #define ZILLIANS_HELPER_DLL_IMPORT
//      #define ZILLIANS_HELPER_DLL_EXPORT
//      #define ZILLIANS_HELPER_DLL_LOCAL
//  #endif
//#endif
//
//#ifdef ZILLIANS_BUILD_DLL
//  #ifdef ZILLIANS_DLL_EXPORTS
//    #define API_DECL ZILLIANS_HELPER_DLL_EXPORT
//  #else
//    #define API_DECL ZILLIANS_HELPER_DLL_IMPORT
//  #endif
//  #define API_LOCAL_DECL ZILLIANS_HELPER_DLL_LOCAL
//#else
//  #define API_DECL
//  #define API_LOCAL_DECL
//#endif

//////////////////////////////////////////////////////////////////////////
// GNU GCC branch prediction optimization
// TODO: find cooresponding pragma in MSVC and other compiler
#ifdef __GNUC__
    #define LIKELY(x)   __builtin_expect(!!(x), 1)
    #define UNLIKELY(x) __builtin_expect(!!(x), 0)
#else
    #define LIKELY(x) (x)
    #define UNLIKELY(x) (x)
#endif

//////////////////////////////////////////////////////////////////////////
/// Enable/Disable template separation model compilation
/// NOTE: currently there's no GNU compiler support for separation compilation model
//#define ZN_ENABLE_TEMPLATE_SEPARATION_MODEL

//////////////////////////////////////////////////////////////////////////
// Network Parameters
#define ZN_IPV6_SUPPORT 0
#define ZN_DEFER_ACCEPT_SUPPORT 1

//////////////////////////////////////////////////////////////////////////
// MIN and MAX macro (using GNU C++ extension if applicable)
//#ifdef __GNUC__
//  #define MIN(x,y) ((x) <? (y))
//  #define MAX(x,y) ((x) >? (y))
//#else
//  #define MIN(x,y)  ((x) < (y) ? (x) : (y))
//  #define MAX(x,y)  ((x) > (y) ? (x) : (y))
//#endif


#endif/*ZILLIANS_COMMON_H_*/
