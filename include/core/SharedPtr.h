/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_SHAREDPTR_H_
#define ZILLIANS_SHAREDPTR_H_

#include <memory>

#include "core/Common.h"

namespace zillians {

using std::static_pointer_cast;
using std::const_pointer_cast;
using std::dynamic_pointer_cast;
using std::shared_ptr;
using std::weak_ptr;
using std::enable_shared_from_this;
using std::make_shared;

struct null_deleter
{
    void operator()(void const *) const
    { }
};

template<typename T>
struct reference_holder
{
    reference_holder(const shared_ptr<T>& obj) : ref(obj) { }
    void operator()(void const*) const
    { }
    const shared_ptr<T> ref;
};

template<typename A, typename B>
shared_ptr<B> reinterpret_pointer_cast(const shared_ptr<A>& obj)
{
    return shared_ptr<B>((B*)obj.get(), reference_holder<A>(obj));
}

} // namespace 'zillians'

/**
 * @brief Allow direct comparison of shared_ptr and its wrapping type (for "equal to" operator)
 */
template<class T>
inline bool operator == (const zillians::shared_ptr<T> &a, const T* b)
{
    return (a.get() == b);
}

/**
 * @brief Allow direct comparison of shared_ptr and its wrapping type (for "not equal to" operator)
 */
template<class T>
inline bool operator != (const zillians::shared_ptr<T> &a, const T* b)
{
    return (a.get() != b);
}

#endif/*ZILLIANS_SHAREDPTR_H_*/
