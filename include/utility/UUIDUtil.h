/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_UUID_H_
#define ZILLIANS_UUID_H_

#include <cstddef>

#include <string>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace zillians {

class UUID : public boost::uuids::uuid
{
public:
    UUID() noexcept = default; // will be uninitialized
    UUID(const boost::uuids::uuid&  impl) noexcept;
    UUID(      boost::uuids::uuid&& impl) noexcept;

public:
    static UUID nil() noexcept;
    static UUID random();
    static UUID parse(const std::string& desc);

    std::string toString() const;
    std::string toString(const char delim) const;
};

} // namespace zillians

#endif /* ZILLIANS_UUID_H_ */
