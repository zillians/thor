/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdint>

#include <boost/fusion/adapted/std_pair.hpp>
#include <boost/optional/optional.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/repository/include/qi_distinct.hpp>
#include <boost/spirit/repository/include/qi_iter_pos.hpp>

namespace zillians {

template <typename Iterator>
struct ProtocolParser : boost::spirit::qi::grammar<Iterator>
{
	ProtocolParser() : ProtocolParser::base_type(rule_start)
	{
		using namespace boost::spirit;
		namespace phoenix = boost::phoenix;

		rule_key   %=   qi::char_("a-zA-Z_") >> *(qi::char_("a-zA-Z_0-9") | qi::char_('-'));
		rule_value %= +(qi::char_("a-zA-Z_0-9") | qi::char_('-'));

		rule_transport %= (( ascii::alpha | qi::char_('_')) > *( ascii::alnum | qi::char_('_') | qi::char_('-')));
		rule_address   %= ((( ascii::alnum | qi::char_('_')) > *( ascii::alnum | qi::char_('_') | qi::char_('.')))) |
				       qi::char_('*');

		rule_option  %= rule_key >> -(qi::lit('=') >> -rule_value);
		rule_options %= rule_option % qi::lit(',');
		rule_start = rule_transport [phoenix::ref(transport) = qi::_1 ] >
		        qi::lit("://") >
				rule_address [phoenix::ref(address) = qi::_1] >
		        ((-(qi::lit(':') > qi::uint_parser<std::uint16_t>())) [phoenix::ref(port) = qi::_1] ) >>
		        -(qi::lit('[') >> rule_options [phoenix::ref(options) = qi::_1] >> qi::lit(']'));
	}

	bool parse(Iterator begin, Iterator end)
	{
		using namespace boost::spirit;

		bool result = qi::phrase_parse(begin, end, *this, qi::space);
		if(!result || begin != end)
			return false;

		return true;
	}

public:
	typedef std::pair<std::string, boost::optional<std::string>> option_t;
	typedef std::vector<std::pair<std::string, boost::optional<std::string>>> options_t;

	std::string transport;
	std::string address;
	boost::optional<std::uint16_t> port;
	options_t   options;


private:
	boost::spirit::qi::rule<Iterator, option_t()> rule_option;
	boost::spirit::qi::rule<Iterator, options_t()> rule_options;
	boost::spirit::qi::rule<Iterator> rule_start;
	boost::spirit::qi::rule<Iterator, std::string()> rule_transport, rule_address, rule_key, rule_value;
};

}
