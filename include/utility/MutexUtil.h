/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_MUTEXUTIL_H_
#define ZILLIANS_MUTEXUTIL_H_

#include <atomic>

namespace zillians {

class spin_mutex
{
public:
    spin_mutex() = default;
    spin_mutex(const spin_mutex&) = delete;
    spin_mutex& operator=(const spin_mutex&) = delete;

    bool try_lock() noexcept;
    void lock() noexcept;
    void unlock() noexcept;

private:
    std::atomic_flag is_locked = ATOMIC_FLAG_INIT;
};

inline bool spin_mutex::try_lock() noexcept
{
    const bool is_prev_locked = is_locked.test_and_set();

    return !is_prev_locked;
}

inline void spin_mutex::lock() noexcept
{
    while (!try_lock());
}

inline void spin_mutex::unlock() noexcept
{
    is_locked.clear();
}

}

#endif /* ZILLIANS_MUTEXUTIL_H_ */
