/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_DLL_PTR_H_
#define ZILLIANS_DLL_PTR_H_

#include <string>

#include <boost/noncopyable.hpp>

namespace zillians {

// This class provides RAII ability for dll auto unloading.
// TODO support different load modes
class dll_ptr : boost::noncopyable
{
public:
    constexpr dll_ptr(              ) noexcept : dll(nullptr) {}
    constexpr dll_ptr(std::nullptr_t) noexcept : dll(nullptr) {}

    explicit dll_ptr(const std::string& dll_path);
    explicit dll_ptr(const char*        dll_path);

    dll_ptr(dll_ptr&& ref) noexcept;

    ~dll_ptr();

    void* load(const std::string& dll_path);
    void* load(const char*        dll_path);
    void  unload();

    dll_ptr& operator=(std::nullptr_t);
    dll_ptr& operator=(dll_ptr&& ref);

    void* find_symbol(const char*        name) noexcept;
    void* find_symbol(const std::string& name) noexcept;

    template<typename target_type, typename arg_type>
    target_type* find_symbol(arg_type&& arg);

    operator void*() const noexcept;

    void swap(dll_ptr& ref) noexcept;

private:
    void* dll;
};

template<typename target_type, typename arg_type>
inline target_type* dll_ptr::find_symbol(arg_type&& arg)
{
    void*const        ptr = find_symbol(std::forward<arg_type>(arg));
    auto*const casted_ptr = reinterpret_cast<target_type*>(ptr);

    return casted_ptr;
}

inline void swap(dll_ptr& p1, dll_ptr& p2) noexcept
{
    p1.swap(p2);
}

}

#endif /* ZILLIANS_DLL_PTR_H_ */
