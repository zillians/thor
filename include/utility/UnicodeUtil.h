/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_UNICODEUTIL_H_
#define ZILLIANS_UNICODEUTIL_H_

#include <iostream>
#include <fstream>
#include <locale>
#include <memory>
#include <string>
#include <algorithm>
#include <boost/regex/pending/unicode_iterator.hpp>

namespace zillians {

namespace detail {

struct disable_stdio_sync
{
    explicit disable_stdio_sync(bool sync) { previous_sync = std::ios::sync_with_stdio(sync); }
    ~disable_stdio_sync() { std::ios::sync_with_stdio(previous_sync); }
    bool previous_sync;
};

}

std::locale& get_default_locale();
std::locale& get_posix_locale();
std::locale& get_utf8_locale();
std::locale& get_c_locale();

template<typename StreamT>
void enable_default_locale(StreamT& stream)
{
    static detail::disable_stdio_sync s(false);
    stream.imbue(get_default_locale());
}

template<typename StreamT>
void enable_posix_locale(StreamT& stream)
{
    static detail::disable_stdio_sync s(false);
    stream.imbue(get_posix_locale());
}

template<typename StreamT>
void enable_utf8_locale(StreamT& stream)
{
    static detail::disable_stdio_sync s(false);
    stream.imbue(get_utf8_locale());
}

template<typename StreamT>
void enable_c_locale(StreamT& stream)
{
    static detail::disable_stdio_sync s(false);
    stream.imbue(get_c_locale());
}

void utf8_to_ucs4(const std::string& input, std::wstring& output);
void ucs4_to_utf8(const std::wstring& input, std::string& output);

void wcs_to_cstr(const wchar_t* src, char* dest);
void cstr_to_wcs(const char* src, wchar_t* dest);
std::wstring s_to_ws(std::string s);
std::string ws_to_s(std::wstring ws);

}

#endif /* ZILLIANS_UNICODEUTIL_H_ */
