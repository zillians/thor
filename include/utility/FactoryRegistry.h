/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FACTORYREGISTRY_H_
#define ZILLIANS_FACTORYREGISTRY_H_

#include <cassert>

#include <functional>
#include <map>
#include <memory>
#include <type_traits>
#include <utility>

#include <boost/noncopyable.hpp>

namespace zillians {

/**
 * Usage:
 * @code
 * struct base_t          {                    virtual ~base_t() {}; };
 * struct d1_t   : base_t { enum { flag = 1 };                       };
 * struct d2_t   : base_t { enum { flag = 2 };                       };
 *
 * using registry = zillians::factory_registry<base_t>;
 *
 * namespace {
 *     registry::auto_register<d1_t> d1_register;
 *     registry::auto_register<d2_t> d2_register;
 * }
 *
 * int main()
 * {
 *     std::unique_ptr<base_t> ptr_0 = registry::create(0); // nullptr
 *     std::unique_ptr<base_t> ptr_1 = registry::create(1); // valid pointer points to d1_t instance
 *     std::unique_ptr<base_t> ptr_2 = registry::create(2); // valid pointer points to d2_t instance
 *
 *     return 0;
 * }
 * @endcode
 */
template<typename type, typename... arg_types>
struct factory_registry : boost::noncopyable
{
    using factory_type = std::function<type *(arg_types&&...)>;
    using mapping_type = std::map<int, factory_type>;

    static typename mapping_type::size_type size()
    {
        const auto& mapping = get_mapping();

        return mapping.size();
    }

    static bool empty()
    {
        const auto& mapping = get_mapping();

        return mapping.empty();
    }

    template<typename... create_arg_types>
    static std::unique_ptr<type> create(int key, create_arg_types&&... args)
    {
        const auto& mapping = get_mapping();
        const auto& pos     = mapping.find(key);

        if (pos == mapping.end())
            return nullptr;

        assert(pos->second != nullptr && "null pointer exception");

        const auto& factory = pos->second;

        return std::unique_ptr<type>(factory(std::forward<create_arg_types>(args)...));
    }

    template<typename impl_type>
    struct auto_register : boost::noncopyable
    {
        explicit  auto_register(                      int real_flag = impl_type::flag);
        explicit  auto_register(factory_type factory, int real_flag = impl_type::flag);
                 ~auto_register();

    private:
        int flag;
    };

private:
    template<typename impl_type>
    friend struct auto_register;

    static mapping_type& get_mapping()
    {
        static_assert(std::is_polymorphic        <type>::value, "non-polymorphic type, you may have design issue...");
        static_assert(std::has_virtual_destructor<type>::value, "you hide the underlying type without virtual destructor, this leads to memory leak");

        static mapping_type mapping;

        return mapping;
    }

    factory_registry() = delete;
};

template<typename type, typename... arg_types>
template<typename impl_type>
inline factory_registry<type, arg_types...>::auto_register<impl_type>::auto_register(int real_flag/* = impl_type::flag*/)
    : auto_register(
          [](arg_types&&... args)
          {
              return new impl_type(std::forward<arg_types>(args)...);
          },
          real_flag
      )
{
}

template<typename type, typename... arg_types>
template<typename impl_type>
inline factory_registry<type, arg_types...>::auto_register<impl_type>::auto_register(factory_type factory, int real_flag/* = impl_type::flag*/)
    : flag(-1)
{
    static_assert(std::is_convertible<impl_type*, type*>::value, "cannot convert derived type pointer to expected type");

    assert(real_flag >= 0       && "negative value is not allowed, they have special meaning");
    assert(factory   != nullptr && "null pointer exception");

    auto& mapping = get_mapping();

    const auto& insertion_result = mapping.emplace(real_flag, std::move(factory));
    const auto& is_inserted      = insertion_result.second;

    assert(is_inserted && "duplicated!?");
    if (is_inserted)
        flag = real_flag;
}

template<typename type, typename... arg_types>
template<typename impl_type>
inline factory_registry<type, arg_types...>::auto_register<impl_type>::~auto_register()
{
    if (flag < 0)
        return;

    auto& mapping = get_mapping();

    const auto& erased_count = mapping.erase(flag);

    assert(erased_count == 1 && "not yet registered!?");
}

}

#endif /* ZILLIANS_FACTORYREGISTRY_H_ */
