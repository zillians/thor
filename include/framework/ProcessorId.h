/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROCESSORID_H_
#define ZILLIANS_FRAMEWORK_PROCESSORID_H_

namespace zillians { namespace framework {

struct ProcessorId
{
	enum Id
	{
		PROCESSOR_PRINCIPLE     = 0,
		PROCESSOR_EXTERNAL,
		PROCESSOR_DATABASE,
		PROCESSOR_LOGGER,
		PROCESSOR_GATEWAY_START	/// NOTE: this should be the last one
	};

	static const char* id_to_name(uint32 id)
	{
		switch(id)
		{
		case PROCESSOR_PRINCIPLE: return "PROCESSOR_MT_0";
		case PROCESSOR_EXTERNAL: return "PROCESSOR_EXTERNAL";
		case PROCESSOR_DATABASE: return "PROCESSOR_DATABASE";
		case PROCESSOR_LOGGER: return "PROCESSOR_LOGGER";
		default: return "PROCESSOR_GATEWAY";
		}
	}
};

struct ServiceId
{
	enum Id
	{
		SERVICE_RUNTIME,
		SERVICE_OBJECT ,
		SERVICE_DOMAIN ,
	};
};

} }

#endif /* ZILLIANS_FRAMEWORK_PROCESSORID_H_ */
