/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTOR_REMOTE_H
#define ZILLIANS_FRAMEWORK_EXECUTOR_REMOTE_H

#include <cstddef>
#include <cstdint>

#include <atomic>
#include <deque>
#include <memory>
#include <mutex>
#include <queue>
#include <unordered_map>
#include <tuple>
#include <vector>

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/unordered_set_of.hpp>
#include <boost/range/algorithm/copy.hpp>

#include <tbb/concurrent_hash_map.h>

#include "framework/Executor.h"
#include "framework/InvocationRequest.h"
#include "network/Session.h"

#include "thor/lang/Domain.h"

namespace zillians { namespace framework {

class executor_remote : public executor
{
private:
    using base_type = executor;

public:
    using local_id_type = decltype(std::declval<executor>().get_id());
    
     executor_remote(executor_rt& local);
    ~executor_remote() = default;

    void send_invocation_request(std::int64_t func_id, std::int64_t session_id, std::deque<std::int8_t>&& parameters);
    void receive_invocation_request(network::session* session, network::session::receive_handler_type handler);
   
    bool add(network::session* session);
    bool remove(network::session* session);
   
    network::session* get_session(std::int64_t session_id);
    std::int64_t      get_session_id(network::session* session);
    
protected:
    virtual void do_work() override;

private:
    std::int64_t  to_session_id(local_id_type local_id);
    local_id_type get_local_id(network::session* session);
    
    network::session::buffer_type* create_read_buffer_for(network::session* session);
    network::session::buffer_type* get_read_buffer(network::session* session);

    void send_invocation_requests_to_remote();
    void process_read_data(network::session* session, const boost::system::error_code& error, std::size_t bytes_transferred, network::session::receive_handler_type handler);

private:
    std::atomic<local_id_type> next_local_id;

    // executor to perform local function calls
    executor_rt& local;

    // requests which are waiting to be sent 
    std::mutex tasks_mutex;
    std::queue<std::tuple<std::int64_t, invocation_request>> pending_tasks;

    // local_id -> session, all connections, created by listeners and session themselves
    std::mutex sessions_mutex;
    boost::bimaps::bimap<
        boost::bimaps::unordered_set_of<local_id_type>,
        boost::bimaps::unordered_set_of<network::session*>
    > local_id_and_sessions;

    // session -> read buffers
    tbb::concurrent_hash_map<
        network::session*,
        std::shared_ptr<network::session::buffer_type>
    > read_buffers;
};

} } // namespace zillians::framework

#endif
