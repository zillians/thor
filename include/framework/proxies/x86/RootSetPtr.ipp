/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROXIES_X86_ROOTSETPTR_IPP
#define ZILLIANS_FRAMEWORK_PROXIES_X86_ROOTSETPTR_IPP

#include <cstddef>
#include <algorithm>

#include "framework/proxies/X86.h"
#include "framework/proxies/x86/RootSetPtr.h"

namespace thor { namespace lang {

class Object;

} }  // namespace thor::lang

namespace zillians { namespace framework { namespace x86_detail {

class object_manager;

} } }

namespace zillians { namespace framework { namespace proxies { namespace x86 {

template<typename value_type>
constexpr root_set_ptr<value_type>::root_set_ptr() noexcept
    : root_set_ptr(nullptr, nullptr)
{
}

template<typename value_type>
constexpr root_set_ptr<value_type>::root_set_ptr(std::nullptr_t) noexcept
    : root_set_ptr(nullptr, nullptr)
{
}

template<typename value_type>
root_set_ptr<value_type>::root_set_ptr(element_type* ptr, x86_detail::object_manager* object_manager)
    : object_manager(object_manager), ptr(ptr)
{
    gc_shared_root_set_add(ptr, object_manager);
}

template<typename value_type>
root_set_ptr<value_type>::root_set_ptr(const root_set_ptr& ref)
    : root_set_ptr(ref.ptr, ref.object_manager)
{
}

template<typename value_type>
root_set_ptr<value_type>::root_set_ptr(root_set_ptr&& ref) noexcept
    : root_set_ptr()
{
        ptr = ref.ptr;
    ref.ptr = nullptr;

        object_manager = ref.object_manager;
    ref.object_manager = nullptr;
}

template<typename value_type>
root_set_ptr<value_type>::~root_set_ptr() noexcept
{
    reset();
}

template<typename value_type>
void root_set_ptr<value_type>::reset()
{
    reset(nullptr);
}

template<typename value_type>
void root_set_ptr<value_type>::reset(std::nullptr_t)
{
    gc_shared_root_set_remove(ptr, object_manager);

    ptr = nullptr;
}

template<typename value_type>
void root_set_ptr<value_type>::reset(element_type* new_ptr)
{
    root_set_ptr local(new_ptr, object_manager);

    swap(local);
}

template<typename value_type>
void root_set_ptr<value_type>::swap(root_set_ptr& ref) const noexcept
{
    std::swap(ptr           , ref.ptr           );
    std::swap(object_manager, ref.object_manager);
}

template<typename value_type> inline auto root_set_ptr<value_type>::get() const noexcept -> element_type* { return ptr; }

template<typename value_type> inline auto root_set_ptr<value_type>::operator* () const noexcept -> element_type& { return *get(); }
template<typename value_type> inline auto root_set_ptr<value_type>::operator->() const noexcept -> element_type* { return  get(); }

template<typename value_type> inline      root_set_ptr<value_type>::operator bool() const noexcept { return ptr != nullptr; }
template<typename value_type> inline bool root_set_ptr<value_type>::operator !   () const noexcept { return ptr == nullptr; }

template<typename value_type>
inline root_set_ptr<value_type>& root_set_ptr<value_type>::operator=(const root_set_ptr& ref)
{
    root_set_ptr local(ref);

    swap(local);

    return *this;
}

template<typename value_type>
inline root_set_ptr<value_type>& root_set_ptr<value_type>::operator=(root_set_ptr&& ref)
{
    root_set_ptr local(std::move(ref));

    swap(local);

    return *this;
}

template<typename value_type> inline bool root_set_ptr<value_type>::operator==(const root_set_ptr& rhs) const noexcept { return ptr == rhs.ptr; }
template<typename value_type> inline bool root_set_ptr<value_type>::operator< (const root_set_ptr& rhs) const noexcept { return ptr <  rhs.ptr; }

// TODO specialize comparison on nullptr for performance

template<typename value_type>
inline root_set_ptr<value_type> make_root_set_ptr(value_type*const ptr, x86_detail::object_manager* object_manager)
{
    return root_set_ptr<value_type>(ptr, object_manager);
}

template<typename value_type>
inline void swap(root_set_ptr<value_type>& a, root_set_ptr<value_type>& b) noexcept(noexcept(a.swap(b)))
{
    a.swap(b);
}

} } } }

#endif /* ZILLIANS_FRAMEWORK_PROXIES_X86_ROOTSETPTR_IPP */
