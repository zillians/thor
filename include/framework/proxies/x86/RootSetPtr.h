/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_PROXIES_X86_ROOTSETPTR_H
#define ZILLIANS_FRAMEWORK_PROXIES_X86_ROOTSETPTR_H

#include <cstddef>

#include <boost/operators.hpp>

namespace thor { namespace lang {

class Object;

} }  // namespace thor::lang

namespace zillians { namespace framework { namespace x86_detail {

class object_manager;

} } }

namespace zillians { namespace framework { namespace proxies { namespace x86 {

template<typename value_type>
class root_set_ptr : public boost::totally_ordered<root_set_ptr<value_type>> {
public:
    using element_type  = value_type;

public:
    constexpr root_set_ptr() noexcept;
    constexpr root_set_ptr(std::nullptr_t) noexcept;
    explicit  root_set_ptr(element_type* ptr, x86_detail::object_manager* object_manager = nullptr);
              root_set_ptr(const root_set_ptr&  ref);
              root_set_ptr(      root_set_ptr&& ref) noexcept;

    ~root_set_ptr() noexcept;

    void reset();
    void reset(std::nullptr_t);
    void reset(element_type* new_ptr);

    void swap(root_set_ptr& ref) const noexcept;

    element_type* get() const noexcept;

    element_type& operator* () const noexcept;
    element_type* operator->() const noexcept;

         operator bool() const noexcept;
    bool operator !   () const noexcept;

    root_set_ptr& operator=(const root_set_ptr&  ref);
    root_set_ptr& operator=(      root_set_ptr&& ref);

    bool operator==(const root_set_ptr& rhs) const noexcept;
    bool operator< (const root_set_ptr& rhs) const noexcept;

private:
    x86_detail::object_manager* object_manager;

    element_type* ptr;
};

// TODO specialize comparison on nullptr for performance

template<typename value_type>
root_set_ptr<value_type> make_root_set_ptr(value_type*const ptr, x86_detail::object_manager* object_manager = nullptr);

template<typename value_type>
void swap(root_set_ptr<value_type>& a, root_set_ptr<value_type>& b) noexcept(noexcept(a.swap(b)));

} } } }

#include "framework/proxies/x86/RootSetPtr.ipp"

#endif /* ZILLIANS_FRAMEWORK_PROXIES_X86_ROOTSETPTR_H */
