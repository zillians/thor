/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_BUFFERBASE_H_
#define ZILLIANS_FRAMEWORK_BUFFERBASE_H_

#include <cstddef>

#include "core/IntTypes.h"
#include "framework/buffer/BufferConstants.h"

namespace zillians { namespace framework { namespace buffer {

/**
 * Base class for all dynamic-sized buffer
 */
template<typename T>
struct DynamicBufferBase
{
    DynamicBufferBase() : location(buffer_location_t::unknown), size(0), owner(-1), data(NULL) { }

	/// buffer location indicates the actual place the buffer is being stored
	buffer_location_t::type location;

	/// the allocated size of the buffer
	std::size_t size;

	/// owner id which indicates the actual owner that is responsible for the buffer's memory (both allocation and de-allocation)
	int32 owner;

	/// the actual buffer content
	void* data;

//	typename T::Dimension declared_dim;

	bool is_same(const DynamicBufferBase& other)
	{
		return location == other.location && size == other.size && owner == other.owner && data == other.data;
	}

private:
	DynamicBufferBase(const DynamicBufferBase&);
	DynamicBufferBase& operator=(const DynamicBufferBase&);
};

template<buffer_location_t::type T>
struct BufferRawAllocator;

} } }

#endif /* ZILLIANS_FRAMEWORK_BUFFERBASE_H_ */
