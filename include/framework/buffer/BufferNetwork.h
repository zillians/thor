/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_BUFFERNETWORK_H_
#define ZILLIANS_FRAMEWORK_BUFFERNETWORK_H_

#include <unordered_map>

#include <boost/array.hpp>

#include "core/Atomic.h"
#include "core/Singleton.h"
#include "utility/Foreach.h"
#include "framework/buffer/BufferManager.h"
#include "framework/Configuration.h"

namespace zillians { namespace framework { namespace buffer {

/**
 * BufferNetwork is used to avoid unnecessary buffer copy & allocations while exchanging data between processors/services.
 */
class BufferNetwork : public Singleton<BufferNetwork, SingletonInitialization::automatic>
{
public:
	BufferNetwork()
	{ }

public:
	/**
	 * Declare the location for buffer read of a specific buffer type
	 * @param processor_id processor unique identifier
	 * @param location the preferred location of the buffer
	 * @param priority the higher the priority is, the more likely (preferred) the location will be used
	 */
	template<typename T>
	void declareReadableLocation(uint32 processor_id, buffer_location_t::type location, uint32 priority)
	{
		LocationPriorities& lp = getBufferLocationsWithPrioritiesForRead<T>(processor_id);
		lp.push_back(std::make_pair(location, priority));
	}

	/**
	 * Declare the location for buffer write of a specific buffer type
	 * @param processor_id processor unique identifier
	 * @param location the preferred location of the buffer
	 * @param priority the higher the priority is, the more likely (preferred) the location will be used
	 */
	template<typename T>
	void declareWritableLocation(uint32 processor_id, buffer_location_t::type location, uint32 priority)
	{
		LocationPriorities& lp = getBufferLocationsWithPrioritiesForWrite<T>(processor_id);
		lp.push_back(std::make_pair(location, priority));
	}

	template<typename T>
	static void declareReadableLocationsWithDefaultPriority(uint32 processor_id)
	{
		BufferNetwork::instance()->declareReadableLocation<T>(processor_id, buffer_location_t::host_unpinned, 0);
#ifdef BUILD_WITH_RDMA
		if(Configuration::instance()->enable_rdma)
		{
			BufferNetwork::instance()->declareReadableLocation<T>(processor_id, buffer_location_t::host_ib_pinned, 1);
		}
#endif
#ifdef BUILD_WITH_CUDA
		if(Configuration::instance()->enable_cuda)
		{
			BufferNetwork::instance()->declareReadableLocation<T>(processor_id, buffer_location_t::host_ptx_pinned, 2);
		}
#endif
#ifdef BUILD_WITH_OPENCL
		if(Configuration::instance()->enable_opencl)
		{
			BufferNetwork::instance()->declareReadableLocation<T>(local_id, buffer_location_t::host_ocl_pinned, 3);
		}
#endif
	}

	template<typename T>
	static void declareWritableLocationsWithDefaultPriority(uint32 processor_id)
	{
		BufferNetwork::instance()->declareWritableLocation<T>(processor_id, buffer_location_t::host_unpinned, 0);
#ifdef BUILD_WITH_RDMA
		if(Configuration::instance()->enable_rdma)
		{
			BufferNetwork::instance()->declareWritableLocation<T>(processor_id, buffer_location_t::host_ib_pinned, 1);
		}
#endif
#ifdef BUILD_WITH_CUDA
		if(Configuration::instance()->enable_cuda)
		{
			BufferNetwork::instance()->declareWritableLocation<T>(processor_id, buffer_location_t::host_ptx_pinned, 2);
		}
#endif
#ifdef BUILD_WITH_OPENCL
		if(Configuration::instance()->enable_opencl)
		{
			BufferNetwork::instance()->declareWritableLocation<T>(local_id, buffer_location_t::host_ocl_pinned, 3);
		}
#endif
	}

    /**
     * Move given buffer from one processor to another.
     *
     * The information of compatible buffer location for each processor will be used to reduce memory copy.
     * If the buffer copy is necessary, it would make use of buffer pool to allocate pooled buffer on target
     * processor, or just use buffer builder to make new buffer from given reference.
     */
    template<typename T>
    bool move(uint32 source, uint32 target, T& source_buffer, T& target_buffer)
    {
        if (source != target && !checkCompatibleLocation<T>(source, target, source_buffer.location))
            return false;

        source_buffer.owner = target;
        source_buffer.moveTo(target_buffer);

        return true;
    }

	template<typename T>
	bool copy(T& source_buffer, T& target_buffer)
	{
	    return source_buffer.copyTo(target_buffer);
	}

    template<typename T>
    bool moveOrCopy(uint32 source, uint32 target, T& source_buffer, T& target_buffer)
    {
        if (source != target && !checkCompatibleLocation<T>(source, target, source_buffer.location))
        {
            return source_buffer.copyTo(target_buffer);
        }
        else
        {
            source_buffer.owner = target;
            source_buffer.moveTo(target_buffer);
            return true;
        }
    }

	template<typename T, typename Dimension>
	bool create(uint32 source, T& buffer, const Dimension& dim)
	{
		buffer.owner = source;

		// start from the first cardinal
		uint32 cardinal = 0;

		LocationPriorities& lp = getBufferLocationsWithPrioritiesForWrite<T>(source);
		while(cardinal < lp.size())
		{
			if(createByLocation<T>(lp[cardinal].first, buffer, dim))
				return true;
			else
				LOG4CXX_DEBUG(mLogger, "failed to create " << typeid(T).name() << "> on location " << buffer_location_t::get_string(lp[cardinal].first));

			++cardinal;
		}

		return false;
	}

	template<typename T, typename Dimension>
	bool create(uint32 source, uint32 target, T& buffer, const Dimension& dim)
	{
		// the owner is always the target who is responsible for destruct the buffer
		buffer.owner = target;

		// start from the first cardinal
		uint32 cardinal = 0;

		while(true)
		{
			if(!queryCompatibleLocation<T>(source, target, buffer.location, cardinal))
			{
				// if we failed to find compatible buffer for given source and target, we fall back to meet the source buffer location requirement
				return create<T>(source, buffer, dim);
			}

			if(createByLocation<T>(buffer.location, buffer, dim))
				return true;

			++cardinal;
		}

		return false;
	}

	template<typename T, typename Dimension>
	bool createByLocation(buffer_location_t::type location, T& buffer, const Dimension& dim)
	{
	    return buffer.construct(location, dim);
	}

	template<typename T>
	bool destroy(T& buffer)
	{
	    return buffer.destruct();
	}

	void clearAll()
	{
		mProcessorCompatibleLocationsForRead.clear();
		mProcessorCompatibleLocationsForWrite.clear();
		mLocationLookupTable.clear();
	}

private:
	/// used to generate unique buffer identifier by buffers' type
	volatile static uint32 mUniqueBufferId;

	/// location with priority tabs
	typedef std::pair<buffer_location_t::type /*location*/, uint32 /*priority*/> LocationPriority;
	struct LocationPriorityCompare
	{
		bool operator()(const LocationPriority& t1, const LocationPriority& t2)
		{
			return t1.second > t2.second;
		}
	};

	/// sorted locations by priorities
	typedef std::vector<LocationPriority> LocationPriorities;
	typedef std::unordered_map<uint32 /*buffer_id*/, LocationPriorities> CompatibleLocations;

	/// compatible buffer location ids to generate look-up tables
	typedef std::unordered_map<uint32 /*processor_id*/, CompatibleLocations /*compatible_locations*/> ProcessorCompatibleLocations;
	ProcessorCompatibleLocations mProcessorCompatibleLocationsForRead;
	ProcessorCompatibleLocations mProcessorCompatibleLocationsForWrite;

	/// 2D look-up table to find out the sorted buffer location given the source and the target processor id
	typedef boost::array<boost::array<LocationPriorities, buffer_constants::max_targets>, buffer_constants::max_targets> LocationLookupTable;
	std::vector<LocationLookupTable> mLocationLookupTable;

private:
	template<typename T>
	uint32 getUniqueBufferId()
	{
		static uint32 id = atomic::inc<uint32>(&mUniqueBufferId) - 1;
		return id;
	}

	LocationPriorities& getBufferLocationsWithPrioritiesForRead(uint32 processor_id, uint32 buffer_id)
	{
		ProcessorCompatibleLocations::iterator it_processor_compatible_locations = mProcessorCompatibleLocationsForRead.find(processor_id);
		if(it_processor_compatible_locations == mProcessorCompatibleLocationsForRead.end())
		{
			mProcessorCompatibleLocationsForRead.insert(std::make_pair(processor_id, CompatibleLocations()));
			it_processor_compatible_locations = mProcessorCompatibleLocationsForRead.find(processor_id);
		}

		CompatibleLocations::iterator it_compatible_locations = it_processor_compatible_locations->second.find(buffer_id);
		if(it_compatible_locations == it_processor_compatible_locations->second.end())
		{
			it_processor_compatible_locations->second.insert(std::make_pair(buffer_id, LocationPriorities()));
			it_compatible_locations = it_processor_compatible_locations->second.find(buffer_id);
		}

		return it_compatible_locations->second;
	}

	LocationPriorities& getBufferLocationsWithPrioritiesForWrite(uint32 processor_id, uint32 buffer_id)
	{
		ProcessorCompatibleLocations::iterator it_processor_compatible_locations = mProcessorCompatibleLocationsForWrite.find(processor_id);
		if(it_processor_compatible_locations == mProcessorCompatibleLocationsForWrite.end())
		{
			mProcessorCompatibleLocationsForWrite.insert(std::make_pair(processor_id, CompatibleLocations()));
			it_processor_compatible_locations = mProcessorCompatibleLocationsForWrite.find(processor_id);
		}

		CompatibleLocations::iterator it_compatible_locations = it_processor_compatible_locations->second.find(buffer_id);
		if(it_compatible_locations == it_processor_compatible_locations->second.end())
		{
			it_processor_compatible_locations->second.insert(std::make_pair(buffer_id, LocationPriorities()));
			it_compatible_locations = it_processor_compatible_locations->second.find(buffer_id);
		}

		return it_compatible_locations->second;
	}

public:
	template<typename T>
	LocationPriorities& getBufferLocationsWithPrioritiesForRead(uint32 processor_id)
	{
		uint32 buffer_id = getUniqueBufferId<T>();
		return getBufferLocationsWithPrioritiesForRead(processor_id, buffer_id);
	}

	template<typename T>
	LocationPriorities& getBufferLocationsWithPrioritiesForWrite(uint32 processor_id)
	{
		uint32 buffer_id = getUniqueBufferId<T>();
		return getBufferLocationsWithPrioritiesForWrite(processor_id, buffer_id);
	}

public:
	template<typename T>
	bool queryCompatibleLocation(uint32 source, uint32 target, buffer_location_t::type& location, uint32 cardinal)
	{
		uint32 buffer_id = getUniqueBufferId<T>();
		return queryCompatibleLocation(source, target, location, buffer_id, cardinal);
	}

	bool queryCompatibleLocation(uint32 source, uint32 target, buffer_location_t::type& location, uint32 buffer_id, uint32 cardinal)
	{
		LocationPriorities& lp = mLocationLookupTable[buffer_id][source][target];
		if(cardinal < lp.size())
		{
			location = lp[cardinal].first;
			return true;
		}
		else
		{
			return false;
		}
	}

	template<typename T>
	bool checkCompatibleLocation(uint32 source, uint32 target, buffer_location_t::type location)
	{
		uint32 buffer_id = getUniqueBufferId<T>();
		return checkCompatibleLocation(source, target, location, buffer_id);
	}

	bool checkCompatibleLocation(uint32 source, uint32 target, buffer_location_t::type location, uint32 buffer_id)
	{
		LocationPriorities& lp = mLocationLookupTable[buffer_id][source][target];
		for(auto& location_and_priority : lp)
		{
			if(location_and_priority.first == location)
				return true;
		}

		return false;
	}

public:
	/**
	 * Compile the static look-up tables
	 */
	bool commit()
	{
		uint32 max_buffer_id = mUniqueBufferId;
		mLocationLookupTable.reserve(max_buffer_id);
		for(auto count : zero_to(max_buffer_id))
            mLocationLookupTable.push_back(LocationLookupTable());

		// sort all buffer location priorities for every processor
		for(auto i : zero_to(buffer_constants::max_targets))
		{
			for(auto buffer_id : zero_to(max_buffer_id))
			{
				LocationPriorities& lp_write = getBufferLocationsWithPrioritiesForWrite(i, buffer_id);
				std::sort(lp_write.begin(), lp_write.end(), LocationPriorityCompare());

				LocationPriorities& lp_read = getBufferLocationsWithPrioritiesForRead(i, buffer_id);
				std::sort(lp_read.begin(), lp_read.end(), LocationPriorityCompare());
			}
		}

		// compile the static look-up tables
		for(auto buffer_id : zero_to(max_buffer_id))
		{
			for(auto source : zero_to(buffer_constants::max_targets)) // i is the source processor index
			{
				for(auto dest : zero_to(buffer_constants::max_targets)) // j is the destination processor index
				{
					// find out the buffer location that is writable by the source and readable by the destination, sorted by the priority
					LocationPriorities& lp_write = getBufferLocationsWithPrioritiesForWrite(source, buffer_id);
					LocationPriorities& lp_read = getBufferLocationsWithPrioritiesForRead(dest, buffer_id);
					for(auto& to_read : lp_read)
					{
						for(auto& to_write : lp_write)
						{
							if(to_read.first == to_write.first)
							{
								mLocationLookupTable[buffer_id][source][dest].push_back(std::make_pair(to_read.first, 0));
								break;
							}
						}
					}
				}
			}
		}

		return true;
	}

private:
	static log4cxx::LoggerPtr mLogger;
};

} } }


#endif /* ZILLIANS_FRAMEWORK_BUFFERNETWORK_H_ */
