/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_BUFFER_BUFFERALLOCATORS_H_
#define ZILLIANS_FRAMEWORK_BUFFER_BUFFERALLOCATORS_H_
#include "core/Platform.h"
#include "framework/buffer/BufferBase.h"
#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
#include "network/sys/infiniband/detail/IBFactory.h"
#include "network/sys/infiniband/detail/IBDeviceManager.h"
#endif
#endif

#ifdef BUILD_WITH_CUDA
#include <cuda.h>
#include <cuda_runtime_api.h>
#endif
#ifdef __PLATFORM_MAC__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

namespace zillians { namespace framework { namespace buffer {

// TODO restruct this into a table
template<>
struct BufferRawAllocator<buffer_location_t::host_unpinned>
{
	static void* malloc(std::size_t size)
	{
		return ::malloc(size);
	}

	static void free(void* ptr)
	{
		::free(ptr);
	}

	static void* realloc(void* ptr, std::size_t size)
	{
		return ::realloc(ptr, size);
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		::memset(ptr, value, size);
	}
};

#ifdef BUILD_WITH_NETWORK_ADDON
#ifdef BUILD_WITH_RDMA
extern "C" struct ibv_mr* zillians_ibv_reg_mr(struct ibv_pd* pd, void* addr, size_t length);
extern "C" int zillians_ibv_dereg_mr(struct ibv_mr* pd);

template<>
struct BufferRawAllocator<buffer_location_t::host_ib_pinned>
{
	static const std::size_t reserved_size = sizeof(ibv_mr*) * IB_MAX_DEVICES;

	static int device_count()
	{
		static int count = network::sys::IBDeviceManager::instance()->device_count();
		BOOST_ASSERT(count < IB_MAX_DEVICES);
		return count;
	}

	static void* malloc(std::size_t size)
	{
		byte* ptr = (byte*)::malloc(size + reserved_size);

		// register on all infiniband devices
		int count = device_count();
		for(int dev = 0; dev < count; ++dev)
		{
			shared_ptr<ibv_pd> pd = network::sys::IBDeviceManager::instance()->device(dev)->get_global_pd();
			((ibv_mr**)ptr)[dev] = zillians_ibv_reg_mr(pd.get(), ptr + reserved_size, size);
			if(!((ibv_mr**)ptr)[dev])
			{
				::free((void*)ptr);
				return NULL;
			}
		}
		return (void*)(ptr + reserved_size);
	}

	static void free(void* p)
	{
		if(!p) return;

		byte* ptr = ((byte*)p) - reserved_size;

		// unregister on all infiniband devices
		int count = device_count();
		for(int dev = 0; dev < count; ++dev)
		{
			shared_ptr<ibv_pd> pd = network::sys::IBDeviceManager::instance()->device(dev)->get_global_pd();
			zillians_ibv_dereg_mr(((ibv_mr**)ptr)[dev]);
		}

		::free((void*)ptr);
	}

	static void* realloc(void* p, std::size_t size)
	{
		byte* ptr = ((byte*)p) - reserved_size;

		// unregister on all infiniband devices
		int count = device_count();
		for(int dev = 0; dev < count; ++dev)
		{
			shared_ptr<ibv_pd> pd = network::sys::IBDeviceManager::instance()->device(dev)->get_global_pd();
			zillians_ibv_dereg_mr(((ibv_mr**)ptr)[dev]);
		}

		ptr = (byte*)::realloc(ptr, size);

		// register on all infiniband devices
		for(int dev = 0; dev < count; ++dev)
		{
			shared_ptr<ibv_pd> pd = network::sys::IBDeviceManager::instance()->device(dev)->get_global_pd();
			((ibv_mr**)ptr)[dev] = zillians_ibv_reg_mr(pd.get(), ptr + reserved_size, size);
		}

		return (void*)(ptr + reserved_size);
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		::memset(ptr, value, size);
	}

	static ibv_mr* query_mr(void* p, int dev)
	{
		byte* ptr = ((byte*)p) - reserved_size;
		return ((ibv_mr**)ptr)[dev];
	}
};
#endif
#endif

#ifdef BUILD_WITH_CUDA
template<>
struct BufferRawAllocator<buffer_location_t::host_ptx_pinned>
{
	static void* malloc(std::size_t size)
	{
		//byte* ptr = NULL;
		//cudaError_t error = cudaHostAlloc((void**)(&ptr), size, cudaHostAllocPortable | cudaHostAllocMapped);
		//BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda pinned host memory");
		//return (void*)ptr;
		return ::malloc(size);
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
//			cudaError_t error = cudaFreeHost(ptr);
//			BOOST_ASSERT(error == cudaSuccess && "failed to free cuda pinned host memory");
			::free(ptr);
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		::memset(ptr, value, size);
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_0>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_1>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_2>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_3>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_4>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_5>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_6>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};

template<>
struct BufferRawAllocator<buffer_location_t::device_ptx_pinned_7>
{
	static void* malloc(std::size_t size)
	{
		void* ptr = NULL;
		cudaError_t error = cudaMalloc(&ptr, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to allocate cuda device memory");
		return ptr;
	}

	static void free(void* ptr)
	{
		if(ptr)
		{
			cudaError_t error = cudaFree(ptr);
			if(error != cudaSuccess)
				BOOST_ASSERT(error == cudaSuccess && "failed to free cuda device memory");
		}
	}

	static void* realloc(void* ptr, std::size_t size)
	{
        UNUSED_ARGUMENT(ptr);
        UNUSED_ARGUMENT(size);
		BOOST_ASSERT(false && "not yet implemented");
		return NULL;
	}

	static void memset(void* ptr, int value, std::size_t size)
	{
		cudaError_t error = ::cudaMemset(ptr, value, size);
		if(error != cudaSuccess)
			BOOST_ASSERT(error == cudaSuccess && "failed to memset cuda device memory");
	}
};
#endif

byte* malloc_by_location(buffer_location_t::type location, std::size_t size);
void free_by_location(buffer_location_t::type location, byte* ptr);
void memset_by_location(buffer_location_t::type location, byte* ptr, int value, std::size_t size);

} } }

#endif /* ZILLIANS_FRAMEWORK_BUFFER_BUFFERALLOCATORS_H_ */
