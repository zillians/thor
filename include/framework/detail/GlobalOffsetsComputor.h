/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_DETAIL_GLOBAL_OFFSETS_COMPUTOR_H_
#define ZILLIANS_FRAMEWORK_DETAIL_GLOBAL_OFFSETS_COMPUTOR_H_

#include <cstdint>

#include <map>
#include <string>
#include <tuple>

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/set_of.hpp>

#include "utility/UUIDUtil.h"

#include "language/Architecture.h"

namespace zillians { namespace language { namespace tree {

class ClassDecl;
class FunctionDecl;
class Tangle;

} } }

namespace zillians { namespace framework { namespace detail {

using global_offsets_t = std::map<
    UUID,
    std::tuple<
        std::int64_t /* function */,
        std::int64_t /* symbol   */,
        std::int64_t /* type     */
    >
>;

using   func_mapping_t = boost::bimaps::bimap<boost::bimaps::set_of<std::int64_t>, boost::bimaps::multiset_of<const language::tree::FunctionDecl*>>;
using   type_mapping_t = boost::bimaps::bimap<boost::bimaps::set_of<std::int64_t>, boost::bimaps::multiset_of<const language::tree::ClassDecl*   >>;
using string_mapping_t = boost::bimaps::bimap<boost::bimaps::set_of<std::int64_t>, boost::bimaps::multiset_of<std::wstring                       >>;

std::tuple<
    global_offsets_t,
      func_mapping_t,
      type_mapping_t,
    string_mapping_t
> compute_global_offsets(const language::tree::Tangle& tangle, language::Architecture arch, bool enable_server, bool enable_client);

namespace {

constexpr int global_offsets_idx_function = 0;
constexpr int global_offsets_idx_symbol   = 1;
constexpr int global_offsets_idx_type     = 2;

}

} } }

#endif /* ZILLIANS_FRAMEWORK_DETAIL_GLOBAL_OFFSETS_COMPUTOR_H_ */
