/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_DETAIL_GLOBAL_DISPATCHER_GENERATOR_H_
#define ZILLIANS_FRAMEWORK_DETAIL_GLOBAL_DISPATCHER_GENERATOR_H_

#include <cstdint>

#include <string>

#include <boost/range/iterator_range.hpp>

#include <log4cxx/logger.h>

#include "language/Architecture.h"

#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace language { namespace tree {

struct FunctionDecl;
struct Tangle;

} } }

namespace zillians { namespace framework { namespace detail {

class global_dispatcher_generator
{
protected:
    using FunctionDecl = language::tree::FunctionDecl;
    using Tangle       = language::tree::Tangle;

public:
    global_dispatcher_generator(const log4cxx::LoggerPtr& new_logger, language::Architecture new_arch, const detail::global_offsets_t& new_global_offsets);

    bool generate(const Tangle& tangle, const detail::func_mapping_t& exported_funcs);

private:
    bool set_global_offsets();
    bool set_global_inits(const Tangle& tangle);
    bool set_exported_functions(const detail::func_mapping_t& exported_funcs);

protected:
    virtual bool load_skeleton() = 0;

    virtual bool set_global_offset(const std::string& name, std::int64_t offset) = 0;

    virtual bool add_global_init(const FunctionDecl& func_decl) = 0;
    virtual bool add_exported_function(boost::iterator_range<detail::func_mapping_t::right_const_iterator> exported_func_with_ids) = 0;

    virtual bool finish() = 0;

protected:
    language::Architecture          arch;
    const detail::global_offsets_t* global_offsets;
    log4cxx::LoggerPtr              logger;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_DETAIL_GLOBAL_DISPATCHER_GENERATOR_H_ */
