/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_NETWORK_LISTENER_H
#define ZILLIANS_NETWORK_LISTENER_H

#include <cstdint>

#include <functional>

#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>

namespace zillians { namespace network {

class session;

class listener : private boost::asio::ip::tcp::acceptor
{
private:
    using protocol_type   = boost::asio::ip::tcp;
    using base_type       = protocol_type::acceptor;
    using endpoint_type   = protocol_type::endpoint;

public:
    enum transport_type
    {
        TCP_V4, TCP_V6
    };

    using accept_callback_signature = void(listener*, const boost::system::error_code&);

public:
    listener(boost::asio::io_service& service);
    ~listener();

    boost::system::error_code close();

    boost::system::error_code listen(transport_type type, std::uint16_t port);
    void async_accept(network::session* session, std::function<accept_callback_signature> handler);

    void cancel_accept();
    network::session* steal_session();

private:
    void set_session(network::session *new_session);

    network::session* session;
};

} }

#endif
