/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_NETWORK_SESSION_FACTORY_H
#define ZILLIANS_NETWORK_SESSION_FACTORY_H

#include <cstddef>

#include <thread>
#include <vector>
#include <memory>
#include <set>

#include <boost/asio.hpp>

#include "network/Listener.h"
#include "network/Session.h"

namespace zillians { namespace network {

class session_factory : private boost::asio::io_service
{
private:
    using base_type = boost::asio::io_service;

public:
     session_factory(std::size_t workers_count = 1);
    ~session_factory();

    session* create_session();
    listener* create_listener();

private:
    std::set<std::unique_ptr<network::session> > sessions;
    std::set<std::unique_ptr<network::listener>> listeners;

    std::vector<std::thread> workers;
};

} } // namespace zillians::network

#endif
