/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Jun 1, 2011 sdk - Initial version created.
 */

#ifndef ZILLIANS_NETWORKING_SYS_TCPSESSIONCONTEXT_H_
#define ZILLIANS_NETWORKING_SYS_TCPSESSIONCONTEXT_H_

#include <boost/function.hpp>

#include "core/Buffer.h"
#include "core/SharedPtr.h"
#include "network/sys/tcp/TcpDispatcher.h"

namespace zillians { namespace network { namespace sys {

struct TcpSessionDispatcherContext
{
    using close_callback_t = boost::function<void()>;
    using error_callback_t = boost::function<void(const boost::system::error_code&)>;
    using dispatcher_t     = TcpDispatcher;

	shared_ptr<Buffer> buffer;
	bool dispatchEnabled;

	close_callback_t onClose;
	error_callback_t onError;
	dispatcher_t*     dispatcher;
};

} } }

#endif /* ZILLIANS_NETWORKING_SYS_TCPSESSIONCONTEXT_H_ */
