/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Jul 17, 2009 sdk - Initial version created.
 */

#ifndef ZILLIANS_NETWORKING_SYS_SESSIONENGINE_H_
#define ZILLIANS_NETWORKING_SYS_SESSIONENGINE_H_

#include <boost/system/error_code.hpp>

#include "network/sys/Session.h"

namespace zillians { namespace network { namespace sys {

struct SessionEngineBase
{
protected:
	virtual void beforeConnect(SessionBase* session, boost::system::error_code& error) = 0;
	virtual void afterConnect(SessionBase* session, boost::system::error_code& error) = 0;
	virtual void beforeAccept(SessionBase* session, boost::system::error_code& error) = 0;
	virtual void afterAccept(SessionBase* session, boost::system::error_code& error) = 0;
};

/**
 * SessionEngineT is a an empty class template for various protocol specialization
 */
template <typename Protocol>
class SessionEngineT;

} } }

#endif/* ZILLIANS_NETWORKING_SYS_SESSIONENGINE_H_ */
