/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Jul 17, 2009 sdk - Initial version created.
 */

#ifndef ZILLIANS_NETWORKING_SYS_PLACEHOLDERS_H_
#define ZILLIANS_NETWORKING_SYS_PLACEHOLDERS_H_

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <boost/bind.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/bind/arg.hpp>
#include <boost/detail/workaround.hpp>

namespace zillians { namespace network { namespace sys { namespace placeholders {

namespace detail {
	template <int Number>
	struct placeholder
	{
		static boost::arg<Number>& get()
		{
			static boost::arg<Number> result;
			return result;
		}
	};
}

#if defined(GENERATING_DOCUMENTATION)

unspecified error;

unspecified byte_transferred;

#elif defined(__BORLANDC__) || defined(__GNUC__)

inline boost::arg<1> error()
{
  return boost::arg<1>();
}

inline boost::arg<2> byte_transferred()
{
  return boost::arg<2>();
}

#else

#if BOOST_WORKAROUND(BOOST_MSVC, < 1400)

static boost::arg<1>& error = zillians::network::sys::placeholders::detail::placeholder<1>::get();
static boost::arg<2>& byte_transferred = zillians::network::sys::placeholders::detail::placeholder<2>::get();

#else

namespace
{
	boost::arg<1>& error = zillians::network::sys::placeholders::detail::placeholder<1>::get();
	boost::arg<2>& byte_transferred = zillians::network::sys::placeholders::detail::placeholder<2>::get();
} // namespace

#endif

#endif

namespace dispatch {

#if defined(GENERATING_DOCUMENTATION)

unspecified source_ref;

unspecified message_ref;

unspecified type;

unspecified buffer_ref;

unspecified size;

unspecified error;

#elif defined(__BORLANDC__) || defined(__GNUC__)

inline boost::arg<1> source_ref()
{
  return boost::arg<1>();
}


inline boost::arg<2> message_ref()
{
  return boost::arg<2>();
}


inline boost::arg<2> type()
{
  return boost::arg<2>();
}

inline boost::arg<3> buffer_ref()
{
  return boost::arg<3>();
}

inline boost::arg<4> size()
{
  return boost::arg<4>();
}


inline boost::arg<2> error()
{
  return boost::arg<2>();
}

#else

#if BOOST_WORKAROUND(BOOST_MSVC, < 1400)

static boost::arg<1>& source_ref = zillians::network::sys::placeholders::detail::placeholder<1>::get();

static boost::arg<2>& message_ref = zillians::network::sys::placeholders::detail::placeholder<2>::get();

static boost::arg<2>& type = zillians::network::sys::placeholders::detail::placeholder<2>::get();
static boost::arg<3>& buffer_ref = zillians::network::sys::placeholders::detail::placeholder<3>::get();
static boost::arg<3>& size = zillians::network::sys::placeholders::detail::placeholder<4>::get();

static boost::arg<2>& error = zillians::network::sys::placeholders::detail::placeholder<2>::get();

#else

namespace
{
	boost::arg<1>& source_ref = zillians::network::sys::placeholders::detail::placeholder<1>::get();

	boost::arg<2>& message_ref = zillians::network::sys::placeholders::detail::placeholder<2>::get();

	boost::arg<2>& type = zillians::network::sys::placeholders::detail::placeholder<2>::get();
	boost::arg<3>& buffer_ref = zillians::network::sys::placeholders::detail::placeholder<3>::get();
	boost::arg<4>& size = zillians::network::sys::placeholders::detail::placeholder<4>::get();

	boost::arg<2>& error = zillians::network::sys::placeholders::detail::placeholder<2>::get();
} // namespace

#endif

#endif

}

} } } }


#endif /* ZILLIANS_NETWORKING_SYS_PLACEHOLDERS_H_ */
