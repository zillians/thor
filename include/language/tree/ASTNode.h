/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ASTNODE_H_
#define ZILLIANS_LANGUAGE_TREE_ASTNODE_H_

#include <ostream>
#include <string>
#include <type_traits>
#include <unordered_set>
#include <utility>

#include <boost/assert.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/noncopyable.hpp>
#include <boost/preprocessor.hpp>
#include <boost/preprocessor/seq/enum.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/type_traits.hpp>
#include <boost/variant.hpp>

#include "core/ContextHub.h"
#include "core/Visitor.h"
#include "utility/Foreach.h"
#include "utility/Traits.h"
#include "language/Architecture.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/GarbageCollector.h"
#include "language/tree/RelinkablePtr.h"

namespace zillians { namespace language { namespace tree {

// forward declaration of ASTNode
struct ASTNode;

template<typename T>
using isNode = traits::is_same_or_base_of<ASTNode, T>;

typedef std::unordered_set<const ASTNode*> ASTNodeSet;

/**
 * Helper template function to implement static type checking system
 *
 * @param ptr the pointer to check with
 * @return true if the given ptr is an instance of Derived type; false otherwise
 */
namespace detail {

template<typename ...Types>
struct isa_impl;

template<typename Type, typename ...RemainTypes>
struct isa_impl<Type, RemainTypes...>
{
    static bool test(const ASTNode* ptr) noexcept;
    static bool test(const ASTNode& ptr) noexcept;
};

template<>
struct isa_impl<>
{
    static bool test(const ASTNode* ptr) noexcept { return false; }
    static bool test(const ASTNode& ptr) noexcept { return false; }
};

}

template<typename ...Types>
inline bool isa(const ASTNode* ptr) noexcept
{
    BOOST_ASSERT(ptr && "null pointer exception");

    return detail::isa_impl<Types...>::test(ptr);
}

template<typename ...Types>
inline bool isa(const ASTNode& ref) noexcept
{
    return detail::isa_impl<Types...>::test(ref);
}

template<typename ...Types, typename NodeType>
inline bool isa(const relinkable_ptr<NodeType>& relinkable) noexcept
{
    BOOST_ASSERT(relinkable.is_ptr() && "relinkable should be pointer in normal cases");

    auto*const ptr = relinkable.get_ptr();

    return isa<Types...>(ptr);
}

template<typename Derived, typename Base>
inline typename std::conditional<
    std::is_const<Base>::value,
    typename std::add_pointer<typename std::add_const<Derived>::type>::type,
    typename std::add_pointer<                        Derived       >::type
>::type cast(Base* ptr) noexcept
{
    typedef typename std::conditional<
        std::is_const<Base>::value,
        typename std::add_pointer<typename std::add_const<Derived>::type>::type,
        typename std::add_pointer<                        Derived       >::type
    >::type Target;

    if(isa<Derived>(ptr))
        return static_cast<Target>(ptr);
    else
        return nullptr;
}

template<typename Derived, typename Base>
inline decltype(cast<Derived, Base>(nullptr)) cast(const relinkable_ptr<Base>& relinkable) noexcept
{
    BOOST_ASSERT(relinkable.is_ptr() && "relinkable should be pointer in normal cases");

    auto*const ptr = relinkable.get_ptr();

    return cast<Derived, Base>(ptr);
}

template<typename Derived, typename Base>
inline decltype(cast<Derived, Base>(nullptr)) cast_or_null(Base* ptr) noexcept
{
    if (ptr == nullptr)
        return nullptr;
    else
        return cast<Derived, Base>(ptr);
}

template<typename Derived, typename Base>
inline decltype(cast_or_null<Derived, Base>(nullptr)) cast_or_null(const relinkable_ptr<Base>& relinkable) noexcept
{
    BOOST_ASSERT(relinkable.is_ptr() && "relinkable should be pointer in normal cases");

    auto*const ptr = relinkable.get_ptr();

    return cast_or_null<Derived, Base>(ptr);
}

template<typename NodeType>
inline NodeType* clone_or_null(const NodeType* node)
{
    if (node == nullptr)
        return nullptr;
    else
        return node->clone();
}

struct ASTNodeType
{
    enum type
    {
        BOOST_PP_SEQ_ENUM(THOR_SCRIPT_AST_NODE_NAMES)
    };
};

struct ASTNode : public VisitableBase<ASTNode>, ContextHub<ContextOwnership::transfer>, boost::noncopyable
{
    DEFINE_VISITABLE();

    // note, the stype is needless to be inlined defined.
    // but clang++ can not compile, and shows a 'coflicting types' error message
    // so, the inline here is a work around for clang++
    static constexpr int stype()
    {
        return ASTNodeType::ASTNode;
    }

    virtual const char* instanceName(        ) const noexcept = 0;
    virtual int         rtype       (        ) const noexcept = 0; // real type
    virtual bool        checkType   (int type) const noexcept = 0;

public:
    /**
     * Compare if two AST tree are equal
     *
     * @param rhs another AST to be compared with
     * @return true if equal, false otherwise
     */
    bool isEqual(const ASTNode& rhs) const;

    /**
     * The actual compare implementation for each derived classes
     *
     * Note that this method is for internal use. DO NOT call it directly
     *
     * @param rhs the right-hand-side of the comparison
     * @param visited the visited node set to avoid redundant visitation
     * @return true if equal, false otherwise
     */
    virtual bool isEqualImpl(const ASTNode& rhs) const = 0;

protected:
    template<typename VariantType>
    struct IsEqualVisitor;

    template<typename VariantType>
    friend struct IsEqualVisitor;

    template<typename T0, typename T1>
    static bool isEqualEval(const std::pair<T0, T1>& lhs, const std::pair<T0, T1>& rhs);

    template<typename ...Ts>
    static bool isEqualEval(const boost::tuple<Ts...>& lhs, const boost::tuple<Ts...>& rhs);

    template<typename ...Ts>
    static bool isEqualEval(const boost::variant<Ts...>& lhs, const boost::variant<Ts...>& rhs);

    template<typename T, typename std::enable_if<std::is_pointer<T>::value>::type * = nullptr>
    static bool isEqualEval(const T& lhs, const T& rhs);

    template<typename T, typename std::enable_if<traits::has_begin_end<T>::value>::type * = nullptr>
    static bool isEqualEval(const T& lhs, const T& rhs);

    template<typename T, typename std::enable_if<isNode<T>::value>::type * = nullptr>
    static bool isEqualEval(const T& lhs, const T& rhs);

    template<typename T, typename std::enable_if<!std::is_pointer<T>::value && !traits::has_begin_end<T>::value && !isNode<T>::value>::type * = nullptr>
    static bool isEqualEval(const T& lhs, const T& rhs);

    static bool isEqualEvalTuple(const boost::tuples::null_type& lhs, const boost::tuples::null_type& rhs);

    template<typename HeadType, typename TailType>
    static bool isEqualEvalTuple(const boost::tuples::cons<HeadType, TailType>& lhs, const boost::tuples::cons<HeadType, TailType>& rhs);

public:
    template<typename T>
    bool hasOwner() const
    {
        return getOwner<T>();
    }

    template<typename ...T>
    ASTNode* getAnyOwner() const
    {
        for (ASTNode* p = parent; p != nullptr; p = p->parent)
            if (isa<T...>(p))
                return p;

        return nullptr;
    }

    template<typename T>
    T* getOwner() const
    {
        return cast_or_null<T>(getAnyOwner<T>());
    }

    template<typename ...T>
    ASTNode* findFromThisToParents() const
    {
        if (isa<T...>(this))
            return const_cast<ASTNode*>(this);

        return getAnyOwner<T...>();
    }

public:
    /**
     * Replace direct children pointer with the given one
     *
     * @param from the current child node to replace
     * @param to the new child node for replacement
     * @param update_parent update the parent pointer of child nodes
     * @return true if the replacement is completed, false otherwise
     */
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) = 0;

protected:
    struct ReplaceUseWithVisitor;

    friend struct ReplaceUseWithVisitor;

    template<typename T0, typename T1>
    static bool replaceUseWithEval(std::pair<T0, T1>& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename ...Ts>
    static bool replaceUseWithEval(boost::tuple<Ts...>& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename ...Ts>
    static bool replaceUseWithEval(boost::variant<Ts...>& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename T>
    static bool replaceUseWithEval(T*& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename T, typename std::enable_if<traits::has_begin_end<T>::value>::type * = nullptr>
    static bool replaceUseWithEval(T& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename T, typename std::enable_if<isNode<T>::value>::type * = nullptr>
    static bool replaceUseWithEval(T& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename T, typename std::enable_if<!traits::has_begin_end<T>::value && !isNode<T>::value>::type * = nullptr>
    static bool replaceUseWithEval(T& t, const ASTNode& from, const ASTNode& to, bool update_parent);

    static bool replaceUseWithEvalTuple(boost::tuples::null_type t, const ASTNode& from, const ASTNode& to, bool update_parent);

    template<typename HeadType, typename TailType>
    static bool replaceUseWithEvalTuple(boost::tuples::cons<HeadType, TailType>& t, const ASTNode& from, const ASTNode& to, bool update_parent);

public:
    /**
     * Clone the ASTNode and all of its descendants
     *
     * @return the cloned ASTNode
     */
    virtual ASTNode* clone() const = 0;

            bool           toSource(const boost::filesystem::path& file_path) const;
            std::wstring   toSource(unsigned indent = 0) const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const = 0;

public:
    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & parent;
    }

protected:
    ASTNode() : parent(NULL)
    {
        GarbageCollector<const ASTNode>::instance()->add(this);
    }

public:
    virtual ~ASTNode() = 0;

public:
    ASTNode* parent;
};

struct output_indent_t
{
    unsigned value;
};

struct output_arch_t
{
    output_indent_t indent;
    Architecture    arch;
};

struct output_source_t
{
    const ASTNode* node;
    unsigned       indent;
};

output_indent_t out_indent(const unsigned indent);
output_arch_t   out_arch(const unsigned indent, const Architecture arch);
output_source_t out_source(const ASTNode& node, const unsigned indent);

std::wostream& operator<<(std::wostream& output, output_indent_t indent);
std::wostream& operator<<(std::wostream& output, output_arch_t   arch  );
std::wostream& operator<<(std::wostream& output, output_source_t source);

#define AST_NODE_HIERARCHY(self, base)                                                                                                 \
    DEFINE_VISITABLE();                                                                                                                \
    typedef base BaseNode;                                                                                                             \
    static constexpr int stype       (        )                         { return ASTNodeType::self;                                  } \
    virtual const char*  instanceName(        ) const noexcept override { return #self;                                              } \
    virtual int          rtype       (        ) const noexcept          { return self::stype();                                      } \
    virtual bool         checkType   (int type) const noexcept override { return self::stype() == type || BaseNode::checkType(type); }

#define AST_NODE_IS_EQUAL_IMPL(r, _, member)                        \
    if (!isEqualEval(member, rhs_node->member)) return false;

#define AST_NODE_IS_EQUAL(members)                                                                 \
    typedef typename std::add_const<                                                               \
        typename std::remove_pointer<                                                              \
            decltype(this)                                                                         \
        >::type                                                                                    \
    >::type ConstSelfNode;                                                                         \
    BOOST_ASSERT(isa<ConstSelfNode>(&rhs) && "mismatched type should be handled in other place");  \
    if (!BaseNode::isEqualImpl(rhs)) return false;                                                 \
    const auto *rhs_node = cast<   ConstSelfNode>(&rhs);                                           \
    BOOST_PP_SEQ_FOR_EACH(AST_NODE_IS_EQUAL_IMPL, _, members)                                      \
    return true

#define AST_NODE_REPLACE_IMPL(r, var_name, member)                   \
    var_name |= replaceUseWithEval(member, from, to, update_parent);

#define AST_NODE_REPLACE(var_name, members)                         \
    bool var_name = false;                                          \
    var_name |= BaseNode::replaceUseWith(from, to, update_parent);  \
    BOOST_PP_SEQ_FOR_EACH(AST_NODE_REPLACE_IMPL, var_name, members) \
    return var_name

#define AST_NODE_SERIALIZE_IMPL(r, archive, member)                   \
    archive & member;

#define AST_NODE_SERIALIZE(archive, members)                          \
    archive & boost::serialization::base_object<BaseNode>(*this);     \
    BOOST_PP_SEQ_FOR_EACH(AST_NODE_SERIALIZE_IMPL, archive, members)

typedef GarbageCollector<const ASTNode> ASTNodeGC;

} } }

#include "language/tree/ASTNode.ipp"

// export serialization declaration
// see http://www.boost.org/doc/libs/1_47_0/libs/serialization/doc/special.html#export
#include <boost/serialization/export.hpp>

#define AST_NODE_SERIALIZE_EXPORT(r, _, node_name)   \
    BOOST_CLASS_EXPORT_KEY2(                         \
        zillians::language::tree:: node_name,        \
        BOOST_PP_STRINGIZE(node_name)                \
    )

BOOST_PP_SEQ_FOR_EACH(AST_NODE_SERIALIZE_EXPORT, _, THOR_SCRIPT_AST_NODE_NAMES)

#undef AST_NODE_SERIALIZE_EXPORT

#endif /* ZILLIANS_LANGUAGE_TREE_ASTNODE_H_ */

