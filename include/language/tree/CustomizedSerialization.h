/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_CUSTOMIZEDSERIALIZATION_H_
#define ZILLIANS_LANGUAGE_TREE_CUSTOMIZEDSERIALIZATION_H_

#include "utility/UUIDUtil.h"

#include <string>
#include <type_traits>

#include <boost/blank_fwd.hpp>
#include <boost/serialization/deque.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/variant.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_serialize.hpp>
#include <boost/filesystem/path.hpp>

namespace zillians { namespace language { namespace tree {

template<int index, class Archive, typename... Types>
void serialize_impl(Archive& ar, boost::tuple<Types...>& t, const unsigned int /*version*/, typename std::enable_if<index >= boost::tuples::length<boost::tuple<Types...>>::value>::type * = nullptr)
{
}

template<int index, class Archive, typename... Types>
void serialize_impl(Archive& ar, boost::tuple<Types...>& t, const unsigned int   version  , typename std::enable_if<index <  boost::tuples::length<boost::tuple<Types...>>::value>::type * = nullptr)
{
    {
        const auto& name = "element-" + std::to_string(index);

        ar & boost::serialization::make_nvp(name.c_str(), boost::get<index>(t));
    }

    serialize_impl<index + 1>(ar, t, version);
}

} } }  // namespace zillians::language::tree

namespace boost { namespace serialization {

template<class Archive>
void serialize(Archive& ar, boost::blank& t, const unsigned int version)
{
}

template<class Archive, typename... Types>
void serialize(Archive& ar, boost::tuple<Types...>& t, const unsigned int version)
{
    zillians::language::tree::serialize_impl<0>(ar, t, version);
}

template<class Archive>
void serialize(Archive& ar, zillians::UUID& t, const unsigned int)
{
    boost::uuids::uuid& uuid_data = t;

    static_assert(sizeof(t) == sizeof(uuid_data), "you need customize serialization after add things into zillians::UUID");

    ar & BOOST_SERIALIZATION_NVP(uuid_data);
}

template<class Archive>
inline void save(
    Archive& ar,
    const boost::filesystem::path& path,
    const unsigned int /* file_version */
){
    ar & path.string();
}

template<class Archive>
inline void load(
    Archive& ar,
    boost::filesystem::path& path,
    const unsigned int /*file_version*/
){

    std::string content;
    ar & content;

    path = content;
}

} } // namespace boost::serialization

BOOST_SERIALIZATION_SPLIT_FREE(boost::filesystem::path)

#endif /* ZILLIANS_LANGUAGE_TREE_CUSTOMIZEDSERIALIZATION_H_ */
