/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTDISCOVER_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTDISCOVER_H_

#include <functional>
#include <type_traits>
#include <utility>

#include <boost/utility/result_of.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/visitor/RecursiveASTVisitor.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

namespace detail {

template<typename NodeType>
class RecursiveASTDiscover : private RecursiveASTVisitor<RecursiveASTDiscover<NodeType>>
{
private:
    using VisitorBase = RecursiveASTVisitor<RecursiveASTDiscover<NodeType>>;
    friend VisitorBase;

public:
    template<typename ActorType>
    explicit RecursiveASTDiscover(ActorType&& actor)
        : actor(std::forward<ActorType>(actor))
    {
    }

    bool discover(ASTNode& node)
    {
        if (auto*const casted_node = cast<NodeType>(&node))
            if (!actor(*casted_node))
                return false;

        return VisitorBase::traverse(node);
    }

private:
    bool traverse(ASTNode& node)
    {
        return discover(node);
    }

private:
    std::function<bool(NodeType&)> actor;
};

}

template<typename NodeType, typename ActorType>
inline bool discoverASTNodes(ASTNode& node, ActorType&& actor)
{
    detail::RecursiveASTDiscover<NodeType> discover(std::forward<ActorType>(actor));

    return discover.discover(node);
}

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTDISCOVER_H_ */
