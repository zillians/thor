/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTVISITOR_H_

#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

/**
 * This visitor split tree traversal into three actions
 * 1. Traversal (  traverseXXX)
 * 2. Walk-up   (walkUpFromXXX)
 * 3. Visit     (     visitXXX)
 *
 * Traversal:
 *   - Define visitation order of child nodes
 *   - Call Walk-up
 *
 * Walk-up:
 *   - Reverse the Visit call from base to derived
 *
 * Visit:
 *   - Do the actual work for specific type
 *
 * Let see the traversal flow of ClassDecl instance.
 *  1. Call @code visitor.traverse(class_decl) @endcode
 *  2. Call @code visitor.traverseClassDecl(class_decl) @endcode
 *  3. Traverse children with order defined by @code traverseClassDecl @endcode
 *  4. @code visitor.walkUpFromClassDecl(class_decl) @endcode
 *  5. @code visitor.walkUpFromDeclaration(class_decl) @endcode
 *  6. @code visitor.walkUpFromAnnotatable(class_decl) @endcode
 *  7. @code visitor.walkUpFromASTNode(class_decl) @endcode
 *  8. @code visitor.visitASTNode(class_decl) @endcode
 *  9. @code visitor.visitAnnotatable(class_decl) @endcode
 * 10. @code visitor.visitDeclaration(class_decl) @endcode
 * 11. @code visitor.visitClassDecl(class_decl) @endcode
 *
 * There are several things shown by the previous steps
 * 1. Children are visited first.
 * 2. You can do some general things on @b visitXXX. For example, counting nodes by replacing @b visitASTNode.
 * 3. Most actions should be done in @b visitXXX. If you're replacing @b travereXXX or @b walkUpFromXXX, make sure you're not @b drunk.
 *
 * How to override the traverseXXX/walkUpFromXXX/visitXXX ?
 *   - Just define the same name in derived class, then call the one in base to chain the call sequence.
 *   - Return @b false will terminate the whole traversel.
 */
template<typename Derived>
struct RecursiveASTVisitor
{
    bool traverse(ASTNode& node);

protected:
#define TRAVERSER_DECL_abstract(name)
#define TRAVERSER_DECL_concrete(name)                               \
    bool BOOST_PP_CAT(traverse, name)(name& node);

#define TRAVERSER_DECL(r, _, info)                                  \
    BOOST_PP_CAT(TRAVERSER_DECL_, BOOST_PP_SEQ_ELEM(2, info))(      \
        BOOST_PP_SEQ_ELEM(0, info)                                  \
    )

BOOST_PP_SEQ_FOR_EACH(TRAVERSER_DECL, _, THOR_SCRIPT_AST_NODE_DEFS)

#undef  TRAVERSER_DECL
#undef  TRAVERSER_DECL_concrete
#undef  TRAVERSER_DECL_abstract

#define WALKUP_VISIT_DECL_IMPL(name)                                                  \
                     bool BOOST_PP_CAT(walkUpFrom, name)(name& node);                 \
    static constexpr bool BOOST_PP_CAT(visit     , name)(name& node) { return true; }

#define WALKUP_VISIT_DECL(r, _, info)                  \
    WALKUP_VISIT_DECL_IMPL(BOOST_PP_SEQ_ELEM(0, info))

BOOST_PP_SEQ_FOR_EACH(WALKUP_VISIT_DECL, _, THOR_SCRIPT_AST_NODE_DEFS)

#undef  WALKUP_VISIT_DECL
#undef  WALKUP_VISIT_DECL_IMPL

protected:
          Derived& getDerived()       noexcept { return static_cast<      Derived&>(*this); }
    const Derived& getDerived() const noexcept { return static_cast<const Derived&>(*this); }
};

} } } }

#include "language/tree/visitor/RecursiveASTVisitor.ipp"

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTVISITOR_H_ */
