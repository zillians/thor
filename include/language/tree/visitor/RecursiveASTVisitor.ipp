/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTVISITOR_IPP_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTVISITOR_IPP_

#include <boost/assert.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/comparison/greater.hpp>
#include <boost/preprocessor/control/iif.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/stringize.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/ASTNodeFactory.h"

#include "language/tree/visitor/RecursiveASTVisitor.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverse(ASTNode& node)
{
#define TRAVERSE_IMPL_abstract(name)                          \
    static_assert(                                            \
        std::is_abstract<name>::value,                        \
        "" BOOST_PP_STRINGIZE(name) " is not a abstract type" \
        ", you may need to modify the forward info in "       \
        "ASTNodeFwd.h"                                        \
    );

#define TRAVERSE_IMPL_concrete(name)                          \
    static_assert(                                            \
        !std::is_abstract<name>::value,                       \
        "" BOOST_PP_STRINGIZE(name) " is not a complete type" \
        ", you may need to modify the forward info in "       \
        "ASTNodeFwd.h"                                        \
    );                                                        \
    case ASTNodeType::name:                                   \
        return getDerived().BOOST_PP_CAT(                     \
            traverse,                                         \
            name                                              \
        )(static_cast<name&>(node));

#define TRAVERSE_IMPL(r, _, info)                             \
    BOOST_PP_CAT(TRAVERSE_IMPL_, BOOST_PP_SEQ_ELEM(2, info))( \
        BOOST_PP_SEQ_ELEM(0, info)                            \
    )

    switch (node.rtype())
    {
        BOOST_PP_SEQ_FOR_EACH(
            TRAVERSE_IMPL,
            _,
            THOR_SCRIPT_AST_NODE_DEFS
        )
    }

#undef  TRAVERSE_IMPL
#undef  TRAVERSE_IMPL_concrete
#undef  TRAVERSE_IMPL_abstract

    BOOST_ASSERT(false && "probably missing forward info in ASTNodwFwd.h");

    return true;
}

#define WALKUP_DEF_IMPL_BODY_3(info)                                                     \
    return  getDerived().BOOST_PP_CAT(visit     , BOOST_PP_SEQ_ELEM(0, info))(node)

#define WALKUP_DEF_IMPL_BODY_4(info)                                                     \
    return  getDerived().BOOST_PP_CAT(walkUpFrom, BOOST_PP_SEQ_ELEM(3, info))(node) &&   \
            getDerived().BOOST_PP_CAT(visit     , BOOST_PP_SEQ_ELEM(0, info))(node)

#define WALKUP_DEF(r, _, info)                                                           \
    template<typename Derived>                                                           \
    inline bool RecursiveASTVisitor<Derived>::BOOST_PP_CAT(                              \
        walkUpFrom, BOOST_PP_SEQ_ELEM(0, info)                                           \
    )(BOOST_PP_SEQ_ELEM(0, info)& node)                                                  \
    {                                                                                    \
        BOOST_PP_CAT(WALKUP_DEF_IMPL_BODY_, BOOST_PP_SEQ_SIZE(info))(info);              \
    }

BOOST_PP_SEQ_FOR_EACH(WALKUP_DEF, _, THOR_SCRIPT_AST_NODE_DEFS)

#undef  WALKUP_DEF
#undef  WALKUP_DEF_IMPL_BODY_4
#undef  WALKUP_DEF_IMPL_BODY_3

#define TRY_TRAVERSE_CHILD(expr)                       \
    if (auto*const child = (expr))                     \
        if (!getDerived().traverse(*child))            \
            return false

#define TRY_TRAVERSE_CHILD_SEQ(seq_expr)               \
    for (auto*const element : (seq_expr))              \
        TRY_TRAVERSE_CHILD(element)

#define TRY_TRAVERSE_CHILD_MAP(map_expr)               \
    for (const auto& entry : (map_expr))               \
    {                                                  \
        TRY_TRAVERSE_CHILD(entry.first );              \
        TRY_TRAVERSE_CHILD(entry.second);              \
    }

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseConfig(Config& node)
{
    return getDerived().walkUpFromConfig(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseInternal(Internal& node)
{
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::VOID_TYPE   ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::BOOL_TYPE   ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT8_TYPE   ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT16_TYPE  ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT32_TYPE  ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT64_TYPE  ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT32_TYPE));
    TRY_TRAVERSE_CHILD(node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT64_TYPE));

    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::VOID_TYPE   ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::BOOL_TYPE   ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::INT8_TYPE   ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::INT16_TYPE  ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::INT32_TYPE  ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::INT64_TYPE  ));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::FLOAT32_TYPE));
    TRY_TRAVERSE_CHILD(node.getPrimitiveType(PrimitiveKind::FLOAT64_TYPE));

    TRY_TRAVERSE_CHILD_SEQ(node.type_set);

    return getDerived().walkUpFromInternal(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTangle(Tangle& node)
{
    TRY_TRAVERSE_CHILD(node.config  );
    TRY_TRAVERSE_CHILD(node.internal);
    TRY_TRAVERSE_CHILD(node.root    );

    return getDerived().walkUpFromTangle(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseSource(Source& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.imports );
    TRY_TRAVERSE_CHILD_SEQ(node.declares);

    return getDerived().walkUpFromSource(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traversePackage(Package& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.id              );

    TRY_TRAVERSE_CHILD_SEQ(node.children);
    TRY_TRAVERSE_CHILD_SEQ(node.sources );

    return getDerived().walkUpFromPackage(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseImport(Import& node)
{
    TRY_TRAVERSE_CHILD(node.ns);

    return getDerived().walkUpFromImport(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseAnnotation(Annotation& node)
{
    TRY_TRAVERSE_CHILD(node.name);

    TRY_TRAVERSE_CHILD_MAP(node.attribute_list);

    return getDerived().walkUpFromAnnotation(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseAnnotations(Annotations& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.annotation_list);

    return getDerived().walkUpFromAnnotations(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseNormalBlock(NormalBlock& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.objects);

    return getDerived().walkUpFromNormalBlock(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseAsyncBlock(AsyncBlock& node)
{
    TRY_TRAVERSE_CHILD(node.getTarget());
    TRY_TRAVERSE_CHILD(node.getBlock ());
    TRY_TRAVERSE_CHILD(node.getGrid  ());

    TRY_TRAVERSE_CHILD_SEQ(node.objects);

    return getDerived().walkUpFromAsyncBlock(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseAtomicBlock(AtomicBlock& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.objects);

    return getDerived().walkUpFromAtomicBlock(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseFlowBlock(FlowBlock& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.objects);

    return getDerived().walkUpFromFlowBlock(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseLockBlock(LockBlock& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.objects);

    return getDerived().walkUpFromLockBlock(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traversePipelineBlock(PipelineBlock& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.objects);

    return getDerived().walkUpFromPipelineBlock(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseSimpleIdentifier(SimpleIdentifier& node)
{
    return getDerived().walkUpFromSimpleIdentifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseNestedIdentifier(NestedIdentifier& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.identifier_list);

    return getDerived().walkUpFromNestedIdentifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTemplatedIdentifier(TemplatedIdentifier& node)
{
    TRY_TRAVERSE_CHILD(node.id);

    TRY_TRAVERSE_CHILD_SEQ(node.templated_type_list);

    return getDerived().walkUpFromTemplatedIdentifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseNumericLiteral(NumericLiteral& node)
{
    return getDerived().walkUpFromNumericLiteral(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseStringLiteral(StringLiteral& node)
{
    return getDerived().walkUpFromStringLiteral(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseObjectLiteral(ObjectLiteral& node)
{
    return getDerived().walkUpFromObjectLiteral(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTypeIdLiteral(TypeIdLiteral& node)
{
    return getDerived().walkUpFromTypeIdLiteral(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseSymbolIdLiteral(SymbolIdLiteral& node)
{
    return getDerived().walkUpFromSymbolIdLiteral(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseFunctionIdLiteral(FunctionIdLiteral& node)
{
    return getDerived().walkUpFromFunctionIdLiteral(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseFunctionSpecifier(FunctionSpecifier& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.getParameterTypes());

    TRY_TRAVERSE_CHILD(node.getReturnType());

    return getDerived().walkUpFromFunctionSpecifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseMultiSpecifier(MultiSpecifier& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.getTypes());

    return getDerived().walkUpFromMultiSpecifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseNamedSpecifier(NamedSpecifier& node)
{
    TRY_TRAVERSE_CHILD(node.getName());

    return getDerived().walkUpFromNamedSpecifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traversePrimitiveSpecifier(PrimitiveSpecifier& node)
{
    return getDerived().walkUpFromPrimitiveSpecifier(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traversePointerType(PointerType& node)
{
    return getDerived().walkUpFromPointerType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseReferenceType(ReferenceType& node)
{
    return getDerived().walkUpFromReferenceType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseMultiType(MultiType& node)
{
    return getDerived().walkUpFromMultiType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traversePrimitiveType(PrimitiveType& node)
{
    return getDerived().walkUpFromPrimitiveType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseRecordType(RecordType& node)
{
    return getDerived().walkUpFromRecordType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseEnumType(EnumType& node)
{
    return getDerived().walkUpFromEnumType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTypenameType(TypenameType& node)
{
    return getDerived().walkUpFromTypenameType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTypedefType(TypedefType& node)
{
    return getDerived().walkUpFromTypedefType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseFunctionType(FunctionType& node)
{
    return getDerived().walkUpFromFunctionType(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseClassDecl(ClassDecl& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.name            );
    TRY_TRAVERSE_CHILD(node.base            );
    TRY_TRAVERSE_CHILD(node.getType()       );

    TRY_TRAVERSE_CHILD_SEQ(node.implements      );
    TRY_TRAVERSE_CHILD_SEQ(node.member_variables);
    TRY_TRAVERSE_CHILD_SEQ(node.member_functions);

    return getDerived().walkUpFromClassDecl(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseEnumDecl(EnumDecl& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.name            );
    TRY_TRAVERSE_CHILD(node.getType()       );

    TRY_TRAVERSE_CHILD_SEQ(node.values);

    return getDerived().walkUpFromEnumDecl(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTypedefDecl(TypedefDecl& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.name            );
    TRY_TRAVERSE_CHILD(node.getType()       );
    TRY_TRAVERSE_CHILD(node.type            );

    return getDerived().walkUpFromTypedefDecl(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseFunctionDecl(FunctionDecl& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.name            );

    TRY_TRAVERSE_CHILD_SEQ(node.parameters);

    TRY_TRAVERSE_CHILD(node.type );
    TRY_TRAVERSE_CHILD(node.block);

    return getDerived().walkUpFromFunctionDecl(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseVariableDecl(VariableDecl& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.name            );
    TRY_TRAVERSE_CHILD(node.initializer     );
    TRY_TRAVERSE_CHILD(node.type            );

    return getDerived().walkUpFromVariableDecl(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTypenameDecl(TypenameDecl& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.name            );
    TRY_TRAVERSE_CHILD(node.getType()       );
    TRY_TRAVERSE_CHILD(node.specialized_type);
    TRY_TRAVERSE_CHILD(node.default_type    );

    return getDerived().walkUpFromTypenameDecl(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseDeclarativeStmt(DeclarativeStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.declaration     );

    return getDerived().walkUpFromDeclarativeStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseExpressionStmt(ExpressionStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.expr            );

    return getDerived().walkUpFromExpressionStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseForStmt(ForStmt& node)
{
    // FIXME traverse 'block' at the end
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.init            );
    TRY_TRAVERSE_CHILD(node.cond            );
    TRY_TRAVERSE_CHILD(node.block           );
    TRY_TRAVERSE_CHILD(node.step            );

    return getDerived().walkUpFromForStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseForeachStmt(ForeachStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.var_decl        );
    TRY_TRAVERSE_CHILD(node.range           );
    TRY_TRAVERSE_CHILD(node.block           );

    return getDerived().walkUpFromForeachStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseWhileStmt(WhileStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.cond            );
    TRY_TRAVERSE_CHILD(node.block           );

    return getDerived().walkUpFromWhileStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseSelection(Selection& node)
{
    TRY_TRAVERSE_CHILD(node.cond );
    TRY_TRAVERSE_CHILD(node.block);

    return getDerived().walkUpFromSelection(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseIfElseStmt(IfElseStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.if_branch       );

    TRY_TRAVERSE_CHILD_SEQ(node.elseif_branches);

    TRY_TRAVERSE_CHILD(node.else_block);

    return getDerived().walkUpFromIfElseStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseSwitchStmt(SwitchStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.node            );

    TRY_TRAVERSE_CHILD_SEQ(node.cases);

    TRY_TRAVERSE_CHILD(node.default_block);

    return getDerived().walkUpFromSwitchStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseBranchStmt(BranchStmt& node)
{
    TRY_TRAVERSE_CHILD(node.getAnnotations());
    TRY_TRAVERSE_CHILD(node.result          );

    return getDerived().walkUpFromBranchStmt(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseArrayExpr(ArrayExpr& node)
{
    TRY_TRAVERSE_CHILD(node.array_type  );
    TRY_TRAVERSE_CHILD(node.element_type);

    TRY_TRAVERSE_CHILD_SEQ(node.elements);

    return getDerived().walkUpFromArrayExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseBinaryExpr(BinaryExpr& node)
{
    if (node.isRighAssociative())
    {
        TRY_TRAVERSE_CHILD(node.right);
        TRY_TRAVERSE_CHILD(node.left );
    }
    else
    {
        TRY_TRAVERSE_CHILD(node.left );
        TRY_TRAVERSE_CHILD(node.right);
    }

    return getDerived().walkUpFromBinaryExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseBlockExpr(BlockExpr& node)
{
    TRY_TRAVERSE_CHILD(node.block);

    return getDerived().walkUpFromBlockExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseCallExpr(CallExpr& node)
{
    TRY_TRAVERSE_CHILD(node.node);

    TRY_TRAVERSE_CHILD_SEQ(node.parameters);

    return getDerived().walkUpFromCallExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseCastExpr(CastExpr& node)
{
    TRY_TRAVERSE_CHILD(node.node);
    TRY_TRAVERSE_CHILD(node.type);

    return getDerived().walkUpFromCastExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseIdExpr(IdExpr& node)
{
    TRY_TRAVERSE_CHILD(node.getId());

    return getDerived().walkUpFromIdExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseIndexExpr(IndexExpr& node)
{
    TRY_TRAVERSE_CHILD(node.array);
    TRY_TRAVERSE_CHILD(node.index);

    return getDerived().walkUpFromIndexExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseIsaExpr(IsaExpr& node)
{
    TRY_TRAVERSE_CHILD(node.node);
    TRY_TRAVERSE_CHILD(node.type);

    return getDerived().walkUpFromIsaExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseLambdaExpr(LambdaExpr& node)
{
    TRY_TRAVERSE_CHILD(node.getLambda());

    return getDerived().walkUpFromLambdaExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseMemberExpr(MemberExpr& node)
{
    TRY_TRAVERSE_CHILD(node.node  );
    TRY_TRAVERSE_CHILD(node.member);

    return getDerived().walkUpFromMemberExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseStringizeExpr(StringizeExpr& node)
{
    TRY_TRAVERSE_CHILD(node.node);

    return getDerived().walkUpFromStringizeExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseSystemCallExpr(SystemCallExpr& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.parameters);

    return getDerived().walkUpFromSystemCallExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTernaryExpr(TernaryExpr& node)
{
    TRY_TRAVERSE_CHILD(node.cond      );
    TRY_TRAVERSE_CHILD(node.true_node );
    TRY_TRAVERSE_CHILD(node.false_node);

    return getDerived().walkUpFromTernaryExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseTieExpr(TieExpr& node)
{
    TRY_TRAVERSE_CHILD_SEQ(node.tied_expressions);

    return getDerived().walkUpFromTieExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseUnaryExpr(UnaryExpr& node)
{
    TRY_TRAVERSE_CHILD(node.node);

    return getDerived().walkUpFromUnaryExpr(node);
}

template<typename Derived>
inline bool RecursiveASTVisitor<Derived>::traverseUnpackExpr(UnpackExpr& node)
{
    TRY_TRAVERSE_CHILD(node.call);

    TRY_TRAVERSE_CHILD_SEQ(node.unpack_list);

    return getDerived().walkUpFromUnpackExpr(node);
}

#undef  TRY_TRAVERSE_CHILD_MAP
#undef  TRY_TRAVERSE_CHILD_SEQ
#undef  TRY_TRAVERSE_CHILD

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_RECURSIVEASTVISITOR_IPP_ */
