/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_ASTGRAPHVIZGENERATOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_ASTGRAPHVIZGENERATOR_H_

#include <iostream>

#include "core/Visitor.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"


namespace zillians { namespace language { namespace stage { namespace visitor {

struct ASTGraphvizNodeGenerator : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(mInvoker, label);

    ASTGraphvizNodeGenerator(std::wostream& os) : stream(os)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mInvoker);
    }

    ~ASTGraphvizNodeGenerator()
    { }

    void label(tree::ASTNode& node);
    void label(tree::Identifier& node);
    void label(tree::UnaryExpr& node);
    void label(tree::BinaryExpr& node);
    void label(tree::Literal& node);
    void label(tree::Type& node);
    void label(tree::FunctionType& node);
    void label(tree::TemplatedIdentifier& node);
    void label(tree::Import& node);
    void label(tree::Package& node);
    void label(tree::Block& node);
    void label(tree::BlockExpr& node);
    void label(tree::IsaExpr& node);
    void label(tree::CastExpr& node);
    void label(tree::TypeSpecifier& node);
    void label(tree::MultiSpecifier& node);
    void label(tree::Declaration& node);
    void label(tree::ClassDecl& node);
    void label(tree::FunctionDecl& node);

    void addNode(tree::ASTNode& node,
                 std::wstring label = L"",
                 const std::wstring& shape = L"",
                 const std::wstring& borderColor = L"",
                 std::wstring fillColor = L"");

    void open_sub(const std::wstring& name);
    void close_sub();
    void print_indent();

private:
    void inc_level();
    void dec_level();

    int level = 1;
    int subgraph_serial_num = 0;
    std::wostream& stream;
} ;

struct ASTGraphvizParentEdgeGenerator : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(mInvoker, genParentEdge);

    ASTGraphvizParentEdgeGenerator(std::wostream& os) : os_(os)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mInvoker);
    }

    ~ASTGraphvizParentEdgeGenerator()
    {
    }

    void genParentEdge(tree::ASTNode& node);
    void addParentEdge(tree::ASTNode* from, tree::ASTNode* to, const std::wstring& label = L"", const std::wstring& color = L"");

private:
    std::wostream& os_;

};

struct ASTGraphvizChildEdgeGenerator : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(mInvoker, genChildEdge);

    ASTGraphvizChildEdgeGenerator(std::wostream& os) : os_(os)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mInvoker);
    }

    ~ASTGraphvizChildEdgeGenerator()
    {
    }

    void genChildEdge(tree::ASTNode& node);
    void genChildEdge(tree::Annotation& node);
    void genChildEdge(tree::Annotations& node);
    void genChildEdge(tree::Internal& node);
    void genChildEdge(tree::Tangle& node);
    void genChildEdge(tree::Source& node);
    void genChildEdge(tree::Package& node);
    void genChildEdge(tree::Import& node);
    void genChildEdge(tree::Block& node);
    void genChildEdge(tree::Identifier& node);
    void genChildEdge(tree::SimpleIdentifier& node);
    void genChildEdge(tree::NestedIdentifier& node);
    void genChildEdge(tree::TemplatedIdentifier& node);
    void genChildEdge(tree::Literal& node);
    void genChildEdge(tree::TypeSpecifier& node);
    void genChildEdge(tree::FunctionSpecifier& node);
    void genChildEdge(tree::MultiSpecifier& node);
    void genChildEdge(tree::NamedSpecifier& node);
    void genChildEdge(tree::PrimitiveSpecifier& node);
    void genChildEdge(tree::PrimitiveType& node);
    void genChildEdge(tree::PointerType& node);
    void genChildEdge(tree::ReferenceType& node);
    void genChildEdge(tree::RecordType& node);
    void genChildEdge(tree::EnumType& node);
    void genChildEdge(tree::TypenameType& node);
    void genChildEdge(tree::TypedefType& node);
    void genChildEdge(tree::FunctionType& node);
    void genChildEdge(tree::Declaration& node);
    void genChildEdge(tree::ClassDecl& node);
    void genChildEdge(tree::EnumDecl& node);
    void genChildEdge(tree::TypedefDecl& node);
    void genChildEdge(tree::FunctionDecl& node);
    void genChildEdge(tree::VariableDecl& node);
    void genChildEdge(tree::TypenameDecl& node);
    void genChildEdge(tree::Statement& node);
    void genChildEdge(tree::DeclarativeStmt& node);
    void genChildEdge(tree::ExpressionStmt& node);
    void genChildEdge(tree::ForStmt& node);
    void genChildEdge(tree::ForeachStmt& node);
    void genChildEdge(tree::WhileStmt& node);
    void genChildEdge(tree::Selection& node);
    void genChildEdge(tree::IfElseStmt& node);
    void genChildEdge(tree::SwitchStmt& node);
    void genChildEdge(tree::BranchStmt& node);
    void genChildEdge(tree::Expression& node);
    void genChildEdge(tree::IdExpr& node);
    void genChildEdge(tree::LambdaExpr& node);
    void genChildEdge(tree::UnaryExpr& node);
    void genChildEdge(tree::BinaryExpr& node);
    void genChildEdge(tree::TernaryExpr& node);
    void genChildEdge(tree::MemberExpr& node);
    void genChildEdge(tree::CallExpr& node);
    void genChildEdge(tree::ArrayExpr& node);
    void genChildEdge(tree::CastExpr& node);
    void genChildEdge(tree::BlockExpr& node);
    void genChildEdge(tree::IsaExpr& node);
    void genChildEdge(tree::TieExpr& node);
    void genChildEdge(tree::IndexExpr& node);
    void genChildEdge(tree::UnpackExpr& node);
    void genChildEdge(tree::SystemCallExpr& node);
    void genChildEdge(tree::StringizeExpr& node);

    void addChildEdge(const tree::ASTNode* parent, const tree::ASTNode* child, const std::wstring& label = L"", const std::wstring& color = L"");

private:
    std::wostream& os_;

};

} } } } // namespace zillians::language::tree::visitor

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_ASTGRAPHVIZGENERATOR_H_ */
