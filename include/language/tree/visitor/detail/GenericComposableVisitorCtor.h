/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef BOOST_PP_IS_ITERATING
#if !defined(ZILLIANS_COMPILER_TREE_VISITOR_GENERICCOMPOSABLEVISITORCTOR_H_)
#define ZILLIANS_COMPILER_TREE_VISITOR_GENERICCOMPOSABLEVISITORCTOR_H_

#include <boost/preprocessor/iterate.hpp>
#include <boost/preprocessor/repetition/enum_params.hpp>
#include <boost/preprocessor/repetition/enum_binary_params.hpp>

#define BOOST_PP_FILENAME_1 \
    "language/tree/visitor/detail/GenericComposableVisitorCtor.h"
#define BOOST_PP_ITERATION_LIMITS (1, FUSION_MAX_VECTOR_SIZE)
#include BOOST_PP_ITERATE()

#endif
#else // defined(BOOST_PP_IS_ITERATING)

#define N BOOST_PP_ITERATION()

#if N == 1
    explicit
#endif
    GenericComposableVisitor(BOOST_PP_ENUM_BINARY_PARAMS(
        N, typename boost::fusion::detail::call_param<T, >::type _))
        : base_type(BOOST_PP_ENUM_PARAMS(N, _)) { REGISTER_ALL_VISITABLE_ASTNODE(applyInvoker) }

#undef N

#endif // defined(BOOST_PP_IS_ITERATING)
