/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_CALLEXPR_IPP_
#define ZILLIANS_LANGUAGE_TREE_CALLEXPR_IPP_

#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/algorithm_ext/push_front.hpp>
#include <boost/range/iterator_range.hpp>

#include "language/tree/expression/CallExpr.h"

namespace zillians { namespace language { namespace tree {

template<typename RangeType>
inline void CallExpr::appendParameters(const RangeType& new_parameters)
{
    const auto old_size = parameters.size();

    boost::push_back(parameters, new_parameters);

    for (auto*const parameter : boost::make_iterator_range(parameters, old_size, 0))
        parameter->parent = this;
}

template<typename RangeType>
inline void CallExpr::prependParameters(const RangeType& new_parameters)
{
    const auto old_size = parameters.size();

    boost::push_front(parameters, new_parameters);

    for (auto*const parameter : boost::make_iterator_range(parameters, 0, old_size))
        parameter->parent = this;
}

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_CALLEXPR_IPP_ */
