/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_EXPRESSION_H_
#define ZILLIANS_LANGUAGE_TREE_EXPRESSION_H_

#include <initializer_list>

#include <boost/logic/tribool.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/RelinkablePtr.h"

namespace zillians { namespace language { namespace tree {

struct NumericLiteral;
struct VariableDecl;

struct Expression : public ASTNode
{
    AST_NODE_HIERARCHY(Expression, ASTNode);

    Expression();

    bool isNullLiteral() const;
    bool isThisLiteral() const;
    bool isConstantLiteral() const;

    NumericLiteral* getConstantNumericLiteral() const;
    VariableDecl*   getConstantEnumerator() const;

    virtual bool hasValue() const = 0;
    virtual bool isLValue() const    ;
    virtual bool isRValue() const = 0;

    boost::tribool isMultiValue() const;

    static Expression* create(std::initializer_list<std::wstring> names);
    static Expression* create(const std::vector<std::wstring>& names);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (resolved_type)
        );
    }

    virtual Expression* clone() const = 0;

    virtual Type* getCanonicalType() const;

    relinkable_ptr<Type> resolved_type;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_EXPRESSION_H_ */
