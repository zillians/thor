/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_BINARYEXPR_H_
#define ZILLIANS_LANGUAGE_TREE_BINARYEXPR_H_

#include <ostream>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct BinaryExpr : public Expression
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(BinaryExpr, Expression);

    struct OpCode
    {
        enum type {
            ASSIGN, LSHIFT_ASSIGN, RSHIFT_ASSIGN, ADD_ASSIGN, SUB_ASSIGN, MUL_ASSIGN, DIV_ASSIGN, MOD_ASSIGN, AND_ASSIGN, OR_ASSIGN, XOR_ASSIGN,
            ARITHMETIC_ADD, ARITHMETIC_SUB, ARITHMETIC_MUL, ARITHMETIC_DIV, ARITHMETIC_MOD,
            BINARY_AND, BINARY_OR, BINARY_XOR, BINARY_LSHIFT, BINARY_RSHIFT,
            LOGICAL_AND, LOGICAL_OR,
            COMPARE_EQ, COMPARE_NE, COMPARE_GT, COMPARE_LT, COMPARE_GE, COMPARE_LE,
            RANGE_ELLIPSIS,
            INVALID,
        };

        static const wchar_t* toString(type op);

        // TODO remove this since it's no longer used ?
        static type decomposeAssignment(type t);
    };

    explicit BinaryExpr(OpCode::type opcode, Expression* left, Expression* right);

    template<int op>
    static BinaryExpr* create(Expression* lhs, Expression* rhs);

    bool isArithmetic() const;
    bool isAssignment() const;
    bool isShift() const;
    bool isBinary() const;
    bool isLogical() const;
    bool isComparison() const;
    bool isEqualComparison() const;
    bool isRighAssociative() const;
    bool isLeftAssociative() const;

    virtual bool hasValue() const override;
    virtual bool isRValue() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual BinaryExpr* clone() const override;

    using BaseNode::toSource;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (opcode)
            (left  )
            (right )
        );
    }

    OpCode::type opcode;
    Expression* left;
    Expression* right;

protected:
    BinaryExpr();
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_BINARYEXPR_H_ */
