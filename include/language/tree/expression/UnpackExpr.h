/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_UNPACKEXPR_H_
#define ZILLIANS_LANGUAGE_TREE_UNPACKEXPR_H_

#include <vector>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct CallExpr;
struct SimpleIdentifier;

struct UnpackExpr : public Expression
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(UnpackExpr, Expression);

    explicit UnpackExpr(CallExpr* call);
             UnpackExpr(CallExpr* call, std::vector<SimpleIdentifier*>&& unpack_list);

    void appendName(SimpleIdentifier* unpack_name);
    template<typename RangeType>
    void appendNames(const RangeType& unpack_names);

    virtual bool hasValue() const override;
    virtual bool isLValue() const override;
    virtual bool isRValue() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (call)
            (unpack_list)
        );
    }

    virtual UnpackExpr* clone() const override;

    CallExpr                       *call;
    std::vector<SimpleIdentifier*>  unpack_list;

private:
    UnpackExpr();

};

} } }

#include "language/tree/expression/UnpackExpr.ipp"

#endif /* ZILLIANS_LANGUAGE_TREE_UNPACKEXPR_H_ */
