/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ASTNODEFWD_H_
#define ZILLIANS_LANGUAGE_TREE_ASTNODEFWD_H_

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/transform.hpp>

namespace zillians { namespace language { namespace tree {

#define THOR_SCRIPT_AST_NODE_DEFS                                                                                \
    /* root */                                                          /* abstract */  /* base     */           \
    ((ASTNode            )("language/tree/ASTNode.h"                  )(   abstract   )                )         \
    /* module */                                                                                                 \
    ((Config             )("language/tree/module/Config.h"            )(   concrete   )(ASTNode       ))         \
    ((Internal           )("language/tree/module/Internal.h"          )(   concrete   )(ASTNode       ))         \
    ((Tangle             )("language/tree/module/Tangle.h"            )(   concrete   )(ASTNode       ))         \
    ((Source             )("language/tree/module/Source.h"            )(   concrete   )(ASTNode       ))         \
    ((Package            )("language/tree/module/Package.h"           )(   concrete   )(Annotatable   ))         \
    ((Import             )("language/tree/module/Import.h"            )(   concrete   )(ASTNode       ))         \
    /* basic */                                                                                                  \
    ((Annotation         )("language/tree/basic/Annotations.h"        )(   concrete   )(ASTNode       ))         \
    ((Annotations        )("language/tree/basic/Annotations.h"        )(   concrete   )(ASTNode       ))         \
    ((Annotatable        )("language/tree/basic/Annotations.h"        )(   abstract   )(ASTNode       ))         \
    ((Block              )("language/tree/basic/Block.h"              )(   abstract   )(ASTNode       ))         \
    ((NormalBlock        )("language/tree/basic/Block.h"              )(   concrete   )(Block         ))         \
    ((AsyncBlock         )("language/tree/basic/Block.h"              )(   concrete   )(Block         ))         \
    ((AtomicBlock        )("language/tree/basic/Block.h"              )(   concrete   )(Block         ))         \
    ((FlowBlock          )("language/tree/basic/Block.h"              )(   concrete   )(Block         ))         \
    ((LockBlock          )("language/tree/basic/Block.h"              )(   concrete   )(Block         ))         \
    ((PipelineBlock      )("language/tree/basic/Block.h"              )(   concrete   )(Block         ))         \
    ((Identifier         )("language/tree/basic/Identifier.h"         )(   abstract   )(ASTNode       ))         \
    ((SimpleIdentifier   )("language/tree/basic/Identifier.h"         )(   concrete   )(Identifier    ))         \
    ((NestedIdentifier   )("language/tree/basic/Identifier.h"         )(   concrete   )(Identifier    ))         \
    ((TemplatedIdentifier)("language/tree/basic/Identifier.h"         )(   concrete   )(Identifier    ))         \
    ((Literal            )("language/tree/basic/Literal.h"            )(   abstract   )(Expression    ))         \
    ((NumericLiteral     )("language/tree/basic/Literal.h"            )(   concrete   )(Literal       ))         \
    ((StringLiteral      )("language/tree/basic/Literal.h"            )(   concrete   )(Literal       ))         \
    ((ObjectLiteral      )("language/tree/basic/Literal.h"            )(   concrete   )(Literal       ))         \
    ((IdLiteral          )("language/tree/basic/Literal.h"            )(   abstract   )(Literal       ))         \
    ((TypeIdLiteral      )("language/tree/basic/Literal.h"            )(   concrete   )(IdLiteral     ))         \
    ((SymbolIdLiteral    )("language/tree/basic/Literal.h"            )(   concrete   )(IdLiteral     ))         \
    ((FunctionIdLiteral  )("language/tree/basic/Literal.h"            )(   concrete   )(IdLiteral     ))         \
    ((TypeSpecifier      )("language/tree/basic/TypeSpecifier.h"      )(   abstract   )(ASTNode       ))         \
    ((FunctionSpecifier  )("language/tree/basic/TypeSpecifier.h"      )(   concrete   )(TypeSpecifier ))         \
    ((MultiSpecifier     )("language/tree/basic/TypeSpecifier.h"      )(   concrete   )(TypeSpecifier ))         \
    ((NamedSpecifier     )("language/tree/basic/TyepSpecifier.h"      )(   concrete   )(TypeSpecifier ))         \
    ((PrimitiveSpecifier )("language/tree/basic/TypeSpecifier.h"      )(   concrete   )(TypeSpecifier ))         \
    /* type */                                                                                                   \
    ((Type               )("language/tree/basic/Type.h"               )(   abstract   )(ASTNode       ))         \
    ((PointerType        )("language/tree/basic/Type.h"               )(   concrete   )(Type          ))         \
    ((ReferenceType      )("language/tree/basic/Type.h"               )(   concrete   )(Type          ))         \
    ((MultiType          )("language/tree/basic/Type.h"               )(   concrete   )(Type          ))         \
    ((PrimitiveType      )("language/tree/basic/PrimitiveType.h"      )(   concrete   )(Type          ))         \
    ((DeclType           )("language/tree/basic/DeclType.h"           )(   abstract   )(Type          ))         \
    ((RecordType         )("language/tree/basic/DeclType.h"           )(   concrete   )(DeclType      ))         \
    ((EnumType           )("language/tree/basic/DeclType.h"           )(   concrete   )(DeclType      ))         \
    ((TypenameType       )("language/tree/basic/DeclType.h"           )(   concrete   )(DeclType      ))         \
    ((TypedefType        )("language/tree/basic/DeclType.h"           )(   concrete   )(DeclType      ))         \
    ((FunctionType       )("language/tree/basic/FunctionType.h"       )(   concrete   )(Type          ))         \
    /* declaration */                                                                                            \
    ((Declaration        )("language/tree/declaration/Declaration.h"  )(   abstract   )(Annotatable   ))         \
    ((ClassDecl          )("language/tree/declaration/ClassDecl.h"    )(   concrete   )(Declaration   ))         \
    ((EnumDecl           )("language/tree/declaration/EnumDecl.h"     )(   concrete   )(Declaration   ))         \
    ((TypedefDecl        )("language/tree/declaration/TypedefDecl.h"  )(   concrete   )(Declaration   ))         \
    ((FunctionDecl       )("language/tree/declaration/FunctionDecl.h" )(   concrete   )(Declaration   ))         \
    ((VariableDecl       )("language/tree/declaration/VariableDecl.h" )(   concrete   )(Declaration   ))         \
    ((TypenameDecl       )("language/tree/declaration/TypenameDecl.h" )(   concrete   )(Declaration   ))         \
    /* statement */                                                                                              \
    ((Statement          )("language/tree/statement/Statement.h"      )(   abstract   )(Annotatable   ))         \
    ((DeclarativeStmt    )("language/tree/statement/DeclarativeStmt.h")(   concrete   )(Statement     ))         \
    ((ExpressionStmt     )("language/tree/statement/ExpressionStmt.h" )(   concrete   )(Statement     ))         \
    ((IterativeStmt      )("language/tree/statement/IterativeStmt.h"  )(   abstract   )(Statement     ))         \
    ((ForStmt            )("language/tree/statement/IterativeStmt.h"  )(   concrete   )(IterativeStmt ))         \
    ((ForeachStmt        )("language/tree/statement/IterativeStmt.h"  )(   concrete   )(IterativeStmt ))         \
    ((WhileStmt          )("language/tree/statement/IterativeStmt.h"  )(   concrete   )(IterativeStmt ))         \
    ((Selection          )("language/tree/statement/SelectionStmt.h"  )(   concrete   )(ASTNode       ))         \
    ((SelectionStmt      )("language/tree/statement/SelectionStmt.h"  )(   abstract   )(Statement     ))         \
    ((IfElseStmt         )("language/tree/statement/SelectionStmt.h"  )(   concrete   )(SelectionStmt ))         \
    ((SwitchStmt         )("language/tree/statement/SelectionStmt.h"  )(   concrete   )(SelectionStmt ))         \
    ((BranchStmt         )("language/tree/statement/BranchStmt.h"     )(   concrete   )(Statement     ))         \
    /* expression */                                                                                             \
    ((ArrayExpr          )("language/tree/expression/ArrayExpr.h"     )(   concrete   )(Expression    ))         \
    ((BinaryExpr         )("language/tree/expression/BinaryExpr.h"    )(   concrete   )(Expression    ))         \
    ((BlockExpr          )("language/tree/expression/BlockExpr.h"     )(   concrete   )(Expression    ))         \
    ((CallExpr           )("language/tree/expression/CallExpr.h"      )(   concrete   )(Expression    ))         \
    ((CastExpr           )("language/tree/expression/CastExpr.h"      )(   concrete   )(Expression    ))         \
    ((Expression         )("language/tree/expression/Expression.h"    )(   abstract   )(ASTNode       ))         \
    ((IdExpr             )("language/tree/expression/IdExpr.h"        )(   concrete   )(Expression    ))         \
    ((IndexExpr          )("language/tree/expression/IndexExpr.h"     )(   concrete   )(Expression    ))         \
    ((IsaExpr            )("language/tree/expression/IsaExpr.h"       )(   concrete   )(Expression    ))         \
    ((LambdaExpr         )("language/tree/expression/LambdaExpr.h"    )(   concrete   )(Expression    ))         \
    ((MemberExpr         )("language/tree/expression/MemberExpr.h"    )(   concrete   )(Expression    ))         \
    ((StringizeExpr      )("language/tree/expression/StringizeExpr.h" )(   concrete   )(Expression    ))         \
    ((SystemCallExpr     )("language/tree/expression/SystemCallExpr.h")(   concrete   )(Expression    ))         \
    ((TernaryExpr        )("language/tree/expression/TernaryExpr.h"   )(   concrete   )(Expression    ))         \
    ((TieExpr            )("language/tree/expression/TieExpr.h"       )(   concrete   )(Expression    ))         \
    ((UnaryExpr          )("language/tree/expression/UnaryExpr.h"     )(   concrete   )(Expression    ))         \
    ((UnpackExpr         )("language/tree/expression/UnpackExpr.h"    )(   concrete   )(Expression    ))

#define THOR_SCRIPT_AST_NODE_NAME_IMPL(r, _, node_def)            BOOST_PP_SEQ_ELEM(0, node_def)
#define THOR_SCRIPT_AST_NODE_NAMES                                BOOST_PP_SEQ_TRANSFORM(THOR_SCRIPT_AST_NODE_NAME_IMPL, _, THOR_SCRIPT_AST_NODE_DEFS)
#define THOR_SCRIPT_AST_QUALIFIED_NODE_NAME_IMPL(r, _, node_def) ::zillians::language::tree::BOOST_PP_SEQ_ELEM(0, node_def)
#define THOR_SCRIPT_AST_QUALIFIED_NODE_NAMES                      BOOST_PP_SEQ_TRANSFORM(THOR_SCRIPT_AST_QUALIFIED_NODE_NAME_IMPL, _, THOR_SCRIPT_AST_NODE_DEFS)

#define AST_NODE_FWD_DECL(r, _, node_name) struct node_name;

BOOST_PP_SEQ_FOR_EACH(AST_NODE_FWD_DECL, _, THOR_SCRIPT_AST_NODE_NAMES)

#undef AST_NODE_FWD_DECL

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_ASTNODEFWD_H_ */

