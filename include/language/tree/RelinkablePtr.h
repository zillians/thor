/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_RELINKABLE_PTR_H_
#define ZILLIANS_LANGUAGE_TREE_RELINKABLE_PTR_H_

#include <cstddef>

#include <type_traits>
#include <utility>

#include <boost/assert.hpp>
#include <boost/operators.hpp>
#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>

#include "language/tree/UniqueName.h"

namespace zillians { namespace language { namespace tree {

template<typename the_value_type>
class relinkable_ptr : public boost::totally_ordered<relinkable_ptr<the_value_type>>
{
    template<typename>
    friend class relinkable_ptr;

public:
    using value_type = the_value_type;

private:
    using variant_type = boost::variant<
        value_type*,
        unique_name_t
    >;

public:
             relinkable_ptr(                                 ) : ptr_or_unique_name(          nullptr     ) {}
             relinkable_ptr(std::nullptr_t                   ) : ptr_or_unique_name(          nullptr     ) {}
    explicit relinkable_ptr(value_type*           ptr        ) : ptr_or_unique_name(          ptr         ) {}
    explicit relinkable_ptr(const unique_name_t&  unique_name) : ptr_or_unique_name(          unique_name ) {}
    explicit relinkable_ptr(      unique_name_t&& unique_name) : ptr_or_unique_name(std::move(unique_name)) {}

    template<typename another_value_type, typename std::enable_if<std::is_convertible<another_value_type*, value_type*>::value>::type * = nullptr>
    relinkable_ptr(const relinkable_ptr<another_value_type>& ref)
        : ptr_or_unique_name(ref.ptr_or_unique_name)
    {
    }

    template<typename another_value_type, typename std::enable_if<std::is_convertible<another_value_type*, value_type*>::value>::type * = nullptr>
    relinkable_ptr(relinkable_ptr<another_value_type>&& ref)
        : ptr_or_unique_name(std::move(ref.ptr_or_unique_name))
    {
    }

    relinkable_ptr& operator=(std::nullptr_t                   ) { ptr_or_unique_name =           nullptr     ; return *this; }
    relinkable_ptr& operator=(value_type*           ptr        ) { ptr_or_unique_name =           ptr         ; return *this; }
    relinkable_ptr& operator=(const unique_name_t&  unique_name) { ptr_or_unique_name =           unique_name ; return *this; }
    relinkable_ptr& operator=(      unique_name_t&& unique_name) { ptr_or_unique_name = std::move(unique_name); return *this; }

    template<typename another_value_type, typename std::enable_if<std::is_convertible<another_value_type*, value_type*>::value>::type * = nullptr>
    relinkable_ptr& operator=(const relinkable_ptr<another_value_type>& ref)
    {
        ptr_or_unique_name = ref.ptr_or_unique_name;

        return *this;
    }

    template<typename another_value_type, typename std::enable_if<std::is_convertible<another_value_type*, value_type*>::value>::type * = nullptr>
    relinkable_ptr& operator=(relinkable_ptr<another_value_type>&& ref)
    {
        ptr_or_unique_name = std::move(ref.ptr_or_unique_name);

        return *this;
    }

    operator value_type*() const { return get_ptr(); }

    value_type& operator* () const { return *get_ptr(); }
    value_type* operator->() const { return  get_ptr(); }

    bool is_ptr        () const { return boost::get<value_type*  >(&ptr_or_unique_name) != nullptr; }
    bool is_unique_name() const { return boost::get<unique_name_t>(&ptr_or_unique_name) != nullptr; }

    const variant_type&  get            () const noexcept { return ptr_or_unique_name       ; }
    value_type*          get_ptr        () const          { return get_impl<value_type*  >(); }
    const unique_name_t& get_unique_name() const          { return get_impl<unique_name_t>(); }

    void set_ptr        (value_type*           ptr        ) { ptr_or_unique_name =           ptr         ; }
    void set_unique_name(const unique_name_t&  unique_name) { ptr_or_unique_name =           unique_name ; }
    void set_unique_name(      unique_name_t&& unique_name) { ptr_or_unique_name = std::move(unique_name); }

    bool operator< (const relinkable_ptr& rhs) const { return ptr_or_unique_name <  rhs.ptr_or_unique_name; }
    bool operator==(const relinkable_ptr& rhs) const { return ptr_or_unique_name == rhs.ptr_or_unique_name; }

    template<typename archive>
    void serialize(archive& ar, unsigned)
    {
        ar & ptr_or_unique_name;
    }

private:
    template<typename want_type>
    const want_type& get_impl() const
    {
        BOOST_ASSERT(boost::get<want_type>(&ptr_or_unique_name) != nullptr && "not specifc type!");

        return boost::get<want_type>(ptr_or_unique_name);
    }

private:
    variant_type ptr_or_unique_name;
};

template<typename value_type>
relinkable_ptr<value_type> make_relinkable(value_type* ptr)
{
    return relinkable_ptr<value_type>(ptr);
}

template<typename value_type>
inline bool operator==(const relinkable_ptr<value_type>& lhs, std::nullptr_t)
{
    return lhs == relinkable_ptr<value_type>(nullptr);
}

template<typename value_type>
inline bool operator!=(const relinkable_ptr<value_type>& lhs, std::nullptr_t)
{
    return lhs != relinkable_ptr<value_type>(nullptr);
}

template<typename value_type>
inline bool operator> (const relinkable_ptr<value_type>& lhs, std::nullptr_t)
{
    return lhs >  relinkable_ptr<value_type>(nullptr);
}

template<typename value_type>
inline bool operator>=(const relinkable_ptr<value_type>& lhs, std::nullptr_t)
{
    return lhs >= relinkable_ptr<value_type>(nullptr);
}

template<typename value_type>
inline bool operator<=(const relinkable_ptr<value_type>& lhs, std::nullptr_t)
{
    return lhs <= relinkable_ptr<value_type>(nullptr);
}

template<typename value_type>
inline bool operator< (const relinkable_ptr<value_type>& lhs, std::nullptr_t)
{
    return lhs <  relinkable_ptr<value_type>(nullptr);
}

template<typename value_type>
inline bool operator==(const relinkable_ptr<value_type>& lhs, value_type*const rhs)
{
    return lhs == relinkable_ptr<value_type>(rhs);
}

template<typename value_type>
inline bool operator!=(const relinkable_ptr<value_type>& lhs, value_type*const rhs)
{
    return lhs != relinkable_ptr<value_type>(rhs);
}

template<typename value_type>
inline bool operator> (const relinkable_ptr<value_type>& lhs, value_type*const rhs)
{
    return lhs >  relinkable_ptr<value_type>(rhs);
}

template<typename value_type>
inline bool operator>=(const relinkable_ptr<value_type>& lhs, value_type*const rhs)
{
    return lhs >= relinkable_ptr<value_type>(rhs);
}

template<typename value_type>
inline bool operator<=(const relinkable_ptr<value_type>& lhs, value_type*const rhs)
{
    return lhs <= relinkable_ptr<value_type>(rhs);
}

template<typename value_type>
inline bool operator< (const relinkable_ptr<value_type>& lhs, value_type*const rhs)
{
    return lhs <  relinkable_ptr<value_type>(rhs);
}

template<typename value_type>
inline bool operator==(std::nullptr_t, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(nullptr) == rhs;
}

template<typename value_type>
inline bool operator!=(std::nullptr_t, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(nullptr) != rhs;
}

template<typename value_type>
inline bool operator> (std::nullptr_t, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(nullptr) >  rhs;
}

template<typename value_type>
inline bool operator>=(std::nullptr_t, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(nullptr) >= rhs;
}

template<typename value_type>
inline bool operator<=(std::nullptr_t, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(nullptr) <= rhs;
}

template<typename value_type>
inline bool operator< (std::nullptr_t, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(nullptr) <  rhs;
}

template<typename value_type>
inline bool operator==(value_type*const lhs, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(lhs) == rhs;
}

template<typename value_type>
inline bool operator!=(value_type*const lhs, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(lhs) != rhs;
}

template<typename value_type>
inline bool operator> (value_type*const lhs, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(lhs) >  rhs;
}

template<typename value_type>
inline bool operator>=(value_type*const lhs, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(lhs) >= rhs;
}

template<typename value_type>
inline bool operator<=(value_type*const lhs, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(lhs) <= rhs;
}

template<typename value_type>
inline bool operator< (value_type*const lhs, const relinkable_ptr<value_type>& rhs)
{
    return relinkable_ptr<value_type>(lhs) <  rhs;
}

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_RELINKABLE_PTR_H_ */
