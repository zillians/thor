/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/stringize.hpp>

#include "utility/Traits.h"

#include "language/tree/ASTNode.h"
#include "language/tree/module/Config.h"
#include "language/tree/module/Internal.h"
#include "language/tree/module/Tangle.h"
#include "language/tree/module/Source.h"
#include "language/tree/module/Package.h"
#include "language/tree/module/Import.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/TypedefDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/statement/IterativeStmt.h"
#include "language/tree/statement/SelectionStmt.h"
#include "language/tree/statement/BranchStmt.h"
#include "language/tree/expression/ArrayExpr.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/CastExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/IdExpr.h"
#include "language/tree/expression/IndexExpr.h"
#include "language/tree/expression/IsaExpr.h"
#include "language/tree/expression/LambdaExpr.h"
#include "language/tree/expression/MemberExpr.h"
#include "language/tree/expression/StringizeExpr.h"
#include "language/tree/expression/SystemCallExpr.h"
#include "language/tree/expression/TernaryExpr.h"
#include "language/tree/expression/TieExpr.h"
#include "language/tree/expression/UnaryExpr.h"
#include "language/tree/expression/UnpackExpr.h"

#define THOR_SCRIPT_AST_NODE_IS_DEFINED(r, _, node_name)         \
    static_assert(                                               \
        ::zillians::traits::is_complete_type<                    \
            ::zillians::language::tree::node_name                \
        >::value,                                                \
        "\"" BOOST_PP_STRINGIZE(node_name) "\" is not defined,"  \
        " please include header contains its definition!"        \
    );

BOOST_PP_SEQ_FOR_EACH(THOR_SCRIPT_AST_NODE_IS_DEFINED, _, THOR_SCRIPT_AST_NODE_NAMES)

#undef THOR_SCRIPT_AST_NODE_IS_DEFINED
