/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_FUNCTIONDECL_H_
#define ZILLIANS_LANGUAGE_TREE_FUNCTIONDECL_H_

#include <ostream>
#include <string>
#include <tuple>
#include <vector>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/declaration/Declaration.h"

namespace zillians { namespace language { namespace tree {

// TODO toString function to print self information
struct FunctionDecl : public Declaration
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(FunctionDecl, Declaration);

    class Creator; // Helper class to create function declaration with fewer arguments

    explicit FunctionDecl(Identifier* name, TypeSpecifier* type, bool is_member, bool is_static, bool is_virtual, bool is_override, Declaration::VisibilitySpecifier::type visibility, Block* block = nullptr);

    void prependParameter(VariableDecl* parameter_decl);
    void appendParameter(VariableDecl* parameter_decl);
    void appendParameter(SimpleIdentifier* name, TypeSpecifier* type = NULL, Expression* initializer = NULL);
    template<typename RangeType>
    void appendParameters(const RangeType& parameter_decls);
    void setReturnType(TypeSpecifier* return_type);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    bool isCompleted() const;
    bool isTemplated() const;
    bool isGlobalInit() const;

    bool isConstructor() const;
    bool isDefaultConstructor() const;
    bool isDestructor() const;

    bool hasBody() const noexcept;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual std::wstring toString() const override;
    virtual FunctionDecl* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (      parameters    )
            (      type          )
            (      is_member     )
            (      is_static     )
            (      is_virtual    )
            (      is_override   )
            ((int&)visibility    )
            (      block         )
            (      is_lambda     )
            (      is_task       )
            (      is_global_init)
        );
    }

    virtual FunctionType* getType() const override;
    virtual FunctionType* getCanonicalType() const override;

    std::vector<VariableDecl*> parameters;
    TypeSpecifier* type;
    bool is_member;
    bool is_static;
    bool is_virtual;
    bool is_override;
    Declaration::VisibilitySpecifier::type visibility;
    Block* block;
    bool is_lambda;
    bool is_task;
    bool is_global_init;

protected:
    FunctionDecl() { }
};

class FunctionDecl::Creator
{
public:
    using VisibilitySpecifier = Declaration::VisibilitySpecifier::type;

    Creator name      (Identifier*         new_name      );
    Creator type      (TypeSpecifier*      new_type      );
    Creator block     (Block*              new_block     );
    Creator visibility(VisibilitySpecifier new_visibility);

    Creator member  (bool value = false);
    Creator static_ (bool value = false);
    Creator virtual_(bool value = false);
    Creator override(bool value = false);

    FunctionDecl* create() const;

private:
    Identifier*         name_       = nullptr;
    TypeSpecifier*      type_       = nullptr;
    Block*              block_      = nullptr;
    VisibilitySpecifier visibility_ = Declaration::VisibilitySpecifier::DEFAULT;
    bool                is_member   = false;
    bool                is_static   = false;
    bool                is_virtual  = false;
    bool                is_override = false;
};

template<typename RangeType>
inline void FunctionDecl::appendParameters(const RangeType& parameter_decls)
{
    for (tree::VariableDecl* parameter_decl : parameter_decls)
        appendParameter(parameter_decl);
}

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_FUNCTIONDECL_H_ */
