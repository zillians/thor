/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_CLASSDECL_H_
#define ZILLIANS_LANGUAGE_TREE_CLASSDECL_H_

#include <cstdint>

#include <ostream>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include <boost/iterator/iterator_categories.hpp>
#include <boost/range/any_range.hpp>

#include "language/resolver/SymbolTable.h"
#include "language/resolver/VirtualTable.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/declaration/Declaration.h"

namespace zillians { namespace language { namespace tree {

struct ClassDecl : public Declaration
{
    friend class boost::serialization::access;

    using BasesRange = boost::any_range<ClassDecl*, boost::random_access_traversal_tag, ClassDecl*, std::ptrdiff_t>;

    AST_NODE_HIERARCHY(ClassDecl, Declaration);

    explicit ClassDecl(Identifier* name, bool is_interface = false);

    void addFunction(FunctionDecl* func);
    void insertFunction(std::vector<FunctionDecl*>::iterator it, FunctionDecl* func);

    void addVariable(VariableDecl* var);

    void setBase(NamedSpecifier* extends_from);
    void addInterface(NamedSpecifier* interface_);

    BasesRange getBaseClassDecls() const;

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    bool isCompleted() const;
    bool isTemplated() const;

    bool isReplicable() const;

    bool hasDefaultConstructor() const;

    bool                           hasVirtual() const;
    std::pair<std::uint32_t, bool> getVirtualIndex(const tree::FunctionDecl& member_function) const;
    bool                           hasPureVirtual() const;
    std::set<const FunctionDecl*>  getPureVirtuals() const;

    std::vector<virtual_table::segment_info_type> generateVirtualTableInfo() const;

    bool isVirtualTableReady() const noexcept;
    bool buildVirtualTable();

private:
    bool buildVirtualTableImpl(std::set<const ClassDecl*>& visited);

public:
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual std::wstring toString() const override;
    virtual ClassDecl* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (base                 )
            (implements           )
            (member_functions     )
            (member_variables     )
            (is_interface         )
            (record_type_         )
            (is_base_vtables_ready)
            (vtable               )
        );
    }

    NamedSpecifier* base;
    std::vector<NamedSpecifier*> implements;
    std::vector<FunctionDecl*> member_functions;
    std::vector<VariableDecl*> member_variables;
    bool is_interface;

    bool          is_base_vtables_ready;
    virtual_table vtable;
    SymbolTable   symbol_table;

    virtual RecordType* getType() const override;
    virtual RecordType* getCanonicalType() const override;

protected:
    ClassDecl() { }

private:
    RecordType* record_type_;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_CLASSDECL_H_ */
