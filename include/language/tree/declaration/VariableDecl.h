/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VARIABLEDECL_H_
#define ZILLIANS_LANGUAGE_TREE_VARIABLEDECL_H_

#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

struct VariableDecl : public Declaration
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(VariableDecl, Declaration);

    class Creator; // Helper class to create variable declaration with fewer arguments

    explicit VariableDecl(Identifier* name, TypeSpecifier* type, bool is_member, bool is_static, bool is_const, Declaration::VisibilitySpecifier::type visibility, Expression* initializer = NULL);

    bool hasInitializer() const noexcept;
    void setInitializer(Expression* init);

    bool isLocal() const;

    bool isParameter() const;

    bool isEnumerator() const;
    int64 getEnumeratorValue() const;
    EnumDecl* getEnumeration() const;

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual std::wstring toString() const override;
    virtual VariableDecl* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<Declaration>(*this);
        ar & type;
        ar & is_member;
        ar & is_static;
        ar & is_const;
        ar & (int&)visibility;
        ar & initializer;
    }

    virtual Type* getType() const override;
    virtual Type* getCanonicalType() const override;

    TypeSpecifier* type;
    bool is_member;
    bool is_static;
    bool is_const;
    Declaration::VisibilitySpecifier::type visibility;
    Expression* initializer;

protected:
    VariableDecl() { }
};

class VariableDecl::Creator
{
public:
    using VisibilitySpecifier = Declaration::VisibilitySpecifier::type;

    Creator name      (Identifier*         new_name      );
    Creator type      (TypeSpecifier*      new_type      );
    Creator init      (Expression*         new_init      );
    Creator visibility(VisibilitySpecifier new_visibility);

    Creator member ();
    Creator static_();
    Creator const_ ();

    VariableDecl* create() const;

private:
    Identifier*         name_       = nullptr;
    TypeSpecifier*      type_       = nullptr;
    Expression*         init_       = nullptr;
    VisibilitySpecifier visibility_ = Declaration::VisibilitySpecifier::DEFAULT;
    bool                is_member   = false;
    bool                is_static   = false;
    bool                is_const    = false;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_VARIABLEDECL_H_ */
