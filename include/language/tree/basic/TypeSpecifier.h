/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_TYPESPECIFIER_H_
#define ZILLIANS_LANGUAGE_TREE_TYPESPECIFIER_H_

#include <iosfwd>
#include <string>
#include <vector>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/basic/PrimitiveKind.h"

namespace zillians { namespace language { namespace tree {

struct Identifier;
struct Type;

struct TypeSpecifier : public ASTNode
{
    AST_NODE_HIERARCHY(TypeSpecifier, ASTNode);

    Type* getCanonicalType() const;

    virtual std::wstring toString() const = 0;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual TypeSpecifier* clone() const = 0;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (resolved_type)
        );
    }

    relinkable_ptr<Type> resolved_type;
};

struct PrimitiveSpecifier : public TypeSpecifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(PrimitiveSpecifier, TypeSpecifier);

private:
             PrimitiveSpecifier();
public:
    explicit PrimitiveSpecifier(PrimitiveKind kind);

    PrimitiveKind getKind() const;

    virtual std::wstring toString() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual PrimitiveSpecifier* clone() const override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (kind)
        );
    }

private:
    PrimitiveKind kind;
};

struct NamedSpecifier : public TypeSpecifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(NamedSpecifier, TypeSpecifier);

private:
             NamedSpecifier();
public:
    explicit NamedSpecifier(Identifier* name);

    Identifier* getName() const noexcept;

    virtual std::wstring toString() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual NamedSpecifier* clone() const override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (name)
        );
    }

private:
    Identifier* name;
};

struct MultiSpecifier : public TypeSpecifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(MultiSpecifier, TypeSpecifier);

private:
             MultiSpecifier();
public:
    explicit MultiSpecifier(const std::vector<TypeSpecifier*>&    types);
    explicit MultiSpecifier(      std::vector<TypeSpecifier*>&&   types);

    const std::vector<TypeSpecifier*>& getTypes() const noexcept;

    virtual std::wstring toString() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual MultiSpecifier* clone() const override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (types)
        );
    }

private:
    std::vector<TypeSpecifier*> types;
};

struct FunctionSpecifier : public TypeSpecifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(FunctionSpecifier, TypeSpecifier);

private:
    FunctionSpecifier();
public:
    FunctionSpecifier(const std::vector<TypeSpecifier*>&  param_types, TypeSpecifier* return_type, bool is_lambda = false);
    FunctionSpecifier(      std::vector<TypeSpecifier*>&& param_types, TypeSpecifier* return_type, bool is_lambda = false);

    const std::vector<TypeSpecifier*>& getParameterTypes() const noexcept;
    TypeSpecifier*                     getReturnType() const noexcept;
    bool                               isLambda() const noexcept;

    virtual std::wstring toString() const override;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual FunctionSpecifier* clone() const override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (param_types)
            (return_type)
            (is_lambda  )
        );
    }

private:
    std::vector<TypeSpecifier*> param_types;
    TypeSpecifier*              return_type;
    bool                        is_lambda  ;
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_TYPESPECIFIER_H_ */
