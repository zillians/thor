/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_BLOCK_H_
#define ZILLIANS_LANGUAGE_TREE_BLOCK_H_

#include <initializer_list>

#include "language/tree/ASTNode.h"
#include "language/tree/statement/Statement.h"
#include "language/Architecture.h"

namespace zillians { namespace language { namespace tree {

struct Expression;

struct Block : public ASTNode
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Block, ASTNode)

    explicit Block(Architecture arch = Architecture::zero());
    explicit Block(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero());

    bool isImplicit() const noexcept;
    void setImplicit(bool new_is_implicit) noexcept;

    void prependObject(Statement* object);
    void appendObject(Statement* object);

    Statement* insertObjectBefore(Statement* before, Statement* object, bool replace_before = false);
    Statement* insertObjectAfter(Statement* after, Statement* object, bool replace_after = false);

    template<typename T> void prependObjects(const T& object_list);
                         void prependObjects(std::initializer_list<Statement*> object_list);
    template<typename T> void appendObjects(const T& object_list);
                         void appendObjects(std::initializer_list<Statement*> object_list);

    template<typename T> bool insertObjectsBefore(Statement* before, T& object_list, bool replace_before = false);
    template<typename T> bool insertObjectsAfter(Statement* after, T& object_list, bool replace_after = false);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    Architecture getActualArch() const noexcept;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override = 0; using BaseNode::toSource;
    virtual Block* clone() const override = 0;

    bool merge(Block& rhs);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);

    Architecture          arch;
    bool                  is_implicit;
    std::list<Statement*> objects;
};

struct NormalBlock : public Block
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(NormalBlock, Block)

    explicit NormalBlock(Architecture arch = Architecture::zero());
    explicit NormalBlock(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero());

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual NormalBlock* clone() const override;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);
};

struct AsyncBlock : public Block
{
    AST_NODE_HIERARCHY(AsyncBlock, Block)

    explicit AsyncBlock(Architecture arch = Architecture::zero(), Expression* target = nullptr, Expression* block = nullptr, Expression* grid = nullptr);
    explicit AsyncBlock(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero(), Expression* target = nullptr, Expression* block = nullptr, Expression* grid = nullptr);

    bool hasTarget() const noexcept;
    bool hasBlock () const noexcept;
    bool hasGrid  () const noexcept;

    Expression* getTarget() const noexcept;
    Expression* getBlock () const noexcept;
    Expression* getGrid  () const noexcept;

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual AsyncBlock* clone() const override;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);

private:
    Expression* target;
    Expression* block;
    Expression* grid;
};

struct AtomicBlock : public Block
{
    AST_NODE_HIERARCHY(AtomicBlock, Block)

    explicit AtomicBlock(Architecture arch = Architecture::zero(), int stm_policy = 0);
    explicit AtomicBlock(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero(), int stm_policy = 0);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual AtomicBlock* clone() const override;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);

private:
    int stm_policy;
};

struct FlowBlock : public Block
{
    AST_NODE_HIERARCHY(FlowBlock, Block)

    explicit FlowBlock(Architecture arch = Architecture::zero());
    explicit FlowBlock(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero());

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual FlowBlock* clone() const override;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);
};

struct LockBlock : public Block
{
    AST_NODE_HIERARCHY(LockBlock, Block)

    explicit LockBlock(Architecture arch = Architecture::zero(), bool rw_lock_or_simple_mutex = true);
    explicit LockBlock(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero(), bool rw_lock_or_simple_mutex = true);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual LockBlock* clone() const override;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);

private:
    bool rw_lock_or_simple_mutex;
};

struct PipelineBlock : public Block
{
    AST_NODE_HIERARCHY(PipelineBlock, Block)

    explicit PipelineBlock(Architecture arch = Architecture::zero(), bool async_or_sync = true);
    explicit PipelineBlock(std::initializer_list<Statement*> objects, Architecture arch = Architecture::zero(), bool async_or_sync = true);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual PipelineBlock* clone() const override;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version);

private:
    bool async_or_sync;
};

} } }

#include "language/tree/basic/Block.ipp"

#endif /* ZILLIANS_LANGUAGE_TREE_BLOCK_H_ */
