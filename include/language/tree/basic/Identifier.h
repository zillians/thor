/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_IDENTIFIER_H_
#define ZILLIANS_LANGUAGE_TREE_IDENTIFIER_H_

#include <initializer_list>
#include <string>

#include "language/tree/ASTNode.h"
#include "language/tree/RelinkablePtr.h"

namespace zillians { namespace language { namespace tree {

struct Identifier : public ASTNode
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Identifier, ASTNode);

    Identifier();

    virtual std::wstring toString() const = 0;
    virtual bool isEmpty() const = 0;
    virtual Identifier* clone() const = 0;

    TemplatedIdentifier* getTemplateId();
    SimpleIdentifier* getSimpleId();

    static SimpleIdentifier* randomId(const std::wstring& prefix = std::wstring());

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (resolved_type)
        );
    }

    virtual Type* getCanonicalType() const;

    relinkable_ptr<Type> resolved_type;
};

struct SimpleIdentifier : public Identifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(SimpleIdentifier, Identifier);

    explicit SimpleIdentifier(const std::wstring& s);

    virtual std::wstring toString() const;
    virtual bool isEmpty() const;

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);
    virtual SimpleIdentifier* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (name)
        );
    }

    std::wstring name;

protected:
    SimpleIdentifier();
};

struct NestedIdentifier : public Identifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(NestedIdentifier, Identifier);

    NestedIdentifier();
    NestedIdentifier(const std::initializer_list<Identifier*>& id_list);

    virtual std::wstring toString() const;
    virtual bool isEmpty() const;

    void appendIdentifier(Identifier* id);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);
    virtual NestedIdentifier* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    static NestedIdentifier* create(std::initializer_list<Identifier*> new_identifier_list);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (identifier_list)
        );
    }

    std::vector<Identifier*> identifier_list;
};

struct TemplatedIdentifier : public Identifier
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(TemplatedIdentifier, Identifier);

    struct Usage
    {
        enum type {
            FORMAL_PARAMETER,
            ACTUAL_ARGUMENT,
        };

        static const wchar_t* toString(type t)
        {
            switch(t)
            {
            case FORMAL_PARAMETER: return L"param";
            case ACTUAL_ARGUMENT: return L"arg";
            }
        }
    };

    explicit TemplatedIdentifier(Usage::type type, SimpleIdentifier* id);
    explicit TemplatedIdentifier(SimpleIdentifier* id, const std::initializer_list<TypeSpecifier*>& templated_types);
    virtual std::wstring toString() const;

    bool isVariadic() const;
    bool isFullySpecialized() const;
    void specialize();

    virtual bool isEmpty() const;
    void setIdentifier(SimpleIdentifier* identifier);

    void append(TypenameDecl* type);
    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);
    virtual TemplatedIdentifier* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        AST_NODE_SERIALIZE(
            ar,
            (type               )
            (id                 )
            (templated_type_list)
        );
    }

    Usage::type type;
    SimpleIdentifier* id;
    std::vector<TypenameDecl*> templated_type_list;

protected:
    TemplatedIdentifier();
};


} } }

#endif /* ZILLIANS_LANGUAGE_TREE_IDENTIFIER_H_ */
