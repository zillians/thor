/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_DECLTYPE_H_
#define ZILLIANS_LANGUAGE_TREE_DECLTYPE_H_

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"

namespace zillians { namespace language { namespace tree {

struct ClassDecl;
struct EnumDecl;
struct TypenameDecl;
struct TypedefDecl;

//////////////////////////////////////////////////////////////////////////////
// DeclType
//////////////////////////////////////////////////////////////////////////////

struct DeclType : public Type
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(DeclType, Type);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);
        ar & boost::serialization::base_object<Type>(*this);
    }

    virtual DeclType* clone() const override = 0;

    virtual Declaration* getDecl() const = 0;
    static DeclType* create(Declaration* declaration);
    virtual Type* getCanonicalType() const override = 0;

private:
    friend Internal;
} ;

//////////////////////////////////////////////////////////////////////////////
// RecordType
//////////////////////////////////////////////////////////////////////////////

struct RecordType : public DeclType
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(RecordType, DeclType);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);
        ar & boost::serialization::base_object<DeclType>(*this);
        ar & decl_;
    }

    virtual RecordType* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    virtual Declaration* getDecl() const;
    static RecordType* create(Internal* internal, ClassDecl* classDecl);
    virtual RecordType* getCanonicalType() const override;
    bool isInterface() const;

protected:
    RecordType();
    RecordType(ClassDecl* classDecl);

private:
    friend Internal;
    friend ClassDecl;
    ClassDecl* decl_;
} ;

//////////////////////////////////////////////////////////////////////////////
// EnumType
//////////////////////////////////////////////////////////////////////////////

struct EnumType : public DeclType
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(EnumType, DeclType);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);
        ar & boost::serialization::base_object<DeclType>(*this);
        ar & decl_;
    }

    virtual EnumType* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    virtual Declaration* getDecl() const;
    static EnumType* create(Internal* internal, EnumDecl* enumDecl);
    virtual EnumType* getCanonicalType() const override;

    virtual PrimitiveType* getArithmeticCompatibleType() const override;

protected:
    EnumType();
    EnumType(EnumDecl* enumDecl);

private:
    friend Internal;
    friend EnumDecl;
    EnumDecl* decl_;
} ;

//////////////////////////////////////////////////////////////////////////////
// TypedefType
//////////////////////////////////////////////////////////////////////////////

struct TypedefType : public DeclType
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(TypedefType, DeclType);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);
        ar & boost::serialization::base_object<DeclType>(*this);
        ar & decl_;
    }

    virtual TypedefType* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    virtual Declaration* getDecl() const;
    static TypedefType* create(Internal* internal, TypedefDecl* typedefDecl);
    virtual Type* getCanonicalType() const override;

protected:
    TypedefType();
    TypedefType(TypedefDecl* typedefDecl);

private:
    friend Internal;
    friend TypedefDecl;
    TypedefDecl* decl_;
} ;

//////////////////////////////////////////////////////////////////////////////
// TypenameType
//////////////////////////////////////////////////////////////////////////////

struct TypenameType : public DeclType
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(TypenameType, DeclType);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from,
                                const ASTNode& to,
                                bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);
        ar & boost::serialization::base_object<DeclType>(*this);
        ar & decl_;
    }

    virtual TypenameType* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;

    virtual Declaration* getDecl() const;
    static TypenameType* create(Internal* internal, TypenameDecl* typenameDecl);
    virtual Type* getCanonicalType() const override;

protected:
    TypenameType();
    TypenameType(TypenameDecl* typenameDecl);

private:
    friend Internal;
    friend TypenameDecl;
    TypenameDecl* decl_;
} ;

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_DECLTYPE_H_ */
