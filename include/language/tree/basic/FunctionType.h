/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_FUNCTIONTYPE_H_
#define ZILLIANS_LANGUAGE_TREE_FUNCTIONTYPE_H_

#include <iosfwd>
#include <string>
#include <vector>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"

namespace zillians { namespace language { namespace tree {

struct RecordType;

struct FunctionType: public Type
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(FunctionType, Type);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (parameter_types)
            (return_type    )
            (class_type     )
        );
    }

public: // ctor
    FunctionType();

public: // interface
    virtual size_t getTypeClassSerialNumber() const;
    virtual ConversionRank getConversionRank(const Type& target, const ConversionPolicy policy = Type::ConversionPolicy::Implicitly) const override;
    virtual bool isSame(const Type& rhs) const;
    virtual bool isLessThan(const Type& rhs) const;
    virtual std::wstring toString() const;
    virtual FunctionType* getCanonicalType() const override;

public: // data member
    std::vector<Type*> parameter_types;
    Type* return_type;
    RecordType* class_type;

public: // basic facility
    virtual FunctionType* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

private:
};

} } }


#endif /* ZILLIANS_LANGUAGE_TREE_FUNCTIONTYPE_H_ */
