/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ANNOTATIONS_H_
#define ZILLIANS_LANGUAGE_TREE_ANNOTATIONS_H_

#include <ostream>
#include <memory>
#include <string>
#include <vector>
#include <initializer_list>

#include <boost/serialization/access.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"

namespace zillians { namespace language { namespace tree {

struct Annotation : public ASTNode
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Annotation, ASTNode);

    explicit Annotation(SimpleIdentifier* name);

    void setName(SimpleIdentifier* s);

    void appendKeyValue(SimpleIdentifier* key, ASTNode* value);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual Annotation* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    bool merge(Annotation& rhs);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (name          )
            (attribute_list)
        );
    }

    SimpleIdentifier* name;
    std::vector<std::pair<
                    SimpleIdentifier* /*key*/,
                    ASTNode* /*value*/>
                > attribute_list;

protected:
    Annotation() { }
};

struct Annotations : public ASTNode
{
    AST_NODE_HIERARCHY(Annotations, ASTNode);

             Annotations();
    explicit Annotations(std::initializer_list<Annotation*> annotation_list);

    void appendAnnotation(Annotation* annotation);
    void removeAnnotation(Annotation* annotation);

    bool        hasAnnotation(const std::wstring& tag) const;
    Annotation* findAnnotation(const std::wstring& tag) const;
    Annotation* findAnnotation(const std::vector<std::wstring>& path) const;
    Annotation* findAnnotation(std::initializer_list<std::wstring> path) const;
    ASTNode*    findAnnotationValue(const std::vector<std::wstring>& path) const;
    ASTNode*    findAnnotationValue(std::initializer_list<std::wstring> path) const;
    void        deleteAnnotation(const std::wstring& tag);

    virtual bool isEqualImpl(const ASTNode& rhs) const;

    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual Annotations* clone() const;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    bool merge(Annotations& rhs);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        AST_NODE_SERIALIZE(
            ar,
            (annotation_list)
        );
    }

    std::vector<Annotation*> annotation_list;
};

struct Annotatable : public ASTNode
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(Annotatable, ASTNode);

protected:
             Annotatable();
    explicit Annotatable(ASTNode& parent);

public:
    Annotations* getAnnotations() const noexcept;
    void         setAnnotations(Annotations* anns) noexcept;

    bool         hasAnnotation(const std::wstring& tag) const;
    Annotation* findAnnotation(const std::wstring& tag) const;
    Annotation* findAnnotation(const std::vector<std::wstring>& path) const;
    Annotation* findAnnotation(std::initializer_list<std::wstring> path) const;
    ASTNode*    findAnnotationValue(const std::vector<std::wstring>& path) const;
    ASTNode*    findAnnotationValue(std::initializer_list<std::wstring> path) const;
    void         addAnnotation(Annotation* anno);
    void         delAnnotation(const std::wstring& tag);

    virtual bool isEqualImpl(const ASTNode& rhs) const override;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true) override;

    virtual std::wostream& toSource(std::wostream& output, unsigned indent = 0) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        AST_NODE_SERIALIZE(
            ar,
            (annotations)
        );
    }

protected:
    Annotations* annotations;
};

} } }


#endif /* ZILLIANS_LANGUAGE_TREE_ANNOTATIONS_H_ */
