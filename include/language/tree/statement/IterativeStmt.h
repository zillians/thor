/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_ITERATIVESTMT_H_
#define ZILLIANS_LANGUAGE_TREE_ITERATIVESTMT_H_

#include "language/tree/statement/Statement.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/basic/Block.h"

namespace zillians { namespace language { namespace tree {

struct IterativeStmt : public Statement
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(IterativeStmt, Statement);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<Statement>(*this);
        ar & block;
    }

    virtual IterativeStmt* clone() const = 0;

    Block* block;

protected:
    IterativeStmt(Block* block = NULL);
};

struct ForStmt : public IterativeStmt
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(ForStmt, IterativeStmt);

    explicit ForStmt(ASTNode* init, Expression* cond, Expression* step, Block* block = NULL);
    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual ForStmt* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<IterativeStmt>(*this);
        ar & init;
        ar & cond;
        ar & step;
    }

    ASTNode* init;
    Expression* cond;
    Expression* step;

protected:
    ForStmt();
};

struct ForeachStmt : public IterativeStmt
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(ForeachStmt, IterativeStmt);

    explicit ForeachStmt(VariableDecl* var_decl, Expression* range, Block* block = NULL);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual ForeachStmt* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<IterativeStmt>(*this);
        ar & var_decl;
        ar & range;
    }

    VariableDecl* var_decl; // TODO semantic-check: it must be L-value expression or declarative statement
    Expression* range;

protected:
    ForeachStmt();
};

struct WhileStmt : public IterativeStmt
{
    friend class boost::serialization::access;

    AST_NODE_HIERARCHY(WhileStmt, IterativeStmt);

    struct Style
    {
        enum type
        {
            WHILE, DO_WHILE
        };

        static const wchar_t* toString(type t)
        {
            switch(t)
            {
            case WHILE: return L"while";
            case DO_WHILE: return L"do-while";
            default: UNREACHABLE_CODE(); return L"";
            }
        }
    };

    explicit WhileStmt(Style::type style, Expression* cond, Block* block = NULL);

    virtual bool isEqualImpl(const ASTNode& rhs) const;
    virtual bool replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent = true);

    virtual WhileStmt* clone() const;
    virtual std::wostream& toSource(std::wostream& output, unsigned indent/* = 0*/) const override; using BaseNode::toSource;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & boost::serialization::base_object<IterativeStmt>(*this);
        ar & (int&)style;
        ar & cond;
    }

    Style::type style;
    Expression* cond;

protected:
    WhileStmt();
};

} } }

#endif /* ZILLIANS_LANGUAGE_TREE_ITERATIVESTMT_H_ */
