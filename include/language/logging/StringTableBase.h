/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef ZILLIANS_LANGUAGE_STRINGTABLEBASE_H_
#define ZILLIANS_LANGUAGE_STRINGTABLEBASE_H_

#include <map>
#include <locale>
#include <string>
#include <utility>

#include "core/IntTypes.h"

#include "language/logging/LoggerWrapper.h"

namespace zillians { namespace language { namespace tree {

struct ASTNode;

} } }

namespace zillians { namespace language {

template<typename the_tag_type>
struct tagged_param
{
    using tag_type = the_tag_type;

    std::wstring value;
};

template<>
struct tagged_param<struct log_tag_optional_filename>
{
    using tag_type = log_tag_optional_filename;

    std::string value;
};

template<typename str_type>
inline tagged_param<struct log_tag_optional_filename> _optional_filename(str_type&& str)
{
    return {std::forward<str_type>(str)};
}

struct log_params_base
{
    std::string optional_filename;

    template<typename log_tag_type>
    bool set_param(tagged_param<log_tag_type             > &&     ) { assert(false && "unexpected parameter, should be refused by meta-programming!"); return false; }
    bool set_param(tagged_param<log_tag_optional_filename> &&param) { optional_filename = std::move(param.value); return true; }

protected:
    template<typename ...dummy_types>
    static void ignore_returned_values(dummy_types &&...) noexcept {}
};

class StringTableBase
{
public:
    void setLocale(std::locale& locale);

    const wchar_t* getLocaledMessage(uint32 id) const;

protected:
    std::string getLCMessage() const;

protected:
    std::locale                                             mLocale;
    std::map<uint32, std::map<std::string, const wchar_t*>> mTranslations;
};

class StringTable; // class should be generated

class LoggerBase
{
public:
    void setWrapper(LoggerWrapper* wrapper);
    void setStringTable(StringTable* stringTable);

protected:
    static std::pair<std::string, uint32> getSourcePosition(const tree::ASTNode* attach_point, const tree::ASTNode* node, const log_params_base& params_base);

protected:
    LoggerWrapper* mWrapper     = nullptr;
    StringTable*   mStringTable = nullptr;
};

} }

#endif /* ZILLIANS_LANGUAGE_STRINGTABLEBASE_H_ */
