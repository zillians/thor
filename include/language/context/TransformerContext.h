/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TRANSFORMERCONTEXT_H_
#define ZILLIANS_LANGUAGE_TRANSFORMERCONTEXT_H_

#include "language/context/ContextBase.h"
#include "language/tree/ASTNodeFactory.h"

namespace zillians { namespace language {

/**
 * SplitReferenceContext is used when AST is transformed and new nodes are created.
 * The newly created nodes will have this context pointing to the origin node where this node is split from
 */
struct SplitReferenceContext
{
    friend class boost::serialization::access;

    SplitReferenceContext() :ref(NULL)
    { }

    SplitReferenceContext(tree::ASTNode* ref) : ref(ref)
    { }

    static tree::ASTNode* get(const tree::ASTNode* node)
    {
        SplitReferenceContext* from = node->get<SplitReferenceContext>();
        if(from)
            return from->ref;
        else
            return NULL;
    }

    static void set(tree::ASTNode* node, tree::ASTNode* ref)
    {
        return node->set<SplitReferenceContext>(new SplitReferenceContext(ref));
    }

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & ref;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    {
        mapper & ref;
    }

    tree::ASTNode* ref;
};

struct SplitInverseReferenceContext
{
    friend class boost::serialization::access;

    SplitInverseReferenceContext()
    { }

    static SplitInverseReferenceContext* get(const tree::ASTNode* node)
    {
        return node->get<SplitInverseReferenceContext>();
    }

    static void set(tree::ASTNode* node, tree::ASTNode* ref)
    {
        SplitInverseReferenceContext* context = NULL;
        if ((context = node->get<SplitInverseReferenceContext>()) == NULL)
        {
            context = new SplitInverseReferenceContext();
            node->set<SplitInverseReferenceContext>(context);
        }
        context->refs.push_back(ref);
    }

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & refs;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    {
        for (auto& ref : refs)
            mapper & ref;
    }

    std::vector<tree::ASTNode*> refs;
};

struct ReplicatorCreatorContextTraits : ContextMultiItemTraitsBase<tree::FunctionDecl>
{
};

struct ReplicatorSerializerContextTraits : ContextMultiItemTraitsBase<tree::FunctionDecl>
{
};

struct ReplicatorDeserializerContextTraits : ContextMultiItemTraitsBase<tree::FunctionDecl>
{
};

typedef ContextBase<ReplicatorCreatorContextTraits> ReplicatorCreatorContext;
typedef ContextBase<ReplicatorSerializerContextTraits> ReplicatorSerializerContext;
typedef ContextBase<ReplicatorDeserializerContextTraits> ReplicatorDeserializerContext;

} }

#endif /* ZILLIANS_LANGUAGE_TRANSFORMERCONTEXT_H_ */
