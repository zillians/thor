/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_MANGLINGSTAGECONTEXT_H_
#define ZILLIANS_LANGUAGE_MANGLINGSTAGECONTEXT_H_

#include <set>
#include <utility>

#include <boost/optional.hpp>

#include "utility/UUIDUtil.h"

#include "language/tree/ASTNode.h"

namespace zillians { namespace language {

// this context will be generated and attached to ASTNode if necessary
struct NameManglingContext
{
    NameManglingContext()
    { }

    NameManglingContext(const std::string& name) : mangled_name(name)
    { }

    static NameManglingContext* get(const tree::ASTNode* node)
    {
        return node->get<NameManglingContext>();
    }

    static void set(tree::ASTNode* node, NameManglingContext* ctx)
    {
        node->set<NameManglingContext>(ctx);
    }

    template<typename Archive>
    void serialize(Archive& ar, unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & mangled_name;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    { }

    std::string mangled_name;

};

// there are three name mangling for static variable
struct StaticVariableManglingContext
{
    StaticVariableManglingContext()
    { }

    StaticVariableManglingContext(const std::string& name, const std::string& guard_name) : mangled_name(name), mangled_guard_name(guard_name)
    { }

    static StaticVariableManglingContext* get(const tree::ASTNode* node)
    {
        return node->get<StaticVariableManglingContext>();
    }

    static void set(tree::ASTNode* node, StaticVariableManglingContext* ctx)
    {
        node->set<StaticVariableManglingContext>(ctx);
    }

    template<typename Archive>
    void serialize(Archive& ar, unsigned int version)
    {
        UNUSED_ARGUMENT(version);

        ar & mangled_name;
        ar & mangled_guard_name;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    { }

    std::string mangled_name;				// mangling name of the static variable
    std::string mangled_guard_name;  		// mangling name of the guard variable
};

struct EnumDeclManglingContext
{
    EnumDeclManglingContext() : is_long_literal(false)
    { }

    EnumDeclManglingContext(bool is_long_literal) : is_long_literal(is_long_literal)
    { }

    static EnumDeclManglingContext* get(const tree::ASTNode* node)
    {
        return node->get<EnumDeclManglingContext>();
    }

    static void set(tree::ASTNode* node, EnumDeclManglingContext* ctx)
    {
        node->set<EnumDeclManglingContext>(ctx);
    }

    template<typename Archive>
    void serialize(Archive& ar, unsigned int)
    {
        ar & is_long_literal;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    { }

    bool is_long_literal;
};

struct EnumIdManglingContext
{
    EnumIdManglingContext() : value(0)
    { }

    EnumIdManglingContext(int64 v) : value(v)
    { }

    static EnumIdManglingContext* get(const tree::ASTNode* node)
    {
        return node->get<EnumIdManglingContext>();
    }

    static void set(tree::ASTNode* node, EnumIdManglingContext* ctx)
    {
        node->set<EnumIdManglingContext>(ctx);
    }

    template<typename Archive>
    void serialize(Archive& ar, unsigned int)
    {
        ar & value;
    }

    template<typename Mapper>
    void update_reference(Mapper& mapper)
    { }

    int64 value;
};

} }

#endif /* ZILLIANS_LANGUAGE_MANGLINGSTAGECONTEXT_H_ */
