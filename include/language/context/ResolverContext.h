/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVERCONTEXT_H_
#define ZILLIANS_LANGUAGE_RESOLVERCONTEXT_H_

#include <set>
#include <map>
#include <vector>

#include "language/context/ContextBase.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/UniqueName.h"

namespace zillians { namespace language {

struct ResolvedTemplateMapTraitsBase
{
    using   KeyTypeOfMap = std::vector<tree::relinkable_ptr<tree::Type>>;
    using ValueTypeOfMap = tree::relinkable_ptr<tree::Declaration>;

    using  ValueType = std::map<KeyTypeOfMap, ValueTypeOfMap>;
    using ResultType = std::map<KeyTypeOfMap, ValueTypeOfMap>*;

    static bool isValid(tree::Declaration* node);

    static ResultType getImpl(ValueType& value)
    {
        return &value;
    }

    static void setImpl(ValueType& value, tree::Declaration* node)
    {
        BOOST_ASSERT(node && "null pointer exception");

              auto key    = generateKey(*node);
        const auto result = value.emplace(std::move(key), tree::make_relinkable(node));

        BOOST_ASSERT(result.second && "duplicated instantiation!");
    }

    template<typename MapperType>
    static void update_reference(MapperType& mapper, ValueType& value)
    {
        for (auto& pair : value)
            detail::update_reference_relinkable(mapper, pair.second);
    }

    static std::vector<tree::relinkable_ptr<tree::Type>> generateKey(const std::vector<tree::Type*>& specializations);
    static std::vector<tree::relinkable_ptr<tree::Type>> generateKey(tree::Declaration&              decl);
};

struct InstantiatedAsTraits : ResolvedTemplateMapTraitsBase {};
struct SpecializedAsTraits : ResolvedTemplateMapTraitsBase {};

struct InstantiatedFromTraits : ContextRelinkableTraitsBase<tree::Declaration>
{
    static bool isValid(const ValueType&   ref);
    static bool isValid(tree::Declaration* ptr);
};

struct SpecializationOfTraits : ContextTraitsBase<tree::Declaration>
{
    static bool isValid(tree::Declaration* node);
};

struct ResolvedPackageTraits : ContextRelinkableTraitsBase<tree::Package>
{
    static bool isValid(const ValueType& ref);
    static bool isValid(tree::Package*   ptr);
};

struct ResolvedSymbolTraits : ContextRelinkableTraitsBase<tree::ASTNode>
{
    static bool isValid(const ValueType& ref);
    static bool isValid(tree::ASTNode*   ptr);
};

struct ResolvedTypeTraits
{
    typedef tree::ASTNode NodeType;

    static bool isValid(tree::ASTNode* node);
};

struct DefaultValueAsTraits : ContextMultiItemTraitsBase<tree::Expression, std::set<tree::Expression*>>
{
    static bool isValid(tree::Expression* node);
};

struct DefaultValueFromTraits : ContextRelinkableTraitsBase<tree::VariableDecl>
{
    static bool isValid(tree::VariableDecl* node);
};

typedef ContextBase<InstantiatedAsTraits> InstantiatedAs;
typedef ContextBase<SpecializedAsTraits> SpecializedAs;
typedef ContextBase<InstantiatedFromTraits> InstantiatedFrom;
typedef ContextBase<SpecializationOfTraits> SpecializationOf;
typedef ContextBase<ResolvedSymbolTraits> ResolvedSymbol;
typedef ContextBase<ResolvedPackageTraits> ResolvedPackage;
typedef ContextBase<DefaultValueAsTraits> DefaultValueAs;
typedef ContextBase<DefaultValueFromTraits> DefaultValueFrom;

struct ResolvedType
{
    static bool isValid(const tree::relinkable_ptr<tree::Type>& ref);

    static tree::Type* get(const tree::ASTNode* node);
    static void        set(      tree::ASTNode* node, tree::Type* ref );

    template<typename Mapper>
    static void update_reference(Mapper& mapper, tree::Type*& ref)
    {
        mapper & ref;
    }
};

} }

#endif /* ZILLIANS_LANGUAGE_RESOLVERCONTEXT_H_ */
