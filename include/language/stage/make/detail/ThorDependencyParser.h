/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_THORDEPENDENCYPARSER_H_
#define ZILLIANS_LANGUAGE_STAGE_THORDEPENDENCYPARSER_H_

#ifndef BOOST_SPIRIT_UNICODE
    #define BOOST_SPIRIT_UNICODE
#endif

#include <vector>
#include <string>

#include <boost/spirit/include/classic_position_iterator.hpp>

namespace zillians { namespace language { namespace stage {

/////////////////////////////////////////////////////////////////////
/// Package Dependency
/////////////////////////////////////////////////////////////////////

typedef boost::spirit::classic::position_iterator2<std::wstring::iterator> pos_iterator_type;

/// @note In order to reduce compile time, we expose only a non-template function here.
/// The implementation is in '../../../src/libzillians-language/language/stage/dep/ThorPackageDependencyGrammar.cpp'
bool getImportedPackages(pos_iterator_type begin, pos_iterator_type end, std::vector<std::wstring>& v);

} } }

#endif /*ZILLIANS_LANGUAGE_STAGE_THORDEPENDENCYPARSER_H_ */
