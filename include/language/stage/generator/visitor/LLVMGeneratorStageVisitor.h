/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEVISITOR_H_

#include <llvm/Config/config.h>

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/IR/InlineAsm.h>
#else
    #include <llvm/InlineAsm.h>
#endif

#include <llvm/Module.h>
#include <llvm/LLVMContext.h>
#include <llvm/Function.h>
#include <llvm/Value.h>
#include <llvm/Type.h>
#include <llvm/IRBuilder.h>
#include <llvm/BasicBlock.h>
#include <llvm/Instruction.h>
#include <llvm/Instructions.h>

#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * LLVMGeneratorVisitor walks the AST tree and generates LLVM instructions
 *
 * As LLVMGeneratorVisitor traverse the AST tree only once, required information must be prepared prior to the visitor,
 * including any necessary transformation.
 *
 * @see LLVMGeneratorPreambleVisitor
 */
struct LLVMGeneratorStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(generateInvoker, generate);

    LLVMGeneratorStageVisitor(llvm::LLVMContext& context, llvm::Module& module);

    void generate(tree::ASTNode& node);
    void generate(tree::NormalBlock& node);
    void generate(tree::AtomicBlock& node);
    void generate(tree::NumericLiteral& node);
    void generate(tree::ObjectLiteral& node);
    void generate(tree::StringLiteral& node);
    void generate(tree::TypeIdLiteral& node);
    void generate(tree::SymbolIdLiteral& node);
    void generate(tree::FunctionIdLiteral& node);
    void generate(tree::Tangle& node);
    void generate(tree::Source& node);
    void generate(tree::ClassDecl& node);
    void generate(tree::FunctionDecl& node);
    void generate(tree::VariableDecl& node);
    void generate(tree::DeclarativeStmt& node);
    void generate(tree::BranchStmt& node);
    void generate(tree::ExpressionStmt& node);
    void generate(tree::IfElseStmt& node);
    void generate(tree::ForStmt& node);
    void generate(tree::ForeachStmt& node);
    void generate(tree::WhileStmt& node);
    void generate(tree::SwitchStmt& node);
    void generate(tree::IdExpr& node);
    void generate(tree::UnaryExpr& node);
    void generate(tree::BinaryExpr& node);
    void generate(tree::TernaryExpr& node);
    void generate(tree::CallExpr& node);
    void generate(tree::CastExpr& node);
    void generate(tree::MemberExpr& node);
    void generate(tree::BlockExpr& node);
    void generate(tree::UnpackExpr& node);
    void generate(tree::SystemCallExpr& node);

private:
    llvm::Function* getRootsetAdder               ();
    llvm::Function* getAsyncInvocationAdder       ();
    llvm::Function* getAsyncInvocationReserver    ();
    llvm::Function* getAsyncInvocationCommiter    ();
    llvm::Function* getAsyncInvocationAborter     ();
    llvm::Function* getAsyncInvocationAppender    (const tree::Type& type);
    llvm::Function* getAsyncInvocationRetPtrSetter();

    llvm::Value* generateAsyncCallParamThis(const tree::FunctionDecl& callee, tree::Expression& callee_this_expr);
    llvm::Value* generateAsyncCallParamNonThis(tree::Expression& param_expr);
    llvm::Value* generateAsyncCallReturnPtrSet(llvm::Value* inv_id, tree::Expression* assignee_expr);

    template<typename RangeType>
    llvm::Value* generateAsyncCallParams(tree::SystemCallExpr& system_call, tree::Expression* assignee_expr, tree::Expression& func_id_expr, const RangeType& parameters);
    llvm::Value* generateAsyncCallNoParams(tree::SystemCallExpr& system_call, tree::Expression* assignee_expr, tree::Expression& func_id_expr);

private:
    struct ReadWriteValue
    {
        llvm::Value* read;
        llvm::Value* write;
    };

private:
    bool isSuperMember(tree::ASTNode& node);
    bool isStatic(tree::ASTNode* resolved_symbol);
    void specialFunctionHandling(tree::FunctionDecl& node);
    void callAddToGlobalRootSet(llvm::Value* llvm_value);
    llvm::Function* generateAddToGlobalRootSet();
    llvm::Function* generateGuardAcquire();
    llvm::Function* generateGuardRelease();
    llvm::Function* generateDynCastImpl();
    llvm::Value* loadThisPointer(tree::FunctionDecl& node);
    void callDefaultFunction(tree::FunctionDecl& from_function, tree::ClassDecl& to_class, std::wstring function_name);
    void callBaseDestructor(tree::FunctionDecl& dtor_node);
    void implementDeleteDestructor(tree::FunctionDecl& dtor_node);
    void callBaseConstructor(tree::FunctionDecl& ctor_node);
    void initializeVTT(tree::FunctionDecl& ctor_node);
    llvm::Value* getVirtualFunction(tree::ClassDecl& ast_class, tree::FunctionDecl& ast_function, llvm::Value* this_value);

    // this is for NVVM only, which has several built-in intrinsics to perform address space conversion
    llvm::Function* getAddrSpaceConversionIntrinsics(unsigned from, unsigned to, int point_to_bitsize = 8);
    llvm::BasicBlock* createBasicBlock(llvm::StringRef name = "", llvm::Function* parent = NULL, llvm::BasicBlock* before = NULL);
    void enterBasicBlock(llvm::BasicBlock* block, bool emit_branch_if_necessary = true);
    llvm::AllocaInst* createAlloca(tree::VariableDecl* node, llvm::Type* type, const llvm::Twine& name = "");
    bool createAlloca(tree::VariableDecl& ast_variable);
    bool startFunction(tree::FunctionDecl& ast_function);
    bool allocateParameters(tree::FunctionDecl& ast_function);
    bool finishFunction(tree::FunctionDecl& ast_function);
    void generateLocalVariable(tree::VariableDecl& var);

private:

    void shortCircuitEval(tree::BinaryExpr& node);
    llvm::BranchInst* createCondBr(llvm::Value* value, llvm::BasicBlock* true_block, llvm::BasicBlock* false_block);

private:
    typedef bool require_read;
    typedef bool require_write;

    llvm::Value* getReadValue(tree::ASTNode& attach, tree::ASTNode& node);
    llvm::Value* getWriteValue(tree::ASTNode& attach, tree::ASTNode& node);
    llvm::Value* getAtomicReadValue     (tree::ASTNode& node, llvm::Value* read_value);
    llvm::Value* getNonAssignedReadValue(tree::ASTNode& attach, tree::ASTNode& node, llvm::Value* read_value);
    llvm::Value* getAssignedReadValue   (tree::ASTNode& attach, tree::ASTNode& node, llvm::Value* read_value);
    llvm::Value* getNonVariableReadValue(tree::ASTNode& attach, tree::ASTNode& node, llvm::Value* read_value);

private:
    bool isFunctionParameter(tree::ASTNode& node);
    int getFunctionParameterIndex(tree::ASTNode& node);
    tree::FunctionDecl* getContainingFunction(tree::ASTNode& node);
    bool hasValue(tree::ASTNode& ast_node);
    bool isResolved(tree::ASTNode& ast_node);
    bool isFunctionVisited(tree::FunctionDecl& ast_function);
    bool isBlockInsertionMasked();
    void setBlockInsertionMask();
    void resetBlockInsertionMask();
    llvm::BasicBlock* currentBlock();
    bool isBlockTerminated(llvm::BasicBlock* block);

private:
    bool propagate(tree::ASTNode* to, tree::ASTNode* from);
    void blockInitialize();
    void blockFinalize();
    llvm::Function* genLLVMExternFunc(const std::string& name, llvm::Type* return_type, const std::vector<llvm::Type*>& param_types);
    void generateAtomicDataStructures();
    bool isBeAssigned(tree::Expression& node);

public:
    llvm::LLVMContext &mContext;
    llvm::Module& mModule;
    llvm::IRBuilder<> mBuilder;
    LLVMHelper mHelper;
    std::vector<llvm::Function*> mGlobalCtors;

    struct {
        llvm::Function* function;
        llvm::BasicBlock* entry_block;
        llvm::BasicBlock* return_block;
        llvm::BasicBlock* continue_block;
        llvm::BasicBlock* break_block;
        llvm::Instruction* alloca_insert_point;
        std::vector< llvm::Value* > return_values;
        bool mask_insertion;
    } mFunctionContext;

    // atomic block
    struct AtomicCtxS {
        AtomicCtxS()
            : tm_read(static_cast<int>(tree::PrimitiveKind::FLOAT64_TYPE) + 2)
            , tm_write(static_cast<int>(tree::PrimitiveKind::FLOAT64_TYPE) + 2)
        {}

        bool isInAtomicBlock() const { return inAtomicBlock; }
        void setInAtomicBlock() { inAtomicBlock = true; }
        void clearInAtomicBlock() { inAtomicBlock = false; }

        bool inAtomicBlock;
        llvm::AllocaInst* tx;
        llvm::Type*       ty_TxThread;
        llvm::Type*       ty_JmpBuf;

        llvm::Function*   sys_init;
        llvm::Function*   thread_init;
        llvm::Function*   thread_shutdown;
        llvm::Function*   sys_shutdown;
        llvm::Function*   tm_begin;
        llvm::Function*   tm_commit;

        llvm::Function*   a_setjmp;
        llvm::Value*      a_stm_selt;

        // if value of FLOAT64_TYPE is 8, the length of array must be 10, the last is for pointer
        std::vector<llvm::Function*>   tm_read;
        std::vector<llvm::Function*>   tm_write;

        const static size_t POINTER_TYPE = tree::PrimitiveKind::FLOAT64_TYPE + 1;

        llvm::Function* getFunctionT(tree::ASTNode* node, const std::vector<llvm::Function*>& tm_functions) {
            tree::Type* resolved_type = tree::ASTNodeHelper::getCanonicalType(node);
            if(tree::PrimitiveType* pri_type = resolved_type->getAsPrimitiveType()) {
                return tm_functions[pri_type->getKind()];
            }
            else if(tree::ClassDecl* cd = resolved_type->getAsClassDecl()) {
                return tm_functions[POINTER_TYPE];
            }
            else {
                UNREACHABLE_CODE();
                return nullptr;
            }
        }

        llvm::Function* getReadFunction(tree::ASTNode* node) {
            return getFunctionT(node, tm_read);
        }

        llvm::Function* getWriteFunction(tree::ASTNode* node) {
            return getFunctionT(node, tm_write);
        }
    };

    AtomicCtxS mAtomic;
};


} } } }


#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEVISITOR_H_ */
