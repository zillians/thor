/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_THORCPPGENERATORSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_THORCPPGENERATORSTAGEVISITOR_H_

#include <sstream>
#include <string>
#include <set>

#include "core/Visitor.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct ThorCPPGeneratorStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(mCPPGeneratoreVisitorInvoker, apply);

    ThorCPPGeneratorStageVisitor(std::ostringstream& oss) : _oss(oss)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(mCPPGeneratoreVisitorInvoker)
    }

    void apply(tree::ASTNode& node)
    {
        revisit(node);
    }

    void apply(tree::ClassDecl& node)
    {
        if(isNativeClass(node))
        {
            node.block = NULL;
        }
    }

private:
    bool static isFormalTemplateFunction(tree::FunctionDecl& node)
    {
        return isa<TemplatedIdentifier>(node.name) &&
               cast<TemplatedIdentifier>(node.name)->type == TemplatedIdentifier::Usage::FORMAL_PARAMETER ;
    }

    std::ostringstream& _oss;

    std::set<tree::ASTNode> _todo;
    std::set<tree::ASTNode> _done;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_THORCPPGENERATORSTAGEVISITOR_H_ */
