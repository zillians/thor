/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEUPDATESTRUCTSIZEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEUPDATESTRUCTSIZEVISITOR_H_

#include "utility/StringUtil.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * LLVMGeneratorStageUpdateStructSizeVisitor is used to update the new restructure size after having synthesized object layout context
 */
struct LLVMGeneratorStageUpdateStructSizeVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(updateInvoker, update);

    LLVMGeneratorStageUpdateStructSizeVisitor();

    void update(tree::ASTNode& node);
    void update(tree::ClassDecl& node);
    void update(tree::FunctionDecl& node);
    void update(tree::BlockExpr& node);
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORSTAGEUPDATESTRUCTSIZEVISITOR_H_ */
