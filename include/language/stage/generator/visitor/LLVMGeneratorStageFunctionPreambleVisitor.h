/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORFUNCTIONPREAMBLESTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORFUNCTIONPREAMBLESTAGEVISITOR_H_

#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/IRBuilder.h>

#include "core/IntTypes.h"
#include "utility/StringUtil.h"
#include "language/GlobalConstant.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"

namespace zillians { namespace language { namespace stage { namespace visitor {


struct LLVMGeneratorStageFunctionPreambleVisitor : public tree::visitor::GenericDoubleVisitor
{
    typedef std::pair<tree::ClassDecl*, uint64> traversed_node_t;

    CREATE_INVOKER(generateInvoker, generate);

    LLVMGeneratorStageFunctionPreambleVisitor(llvm::LLVMContext& context, llvm::Module& module);

    void generate(tree::ASTNode& node);
    void generate(tree::Tangle& node);
    void generate(tree::ClassDecl& node);
    void generate(tree::FunctionDecl& node);

private:
    llvm::LLVMContext &mContext;
    llvm::Module& mModule;
    llvm::IRBuilder<> mBuilder;
    LLVMHelper mHelper;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_LLVMGENERATORFUNCTIONPREAMBLESTAGEVISITOR_H_ */
