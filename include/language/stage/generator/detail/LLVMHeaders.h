/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_GENERATOR_DETAIL_LLVMHEADERS_H_
#define ZILLIANS_LANGUAGE_STAGE_GENERATOR_DETAIL_LLVMHEADERS_H_


// defined to eliminate #error in llvm/Support/DataTypes.h
#ifndef __STDC_LIMIT_MACROS
    #define __STDC_LIMIT_MACROS
#endif

#include <llvm/Config/config.h>

#include <llvm/ADT/APInt.h>
#include <llvm/PassManager.h>
#include <llvm/Analysis/Verifier.h>
#include <llvm/Assembly/PrintModulePass.h>
#include <llvm/Assembly/AssemblyAnnotationWriter.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/CFG.h>
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/DebugInfo.h>
#include <llvm/DIBuilder.h>

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3

    #include <llvm/IR/LLVMContext.h>
    #include <llvm/IR/Function.h>
    #include <llvm/IR/BasicBlock.h>
    #include <llvm/IR/Metadata.h>
    #include <llvm/IR/Module.h>
    #include <llvm/IR/CallingConv.h>
    #include <llvm/IR/Instructions.h>
    #include <llvm/IR/DataLayout.h>
    #include <llvm/IR/IRBuilder.h>
    #include <llvm/IR/GlobalVariable.h>
    #include <llvm/IRReader/IRReader.h>

#else

    #include <llvm/LLVMContext.h>
    #include <llvm/Function.h>
    #include <llvm/BasicBlock.h>
    #include <llvm/Metadata.h>
    #include <llvm/Module.h>
    #include <llvm/CallingConv.h>
    #include <llvm/Instructions.h>
    #include <llvm/DataLayout.h>
    #include <llvm/IRBuilder.h>
    #include <llvm/GlobalVariable.h>
    #include <llvm/Support/IRReader.h>

#endif

#endif /* ZILLIANS_LANGUAGE_STAGE_GENERATOR_DETAIL_LLVMHEADERS_H_ */
