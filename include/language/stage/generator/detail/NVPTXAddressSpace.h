/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_NVPTXADDRESS_SPACE_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_NVPTXADDRESS_SPACE_H_

namespace zillians { namespace language { namespace stage { namespace visitor {

enum NVPTXAddressSpace {
	NVPTX_AS_GENERIC = 0,
	NVPTX_AS_GLOBAL = 1,
	NVPTX_AS_CONST_NOT_GEN = 2,
	NVPTX_AS_SHARED = 3,
	NVPTX_AS_CONST = 4,
	NVPTX_AS_LOCAL = 5,
	NVPTX_AS_PARAM = 101
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_NVPTXADDRESS_SPACE_H_ */
