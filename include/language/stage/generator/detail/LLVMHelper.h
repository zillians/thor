/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_LLVMHELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_LLVMHELPER_H_

#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>

#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/IRBuilder.h>
#include <llvm/Type.h>
#include <llvm/DerivedTypes.h>

#include <boost/range/adaptor/indirected.hpp>

#include "utility/UnicodeUtil.h"

#include "language/context/ResolverContext.h"
#include "language/context/GeneratorContext.h"
#include "language/context/ManglingStageContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/ASTNodeHelper.h"

#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/context/SynthesizedTypeContext.h"
#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"
#include "language/stage/generator/detail/LLVMHeaders.h"
#include "language/stage/generator/detail/NVPTXAddressSpace.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct LLVMHelper
{
    LLVMHelper(llvm::LLVMContext& context, llvm::Module& module, llvm::IRBuilder<>& builder ) : mContext(context), mModule(module), mBuilder(builder)
    { }

    llvm::Type* getType(tree::Type& type)
    {
        if (const auto*const type_primitive = type.getAsPrimitiveType())
            return getType(type_primitive->getKind());

        if (auto*const func_type = type.getAsFunctionType())
            return getType(*func_type);

        if (auto*const multi_type = type.getAsMultiType())
            return getType(*multi_type);

        if (auto*const decl_cls = type.getAsClassDecl())
            return getType(*decl_cls);

        if (auto*const decl_enum = type.getAsEnumDecl())
            return getType(*decl_enum);

        UNREACHABLE_CODE();
        return nullptr;
    }

    llvm::Type* getType(tree::PrimitiveKind type)
    {
        using namespace language::tree;

        switch(type)
        {
        case PrimitiveKind::VOID_TYPE   : return llvm::Type::getVoidTy(mContext);
        case PrimitiveKind::BOOL_TYPE   : return llvm::IntegerType::getInt8Ty(mContext);
        case PrimitiveKind::INT8_TYPE   : return llvm::IntegerType::getInt8Ty(mContext);
        case PrimitiveKind::INT16_TYPE  : return llvm::IntegerType::getInt16Ty(mContext);
        case PrimitiveKind::INT32_TYPE  : return llvm::IntegerType::getInt32Ty(mContext);
        case PrimitiveKind::INT64_TYPE  : return llvm::IntegerType::getInt64Ty(mContext);
        case PrimitiveKind::FLOAT32_TYPE: return llvm::Type::getFloatTy(mContext);
        case PrimitiveKind::FLOAT64_TYPE: return llvm::Type::getDoubleTy(mContext);
        default                         : return nullptr;
        }
    }

    llvm::Type* getType(tree::FunctionType& func_type)
    {
        auto*const function_type = getFunctionType(func_type);
        BOOST_ASSERT(function_type != nullptr && "failed to get function type");

        return function_type->getPointerTo();
    }

    llvm::Type* getType(tree::MultiType& multi_type)
    {
        return getMultiType(multi_type);
    }

    llvm::Type* getType(tree::ClassDecl& cls_decl)
    {
        auto*const struct_type = getStructType(cls_decl, true);
        BOOST_ASSERT(struct_type != nullptr && "failed to get struct type");

        return struct_type->getPointerTo();
    }

    llvm::Type* getType(tree::EnumDecl& enum_decl)
    {
        if(EnumDeclManglingContext::get(&enum_decl)->is_long_literal)
            return getType(tree::PrimitiveKind::INT64_TYPE);
        else
            return getType(tree::PrimitiveKind::INT32_TYPE);
    }

    llvm::Type* getType(tree::TypeSpecifier& specifier)
    {
        auto*const type = specifier.getCanonicalType();
        BOOST_ASSERT(type != nullptr && "canonical type is not ready");

        auto*const llvm_type = getType(*type);

        if (llvm_type == nullptr)
            return nullptr;

        return llvm_type;
    }

    llvm::Constant* getTypeInitializer(tree::PrimitiveKind type)
    {
        using namespace language::tree;

        switch(type)
        {
        case PrimitiveKind::BOOL_TYPE   : return llvm::ConstantInt::get(llvm::IntegerType::get(mContext,  8), 0, false);
        case PrimitiveKind::INT8_TYPE   : return llvm::ConstantInt::get(llvm::IntegerType::get(mContext,  8), 0, false);
        case PrimitiveKind::INT16_TYPE  : return llvm::ConstantInt::get(llvm::IntegerType::get(mContext, 16), 0, false);
        case PrimitiveKind::INT32_TYPE  : return llvm::ConstantInt::get(llvm::IntegerType::get(mContext, 32), 0, false);
        case PrimitiveKind::INT64_TYPE  : return llvm::ConstantInt::get(llvm::IntegerType::get(mContext, 64), 0, false);
        case PrimitiveKind::FLOAT32_TYPE: return llvm::ConstantFP::get(llvm::Type::getFloatTy(mContext), 0);
        case PrimitiveKind::FLOAT64_TYPE: return llvm::ConstantFP::get(llvm::Type::getDoubleTy(mContext), 0);
        default                         : return nullptr;
        }
    }

    llvm::Constant* getTypeInitializer(tree::TypeSpecifier& specifier)
    {
        // TODO these are cases that shouldn't happen
        tree::Type* resolved_type = specifier.getCanonicalType();
        if(resolved_type)
        {
            if(resolved_type->isRecordType())
            {
                auto*const struct_type = getStructType(*resolved_type->getAsClassDecl(), true);

                BOOST_ASSERT(struct_type != nullptr && "failed to get struct type");

                return llvm::ConstantPointerNull::get(struct_type->getPointerTo());
            }
            else if(resolved_type->isEnumType())
            {
                return llvm::ConstantInt::get(llvm::IntegerType::get(mContext, 32), 0, false);
            }
            else if(resolved_type->isPrimitiveType())
            {
                return getTypeInitializer(resolved_type->getAsPrimitiveType()->getKind());
            }
            else
            {
                BOOST_ASSERT(false && "resolved to unknown type");
            }
        }
        else
        {
            BOOST_ASSERT(false && "resolved to unknown type");
        }

    	return nullptr;
    }

    llvm::StructType* getMultiType(tree::MultiType& multi_type)
    {
        // collect multiple types, and construct a equivalent composite llvm type
        std::vector<llvm::Type*> sub_llvm_types;

        for (auto*const sub_type : multi_type.types)
        {
            auto*const sub_llvm_type = getType(*sub_type);

            BOOST_ASSERT(sub_llvm_type != nullptr && "unknown sub type");

            sub_llvm_types.emplace_back(sub_llvm_type);
        }

        /// construct unnameed structure type here
        return llvm::StructType::create(
            mContext,
            sub_llvm_types,
            "",
            true /* packed, no padding */
        );
    }

    llvm::StructType* getStructType(tree::ClassDecl& ast_class, bool create_forward_decl_only = false)
    {
        llvm::StructType* llvm_struct_type = llvm::cast_or_null<llvm::StructType>(GET_SYNTHESIZED_LLVM_TYPE(&ast_class));

        if(llvm_struct_type == nullptr)
        {
            llvm_struct_type = llvm::StructType::create(mContext, LLVMHelper::encodeIdentifier(s_to_ws(NameManglingContext::get(&ast_class)->mangled_name)));

            if(!llvm_struct_type)
            {
                BOOST_ASSERT(false && "failed to create opaque llvm struct type");
                return nullptr;
            }

            SET_SYNTHESIZED_LLVM_TYPE(&ast_class, llvm_struct_type);
        }

        if(!create_forward_decl_only)
        {
            if(llvm_struct_type->isOpaque())
            {
                std::vector<llvm::Type*> member_types;
                bool has_virtual = false;
                SynthesizedObjectLayoutContext* object_layout = new SynthesizedObjectLayoutContext();
                if (!getStructTypeImpl(ast_class, member_types, object_layout, has_virtual)) return llvm_struct_type;

                llvm_struct_type->setBody(member_types, false);
                SET_SYNTHESIZED_OBJECTLAYOUT(&ast_class, object_layout);

                // Fill the remained information
                object_layout->object_size = getStructSizeInBytes(llvm_struct_type);

                for(auto& i : object_layout->class_offset)
                {
                	auto index = i.second.first;
                	i.second.second = getStructElementOffset(llvm_struct_type, index);
                }

                for(auto& i : object_layout->member_attributes)
                {
                	auto index = i.second.get<0>();
                	i.second.get<1>() = getStructElementOffset(llvm_struct_type, index);
                }
            }
        }

        return llvm_struct_type;
    }

    llvm::FunctionType* getFunctionType(tree::FunctionType& ast_function_type)
    {
        // prepare LLVM function return type
        auto*const llvm_function_return_type = getType(*ast_function_type.return_type);

        if (llvm_function_return_type == nullptr)
            return nullptr;

        // prepare LLVM function parameter type list
        std::vector<llvm::Type*> function_parameter_types;

        if (ast_function_type.class_type != nullptr)
        {
            auto*const t = getType(*ast_function_type.class_type);

            if (t == nullptr)
                return nullptr;

            function_parameter_types.emplace_back(t);
        }

        // append the rest of parameter types
        {
            for(auto* type : ast_function_type.parameter_types)
            {
                auto*const t = getType(*type);

                if (t == nullptr)
                    return nullptr;

                function_parameter_types.push_back(t);
            }
        }

        auto*const llvm_function_type = llvm::FunctionType::get(llvm_function_return_type, function_parameter_types, false /*not variadic*/);

        SET_SYNTHESIZED_LLVM_TYPE(&ast_function_type, llvm_function_type);

        return llvm_function_type;
    }

    llvm::FunctionType* getFunctionType(const tree::FunctionDecl& ast_function)
    {
        auto*const ast_type = ast_function.getCanonicalType();
        BOOST_ASSERT(ast_type != nullptr && "canonical type is not ready");

        auto*const llvm_type = getFunctionType(*ast_type);

        return llvm::cast_or_null<llvm::FunctionType>(llvm_type);
    }

    static llvm::BasicBlock* getPredecessorBlock(llvm::BasicBlock* block, int index)
    {
        int i=0;
        for (llvm::pred_iterator it = llvm::pred_begin(block), it_end = llvm::pred_end(block); it != it_end; ++it, ++i)
        {
            if(i == index)
                return *it;
        }
        return nullptr;
    }

    static std::string toAsciiNumber(char c)
    {
        static char buffer[4];
        snprintf(buffer, 3, "%d", c);
        return std::string(buffer);
    }

    static std::string encodeIdentifier(const std::wstring ucs4)
    {
        std::string ucs4_to_utf8_temp;
        std::string utf8_to_llvm_temp;

        // first we should covert UCS-4 to UTF-8
        ucs4_to_utf8(ucs4, ucs4_to_utf8_temp);

        // because LLVM only accept identifier of the form: '[%@][a-zA-Z$._][a-zA-Z$._0-9]*'
        // we have to convert illegal identifier into legal one
        for(std::string::const_iterator i = ucs4_to_utf8_temp.begin(), e = ucs4_to_utf8_temp.end(); i != e; ++i)
        {
            char c = *i;
            if( ((i == ucs4_to_utf8_temp.begin()) ? false : isdigit(c)) || isalpha(c) || (c == '_') || (c == '.') )
                utf8_to_llvm_temp.push_back(c);
            else
            {
                utf8_to_llvm_temp.push_back('$');
                utf8_to_llvm_temp.append(toAsciiNumber(c));
                utf8_to_llvm_temp.push_back('$');
            }
        }

        return utf8_to_llvm_temp;
    }

    uint64 getStructElementOffset(tree::ClassDecl& node, uint32 index)
    {
        llvm::StructType* llvm_struct_type = getStructType(node);
        return getStructElementOffset(llvm_struct_type, index);
    }

    uint64 getStructElementOffset(llvm::StructType* struct_type, uint32 index)
    {
    	if ( index >= struct_type->getNumElements())
    	{
    		// Happens in empty class
    		return 0;
    	}

        // Get the struct layout from target data layout
    	llvm::DataLayout target_data_layout(mModule.getDataLayout());

        const llvm::StructLayout* layout = target_data_layout.getStructLayout(struct_type);
        return layout->getElementOffset(index);
    }

    uint64 getStructSizeInBytes(llvm::StructType* struct_type)
    {
        llvm::DataLayout target_data_layout(mModule.getDataLayout());
        const llvm::StructLayout* layout = target_data_layout.getStructLayout(struct_type);
        return layout->getSizeInBytes();
    }

	uint64 getTypeStoreSize(llvm::Type *Ty)
	{
        llvm::DataLayout target_data_layout(mModule.getDataLayout());
        return target_data_layout.getTypeStoreSize(Ty);
	}

    uint32 getPointerSizeInBits()
    {
        llvm::DataLayout target_data_layout(mModule.getDataLayout());
        return target_data_layout.getPointerSizeInBits();
    }

    uint32 getPointerABIAlignment()
    {
        llvm::DataLayout target_data_layout(mModule.getDataLayout());
        return target_data_layout.getPointerABIAlignment();
    }

    uint64 getTypeSizeInBits(llvm::Type* type)
    {
        llvm::DataLayout target_data_layout(mModule.getDataLayout());
        return target_data_layout.getTypeAllocSizeInBits(type);
    }

    uint32 getTypeAlignmentInBits(llvm::Type* type)
    {
        llvm::DataLayout target_data_layout(mModule.getDataLayout());
        return target_data_layout.getABITypeAlignment(type) * 8;
    }

    int64 getClassHierarchyOffset(tree::ClassDecl* source_class, tree::ClassDecl* target_class)
    {
        bool negative = false;
        tree::ClassDecl* derived_class = source_class;
        tree::ClassDecl* base_class = target_class;
        SynthesizedObjectLayoutContext* object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(derived_class);
        if (object_layout->class_offset.count(base_class) == 0)
        {
            // Well, the source class is the parent of target class, denote it with negative
            derived_class = target_class;
            base_class = source_class;
            negative = true;
        }
        object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(derived_class);
        BOOST_ASSERT(object_layout->class_offset.count(base_class) && "Invalid pointer adjustment");

        auto*const derived_struct_type = getStructType(*derived_class);
        auto*const base_struct_type    = getStructType(*base_class);

        int64 offset_in_bytes = 0;
        if (base_struct_type->getNumElements())
        {
            // Well, offset_index == getNumElements happened when the target structs is empty. This is because the
            // design of the offset_index calculation. It always points to previous size.
            offset_in_bytes = object_layout->class_offset[base_class].second;
            if (negative)
                offset_in_bytes *= -1;
        }
        return offset_in_bytes;
    }

    llvm::Value* adjustPointer(llvm::Value* source_pointer, tree::ClassDecl* source_class, tree::ClassDecl* target_class)
    {
        llvm::StructType* target_struct_type = getStructType(*target_class);

        int64 offset_in_bytes = getClassHierarchyOffset(source_class, target_class);

        // Now, we need to bitcast source_pointer to i8*, and add the offset
        llvm::PointerType* int8PtrTy = llvm::Type::getInt8PtrTy(mContext);
        llvm::Value* cast_pointer = mBuilder.CreateBitCast(source_pointer, int8PtrTy);
        llvm::Value* target_pointer = mBuilder.CreateConstGEP1_64(cast_pointer, offset_in_bytes);

        // Cast back to target class type
        return mBuilder.CreateBitCast(target_pointer, target_struct_type->getPointerTo());
    }

    std::string getGlobalOffsetNameBase(const UUID& tangle_id)
    {
        return "__" + tangle_id.toString('_');
    }

    std::string getGlobalOffsetNameOfType(const UUID& tangle_id)
    {
        return getGlobalOffsetNameBase(tangle_id) + "_tid";
    }

    std::string getGlobalOffsetNameOfFunction(const UUID& tangle_id)
    {
        return getGlobalOffsetNameBase(tangle_id) + "_fid";
    }

    std::string getGlobalOffsetNameOfSymbol(const UUID& tangle_id)
    {
        return getGlobalOffsetNameBase(tangle_id) + "_sid";
    }

    llvm::Value* get_type_id(tree::ClassDecl& node)
    {
        const auto*const tangle = node.getOwner<tree::Tangle>();
        BOOST_ASSERT(tangle && "invalid AST structure");

        const auto& find_id_result = tangle->getIdOfType(node);
        const auto& offseted_id    = find_id_result.first;
        const auto& is_found       = find_id_result.second;
        BOOST_ASSERT(is_found && "type not found! there must be something wrong...");

        const auto& tangle_id = offseted_id.first;
        const auto& offset    = offseted_id.second;

        auto*const global_offset_type = llvm::Type::getInt64Ty(mContext);
        auto*const global_offset_var  = mModule.getOrInsertGlobal(getGlobalOffsetNameOfType(tangle_id), global_offset_type);

        llvm::Value* adjusted_type_id = mBuilder.CreateAdd(
            mBuilder.CreateLoad(global_offset_var),
            llvm::ConstantInt::getSigned(llvm::Type::getInt64Ty(mContext), offset)
        );

        return adjusted_type_id;
    }

    template<typename T>
    static bool isCurrentCodeGenerationTarget(T* node)
    {
        const Architecture& arch = node->arch.is_zero() ? Architecture::default_ : node->arch;

        return arch.is_any(getGeneratorContext().active_config->arch);
    }

private:
    bool appendMemberType(tree::ClassDecl& ast_class, std::vector<llvm::Type*>& member_types, std::map<const tree::VariableDecl*, SynthesizedObjectLayoutContext::MEMBER_ATTRIBUTES>& member_attributes)
    {
        using namespace language::tree;

        if(isa<TemplatedIdentifier>(ast_class.name) && !cast<TemplatedIdentifier>(ast_class.name)->isFullySpecialized())
            return true;

        for(auto* decl : ast_class.member_variables)
        {
        	// skip static data member
        	if (decl->is_static) continue;

            Type* resolved_type = cast<VariableDecl>(decl)->type->getCanonicalType();
            BOOST_ASSERT(resolved_type && "failed to find resolved type while generating LLVM struct for ClassDecl node");
            if(PrimitiveType* pri_type = resolved_type->getAsPrimitiveType())
            {
                llvm::Type* t = getType(pri_type->getKind());

                if (t == nullptr)
                    return false;

                member_types.push_back(t);
            }
            else if(FunctionType* func_type = resolved_type->getAsFunctionType())
            {
                llvm::Type* t = GET_SYNTHESIZED_LLVM_TYPE(func_type);
                // TODO the lambda function should not be passed as function pointer...need improvement here
                BOOST_ASSERT(false && "function type is not yet unsupported as structure member type");
                member_types.push_back(llvm::PointerType::getUnqual(t));
            }
            else if(ClassDecl* class_decl = resolved_type->getAsClassDecl())
            {
                llvm::Type* t = GET_SYNTHESIZED_LLVM_TYPE(class_decl);
                member_types.push_back(llvm::PointerType::getUnqual(t));
            }
            else if(EnumDecl* enum_decl = resolved_type->getAsEnumDecl())
            {
                llvm::Type* t = getType(*enum_decl);

                if (t == nullptr)
                    return false;

                member_types.push_back(t);
            }
            else
            {
                BOOST_ASSERT(false && "unsupported structure member type");
                return false;
            }

            BOOST_ASSERT(member_attributes.count(decl) == 0);

            SynthesizedObjectLayoutContext::OFFSET_IN_INDEX offset_index = member_types.size() - 1;
            SynthesizedObjectLayoutContext::SIZE_IN_BYTES size_in_bytes = getTypeStoreSize(member_types[offset_index]);

            member_attributes.insert(std::make_pair(decl, boost::make_tuple(offset_index, 0 /* calculate later*/, size_in_bytes)));
        }
        return true;
    }

    bool getStructTypeImpl(tree::ClassDecl& ast_class, std::vector<llvm::Type*>& member_types, SynthesizedObjectLayoutContext* object_layout, bool& has_virtual)
    {
        // Remember the current offset for the class
        BOOST_ASSERT( object_layout->class_offset.count(&ast_class) == 0 && "Uh... visited twice!?" );

      	SynthesizedObjectLayoutContext::OFFSET_IN_INDEX index = 0;
        if (!tree::ASTNodeHelper::isDynamicClass(ast_class) && has_virtual)
        {
            // Well, if the class is not dynamic class, and there exists virtual functions in derived classes, we need to shift one place,
            // since if not, the original place will be placed a vptr which is not good for non-dynamic class.
        	index = member_types.size() + 1;
        }
        else
        {
        	index = member_types.size();
        }
        object_layout->class_offset.insert(std::make_pair(&ast_class, std::make_pair(index, 0 /* calculate later */)));

        // check if I have virtual functions
        if (!has_virtual)
        {
            has_virtual = hasVirtualFunction(ast_class);
        }

        // keep tracing
        std::list<tree::ClassDecl*> bases = tree::ASTNodeHelper::getDirectBases(ast_class);

        using std::begin;
        using std::end;
        // modify the bases list, so that the first one is surely has virtual function
        auto found = std::find_if(
            begin(bases), end(bases),
            [](decltype(bases)::const_reference base){
                return tree::ASTNodeHelper::isDynamicClass(*base);
            }
        );

        if (found != end(bases))
        {
            auto *first_dynamic_class = *found;
            bases.erase(found);
            bases.push_front(first_dynamic_class);
        }

        for (auto* base : bases)
        {
            if (!getStructTypeImpl(*base, member_types, object_layout, has_virtual)) return false;
        }

        // Insert data members
        if (has_virtual)
        {
            member_types.push_back(llvm::Type::getInt8PtrTy(mContext));
            object_layout->vptr_index.push_back(member_types.size() - 1) ;
            has_virtual = false;
        }

        if (!appendMemberType(ast_class, member_types, object_layout->member_attributes)) return false;

        return true;
    }

    bool hasVirtualFunction(tree::ClassDecl& ast_class)
    {
        for(auto* method : ast_class.member_functions)
        {
            if (method->is_virtual || ast_class.is_interface)
            {
                return true;
            }
        }
        return false;
    }

private:
    llvm::LLVMContext& mContext;
    llvm::Module& mModule;
    llvm::IRBuilder<>& mBuilder;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_LLVMHELPER_H_ */
