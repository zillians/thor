/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVIRTUALFUNCTIONCONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVIRTUALFUNCTIONCONTEXT_H_

#include "language/tree/ASTNodeFactory.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

struct SynthesizedVirtualFunctionContext
{
	SynthesizedVirtualFunctionContext() : llvm_this_pointer(NULL), llvm_virtual_function(NULL)
	{ }

    SynthesizedVirtualFunctionContext(llvm::Value* llvm_this_pointer, llvm::Value* llvm_virtual_function) : llvm_this_pointer(llvm_this_pointer), llvm_virtual_function(llvm_virtual_function)
    { }

    static SynthesizedVirtualFunctionContext* get(tree::ASTNode* node)
    {
        return node->get<SynthesizedVirtualFunctionContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedVirtualFunctionContext* ctx)
    {
        node->set<SynthesizedVirtualFunctionContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedVirtualFunctionContext>();
    }

    llvm::Value* llvm_this_pointer;
    llvm::Value* llvm_virtual_function;
};

#define GET_SYNTHESIZED_VIRTUAL_FUNCTION(x) \
    ((SynthesizedVirtualFunctionContext::get((x))) ? SynthesizedVirtualFunctionContext::get((x)) : NULL)

#define SET_SYNTHESIZED_VIRTUAL_FUNCTION(x, v1, v2)  \
    { \
        if(SynthesizedVirtualFunctionContext::get(x)) \
        { \
            SynthesizedVirtualFunctionContext::get((x))->llvm_this_pointer = v1; \
            SynthesizedVirtualFunctionContext::get((x))->llvm_virtual_function = v2; \
        } \
        else \
            SynthesizedVirtualFunctionContext::set(x, new SynthesizedVirtualFunctionContext(v1, v2)); \
    }

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVIRTUALFUNCTIONCONTEXT_H_ */
