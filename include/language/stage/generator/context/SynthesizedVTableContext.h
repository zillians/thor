/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVTABLECONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVTABLECONTEXT_H_

#include <vector>

#include "core/IntTypes.h"

#include "language/tree/ASTNode.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

struct SynthesizedVTableContext
{
	SynthesizedVTableContext() : virtual_table(NULL)
	{ }

    SynthesizedVTableContext(llvm::GlobalVariable* virtual_table, const std::vector<uint32>& subtable_start)
        : virtual_table(virtual_table), subtable_start(subtable_start)
    { }

    static SynthesizedVTableContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedVTableContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedVTableContext* ctx)
    {
        node->set<SynthesizedVTableContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
    	node->reset<SynthesizedVTableContext>();
    }

    // The virtual table is a constant array
    llvm::GlobalVariable* virtual_table;

    // The start index of each subtable in virtual table
    std::vector<uint32> subtable_start;
};


#define GET_SYNTHESIZED_VTABLE(x) \
    ((SynthesizedVTableContext::get(x)) ? SynthesizedVTableContext::get(x) : NULL)

#define SET_SYNTHESIZED_VTABLE(x, v1, v2) \
    { \
        if(SynthesizedVTableContext::get(x)) \
        { \
            SynthesizedVTableContext::get((x))->virtual_table = v1; \
            SynthesizedVTableContext::get((x))->subtable_start = v2; \
        } \
        else \
            SynthesizedVTableContext::set(x, new SynthesizedVTableContext(v1, v2)); \
    }

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDVTABLECONTEXT_H_ */
