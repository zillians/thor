/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDFUNCTIONCONTEXT_H_
#define ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDFUNCTIONCONTEXT_H_

#include "language/tree/ASTNodeFactory.h"
#include "language/stage/generator/detail/LLVMHeaders.h"

namespace zillians { namespace language { namespace stage {

struct SynthesizedFunctionContext
{
    SynthesizedFunctionContext() : f(NULL)
    { }

    SynthesizedFunctionContext(llvm::Function* f) : f(f)
    { }

    static SynthesizedFunctionContext* get(const tree::ASTNode* node)
    {
        return node->get<SynthesizedFunctionContext>();
    }

    static void set(tree::ASTNode* node, SynthesizedFunctionContext* ctx)
    {
        node->set<SynthesizedFunctionContext>(ctx);
    }

    static void reset(tree::ASTNode* node)
    {
        node->reset<SynthesizedFunctionContext>();
    }

    llvm::Function* f;
};

#define GET_SYNTHESIZED_LLVM_FUNCTION(x) \
    ((zillians::language::stage::SynthesizedFunctionContext::get((x))) ? zillians::language::stage::SynthesizedFunctionContext::get((x))->f : NULL)

#define SET_SYNTHESIZED_LLVM_FUNCTION(x, func)  \
    { \
        if(zillians::language::stage::SynthesizedFunctionContext::get(x)) \
            zillians::language::stage::SynthesizedFunctionContext::get((x))->f = func; \
        else \
            zillians::language::stage::SynthesizedFunctionContext::set(x, new zillians::language::stage::SynthesizedFunctionContext(func)); \
    }

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_SYNTHESIZEDFUNCTIONCONTEXT_H_ */
