/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_CXXINFOCOLLECTOR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_CXXINFOCOLLECTOR_H_

#include <map>
#include <memory>

#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>

#include "language/stage/import/TSInfo.h"

namespace zillians { namespace language { namespace stage { namespace import {

class CXXInfoCollector
{
public:
    explicit CXXInfoCollector(const clang::ASTContext& ast_context);

    bool enter_ns(const clang::NamespaceDecl& ns);
    void leave_ns(const clang::NamespaceDecl& ns);

    bool enter_cls(const clang::CXXRecordDecl& cls);
    void leave_cls(const clang::CXXRecordDecl& cls);

    bool register_field(const clang::FieldDecl& field);
    bool register_function(const clang::FunctionDecl& function);
    bool register_typedef(const clang::TypedefNameDecl& type_def);
    bool register_enum(const clang::EnumDecl& e);
    bool register_union(const clang::CXXRecordDecl& u);

    ts_package* steal_root();

    bool has_unhandled_anonymous_type_name_request() const noexcept;

private:
    const clang::ASTContext* ast_context;

    ts_package* root;

    std::map<const clang::CXXRecordDecl*, ts_class*   > cxx_ts_class_map;
    std::map<const clang::FunctionDecl* , ts_function*> cxx_ts_function_map;
    std::map<const clang::FieldDecl*    , ts_getset*  > cxx_ts_getset_map;
    std::set<ts_class*>                                 anonymous_type_name_requests;

};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_CXXINFOCOLLECTOR_H_ */

