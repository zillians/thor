/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_CLASS_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_CLASS_GRAMMAR_H_

#include <vector>

#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxGetSetGrammar.h"
#include "language/stage/import/grammar/CxxFunctionGrammar.h"
#include "language/stage/import/grammar/CxxPimplGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct cxx_class_fwd_grammar : public boost::spirit::karma::grammar<iterator, ts_class*()>
{
    cxx_class_fwd_grammar();

    boost::spirit::karma::rule<iterator, ts_class*()>                   start;

    boost::spirit::karma::rule<iterator, ts_class*()>                   cls;
    identifier_grammar<iterator>                                        identifier;
};

template<typename iterator>
struct cxx_class_decl_grammar : public boost::spirit::karma::grammar<iterator, ts_class*()>
{
    cxx_class_decl_grammar();

    boost::spirit::karma::rule<iterator, ts_class*()>                   start;

    boost::spirit::karma::rule<iterator, ts_class*()>                   cls;

    boost::spirit::karma::rule<iterator, std::vector<ts_function*>()>   functions;
    boost::spirit::karma::rule<iterator, ts_function*()>                function;
    cxx_function_decl_grammar<iterator>                                 function_impl;

    cxx_getset_decl_grammar<iterator>                                   getset;

    boost::spirit::karma::rule<iterator, ts_class*()>                   pimpl;
    cxx_pimpl_getter_decl_grammar<iterator>                             pimpl_getter;
    cxx_pimpl_setter_decl_grammar<iterator>                             pimpl_setter;
    cxx_pimpl_creator_decl_grammar<iterator>                            pimpl_creator;
    boost::spirit::karma::rule<iterator, ts_class*()>                   pimpl_variables;

    identifier_grammar<iterator>                                        identifier;
};

template<typename iterator>
struct cxx_class_def_grammar : public boost::spirit::karma::grammar<iterator, ts_class*()>
{
    cxx_class_def_grammar();

    boost::spirit::karma::rule<iterator, ts_class*()>   start;

    boost::spirit::karma::rule<iterator, ts_class*()>   cls;

    cxx_function_def_grammar<iterator>                  function;

    cxx_getset_def_grammar<iterator>                    getset;

    cxx_pimpl_getter_def_grammar<iterator>              pimpl_getter;
    cxx_pimpl_setter_def_grammar<iterator>              pimpl_setter;
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_class_fwd_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_class_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_class_def_grammar)

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_CLASS_GRAMMAR_H_ */
