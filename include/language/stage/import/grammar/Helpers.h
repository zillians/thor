/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_HELPERS_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_HELPERS_H_

#include <string>
#include <type_traits>
#include <vector>

#include <boost/fusion/container/vector/vector.hpp>
#include <boost/spirit/include/phoenix_function.hpp>
#include <boost/variant.hpp>

#include "language/stage/import/TSInfo.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace helpers {

struct replace_non_alnum_impl
{
    template<typename arg_type>
    struct result
    {
        typedef std::string type;
    };

    template<typename arg_type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        typename result<arg_type>::type result = arg;

        for(char& c: result)
        {
            if( !('a' <= c && c <= 'z') &&
                !('A' <= c && c <= 'Z') &&
                !('0' <= c && c <= '9'))
            {
                c = '_';
            }
        }

        return result;
    }
};

struct cxx_name_impl
{
    template<typename arg_type>
    struct result
    {
        typedef std::string type;
    };

    template<typename arg_type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return to_string(arg);
    }

    static std::string to_string(ts_primitive primitive);
    static std::string to_string(ts_builtin builtin);
};

struct ts_name_impl
{
    template<typename arg_type>
    struct result
    {
        typedef std::string type;
    };

    template<typename arg_type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return to_string(arg);
    }

    static std::string to_string(ts_primitive primitive);
    static std::string to_string(ts_builtin builtin);
};

struct ts_type_is_none_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        // FIXME add compile time assertion to warn the change of type variants (ts_type)
        return arg.which() == 0;
    }
};

struct ts_type_is_primitive_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        // FIXME add compile time assertion to warn the change of type variants (ts_type)
        return arg.which() == 1;
    }
};

struct ts_type_is_builtin_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        // FIXME add compile time assertion to warn the change of type variants (ts_type)
        return arg.which() == 2;
    }
};

struct ts_type_is_class_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        // FIXME add compile time assertion to warn the change of type variants (ts_type)
        return arg.which() == 3;
    }
};

struct ts_type_get_primitive_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef ts_primitive type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return boost::get<ts_primitive>(arg);
    }
};

struct ts_type_get_builtin_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef ts_builtin type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return boost::get<ts_builtin>(arg);
    }
};

struct ts_type_get_class_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    struct result
    {
        typedef ts_class* type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_type, arg_type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return boost::get<ts_class*>(arg);
    }
};

struct ts_identifier_name_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_identifier, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::string& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_identifier, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type arg) const
    {
        return arg->name;
    }
};

struct ts_decl_scope_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef ts_package* type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type arg) const
    {
        return arg->scope;
    }
};

struct ts_decl_member_of_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef ts_class* type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->member_of;
    }
};

struct ts_decl_id_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef ts_identifier* type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type arg) const
    {
        return arg->id;
    }
};

struct ts_decl_wrapped_name_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::string& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type arg) const
    {
        return arg->wrapped_name;
    }
};

struct ts_decl_get_package_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef ts_package* type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_decl, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type arg) const
    {
        return arg->get_package();
    }
};

struct ts_function_result_type_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const ts_type& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->result_type;
    }
};

template<ts_function::wrap_method method>
struct ts_function_is_result_wrapping_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->result_wrapping == method;
    }
};

struct ts_function_parameters_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::vector<ts_parameter*>& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->parameters;
    }
};

struct ts_function_is_constructor_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->is_constructor();
    }
};

struct ts_function_is_destructor_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_function, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->is_destructor();
    }
};

template<bool(ts_getset::*condition)() const noexcept>
struct ts_getset_condition_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_getset, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_getset, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const noexcept
    {
        return (arg->*condition)();
    }
};

struct ts_getset_type_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_getset, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const ts_type& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_getset, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->type;
    }
};

struct ts_class_constructors_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_class, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::vector<ts_function*>& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_class, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->constructors;
    }
};

struct ts_class_destructors_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_class, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::vector<ts_function*>& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_class, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->destructors;
    }
};

struct ts_class_getsets_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_class, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::vector<ts_getset*>& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_class, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->getsets;
    }
};

struct ts_parameter_type_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_parameter, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const ts_type& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_parameter, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->type;
    }
};

template<ts_parameter::wrap_method method>
struct ts_parameter_is_wrapping_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_parameter, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_parameter, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->wrapping == method;
    }
};

struct ts_package_declarations_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const decltype(ts_package::declarations)& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->declarations;
    }
};

struct ts_package_refered_packages_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef std::vector<ts_package*> type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return typename result<arg_type>::type(arg->refered_packages.begin(), arg->refered_packages.end());
    }
};

struct ts_package_refered_package_pairs_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef std::vector<boost::fusion::vector<ts_package*, ts_package*>> type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        const auto& refered_packages = arg->refered_packages;

        typename result<arg_type>::type result;

        result.reserve(refered_packages.size());

        for(ts_package* refered_package: refered_packages)
        {
            result.emplace_back(
                arg,
                refered_package
            );
        }

        return result;
    }
};

template<ts_builtin builtin>
struct ts_package_has_used_builtins_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef bool type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->used_builtins.count(builtin) != 0;
    }
};

struct ts_package_depth_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef unsigned type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg) const
    {
        return arg->depth;
    }
};

struct ts_package_get_common_parent_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef ts_package* type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg1, const arg_type& arg2) const
    {
        return &arg1->get_common_parent(*arg2);
    }
};

struct ts_package_get_wrapped_includes_impl
{
    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    struct result
    {
        typedef const std::vector<std::string>& type;
    };

    template<typename arg_type, typename = typename std::enable_if<std::is_base_of<ts_package, typename std::remove_pointer<arg_type>::type>::value>::type>
    typename result<arg_type>::type operator()(const arg_type& arg1) const
    {
        return arg1->get_wrapped_includes();
    }
};

extern boost::phoenix::function<replace_non_alnum_impl>                                            replace_non_alnum;
extern boost::phoenix::function<cxx_name_impl>                                                     cxx_name;
extern boost::phoenix::function<ts_name_impl>                                                      ts_name;
extern boost::phoenix::function<ts_type_is_none_impl>                                              ts_type_is_none;
extern boost::phoenix::function<ts_type_is_primitive_impl>                                         ts_type_is_primitive;
extern boost::phoenix::function<ts_type_is_builtin_impl>                                           ts_type_is_builtin;
extern boost::phoenix::function<ts_type_is_class_impl>                                             ts_type_is_class;
extern boost::phoenix::function<ts_type_get_primitive_impl>                                        ts_type_get_primitive;
extern boost::phoenix::function<ts_type_get_builtin_impl>                                          ts_type_get_builtin;
extern boost::phoenix::function<ts_type_get_class_impl>                                            ts_type_get_class;
extern boost::phoenix::function<ts_identifier_name_impl>                                           ts_identifier_name;
extern boost::phoenix::function<ts_decl_scope_impl>                                                ts_decl_scope;
extern boost::phoenix::function<ts_decl_member_of_impl>                                            ts_decl_member_of;
extern boost::phoenix::function<ts_decl_id_impl>                                                   ts_decl_id;
extern boost::phoenix::function<ts_decl_wrapped_name_impl>                                         ts_decl_wrapped_name;
extern boost::phoenix::function<ts_decl_get_package_impl>                                          ts_decl_get_package;
extern boost::phoenix::function<ts_function_result_type_impl>                                      ts_function_result_type;
extern boost::phoenix::function<ts_function_is_result_wrapping_impl<ts_function::BY_PASS>>         ts_function_is_result_wrapping_by_pass;
extern boost::phoenix::function<ts_function_is_result_wrapping_impl<ts_function::COPY_CREATE>>     ts_function_is_result_wrapping_copy_create;
extern boost::phoenix::function<ts_function_parameters_impl>                                       ts_function_parameters;
extern boost::phoenix::function<ts_function_is_constructor_impl>                                   ts_function_is_constructor;
extern boost::phoenix::function<ts_function_is_destructor_impl>                                    ts_function_is_destructor;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter              >>    ts_getset_has_getter;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_by_pass      >>    ts_getset_has_getter_by_pass;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_address_of   >>    ts_getset_has_getter_address_of;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_array_element>>    ts_getset_has_getter_array_element;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_array_size   >>    ts_getset_has_getter_array_size;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter            >>      ts_getset_has_setter;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter_by_pass    >>      ts_getset_has_setter_by_pass;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter_copy_assign>>      ts_getset_has_setter_copy_assign;
extern boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter_array_element>>    ts_getset_has_setter_array_element;
extern boost::phoenix::function<ts_getset_type_impl>                                               ts_getset_type;
extern boost::phoenix::function<ts_class_constructors_impl>                                        ts_class_constructors;
extern boost::phoenix::function<ts_class_destructors_impl>                                         ts_class_destructors;
extern boost::phoenix::function<ts_class_getsets_impl>                                             ts_class_getsets;
extern boost::phoenix::function<ts_parameter_type_impl>                                            ts_parameter_type;
extern boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::BY_PASS  >>            ts_parameter_is_wrapping_by_pass;
extern boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::VALUE    >>            ts_parameter_is_wrapping_value;
extern boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::REFERENCE>>            ts_parameter_is_wrapping_reference;
extern boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::POINTER  >>            ts_parameter_is_wrapping_pointer;
extern boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::C_STRING >>            ts_parameter_is_wrapping_c_string;
extern boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::C_WSTRING>>            ts_parameter_is_wrapping_c_wstring;
extern boost::phoenix::function<ts_package_declarations_impl>                                      ts_package_declarations;
extern boost::phoenix::function<ts_package_refered_packages_impl>                                  ts_package_refered_packages;
extern boost::phoenix::function<ts_package_refered_package_pairs_impl>                             ts_package_refered_package_pairs;
extern boost::phoenix::function<ts_package_has_used_builtins_impl<ts_builtin::C_STRING>>           ts_package_has_used_builtins_c_string;
extern boost::phoenix::function<ts_package_depth_impl>                                             ts_package_depth;
extern boost::phoenix::function<ts_package_get_common_parent_impl>                                 ts_package_get_common_parent;
extern boost::phoenix::function<ts_package_get_wrapped_includes_impl>                              ts_package_get_wrapped_includes;

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_HELPERS_H_ */
