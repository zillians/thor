/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_INCLUDE_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_INCLUDE_GRAMMAR_H_

#include <string>
#include <vector>

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct cxx_include_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_package*, ts_package*>&()>
{
    typedef boost::fusion::vector<ts_package*, ts_package*> pair_type;

    cxx_include_grammar(const std::string& header_name);

    std::string                                        header_name;

    boost::spirit::karma::rule<iterator, pair_type&()> start;

    boost::spirit::karma::rule<iterator, pair_type&()> include;

    boost::spirit::karma::rule<iterator, pair_type&(), boost::spirit::karma::locals<ts_package*>>   relative_path;
    boost::spirit::karma::rule<iterator, unsigned(), boost::spirit::karma::locals<unsigned>>        relative_backward;
    boost::spirit::karma::rule<iterator, pair_type&()>                                              relative_forward;

    identifier_grammar<iterator> identifier;
};

template<typename iterator>
struct cxx_refers_grammar : public boost::spirit::karma::grammar<iterator, ts_package*()>
{
    cxx_refers_grammar(const std::string& header_name);

    boost::spirit::karma::rule<iterator, ts_package*()>   start;

    boost::spirit::karma::rule<iterator, ts_package*()>                                                     refers;
    boost::spirit::karma::rule<iterator, std::vector<boost::fusion::vector<ts_package*, ts_package*>>&()>   refers_impl;

    cxx_include_grammar<iterator>                         include;
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_include_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_refers_grammar )

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_INCLUDE_GRAMMAR_H_ */
