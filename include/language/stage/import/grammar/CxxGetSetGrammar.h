/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_GETSET_GRAMMAR_H_
#define ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_GETSET_GRAMMAR_H_

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma_grammar.hpp>
#include <boost/spirit/include/karma_rule.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxOptionalScopeGrammar.h"
#include "language/stage/import/grammar/CxxTypeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
struct cxx_getter_array_size_decl_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_getset*, bool>()>
{
    cxx_getter_array_size_decl_grammar();

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_getset*, bool>()>  start;

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_getset*, bool>()>  size;

    cxx_optional_scope_grammar<iterator>                                             optinal_scope;
};

template<typename iterator>
struct cxx_getter_array_size_def_grammar : public boost::spirit::karma::grammar<iterator, ts_getset*()>
{
    cxx_getter_array_size_def_grammar();

    boost::spirit::karma::rule<iterator, ts_getset*()>  start;

    boost::spirit::karma::rule<iterator, ts_getset*()>  size;

    cxx_getter_array_size_decl_grammar<iterator>        size_decl;
    boost::spirit::karma::rule<iterator, ts_getset*()>  size_body;

    boost::spirit::karma::rule<iterator, ts_getset*()>  size_expression;
    boost::spirit::karma::rule<iterator, ts_getset*()>  size_array;
};

template<typename iterator>
struct cxx_getter_decl_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_getset*, bool>()>
{
    cxx_getter_decl_grammar();

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_getset*, bool>()>  start;

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_getset*, bool>()>  getter;

    boost::spirit::karma::rule<iterator, ts_getset*()>                               extra_arguments;
    boost::spirit::karma::rule<iterator, ts_getset*()>                               extra_arguments_array_element;

    cxx_optional_scope_grammar<iterator>                                             optinal_scope;
    cxx_type_grammar<iterator>                                                       type;
};

template<typename iterator>
struct cxx_getter_def_grammar : public boost::spirit::karma::grammar<iterator, ts_getset*()>
{
    cxx_getter_def_grammar();

    boost::spirit::karma::rule<iterator, ts_getset*()>  start;

    boost::spirit::karma::rule<iterator, ts_getset*()>  getter;

    boost::spirit::karma::rule<iterator, ts_getset*()>  getter_field;

    cxx_getter_decl_grammar<iterator>                   getter_decl;
    boost::spirit::karma::rule<iterator, ts_getset*()>  getter_body;

    boost::spirit::karma::rule<iterator, ts_getset*()>  getter_body_impl_by_pass;
    boost::spirit::karma::rule<iterator, ts_getset*()>  getter_body_impl_address_of;

    cxx_type_grammar<iterator>                          type;
};

template<typename iterator>
struct cxx_setter_decl_grammar : public boost::spirit::karma::grammar<iterator, boost::fusion::vector<ts_getset*, bool>()>
{
    cxx_setter_decl_grammar();

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_getset*, bool>()>  start;

    boost::spirit::karma::rule<iterator, boost::fusion::vector<ts_getset*, bool>()>  setter;

    boost::spirit::karma::rule<iterator, ts_getset*()>                               extra_arguments;
    boost::spirit::karma::rule<iterator, ts_getset*()>                               extra_arguments_array_element;

    cxx_optional_scope_grammar<iterator>                                             optinal_scope;
    cxx_type_grammar<iterator>                                                       type;
};

template<typename iterator>
struct cxx_setter_def_grammar : public boost::spirit::karma::grammar<iterator, ts_getset*()>
{
    cxx_setter_def_grammar();

    boost::spirit::karma::rule<iterator, ts_getset*()>  start;

    boost::spirit::karma::rule<iterator, ts_getset*()>  setter;

    boost::spirit::karma::rule<iterator, ts_getset*()>  setter_field;
    boost::spirit::karma::rule<iterator, ts_getset*()>  setter_param;

    cxx_setter_decl_grammar<iterator>                   setter_decl;
    boost::spirit::karma::rule<iterator, ts_getset*()>  setter_body;

    boost::spirit::karma::rule<iterator, ts_getset*()>  setter_body_impl_by_pass;
    boost::spirit::karma::rule<iterator, ts_getset*()>  setter_body_impl_copy_assign;
};

template<typename iterator>
struct cxx_getset_decl_grammar : public boost::spirit::karma::grammar<iterator, ts_getset*()>
{
    cxx_getset_decl_grammar();

    boost::spirit::karma::rule<iterator, ts_getset*()>  start;

    boost::spirit::karma::rule<iterator, ts_getset*()>  getset;

    cxx_getter_decl_grammar<iterator>                   getter;
    cxx_setter_decl_grammar<iterator>                   setter;

    cxx_getter_array_size_decl_grammar<iterator>        getter_array_size;
};

template<typename iterator>
struct cxx_getset_def_grammar : public boost::spirit::karma::grammar<iterator, ts_getset*()>
{
    cxx_getset_def_grammar();

    boost::spirit::karma::rule<iterator, ts_getset*()>  start;

    boost::spirit::karma::rule<iterator, ts_getset*()>  getset;

    cxx_getter_def_grammar<iterator>                    getter;
    cxx_setter_def_grammar<iterator>                    setter;

    cxx_getter_array_size_def_grammar<iterator>         getter_array_size;
};

TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_getter_array_size_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_getter_array_size_def_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_getter_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_getter_def_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_setter_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_setter_def_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_getset_decl_grammar)
TS_IMPORT_GRAMMAR_EXTERN_TEMPLATES(cxx_getset_def_grammar)

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_IMPORT_GRAMMAR_CXX_GETSET_GRAMMAR_H_ */
