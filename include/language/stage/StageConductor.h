/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_STAGECONDUCTOR_H_
#define ZILLIANS_LANGUAGE_STAGE_STAGECONDUCTOR_H_

#include <vector>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>

#include "core/SharedPtr.h"

#include "language/stage/Stage.h"

namespace zillians { namespace language { namespace stage {

class StageConductor
{
public:
    StageConductor(bool require_input);
    virtual ~StageConductor();

public:
    void appendStage(shared_ptr<Stage> s);
    void appendOptionsFromAllStages(po::options_description& options_desc_public, po::options_description& options_desc_private);

public:
    virtual int main(int argc, const char** argv);

protected:
    po::options_description mOptionDescGlobal;
    po::positional_options_description mPositionalOptionDesc;
    std::vector<shared_ptr<Stage>> mStages;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_STAGECONDUCTOR_H_ */
