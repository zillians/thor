/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR2_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR2_H_

#include "language/tree/ASTNode.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

// CHECKS IN SEMANTIC VERIFICATION STAGE 2

// ERRORS:
// ====================================
// TASK_CANNOT_BE_CALLED_DIRECTLY

// WARNINGS:
// ====================================
// (NONE)

namespace zillians { namespace language { namespace stage { namespace visitor {

struct SemanticVerificationStageVisitor2 : public tree::visitor::GenericDoubleVisitor
{
public:
    CREATE_INVOKER(verifyInvoker, verify);

    SemanticVerificationStageVisitor2();

    void verify(tree::ASTNode& node);
    void verify(tree::ClassDecl& node);
    void verify(tree::FunctionDecl& node);
    void verify(tree::CallExpr& node);
    void verify(tree::AsyncBlock& node);

private:
    void verifyNotTaskCall(tree::CallExpr& node);

public:
    void applyCleanup();

private:
    std::vector<std::function<void()>> cleanup;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR2_H_ */

