/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR0_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR0_H_

#include <string>
#include <functional>
#include <unordered_set>
#include <vector>

#include "language/context/ConfigurationContext.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ParserContext.h"
#include "language/stage/verifier/context/SemanticVerificationContext.h"
#include "language/stage/verifier/detail/NameShadowVerifier.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

// CHECKS IN SEMANTIC VERIFICATION STAGE 0

// ERRORS:
// ====================================
// INCOMPLETE_FUNC
// DUPE_NAME
// WRITE_RVALUE
// MISSING_STATIC_INIT
// MISSING_BREAK_TARGET
// MISSING_CONTINUE_TARGET
// MISSING_PARAM_INIT
// UNEXPECTED_VARIADIC_PARAM
// UNEXPECTED_VARIADIC_TEMPLATE_PARAM
// UNEXPECTED_VARIADIC_TEMPLATE_ARG
// EXCEED_PARAM_LIMIT
// EXCEED_TEMPLATE_PARAM_LIMIT

// WARNINGS:
// ====================================
// DEAD_CODE

namespace zillians { namespace language { namespace stage { namespace visitor {

struct SemanticVerificationStageVisitor0 : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(verifyInvoker, verify);

    SemanticVerificationStageVisitor0();

    void verify(tree::ASTNode& node);
    void verify(tree::Source& node);
    void verify(tree::Import& node);
    void verify(tree::Package& node);
    void verify(tree::FunctionSpecifier& node);
    void verify(tree::Annotation& node);
    void verify(tree::UnaryExpr& node);
    void verify(tree::BranchStmt& node);
    void verify(tree::ClassDecl& node);
    void verify(tree::VariableDecl& node);
    void verify(tree::FunctionDecl& node);
    void verify(tree::SimpleIdentifier& node);
    void verify(tree::Block& node);
    void verify(tree::TypenameDecl& node);
    void verify(tree::EnumDecl& node);

public:
    void applyCleanup();

private:
    void verifyImportedPackageIsValid(tree::Import& node);
    void verifyHasValidAnnotation(tree::Annotation& node);
    void verifyVariableHasInit(tree::VariableDecl& node);
    void verifyTypeSpecifierNotVoid(tree::TypeSpecifier& node);
    void verifyVariableHasValidType(tree::VariableDecl& node);
    void verifyIsValidEntryFunction(tree::FunctionDecl& node);
    void verifyFunctionTypeParamsValid(tree::FunctionSpecifier& node);
    void verifyFunctionTypeReturnValid(tree::FunctionSpecifier& node);
    void verifyValidNewExpression(tree::UnaryExpr& node);
    void verifyValidOperatorOverloading(tree::FunctionDecl& node);
    void verifyLambdaSuperCall(tree::FunctionDecl& node);
    void verifyNonMemberCtorDtor(tree::FunctionDecl& node);
    void verifyMissingReturnType(tree::FunctionDecl& node);
    void verifyParameter(tree::FunctionDecl& node);
    void verifyFunctionLinkage(tree::FunctionDecl& node);
    void verifyBreakOrContinueTarget(tree::BranchStmt& node);
    void verifyTemplateMemberFunction(tree::FunctionDecl& node);
    void verifyFlowStatement(tree::Statement& node);
    void verifyValidCtorOrDtor(tree::FunctionDecl& node);
    void verifyDefaultTemplateArgumentOrder(const tree::TemplatedIdentifier* identifier);
    void verifyCtorDtorMustNotBeStatic(tree::FunctionDecl& node);
    void verifyCtorMustNotBeVirtual(tree::FunctionDecl& node);
    void verifyDtorMustAcceptNoParameter(tree::FunctionDecl& node);

private:
    void verifyIsConfictWithAlternativeKeywords(tree::ASTNode& node, std::wstring name);
    void verifyPackageConfictWithAlternativeKeywords(tree::Package& node);

private:
    std::vector<std::function<void()>> cleanup;
    uint64 errorCount_;
    uint64 warnCount_;

private:
    NameShadowVerifier nameShadowVerifier;
    std::unordered_set<std::wstring> alternative_keywords;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_SEMANTICVERIFICATIONSTAGEVISITOR0_H_ */
