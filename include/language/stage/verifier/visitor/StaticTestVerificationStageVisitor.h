/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_TREE_VISITOR_ERRORMESSAGEANNOTATIONCHECKVISITOR_H_
#define ZILLIANS_LANGUAGE_TREE_VISITOR_ERRORMESSAGEANNOTATIONCHECKVISITOR_H_

#include <string>

#include "core/Visitor.h"
#include "language/context/LogInfoContext.h"
#include "language/context/ResolverContext.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/parser/context/SourceInfoContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct StaticTestVerificationStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(errorMessageAnnotationCheckInvoker, check);

    StaticTestVerificationStageVisitor() : mAllMatch(true)
    {
        REGISTER_ALL_VISITABLE_ASTNODE(errorMessageAnnotationCheckInvoker);
    }

    bool isAllMatch() const
    {
        return mAllMatch;
    }

    void check(tree::ASTNode& node)
    {
        revisit(node);
    }

    void check(tree::Annotation& node)
    {
        return;
    }

    void check(tree::FunctionDecl& node)
    {
        staticTest(node);
        tree::TemplatedIdentifier* tid = tree::cast<tree::TemplatedIdentifier>(node.name);
        if(tid != nullptr && !tid->isFullySpecialized())
            return;

        revisit(node);
    }

    void check(tree::Statement& node)
    {
        staticTest(node);
        revisit(node); // NOTE: not sure if needed (lambda???)
    }

    void check(tree::Declaration& node)
    {
        staticTest(node);
        revisit(node);
    }

    void check(tree::Package& node)
    {
        staticTest(node);
        revisit(node);
    }

private:

    bool underCompleted(tree::ASTNode& node)
    {
        if(tree::FunctionDecl* f = node.getOwner<tree::FunctionDecl>()) {
            if(!f->isCompleted()) {
                return false;
            }
        }
        if(tree::ClassDecl* c = node.getOwner<tree::ClassDecl>()) {
            if(!c->isCompleted()) {
                return false;
            }
        }
        return true;
    }

    template<typename NodeType>
    void staticTest(NodeType& node)
    {
        // skip if in non-fully specialized function
        if(!underCompleted(node))
        {
            return;
        }

        // construct anno LogInfos
        std::vector<LogInfo> annotatedLogInfoVec = constructLogInfoVecFromAnnotations(node.getAnnotations());

        // get hooked LogInfos
        std::vector<LogInfo> hookedLogInfoVec;
        if(tree::ASTNode* inst_node = InstantiatedFrom::get(&node))
            hookedLogInfoVec = LogInfoContext::get(inst_node) ? LogInfoContext::get(inst_node)->log_infos : std::vector<LogInfo>();
        else
            hookedLogInfoVec = LogInfoContext::get(&node) ? LogInfoContext::get(&node)->log_infos : std::vector<LogInfo>();

        auto logInfoLessCompare = [](const LogInfo& lhs, const LogInfo& rhs) -> bool {
            if(int level_compare_result = lhs.log_level.compare(rhs.log_level))
            {
                return level_compare_result < 0;
            }
            else
            {
                if(int id_compare_result = lhs.log_id.compare(rhs.log_id))
                {
                    return id_compare_result < 0;
                }
                else
                {
                    return lhs.parameters < rhs.parameters;
                }
            }
        };

        std::sort(annotatedLogInfoVec.begin(), annotatedLogInfoVec.end(), logInfoLessCompare);
        std::sort(hookedLogInfoVec.begin(), hookedLogInfoVec.end(), logInfoLessCompare);

        // compare
        if(!compareLogInfoVec(&node, annotatedLogInfoVec, hookedLogInfoVec))
        {
            mAllMatch = false;
        }

        // check resolution symbol
        if(!checkResolutionSymbol(node))
        {
            mAllMatch = false;
        }

        // check resolution type
        if(!checkResolutionType(node))
        {
            mAllMatch = false;
        }

        // check SpecializationOf
        if(!checkSpecializationOf(node))
        {
            mAllMatch = false;
        }

        // check type inferred type
        if(!checkInferredType(node))
        {
            mAllMatch = false;
        }

        // check constant folding result
        if(!checkConstantFolding(node))
        {
            mAllMatch = false;
        }
    }

    std::vector<LogInfo> constructLogInfoVecFromAnnotations(tree::Annotations* annos);
    bool compareLogInfoVec(tree::ASTNode* errorNode, const std::vector<LogInfo>& annotatedLogInfoVec, const std::vector<LogInfo>& hookedLogInfoVec);
    bool checkResolutionSymbol(tree::ASTNode& node);
    bool checkResolutionType(tree::ASTNode& node);
    bool checkInferredType(tree::ASTNode& node);
    bool checkSpecializationOf(tree::ASTNode& node);
    bool checkConstantFolding(tree::ASTNode& node);

private:
    bool mAllMatch;
};

} } } } // namespace zillians::language::tree::visitor

#endif /* ZILLIANS_LANGUAGE_TREE_VISITOR_ERRORMESSAGEANNOTATIONCHECKVISITOR_H_ */
