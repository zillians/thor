/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_ARCHITECTUREVERIFIER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_ARCHITECTUREVERIFIER_H_

#include <stack>
#include <functional>

#include "utility/Functional.h"
#include "language/Architecture.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

class ArchitectureVerifier
{
public:
    ArchitectureVerifier();

    language::Architecture getParentArchitecture() const;

    void reportErrorIfCrossArchReference(const tree::Expression&) const;

    void reportErrorIfAny(const tree::Declaration&) const;
    void reportErrorIfArchDiffFromParent(const tree::Declaration&) const;
    void reportErrorIfArchDiffFromProject(const tree::Declaration&) const;
    void reportErrorIfCrossArchReference(const tree::Declaration&, const tree::TypeSpecifier&) const;

    void verifyArchOfBaseAndInterfaces(const tree::ClassDecl&) const;

    class AutoArchPusher
    {
    public:
        AutoArchPusher(ArchitectureVerifier&, const language::Architecture&);
        ~AutoArchPusher();

    private:
        ArchitectureVerifier& verifier;
    };

    friend class AutoArchPusher;

private:
    bool isArchDiffFromParent(const tree::Expression&) const;
    bool isArchDiffFromParent(const tree::Declaration&) const;
    bool isArchDiffFromProject(const tree::Declaration&) const;

    language::Architecture             project_architecture;
    std::stack<language::Architecture> parent_archs;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_ARCHITECTUREVERIFIER_H_ */
