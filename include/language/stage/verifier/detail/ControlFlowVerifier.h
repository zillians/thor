/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_CONTROLFLOWVERIFIER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_CONTROLFLOWVERIFIER_H_

#include "language/logging/StringTable.h"
#include "language/stage/verifier/context/SemanticVerificationContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

template<bool ContinueOnReturn, typename Derived>
struct ControlFlowVerifier
{
    typedef ControlFlowVerifier BaseType;

    static bool verifyStmtImpl(tree::Statement& stmt, const bool previous_stmt_returned);
    static bool verifyIfElseImpl(tree::IfElseStmt& if_else_stmt);
    static bool verifySwitchImpl(tree::SwitchStmt& switch_stmt);
    static bool verifyExprImpl(tree::ExpressionStmt& expr_stmt);

    static bool verifyBlockImpl(tree::Block& block);
    static bool verifyFuncDeclImpl(tree::FunctionDecl& func);
};

struct DeadCodeVerifier : ControlFlowVerifier<true, DeadCodeVerifier>
{
    static bool verifyStmtImpl(tree::Statement& stmt, const bool previous_stmt_returned);
};

struct AllPathReturnVerifier : ControlFlowVerifier<false, AllPathReturnVerifier>
{
    static bool verifyFuncDeclImpl(tree::FunctionDecl& func);
};

template<bool ContinueOnReturn, typename Derived>
inline bool ControlFlowVerifier<ContinueOnReturn, Derived>::verifyStmtImpl(tree::Statement& stmt,
                                                                           const bool /*previous_stmt_returned*/)
{
    using namespace language::tree;

    // true!!!!!!
    if(BranchStmt* branch_stmt = cast<BranchStmt>(&stmt))
        return branch_stmt->opcode == BranchStmt::OpCode::RETURN;

    // if-else
    else if(IfElseStmt* if_else_stmt = cast<IfElseStmt>(&stmt))
        return Derived::verifyIfElseImpl(*if_else_stmt);

    // switch
    else if(SwitchStmt* switch_stmt = cast<SwitchStmt>(&stmt))
        return Derived::verifySwitchImpl(*switch_stmt);

    // expression-stmt (for blocks)
    else if(ExpressionStmt* expr_stmt = cast<ExpressionStmt>(&stmt))
        return Derived::verifyExprImpl(*expr_stmt);

    else
        return false;
}

template<bool ContinueOnReturn, typename Derived>
inline bool ControlFlowVerifier<ContinueOnReturn, Derived>::verifyIfElseImpl(tree::IfElseStmt& if_else_stmt)
{
    bool has_return = true;

    if(if_else_stmt.else_block == NULL || !Derived::verifyBlockImpl(*if_else_stmt.else_block))
    {
        has_return = false;

        if(!ContinueOnReturn)
            return false;
    }

    if(!Derived::verifyBlockImpl(*if_else_stmt.if_branch->block))
    {
        has_return = false;

        if(!ContinueOnReturn)
            return false;
    }

    for(tree::Selection* elseif_branch: if_else_stmt.elseif_branches)
    {
        if(!Derived::verifyBlockImpl(*elseif_branch->block))
        {
            has_return = false;

            if(!ContinueOnReturn)
                return false;
        }
    }

    return has_return;
}

template<bool ContinueOnReturn, typename Derived>
inline bool ControlFlowVerifier<ContinueOnReturn, Derived>::verifySwitchImpl(tree::SwitchStmt& switch_stmt)
{
    bool has_return = true;

    if(switch_stmt.default_block == NULL || !Derived::verifyBlockImpl(*switch_stmt.default_block))
    {
        has_return = false;

        if(!ContinueOnReturn)
            return false;
    }

    for(tree::Selection* case_: switch_stmt.cases)
    {
        if(!Derived::verifyBlockImpl(*case_->block))
        {
            has_return = false;

            if(!ContinueOnReturn)
                return false;
        }
    }

    return has_return;
}

template<bool ContinueOnReturn, typename Derived>
inline bool ControlFlowVerifier<ContinueOnReturn, Derived>::verifyExprImpl(tree::ExpressionStmt& expr_stmt)
{
    if(auto block_expr = tree::cast<tree::BlockExpr>(expr_stmt.expr))
        return Derived::verifyBlockImpl(*block_expr->block);
    else
        return false;
}

template<bool ContinueOnReturn, typename Derived>
inline bool ControlFlowVerifier<ContinueOnReturn, Derived>::verifyBlockImpl(tree::Block& block)
{
    bool has_return = false;

    for(auto* stmt : block.objects)
    {
        if(Derived::verifyStmtImpl(*stmt, has_return))
        {
            has_return = true;

            if(!ContinueOnReturn)
                return true;
        }
    }

    return has_return;
}

template<bool ContinueOnReturn, typename Derived>
inline bool ControlFlowVerifier<ContinueOnReturn, Derived>::verifyFuncDeclImpl(tree::FunctionDecl& func)
{
    if(func.block == NULL)
        return true;

    return Derived::verifyBlockImpl(*func.block);
}

inline bool DeadCodeVerifier::verifyStmtImpl(tree::Statement& stmt, const bool previous_stmt_returned)
{
    if(previous_stmt_returned)
        LOG_MESSAGE(DEAD_CODE, &stmt);

    return BaseType::verifyStmtImpl(stmt, previous_stmt_returned);
}

inline bool AllPathReturnVerifier::verifyFuncDeclImpl(tree::FunctionDecl& func)
{
    if(func.type == NULL)
        return true;

    const auto*const type_primitive = tree::cast<tree::PrimitiveSpecifier>(func.type);

    if (type_primitive != nullptr && type_primitive->getKind() == tree::PrimitiveKind::VOID_TYPE)
        return true;

    return BaseType::verifyFuncDeclImpl(func);
}

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_CONTROLFLOWVERIFIER_H_ */
