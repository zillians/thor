/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_TYPE_CACHER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_TYPE_CACHER_H_

#include <unordered_map>

#include "language/tree/UniqueName.h"

namespace zillians { namespace language {

namespace tree {

struct Declaration;
struct FunctionType;
struct Internal;
struct MultiType;
struct Package;
struct Type;

}

class unique_name_cacher
{
public:
    explicit unique_name_cacher(tree::Internal& internal);

    tree::Declaration* lookup_decl   (const tree::unique_name_t& unique_name);
    tree::Package*     lookup_package(const tree::unique_name_t& unique_name);
    tree::Type*        lookup_type   (const tree::unique_name_t& unique_name);

    bool register_decl   (tree::Declaration& decl);
    bool register_package(tree::Package&     pkg );
    bool register_type   (tree::Type&        type);

private:
    tree::FunctionType* create_type_function(const tree::unique_name_t& unique_name);
    tree::MultiType*    create_type_multi   (const tree::unique_name_t& unique_name);

private:
    tree::Internal*                                             internal;
    std::unordered_map<tree::unique_name_t, tree::Declaration*> mapping_decl;
    std::unordered_map<tree::unique_name_t, tree::Package*    > mapping_pkg ;
    std::unordered_map<tree::unique_name_t, tree::Type*       > mapping_type;
};

} }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_TYPE_CACHER_H_ */
