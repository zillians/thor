/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_TREE_CONTEXT_REFACTORER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_TREE_CONTEXT_REFACTORER_H_

#include <deque>
#include <map>
#include <utility>

#include <boost/serialization/access.hpp>

namespace zillians { namespace language { namespace tree {

struct Declaration;
struct Tangle;

} } }  // namespace zillians::language::tree

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace detail {

class bundled_ast
{
    friend class boost::serialization::access;

public:
    using grouped_conflict_decl_map_type = std::map<
                                               std::string,
                                               std::deque<tree::Declaration*>
                                           >;

private:
    bundled_ast();
    bundled_ast(tree::Tangle* tangle, const grouped_conflict_decl_map_type&  grouped_may_conflict_decls);
    bundled_ast(tree::Tangle* tangle,       grouped_conflict_decl_map_type&& grouped_may_conflict_decls);

public:
    static bundled_ast   create(tree::Tangle& tangle);
    static bundled_ast   create_empty();
    static bundled_ast   create_dummy(tree::Tangle& tangle);
    static tree::Tangle* merge(const std::deque<bundled_ast>& asts);

    tree::Tangle*                         get_tangle() const noexcept;
    const grouped_conflict_decl_map_type& get_grouped_may_conflict_decls() const noexcept;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned)
    {
        ar & tangle                    ;
        ar & grouped_may_conflict_decls;
    }

private:
    tree::Tangle*                  tangle;
    grouped_conflict_decl_map_type grouped_may_conflict_decls;
};

} // namespace detail

using detail::bundled_ast;

} } } } // namespace zillians::language::stage::visitor

#endif
