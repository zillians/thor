/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESTRUCTURESTAGE0_H_
#define ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESTRUCTURESTAGE0_H_

#include "language/stage/CompoundStage.h"

namespace zillians { namespace language { namespace stage {

/**
 * RestructureStage0 will be executed after resolution stage and transform the AST generated by parser into a simpler form for later stage to process
 *
 * The AST generated by parser is a direct interpretation of the given source according the grammar, so it's
 * just another representation of the source. In RestructureStage, we transform the AST into another representation
 * which allows easier and more general post-processing on the AST.
 */
class RestructureStage0 : public CompoundSubStage
{
public:
    RestructureStage0();
    virtual ~RestructureStage0();

public:
    virtual const char* name();
    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions();
    virtual bool parseOptions(po::variables_map& vm);
    virtual bool execute(bool& continue_execution);
    virtual bool hasProgress() override;

private:
    bool debug;
    bool dump_graphviz;
    bool no_system_bundle;
    std::string dump_graphviz_dir;
    bool keep_going;
    bool has_progress;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESTRUCTURESTAGE0_H_ */
