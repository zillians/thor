/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESOLUTIONSTAGE_H_
#define ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESOLUTIONSTAGE_H_

#include <cstddef>

#include <string>
#include <utility>
#include <unordered_set>

#include "language/stage/CompoundStage.h"
#include "language/resolver/Resolver.h"

namespace zillians { namespace language { namespace stage {

/**
 * ResolutionStage tries to resolve types and symbols for all nodes in the AST by using multiple passes
 */
class ResolutionStage : public CompoundSubStage
{
public:
    struct ResolveReport
    {
        ResolveReport(bool has_resolved,
                      bool has_failed,
                      bool has_unknown,
                      bool has_instantiated)
            : has_resolved    (has_resolved    )
            , has_failed      (has_failed      )
            , has_unknown     (has_unknown     )
            , has_instantiated(has_instantiated)
        {}

        ResolveReport()
            : has_resolved(false)
            , has_failed(false)
            , has_unknown(false)
            , has_instantiated(false)
        {}

        void reset()
        {
            has_resolved = false;
            has_failed = false;
            has_unknown = false;
            has_instantiated = false;
        }

        bool has_resolved;
        bool has_failed;
        bool has_unknown;
        bool has_instantiated;
    };

public:
    ResolutionStage();
    virtual ~ResolutionStage();

public:
    virtual const char* name();
    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions();
    virtual bool parseOptions(po::variables_map& vm);
    virtual bool execute(bool& continue_execution);

public:
    virtual bool hasProgress();    

private:
    template<typename... Args>
    ResolveReport resolveSymbols(Args&... args);

private:
    void removeTrivialErrors();
    void checkUninferredVariable();
    void reportErrors();

private:
    bool debug;
    bool disable_type_inference;
    std::size_t total_unresolved_count_type;
    std::size_t total_unresolved_count_symbol;

    std::unordered_set<tree::ASTNode*> unresolved_symbols;
    std::unordered_set<tree::ASTNode*> unresolved_types;
    bool dump_ts;
    std::string dump_ts_dir;
    bool dump_graphviz;
    std::string dump_graphviz_dir;
    bool keep_going;

    ResolveReport resolve_report;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_RESOLUTIONSTAGE_H_ */
