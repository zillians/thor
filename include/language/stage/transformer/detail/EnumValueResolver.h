/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ENUM_VALUE_RESOLVER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ENUM_VALUE_RESOLVER_H_

#include <utility>

#include <boost/logic/tribool_fwd.hpp>

#include "language/tree/basic/PrimitiveKind.h"

#include "language/tree/declaration/VariableDecl.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace detail {

struct EnumValueResolver
{
    using Enumerator = tree::VariableDecl;

private:
    bool use_int64_as_underlying_type;
    bool previous_enumerator_has_value;
    bool has_unresolved_value;

    int64 current_enumerated_value;

public:
    EnumValueResolver();

    std::pair<bool, boost::tribool> resolve(Enumerator* enumerator);

    tree::PrimitiveKind getUnderlyingType() const;

    bool hasUnresolvedValue() const;
};

} // namespace detail

} } } } // namespace zillians::language::stage::visitor


#endif
