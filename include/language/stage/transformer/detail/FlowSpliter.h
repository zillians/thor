/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef _LANGUAGE_STAGE_TRANSFORMER_DETAIL_FLOWSPLITER_H_
#define _LANGUAGE_STAGE_TRANSFORMER_DETAIL_FLOWSPLITER_H_

#include <iosfwd>
#include <memory>
#include <set>
#include <vector>

#include <boost/graph/adjacency_list.hpp>

namespace zillians { namespace language {

namespace tree {

struct Expression;
struct FunctionDecl;
struct Statement;
struct VariableDecl;

}

namespace stage { namespace visitor { namespace flow_spliter_detail {

enum
{
    TRANS_UNKNOWN  ,
    TRANS_CASE     ,
    TRANS_CONDITION,
    TRANS_FLOW     ,
    TRANS_MERGE    ,
    TRANS_NORMAL   ,
    TRANS_NULL     ,
};

struct block_info_t
{
    int               tag        = -1;            // for debugging only
    int               transition = TRANS_UNKNOWN;
    tree::Expression* cond_expr  = nullptr;

    std::vector<tree::Statement*> statements;
    std::set<tree::VariableDecl*> decl_variables;
    std::set<tree::VariableDecl*> used_variables;
};

struct link_info_t
{
    tree::Expression* cond_expr = nullptr; // For case expression of SwitchStmt only
};

struct graph_info_t
{
    bool need_this = false;
};

// Assume the following things:
// 1. 1st vertex is entry block
//
// Detail of representation in graph:
// 1. "block_t" is identifier of basic block.
// 2. Information is stored on xxx_info_t.
// 3. Transition is formed by *block_info_t::transition" and out edges of each block.
//
// For each transition (except unknown transition, it's used internally and should not be exposed):
// |           | block properties |  out edges | out edges properties |
// | case      | cond_expr        |  >= 1      | cond_expr            |
// | condition | cond_expr        |  == 2      |                      |
// | flow      |                  |  >= 1      |                      |
// | merge     |                  |  == 1      |                      |
// | normal    |                  |  == 1      |                      |
// | null      |                  |  == 0      |                      |
//
// 1. *cond_expr* on block properties is the origin condition expression on IfStmt or SwitchStmt
// 2. *cond_expr* on out edge properties is *1* on *case 1* of SwitchStmt
// 3. For SwitchStmt, *cond_expr* on out edge means it's the default case
using graph_t = boost::adjacency_list<
    boost::vecS, // required to keep the order of edges for underlying implementation
    boost::vecS,
    boost::bidirectionalS,
    block_info_t,
    link_info_t,
    graph_info_t
>;

using block_t = graph_t::vertex_descriptor;
using  link_t = graph_t::  edge_descriptor;

std::unique_ptr<graph_t> flow_split(tree::FunctionDecl& func);

bool optimize_erase_empty_or_null(graph_t& graph);
bool optimize_merge_block(graph_t& graph);
bool optimize_all(graph_t& graph);

void dump(const graph_t& graph, const char*         filepath);
void dump(const graph_t& graph, const std::string&  filepath);
void dump(const graph_t& graph,       std::ostream& output  );

} } }

} }

#endif // _LANGUAGE_STAGE_TRANSFORMER_DETAIL_FLOWSPLITER_H_
