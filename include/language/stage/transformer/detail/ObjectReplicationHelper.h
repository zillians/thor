/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_OBJECTREPLICATIONHELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_OBJECTREPLICATIONHELPER_H_

#include <set>
#include <tuple>
#include <vector>

#include <boost/logic/tribool.hpp>
#include <boost/variant.hpp>

#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail { namespace object_replication {

boost::tribool is_really_replicable(const tree::ClassDecl& cls);

bool need_creator(tree::ClassDecl& cls);
void create_creator(tree::ClassDecl& cls);

bool need_serializer(tree::ClassDecl& cls);
void create_serializer(tree::ClassDecl& cls);

bool need_deserializer(tree::ClassDecl& cls);
void create_deserializer(tree::ClassDecl& cls);

struct verification_state
{
    std::set<tree::ClassDecl*> verified_creators;
    std::set<tree::ClassDecl*> verified_serializers;
    std::set<tree::ClassDecl*> verified_deserializers;
};

struct verification_result
{
    struct tag_state_non_changed      {};
    struct tag_variable_name_conflict {};

    typedef boost::variant<
        tag_state_non_changed           ,
        tag_variable_name_conflict      ,
        std::vector<tree::FunctionDecl*>, // multiple definition
        tree::FunctionDecl*               // prototype mismatch
    > result_type;

    result_type      creator_result;
    result_type   serializer_result;
    result_type deserializer_result;
};

verification_result continue_verification(verification_state& state, tree::ClassDecl& cls);

} } } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_OBJECTREPLICATIONHELPER_H_ */

