/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_CONSTANT_FOLDER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_CONSTANT_FOLDER_H_

namespace zillians { namespace language { namespace tree {

struct TernaryExpr;
struct BinaryExpr;
struct UnaryExpr ;
struct CastExpr  ;
struct BlockExpr ;

} } }

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail {

bool fold_constant(tree::TernaryExpr& ternary_expr);
bool fold_constant(tree::BinaryExpr&  binary_expr );
bool fold_constant(tree::UnaryExpr&   unary_expr  );
bool fold_constant(tree::CastExpr&    cast_expr   );
bool fold_constant(tree::BlockExpr&   block_expr  );

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_CONSTANT_FOLDER_H_ */
