/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_RESTRUCTUREHELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_RESTRUCTUREHELPER_H_

#include <array>
#include <functional>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "language/tree/ASTNodeFwd.h"

#include "language/context/TransformerContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace restructure_helper {

void splitVariableInitializer(tree::VariableDecl& node);
void splitVariableInitializer(tree::VariableDecl& node, std::vector<std::function<void()>>& transforms, bool postpone = true);

std::array<tree::Statement*, 2> createVarWithInit(tree::VariableDecl& var, tree::Expression& init);

bool restructureMultiType(tree::BranchStmt& node, std::vector<std::function<void()>>& transforms);
bool restructureMultiType(tree::BinaryExpr& node, std::vector<std::function<void()>>& transforms);

void restructureUnaryNewDirect(tree::UnaryExpr& node);
void restructureUnaryNew(tree::UnaryExpr& node, std::vector<std::function<void()>>& transforms);

std::tuple<std::unordered_set<tree::VariableDecl*>, tree::ClassDecl*> getCaptureInfo(tree::ASTNode& node, std::function<bool(tree::ASTNode&)> visit_condition = nullptr);

inline void setSplitReference(tree::ASTNode& node) { /* DO NOTHING. It's the end of instantiate recursion. */ }

void tryGenerateSpecialMethods(tree::ClassDecl& class_, std::vector<std::function<void()>>& actions);
void tryGenerateSpecialMethods(tree::ClassDecl& class_);

template <typename T, typename ... Types>
inline void setSplitReference(tree::ASTNode& node, T splitted_node, Types ... remains)
{
    SplitReferenceContext::set(splitted_node, &node);
    SplitInverseReferenceContext::set(&node, splitted_node);
    setSplitReference(node, remains...);
}

} } } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_RESTRUCTUREHELPER_H_ */
