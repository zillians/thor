/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ASYNC_HELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ASYNC_HELPER_H_

#include <cstdint>

#include <tuple>

namespace zillians { namespace language { namespace tree {

struct AsyncBlock;
struct CallExpr;
struct Expression;
struct Statement;
struct VariableDecl;

} } }

namespace zillians { namespace language { namespace stage { namespace detail {

namespace async_invalid_flags {

constexpr std::uint32_t OK             = 0x00;
constexpr std::uint32_t NOT_A_CALL     = 0x01;
constexpr std::uint32_t NOT_A_ASSIGN   = 0x02;
constexpr std::uint32_t INVALID_ASSIGN = 0x04;
constexpr std::uint32_t FOUND_CAST     = 0x08;

}

std::uint32_t get_async_flags(const tree::AsyncBlock& block);

void verify_async(const tree::AsyncBlock& async_block);

std::tuple<
    const tree::VariableDecl*, // declaration of assignee
    tree::Expression*,         // LHS expression
    tree::Expression*          // RHS expression
> extract_assignment_info_from_asynced_stmt(tree::Statement& stmt);

language::tree::CallExpr* get_implicit_async_call(const tree::AsyncBlock& block);

void set_async_alternative_id_var(tree::AsyncBlock& block, tree::VariableDecl& var);

void build_local_async_call(tree::AsyncBlock& block);
void build_remote_async_call(tree::AsyncBlock& block, tree::Expression& target_expr);

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_DETAIL_ASYNC_HELPER_H_ */
