/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEMANGLINGSTAGEVISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEMANGLINGSTAGEVISITOR_H_

#include "utility/StringUtil.h"

#include "language/context/ManglingStageContext.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/visitor/NameManglingVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * NameManglingStageVisitor is the visitation helper for ManglingStage
 *
 * @see ManglingStage
 */
struct NameManglingStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(mangleInvoker, apply);

    NameManglingStageVisitor();

    void apply(tree::ASTNode&      node);
    void apply(tree::Source&       node);
    void apply(tree::Package&      node);
    void apply(tree::Type&         node);
    void apply(tree::Identifier&   node);
    void apply(tree::Declaration&  node);
    void apply(tree::ClassDecl&    node);
    void apply(tree::EnumDecl&     node);
    void apply(tree::FunctionDecl& node);
    void apply(tree::VariableDecl& node);

private:
    tree::visitor::NameManglingVisitor mangler;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_NAMEMANGLINGSTAGEVISITOR_H_ */
