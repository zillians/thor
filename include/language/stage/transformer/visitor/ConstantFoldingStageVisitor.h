/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_CONSTANT_FOLDING_STAGE_VISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_CONSTANT_FOLDING_STAGE_VISITOR_H_

#include <type_traits>

#include <boost/mpl/contains.hpp>
#include <boost/mpl/vector.hpp>

#include "language/stage/transformer/detail/ConstantFolder.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct ConstantFoldingStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(foldingInvoker, apply);

    ConstantFoldingStageVisitor()
    {
        REGISTER_ALL_VISITABLE_ASTNODE(foldingInvoker);
    }

    void apply(tree::ASTNode& node)
    {
        revisit(node);
    }

    template<
        typename NodeType,
        typename std::enable_if<
            boost::mpl::contains<
                boost::mpl::vector<
                    tree::ClassDecl   ,
                    tree::FunctionDecl
                >,
                typename std::remove_cv<NodeType>::type
            >::value
        >::type * = nullptr
    >
    void apply(NodeType& node)
    {
        if (node.isCompleted())
        {
            revisit(node);
        }
    }

    template<
        typename NodeType,
        typename std::enable_if<
            boost::mpl::contains<
                boost::mpl::vector<
                    tree::TernaryExpr,
                    tree::BinaryExpr,
                    tree::UnaryExpr ,
                    tree::CastExpr  ,
                    tree::BlockExpr
                >,
                typename std::remove_cv<NodeType>::type
            >::value
        >::type * = nullptr
    >
    void apply(NodeType& node)
    {
        revisit(node);

        detail::fold_constant(node);
    }
};

} } } }


#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_CONSTANT_FOLDING_STAGE_VISITOR_H_ */
