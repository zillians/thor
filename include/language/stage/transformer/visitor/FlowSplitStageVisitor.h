/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_FLOW_SPLIT_STAGE_VISITOR_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_FLOW_SPLIT_STAGE_VISITOR_H_

#include <deque>
#include <memory>
#include <utility>

#include "language/tree/ASTNodeFwd.h"
#include "language/stage/transformer/detail/FlowRestructurer.h"
#include "language/stage/transformer/detail/FlowSpliter.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

struct FlowSplitStageVisitor : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(flowSplitInvoker, apply);

    FlowSplitStageVisitor()
    {
        REGISTER_ALL_VISITABLE_ASTNODE(flowSplitInvoker);
    }

    void apply(tree::ASTNode& node)
    {
        revisit(node);
    }

    void apply(tree::FunctionDecl& node)
    {
        revisit(node);

        auto graph = flow_spliter_detail::flow_split(node);

        if (graph == nullptr)
            return;

        actions.emplace_back(
            &node,
            std::move(graph)
        );
    }

public:
    bool hasTransforms() const noexcept
    {
        return !actions.empty();
    }

    void applyTransforms()
    {
        for (const auto& action : actions)
        {
            const auto*const decl  = std::get<0>(action);
            const auto&      graph = std::get<1>(action);

            NOT_NULL(decl );
            NOT_NULL(graph);

            stage::detail::transform_flow(*decl, *graph);
        }
    }

private:
    std::deque<std::pair<const tree::FunctionDecl*, std::unique_ptr<flow_spliter_detail::graph_t>>> actions;

};

} } } }


#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_FLOW_SPLIT_STAGE_VISITOR_H_ */

