/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGELAMBDAVISITOR1_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGELAMBDAVISITOR1_H_

#include <functional>
#include <unordered_set>
#include <vector>

#include "language/tree/ASTNodeFwd.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

/**
 * @see RestructureStage
 */
struct RestructureStageLambdaVisitor1 : public tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(restructInvoker, restruct);

    explicit RestructureStageLambdaVisitor1(bool no_system_bundle);

    void restruct(tree::ASTNode& node);

    void restruct(tree::AsyncBlock& node);

    void restruct(tree::ClassDecl& node);
    void restruct(tree::FunctionDecl& node);

    void restruct(tree::BlockExpr& node);

    bool hasTransforms();
    void applyTransforms();

    void getStatus(unsigned& lambda_class_construct_count, unsigned& async_restruct_count);

private:
    void deferClearResolution(tree::ASTNode& node, const std::unordered_set<tree::VariableDecl*>& captured_list);

    void replaceUseWith(tree::ASTNode& parent, tree::ASTNode& replaced, tree::ASTNode& replace_with, const std::unordered_set<tree::VariableDecl*>& captured_list);

    /*
     * The idea behind the scene is to use SplitReferenceContext to see if we captured the node that is the same as 
     * the lambda variable declaration. 
     *
     * That is, 
     *          var f = lambda() : void { f(); };
     *
     *          the f in lambda body will be resolved to variable declaration f. If we know they are the same, it must
     *          be recursive lambda case. And we rely on the split reference contxt in binary expression (assign).
     */
    bool isRecursiveLambda(tree::FunctionDecl& lambda, tree::VariableDecl& captured_node);

    /***
     * By given a FunctionDecl, extract the return type and the input parameters, we will have a 
     * corresponding Lambda<R, ...> templated identifier.
     */
    tree::Identifier* createSystemLambdaIdentifierFromFunction(tree::FunctionDecl& node);
    tree::ClassDecl* createLambdaClass(tree::FunctionDecl& node, const std::unordered_set<tree::VariableDecl*>& captured_list, tree::ClassDecl* captured_class);

    void transformLambdaBody(tree::FunctionDecl& node, const std::unordered_set<tree::VariableDecl*>& captured_list, tree::ClassDecl& captured_class, tree::Block& lambda_body);

private:
    bool no_system_bundle;
    std::vector<std::function<void()>> transforms;
    std::vector<std::function<void()>> defer_clear;

    // True if we are transforming the lambda, and the nested lambda will not parse
    bool transform_lambda;

    struct Status
    {
        unsigned lambda_class_construct_count = 0;
        unsigned async_restruct_count      = 0;
    } status;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_RESTRUCTURESTAGELAMBDAVISITOR1_H_ */
