/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_IMPLICITCONVERSIONSTAGE_H_
#define ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_IMPLICITCONVERSIONSTAGE_H_

#include "language/stage/CompoundStage.h"

namespace zillians { namespace language { namespace tree {

struct CastExpr;

} } }

namespace zillians { namespace language { namespace stage {

/**
 * ImplicitConversionStage will transform the AST by adding implicit conversion (CastExpr)
 */
class ImplicitConversionStage : public CompoundSubStage
{
public:
	struct ImplicitConversionReport
	{
		ImplicitConversionReport()
		{}

        void reset()
        {
            cast_expr_collection.clear();
        }

		bool hasProgress()
		{
			return cast_expr_collection.size() > 0;
		}

		std::unordered_set<tree::CastExpr*> cast_expr_collection;
	};

public:
    ImplicitConversionStage();
    virtual ~ImplicitConversionStage();

public:
    virtual const char* name();
    virtual std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> getOptions();
    virtual bool parseOptions(po::variables_map& vm);
    virtual bool execute(bool& continue_execution);

public:
    virtual bool hasProgress();

private:
    bool debug;
    bool dump_graphviz;
    bool keep_going;
    std::string dump_graphviz_dir;

	ImplicitConversionReport report;
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_TRANSFORMER_IMPLICITCONVERSIONSTAGE_H_ */
