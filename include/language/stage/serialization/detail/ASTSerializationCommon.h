/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_VISITOR_ASTSERIALIZATIONCOMMON_H_
#define ZILLIANS_LANGUAGE_STAGE_VISITOR_ASTSERIALIZATIONCOMMON_H_

#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/vector.hpp>

#include "core/ContextHubSerialization.h"

#include "language/context/ManglingStageContext.h"
#include "language/context/ResolverContext.h"
#include "language/context/TransformerContext.h"
#include "language/stage/parser/context/SourceInfoContext.h"
#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using NoCrossBundleSerializeContexts = boost::mpl::vector<
    SourceInfoContext,
    SpecializationOf,
    SpecializedAs,
    NameManglingContext,
    EnumDeclManglingContext,
    EnumIdManglingContext,
    StaticVariableManglingContext,
    SplitReferenceContext,
    SplitInverseReferenceContext,
    ReplicatorCreatorContext,
    ReplicatorSerializerContext,
    ReplicatorDeserializerContext
>;

using RelinkableSerializeContexts = boost::mpl::vector<
    ResolvedSymbol,
    ResolvedPackage,
    InstantiatedAs,
    InstantiatedFrom,
    DefaultValueFrom
>;

using DroppableSerializeContexts = boost::mpl::vector<
    SynthesizedObjectLayoutContext,
    DefaultValueAs
>;

// this defines all context object types needed to be de-serialized
using SerializeContexts = boost::mpl::joint_view<
    NoCrossBundleSerializeContexts,
    boost::mpl::joint_view<
        RelinkableSerializeContexts,
        DroppableSerializeContexts
    >
>;

using FullSerializer = ContextHubSerialization<SerializeContexts>;

} } } }

#endif /* ZILLIANS_LANGUAGE_STAGE_VISITOR_ASTSERIALIZATIONCOMMON_H_ */
