/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_STAGE_ASTSERIALIZATIONHELPER_H_
#define ZILLIANS_LANGUAGE_STAGE_ASTSERIALIZATIONHELPER_H_

#include <deque>
#include <iostream>
#include <map>
#include <string>
#include <utility>

#include "language/tree/ASTNodeFwd.h"
#include "language/stage/serialization/detail/ASTSerializationCommon.h"
#include "language/stage/tree_refactor/detail/TreeContextRefactorer.h"

namespace zillians { namespace language { namespace stage {

class ASTSerializationHelper
{
public:
    static bool serialize(const std::string& filename, tree::ASTNode* node);
    static bool serialize(std::ostream& output, tree::ASTNode* node);

    static tree::ASTNode* deserialize(const std::string& filename);

    using BundleASTType = visitor::bundled_ast;

    static bool            serialize_bundle_ast(const std::string&  filename, const BundleASTType& bundle_ast);
    static bool            serialize_bundle_ast(      std::ostream& output  , const BundleASTType& bundle_ast);
    static BundleASTType deserialize_bundle_ast(const std::string&  filename);

private:
    ASTSerializationHelper() { }
};

} } }

#endif /* ZILLIANS_LANGUAGE_STAGE_ASTSERIALIZATIONHELPER_H_ */
