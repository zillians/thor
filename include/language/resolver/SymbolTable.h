/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVER_SYMBOLTABLE_H_
#define ZILLIANS_LANGUAGE_RESOLVER_SYMBOLTABLE_H_

#include <vector>
#include <string>
#include <map>
#include <boost/assert.hpp>
#include <boost/logic/tribool.hpp>
#include "language/resolver/SymbolTable.h"

namespace zillians { namespace language {

namespace tree {
    struct ASTNode;
    struct Package;
    struct Import;
    struct Declaration;
    struct FunctionType;
    struct TypeSpecifier;
}

/**
 * @brief Just a symbol table
 *
 * Implemente the symbol table design in "Adavanced Compiler Design & Implementation"
 */
class SymbolTable
{
// public types
public:

    /**
     * @brief Type of the collection if entry
     *
     * The type meet STL container concept.
     */
    typedef std::vector<tree::ASTNode*> CollectionType;

    /**
     * @brief Result of a \p find() call
     */
    struct FindResult
    {
        FindResult();
        FindResult(const boost::tribool is_found);
        FindResult(const boost::tribool is_found, const CollectionType& candidates);

        /**
         * @brief The status if this find() call
         *
         * \p is_found is \p true if symbol found, \p false if not found, and \p
         * boost::indeterminate if \p SymbolTable is 'not clean'.
         *
         * The term 'clean', mean the query result if the symbol table is valid,
         * trustable. A symbol table is dirty if the owner of the symbol table is a
         * \p ClassDecl, and the bases if that class is not resolved yet. When a
         * class's base is not resolved yet, the symbol_table.find() result is not
         * valid.
         */
        boost::tribool is_found;

        /// Candidates that fit the find()
        CollectionType candidates;
    } ;

    struct InsertResult
    {
        enum value {
            OK,         ///< Insert success
            DUPE,       ///< Insert fail, symbol name dupe in the same scope
            SHADOW,     ///< Insert fali, symbol name shadow another symblo in otter scope
        } ;

        explicit InsertResult(value v)
            : result(v)
        {
            BOOST_ASSERT(v == OK);
        }

        InsertResult(value v, const std::wstring& s)
            : result(v)
            , error_id(s)
        {
            BOOST_ASSERT(v != OK);
        }

        bool is_ok    () const { return result == OK ; }
        bool is_dupe  () const { return result == DUPE ; }
        bool is_shadow() const { return result == SHADOW ; }

        value result;
        std::wstring error_id;
    } ;

    struct ImportStatus
    {
        enum value
        {
            NOT_PACKAGE,    ///< The symbol table is not a Package local symbol table
            NOT_IMPORTED,   ///< The package is not imported, should not be seen
            IMPORTED,       ///< The package is imported, members will be seen
            ON_PATH,        ///< The package is not imported, but on import-path, the package can be seen, but not his member
        } ;
    } ;

public:
    ////////////////////////////////////////////////////////////////////////
    // ctor/dtor
    ////////////////////////////////////////////////////////////////////////

    SymbolTable();
    SymbolTable(const ImportStatus::value is);

    ////////////////////////////////////////////////////////////////////////
    // scope
    ////////////////////////////////////////////////////////////////////////

    void enter(tree::ASTNode* scope);

    /**
     * @brief Leave the scope and remove symbols in this scope from the symbol table
     * @param scope Scope to leave
     *
     * \b Precondition: The scope must be the same with the one on the top of
     * the scope_stack.
     *
     * Symbols in this scope will be removed from the symbol table.
     */
    void leave(tree::ASTNode* scope);

    ////////////////////////////////////////////////////////////////////////
    // modifier
    ////////////////////////////////////////////////////////////////////////

    /**
     * @brief Insert 0, 1 or 2 symbols into the symbol table
     *
     * Generally, insert symbol can not duplicate, and only one symbol will be
     * inserted, but have following exeception:
     *
     * 1. \p FunctionDecl can have the same name (for function overloading and
     * template function specialization)
     *
     * 2. \p ClassDecl can have the same name (for class template
     * sprcialization)
     *
     * 3. <tt>import a.b;</tt> will insert "a", not "a.b". Therefore when you
     *
     * \code
     * import a.b;
     * import a.b.c;
     * \endcode
     *
     * only one symbol "a" will be inserted, however, the second insertion is
     * still successful.
     *
     * 4. <tt>import x = a.b.c;</tt> will insert two symbol "a" and "x". "a" is
     * refering to \p Package "a", and "x" is refering to \p Package "a.b.c".
     *
     * 5. <tt>import . = a.b.c;</tt> will insert no symbol, but add a base.
     *
     * \note I am thinking maybe the import related action should be moved to
     * ResolutionStageVisitor?
     */
    InsertResult insert(tree::Package* symbol);
    InsertResult insert(tree::Import* symbol);
    InsertResult insert(tree::Declaration* symbol);

    void set_clean();
    void add_base(SymbolTable* base_table);
    void set_import_status(const SymbolTable::ImportStatus::value tag);
    ImportStatus::value get_import_status() const;

    ////////////////////////////////////////////////////////////////////////
    // query
    ////////////////////////////////////////////////////////////////////////

    boost::tribool has(const std::wstring& id) const;
    FindResult find(const std::wstring& id) const;
    bool is_clean() const;

private:
    struct Entry
    {
        enum TAG {
            NONE,
            IMPORT_NS,
            PACKAGE,
        } ;

        Entry(tree::ASTNode* n,
              const std::wstring& id,
              const std::vector<Entry>::size_type i,
              TAG tag = NONE)
            : symbol(n)
            , id(id)
            , prev_index(i)
            , tag(tag)
        {}

        bool is_import_ns() const
        {
            return tag == IMPORT_NS;
        }

        bool is_package() const
        {
            return tag == PACKAGE;
        }

        tree::ASTNode* symbol;
        std::wstring id;
        std::vector<Entry>::size_type prev_index;
        TAG tag;
    } ;

    typedef std::vector<Entry> EntryListType;

public:
    typedef std::vector<Entry>::size_type EntryListSizeType;

public:
    EntryListSizeType size() const;
    std::vector<SymbolTable*>::size_type size_base() const;
    void print() const;
    void dump(std::wostream& os) const;
    void disable_report_error();

private:
    bool clean;
    std::vector<SymbolTable*> base_tables;

    FindResult find_in_base(const std::wstring& id) const;

    struct Frame
    {
        Frame(tree::ASTNode* b, const std::vector<Entry>::size_type i)
            : block(b)
            , frame_index(i)
        {}
        tree::ASTNode* block;
        std::vector<Entry>::size_type frame_index;
    } ;

    InsertResult in_table_state(const std::wstring& id, tree::ASTNode* node);
    void insert_entry(const std::wstring& id, tree::ASTNode* node, const Entry::TAG tag = Entry::NONE);
    void remove_entry();

    EntryListType entry_list;
    std::vector<Frame> scope_index_stack;
    std::map<std::wstring, EntryListSizeType> str_table;
    ImportStatus::value import_status;
    bool report_error;
} ;

} }

#endif // ZILLIANS_LANGUAGE_RESOLVER_SYMBOLTABLE_H_
