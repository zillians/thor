/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_RESOLVER_VIRTUALTABLE_H_
#define ZILLIANS_LANGUAGE_RESOLVER_VIRTUALTABLE_H_

#include <cstdint>

#include <deque>
#include <map>
#include <set>
#include <string>
#include <vector>

#include <boost/operators.hpp>
#include <boost/serialization/nvp.hpp>

#include "language/tree/RelinkablePtr.h"
#include "language/tree/UniqueName.h"

namespace zillians { namespace language {

namespace tree {

struct ClassDecl;
struct FunctionDecl;
struct Type;

}  // namespace tree

class virtual_table
{
public:
    friend class boost::serialization::access;

    struct signature_type : public boost::totally_ordered<signature_type>
    {
        signature_type();
        signature_type(std::wstring&& name, std::vector<tree::relinkable_ptr<tree::Type>>&& parameter_types);

        std::wstring to_string() const;

        bool operator==(const signature_type& rhs) const noexcept;
        bool operator< (const signature_type& rhs) const noexcept;

        template<typename Archive>
        void serialize(Archive& ar, const unsigned int)
        {
            ar & BOOST_SERIALIZATION_NVP(name           );
            ar & BOOST_SERIALIZATION_NVP(parameter_types);
        }

        std::wstring                                  name;
        std::vector<tree::relinkable_ptr<tree::Type>> parameter_types;
    };

    struct entry_type
    {
        signature_type                                             signature;
        bool                                                       is_pure;
        std::deque<tree::relinkable_ptr<const tree::FunctionDecl>> overriden_decls; // last one is the leaf declaration

        template<typename Archive>
        void serialize(Archive& ar, const unsigned int)
        {
            ar & BOOST_SERIALIZATION_NVP(signature      );
            ar & BOOST_SERIALIZATION_NVP(is_pure        );
            ar & BOOST_SERIALIZATION_NVP(overriden_decls);
        }
    };

    struct segment_type
    {
        using entry_container = std::vector<entry_type>;

        tree::relinkable_ptr<const tree::ClassDecl> source_class;
        entry_container                             entries;

        template<typename Archive>
        void serialize(Archive& ar, const unsigned int)
        {
            ar & BOOST_SERIALIZATION_NVP(source_class);
            ar & BOOST_SERIALIZATION_NVP(entries     );
        }
    };

    struct entry_info_type
    {
        const tree::FunctionDecl*   impl_decl;
        const tree::FunctionDecl* origin_decl;
    };

    struct segment_info_type
    {
        using entry_info_container = std::vector<entry_info_type>;

        const tree::ClassDecl* source_class;
        entry_info_container   entry_infos;
    };

    bool empty() const noexcept;
    bool is_ready() const noexcept;

    bool                           has_virtual() const noexcept;
    std::pair<std::uint32_t, bool> get_virtual_index(const tree::FunctionDecl& member_function) const;

    bool                                has_pure_virtual() const;
    std::set<const tree::FunctionDecl*> get_pure_virtuals() const;

    std::vector<const tree::FunctionDecl*> find_overridens(const tree::FunctionDecl& member_function) const;

    std::vector<segment_info_type> generate_info() const;

    // if member function is implicit virtual, we will set 'FunctionDecl::is_virtual' to true
    bool build(const tree::ClassDecl& owner_class, const std::vector<const virtual_table*>& parent_vtables, const std::vector<tree::FunctionDecl*>& member_functions);

    void err_status() const;

    template<typename Archive>
    void serialize(Archive& ar, const unsigned int)
    {
        ar & BOOST_SERIALIZATION_NVP(owner           );
        ar & BOOST_SERIALIZATION_NVP(segments_ready  );
        ar & BOOST_SERIALIZATION_NVP(segments        );
        ar & BOOST_SERIALIZATION_NVP(override_states );
        ar & BOOST_SERIALIZATION_NVP(function_indices);
    }

private:
    void inherit(const virtual_table& vtable);
    void build_function_indices();
    void add_virtual(tree::FunctionDecl& member_function);

public:
    struct entry_index_type
    {
        std::vector<segment_type>::size_type     segment;
        segment_type::entry_container::size_type index;

        template<typename Archive>
        void serialize(Archive& ar, const unsigned int)
        {
            ar & BOOST_SERIALIZATION_NVP(segment);
            ar & BOOST_SERIALIZATION_NVP(index  );
        }
    };

    struct overriden_state
    {
        bool                         is_pure;
        std::deque<entry_index_type> indices;

        template<typename Archive>
        void serialize(Archive& ar, const unsigned int)
        {
            ar & BOOST_SERIALIZATION_NVP(is_pure);
            ar & BOOST_SERIALIZATION_NVP(indices);
        }
    };

    const tree::ClassDecl*                                                  owner          = nullptr;
    bool                                                                    segments_ready = false;
    std::vector<segment_type>                                               segments;
    std::map<signature_type, overriden_state>                               override_states;
    std::map<tree::relinkable_ptr<const tree::FunctionDecl>, std::uint32_t> function_indices;
};

} }

#endif // ZILLIANS_LANGUAGE_RESOLVER_VIRTUALTABLE_H_
