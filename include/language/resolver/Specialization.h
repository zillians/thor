/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_SPECIALIZATION_H_
#define ZILLIANS_LANGUAGE_SPECIALIZATION_H_

#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/indexed_by.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/noncopyable.hpp>

#include "language/resolver/SpecializationFwd.h"
#include "language/tree/ASTNodeFwd.h"

namespace zillians { namespace language { namespace resolution { namespace specialization {

namespace detail {

template<typename DeclType>
struct Item;

template<>
struct Item<tree::ClassDecl>
{
    tree::ASTNode* owner;
    std::wstring name;
    std::vector<tree::ClassDecl*> declarations;

    typedef boost::multi_index::composite_key<
        Item,
        boost::multi_index::member<
            Item,
            tree::ASTNode*,
            &Item::owner
        >,
        boost::multi_index::member<
            Item,
            std::wstring,
            &Item::name
        >
    > UniqueKey;
};

template<>
struct Item<tree::FunctionDecl>
{
    tree::ASTNode* owner;
    std::size_t call_parameter_count;
    std::size_t template_parameter_count;
    std::wstring name;
    std::vector<tree::FunctionDecl*> declarations;

    typedef boost::multi_index::composite_key<
        Item,
        boost::multi_index::member<
            Item,
            tree::ASTNode*,
            &Item::owner
        >,
        boost::multi_index::member<
            Item,
            std::size_t,
            &Item::call_parameter_count
        >,
        boost::multi_index::member<
            Item,
            std::size_t,
            &Item::template_parameter_count
        >,
        boost::multi_index::member<
            Item,
            std::wstring,
            &Item::name
        >
    > UniqueKey;
};

}

class Grouper : public boost::noncopyable
{
public:
    struct TagOwnerAndName;
    struct TagOwner;

    template<typename DeclType>
    struct GroupType
    {
        typedef detail::Item<DeclType> ItemType;

        typedef boost::multi_index_container<
            ItemType,
            boost::multi_index::indexed_by<
                boost::multi_index::ordered_unique<
                    boost::multi_index::tag<TagOwnerAndName>,
                    typename ItemType::UniqueKey
                >,
                boost::multi_index::ordered_non_unique<
                    boost::multi_index::tag<TagOwner>,
                    boost::multi_index::member<
                        ItemType,
                        tree::ASTNode*,
                        &ItemType::owner
                    >
                >
            >
        > Type;
    };

    void add(tree::ClassDecl& cls_decl);
    void add(tree::FunctionDecl& func_decl);

    void ready();

private:
    void group_classes();
    void group_functions();

    GroupType<tree::ClassDecl>::Type cls_groups;
    GroupType<tree::FunctionDecl>::Type func_groups;
};

} } } }

#endif /* ZILLIANS_LANGUAGE_SPECIALIZATION_H_ */
