/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_THORTOOLBASEH_
#define ZILLIANS_LANGUAGE_THORTOOLBASEH_

#include <string>

#include <boost/filesystem/path.hpp>

#include "language/ThorConfiguration.h"

namespace zillians { namespace language {

class ThorToolBase
{
public:
    static const std::string& getDefaultDebugToolCommand();

    static int shell(const boost::filesystem::path& executable_path, const std::string& parameters, const std::string& debug_tool = std::string(), const std::string& debugger_command = std::string());
    static int shell(const ThorBuildConfiguration& config, const std::string& tool, const std::string& parameters);
};

} }

#endif /* ZILLIANS_LANGUAGE_THORTOOLBASEH_ */
