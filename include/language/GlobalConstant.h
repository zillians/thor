/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_GLOBALCONSTANT_H_
#define ZILLIANS_LANGUAGE_GLOBALCONSTANT_H_

namespace zillians { namespace language { namespace global_constant {

extern const char *const DYN_CAST_IMPL;
extern const char *const ROOTSET_ADDER;
extern const char *const ROOTSET_REMOVER;

extern const char *const INVOCATION_ADDER         ;
extern const char *const INVOCATION_RESERVER      ;
extern const char *const INVOCATION_COMMITER      ;
extern const char *const INVOCATION_ABORTER       ;

extern const char *const INVOCATION_APPENDER_BOOL ;
extern const char *const INVOCATION_APPENDER_INT8 ;
extern const char *const INVOCATION_APPENDER_INT16;
extern const char *const INVOCATION_APPENDER_INT32;
extern const char *const INVOCATION_APPENDER_INT64;
extern const char *const INVOCATION_APPENDER_FLT32;
extern const char *const INVOCATION_APPENDER_FLT64;
extern const char *const INVOCATION_APPENDER_INT8P;

extern const char *const INVOCATION_RET_PTR_SETTER;

} } }

#endif /* ZILLIANS_LANGUAGE_GLOBALCONSTANT_H_ */
