/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_GRAMMAR_THOR_H_
#define ZILLIANS_LANGUAGE_GRAMMAR_THOR_H_

#define BOOST_SPIRIT_UNICODE
#define BOOST_SPIRIT_ACTIONS_ALLOW_ATTR_COMPAT

#include <boost/spirit/include/support_multi_pass.hpp>
#include <boost/spirit/include/classic_position_iterator.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_function.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_bind.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/karma_attr_cast.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/qi_numeric.hpp>
#include <boost/spirit/include/qi_no_case.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/repository/include/qi_distinct.hpp>
#include <boost/spirit/repository/include/qi_iter_pos.hpp>
#include <boost/regex/pending/unicode_iterator.hpp>

#include "utility/UnicodeUtil.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/context/ParserContext.h"
#include "WhiteSpace.h"

#define DISTINCT_IDENTIFIER(x)       distinct(unicode::alnum | unicode::char_(L'_'))[x]
#define DISTINCT_NO_ASSIGN_FOLLOW(x) distinct(unicode::char_(L'='))[x]
#define EMIT_BOOL(x)                 ((x) > qi::attr(true))
#define EMIT_INT(x)                  ((x) > qi::attr(1024))
#define DECL_RULE_LEXEME(x)          qi::rule<Iterator, typename SA::x::attribute_type, typename SA::x::local_type> x
#define DECL_RULE(x)                 qi::rule<Iterator, typename SA::x::attribute_type, detail::WhiteSpace<Iterator>, typename SA::x::local_type> x
#define DECL_RULE_CUSTOM_SA(x, sa)   qi::rule<Iterator, typename SA::sa::attribute_type, detail::WhiteSpace<Iterator>, typename SA::sa::local_type> x
#define INIT_RULE(x) \
        { \
            x.name(#x); \
            if(getParserContext().dump_rule_debug) \
                debug(x); \
        }

namespace spirit = boost::spirit;
namespace qi = spirit::qi;
namespace unicode = spirit::unicode;

using spirit::repository::distinct;
using spirit::ascii::no_case;
using spirit::repository::qi::iter_pos;
using spirit::omit;

// traits type to perform attribute conversion
namespace boost { namespace spirit { namespace traits {

    template <>
    struct transform_attribute<
        zillians::language::tree::SimpleIdentifier*,
        zillians::language::tree::Identifier*,
        qi::domain
    >
    {
        using source_type = zillians::language::tree::SimpleIdentifier*;
        using result_type = zillians::language::tree::Identifier*;

        using type = result_type;

        static type pre(source_type source)
        {
            return static_cast<type>(source);
        }
    };

} } } // namespace boost::spirit::traits

namespace zillians { namespace language { namespace grammar {

/////////////////////////////////////////////////////////////////////
/// detail
/////////////////////////////////////////////////////////////////////


namespace detail {

template <typename Iterator, typename SA>
struct Identifier : qi::grammar<Iterator, typename SA::identifier::attribute_type, typename SA::identifier::local_type>
{
    Identifier() : Identifier::base_type(identifier)
    {
        location
            = omit[ iter_pos[ typename SA::location::init() ] ]
            ;

        keyword_sym =
            L"void", L"bool",
            L"int8", L"int16", L"int32", L"int64",
            L"float32", L"float64",
            L"true", L"false", L"null", L"this", L"super", L"...",
            L"const", L"static",
            L"typedef", L"class", L"interface", L"enum",
            L"public", L"protected", L"private",
            L"var", L"function", L"lambda",
            L"if", L"elif", L"else",
            L"switch", L"case", L"default",
            L"while", L"do", L"in", L"for",
            L"break", L"continue", L"return",
            L"new", L"delete", L"isa", L"cast", L"instanceof",
            L"import",
            L"extends", L"implements";
        keyword = DISTINCT_IDENTIFIER(keyword_sym);

        start %= qi::lexeme[ ((unicode::alpha | unicode::char_(L'_')) > *(unicode::alnum | unicode::char_(L'_'))) - keyword ];

        identifier
            = location [ typename SA::location::cache_loc() ]
                >> start [ typename SA::identifier::init() ]
            ;

        INIT_RULE(identifier);
    }

    Identifier(const Identifier& other)
    : Identifier() { }

    DECL_RULE_LEXEME(location);
    qi::symbols<wchar_t const> keyword_sym;
    qi::rule<Iterator, std::wstring()> keyword;
    qi::rule<Iterator, std::wstring()> start;
    DECL_RULE_LEXEME(identifier);
};

template <typename Iterator, typename SA>
struct IntegerLiteral : qi::grammar<Iterator, typename SA::integer_literal::attribute_type, typename SA::integer_literal::local_type>
{
    IntegerLiteral() : IntegerLiteral::base_type(integer_literal)
    {
        location
            = omit[ iter_pos[ typename SA::location::init() ] ]
            ;

        decimal_sequence %= distinct(qi::lit(L'.') | L'x' | no_case[L'e'])[ ( +qi::digit ) ];
        hex_sequence %= qi::omit[ no_case[ qi::lit(L"0x") ] ] > ( +qi::xdigit );

        integer_literal
            = location [ typename SA::location::cache_loc() ]
            >> ( (-no_case[ qi::char_(L'-') ] >> decimal_sequence >> -no_case[ qi::char_(L'l') ]) [ typename SA::integer_literal::template init<10L>() ]
               | (-no_case[ qi::char_(L'-') ] >>     hex_sequence >> -no_case[ qi::char_(L'l') ]) [ typename SA::integer_literal::template init<16L>() ]
               )
            ;

        INIT_RULE(integer_literal);
    }

    DECL_RULE_LEXEME(location);
    qi::rule<Iterator, std::wstring()> decimal_sequence;
    qi::rule<Iterator, std::wstring()> hex_sequence;
    DECL_RULE_LEXEME(integer_literal);
};

template <typename Iterator, typename SA>
struct FloatLiteral : qi::grammar<Iterator, typename SA::float_literal::attribute_type, typename SA::float_literal::local_type>
{
    FloatLiteral() : FloatLiteral::base_type(float_literal)
    {
        location
            = omit[ iter_pos[ typename SA::location::init() ] ]
            ;

        start
            %=  ( builtin_float_parser
                | ( (qi::uint_ | builtin_float_parser) > no_case[L'e'] > -qi::lit(L'-') > qi::uint_ )
                )
            ;

        float_literal
            = location [ typename SA::location::cache_loc() ]
            >> ( (start >> no_case[L'f']) [ typename SA::float_literal::with_postfix_init() ]
                 | start [ typename SA::float_literal::init() ]
               )
            ;

        INIT_RULE(float_literal);
    }

    DECL_RULE_LEXEME(location);
    qi::real_parser<double, qi::strict_ureal_policies<double> > builtin_float_parser;
    qi::rule<Iterator, double()> start;
    DECL_RULE_LEXEME(float_literal);
};

template <typename Iterator, typename SA>
struct StringLiteral : qi::grammar<Iterator, typename SA::string_literal::attribute_type, typename SA::string_literal::local_type>
{
    StringLiteral() : StringLiteral::base_type(string_literal)
    {
        location
            = omit[ iter_pos[ typename SA::location::init() ] ]
            ;

        unescaped_char_sym.add
            (L"\\a", L'\a')
            (L"\\b", L'\b')
            (L"\\f", L'\f')
            (L"\\n", L'\n')
            (L"\\r", L'\r')
            (L"\\t", L'\t')
            (L"\\v", L'\v')
            (L"\\{", language::tree::StringLiteral::getInternalPlaceholderBegin() )
            (L"\\\\", L'\\')
            (L"\\\'", L'\'')
            (L"\\\"", L'\"');

        start
            %= qi::lit(L'\"')
                >   *( ( ( unicode::char_ - L'\"' ) - L'\\' )
                    | ( unescaped_char_sym ) 
                    | ( L"\\x" > qi::hex )
                    )
                >   L'\"'
            ;

        string_literal
            = location [ typename SA::location::cache_loc() ]
                >> start [ typename SA::string_literal::init() ]
            ;

        INIT_RULE(string_literal);
    }

    DECL_RULE_LEXEME(location);
    qi::symbols<wchar_t const, wchar_t const> unescaped_char_sym;
    qi::rule<Iterator, std::wstring()> start;
    DECL_RULE_LEXEME(string_literal);
};

} // namepaces detail

/////////////////////////////////////////////////////////////////////
/// Thor
/////////////////////////////////////////////////////////////////////
template<typename Iterator, typename SA>
struct Thor : qi::grammar<Iterator, typename SA::start::attribute_type, detail::WhiteSpace<Iterator>, typename SA::start::local_type >
{
    Thor() : Thor::base_type(start)
    {
        /////////////////////////////////////////////////////////////////////
        /// Operators
        /////////////////////////////////////////////////////////////////////

        namespace phx = boost::phoenix;

        // operators & scope
        {
            // miscellaneous
            {
                ELLIPSIS  = qi::lit(L"...");
                DOT       = qi::lit(L'.');
                COLON     = qi::lit(L":");
                SEMICOLON = qi::lit(L';');
                COMMA     = qi::lit(L',');
                AT_SYMBOL = qi::lit(L'@');
                Q_MARK    = qi::lit(L'?');
            }

            // assignments
            {
                ASSIGN        = DISTINCT_NO_ASSIGN_FOLLOW(L'=');
                RSHIFT_ASSIGN = qi::lit(L">>=");
                LSHIFT_ASSIGN = qi::lit(L"<<=");
                PLUS_ASSIGN   = qi::lit(L"+=");
                MINUS_ASSIGN  = qi::lit(L"-=");
                MUL_ASSIGN    = qi::lit(L"*=");
                DIV_ASSIGN    = qi::lit(L"/=");
                MOD_ASSIGN    = qi::lit(L"%=");
                AND_ASSIGN    = qi::lit(L"&=");
                OR_ASSIGN     = qi::lit(L"|=");
                XOR_ASSIGN    = qi::lit(L"^=");
            }

            // unary operator
            {
                INCREMENT = qi::lit(L"++");
                DECREMENT = qi::lit(L"--");
            }

            // arithmetic operators
            {
                ARITHMETIC_PLUS  = distinct(qi::lit(L'+') | L'=')[qi::lit(L'+')];
                ARITHMETIC_MINUS = distinct(qi::lit(L'-') | L'=')[qi::lit(L'-')];
                ARITHMETIC_MUL   = DISTINCT_NO_ASSIGN_FOLLOW(qi::lit(L'*'));
                ARITHMETIC_DIV   = DISTINCT_NO_ASSIGN_FOLLOW(qi::lit(L'/'));
                ARITHMETIC_MOD   = DISTINCT_NO_ASSIGN_FOLLOW(qi::lit(L'%'));
            }

            // binary operators
            {
                BINARY_AND = distinct(qi::lit(L'&') | L'=')[qi::lit(L'&')];
                BINARY_OR  = distinct(qi::lit(L'|') | L'=')[qi::lit(L'|')];
                BINARY_XOR = DISTINCT_NO_ASSIGN_FOLLOW(qi::lit(L'^'));
                BINARY_NOT = qi::lit(L'~');
            }

            // shift operators
            {
                RSHIFT = distinct(qi::lit(L'>') | L"=")[L">>"];
                LSHIFT = DISTINCT_NO_ASSIGN_FOLLOW(qi::lit(L"<<"));
            }

            // logical operators
            {
                LOGICAL_AND = qi::lit(L"&&");
                LOGICAL_OR  = qi::lit(L"||");
                LOGICAL_NOT = DISTINCT_NO_ASSIGN_FOLLOW(qi::lit(L'!'));
            }

            // comparison
            {
                COMPARE_EQ = qi::lit(L"==");
                COMPARE_NE = qi::lit(L"!=");
                COMPARE_GT = distinct(qi::lit(L'>') | L'=')[qi::lit(L'>')];
                COMPARE_LT = distinct(qi::lit(L'<') | L'=')[qi::lit(L'<')];
                COMPARE_GE = qi::lit(L">=");
                COMPARE_LE = qi::lit(L"<=");
            }

            // scope
            {
                LEFT_BRACE    = qi::lit(L'{');
                RIGHT_BRACE   = qi::lit(L'}');
                LEFT_BRACKET  = qi::lit(L'[');
                RIGHT_BRACKET = qi::lit(L']');
                LEFT_PAREN    = qi::lit(L'(');
                RIGHT_PAREN   = qi::lit(L')');
            }
        }

        /////////////////////////////////////////////////////////////////////
        /// Keywords
        /////////////////////////////////////////////////////////////////////

#define DECL_TOKEN(token, str) \
    token = DISTINCT_IDENTIFIER(qi::lit(str))

        // tokens
        {
            DECL_TOKEN(_TRUE, L"true");
            DECL_TOKEN(_FALSE, L"false");
            DECL_TOKEN(_NULL, L"null");
            DECL_TOKEN(_THIS, L"this");
            DECL_TOKEN(SUPER, L"super");

            DECL_TOKEN(_CONST, L"const");
            DECL_TOKEN(STATIC, L"static");
            DECL_TOKEN(VIRTUAL, L"virtual");
            DECL_TOKEN(OVERRIDE, L"override");

            DECL_TOKEN(_BOOLEAN, L"bool");
            DECL_TOKEN(INT8, L"int8");
            DECL_TOKEN(INT16, L"int16");
            DECL_TOKEN(INT32, L"int32");
            DECL_TOKEN(INT64, L"int64");
            DECL_TOKEN(FLOAT32, L"float32");
            DECL_TOKEN(FLOAT64, L"float64");
            DECL_TOKEN(_VOID, L"void");

            DECL_TOKEN(TYPEDEF, L"typedef");
            DECL_TOKEN(CLASS, L"class");
            DECL_TOKEN(INTERFACE, L"interface");
            DECL_TOKEN(ENUM, L"enum");

            DECL_TOKEN(PUBLIC, L"public");
            DECL_TOKEN(PROTECTED, L"protected");
            DECL_TOKEN(PRIVATE, L"private");

            DECL_TOKEN(VAR, L"var");
            DECL_TOKEN(FUNCTION, L"function");
            DECL_TOKEN(LAMBDA, L"lambda");
            DECL_TOKEN(TASK, L"task");

            DECL_TOKEN(IF, L"if");
            DECL_TOKEN(ELIF, L"elif");
            DECL_TOKEN(ELSE, L"else");

            DECL_TOKEN(SWITCH, L"switch");
            DECL_TOKEN(CASE, L"case");
            DECL_TOKEN(DEFAULT, L"default");

            DECL_TOKEN(WHILE, L"while");
            DECL_TOKEN(DO, L"do");
            DECL_TOKEN(_IN, L"in");
            DECL_TOKEN(FOR, L"for");

            DECL_TOKEN(RETURN, L"return");
            DECL_TOKEN(BREAK, L"break");
            DECL_TOKEN(CONTINUE, L"continue");

            DECL_TOKEN(NEW, L"new");
            DECL_TOKEN(DELETE, L"delete");
            DECL_TOKEN(ISA, L"isa");
            DECL_TOKEN(CAST, L"cast");

            DECL_TOKEN(IMPORT, L"import");

            DECL_TOKEN(EXTENDS, L"extends");
            DECL_TOKEN(IMPLEMENTS, L"implements");

            DECL_TOKEN(PIPELINE, L"pipeline");
            DECL_TOKEN(ASYNC, L"async");
            DECL_TOKEN(ATOMIC, L"atomic");
            DECL_TOKEN(FLOW, L"flow");
            DECL_TOKEN(LOCK, L"lock");
            DECL_TOKEN(ON_DOMAIN, L"on");
        }

#undef DECL_TOKEN

        /////////////////////////////////////////////////////////////////////
        /// Rule Define
        /////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////
        /// BEGIN BASIC
        ///
        location
            = omit[ iter_pos[ typename SA::location::init() ] ]
            ;

        expect_semicolon
            = qi::eps [ typename SA::location::cache_loc() ] > SEMICOLON
            ;

        param_decl_list
            %= (variable_decl_stem % COMMA)
            ;

        param_decl_with_init_list
            %= (param_decl_with_init % COMMA)
            ;

        expr_init_specifier
            %= ASSIGN > expression
            ;

        type_initializer_or_specifier
            = ( (ASSIGN > arg_thor_type) [ typename SA::type_initializer_or_specifier::initializer() ]
               | type_specifier [ typename SA::type_initializer_or_specifier::specifier() ]
               )
            ;

        // var a : bool ;
        //       ^^^^^^
        type_specifier
            %= COLON >> arg_thor_type
            ;

        multi_type_specifier
            = multi_type_specifier_impl [ qi::_val = phx::new_<tree::MultiSpecifier>(qi::_1) ]
            | type_specifier            [ qi::_val = qi::_1 ]
            ;

        multi_type_specifier_impl
            %= COLON
            >> LEFT_PAREN
             > (arg_thor_type > +(COMMA > arg_thor_type))
             > RIGHT_PAREN
            ;

        arg_thor_type
            =  ( arg_thor_non_qualified_type >> -(EMIT_BOOL(LEFT_BRACKET) >> -(INTEGER_LITERAL % COMMA) >> RIGHT_BRACKET)) [ typename SA::arg_thor_type::init_array_type() ]
            ;

        // var a : bool, int8, ... primitives ;
        //         ^^^^  ^^^^
        // or
        // var a : Foo<T, U>
        //         ^^^^^^^^^
        // or
        // var a : function(int32, int64 ...) : int32 ;
        //         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        //
        // TODO lambda function grammar wrong: no variable name
        arg_thor_non_qualified_type
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( DISTINCT_IDENTIFIER(qi::lit(L"void"))                                          [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::VOID_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"bool"))                                          [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::BOOL_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"int8"))                                          [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::INT8_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"int16"))                                         [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::INT16_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"int32"))                                         [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::INT32_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"int64"))                                         [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::INT64_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"float32"))                                       [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::FLOAT32_TYPE>() ]
                    | DISTINCT_IDENTIFIER(qi::lit(L"float64"))                                       [ typename SA::arg_thor_non_qualified_type::template init_primitive_type<tree::PrimitiveKind::FLOAT64_TYPE>() ]
                    | nested_name                                                                    [ typename SA::arg_thor_non_qualified_type::init_type() ]
                    | (FUNCTION > LEFT_PAREN > type_arg_list > RIGHT_PAREN > -multi_type_specifier) [ typename SA::arg_thor_non_qualified_type::init_function_type() ]
                    | (LAMBDA > LEFT_PAREN > type_arg_list > RIGHT_PAREN > type_specifier)          [ typename SA::arg_thor_non_qualified_type::init_lambda_function_type() ]
                    )
            ;

        id_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( simple_template_id
                    | IDENTIFIER
                    ) [ typename SA::id_expression::init() ]
            ; 

        // var a : Foo<int32, Bar> ;
        //         ^^^^^^^^^^^^^^^
        simple_template_id
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( IDENTIFIER >> type_specialize_specifier ) [ typename SA::simple_template_id::init() ]
            ;

        // class Foo<T : int32> {}
        //       ^^^^^^^^^^^^^^
        // or
        // class Foo<T = float32> {}
        //       ^^^^^^^^^^^^^^^^
        // or
        // class Foo<T = a.b.c> {}
        //       ^^^^^^^^^^^^^^
        template_param_identifier
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (IDENTIFIER > -(template_param_list)) [ typename SA::template_param_identifier::init() ]
            ;

        // class Foo<T : int32> {}
        //          ^^^^^^^^^^^
        // or
        // class Foo<T = float32> {}
        //          ^^^^^^^^^^^^^
        // or
        // class Foo<T = a.b.c> {}
        //          ^^^^^^^^^^^
        template_param_list
            = (COMPARE_LT >> (typename_decl % COMMA) >> COMPARE_GT)
            ;

        // var a : Foo<int32, Bar> ;
        //            ^^^^^^^^^^^^
        type_specialize_specifier
            %= (COMPARE_LT >> type_arg_list >> COMPARE_GT)
            ;

        // var a : Foo<int32, Bar> ;
        //             ^^^^^^^^^^ 
        type_arg_list
            %= arg_thor_type >> *(COMMA > arg_thor_type)
             | qi::attr(typename SA::type_arg_list::value_type())
            ;

        class_member_visibility
            = PUBLIC    [ typename SA::class_member_visibility::template init<tree::Declaration::VisibilitySpecifier::PUBLIC>() ]
            | PROTECTED [ typename SA::class_member_visibility::template init<tree::Declaration::VisibilitySpecifier::PROTECTED>() ]
            | PRIVATE   [ typename SA::class_member_visibility::template init<tree::Declaration::VisibilitySpecifier::PRIVATE>() ]
            ;

        interface_member_visibility
            = PUBLIC    [ typename SA::class_member_visibility::template init<tree::Declaration::VisibilitySpecifier::PUBLIC>() ]
            | PROTECTED [ typename SA::class_member_visibility::template init<tree::Declaration::VisibilitySpecifier::PROTECTED>() ]
            ;

        annotation_list
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (+annotation) [ typename SA::annotation_list::init() ]
            ;

        annotation
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (AT_SYMBOL > IDENTIFIER > -annotation_body) [ typename SA::annotation::init() ]
            ;

        annotation_body
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (LEFT_BRACE
                        > ((IDENTIFIER > ASSIGN > (primary_expression | annotation_body)) % COMMA)
                        > RIGHT_BRACE
                    ) [ typename SA::annotation_body::init() ]
            ;

        new_or_delete_id
            = qi::eps [ typename SA::location::cache_loc() ]
                >> ((EMIT_INT(NEW) > -template_param_list) | EMIT_INT(DELETE)) [ typename SA::new_or_delete_id::init() ]
            ;

        nested_simple_name
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (IDENTIFIER > *(DOT > IDENTIFIER)) [ typename SA::nested_simple_name::init() ]
            ;

        nested_name
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (id_expression > *(DOT > id_expression)) [ typename SA::nested_name::init() ]
            ;


        ///
        /// END BASIC
        /////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////
        /// BEGIN EXPRESSION
        ///
        /// 0.  POSTFIX_STEP
        /// 1.  UNARY_SIGN, PREFIX_STEP
        /// 2.  MULTIPLICATIVE
        /// 3.  ADDITIVE
        /// 4.  BITWISE_SHIFT
        /// 5.  RELATIONAL
        /// 6.  EQUALITY
        /// 7.  BITWISE_AND
        /// 8.  BITWISE_XOR
        /// 9.  BITWISE_OR
        /// 10. LOGICAL_AND
        /// 11. LOGICAL_OR
        /// 12. RANGE
        /// 13. TERNARY
        /// 14. ASSIGNMENT, MODIFY_ASSIGN
        /// 15. COMMA
        ///
        primary_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( IDENTIFIER                              [ typename SA::primary_expression::init_id() ]
                    | INTEGER_LITERAL                         [ typename SA::primary_expression::init_literal() ]
                    | FLOAT_LITERAL                           [ typename SA::primary_expression::init_literal() ]
                    | STRING_LITERAL                          [ typename SA::primary_expression::init_literal() ]
                    | _TRUE                                   [ typename SA::primary_expression::template init_bool<true>() ]
                    | _FALSE                                  [ typename SA::primary_expression::template init_bool<false>() ]
                    | _NULL                                   [ typename SA::primary_expression::template init_object_literal<tree::ObjectLiteral::LiteralType::NULL_OBJECT>() ]
                    | _THIS                                   [ typename SA::primary_expression::template init_object_literal<tree::ObjectLiteral::LiteralType::THIS_OBJECT>() ]
                    | SUPER                                   [ typename SA::primary_expression::template init_object_literal<tree::ObjectLiteral::LiteralType::SUPER_OBJECT>() ]
                    | (-(COMPARE_LT > arg_thor_type > COMPARE_GT) >> LEFT_BRACKET >> -(expression % COMMA) >> RIGHT_BRACKET) [ typename SA::primary_expression::init_array_expression() ]
                    | (LEFT_PAREN > tieable_expression > RIGHT_PAREN) [ typename SA::primary_expression::init_paren_expression() ]
                    |   (LAMBDA > LEFT_PAREN > -param_decl_list > RIGHT_PAREN > type_specifier > block
                        ) [ typename SA::primary_expression::init_lambda() ]
                    | ( (EMIT_BOOL(ISA) | EMIT_INT(CAST)) > COMPARE_LT > arg_thor_type > COMPARE_GT > LEFT_PAREN > expression > RIGHT_PAREN
                      ) [ typename SA::primary_expression::init_isa_or_cast() ]
                    )
            ;

        // postfix expression
        // associativity: left-to-right
        // rank: 0
        postfix_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( (simple_template_id                                              [ typename SA::primary_expression::init_id() ]
                         >>  +( (LEFT_BRACKET > expression > RIGHT_BRACKET)            [ typename SA::postfix_expression::append_postfix_array() ]
                              | (LEFT_PAREN > -(expression % COMMA) > RIGHT_PAREN) [ typename SA::postfix_expression::append_postfix_call() ]
                              | (DOT >> id_expression)                                 [ typename SA::postfix_expression::append_postfix_member() ]
                              )
                      )
                    | (primary_expression                                              [ typename SA::postfix_expression::init_primary_expression() ]
                         >>  *( (LEFT_BRACKET > expression > RIGHT_BRACKET)            [ typename SA::postfix_expression::append_postfix_array() ]
                              | (LEFT_PAREN > -(expression % COMMA) > RIGHT_PAREN) [ typename SA::postfix_expression::append_postfix_call() ]
                              | (DOT >> id_expression)                                 [ typename SA::postfix_expression::append_postfix_member() ]
                              )
                      )
                    )
            ;

        // components for new expression
        // rank: 0
        new_type_id 
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (id_expression                  [ typename SA::primary_expression::init_id() ]
                        > *(DOT >> id_expression)   [ typename SA::postfix_expression::append_postfix_member() ]
                    )
            ;

        new_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >> ( new_type_id 
                     > -(LEFT_PAREN > -(expression % COMMA) > RIGHT_PAREN)
                   ) [ typename SA::new_expression::init() ]
            ;

        // prefix expression
        // associativity: right-to-left
        // rank: 1
        prefix_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( (    NEW > qi::attr(tree::UnaryExpr::OpCode::NEW    ) >> new_expression     )
                    | (   ( ( INCREMENT        > qi::attr(tree::UnaryExpr::OpCode::INCREMENT) )
                          | ( DECREMENT        > qi::attr(tree::UnaryExpr::OpCode::DECREMENT) )
                          | ( BINARY_NOT       > qi::attr(tree::UnaryExpr::OpCode::BINARY_NOT) )
                          | ( LOGICAL_NOT      > qi::attr(tree::UnaryExpr::OpCode::LOGICAL_NOT) )
                          | ( ARITHMETIC_MINUS > qi::attr(tree::UnaryExpr::OpCode::ARITHMETIC_NEGATE) )
                          ) > prefix_expression
                      )
                    | postfix_expression
                    ) [ typename SA::prefix_expression::init() ]
            ;

        // multiplicative expression
        // associativity: left-to-right
        // rank: 2
        multiplicative_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (prefix_expression
                    %   ( ( ARITHMETIC_MUL > qi::attr(tree::BinaryExpr::OpCode::ARITHMETIC_MUL) )
                        | ( ARITHMETIC_DIV > qi::attr(tree::BinaryExpr::OpCode::ARITHMETIC_DIV) )
                        | ( ARITHMETIC_MOD > qi::attr(tree::BinaryExpr::OpCode::ARITHMETIC_MOD) )
                        ) [ typename SA::left_to_right_binary_op_vec::append_op() ]
                    ) [ typename SA::left_to_right_binary_op_vec::init() ]
            ;

        // additive expression
        // associativity: left-to-right
        // rank: 3
        additive_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (multiplicative_expression
                    %   ( ( ARITHMETIC_PLUS  > qi::attr(tree::BinaryExpr::OpCode::ARITHMETIC_ADD) )
                        | ( ARITHMETIC_MINUS > qi::attr(tree::BinaryExpr::OpCode::ARITHMETIC_SUB) )
                        ) [ typename SA::left_to_right_binary_op_vec::append_op() ]
                    ) [ typename SA::left_to_right_binary_op_vec::init() ]
            ;

        // shift expression
        // associativity: left-to-right
        // rank: 4
        shift_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (additive_expression
                    %   ( ( RSHIFT            > qi::attr(tree::BinaryExpr::OpCode::BINARY_RSHIFT) )
                        | ( LSHIFT            > qi::attr(tree::BinaryExpr::OpCode::BINARY_LSHIFT) )
                        ) [ typename SA::left_to_right_binary_op_vec::append_op() ]
                    ) [ typename SA::left_to_right_binary_op_vec::init() ]
            ;

        // rational expression
        // associativity: left-to-right
        // rank: 5
        relational_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (shift_expression
                    %   ( ( COMPARE_GT > qi::attr(tree::BinaryExpr::OpCode::COMPARE_GT) )
                        | ( COMPARE_LT > qi::attr(tree::BinaryExpr::OpCode::COMPARE_LT) )
                        | ( COMPARE_GE > qi::attr(tree::BinaryExpr::OpCode::COMPARE_GE) )
                        | ( COMPARE_LE > qi::attr(tree::BinaryExpr::OpCode::COMPARE_LE) )
                        ) [ typename SA::left_to_right_binary_op_vec::append_op() ]
                    ) [ typename SA::left_to_right_binary_op_vec::init() ]
            ;

        // equality expression
        // associativity: left-to-right
        // rank: 6
        equality_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (relational_expression
                    %   ( ( COMPARE_EQ > qi::attr(tree::BinaryExpr::OpCode::COMPARE_EQ) )
                        | ( COMPARE_NE > qi::attr(tree::BinaryExpr::OpCode::COMPARE_NE) )
                        ) [ typename SA::left_to_right_binary_op_vec::append_op() ]
                    ) [ typename SA::left_to_right_binary_op_vec::init() ]
            ;

        // and expression
        // associativity: left-to-right
        // rank: 7
        and_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (equality_expression % BINARY_AND
                    ) [ typename SA::left_to_right_binary_op::template init<tree::BinaryExpr::OpCode::BINARY_AND>() ]
            ;

        // xor expression
        // associativity: left-to-right
        // rank: 8
        xor_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (and_expression % BINARY_XOR
                    ) [ typename SA::left_to_right_binary_op::template init<tree::BinaryExpr::OpCode::BINARY_XOR>() ]
            ;

        // or expression
        // associativity: left-to-right
        // rank: 9
        or_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (xor_expression % BINARY_OR
                    ) [ typename SA::left_to_right_binary_op::template init<tree::BinaryExpr::OpCode::BINARY_OR>() ]
            ;

        // logical and expression
        // associativity: left-to-right
        // rank: 10
        logical_and_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (or_expression % LOGICAL_AND
                    ) [ typename SA::left_to_right_binary_op::template init<tree::BinaryExpr::OpCode::LOGICAL_AND>() ]
            ;

        // logical or expression
        // associativity: left-to-right
        // rank: 11
        logical_or_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (logical_and_expression % LOGICAL_OR
                    ) [ typename SA::left_to_right_binary_op::template init<tree::BinaryExpr::OpCode::LOGICAL_OR>() ]
            ;

        // range expression
        // associativity: left-to-right
        // rank: 12
        range_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (logical_or_expression > -(ELLIPSIS > logical_or_expression)
                    ) [ typename SA::range_expression::init() ]
            ;

        // ternary expression
        // associativity: right-to-left
        // rank: 13
        ternary_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (range_expression > -(Q_MARK > range_expression > COLON > range_expression)
                    ) [ typename SA::ternary_expression::init() ]
            ;

        // tie expression
        // associativity: left-to-right
        // rank: 14
        tie_expression
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (ternary_expression % COMMA
                    ) [ typename SA::tie_expression::init() ]
            ;

        // tieable_expression
        // associativity: right-to-left
        // rank: 15
        tieable_expression
            = location [ typename SA::location::cache_loc() ]
                >>  (tie_expression
                    %   ( ( ASSIGN                   > qi::attr(tree::BinaryExpr::OpCode::ASSIGN) )
                        | ( RSHIFT_ASSIGN            > qi::attr(tree::BinaryExpr::OpCode::RSHIFT_ASSIGN) )
                        | ( LSHIFT_ASSIGN            > qi::attr(tree::BinaryExpr::OpCode::LSHIFT_ASSIGN) )
                        | ( PLUS_ASSIGN              > qi::attr(tree::BinaryExpr::OpCode::ADD_ASSIGN) )
                        | ( MINUS_ASSIGN             > qi::attr(tree::BinaryExpr::OpCode::SUB_ASSIGN) )
                        | ( MUL_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::MUL_ASSIGN) )
                        | ( DIV_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::DIV_ASSIGN) )
                        | ( MOD_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::MOD_ASSIGN) )
                        | ( AND_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::AND_ASSIGN) )
                        | ( OR_ASSIGN                > qi::attr(tree::BinaryExpr::OpCode::OR_ASSIGN) )
                        | ( XOR_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::XOR_ASSIGN) )
                        ) [ typename SA::right_to_left_binary_op_vec::append_op() ]
                    ) [ typename SA::right_to_left_binary_op_vec::init() ]
            ;

        // expression
        // associativity: right-to-left
        // rank: 15
        expression
            = location [ typename SA::location::cache_loc() ]
                >>  (ternary_expression
                    %   ( ( ASSIGN                   > qi::attr(tree::BinaryExpr::OpCode::ASSIGN) )
                        | ( RSHIFT_ASSIGN            > qi::attr(tree::BinaryExpr::OpCode::RSHIFT_ASSIGN) )
                        | ( LSHIFT_ASSIGN            > qi::attr(tree::BinaryExpr::OpCode::LSHIFT_ASSIGN) )
                        | ( PLUS_ASSIGN              > qi::attr(tree::BinaryExpr::OpCode::ADD_ASSIGN) )
                        | ( MINUS_ASSIGN             > qi::attr(tree::BinaryExpr::OpCode::SUB_ASSIGN) )
                        | ( MUL_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::MUL_ASSIGN) )
                        | ( DIV_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::DIV_ASSIGN) )
                        | ( MOD_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::MOD_ASSIGN) )
                        | ( AND_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::AND_ASSIGN) )
                        | ( OR_ASSIGN                > qi::attr(tree::BinaryExpr::OpCode::OR_ASSIGN) )
                        | ( XOR_ASSIGN               > qi::attr(tree::BinaryExpr::OpCode::XOR_ASSIGN) )
                        ) [ typename SA::right_to_left_binary_op_vec::append_op() ]
                    ) [ typename SA::right_to_left_binary_op_vec::init() ]
            ;

        ///
        /// END EXPRESSION
        /////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////
        /// BEGIN STATEMENT
        ///
        statement
            =   (   (location [ typename SA::location::cache_loc() ]
                                >> ON_DOMAIN >> LEFT_PAREN >> (((IDENTIFIER % COMMA) >  ASSIGN > block) % COMMA) > RIGHT_PAREN
                            ) [ typename SA::statement::init_on_domain_block() ]
                |   (-annotation_list >> location [ typename SA::location::cache_loc() ]
                        >>  ( block
                            | async_block
                            | atomic_block
                            | flow_block
                            | lock_block
                            | pipeline_block
                            )
                    ) [ typename SA::statement::init_block() ]
                |   (-annotation_list >> location [ typename SA::location::cache_loc() ]
                        >>  ( decl_statement
                            | expression_statement
                            | selection_statement
                            | iteration_statement
                            | branch_statement
                            )
                    ) [ typename SA::statement::init() ]
                )
           ;

        decl_statement
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (-EMIT_BOOL(STATIC) >> (variable_decl_stmt | const_decl)) [ typename SA::decl_statement::init() ]
            ;

        expression_statement
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (-tieable_expression >> SEMICOLON) [ typename SA::expression_statement::init() ]
            ;

        selection
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (CASE > expression > COLON > braceless_block) [ typename SA::selection::init_selection() ]
            ;

        selection_statement
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (   (IF > LEFT_PAREN > expression > RIGHT_PAREN > block_or_one_stmt_block
                            > *(ELIF > LEFT_PAREN > expression > RIGHT_PAREN > block_or_one_stmt_block)
                            > -(ELSE > block_or_one_stmt_block)
                        ) [ typename SA::selection_statement::init_if_statement() ]
                    |   (SWITCH > LEFT_PAREN > expression > RIGHT_PAREN
                            > LEFT_BRACE
                            >   *( selection
                                 | ( DEFAULT > COLON > braceless_block )
                                 )
                            > RIGHT_BRACE
                        ) [ typename SA::selection_statement::init_switch_statement() ]
                    )
            ;

        iteration_statement
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (   (WHILE > LEFT_PAREN > expression > RIGHT_PAREN > block_or_one_stmt_block
                        ) [ typename SA::iteration_statement::init_while_loop() ]
                    |   (DO > block_or_one_stmt_block > WHILE > LEFT_PAREN > expression > RIGHT_PAREN > expect_semicolon
                        ) [ typename SA::iteration_statement::init_do_while_loop() ]
                    |   (FOR >> LEFT_PAREN >> VAR >> variable_decl_stem >> _IN >> expression >> RIGHT_PAREN >> block_or_one_stmt_block
                        ) [ typename SA::iteration_statement::init_foreach() ]
                    |   (FOR > LEFT_PAREN
                             > ( (-annotation_list >> variable_decl_stmt)
                               | (expression > expect_semicolon)
                               | expect_semicolon
                               )
                             > -expression > expect_semicolon > -expression > RIGHT_PAREN > block_or_one_stmt_block
                        ) [ typename SA::iteration_statement::init_for() ]
                    )
            ;

        branch_statement
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( (RETURN > -tieable_expression > expect_semicolon) [ typename SA::branch_statement::init_return() ]
                    | (BREAK > expect_semicolon)                        [ typename SA::branch_statement::template init<tree::BranchStmt::OpCode::BREAK>() ]
                    | (CONTINUE > expect_semicolon)                     [ typename SA::branch_statement::template init<tree::BranchStmt::OpCode::CONTINUE>() ]
                    )
            ;

        block
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (  LEFT_BRACE
                    > *statement
                    > RIGHT_BRACE [ typename SA::location::cache_loc() ]
                    )
                    [ typename SA::block::init_normal() ]
            ;

        async_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (
                      ASYNC
                   >> -(
                          LEFT_BRACKET
                       >> expression
                       >> qi::repeat(0, 2)[COMMA > expression]
                       >> RIGHT_BRACKET
                       )
                   >> qi::lit(L"->")
                   >> block_or_one_stmt_block_impl
                   ) [ typename SA::block::init_async() ]
            ;

        atomic_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (
                      ATOMIC
                   >> qi::lit(L"->")
                   >> block_or_one_stmt_block_impl
                   ) [ typename SA::block::template init_implicitable_block<tree::AtomicBlock>() ]
            ;

        flow_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (
                      FLOW
                   >> qi::lit(L"->")
                   >> block_or_one_stmt_block_impl
                   ) [ typename SA::block::template init_implicitable_block<tree::FlowBlock>() ]
            ;

        lock_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (
                      LOCK
                   >> -(
                           LEFT_BRACKET
                       >> IDENTIFIER
                       >> RIGHT_BRACKET
                       )
                   >> qi::lit(L"->")
                   >> block_or_one_stmt_block_impl
                   ) [ typename SA::block::init_lock() ]
            ;

        pipeline_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (
                      PIPELINE
                   >> qi::lit(L"->")
                   >> block_or_one_stmt_block_impl
                   ) [ typename SA::block::init_pipeline() ]
            ;

        optional_brace_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> statement [ typename SA::optional_brace_block::init() ]
            ;

        block_or_one_stmt_block
            =  qi::eps                      [ typename SA::location::cache_loc() ]
            >> block_or_one_stmt_block_impl [ typename SA::block::template init_implicitable_block<tree::NormalBlock>() ]
            ;

        block_or_one_stmt_block_impl
            =  qi::eps [ typename SA::location::cache_loc() ]
            >> (  LEFT_BRACE
               > *statement
               > RIGHT_BRACE  [ typename SA::location::cache_loc()             ]
               )              [ phx::at_c<0>(qi::_val) = qi::_1                ]
                              [ phx::at_c<1>(qi::_val) = false                 ]
            |  statement      [ phx::push_back(phx::at_c<0>(qi::_val), qi::_1) ]
                              [ phx::at_c<1>(qi::_val) = true                  ]
            ;

        braceless_block
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (*statement) [ typename SA::block::init_normal() ]
            ;

        ///
        /// END STATEMENT
        /////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////
        /// BEGIN DECLARATION
        ///

        global_decl
            =   (-annotation_list >> location //[ typename SA::location::cache_loc() ]
                    >>  ( variable_decl_stmt
                        | const_decl
                        | function_decl
                        | typedef_decl
                        | class_decl
                        | interface_decl
                        | enum_decl
                        )
                ) [ typename SA::global_decl::init() ]
            ;

        variable_decl_stem
            = location [ typename SA::location::cache_loc() ]
                >> (IDENTIFIER > -type_specifier) [ typename SA::variable_decl_stem::init() ]
            ;

        param_decl_with_init
            = (-annotation_list >> variable_decl_stem >> -expr_init_specifier) [ typename SA::param_decl_with_init::init() ]
            ;
        
        // a little different from 'param_decl_with_init', because annotation is not allowed to appear in 
        // variable declaration, so made a new rule individually
        variable_decl_with_init
            = (variable_decl_stem >> -expr_init_specifier) [ typename SA::variable_decl_with_init::init() ]
            ;

        variable_decl_stmt
            %= (VAR > variable_decl_with_init > expect_semicolon) 
            ;

        typename_decl
            = (-annotation_list > location [ typename SA::location::cache_loc() ] > IDENTIFIER > -type_initializer_or_specifier) [ typename SA::typename_decl::init() ]
            ;

        const_decl
            = (_CONST > variable_decl_stem > expr_init_specifier > SEMICOLON) [ typename SA::const_decl::init() ]
            ;

        function_decl
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  ( ( FUNCTION > qi::attr(false)
                      | TASK     > qi::attr(true )
                      )
                    > (template_param_identifier | new_or_delete_id)
                                              //   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                              //   avoid this attribute degenerate to 'optional(template_param_list)'
                                              //   it might a bug of spirit, when it degenerate,
                                              //   the semantic action of 'optional' will not be called
                    > LEFT_PAREN > -param_decl_with_init_list > RIGHT_PAREN > -multi_type_specifier
                    > (block | expect_semicolon)
                    ) [ typename SA::function_decl::init() ]
            ;

        member_function_decl
            = qi::eps [ typename SA::location::cache_loc() ]
                >> ( -( (VIRTUAL >> qi::attr(0)) | (OVERRIDE >> qi::attr(1)) ) >> function_decl) [ typename SA::member_function_decl::init() ];

        typedef_decl
            = qi::eps [ typename SA::location::cache_loc() ]
                >> (TYPEDEF > arg_thor_type > IDENTIFIER > expect_semicolon) [ typename SA::typedef_decl::init() ]
            ;

        class_decl
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (CLASS > template_param_identifier
                        > -(EXTENDS > nested_name) > -(IMPLEMENTS > (nested_name % COMMA))
                            > ((LEFT_BRACE > *class_member_decl > RIGHT_BRACE) | expect_semicolon)
                    ) [ typename SA::class_decl::init() ]
            ;

        class_member_decl
            =   (-annotation_list >> location //[ typename SA::location::cache_loc() ]
                    >> -class_member_visibility >> -EMIT_BOOL(STATIC)
                    >>  ( variable_decl_stmt
                        | const_decl
                        | member_function_decl
                        )
                ) [ typename SA::class_member_decl::init() ]
            ;

        interface_decl
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (INTERFACE > IDENTIFIER
                        > -(EXTENDS > (nested_name % COMMA))
                        > LEFT_BRACE
                        > *interface_member_function_decl
                        > RIGHT_BRACE
                    ) [ typename SA::interface_decl::init() ]
            ;

        interface_member_function_decl
            =   (-annotation_list >> location [ typename SA::location::cache_loc() ]
                    >> -interface_member_visibility
                    >> ( FUNCTION > qi::attr(false)
                       | TASK     > qi::attr(true )
                       )
                    >> ( qi::attr_cast<tree::Identifier*>(IDENTIFIER)
                       | new_or_delete_id
                       )
                    >> LEFT_PAREN >> -param_decl_list >> RIGHT_PAREN >> type_specifier > expect_semicolon
                ) [ typename SA::interface_member_function_decl::init() ]
            ;

        enum_decl
            = qi::eps [ typename SA::location::cache_loc() ]
                >>  (ENUM > IDENTIFIER
                        > LEFT_BRACE
                        > ((-annotation_list > IDENTIFIER > -expr_init_specifier) % COMMA)
                        > RIGHT_BRACE
                    ) [ typename SA::enum_decl::init() ]
            ;

        ///
        /// END DECLARATION
        /////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////
        /// BEGIN MODULE
        ///
        program
            =   *(  location [ typename SA::location::cache_loc() ] >>
                    (IMPORT >> -((IDENTIFIER | EMIT_BOOL(DOT)) >> ASSIGN) > nested_simple_name > expect_semicolon
                    ) [ typename SA::program::append_import() ] )
            >   *( global_decl [ typename SA::program::append_global_decl() ] )
            ;

        ///
        /// END MODULE
        /////////////////////////////////////////////////////////////////////

        start
            = location [ typename SA::start::reset() ]
                >> (program > qi::eoi)
            ;

        /////////////////////////////////////////////////////////////////////
        /// Debugging Names / Handlers
        /////////////////////////////////////////////////////////////////////

        // keywords
        _TRUE.name("TRUE_LITERAL");
        _FALSE.name("FALSE_LITERAL");
        _NULL.name("NULL_LITERAL");

        // terminals
        IDENTIFIER.name("IDENTIFIER.grammar");
        INTEGER_LITERAL.name("INTEGER_LITERAL.grammar");
        FLOAT_LITERAL.name("FLOAT_LITERAL.grammar");
        STRING_LITERAL.name("STRING_LITERAL.grammar");

        if(getParserContext().dump_rule_debug)
        {
            // keywords
            debug(_TRUE);
            debug(_FALSE);
            debug(_NULL);
        }

        // basic
        INIT_RULE(location);
        INIT_RULE(expect_semicolon);
        INIT_RULE(param_decl_list);
        INIT_RULE(param_decl_with_init_list);
        INIT_RULE(expr_init_specifier);
        INIT_RULE(type_initializer_or_specifier);
        INIT_RULE(arg_thor_type);
        INIT_RULE(id_expression);
        INIT_RULE(simple_template_id);
        INIT_RULE(template_param_identifier);
        INIT_RULE(template_param_list);
        INIT_RULE(type_specialize_specifier);
        INIT_RULE(type_arg_list);
        INIT_RULE(class_member_visibility);
        INIT_RULE(interface_member_visibility);
        INIT_RULE(annotation_list);
        INIT_RULE(annotation);
        INIT_RULE(annotation_body);
        INIT_RULE(new_or_delete_id);
        INIT_RULE(nested_simple_name);
        INIT_RULE(nested_name);

        // expression
        INIT_RULE(primary_expression);
        INIT_RULE(postfix_expression);
        INIT_RULE(new_type_id);
        INIT_RULE(new_expression);
        INIT_RULE(prefix_expression);
        INIT_RULE(multiplicative_expression);
        INIT_RULE(additive_expression);
        INIT_RULE(shift_expression);
        INIT_RULE(relational_expression);
        INIT_RULE(equality_expression);
        INIT_RULE(and_expression);
        INIT_RULE(xor_expression);
        INIT_RULE(or_expression);
        INIT_RULE(logical_and_expression);
        INIT_RULE(logical_or_expression);
        INIT_RULE(tie_expression);
        INIT_RULE(range_expression);
        INIT_RULE(ternary_expression);
        INIT_RULE(expression);
        INIT_RULE(tieable_expression);

        // statement
        INIT_RULE(statement);
        INIT_RULE(decl_statement);
        INIT_RULE(expression_statement);
        INIT_RULE(selection);
        INIT_RULE(selection_statement);
        INIT_RULE(iteration_statement);
        INIT_RULE(branch_statement);
        INIT_RULE(block);
        INIT_RULE(async_block);
        INIT_RULE(atomic_block);
        INIT_RULE(flow_block);
        INIT_RULE(lock_block);
        INIT_RULE(pipeline_block);
        INIT_RULE(optional_brace_block);
        INIT_RULE(block_or_one_stmt_block);
        INIT_RULE(block_or_one_stmt_block_impl);
        INIT_RULE(braceless_block);

        // global_decl
        INIT_RULE(global_decl);
        INIT_RULE(param_decl_with_init);
        INIT_RULE(variable_decl_with_init);
        INIT_RULE(variable_decl_stmt);
        INIT_RULE(typename_decl);
        INIT_RULE(const_decl);
        INIT_RULE(function_decl);
        INIT_RULE(member_function_decl);
        INIT_RULE(typedef_decl);
        INIT_RULE(class_decl);
        INIT_RULE(class_member_decl);
        INIT_RULE(interface_decl);
        INIT_RULE(interface_member_function_decl);
        INIT_RULE(enum_decl);

        // module
        INIT_RULE(program);

        // start
        INIT_RULE(start);
    }

    /////////////////////////////////////////////////////////////////////
    /// Rule Declare
    /////////////////////////////////////////////////////////////////////

    // operators
    qi::rule<Iterator, detail::WhiteSpace<Iterator> >
        ELLIPSIS, DOT, COLON, SEMICOLON, COMMA, AT_SYMBOL, Q_MARK,
        ASSIGN, RSHIFT_ASSIGN, LSHIFT_ASSIGN, PLUS_ASSIGN, MINUS_ASSIGN, MUL_ASSIGN, DIV_ASSIGN, MOD_ASSIGN, AND_ASSIGN, OR_ASSIGN, XOR_ASSIGN,
        INCREMENT, DECREMENT,
        ARITHMETIC_PLUS, ARITHMETIC_MINUS, ARITHMETIC_MUL, ARITHMETIC_DIV, ARITHMETIC_MOD,
        BINARY_AND, BINARY_OR, BINARY_XOR, BINARY_NOT,
        RSHIFT, LSHIFT,
        LOGICAL_AND, LOGICAL_OR, LOGICAL_NOT,
        COMPARE_EQ, COMPARE_NE, COMPARE_GT, COMPARE_LT, COMPARE_GE, COMPARE_LE,
        LEFT_BRACE, RIGHT_BRACE, LEFT_BRACKET, RIGHT_BRACKET, LEFT_PAREN, RIGHT_PAREN;

    // keywords
    qi::rule<Iterator, detail::WhiteSpace<Iterator> >
        _TRUE, _FALSE, _NULL, _THIS, SUPER,
        _CONST, STATIC, VIRTUAL, OVERRIDE,
        INT8, INT16, INT32, INT64, FLOAT32, FLOAT64, _VOID, _BOOLEAN,
        TYPEDEF, CLASS, INTERFACE, ENUM,
        PUBLIC, PROTECTED, PRIVATE,
        VAR, FUNCTION, LAMBDA, TASK,
        IF, ELIF, ELSE,
        SWITCH, CASE, DEFAULT,
        WHILE, 
        DO, 
        _IN, 
        FOR,
        RETURN, BREAK, CONTINUE,
        NEW, DELETE, ISA, CAST,
        IMPORT,
        EXTENDS, IMPLEMENTS,
        PIPELINE, ASYNC, ATOMIC, FLOW, LOCK,
        ON_DOMAIN
    ;

    // terminals
    detail::Identifier<Iterator, SA>     IDENTIFIER;
    detail::IntegerLiteral<Iterator, SA> INTEGER_LITERAL;
    detail::FloatLiteral<Iterator, SA>   FLOAT_LITERAL;
    detail::StringLiteral<Iterator, SA>  STRING_LITERAL;

    // location
    DECL_RULE_LEXEME(location);

    // operator
    DECL_RULE(           expect_semicolon);

    // basic
    DECL_RULE_CUSTOM_SA( param_decl_list, variable_decl_list);
    DECL_RULE_CUSTOM_SA( param_decl_with_init_list, variable_decl_list);
    DECL_RULE(           expr_init_specifier);
    DECL_RULE(           type_initializer_or_specifier);
    DECL_RULE(           type_specifier);
    DECL_RULE_CUSTOM_SA( multi_type_specifier, type_specifier);
    DECL_RULE(           multi_type_specifier_impl);
    DECL_RULE(           arg_thor_non_qualified_type);
    DECL_RULE(           arg_thor_type);
    DECL_RULE(           id_expression);
    DECL_RULE(           simple_template_id);
    DECL_RULE(           template_param_identifier);
    DECL_RULE( 			 template_param_list);
    DECL_RULE_CUSTOM_SA( type_specialize_specifier, type_arg_list);
    DECL_RULE(           type_arg_list);
    DECL_RULE(           class_member_visibility);
    DECL_RULE_CUSTOM_SA( interface_member_visibility, class_member_visibility );
    DECL_RULE(           annotation_list);
    DECL_RULE(           annotation);
    DECL_RULE(           annotation_body);
    DECL_RULE(           new_or_delete_id);
    DECL_RULE(           nested_simple_name);
    DECL_RULE(           nested_name);

    // expression
    DECL_RULE(           primary_expression);
    DECL_RULE(           postfix_expression);
    DECL_RULE_CUSTOM_SA( new_type_id,               postfix_expression);
    DECL_RULE(           new_expression);
    DECL_RULE(           prefix_expression);
    DECL_RULE_CUSTOM_SA( multiplicative_expression, left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( additive_expression,       left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( shift_expression,          left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( relational_expression,     left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( equality_expression,       left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( and_expression,            left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( xor_expression,            left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( or_expression,             left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( logical_and_expression,    left_to_right_binary_op_vec );
    DECL_RULE_CUSTOM_SA( logical_or_expression,     left_to_right_binary_op_vec );
    DECL_RULE(           tie_expression);
    DECL_RULE(           range_expression);
    DECL_RULE(           ternary_expression);
    DECL_RULE_CUSTOM_SA( expression,                right_to_left_binary_op_vec );
    DECL_RULE_CUSTOM_SA( tieable_expression,        right_to_left_binary_op_vec );

    // statement
    DECL_RULE(statement);
    DECL_RULE(decl_statement);
    DECL_RULE(expression_statement);
    DECL_RULE(selection);
    DECL_RULE(selection_statement);
    DECL_RULE(iteration_statement);
    DECL_RULE(branch_statement);
    DECL_RULE(block);
    DECL_RULE_CUSTOM_SA(async_block   , block);
    DECL_RULE_CUSTOM_SA(atomic_block  , block);
    DECL_RULE_CUSTOM_SA(flow_block  , block);
    DECL_RULE_CUSTOM_SA(lock_block    , block);
    DECL_RULE_CUSTOM_SA(pipeline_block, block);
    DECL_RULE(optional_brace_block);
    DECL_RULE_CUSTOM_SA(block_or_one_stmt_block, block);
    DECL_RULE(block_or_one_stmt_block_impl);
    DECL_RULE_CUSTOM_SA(braceless_block, block);

    // global_decl
    DECL_RULE(global_decl);
    DECL_RULE(variable_decl_stem);
    DECL_RULE(param_decl_with_init);
    DECL_RULE(variable_decl_with_init);
    DECL_RULE(variable_decl_stmt);
    DECL_RULE(typename_decl);
    DECL_RULE(const_decl);
    DECL_RULE(function_decl);
    DECL_RULE(member_function_decl);
    DECL_RULE(typedef_decl);
    DECL_RULE(class_decl);
    DECL_RULE(class_member_decl);
    DECL_RULE(interface_decl);
    DECL_RULE(interface_member_function_decl);
    DECL_RULE(enum_decl);

    // module
    DECL_RULE(program);

    // start
    DECL_RULE(start);
};

} } }

#endif /* ZILLIANS_LANGUAGE_GRAMMAR_THOR_H_ */
