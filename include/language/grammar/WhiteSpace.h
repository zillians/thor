/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_GRAMMAR_WHITESPACE_H_
#define ZILLIANS_LANGUAGE_GRAMMAR_WHITESPACE_H_

#define BOOST_SPIRIT_UNICODE

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_as.hpp>
#include <boost/spirit/include/qi_char_class.hpp>
#include <boost/spirit/home/support/char_encoding/unicode.hpp>
#include <boost/regex/pending/unicode_iterator.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>

namespace qi = boost::spirit::qi;
namespace unicode = boost::spirit::unicode;

namespace zillians { namespace language { namespace grammar {

/////////////////////////////////////////////////////////////////////
/// detail
/////////////////////////////////////////////////////////////////////

namespace detail {

template <typename Iterator>
struct WhiteSpace : qi::grammar<Iterator>
{
    WhiteSpace() : WhiteSpace::base_type(start)
    {
        comment_c_style = qi::lexeme[L"/*" > *(unicode::char_ - L"*/") > L"*/"];
        comment_c_style.name("comment_in_c_style");

        comment_cpp_style = qi::lexeme[L"//" > *(unicode::char_ - qi::eol) > qi::eol];
        comment_cpp_style.name("comment_in_cpp_style");

        start
            = unicode::space    // tab/space/cr/lf
            | comment_c_style   // c-style comment "/* */"
            | comment_cpp_style // cpp-style comment "//"
            ;

        start.name("WHITESPACE");
    }

    qi::rule<Iterator> start;
    qi::rule<Iterator> comment_c_style;
    qi::rule<Iterator> comment_cpp_style;
};

} // detail

} } } // zillians language grammar

#endif /*ZILLIANS_LANGUAGE_GRAMMAR_WHITESPACE_H_ */

