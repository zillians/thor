/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_SEMANTICACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_SEMANTICACTIONS_H_

#include "utility/Foreach.h"
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/repository/include/qi_iter_pos.hpp>
#include "language/tree/ASTNodeFactory.h"
#include <boost/mpl/bool.hpp>
#include <boost/fusion/container/vector.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/add_reference.hpp>

// prerequisite
#include "language/action/detail/SemanticActionsDetail.h"

// basic
#include "language/action/basic/BasicActions.h"
#include "language/action/basic/IdentifierActions.h"
#include "language/action/basic/LiteralActions.h"
#include "language/action/basic/SpecifierActions.h"

// non-basic
#include "language/action/nonbasic/DeclarationActions.h"
#include "language/action/nonbasic/ExpressionActions.h"
#include "language/action/nonbasic/ProgramActions.h"
#include "language/action/nonbasic/StatementActions.h"

namespace zillians { namespace language { namespace action {

struct ThorTreeAction
{
    //////////////////////////////////////////////////////////////////////
    /// Semantic Actions for Terminals

    // terminals
    typedef action::identifier      identifier;
    typedef action::integer_literal integer_literal;
    typedef action::float_literal   float_literal;
    typedef action::string_literal  string_literal;

    // basicvariable_decl_list
    typedef action::location                          location;
    typedef action::expect_semicolon                  expect_semicolon;
    typedef action::variable_decl_list                variable_decl_list;
    typedef action::expr_init_specifier               expr_init_specifier;
    typedef action::type_initializer_or_specifier     type_initializer_or_specifier;
    typedef action::type_specifier                    type_specifier;
    typedef action::multi_type_specifier_impl         multi_type_specifier_impl;
    typedef action::arg_thor_non_qualified_type       arg_thor_non_qualified_type;
    typedef action::arg_thor_type                     arg_thor_type;
    typedef action::id_expression                     id_expression;
    typedef action::simple_template_id                simple_template_id; 
    typedef action::template_param_identifier         template_param_identifier;
    typedef action::template_param_list               template_param_list;
    typedef action::template_arg_identifier           template_arg_identifier;
    typedef action::type_list                         type_list;
    typedef action::type_arg_list                     type_arg_list;
    typedef action::class_member_visibility           class_member_visibility;
    typedef action::annotation_list                   annotation_list;
    typedef action::annotation                        annotation;
    typedef action::annotation_body                   annotation_body;
    typedef action::new_or_delete_id                  new_or_delete_id;
    typedef action::nested_simple_name                nested_simple_name;
    typedef action::nested_name                       nested_name;

    //////////////////////////////////////////////////////////////////////
    /// Semantic Actions for Non-terminals

    // expression
    typedef action::primary_expression          primary_expression;
    typedef action::postfix_expression          postfix_expression;
    typedef action::new_expression              new_expression;
    typedef action::prefix_expression           prefix_expression;
    typedef action::left_to_right_binary_op_vec left_to_right_binary_op_vec;
    typedef action::right_to_left_binary_op_vec right_to_left_binary_op_vec;
    typedef action::left_to_right_binary_op     left_to_right_binary_op;
#if 0 // NOTE: unused
    typedef action::right_to_left_binary_op     right_to_left_binary_op;
#endif
    typedef action::tie_expression              tie_expression;
    typedef action::range_expression            range_expression;
    typedef action::ternary_expression          ternary_expression;

    // global_decl
    typedef action::global_decl                    global_decl;
    typedef action::variable_decl_stem             variable_decl_stem;
    typedef action::param_decl_with_init           param_decl_with_init;
    typedef action::variable_decl_with_init        variable_decl_with_init;
    typedef action::variable_decl_stmt             variable_decl_stmt;
    typedef action::typename_decl                  typename_decl;
    typedef action::const_decl                     const_decl;
    typedef action::function_decl                  function_decl;
    typedef action::member_function_decl           member_function_decl;
    typedef action::typedef_decl                   typedef_decl;
    typedef action::class_decl                     class_decl;
    typedef action::class_member_decl              class_member_decl;
    typedef action::interface_decl                 interface_decl;
    typedef action::interface_member_function_decl interface_member_function_decl;
    typedef action::enum_decl                      enum_decl;

    // module
    typedef action::program program;

    // statement
    typedef action::statement                    statement;
    typedef action::decl_statement               decl_statement;
    typedef action::block                        block;
    typedef action::optional_brace_block         optional_brace_block;
    typedef action::block_or_one_stmt_block_impl block_or_one_stmt_block_impl;
    typedef action::expression_statement         expression_statement;
    typedef action::selection                    selection;
    typedef action::selection_statement          selection_statement;
    typedef action::iteration_statement          iteration_statement;
    typedef action::branch_statement             branch_statement;

    /**
     * start is the entry rule for the Thor parser
     */
    struct start
    {
        DEFINE_ATTRIBUTES(void)
        DEFINE_LOCALS(LOCATION_TYPE)

        BEGIN_ACTION(reset)
        {
#ifdef DEBUG
            printf("start::reset param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
            BIND_CACHED_LOCATION(getParserContext().active_source);
            //getParserContext().active_package = getParserContext().active_source->root;
        }
        END_ACTION
    };
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_SEMANTICACTIONS_H_ */
