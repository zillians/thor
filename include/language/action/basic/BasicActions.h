/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_BASIC_BASICACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_BASIC_BASICACTIONS_H_

#include <utility>
#include <vector>

#include <boost/assert.hpp>

#include "language/action/detail/SemanticActionsDetail.h"

namespace zillians { namespace language { namespace action {

using ImplicitableStatements = boost::fusion::vector<
    std::vector<tree::Statement*>, /* statements */
    bool                           /* is_implicit block */
>;

struct location
{
    DEFINE_ATTRIBUTES(void)
    DEFINE_LOCALS()

    BEGIN_ACTION(cache_loc)
    {
        CACHE_LOCATION;
    }
    END_ACTION

    BEGIN_ACTION(init) // NOTE: _result_t will not be "void" if used by other SA
    {
#ifdef DEBUG
        printf("location param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        getParserContext().debug.line   = _param(0).get_position().line;
        getParserContext().debug.column = _param(0).get_position().column;
    }
    END_ACTION
};

struct expect_semicolon
{
    DEFINE_ATTRIBUTES(void)
    DEFINE_LOCALS(LOCATION_TYPE)
};

struct block
{
    DEFINE_ATTRIBUTES(tree::Block*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_normal)
    {
#ifdef DEBUG
        printf("block param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new NormalBlock());
        std::vector<Statement*>& stmts = _param(0);
        _result->appendObjects(stmts);
    }
    END_ACTION

    BEGIN_ACTION(init_async)
    {
        using boost::fusion::at_c;

        if (_param(0).is_initialized())
        {
            Expression* target = nullptr;
            Expression* block  = nullptr;
            Expression* grid   = nullptr;

            BOOST_ASSERT(at_c<1>(*_param(0)).size() <= 2 && "unexpected amount of expression");

            switch (at_c<1>(*_param(0)).size())
            {
            default:
                target = at_c<0>(*_param(0));
                break;
            case 1 :
                block  = at_c<0>(*_param(0));
                grid   = at_c<1>(*_param(0))[0];
                break;
            case 2 :
                target = at_c<0>(*_param(0));
                block  = at_c<1>(*_param(0))[0];
                grid   = at_c<1>(*_param(0))[1];
                break;
            }

            _result = new AsyncBlock(
                Architecture::zero(),
                target,
                block ,
                grid
            );
        }
        else
        {
            _result = new AsyncBlock();
        }

        {
            const auto& stmts       = at_c<0>(_param(1));
            const auto& is_implicit = at_c<1>(_param(1));

            _result->appendObjects(stmts);

            if (is_implicit)
                _result->setImplicit(true);
        }

        BIND_CACHED_LOCATION(_result);
    }
    END_ACTION

    BEGIN_TEMPLATED_ACTION(init_implicitable_block, typename BlockType)
    {
        using boost::fusion::at_c;

        const auto& stmts       = _param(0);
        const auto& is_implicit = _param(1);

        auto*const block = new BlockType;

        block->appendObjects(stmts);

        if (is_implicit)
            block->setImplicit(true);

        BIND_CACHED_LOCATION(_result = block);
    }
    END_ACTION

    BEGIN_ACTION(init_lock)
    {
        // TODO implement it
        BIND_CACHED_LOCATION(_result = new LockBlock());
    }
    END_ACTION

    BEGIN_ACTION(init_pipeline)
    {
        BIND_CACHED_LOCATION(_result = new PipelineBlock());
    }
    END_ACTION
};

struct optional_brace_block
{
    DEFINE_ATTRIBUTES(tree::Block*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("optional_brace_block param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        if(_param(0) == NULL)
            _result = NULL;
        else if(Statement* stmt = cast<Statement>(_param(0)))
        {
            BIND_CACHED_LOCATION(_result = new NormalBlock());
            _result->appendObject(stmt);
        }
        else
        {
            UNREACHABLE_CODE();
        }
    }
    END_ACTION
};

struct block_or_one_stmt_block_impl
{
    DEFINE_ATTRIBUTES(ImplicitableStatements)
    DEFINE_LOCALS(LOCATION_TYPE)
};

struct variable_decl_list
{
    DEFINE_ATTRIBUTES(std::vector<tree::VariableDecl*>)
    DEFINE_LOCALS()
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_BASIC_BASICACTIONS_H_ */
