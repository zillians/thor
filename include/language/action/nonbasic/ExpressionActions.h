/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_DECLARATION_EXPRESSIONACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_DECLARATION_EXPRESSIONACTIONS_H_

#include <vector>

#include <boost/assert.hpp>
#include <boost/range/adaptor/reversed.hpp>

#include "core/Common.h"
#include "utility/StringUtil.h"
#include "language/action/detail/SemanticActionsDetail.h"

#define LEFT_TO_RIGHT(op_code) \
    { \
        using std::begin; \
        using std::end; \
        if(_param(0).size() == 1) \
            _result = *begin(_param(0)); \
        else { \
            auto next_expr = begin(_param(0)); \
            auto* left = *next_expr++; \
            for(; next_expr != end(_param(0)); ++next_expr) \
                BIND_CACHED_LOCATION(left = new BinaryExpr(op_code, left, *next_expr)); \
            _result = left; \
        } \
    }

#define LEFT_TO_RIGHT_VEC(op_code_vec) \
    { \
        using std::begin; \
        using std::end; \
        if(_param(0).size() == 1) \
            _result = *begin(_param(0)); \
        else { \
            auto next_op = begin(op_code_vec); \
            auto next_expr = begin(_param(0)); \
            auto* left = *next_expr++; \
            for(; next_expr != end(_param(0)); ++next_expr) { \
                BIND_CACHED_LOCATION(left = new BinaryExpr(*next_op, left, *next_expr)); \
                ++next_op; \
            } \
            _result = left; \
        } \
    }

#define RIGHT_TO_LEFT_VEC(op_code_vec) \
    { \
        using std::begin; \
        using std::end; \
        if(_param(0).size() == 1) \
            _result = *begin(_param(0)); \
        else { \
            auto next_op = begin(op_code_vec | boost::adaptors::reversed); \
            auto reversed_exprs = _param(0) | boost::adaptors::reversed; \
            auto next_expr = reversed_exprs.begin(); \
            auto* right = *next_expr++; \
            for(; next_expr != reversed_exprs.end(); ++next_expr) { \
                BIND_CACHED_LOCATION(right = new BinaryExpr(*next_op, *next_expr, right)); \
                next_op++; \
            } \
            _result = right; \
        } \
    }

namespace zillians { namespace language { namespace action {

typedef std::vector<tree::BinaryExpr::OpCode::type> binary_ops_t;

struct primary_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_id)
    {
#ifdef DEBUG
        printf("primary_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new IdExpr(_param(0)));
    }
    END_ACTION

    BEGIN_ACTION(init_literal)
    {
#ifdef DEBUG
        printf("primary_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = _param(0));
    }
    END_ACTION

    BEGIN_TEMPLATED_ACTION(init_bool, bool Value)
    {
#ifdef DEBUG
        printf("primary_expression::init_bool param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new NumericLiteral(Value));
    }
    END_ACTION

    BEGIN_TEMPLATED_ACTION(init_object_literal, tree::ObjectLiteral::LiteralType::type Type)
    {
#ifdef DEBUG
        printf("primary_expression::init_object_literal param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new ObjectLiteral(Type));
    }
    END_ACTION

    BEGIN_ACTION(init_array_expression)
    {
#ifdef DEBUG
        printf("primary_expression::init_array_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new ArrayExpr());
        if(_param(0).is_initialized())
        {
            cast<ArrayExpr>(_result)->setElementType(*_param(0));
        }
        if(_param(1).is_initialized())
        {
            for(auto& elem : *_param(1))
                cast<ArrayExpr>(_result)->appendElement(elem);
        }
    }
    END_ACTION

    BEGIN_ACTION(init_paren_expression)
    {
#ifdef DEBUG
        printf("primary_expression::init_paren_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        _result = _param(0);
    }
    END_ACTION

    BEGIN_ACTION(init_lambda)
    {
#ifdef DEBUG
        printf("primary_expression::lambda_expression param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("primary_expression::lambda_expression param(1) type = %s\n", typeid(_param_t(1)).name());
        printf("primary_expression::lambda_expression param(2) type = %s\n", typeid(_param_t(2)).name());
#endif
        std::vector<VariableDecl*>*            parameters  = _param(0).is_initialized() ? &(*_param(0)) : NULL;
        TypeSpecifier*                         return_type = _param(1);
        Declaration::VisibilitySpecifier::type visibility  = Declaration::VisibilitySpecifier::PUBLIC;
        bool                                   is_member   = false;
        bool                                   is_static   = false;
        bool                                   is_virtual  = false;
        bool                                   is_override = false;
        bool                                   is_lambda   = true;

        SimpleIdentifier* lambda_name = new SimpleIdentifier(L""); BIND_CACHED_LOCATION(lambda_name);
        FunctionDecl* function_decl = new FunctionDecl(
            lambda_name, return_type,
            is_member, is_static, is_virtual, is_override, visibility, _param(2)
        );
        function_decl->is_lambda = is_lambda;
        BIND_CACHED_LOCATION(function_decl);

        if(parameters)
        {
            for(auto& param : *parameters)
                function_decl->appendParameter(param);
        }
        BIND_CACHED_LOCATION(_result = new LambdaExpr(function_decl));
    }
    END_ACTION

    BEGIN_ACTION(init_isa_or_cast)
    {
#ifdef DEBUG
        printf("primary_expression::lambda_expression param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("primary_expression::lambda_expression param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        TypeSpecifier* type = _param(1);
        Expression*    expr = _param(2);

        switch(_param(0).which())
        {
        case 0: _result = new IsaExpr(expr, type); break;
        case 1: _result = new CastExpr(expr, type); break;
        default: UNREACHABLE_CODE(); break;
        }

        BIND_CACHED_LOCATION(_result);
    }
    END_ACTION
};

struct postfix_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init_primary_expression)
    {
#ifdef DEBUG
        printf("postfix_expression::init_primary_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        _result = _param(0);
    }
    END_ACTION

    BEGIN_ACTION(append_postfix_array)
    {
#ifdef DEBUG
        printf("postfix_expression::init_postfix_array param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
//        BIND_CACHED_LOCATION(_result = new BinaryExpr(BinaryExpr::OpCode::ARRAY_SUBSCRIPT, _result, _param(0)));
        BIND_CACHED_LOCATION(_result = new IndexExpr(_result, _param(0)));
    }
    END_ACTION

    BEGIN_ACTION(append_postfix_call)
    {
#ifdef DEBUG
        printf("postfix_expression::init_postfix_call param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new CallExpr(_result));
        if(_param(0).is_initialized())
        {
            for(auto& param : *_param(0))
                cast<CallExpr>(_result)->appendParameter(param);
        }
    }
    END_ACTION

    BEGIN_ACTION(append_postfix_member)
    {
#ifdef DEBUG
        printf("postfix_expression::init_postfix_member param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BIND_CACHED_LOCATION(_result = new MemberExpr(_result, _param(0)));
    }
    END_ACTION
};

struct new_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("new_expression::init param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("new_expression::init param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        BIND_CACHED_LOCATION( _result = new CallExpr(_param(0)));
        if(_param(1).is_initialized())
        {
            for(auto& param : *_param(1))
                cast<CallExpr>(_result)->appendParameter(param);
        }
    }
    END_ACTION
};

struct prefix_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("prefix_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        switch(_param(0).which())
        {
        case 0:
            {
                typedef boost::fusion::vector2<UnaryExpr::OpCode::type, Expression*> fusion_vec_t;
                fusion_vec_t& vec = boost::get<fusion_vec_t>(_param(0));
                UnaryExpr::OpCode::type type = boost::fusion::at_c<0>(vec);
                Expression*             expr = boost::fusion::at_c<1>(vec);
                BIND_CACHED_LOCATION(_result = new UnaryExpr(type, expr));
            }
            break;
        case 1:
            _result = boost::get<Expression*>( _param(0) );
            break;
        default:
            _result = NULL;
        }
    }
    END_ACTION
};

struct left_to_right_binary_op_vec
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE, shared_ptr<binary_ops_t>)

    BEGIN_ACTION(append_op)
    {
#ifdef DEBUG
        printf("left_to_right_binary_op_vec::append_op param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        if(!_local(1))
            _local(1).reset(new binary_ops_t());
        _local(1)->push_back(_param(0));
    }
    END_ACTION

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("left_to_right_binary_op_vec param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        LEFT_TO_RIGHT_VEC(*_local(1));
    }
    END_ACTION
};

struct right_to_left_binary_op_vec
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE, shared_ptr<binary_ops_t>)

    BEGIN_ACTION(append_op)
    {
#ifdef DEBUG
        printf("right_to_left_binary_op_vec::append_op param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        if(!_local(1))
            _local(1).reset(new binary_ops_t());
        _local(1)->push_back(_param(0));
    }
    END_ACTION

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("right_to_left_binary_op_vec param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        RIGHT_TO_LEFT_VEC(*_local(1));
    }
    END_ACTION
};

struct left_to_right_binary_op
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_TEMPLATED_ACTION(init, tree::BinaryExpr::OpCode::type Type)
    {
#ifdef DEBUG
        printf("left_to_right_binary_op param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        LEFT_TO_RIGHT(Type);
    }
    END_ACTION
};

struct range_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("range_expression param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("range_expression param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(_param(1).is_initialized())
        {
            BIND_CACHED_LOCATION(
                _result = new BinaryExpr(BinaryExpr::OpCode::RANGE_ELLIPSIS, _param(0), *_param(1))
            ) // NOTE: omit SEMICOLON
        }
        else
        {
            _result = _param(0);
        }
    }
    END_ACTION
};

struct ternary_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("ternary_expression param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("ternary_expression param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(_param(1).is_initialized())
        {
            Expression* true_node  = boost::fusion::at_c<0>(*_param(1));
            Expression* false_node = boost::fusion::at_c<1>(*_param(1));
            BIND_CACHED_LOCATION(_result = new TernaryExpr(_param(0), true_node, false_node));
        }
        else
            _result = _param(0);
    }
    END_ACTION
};

struct tie_expression
{
    DEFINE_ATTRIBUTES(tree::Expression*)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(init)
    {
#ifdef DEBUG
        printf("tie_expression param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        BOOST_ASSERT((_param(0).size() > 0) && "no expression after parsing!");

        if (_param(0).size() == 1)
        {
            _result = _param(0).front();
        }
        else
        {
            _result = new TieExpr(_param(0));
        }

        BIND_CACHED_LOCATION(_result);
    }
    END_ACTION
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_DECLARATION_EXPRESSIONACTIONS_H_ */
