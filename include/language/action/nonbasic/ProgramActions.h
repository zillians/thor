/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_MODULE_PROGRAMACTIONS_H_
#define ZILLIANS_LANGUAGE_ACTION_MODULE_PROGRAMACTIONS_H_

#include "language/action/detail/SemanticActionsDetail.h"

namespace zillians { namespace language { namespace action {

struct program
{
    DEFINE_ATTRIBUTES(void)
    DEFINE_LOCALS(LOCATION_TYPE)

    BEGIN_ACTION(append_import)
    {
#ifdef DEBUG
        printf("program::append_import param(0) type = %s\n", typeid(_param_t(0)).name());
        printf("program::append_import param(1) type = %s\n", typeid(_param_t(1)).name());
#endif
        if(getParserContext().active_source)
        {
            Import* import = nullptr;
            if(_param(0).is_initialized())
            {
                Identifier* ident = nullptr;
                switch((*_param(0)).which())
                {
                case 0: ident = boost::get<SimpleIdentifier*>(*_param(0)); break;
                case 1: ident = new SimpleIdentifier(L""); break;
                }
                import = new Import(ident, _param(1));
                BIND_CACHED_LOCATION(import);
            }
            else
            {
                import = new Import(_param(1));
                BIND_CACHED_LOCATION(import);
            }
            getParserContext().active_source->addImport(import);
        }
    }
    END_ACTION

    BEGIN_ACTION(append_global_decl)
    {
#ifdef DEBUG
        printf("program::append_global_decl param(0) type = %s\n", typeid(_param_t(0)).name());
#endif
        if(getParserContext().active_source)
            getParserContext().active_source->addDeclare(_param(0));
    }
    END_ACTION
};

} } }

#endif /* ZILLIANS_LANGUAGE_ACTION_MODULE_PROGRAMACTIONS_H_ */
