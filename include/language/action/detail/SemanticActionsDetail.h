/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_LANGUAGE_ACTION_DETAIL_SEMANTICACTIONSDETAIL_H_
#define ZILLIANS_LANGUAGE_ACTION_DETAIL_SEMANTICACTIONSDETAIL_H_

#include <boost/mpl/bool.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/fusion/container/vector.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/type_traits/add_reference.hpp>

#include "core/Common.h"
#include "utility/Foreach.h"
#include "language/context/ParserContext.h"
#include "language/stage/parser/context/SourceInfoContext.h"
#include "language/tree/ASTNodeFactory.h"

#define DEFINE_ATTRIBUTES(return_type) typedef return_type (attribute_type) ();
#define DEFINE_LOCALS(...) typedef boost::spirit::locals<__VA_ARGS__> local_type;
#define DEFINE_ACTION(name) \
        struct name \
        { \
            template<typename Attribute, typename Context> \
            void operator()(Attribute& parser_attribute, Context& context, qi::unused_type) const; \
        };

#define BEGIN_ACTION(name) \
        struct name \
        { \
            template<typename ParserAttribute, typename ParserContext> \
            void operator()(ParserAttribute& parser_attribute, ParserContext& context, qi::unused_type) const \
            { \
                using namespace language::tree; \
                { \
                    UNUSED_ARGUMENT(parser_attribute); \
                    UNUSED_ARGUMENT(context); \
                    if(!getParserContext().enable_semantic_action) \
                        return;

#define BEGIN_TEMPLATED_ACTION(name, ...) \
        template<__VA_ARGS__> \
        struct name \
        { \
            template<typename ParserAttribute, typename ParserContext> \
            void operator()(ParserAttribute& parser_attribute, ParserContext& context, qi::unused_type) const \
            { \
                using namespace language::tree; \
                { \
                    UNUSED_ARGUMENT(parser_attribute); \
                    UNUSED_ARGUMENT(context); \
                    if(!getParserContext().enable_semantic_action) \
                        return;

#define END_ACTION \
                } \
            } \
        };

#define _result   boost::fusion::at_c<0>(context.attributes)
#define _result_t decltype(boost::fusion::at_c<0>(context.attributes))

#define _param(i)   detail::attribute_accessor<i, ParserAttribute>::get(parser_attribute)
#define _param_t(i) typename detail::attribute_accessor<i, ParserAttribute>::result_type

#define _aux_param(i)   boost::fusion::at_c<i+1>(context.attributes)
#define _aux_param_t(i) decltype(boost::fusion::at_c<i+1>(context.attributes))

#define _local(i)   boost::fusion::at_c<i>(context.locals)
#define _local_t(i) decltype(boost::fusion::at_c<i>(context.locals))

#define LOCATION_TYPE shared_ptr<stage::SourceInfoContext> // _local(0)
#define CACHE_LOCATION \
        { \
            BOOST_MPL_ASSERT(( boost::is_same<_local_t(0), LOCATION_TYPE&> )); \
            _local(0).reset(new stage::SourceInfoContext( \
                    getParserContext().debug.line, \
                    getParserContext().debug.column)); \
        }
#define BIND_CACHED_LOCATION(x) \
        { \
            BOOST_MPL_ASSERT(( boost::is_same<_local_t(0), LOCATION_TYPE&> )); \
            if(_local(0)) \
                stage::SourceInfoContext::set((x), new stage::SourceInfoContext(*(_local(0).get()))); \
        }

namespace zillians { namespace language { namespace action { namespace detail {

template<typename T>
struct is_fusion_vector : boost::is_base_of<boost::fusion::sequence_base<T>, T>
{ };

template<typename T>
struct is_std_vector : boost::mpl::false_
{ };

template<typename _Tp, typename _Alloc>
struct is_std_vector<std::vector<_Tp, _Alloc>> : boost::mpl::true_
{ };

template<int N, typename Attribute, bool IsFusionVector>
struct attribute_accessor_impl;

template<int N, typename Attribute>
struct attribute_accessor_impl<N, Attribute, true>
{
    typedef typename boost::add_reference<
                typename boost::mpl::at_c<typename Attribute::types, N>::type
            >::type result_type;

    static result_type get(Attribute& attribute)
    {
        return boost::fusion::at_c<N>(attribute);
    }
};

template<int N, typename Attribute>
struct attribute_accessor_impl<N, Attribute, false>
{
    typedef typename boost::add_reference<Attribute>::type result_type;
    static result_type get(Attribute& attribute)
    {
        BOOST_MPL_ASSERT(( boost::mpl::bool_<N == 0> ));
        return attribute;
    }
};

template<int N, typename Attribute>
struct attribute_accessor
{
    typedef typename attribute_accessor_impl<N, Attribute, is_fusion_vector<Attribute>::value>::result_type result_type;
    static result_type get(Attribute& attribute)
    {
        return attribute_accessor_impl<N, Attribute, is_fusion_vector<Attribute>::value>::get(attribute);
    }
};

} } } }

#endif /* ZILLIANS_LANGUAGE_ACTION_DETAIL_SEMANTICACTIONSDETAIL_H_ */
