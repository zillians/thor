#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
# include custom macros

include(CMakeParseArguments)

MACRO(ZILLIANS_THOR_FIND_BOOST)
    ADD_DEFINITIONS(-DBOOST_ALL_DYN_LINK)

    SET(THOR_REQUIRED_BOOST_LIB_NAMES
        thread
        date_time
        program_options
        timer
        prg_exec_monitor
        unit_test_framework
        filesystem
        regex
        system
        iostreams
        serialization
        context
    )

    # fr3@K
    # Do a "FIND_PACKAGE(Boost REQUIRED)" first to get boost version
    FIND_PACKAGE(Boost REQUIRED)
    IF(NOT (${Boost_VERSION} LESS 105500))
        LIST(APPEND THOR_REQUIRED_BOOST_LIB_NAMES coroutine)
    ENDIF()

    FIND_PACKAGE(Boost REQUIRED ${THOR_REQUIRED_BOOST_LIB_NAMES})

    UNSET(THOR_REQUIRED_BOOST_LIB_NAMES)
ENDMACRO()

ZILLIANS_THOR_FIND_BOOST()

FIND_PACKAGE(WebSocketPP REQUIRED)
FIND_PACKAGE(MsgPack     REQUIRED)
FIND_PACKAGE(RSTM        REQUIRED)
FIND_PACKAGE(LLVM        REQUIRED)
FIND_PACKAGE(LOG4CXX     REQUIRED)
FIND_PACKAGE(ZLIB        REQUIRED)

IF(ENABLE_FEATURE_CUDA)
    FIND_PACKAGE(CUDA   REQUIRED)
    FIND_PACKAGE(Thrust REQUIRED)
    FIND_PACKAGE(NVVM           )
ENDIF()

IF(ENABLE_FEATURE_TBB)
    FIND_PACKAGE(SystemTBB REQUIRED)
ENDIF()

IF(ENABLE_FEATURE_RDMA)
    FIND_PACKAGE(RDMA)
ENDIF()

##########################################################################
# Add build-enable preprocessors to global compilation definition
IF(ENABLE_FEATURE_NETWORK_ADDON)
    ADD_DEFINITIONS(-DBUILD_WITH_NETWORK_ADDON)
ENDIF()

IF(ENABLE_FEATURE_FRAMEWORK_ADDON)
    ADD_DEFINITIONS(-DBUILD_WITH_FRAMEWORK_ADDON)
ENDIF()

IF(CUDA_FOUND)
	ADD_DEFINITIONS(-DBUILD_WITH_CUDA)
ENDIF()

IF(NVVM_FOUND)
    ADD_DEFINITIONS(-DBUILD_WITH_NVVM)
ENDIF()

IF(RDMA_FOUND)
	ADD_DEFINITIONS(-DBUILD_WITH_RDMA)
ENDIF()

##########################################################################
# Add all header paths to default search
SET(ZILLIANS_INCLUDE_DIRS "")

IF(CUDA_FOUND)
	LIST(APPEND ZILLIANS_INCLUDE_DIRS ${CUDA_INCLUDE_DIRS} ${CUDA_CUT_INCLUDE_DIR})
	MESSAGE(STATUS "CUDA version: ${CUDA_VERSION}")
	IF(CUDA_VERSION VERSION_GREATER "3.1")
	    MESSAGE(STATUS "CUDA enable 64bit cuda device pointer")
	    ADD_DEFINITIONS(-DENABLE_CUDA_POINTER_64BIT)
	ENDIF()
ENDIF(CUDA_FOUND)

IF(Boost_FOUND)
	LIST(APPEND ZILLIANS_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
ENDIF(Boost_FOUND)

IF(LOG4CXX_FOUND)
	LIST(APPEND ZILLIANS_INCLUDE_DIRS ${LOG4CXX_INCLUDE_DIR})
ENDIF(LOG4CXX_FOUND)

IF(TBB_FOUND)
	LIST(APPEND ZILLIANS_INCLUDE_DIRS ${TBB_INCLUDE_DIRS})
ENDIF(TBB_FOUND)

IF(RDMA_FOUND)
	LIST(APPEND ZILLIANS_INCLUDE_DIRS ${RDMA_INCLUDE_DIRS})
ENDIF(RDMA_FOUND)

IF(THRUST_FOUND)
	LIST(APPEND ZILLIANS_INCLUDE_DIRS ${THRUST_INCLUDE_DIR})
ENDIF(THRUST_FOUND)

IF(ZLIB_FOUND)
    LIST(APPEND ZILLIANS_INCLUDE_DIRS ${ZLIB_INCLUDE_DIRS})
ENDIF(ZLIB_FOUND)

INCLUDE_DIRECTORIES(
		${ZILLIANS_INCLUDE_DIRS}
		)

IF(ENABLE_FEATURE_CUDA)
    CUDA_INCLUDE_DIRECTORIES(
    		${ZILLIANS_INCLUDE_DIRS}
    		)
ENDIF()

##########################################################################
# Add all library paths to default link
SET(ZILLIANS_LINK_DIRS "")

IF(Boost_FOUND)
	LIST(APPEND ZILLIANS_LINK_DIRS ${Boost_LIBRARY_DIRS})
ENDIF(Boost_FOUND)

IF(TBB_FOUND)
	LIST(APPEND ZILLIANS_LINK_DIRS ${TBB_LIBRARY_DIRS})
ENDIF(TBB_FOUND)

LINK_DIRECTORIES(
		${ZILLIANS_LINK_DIRS}
		)

##########################################################################
# Add all dependencies to ZILLIANS_DEP_LIBS variable
SET(ZILLIANS_DEP_LIBS "")

IF(Boost_FOUND)
	LIST(APPEND ZILLIANS_DEP_LIBS ${Boost_LIBRARIES})
ENDIF(Boost_FOUND)

IF(LOG4CXX_FOUND)
	LIST(APPEND ZILLIANS_DEP_LIBS ${LOG4CXX_LIBRARIES})
ENDIF(LOG4CXX_FOUND)

IF(TBB_FOUND)
	LIST(APPEND ZILLIANS_DEP_LIBS ${TBB_LIBRARIES})
ENDIF(TBB_FOUND)

IF(ZLIB_FOUND)
	LIST(APPEND ZILLIANS_DEP_LIBS ${ZLIB_LIBRARIES})
ENDIF(ZLIB_FOUND)

SET(ZILLIANS_DEP_LIBS ${ZILLIANS_DEP_LIBS} CACHE STRING "Gloabl depencent libraries" FORCE)
MESSAGE(STATUS "The global dependecies: ${ZILLIANS_DEP_LIBS}")

MARK_AS_ADVANCED(ZILLIANS_DEP_LIBS)
