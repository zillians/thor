#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the WebSocketPP script
# Once done this will define
#
# WEBSOCKETPP_FOUND - system has WebSocketPP

IF(NOT WIN32)
    FIND_PATH(WEBSOCKETPP_INCLUDE_DIR websocketpp.hpp PATHS
        /usr/local/include/websocketpp
        /usr/include/websocketpp
        PATH_SUFFIXES "websocketpp"
        )
    FIND_LIBRARY(WEBSOCKETPP_LIBRARIES websocketpp)
ENDIF()

IF(WEBSOCKETPP_INCLUDE_DIR AND WEBSOCKETPP_LIBRARIES)
    SET(WEBSOCKETPP_FOUND 1)
    IF(NOT WEBSOCKETPP_FOUND_QUIETLY)
        MESSAGE(STATUS "Found WebSocketPP: script = ${WEBSOCKETPP_FOUND}")
    ENDIF()
ELSE()
    SET(WEBSOCKETPP_FOUND 0 CACHE BOOL "WebSocketPP not found")
    MESSAGE(STATUS "WebSocketPP not found, disabled")
ENDIF()

MARK_AS_ADVANCED(WEBSOCKETPP_FOUND)
