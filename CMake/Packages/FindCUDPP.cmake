#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the CUDPP libraries
# Once done this will define
#
# CUDPP_FOUND - system has CUDPP
# CUDPP_INCLUDE_DIR - the CUDPP include directory
# CUDPP_LIBRARIES - CUDPP library

MESSAGE(STATUS "Looking for CUDPP...")

FIND_PATH(CUDPP_INCLUDE_DIR cudpp.h PATHS 
	/usr/include/ 
	/usr/local/include/
	${CUDA_INCLUDE_DIRS}
	)

IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
	FIND_LIBRARY(CUDPP_LIBRARIES NAMES cudpp64 PATHS 
	    /usr/local/cuda/lib64/ 
	    )
ELSE()
	FIND_LIBRARY(CUDPP_LIBRARIES NAMES cudpp PATHS 
	    /usr/local/cuda/lib/ 
	    )
ENDIF()


IF(CUDPP_INCLUDE_DIR AND CUDPP_LIBRARIES)
	SET(CUDPP_FOUND 1)
	IF(NOT CUDPP_FIND_QUIETLY)
		MESSAGE(STATUS "Found CUDPP: libraries = ${CUDPP_LIBRARIES}")
	ENDIF(NOT CUDPP_FIND_QUIETLY)
ELSE()
	SET(CUDPP_FOUND 0 CACHE BOOL "CUDPP not found")
	IF(CUDPP_FIND_REQUIRED)
	    MESSAGE(FATAL_ERROR "Could NOT find CUDPP, error")
	ELSE()
	    MESSAGE(STATUS "Could NOT find CUDPP, disabled")
	ENDIF()
ENDIF()

MARK_AS_ADVANCED(CUDPP_INCLUDE_DIR CUDPP_LIBRARIES)

