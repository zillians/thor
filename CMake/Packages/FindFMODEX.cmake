#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
#IF(FMODEX_INCLUDE_DIR AND FMODEX_LIBRARIES)
#    SET(FMODEX_FIND_QUIETLY TRUE)
#ENDIF()

MESSAGE(STATUS "Looking for FMODEX...")

IF(WIN32)
#TODO
	MESSAGE(ERROR "FMod Ex for Windows CMake is not implemented")
ELSE()
	FIND_PATH(FMODEX_INCLUDE_DIR fmodex/fmod.hpp
		/usr/local/include
		/usr/include
		/opt/include
	)

	FIND_LIBRARY(FMODEX_LIBRARIES
		NAMES fmodex64
		PATHS
		/usr/local/lib
		/usr/lib
		/usr/lib64
	)
ENDIF()

MARK_AS_ADVANCED(FMODEX_INCLUDE_DIR)
MARK_AS_ADVANCED(FMODEX_LIBRARIES)


IF(FMODEX_INCLUDE_DIR AND FMODEX_LIBRARIES)
	SET(FMODEX_FOUND TRUE)

	MESSAGE(STATUS "FMOD EX include: " ${FMODEX_INCLUDE_DIR})
	MESSAGE(STATUS "FMOD EX library: " ${FMODEX_LIBRARIES})
ENDIF()

