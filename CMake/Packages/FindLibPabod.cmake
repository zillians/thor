#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

IF(NOT WIN32)
    FIND_PATH(LIBPABOD_DIR include/pabod.h PATHS
        ${CMAKE_SOURCE_DIR}/dep/linux/libpabod
        )
    IF(LIBPABOD_DIR)
        SET(LIBPABOD_FOUND 1)
        SET(LIBPABOD_DIR ${LIBPABOD_DIR} CACHE PATH "libpabod directory" FORCE)
        SET(LIBPABOD_BINARY_DIR "${CMAKE_BINARY_DIR}/dep/linux/libpabod" CACHE STRING "libpabod binary directory")
        ADD_SUBDIRECTORY("${LIBPABOD_DIR}")
    ENDIF()
ENDIF()

