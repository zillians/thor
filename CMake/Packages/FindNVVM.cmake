#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the NVIDIA Compiler SDK
# Once done this will define
#
# NVVM_FOUND - set if system has NVVM
# NVVM_INCLUDE_DIR - the NVVM include directory
# NVVM_LIBRARIES - NVVM library

IF(NOT WIN32)
    FIND_PATH(NVVM_INCLUDE_DIR nvvm.h PATHS
        /usr/include/
        /usr/local/include/
        /usr/local/cuda/nvvm/include/
        )
    FIND_LIBRARY(NVVM_LIBRARIES NAMES nvvm PATHS
        /usr/lib/
        /usr/local/lib/
        /usr/local/cuda/nvvm/lib64/
        )
ENDIF()

IF(NVVM_INCLUDE_DIR AND NVVM_LIBRARIES)
    SET(NVVM_FOUND 1)
    IF(NOT NVVM_FOUND_QUIETLY)
        MESSAGE(STATUS "Found NVVM: include = ${NVVM_INCLUDE_DIR}, libraries = ${NVVM_LIBRARIES}")
    ENDIF()
ELSE()
    SET(NVVM_FOUND 0 CACHE BOOL "NVVM not found")
    MESSAGE(STATUS "NVVM not found, disabled")
ENDIF()

MARK_AS_ADVANCED(NVVM_INCLUDE_DIR NVVM_LIBRARIES)
