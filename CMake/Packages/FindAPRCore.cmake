#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# - Try to find the APR libraries
# Once done this will define
#
# APRCORE_FOUND - system has APR Core
# APRCORE_INCLUDE_DIR - the APR Core include directory
# APRCORE_LIBRARIES - APR Core library

IF(WIN32)
    FIND_PATH(APRCORE_INCLUDE_DIR apr.h PATHS 
    	/usr/include/apr-1.0/
    	/usr/local/include/apr-1.0/ 
    	${CMAKE_SOURCE_DIR}/dep/windows/apr/include/
    	)
    IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
        FIND_LIBRARY(APRCORE_LIBRARIES NAMES libapr-1 PATHS
        	${CMAKE_SOURCE_DIR}/dep/windows/apr/lib/x86-64
        	)
    ELSE()
        FIND_LIBRARY(APRCORE_LIBRARIES NAMES libapr-1 PATHS
        	${CMAKE_SOURCE_DIR}/dep/windows/apr/lib/x86
        	)
    ENDIF()
ELSE()
    FIND_PATH(APRCORE_INCLUDE_DIR apr.h PATHS 
    	/usr/include/apr-1.0/
	/usr/include/apr-1/
    	/usr/local/include/apr-1.0/ 
    	/usr/local/include/apr-1/ 
    	${CMAKE_SOURCE_DIR}/dep/linux/apr/include/
    	)
    IF(CMAKE_SIZEOF_VOID_P EQUAL 8)
        FIND_LIBRARY(APRCORE_LIBRARIES NAMES apr-1 PATHS
        	${CMAKE_SOURCE_DIR}/dep/linux/apr/lib/x86-64
        	)
    ELSE()
        FIND_LIBRARY(APRCORE_LIBRARIES NAMES apr-1 PATHS
        	${CMAKE_SOURCE_DIR}/dep/linux/apr/lib/x86
        	)
    ENDIF()
ENDIF()

IF(APRCORE_INCLUDE_DIR AND APRCORE_LIBRARIES)
	SET(APRCORE_FOUND 1)
	IF(NOT APRCORE_FIND_QUIETLY)
		MESSAGE(STATUS "Found APRCORE: include = ${APRCORE_INCLUDE_DIR}, libraries = ${APRCORE_LIBRARIES}")
	ENDIF()
ELSE()
	SET(APRCORE_FOUND 0 CACHE BOOL "APRCORE not found")
	MESSAGE(STATUS "APRCORE not found, disabled")
ENDIF()

MARK_AS_ADVANCED(APRCORE_INCLUDE_DIR APRCORE_LIBRARIES)
