#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

MACRO(thor_enable_testing)
    IF("${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_SOURCE_DIR}")
        ENABLE_TESTING()

        MESSAGE(STATUS "CMAKE_TEST_COMMAND: ${CMAKE_TEST_COMMAND}")

        ADD_CUSTOM_TARGET(check COMMAND ${CMAKE_CTEST_COMMAND})
        ADD_CUSTOM_TARGET(build-test)

        ADD_DEPENDENCIES(check build-test)

        SET(THOR_TEST_NAME_PREFIX      "thor-test")
        SET(THOR_TEST_PROG_NAME_PREFIX "thor-test-prog")
    ELSE()
        MESSAGE(FATAL_ERROR "this macro should be put in root dir!\nDue to the limitation of underlying cmake command ENABLE_TESTING()")
    ENDIF()
ENDMACRO()

MACRO(thor_test_append_prefix prefix)
    SET(prefix "${prefix}")

    IF(NOT prefix)
        MESSAGE(SEND_ERROR "empty string!")
    ELSE()
        SET(THOR_TEST_NAME_PREFIX      "${THOR_TEST_NAME_PREFIX}-${prefix}")
        SET(THOR_TEST_PROG_NAME_PREFIX "${THOR_TEST_PROG_NAME_PREFIX}-${prefix}")
    ENDIF()

    UNSET(prefix)
ENDMACRO()

MACRO(thor_add_test)
    SET(thor_add_test_one_value_opts   NAME)
    SET(thor_add_test_multi_value_opts COMMAND)

    CMAKE_PARSE_ARGUMENTS(THOR_ADD_TEST "" "${thor_add_test_one_value_opts}" "${thor_add_test_multi_value_opts}" ${ARGN})

    IF(NOT THOR_ADD_TEST_NAME)
        MESSAGE(SEND_ERROR "test name missing")
    ELSEIF(NOT THOR_ADD_TEST_COMMAND)
        MESSAGE(SEND_ERROR "test command missing")
    ELSE()
        SET(THOR_ADD_TEST_NAME ${THOR_TEST_NAME_PREFIX}-${THOR_ADD_TEST_NAME})

        ADD_CUSTOM_TARGET(${THOR_ADD_TEST_NAME} ${THOR_ADD_TEST_COMMAND})
        ADD_TEST(${THOR_ADD_TEST_NAME} ${THOR_ADD_TEST_COMMAND})

        UNSET(THOR_ADD_TEST_NAME)
    ENDIF()

    SET(thor_add_test_multi_value_opts)
    SET(thor_add_test_one_value_opts)
ENDMACRO()

MACRO(thor_add_test_with_target)
    SET(thor_add_test_with_target_one_value_opts   SUFFIX)
    SET(thor_add_test_with_target_multi_value_opts SOURCES DEPENDS)

    CMAKE_PARSE_ARGUMENTS(THOR_ADD_TEST_WITH_TARGET "" "${thor_add_test_with_target_one_value_opts}" "${thor_add_test_with_target_multi_value_opts}" ${ARGN})

    IF(NOT THOR_ADD_TEST_WITH_TARGET_SUFFIX)
        MESSAGE(SEND_ERROR "test suffix missing")
    ELSEIF(NOT THOR_ADD_TEST_WITH_TARGET_SOURCES)
        MESSAGE(SEND_ERROR "test sources missing")
    ELSE()
        SET(THOR_ADD_TEST_PROG_NAME ${THOR_TEST_PROG_NAME_PREFIX}-${THOR_ADD_TEST_WITH_TARGET_SUFFIX})
        SET(THOR_ADD_TEST_NAME      ${THOR_TEST_NAME_PREFIX}-${THOR_ADD_TEST_WITH_TARGET_SUFFIX})

        ADD_EXECUTABLE(${THOR_ADD_TEST_PROG_NAME} EXCLUDE_FROM_ALL ${THOR_ADD_TEST_WITH_TARGET_SOURCES})

        GET_TARGET_PROPERTY(THOR_ADD_TEST_PROG_PATH ${THOR_ADD_TEST_PROG_NAME} LOCATION)

        ADD_CUSTOM_TARGET(${THOR_ADD_TEST_NAME} COMMAND ${THOR_ADD_TEST_PROG_PATH} DEPENDS ${THOR_ADD_TEST_PROG_NAME})
        ADD_TEST(${THOR_ADD_TEST_NAME} ${THOR_ADD_TEST_PROG_PATH})

        ADD_DEPENDENCIES(build-test ${THOR_ADD_TEST_PROG_NAME})

        IF(THOR_ADD_TEST_WITH_TARGET_DEPENDS)
            TARGET_LINK_LIBRARIES(${THOR_ADD_TEST_PROG_NAME}
                ${THOR_ADD_TEST_WITH_TARGET_DEPENDS}
            )
        ENDIF()

        UNSET(THOR_ADD_TEST_PROG_PATH)
        UNSET(THOR_ADD_TEST_NAME)
        UNSET(THOR_ADD_TEST_PROG_NAME)
    ENDIF()

    SET(thor_add_test_with_target_multi_value_opts)
    SET(thor_add_test_with_target_one_value_opts)
ENDMACRO()
