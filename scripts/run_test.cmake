#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

# We need the following variables
# - BUILD_DIR
# - SOURCE_DIR
# - TEST_NAME
# - CONFIG_MANIFEST
# - WRAPPER_GENERATOR
# - WRAPPER_HDR
# - NATIVES
# - NATIVE_INCS
# - NATIVE_FLAGS
# - NATIVE_LIB_NAME
# - NATIVE_LIB_TYPE
# - CMD_0 ~ CMD_N

GET_FILENAME_COMPONENT(COPIED_NAME "${SOURCE_DIR}" NAME)
IF(NOT WORKING_DIR)
    SET(WORKING_DIR  "${BUILD_DIR}/${TEST_NAME}")
ENDIF()
SET(COPIED_DIR "${WORKING_DIR}/${COPIED_NAME}")
SET(BIN_DIR    "${COPIED_DIR}/bin")
SET(BUI_DIR    "${COPIED_DIR}/build")
SET(LIB_DIR    "${COPIED_DIR}/lib")
SET(SRC_DIR    "${COPIED_DIR}/src")
SET(NATIVE_DIR "${COPIED_DIR}/native")
SET(NATIVE_DEP "")

#MESSAGE(STATUS "BUILD_DIR        : ${BUILD_DIR}")
#MESSAGE(STATUS "SOURCE_DIR       : ${SOURCE_DIR}")
#MESSAGE(STATUS "TEST_NAME        : ${TEST_NAME}")
#MESSAGE(STATUS "CONFIG_MANIFEST  : ${CONFIG_MANIFEST}")
#MESSAGE(STATUS "WRAPPER_GENERATOR: ${WRAPPER_GENERATOR}")
#MESSAGE(STATUS "WRAPPER_HDR      : ${WRAPPER_HDR}")
#MESSAGE(STATUS "NATIVES          : ${NATIVES}")
#MESSAGE(STATUS "NATIVE_INCS      : ${NATIVE_INCS}")
#MESSAGE(STATUS "NATIVE_FLAGS     : ${NATIVE_FLAGS}")
#MESSAGE(STATUS "NATIVE_LIB_NAME  : ${NATIVE_LIB_NAME}")
#MESSAGE(STATUS "NATIVE_LIB_TYPE  : ${NATIVE_LIB_TYPE}")
#MESSAGE(STATUS "CMD_0            : ${CMD_0}")
#MESSAGE(STATUS "CMD_1            : ${CMD_1}")
#MESSAGE(STATUS "CMD_2            : ${CMD_2}")
#MESSAGE(STATUS "CMD_3            : ${CMD_3}")
#MESSAGE(STATUS "CMD_4            : ${CMD_4}")
#MESSAGE(STATUS "CMD_5            : ${CMD_5}")
#MESSAGE(STATUS "CMD_6            : ${CMD_6}")
#MESSAGE(STATUS "CMD_7            : ${CMD_7}")
#MESSAGE(STATUS "CMD_8            : ${CMD_8}")
#MESSAGE(STATUS "CMD_9            : ${CMD_9}")
#
#MESSAGE(STATUS "WORKING_DIR       : ${WORKING_DIR}")
#MESSAGE(STATUS "COPIED_DIR        : ${COPIED_DIR}")
#MESSAGE(STATUS "BIN_DIR           : ${BIN_DIR}")
#MESSAGE(STATUS "BUI_DIR           : ${BUI_DIR}")
#MESSAGE(STATUS "LIB_DIR           : ${LIB_DIR}")
#MESSAGE(STATUS "SRC_DIR           : ${SRC_DIR}")
#MESSAGE(STATUS "NATIVE_DIR        : ${NATIVE_DIR}")
#MESSAGE(STATUS "NATIVE_DEP        : ${NATIVE_DEP}")

FILE(REMOVE_RECURSE ${COPIED_DIR})
FILE(COPY ${SOURCE_DIR} DESTINATION ${WORKING_DIR})

IF(NOT "${NATIVES}" STREQUAL "")
    MESSAGE(STATUS "found native files, start compiling")

    IF(NOT "${NATIVE_INCS}" STREQUAL "")
        SET(NATIVE_INCS_TMP "")

        FOREACH(NATIVE_INC ${NATIVE_INCS})
            IF("${NATIVE_INCS_TMP}" STREQUAL "")
                SET(NATIVE_INCS_TMP "-I${NATIVE_INC}")
            ELSE()
                SET(NATIVE_INCS_TMP "${NATIVE_INCS_TMP} -I${NATIVE_INC}")
            ENDIF()
        ENDFOREACH()

        SET(NATIVE_INCS ${NATIVE_INCS_TMP})

        MESSAGE(STATUS "use include directory: ${NATIVE_INCS}")
    ELSE()
        SET(NATIVE_INCS)
    ENDIF()

    SEPARATE_ARGUMENTS(NATIVE_FLAGS UNIX_COMMAND ${NATIVE_FLAGS})

    FILE(MAKE_DIRECTORY ${NATIVE_DIR})

    SET(NATIVE_OBJECTS)

    FOREACH(NATIVE ${NATIVES})
        GET_FILENAME_COMPONENT(NATIVE_NAME ${NATIVE} NAME_WE)

        SET(NATIVE_OBJECT ${NATIVE_DIR}/${NATIVE_NAME}.o)

        SET(NATIVE_CMD gcc -std=c++11 -g -fPIC ${NATIVE_INCS} ${NATIVE_FLAGS} -c -o ${NATIVE_OBJECT} ${NATIVE_DIR}/${NATIVE})

        EXECUTE_PROCESS(
            COMMAND ${NATIVE_CMD}
            RESULT_VARIABLE NATIVE_RESULT
        )
        SET(NATIVE "${NATIVE_${INDEX}}")

        IF(NATIVE_RESULT)
            MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: ${NATIVE_RESULT} when invoking: ${NATIVE_CMD}")
        ELSE()
            SET(NATIVE_OBJECTS ${NATIVE_OBJECTS} ${NATIVE_OBJECT})
        ENDIF()
    ENDFOREACH()

    MESSAGE(STATUS "end of native files compiling")

    IF(NATIVE_LIB_NAME AND NATIVE_LIB_TYPE)
        MESSAGE(STATUS "output as ${NATIVE_LIB_TYPE}")

        IF("${NATIVE_LIB_TYPE}" STREQUAL "DLL")
            FILE(MAKE_DIRECTORY ${BIN_DIR})
            SET(NATIVE_LIB_CMD gcc -shared -fPIC ${NATIVE_OBJECTS} -o ${BIN_DIR}/lib${NATIVE_LIB_NAME}-native.so)
            SET(NATIVE_DEP "<native_shared_library name=\"${NATIVE_LIB_NAME}-native\" lpath=\"bin\" />")
        ELSE()
            FILE(MAKE_DIRECTORY ${LIB_DIR})
            SET(NATIVE_LIB_CMD ar rsc ${LIB_DIR}/lib${NATIVE_LIB_NAME}-native.a ${NATIVE_OBJECTS})
            SET(NATIVE_DEP "<native_library name=\"${NATIVE_LIB_NAME}-native\" lpath=\"lib\" />")
        ENDIF()

        EXECUTE_PROCESS(
            COMMAND ${NATIVE_LIB_CMD}
            RESULT_VARIABLE NATIVE_RESULT
        )

        IF(NATIVE_RESULT)
            MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: ${NATIVE_RESULT} when invoking: ${NATIVE_LIB_CMD}")
        ENDIF()
    ENDIF()

    IF(WRAPPER_GENERATOR AND WRAPPER_HDR)
        MESSAGE(STATUS "Generating Thor wrapper with: ${WRAPPER_GENERATOR}")

        SET(WRAPPER_GEN_CMD
            ${WRAPPER_GENERATOR}
            -I ${NATIVE_DIR}
            -D ${SRC_DIR}
            -T "import_test"
            ${NATIVE_DIR}/${WRAPPER_HDR}
        )

        EXECUTE_PROCESS(
            COMMAND         ${WRAPPER_GEN_CMD}
            RESULT_VARIABLE WRAPPER_RESULT
        )

        IF(WRAPPER_RESULT)
            MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: ${WRAPPER_RESULT} when invoking: ${WRAPPER_GEN_CMD}")
        ENDIF()

        FILE(GLOB_RECURSE WRAPPER_SOURCES "${COPIED_DIR}/**/ts_import.cpp")
        #MESSAGE(STATUS "WRAPPER_SOURCES: ${WRAPPER_SOURCES}")

        IF(NOT WRAPPER_SOURCES)
            MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: no native source for Thor wrapper")
        ENDIF()

        FILE(MAKE_DIRECTORY ${BUI_DIR})
        FILE(MAKE_DIRECTORY ${LIB_DIR})
        SET(WRAPPER_OBJECTS)

        FOREACH(WRAPPER_SOURCE ${WRAPPER_SOURCES})
            FILE(RELATIVE_PATH WRAPPER_SOURCE_PATH ${COPIED_DIR} ${WRAPPER_SOURCE})
            STRING(REGEX REPLACE "[/]|[\\]" "_" WRAPPER_OBJECT_NAME "${WRAPPER_SOURCE_PATH}")

            SET(WRAPPER_OBJECT ${BUI_DIR}/wrapper-${WRAPPER_OBJECT_NAME}.o)

            SET(WRAPPER_COMPILE_CMD gcc -std=c++11 -g -fPIC ${NATIVE_INCS} -I${SRC_DIR} ${NATIVE_FLAGS} -c -o ${WRAPPER_OBJECT} ${WRAPPER_SOURCE})

            EXECUTE_PROCESS(
                COMMAND         ${WRAPPER_COMPILE_CMD}
                RESULT_VARIABLE WRAPPER_RESULT
            )

            IF(WRAPPER_RESULT)
                MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: ${WRAPPER_RESULT} when invoking: ${WRAPPER_COMPILE_CMD}")
            ELSE()
                SET(WRAPPER_OBJECTS ${WRAPPER_OBJECTS} ${WRAPPER_OBJECT})
            ENDIF()
        ENDFOREACH()

        SET(WRAPPER_LIB_NAME ${NATIVE_LIB_NAME}-wrapper)
        SET(WRAPPER_AR_CMD ar rsc ${LIB_DIR}/lib${WRAPPER_LIB_NAME}.a ${WRAPPER_OBJECTS})

        EXECUTE_PROCESS(
            COMMAND         ${WRAPPER_AR_CMD}
            RESULT_VARIABLE WRAPPER_RESULT
        )

        IF(WRAPPER_RESULT)
            MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: ${WRAPPER_RESULT} when invoking: ${WRAPPER_AR_CMD}")
        ELSE()
            SET(NATIVE_DEP ${NATIVE_DEP} "<native_library name=\"${WRAPPER_LIB_NAME}\" lpath=\"lib\" />")
        ENDIF()
    ENDIF()
ENDIF()

IF("${CONFIG_MANIFEST}" STREQUAL "yes")
    CONFIGURE_FILE(
        ${COPIED_DIR}/manifest.xml.in
        ${COPIED_DIR}/manifest.xml
    )
ENDIF()

FOREACH(INDEX RANGE 10)
    SET(CMD "${CMD_${INDEX}}")

    #MESSAGE(STATUS "Index: ${INDEX}")
    #MESSAGE(STATUS "CMD: ${CMD}")

    IF(NOT ${CMD} STREQUAL "")
        SEPARATE_ARGUMENTS(CMD_IN_LIST UNIX_COMMAND ${CMD})

        #MESSAGE(STATUS "CMD_IN_LIST: ${CMD_IN_LIST}")

        EXECUTE_PROCESS(
            COMMAND ${CMD_IN_LIST}
            WORKING_DIRECTORY ${COPIED_DIR}
            RESULT_VARIABLE CMD_RESULT
        )

        IF(CMD_RESULT)
            MESSAGE(FATAL_ERROR "${TEST_NAME} failed with: ${CMD_RESULT} when invoking: ${CMD}")
        ENDIF()
    ELSE()
        MESSAGE(STATUS "${TEST_NAME} succeeded after ${INDEX} command(s)")
        BREAK()
    ENDIF()
ENDFOREACH()
