#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
# convert shell command from "string" to "list" so EXECUTE_PROCESS parses arguments correctly
string(REPLACE " " ";" TEST_PROG ${TEST_PROG})

execute_process(
    COMMAND ${TEST_PROG}
    RESULT_VARIABLE __exit_code
    )

if(EXPECT_FAIL)
    if(NOT __exit_code)
        message(FATAL_ERROR "fail! (run.cmake) -- expect fail but exit code zero")
    endif()
else()
    if(__exit_code)
        message(FATAL_ERROR "fail! (run.cmake) -- exit code == " ${__exit_code})
    endif()
endif()
