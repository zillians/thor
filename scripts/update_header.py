#!/usr/bin/env python3
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import os
import sys
import re
import shutil

copyright = """ Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>

 This file is part of Thor.
 Thor is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License, version 3,
 as published by the Free Software Foundation.

 Thor is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with Thor.  If not, see <http://www.gnu.org/licenses/>.

 If you want to develop any commercial services or closed-source products with
 Thor, to adapt sources of Thor in your own projects without
 disclosing sources, purchasing a commercial license is mandatory.

 For more information, please contact Zillians, Inc.
 <thor@zillians.com>
"""

black_files = [
"sha1.cpp",
"sha1.h",
"utf8_codecvt_facet.cpp",
"run_nvcc.cmake",
"make2cmake.cmake",
"parse_cubin.cmake",
"FindCUDA.cmake"
]

def src_copyright():
    result = "/**\n *\n"

    for line in copyright.split('\n'):
        result += " *" + line + "\n"

    result += " */"

    return result

def script_copyright():
    result = "#\n"

    for line in copyright.split('\n'):
        result += "#" + line + "\n"

    return result

def find_header(filename):
    with open(filename) as f:
        content = f.read()

        header = re.search(header_regex, content, re.DOTALL | re.MULTILINE)
        if header:
            return True, header.group(0)

        return False, ""

    return False, ""

def collect_headers(path, name_regex):
    header = {}
    no_header = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if name in black_files:
                continue

            if re.search(name_regex, name):
                fullpath = os.path.join(root, name)
                is_found, h = find_header(fullpath)
                if is_found:
                    if not h in header:
                        header[h] = []

                    header[h].append(fullpath)

                else:
                    no_header.append(fullpath)

    return header, no_header

temp_path = "/tmp/update_header_py_tmp"

def prepend_worker(src_path):
    with open(src_path, "r") as src, open(temp_path, "w") as tmp:
        content = src.read()
        tmp.write(copyright + "\n" + content)

    if not dryrun:
        shutil.copyfile(temp_path, src_path)

def update_worker(src_path):
    with open(src_path, "r") as src, open(temp_path, "w") as tmp:
        content = src.read()
        new_content = re.sub(header_regex, copyright, content, flags = re.DOTALL | re.MULTILINE)
        tmp.write(new_content)

    if not dryrun:
        shutil.copyfile(temp_path, src_path)

def update_headers(path, name_regex):
    replace = []
    prepend = []

    for root, dirs, files in os.walk(path):
        for name in files:
            if name in black_files:
                continue

            if re.search(name_regex, name):
                fullpath = os.path.join(root, name)
                is_found, h = find_header(fullpath)
                if is_found:
                    update_worker(fullpath)
                    replace.append(fullpath)

                else:
                    prepend_worker(fullpath)
                    prepend.append(fullpath)

    return replace, prepend

MODE_SRC    = 1
MODE_SCRIPT = 2

if __name__ == "__main__":
    verbose       = "-v" in sys.argv
    show_header   = "-h" in sys.argv
    update_header = "-u" in sys.argv
    dryrun        = "-d" in sys.argv
    path = sys.argv[1]
    mode = MODE_SRC
    if "script" in sys.argv:
        mode = MODE_SCRIPT

    print("Working mode      : ", mode)
    print("Working root path : ", path)

    if mode == MODE_SRC:
        name_regex = r"\.c$|\.cpp$|\.h$|\.hpp$|\.inc$|\.ipp$|\.t$|\.cu$|\.cuh$"
        header_regex = r"^\/\*.*?Copyright.*?(z|Z)illians.*?\*\/"
        copyright = src_copyright()

    elif mode == MODE_SCRIPT:
        header_regex = r"^#.*?Copyright.*?(z|Z)illians.*?#\n\n"
        name_regex = r"\.cmake$|\.py$|CMakeLists.txt$"
        copyright = script_copyright()

    if update_header:
        replace, prepend = update_headers(path, name_regex)

        if verbose:
            print("Replaced files: ")
            for fn in replace:
                print(fn)

            print("Prepended files: ")
            for fn in prepend:
                print(fn)

        print("****************************")
        print("Total replaced : ", len(replace))
        print("Total prepended: ", len(prepend))

    else:
        header, no_header = collect_headers(path, name_regex)

        if verbose or show_header:
            hcounter = 1
            for h, f in header.items():
                if show_header:
                    print(hcounter,"==========================================")
                    print(h)
                    hcounter += 1

                    if verbose:
                        print(f)

        if verbose:
            print("No header: ")
            for nh in no_header:
                try:
                    content = open(nh).read()
                    print(nh,": ")
                    print(content[0:64])
                    print("--------------------------------------")
                except IOError as e:
                    print("The file is not exist:", nh)

        header_count = 0
        for h, f in header.items():
            header_count += len(f)

        print("****************************")
        print("Total header: ", len(header))
        print("Header-found file count: ", header_count)
        print("No header file count: ", len(no_header))

