/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <utility>

#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>

#include "network/Session.h"
#include "network/Listener.h"

#ifndef RETURN_ON_FAILURE
    #define RETURN_ON_FAILURE(error_code)              \
                do {                                   \
                    if (error_code) return error_code; \
                } while (false)
#endif


namespace zillians { namespace network {

listener::listener(boost::asio::io_service& service)
: base_type(service)
{ }

listener::~listener()
{
    close();
}

boost::system::error_code listener::close()
{
    boost::system::error_code error;

    base_type::close(error);

    return error;
}

auto listener::listen(transport_type type, std::uint16_t port) -> boost::system::error_code
{
    const auto protocol = (type == TCP_V4 ? protocol_type::v4() : protocol_type::v6());
    boost::system::error_code error;

    base_type::open(protocol, error);
    RETURN_ON_FAILURE(error);

    // re-use the address (in some system, the listening port is not free immediately after program exit, so we have to re-use the port)
    boost::asio::socket_base::reuse_address option(true);
    base_type::set_option(option, error);
    RETURN_ON_FAILURE(error);

    base_type::bind(endpoint_type(protocol, port), error);
    RETURN_ON_FAILURE(error);

    base_type::listen(
        SOMAXCONN,
        error
    );

    return error;
}

void listener::async_accept(network::session* session, std::function<accept_callback_signature> handler)
{
    set_session(session);
    base_type::async_accept(
        *session, 
        std::bind(std::move(handler), this, std::placeholders::_1)
    );
}

void listener::cancel_accept()
{
    session->close();
    session = nullptr;
}

network::session* listener::steal_session()
{
    auto* to_give = session;       
    session = nullptr; 
    return to_give; 
}

void listener::set_session(network::session *new_session)
{
    session = new_session;
}

} }

#undef RETURN_ON_FAILURE
