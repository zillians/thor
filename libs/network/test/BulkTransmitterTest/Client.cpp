/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Feb 26, 2009 sdk - Initial version created.
 */

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <tbb/tbb_thread.h>
#include <tbb/atomic.h>

#include "network/sys/BulkTransmitter.h"
#include "network/sys/tcp/TcpSession.h"
#include "network/sys/tcp/TcpSessionEngine.h"

#include "Common.h"

using namespace zillians;
using namespace zillians::network::sys;

using boost::asio::ip::tcp;

log4cxx::LoggerPtr gLogger(log4cxx::Logger::getLogger("BulkTransmitterClient"));

TcpSessionEngine* gEngine;
BulkTransmitter<TcpSessionEngine, TcpSession>* gTransfer;
uint32 gTransferCount;

// forward declaration
void handle_session_close(TcpSession* session);
void handle_session_error(TcpSession* session, const boost::system::error_code& ec);

void handle_connected(const boost::system::error_code& ec, TcpSession* session)
{
	if(!ec)
	{
		gEngine->startDispatch(
				session,
				boost::bind(handle_session_close, session),
				boost::bind(handle_session_error, session, _1)
		);

		DummyMessage message;
		message.length = gTransferCount;
		for(uint32 i=0;i<gTransferCount;++i)
		{
			message.buffer << i;
		}

		gTransfer->write(*session, message);
	}
	else
	{
		LOG4CXX_DEBUG(gLogger, "failed to connect to remote, error = " << ec.message());
	}
}

void handle_session_close(TcpSession* session)
{
	gEngine->stop();
}

void handle_session_error(TcpSession* session, const boost::system::error_code& ec)
{
	LOG4CXX_DEBUG(gLogger, "error encountered = " << ec.message());
}

//////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	log4cxx::BasicConfigurator::configure();

	try
	{
		if (argc != 4)
		{
			std::cerr << "Usage: " << argv[0] << " <host> <port> <size>\n";
			return 1;
		}

		gTransferCount = atoi(argv[3]);

		boost::asio::io_service ios;

		gEngine = new TcpSessionEngine(&ios);
		gTransfer = new BulkTransmitter<TcpSessionEngine, TcpSession>(*gEngine);

		TcpSession* new_session = gEngine->createSession();

		gEngine->connectAsync(
				new_session,
				tcp::v4(), std::string(argv[1]), std::string(argv[2]),
				boost::bind(
						handle_connected,
						placeholders::error, new_session));

		gEngine->run();

		SAFE_DELETE(gTransfer);
		SAFE_DELETE(gEngine);

	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
