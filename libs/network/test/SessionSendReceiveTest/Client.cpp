/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Feb 26, 2009 sdk - Initial version created.
 */

#include "network/sys/Session.h"
#include "network/sys/SessionEngine.h"

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <tbb/tbb_thread.h>

#include "Common.h"

using namespace zillians;
using namespace zillians::network::sys;

using boost::asio::ip::tcp;

log4cxx::LoggerPtr gLogger(log4cxx::Logger::getLogger("Client"));

// forward declaration
void handleConnected(int id, int count, double* latencies, const boost::system::error_code& ec, TcpSession* session);
void handleMessageRead(const boost::system::error_code& ec, TcpSession* session);
void handleClosed(TcpSession* session);

void handleConnected(const boost::system::error_code& ec, TcpSession* session)
{
	MySessionContext* ctx = session->getContext<MySessionContext>();

//	shared_ptr<Buffer> b(new Buffer(1));
//	session->readAsync(b, boost::bind(handleClosed, session));

	if(!ec)
	{
		LOG4CXX_INFO(gLogger, "client #" << ctx->id << " waiting for write");
		sleep(5);
		LOG4CXX_INFO(gLogger, "client #" << ctx->id << " start write");
		//session->readAsync(ctx->buffer, boost::bind(handleMessageRead, placeholders::error, session));
		sendTestMessage(*session);

		uint32 dummy_type = 0;
		session->read(dummy_type, ctx->buffer);
		session->read(dummy_type, ctx->buffer);
		session->read(dummy_type, ctx->buffer);
		session->read(dummy_type, ctx->buffer);
		if(verifyTestMessage(*session, ctx->buffer))
		{
			LOG4CXX_INFO(gLogger, "client #" << ctx->id << " message verified");
		}
		else
		{
			LOG4CXX_ERROR(gLogger, "client #" << ctx->id << " verification failed");
		}
		//session->closeAsync(boost::bind(handleClosed, _1));
	}
	else
	{
		LOG4CXX_DEBUG(gLogger, "client #" << ctx->id << " failed to connect to remote, error = " << ec.message());
	}
}

void handleClosed(TcpSession* session)
{
	MySessionContext* ctx = session->getContext<MySessionContext>();
	session->socket().cancel();
	LOG4CXX_INFO(gLogger, "client #" << ctx->id << " closed");
}

//////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{

#  if !defined _LIBC || defined _LIBC_REENTRANT
	printf("cond1\n");
#endif

#ifdef _REENTRANT
	printf("cond2\n");
#endif

	log4cxx::BasicConfigurator::configure();

	try
	{
		if (argc != 5)
		{
			std::cerr << "Usage: " << argv[0] << " <host> <port> <threads> <count>\n";
			return 1;
		}

		int thread_count = atoi(argv[3]);
		int send_count = atoi(argv[4]);

		TcpSessionEngine engine;

		for(int i=0;i<thread_count;++i)
		{
			TcpSession* sessionNew = engine.createSession();

			MySessionContext* ctx = createContext(i);
			sessionNew->setContext(ctx);

			engine.connectAsync(
					sessionNew,
					tcp::v4(), std::string(argv[1]), std::string(argv[2]),
					boost::bind(
							handleConnected,
							placeholders::error, sessionNew));
		}

		engine.run();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
