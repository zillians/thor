/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @date Feb 26, 2009 sdk - Initial version created.
 */

#include <log4cxx/logger.h>
#include <log4cxx/basicconfigurator.h>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <tbb/tbb_thread.h>

#include "network/sys/tcp/TcpSession.h"
#include "network/sys/tcp/TcpSessionEngine.h"

#include "Common.h"

using namespace zillians;
using namespace zillians::network::sys;

using boost::asio::ip::tcp;

log4cxx::LoggerPtr gLogger(log4cxx::Logger::getLogger("Server"));

tbb::atomic<uint32> gSessionUniqueId;

// forward declaration
void handleListen(TcpSessionEngine* engine, const boost::system::error_code& ec);
void handleAccept(TcpSessionEngine* engine, TcpSession* session, const boost::system::error_code& ec);
void handleMessageRead(TcpSession* session, const boost::system::error_code& ec);
void handleClosed();

void handleListen(TcpSessionEngine* engine, const boost::system::error_code& ec)
{
	// create new session object
	TcpSession* sessionNew = engine->createSession();
	MySessionContext* context = createContext(gSessionUniqueId++);
	sessionNew->setContext<MySessionContext>(context);

	// start accepting connections
	engine->acceptAsync(sessionNew, boost::bind(handleAccept, engine, sessionNew, placeholders::error));
}

void handleAccept(TcpSessionEngine* engine, TcpSession* session, const boost::system::error_code& ec)
{
	MySessionContext* ctx = session->getContext<MySessionContext>();

	if (!ec)
	{
		LOG4CXX_DEBUG(gLogger, "client #" << ctx->id << " accepted");

		sendTestMessage(*session);
		//session->readAsync(ctx->buffer, boost::bind(handleMessageRead, session, placeholders::error));
		uint32 dummy_type = 0;
		session->read(dummy_type, ctx->buffer);
		session->read(dummy_type, ctx->buffer);
		session->read(dummy_type, ctx->buffer);
		session->read(dummy_type, ctx->buffer);
		if(verifyTestMessage(*session, ctx->buffer))
		{
			LOG4CXX_INFO(gLogger, "client #" << ctx->id << " message verified");
		}
		else
		{
			LOG4CXX_ERROR(gLogger, "client #" << ctx->id << " verification failed");
		}
		//session->closeAsync(boost::bind(handleClosed));
	}
	else
	{
		LOG4CXX_INFO(gLogger, "client #" << ctx->id << " failed to accept, error = " << ec.message());
		session->markForDeletion();
	}
}

void handleClosed()
{
//	MySessionContext* ctx = session.getContext<MySessionContext>();
//
//	LOG4CXX_INFO(gLogger, "client #" << ctx->id << " closed");
}

//////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
	if (argc != 3)
	{
		std::cerr << "Usage: " << argv[0] << " <port> <concurrency>\n";
		return 1;
	}

	log4cxx::BasicConfigurator::configure();

	int port = atoi(argv[1]);
	int thread_count = atoi(argv[2]);

	TcpSessionEngine engine;

	engine.listenAsync(tcp::v4(), port, boost::bind(handleListen, &engine, placeholders::error));


	// run the service forever (until all events are processed)
	//io_service.run(thread_count);
	engine.run();

	return 0;
}
