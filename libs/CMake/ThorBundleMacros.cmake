#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

INCLUDE(ThorToolchainDefs)

SET(THORC_BUNDLE_PATH "${CMAKE_BINARY_DIR}/bundle")

FILE(MAKE_DIRECTORY ${THORC_BUNDLE_PATH})

MACRO(thor_bundle_declare)
    SET(THOR_BUNDLE_DECLARE_SWITCHES
        DEBUG
    )
    SET(THOR_BUNDLE_DECLARE_OPTS
        NAME
        TARGET_NAME
        OUTPUT_DIR
        INST_DEST
        INST_COMP
    )
    SET(THOR_BUNDLE_DECLARE_MULTIVALUE_OPTS
        DEPENDS
        DEPENDED
    )

    CMAKE_PARSE_ARGUMENTS(
        THOR_BUNDLE
        "${THOR_BUNDLE_DECLARE_SWITCHES}"
        "${THOR_BUNDLE_DECLARE_OPTS}"
        "${THOR_BUNDLE_DECLARE_MULTIVALUE_OPTS}"
        ${ARGN}
    )

    UNSET(THOR_BUNDLE_DECLARE_MULTIVALUE_OPTS)
    UNSET(THOR_BUNDLE_DECLARE_OPTS)
    UNSET(THOR_BUNDLE_DECLARE_SWITCHES)

    IF(NOT THOR_BUNDLE_NAME OR NOT THOR_BUNDLE_TARGET_NAME)
        MESSAGE(SEND_ERROR "bundle name or target name is required when declaring bundle")
    ELSE()
        SET(THOR_BUNDLE_FILENAME "${THOR_BUNDLE_NAME}.bundle"                        )
        SET(THOR_BUNDLE_ARCHS                                                        )
        SET(THOR_BUNDLE_LIBS_DIR "${CMAKE_CURRENT_BINARY_DIR}/lib"                   )
        SET(THOR_BUNDLE_WORK_DIR "${CMAKE_CURRENT_BINARY_DIR}/create_bundle_work_dir")
        SET(THOR_BUNDLE_TEMP_DIR "${THOR_BUNDLE_WORK_DIR}/${THOR_BUNDLE_NAME}"       )
    ENDIF()
ENDMACRO()

MACRO(thor_bundle_add_arch_x86)
    SET(THOR_BUNDLE_ADD_ARCH_X86_MULTIVALUE_OPTS
        INCLUDE_DIRS
        SOURCES
    )

    CMAKE_PARSE_ARGUMENTS(
        THOR_BUNDLE_ADD_ARCH_X86
        "" ""
        "${THOR_BUNDLE_ADD_ARCH_X86_MULTIVALUE_OPTS}"
        ${ARGN}
    )

    SET(THOR_BUNDLE_ADD_ARCH_X86_TARGET_NAME ${THOR_BUNDLE_TARGET_NAME}-x86)

    INCLUDE_DIRECTORIES(${THOR_BUNDLE_ADD_ARCH_X86_INCLUDE_DIRS})

    ADD_LIBRARY(${THOR_BUNDLE_ADD_ARCH_X86_TARGET_NAME}
        ${THOR_BUNDLE_ADD_ARCH_X86_SOURCES}
    )
    SET_TARGET_PROPERTIES(${THOR_BUNDLE_ADD_ARCH_X86_TARGET_NAME}
        PROPERTIES ARCHIVE_OUTPUT_DIRECTORY "${THOR_BUNDLE_LIBS_DIR}"
    )

    LIST(APPEND THOR_BUNDLE_ARCHS "x86")

    UNSET(THOR_BUNDLE_ADD_ARCH_X86_TARGET_NAME)
    UNSET(THOR_BUNDLE_ADD_ARCH_X86_MULTIVALUE_OPTS)
    UNSET(THOR_BUNDLE_ADD_ARCH_X86_OPTS)
    UNSET(THOR_BUNDLE_ADD_ARCH_X86_SWITCHES)
ENDMACRO()

MACRO(thor_bundle_add_arch_cuda)
    IF(CUDA_NVCC_EXECUTABLE)
        SET(THOR_BUNDLE_ADD_ARCH_CUDA_MULTIVALUE_OPTS
            SM_VERSIONS
            INCLUDE_DIRS
            SOURCES
        )

        CMAKE_PARSE_ARGUMENTS(
            THOR_BUNDLE_ADD_ARCH_CUDA
            "" ""
            "${THOR_BUNDLE_ADD_ARCH_CUDA_MULTIVALUE_OPTS}"
            ${ARGN}
        )

        IF(THOR_BUNDLE_ADD_ARCH_CUDA_SM_VERSIONS)
            FOREACH(sm_version ${THOR_BUNDLE_ADD_ARCH_CUDA_SM_VERSIONS})
                SET(THOR_BUNDLE_ADD_ARCH_CUDA_TARGET_NAME ${THOR_BUNDLE_TARGET_NAME}-sm_${sm_version})
                SET(THOR_BUNDLE_ADD_ARCH_CUDA_COMPILE_CMD ${CUDA_NVCC_EXECUTABLE} -arch=sm_${sm_version} -Xcompiler -fPIC -dc)

                IF(NOT ${CUDA_VERSION} VERSION_LESS "6.0")
                    LIST(APPEND THOR_BUNDLE_ADD_ARCH_CUDA_COMPILE_CMD --cudart=shared)
                ENDIF()

                SET(THOR_BUNDLE_ADD_ARCH_CUDA_OBJ_FILES)

                FOREACH(inc_dir ${THOR_BUNDLE_ADD_ARCH_CUDA_INCLUDE_DIRS})
                    LIST(APPEND THOR_BUNDLE_ADD_ARCH_CUDA_COMPILE_CMD "-I${inc_dir}")
                ENDFOREACH()

                FOREACH(src_file ${THOR_BUNDLE_ADD_ARCH_CUDA_SOURCES})
                    GET_FILENAME_COMPONENT(src_file_we "${src_file}" NAME_WE)

                    SET(obj_file_name "${src_file_we}.sm_${sm_version}.o")

                    # FIXME find a general way to scan dependencies of cuda source
                    ADD_CUSTOM_COMMAND(
                        OUTPUT  ${obj_file_name} dummy_output_to_trigger_build_${obj_file_name}
                        COMMAND ${THOR_BUNDLE_ADD_ARCH_CUDA_COMPILE_CMD} -o ${obj_file_name} ${src_file}
                        COMMENT "compiling CUDA source with sm_${sm_version} to ${obj_file_name}"
                    )

                    LIST(APPEND THOR_BUNDLE_ADD_ARCH_CUDA_OBJ_FILES ${obj_file_name})

                    UNSET(obj_file_name)
                    UNSET(src_file_we)
                ENDFOREACH()

                IF(NOT THOR_BUNDLE_ADD_ARCH_CUDA_OBJ_FILES)
                    MESSAGE(SEND_ERROR "no cuda source is given")
                ELSE()
                    SET(THOR_BUNDLE_ADD_ARCH_CUDA_LIB_NAME "lib${THOR_BUNDLE_ADD_ARCH_CUDA_TARGET_NAME}.a")
                    SET(THOR_BUNDLE_ADD_ARCH_CUDA_LIB_PATH "${THOR_BUNDLE_LIBS_DIR}/${THOR_BUNDLE_ADD_ARCH_CUDA_LIB_NAME}")

                    ADD_CUSTOM_TARGET(${THOR_BUNDLE_ADD_ARCH_CUDA_TARGET_NAME}
                                ar cr ${THOR_BUNDLE_ADD_ARCH_CUDA_LIB_PATH} ${THOR_BUNDLE_ADD_ARCH_CUDA_OBJ_FILES}
                        DEPENDS ${THOR_BUNDLE_ADD_ARCH_CUDA_OBJ_FILES}
                        COMMENT "archiving sm_${sm_version} object files into ${THOR_BUNDLE_ADD_ARCH_CUDA_LIB_PATH}"
                    )

                    LIST(APPEND THOR_BUNDLE_ARCHS "sm_${sm_version}")

                    UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_LIB_PATH)
                    UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_LIB_NAME)
                ENDIF()

                UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_OBJ_FILES)
                UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_COMPILE_CMD)
                UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_TARGET_NAME)
            ENDFOREACH()
        ELSE()
            MESSAGE(SEND_ERROR "no sm version is specified for nvcc")
        ENDIF()

        UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_MULTIVALUE_OPTS)
        UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_OPTS)
        UNSET(THOR_BUNDLE_ADD_ARCH_CUDA_SWITCHES)
    ELSE()
        MESSAGE(SEND_ERROR "nvcc is not found!")
    ENDIF()
ENDMACRO()

MACRO(thor_bundle_define)
    SET(THOR_BUNDLE_DEFINE_OPTS
        INST_DEST
        INST_COMP
    )

    CMAKE_PARSE_ARGUMENTS(
        THOR_BUNDLE_DEFINE
        ""
        "${THOR_BUNDLE_DEFINE_OPTS}"
        ""
        ${ARGN}
    )

    SET(THOR_BUNDLE_NATIVE_LIB_TAGS                                           )
    SET(THOR_BUDNLE_MANIFEST        "${CMAKE_CURRENT_BINARY_DIR}/manifest.xml")
    SET(THOR_BUNDLE_PRE_TARGET_NAME ${THOR_BUNDLE_TARGET_NAME}-pre            )

    ADD_CUSTOM_TARGET(${THOR_BUNDLE_PRE_TARGET_NAME}
                ${CMAKE_COMMAND} -E remove_directory  ${THOR_BUNDLE_WORK_DIR}
        COMMAND ${CMAKE_COMMAND} -E make_directory    ${THOR_BUNDLE_WORK_DIR}
        COMMAND ${CMAKE_COMMAND} -E copy_directory    ${CMAKE_CURRENT_SOURCE_DIR} ${THOR_BUNDLE_TEMP_DIR}
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${THOR_BUDNLE_MANIFEST} "${THOR_BUNDLE_TEMP_DIR}/manifest.xml"
        COMMAND ${CMAKE_COMMAND} -E copy_directory    ${THOR_BUNDLE_LIBS_DIR} "${THOR_BUNDLE_TEMP_DIR}/lib"
    )

    SET(THOR_BUNDLE_DEFINE_CMDS)

    # This is the first command in custom target, omit "COMMAND"
    IF(THOR_BUNDLE_DEBUG)
        LIST(APPEND THOR_BUNDLE_DEFINE_CMDS
            ${THOR_DRIVER} build debug   --verbose
        )
    ELSE()
        LIST(APPEND THOR_BUNDLE_DEFINE_CMDS
            ${THOR_DRIVER} build release --verbose
        )
    ENDIF()

    LIST(APPEND THOR_BUNDLE_DEFINE_CMDS
        COMMAND ${THOR_DRIVER} generate bundle
    )

    LIST(APPEND THOR_BUNDLE_DEFINE_CMDS
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${THOR_BUNDLE_TEMP_DIR}/bin/${THOR_BUNDLE_FILENAME} ${THOR_BUNDLE_OUTPUT_DIR}/${THOR_BUNDLE_FILENAME})

    ADD_CUSTOM_TARGET(${THOR_BUNDLE_TARGET_NAME}
                          ${THOR_BUNDLE_DEFINE_CMDS}
        WORKING_DIRECTORY ${THOR_BUNDLE_TEMP_DIR}
        DEPENDS           ${THOR_BUNDLE_PRE_TARGET_NAME}
        COMMENT "Building bundle: ${THOR_BUNDLE_NAME}"
    )

    FOREACH(arch ${THOR_BUNDLE_ARCHS})
        ADD_DEPENDENCIES(${THOR_BUNDLE_PRE_TARGET_NAME}
            ${THOR_BUNDLE_TARGET_NAME}-${arch}
        )

        SET(THOR_BUNDLE_NATIVE_LIB_TAGS
            "${THOR_BUNDLE_NATIVE_LIB_TAGS}\n<native_library name=\"thor-bundle-system-${arch}\" lpath=\"lib\" arch=\"${arch}\"/>"
        )
    ENDFOREACH()

    FOREACH(depend_target ${THOR_BUNDLE_DEPENDS})
        ADD_DEPENDENCIES(${THOR_BUNDLE_TARGET_NAME}
            ${depend_target}
        )
    ENDFOREACH()

    FOREACH(depended_target ${THOR_BUNDLE_DEPENDED})
        ADD_DEPENDENCIES(${depended_target}
            ${THOR_BUNDLE_TARGET_NAME}
        )
    ENDFOREACH()

    CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/manifest.xml ${THOR_BUDNLE_MANIFEST})

    IF(THOR_BUNDLE_DEFINE_INST_DEST AND THOR_BUNDLE_DEFINE_INST_COMP)
        INSTALL(
            FILES       ${THORC_BUNDLE_PATH}/${THOR_BUNDLE_FILENAME}
            DESTINATION ${THOR_BUNDLE_DEFINE_INST_DEST}
            COMPONENT   ${THOR_BUNDLE_DEFINE_INST_COMP}
        )
    ENDIF()

    UNSET(THOR_BUNDLE_DEFINE_CMDS)
    UNSET(THOR_BUNDLE_PRE_TARGET_NAME)
    UNSET(THOR_BUDNLE_MANIFEST)
    UNSET(THOR_BUNDLE_NATIVE_LIB_TAGS)
    UNSET(THOR_BUNDLE_DEFINE_OPTS)
ENDMACRO()
