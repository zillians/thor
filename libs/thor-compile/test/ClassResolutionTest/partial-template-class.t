/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test { resolution="Bar" }
class Bar
{
    @static_test { resolution="Bar.a" } static var a:int32 = 0;
    @static_test { resolution="Bar.b" } static var b:int64 = 0;
    @static_test { resolution="Bar.c" } static var c:Foo = null;
}

@static_test { resolution="Foo" }
class Foo
{
    @static_test { resolution="Foo.a" } static var a:int32 = 0;
    @static_test { resolution="Foo.b" } static var b:int64 = 0;
    @static_test { resolution="Foo.c" } static var c:Bar = null;
}

@static_test { resolution="Use<T,U>" }
class Use<T, U> extends Foo
{
    @static_test { resolution="Use<T,U>.v" } static var v : Bar = null;
}

@static_test { resolution="Use<float32,U>" }
class Use<T:float32, U> extends Bar
{
    @static_test { resolution="Use<float32,U>.v" } static var v : U = null;
}

@static_test { resolution="Use<float64,U>" }
class Use<T:float64, U> extends U
{
    @static_test { resolution="Use<float64,U>.v" } static var v : Bar = null;
}

function ForTest() : void
{
    @static_test { expect_resolution="Use<T,U>.v"        } Use<int32, int32>.v;
    @static_test { expect_resolution="Use<float32,U>.v"  } Use<float32, int32>.v;
    @static_test { expect_resolution="Use<float64,U>.v"  } Use<float64, Foo>.v;

    @static_test { expect_resolution="Foo.a"  } Use<int32, int32>.a;
    @static_test { expect_resolution="Foo.b"  } Use<int32, int32>.b;
    @static_test { expect_resolution="Foo.c"  } Use<int32, int32>.c;
    @static_test { expect_resolution="Bar.a"  } Use<int32, int32>.v.a;
    @static_test { expect_resolution="Bar.b"  } Use<int32, int32>.v.b;
    @static_test { expect_resolution="Bar.c"  } Use<int32, int32>.v.c;

    @static_test { expect_resolution="Foo.a"  } Use<int32, int32>.v.c.a;
    @static_test { expect_resolution="Foo.b"  } Use<int32, int32>.v.c.b;
    @static_test { expect_resolution="Foo.c"  } Use<int32, int32>.v.c.c;
    @static_test { expect_resolution="Bar.a"  } Use<int32, int32>.v.c.c.a;
    @static_test { expect_resolution="Bar.b"  } Use<int32, int32>.v.c.c.b;
    @static_test { expect_resolution="Bar.c"  } Use<int32, int32>.v.c.c.c;

    @static_test { expect_resolution="Foo.a"  } Use<float64, Foo>.a;
    @static_test { expect_resolution="Bar.a"  } Use<float64, Bar>.a;
}
