/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

@static_test { resolution="Foo" }
class Foo {}

@static_test { resolution="Xer<T>" }
class Xer<T> {}

@static_test { resolution="Xer<Foo>" }
class Xer<T:Foo> {}

@static_test { resolution="Vas<T>" }
class Vas<T> {}

@static_test { resolution="Vas<Xer<Foo>>" }
class Vas<T:Xer<Foo> > {}

typedef Foo FooAlias;
typedef FooAlias FooAliasAlias;
typedef FooAliasAlias FooAliasAliasAlias;

typedef Xer<Foo> XerFoo;
typedef XerFoo XerFooAlias;

function main() : void
{
    { @static_test { expect_resolution="Xer<Foo>" } var a : Xer<FooAlias>; }
    { @static_test { expect_resolution="Xer<Foo>" } var a : Xer<FooAliasAlias>; }
    { @static_test { expect_resolution="Xer<Foo>" } var a : Xer<FooAliasAliasAlias>; }
    { @static_test { expect_resolution="Vas<Xer<Foo>>" } var a : Vas<XerFoo>; }
    { @static_test { expect_resolution="Vas<Xer<Foo>>" } var a : Vas<XerFooAlias>; }
}
