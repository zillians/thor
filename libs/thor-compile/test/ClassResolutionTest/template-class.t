/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test { resolution="Bar" }
class Bar
{
    @static_test { resolution="Bar.v"        }        var base_v        : int16;
    @static_test { resolution="Bar.static_v" } static var base_static_v : int32 = 0;
}

@static_test { resolution="Foo" }
class Foo
{
    @static_test { resolution="Foo.v"        }        var base_v        : Foo;
    @static_test { resolution="Foo.static_v" } static var base_static_v : Bar = null;
}

@static_test { resolution="Use<T,U>" }
class Use<T, U> extends Foo
{
    @static_test { resolution="Use<T,U>.v"        }        var v        : Bar ;
    @static_test { resolution="Use<T,U>.static_v" } static var static_v : Foo = null;
}

function ForTest() : void
{
    @static_test { expect_resolution="Use<T,U>.v"        } Use<int32, int32>.v;
    @static_test { expect_resolution="Bar.v"             } Use<int32, int32>.v.base_v;
    @static_test { expect_resolution="Bar.static_v"      } Use<int32, int32>.v.base_static_v;

    @static_test { expect_resolution="Use<T,U>.static_v" } Use<int32, int32>.static_v;
    @static_test { expect_resolution="Foo.v"             } Use<int32, int32>.static_v.base_v;
    @static_test { expect_resolution="Foo.static_v"      } Use<int32, int32>.static_v.base_static_v;
}
