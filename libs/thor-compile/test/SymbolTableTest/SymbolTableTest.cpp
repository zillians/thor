/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>
#include <boost/logic/tribool.hpp>

#include "utility/UUIDUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/context/ParserContext.h"
#include "language/resolver/SymbolTable.h"

#define BOOST_TEST_MODULE ThorResolutionTest_SymbolTableTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( ThorResolutionTest_SymbolTableTestTestSuite )


BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestNameCollide )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    VariableDecl* var_a  = new VariableDecl(new SimpleIdentifier(L"a"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* var_a2 = new VariableDecl(new SimpleIdentifier(L"a"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* var_b  = new VariableDecl(new SimpleIdentifier(L"b"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);

    Block* block1 = new NormalBlock();

    SymbolTable st;
    st.disable_report_error();
    BOOST_CHECK(st.size() == 0);

    st.enter(block1);
    BOOST_CHECK(st.insert(var_a ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 1);
    BOOST_CHECK(st.insert(var_b ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 2);
    BOOST_CHECK(st.insert(var_a2).is_dupe()); BOOST_CHECK_EQUAL(st.size(), 2);
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestScopeRemove )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    VariableDecl* var_a = new VariableDecl(new SimpleIdentifier(L"a"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* var_b = new VariableDecl(new SimpleIdentifier(L"b"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* var_c = new VariableDecl(new SimpleIdentifier(L"c"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);

    Block* block1 = new NormalBlock();
    Block* block2 = new NormalBlock();

    SymbolTable st;
    BOOST_CHECK(st.size() == 0);

    st.enter(block1);
    BOOST_CHECK(st.insert(var_a).is_ok()); BOOST_CHECK_EQUAL(st.size(), 1);

    st.enter(block2);
    BOOST_CHECK(st.insert(var_b).is_ok()); BOOST_CHECK_EQUAL(st.size(), 2);
    BOOST_CHECK(st.insert(var_c).is_ok()); BOOST_CHECK_EQUAL(st.size(), 3);

    st.leave(block2);
    BOOST_CHECK_EQUAL(st.size(), 1);
    st.leave(block1);
    BOOST_CHECK_EQUAL(st.size(), 0);
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestFunctionOverloadingAndSpecialization )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    FunctionDecl* f1 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f2 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f3 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());

    ClassDecl* class_f = new ClassDecl(new SimpleIdentifier(L"f"));

    VariableDecl* var_a = new VariableDecl(new SimpleIdentifier(L"a"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* var_f = new VariableDecl(new SimpleIdentifier(L"f"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);

    Block* block1 = new NormalBlock();
    Block* block2 = new NormalBlock();
    Block* block3 = new NormalBlock();

    SymbolTable st;
    st.disable_report_error();
    BOOST_CHECK(st.size() == 0);

    st.enter(block1);
    BOOST_CHECK(st.insert(f1   ).is_ok()); BOOST_CHECK_EQUAL(st.size(), 1);
    BOOST_CHECK(st.insert(var_a).is_ok()); BOOST_CHECK_EQUAL(st.size(), 2);

    st.enter(block2);
    BOOST_CHECK(st.insert(f2).is_ok()); BOOST_CHECK_EQUAL(st.size(), 3);

    st.enter(block3);
    BOOST_CHECK(st.insert(f3).is_ok()); BOOST_CHECK_EQUAL(st.size(), 4);

    BOOST_CHECK(st.insert(var_f).is_dupe());
    BOOST_CHECK_EQUAL(st.size(), 4);

    st.leave(block3);
    BOOST_CHECK_EQUAL(st.size(), 3);

    st.leave(block2);
    BOOST_CHECK_EQUAL(st.size(), 2);

    st.leave(block1);
    BOOST_CHECK_EQUAL(st.size(), 0);
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestClassSpecialization )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    ClassDecl* class1 = new ClassDecl(new SimpleIdentifier(L"Foo"));
    ClassDecl* class2 = new ClassDecl(new SimpleIdentifier(L"Foo"));
    ClassDecl* class3 = new ClassDecl(new SimpleIdentifier(L"Foo"));

    FunctionDecl* f_foo = new FunctionDecl(new SimpleIdentifier(L"Foo"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f_bar = new FunctionDecl(new SimpleIdentifier(L"bar"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());

    VariableDecl* var_foo = new VariableDecl(new SimpleIdentifier(L"Foo"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* var_bar = new VariableDecl(new SimpleIdentifier(L"bar"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);

    Block* block1 = new NormalBlock();
    Block* block2 = new NormalBlock();
    Block* block3 = new NormalBlock();

    SymbolTable st;
    st.disable_report_error();
    BOOST_CHECK_EQUAL(st.size(), 0);

    st.enter(block1);
    BOOST_CHECK(st.insert(class1).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 1);
    BOOST_CHECK(st.insert(class2).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 2);
    BOOST_CHECK(st.insert(f_foo ).is_dupe()); BOOST_CHECK_EQUAL(st.size(), 2);
    BOOST_CHECK(st.insert(f_bar ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 3);

    st.enter(block2);
    BOOST_CHECK(st.insert(class3 ).is_ok()    ); BOOST_CHECK_EQUAL(st.size(), 4);
    BOOST_CHECK(st.insert(var_foo).is_dupe()  ); BOOST_CHECK_EQUAL(st.size(), 4);
    BOOST_CHECK(st.insert(var_bar).is_shadow()); BOOST_CHECK_EQUAL(st.size(), 4);

    st.leave(block2);
    BOOST_CHECK_EQUAL(st.size(), 3);

    st.leave(block1);
    BOOST_CHECK_EQUAL(st.size(), 0);
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestFind )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    ClassDecl* class1 = new ClassDecl(new SimpleIdentifier(L"Foo"));
    ClassDecl* class2 = new ClassDecl(new SimpleIdentifier(L"Foo"));
    ClassDecl* class3 = new ClassDecl(new SimpleIdentifier(L"Foo"));

    FunctionDecl* f1 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f2 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());

    VariableDecl* v1 = new VariableDecl(new SimpleIdentifier(L"v1"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);
    VariableDecl* v2 = new VariableDecl(new SimpleIdentifier(L"v2"), new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), false, false, false, Declaration::VisibilitySpecifier::DEFAULT);

    Block* block1 = new NormalBlock();
    Block* block2 = new NormalBlock();
    Block* block3 = new NormalBlock();

    SymbolTable st;
    st.set_clean();
    BOOST_CHECK(st.size() == 0);

    BOOST_CHECK(!st.has(L"Foo"));
    BOOST_CHECK(!st.has(L"f"));
    BOOST_CHECK(!st.has(L"v1"));
    BOOST_CHECK(!st.has(L"v2"));

    st.enter(block1);
    BOOST_CHECK(st.insert(class1).is_ok()); BOOST_CHECK_EQUAL(st.size(), 1);
    BOOST_CHECK(st.insert(f1    ).is_ok()); BOOST_CHECK_EQUAL(st.size(), 2);
    BOOST_CHECK(st.insert(v1    ).is_ok()); BOOST_CHECK_EQUAL(st.size(), 3);

    st.enter(block2);
    BOOST_CHECK(st.insert(class2).is_ok()); BOOST_CHECK_EQUAL(st.size(), 4);
    BOOST_CHECK(st.insert(f2    ).is_ok()); BOOST_CHECK_EQUAL(st.size(), 5);
    BOOST_CHECK(st.insert(v2    ).is_ok()); BOOST_CHECK_EQUAL(st.size(), 6);

    st.enter(block3);
    BOOST_CHECK(st.insert(class3).is_ok()); BOOST_CHECK(st.size() == 7);

    BOOST_CHECK(st.has(L"Foo"));
    BOOST_CHECK(st.has(L"f"));
    BOOST_CHECK(st.has(L"v1"));
    BOOST_CHECK(st.has(L"v2"));

    {
        SymbolTable::FindResult result = st.find(L"Foo");
        BOOST_CHECK(result.is_found);
        BOOST_CHECK_EQUAL(result.candidates.size(), 3);
        BOOST_CHECK_EQUAL(result.candidates[0], class3);
        BOOST_CHECK_EQUAL(result.candidates[1], class2);
        BOOST_CHECK_EQUAL(result.candidates[2], class1);
    }

    {
        SymbolTable::FindResult result = st.find(L"f");
        BOOST_CHECK(result.is_found);
        BOOST_CHECK_EQUAL(result.candidates.size(), 2);
        BOOST_CHECK_EQUAL(result.candidates[0], f2);
        BOOST_CHECK_EQUAL(result.candidates[1], f1);
    }

    {
        SymbolTable::FindResult result = st.find(L"v1");
        BOOST_CHECK(result.is_found);
        BOOST_CHECK(result.candidates.size() == 1);
        BOOST_CHECK_EQUAL(result.candidates[0], v1);
    }

    {
        SymbolTable::FindResult result = st.find(L"v2");
        BOOST_CHECK(result.is_found);
        BOOST_CHECK(result.candidates.size() == 1);
        BOOST_CHECK_EQUAL(result.candidates[0], v2);
    }
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestFindInBase )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    SymbolTable b1;
    SymbolTable b2;
    SymbolTable st;

    FunctionDecl* f1 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f2 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f3 = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());

    BOOST_CHECK(b1.insert(f1).is_ok()); b1.set_clean();
    BOOST_CHECK(b2.insert(f2).is_ok()); b2.set_clean();

    BOOST_CHECK(boost::indeterminate(st.has(L"f")));

    st.set_clean();

    BOOST_CHECK(!st.has(L"f"));

    st.add_base(&b1);
    st.add_base(&b2);

    BOOST_CHECK(st.has(L"f"));

    {
        SymbolTable::FindResult result = st.find(L"f");
        BOOST_CHECK_EQUAL(result.candidates.size(), 2);
        BOOST_CHECK_EQUAL(result.candidates[0], f1);
        BOOST_CHECK_EQUAL(result.candidates[1], f2);
    }

    // "f" in st will shadow "f" in b1 & b2
    st.insert(f3);

    {
        SymbolTable::FindResult result = st.find(L"f");
        BOOST_CHECK_EQUAL(result.candidates.size(), 1);
        BOOST_CHECK_EQUAL(result.candidates[0], f3);
    }
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestImport )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    setParserContext(new ParserContext());
    getParserContext().tangle = new Tangle(zillians::UUID::random());

    Package* package_root = new Package(new SimpleIdentifier(L""));    package_root->symbol_table.set_clean();
    Package* package_a    = new Package(new SimpleIdentifier(L"a"));   package_a   ->symbol_table.set_clean();
    Package* package_b    = new Package(new SimpleIdentifier(L"b"));   package_b   ->symbol_table.set_clean();
    Package* package_e    = new Package(new SimpleIdentifier(L"e"));   package_e   ->symbol_table.set_clean();
    Package* package_x    = new Package(new SimpleIdentifier(L"x"));   package_e   ->symbol_table.set_clean();
    Package* package_y    = new Package(new SimpleIdentifier(L"y"));   package_e   ->symbol_table.set_clean();
    Package* package_z    = new Package(new SimpleIdentifier(L"z"));   package_e   ->symbol_table.set_clean();

    getParserContext().tangle->root = package_root;

    package_root->addPackage(package_a); package_root->symbol_table.insert(package_a);
    package_root->addPackage(package_b); package_root->symbol_table.insert(package_b);
    package_root->addPackage(package_e); package_root->symbol_table.insert(package_e);

    package_root->addPackage(package_x); package_root->symbol_table.insert(package_x);
    package_x   ->addPackage(package_y); package_x   ->symbol_table.insert(package_y);
    package_y   ->addPackage(package_z); package_y   ->symbol_table.insert(package_z);

    Import* import_a    = new Import(new SimpleIdentifier(L"a"));
    Import* import_b    = new Import(new SimpleIdentifier(L"b"));
    Import* import_c_b  = new Import(new SimpleIdentifier(L"c"), new SimpleIdentifier(L"b"));
    Import* import_d_e  = new Import(new SimpleIdentifier(L"d"), new SimpleIdentifier(L"e"));

    NestedIdentifier* nid_xy = new NestedIdentifier();
    nid_xy->appendIdentifier(new SimpleIdentifier(L"x"));
    nid_xy->appendIdentifier(new SimpleIdentifier(L"y"));
    Import* import_xy = new Import(nid_xy);

    NestedIdentifier* nid_xyz = new NestedIdentifier();
    nid_xyz->appendIdentifier(new SimpleIdentifier(L"x"));
    nid_xyz->appendIdentifier(new SimpleIdentifier(L"y"));
    nid_xyz->appendIdentifier(new SimpleIdentifier(L"z"));
    Import* import_xyz = new Import(nid_xyz);

    SymbolTable st;
    st.set_clean();
    st.disable_report_error();
    BOOST_CHECK_EQUAL(st.size(), 0);

    Source* source_a = new Source("a.t", false);
    st.enter(source_a);

    BOOST_CHECK(st.insert(import_a  ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 1);
    BOOST_CHECK(st.insert(import_b  ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 2);
    BOOST_CHECK(st.insert(import_c_b).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 3);
    BOOST_CHECK(st.insert(import_c_b).is_dupe()); BOOST_CHECK_EQUAL(st.size(), 3);
    BOOST_CHECK(st.insert(import_d_e).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 5);

    SymbolTable::FindResult r_a = st.find(L"a"); BOOST_CHECK(r_a.is_found); BOOST_CHECK_EQUAL(r_a.candidates[0], package_a);
    SymbolTable::FindResult r_b = st.find(L"b"); BOOST_CHECK(r_b.is_found); BOOST_CHECK_EQUAL(r_b.candidates[0], package_b);
    SymbolTable::FindResult r_c = st.find(L"c"); BOOST_CHECK(r_c.is_found); BOOST_CHECK_EQUAL(r_c.candidates[0], package_b);
    SymbolTable::FindResult r_d = st.find(L"d"); BOOST_CHECK(r_d.is_found); BOOST_CHECK_EQUAL(r_d.candidates[0], package_e);
    SymbolTable::FindResult r_e = st.find(L"e"); BOOST_CHECK(r_e.is_found); BOOST_CHECK_EQUAL(r_e.candidates[0], package_e);

    Source* source_b = new Source("b.t", false);
    st.enter(source_b);

    BOOST_CHECK(st.insert(import_xy  ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 6);
    BOOST_CHECK(st.insert(import_xyz ).is_ok()  ); BOOST_CHECK_EQUAL(st.size(), 6);

    BOOST_CHECK(st.find(L"x").is_found);
    BOOST_CHECK(!st.find(L"x.y").is_found);
    BOOST_CHECK(!st.find(L"x.y.z").is_found);

    st.leave(source_b);
    BOOST_CHECK_EQUAL(st.size(), 5);

    st.leave(source_a);
    BOOST_CHECK_EQUAL(st.size(), 0);

    setParserContext(NULL);
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestVisible )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    setParserContext(new ParserContext());
    getParserContext().tangle = new Tangle(zillians::UUID::random());

    Package* package_root = new Package(new SimpleIdentifier(L""));    package_root->symbol_table.set_clean();
    Package* package_a    = new Package(new SimpleIdentifier(L"a"));   package_a   ->symbol_table.set_clean();
    Package* package_b    = new Package(new SimpleIdentifier(L"b"));   package_b   ->symbol_table.set_clean();
    Package* package_b_2  = new Package(new SimpleIdentifier(L"b_2")); package_b_2 ->symbol_table.set_clean();
    Package* package_c    = new Package(new SimpleIdentifier(L"c"));   package_c   ->symbol_table.set_clean();
    Package* package_d    = new Package(new SimpleIdentifier(L"d"));   package_d   ->symbol_table.set_clean();

    getParserContext().tangle->root = package_root;

    package_root->addPackage(package_a);            package_root->symbol_table.insert(package_a);
    package_a   ->addPackage(package_b);            package_a   ->symbol_table.insert(package_b);
    package_a   ->addPackage(package_b_2);          package_a   ->symbol_table.insert(package_b_2);
    package_b   ->addPackage(package_c);            package_b   ->symbol_table.insert(package_c);
    package_c   ->addPackage(package_d);            package_c   ->symbol_table.insert(package_d);

    FunctionDecl* f_in_package_a = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    FunctionDecl* f_in_package_d = new FunctionDecl(new SimpleIdentifier(L"f"), NULL, false, false, false, false, Declaration::VisibilitySpecifier::PUBLIC, new NormalBlock());
    package_a->symbol_table.insert(f_in_package_a);
    package_d->symbol_table.insert(f_in_package_d);

    NestedIdentifier* nid_abcd = new NestedIdentifier();
    nid_abcd->appendIdentifier(new SimpleIdentifier(L"a"));
    nid_abcd->appendIdentifier(new SimpleIdentifier(L"b"));
    nid_abcd->appendIdentifier(new SimpleIdentifier(L"c"));
    nid_abcd->appendIdentifier(new SimpleIdentifier(L"d"));
    Import* import_abcd = new Import(nid_abcd);

    BOOST_CHECK_EQUAL(package_a  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_b  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_b_2->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_c  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_d  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);

    // set import status
    SymbolTable st;
    st.set_clean();
    Source* source_a = new Source("a.t", false);
    st.enter(source_a);

    st.insert(import_abcd);

    BOOST_CHECK_EQUAL(package_a  ->getImportStatus(), SymbolTable::ImportStatus::ON_PATH);
    BOOST_CHECK_EQUAL(package_b  ->getImportStatus(), SymbolTable::ImportStatus::ON_PATH);
    BOOST_CHECK_EQUAL(package_b_2->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_c  ->getImportStatus(), SymbolTable::ImportStatus::ON_PATH);
    BOOST_CHECK_EQUAL(package_d  ->getImportStatus(), SymbolTable::ImportStatus::IMPORTED);

    // root can see a
    SymbolTable::FindResult result_a = package_root->symbol_table.find(L"a");
    BOOST_CHECK(result_a.is_found);
    BOOST_CHECK_EQUAL(result_a.candidates.size(), 1);
    BOOST_CHECK_EQUAL(result_a.candidates[0], package_a);

    // a can not see f
    SymbolTable::FindResult result_a_f = package_root->symbol_table.find(L"f");
    BOOST_CHECK(!result_a_f.is_found);
    BOOST_CHECK_EQUAL(result_a_f.candidates.size(), 0);

    // a can see b
    SymbolTable::FindResult result_b = package_a->symbol_table.find(L"b");
    BOOST_CHECK(result_b.is_found);
    BOOST_CHECK_EQUAL(result_b.candidates.size(), 1);
    BOOST_CHECK_EQUAL(result_b.candidates[0], package_b);

    // a can not see b_2
    SymbolTable::FindResult result_none = package_a->symbol_table.find(L"b_2");
    BOOST_CHECK(!result_none.is_found);
    BOOST_CHECK_EQUAL(result_none.candidates.size(), 0);

    // d can see f
    SymbolTable::FindResult result_d_f = package_d->symbol_table.find(L"f");
    BOOST_CHECK(result_d_f.is_found);
    BOOST_CHECK_EQUAL(result_d_f.candidates.size(), 1);
    BOOST_CHECK_EQUAL(result_d_f.candidates[0], f_in_package_d);

    // revert import status
    st.leave(source_a);
    BOOST_CHECK_EQUAL(st.size(), 0);

    BOOST_CHECK_EQUAL(package_a  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_b  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_b_2->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_c  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_d  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);

    // import . = a.b.c.d ;
    NestedIdentifier* nid_dot_abcd = new NestedIdentifier();
    nid_dot_abcd->appendIdentifier(new SimpleIdentifier(L"a"));
    nid_dot_abcd->appendIdentifier(new SimpleIdentifier(L"b"));
    nid_dot_abcd->appendIdentifier(new SimpleIdentifier(L"c"));
    nid_dot_abcd->appendIdentifier(new SimpleIdentifier(L"d"));
    Import* import_dot_abcd = new Import(new SimpleIdentifier(L""), nid_dot_abcd);

    // import . = a.b_2 ;
    NestedIdentifier* nid_dot_ab2 = new NestedIdentifier();
    nid_dot_ab2->appendIdentifier(new SimpleIdentifier(L"a"));
    nid_dot_ab2->appendIdentifier(new SimpleIdentifier(L"b_2"));
    Import* import_dot_ab2 = new Import(new SimpleIdentifier(L""), nid_dot_ab2);

    st.enter(source_a);

    SymbolTable::FindResult result_dot_f_before = st.find(L"f");
    BOOST_CHECK(!result_dot_f_before.is_found);
    BOOST_CHECK(st.insert(import_dot_abcd).is_ok());
    BOOST_CHECK_EQUAL(st.size(), 1);
    BOOST_CHECK(st.insert(import_dot_ab2).is_ok());
    BOOST_CHECK_EQUAL(st.size(), 1);
    SymbolTable::FindResult result_dot_f = st.find(L"f");
    BOOST_CHECK(result_dot_f.is_found);
    BOOST_CHECK_EQUAL(result_dot_f.candidates.size(), 1);
    BOOST_CHECK_EQUAL(result_dot_f.candidates[0], f_in_package_d);
    SymbolTable::FindResult result_dot_a = st.find(L"a");
    BOOST_CHECK_EQUAL(result_dot_a.candidates.size(), 1);
    BOOST_CHECK_EQUAL(result_dot_a.candidates[0], package_a);

    st.leave(source_a);

    BOOST_CHECK(!st.find(L"f").is_found);

    BOOST_CHECK_EQUAL(st.size_base(), 0);
    BOOST_CHECK_EQUAL(package_a  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_b  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_b_2->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_c  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
    BOOST_CHECK_EQUAL(package_d  ->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);

    setParserContext(NULL);
}

BOOST_AUTO_TEST_CASE( ThorResolutionTest_SymbolTableTestRootImport )
{
    using namespace zillians::language;
    using namespace zillians::language::tree;

    setParserContext(new ParserContext());
    getParserContext().tangle = new Tangle(zillians::UUID::random());

    Package* package_root = new Package(new SimpleIdentifier(L""));    package_root->symbol_table.set_clean();
    Package* package_a    = new Package(new SimpleIdentifier(L"a"));   package_a   ->symbol_table.set_clean();

    getParserContext().tangle->root = package_root;

    package_root->addPackage(package_a);            package_root->symbol_table.insert(package_a);

    Source* source_a = new Source("a.t", false);
    package_root->symbol_table.enter(source_a);

    SimpleIdentifier* nid_a = new SimpleIdentifier(L"a");
    Import* import_a = new Import(nid_a);

    BOOST_CHECK(!package_root->symbol_table.find(L"a").is_found);
    BOOST_CHECK_EQUAL(package_root->symbol_table.size(), 1);

    BOOST_CHECK(package_root->symbol_table.insert(import_a).is_ok());

    BOOST_CHECK(package_root->symbol_table.find(L"a").is_found);
    BOOST_CHECK_EQUAL(package_root->symbol_table.size(), 1);

    package_root->symbol_table.leave(source_a);

    BOOST_CHECK(!package_root->symbol_table.find(L"a").is_found);
    BOOST_CHECK_EQUAL(package_root->symbol_table.size(), 1);
    BOOST_CHECK_EQUAL(package_a->getImportStatus(), SymbolTable::ImportStatus::NOT_IMPORTED);
}


BOOST_AUTO_TEST_SUITE_END()
