/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// global name conflicts
//import   test_2; // cannot static test annotations on packages, comment it
@static_test { expect_message = { level = "LEVEL_WARNING", id = "MISSING_VAR_INIT" } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } } var      global_test_2: int32;
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"   } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } class    global_test_2   {}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } }
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } class    global_test_2<T>{}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } function global_test_2   (                  ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } function global_test_2   (a: int32          ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } function global_test_2   (a: int32, b: int32): void {}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } function global_test_2<T>(                  ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } function global_test_2<T>(a: T              ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "global_test_2" } } } function global_test_2<T>(a: T    , b: T    ): void {}

// duplicated parameters
function test_6(
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } } a: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } } a: int32
): void {}
function test_6(
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                            } } a: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } } a: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = { id = "a" } } } a: int32
): void {}

// duplicated global variables
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                           } } var var_test_1: int32 = 0;
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "var_test_1" } } } var var_test_1: int32 = 0;

@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                           } } var var_test_2: int32   = 0;
@static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "var_test_2" } } } var var_test_2: float32 = 0;

function var_test_3(): int32
{
    // duplicated local variables
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } } var local_1_1: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_1" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_1" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_1" } } } var local_1_1: int32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } } var local_1_2: int32   = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_2" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_2" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_2" } } } var local_1_2: float32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } } static var local_1_3: int32   = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_3" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_3" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_1_3" } } }        var local_1_3: float32 = 0;

    {
        var local_2_1: int32;
        var local_2_2: int32;
        var local_2_3: int32;

        // duplicated local variables in inner block
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } } var local_2_4: int32 = 0;
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_4" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_4" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_4" } } } var local_2_4: int32 = 0;

        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } } var local_2_5: int32   = 0;
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_5" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_5" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_5" } } } var local_2_5: float32 = 0;

        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                        } } static var local_2_6: int32   = 0;
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_6" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_6" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"     , parameters = {     id = "local_2_6" } } }        var local_2_6: float32 = 0;
    }

    return 0;
}

class var_test_4
{
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                         } } var member_1: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_1" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_1" } } } var member_1: int32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                         } } var member_2: int32   = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_2" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_2" } } } var member_2: float32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                         } } static var member_3: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_3" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_3" } } }        var member_3: int32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME_PREV"                                         } } static var member_4: int32   = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_4" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME"       , parameters = {     id = "member_4" } } }        var member_4: float32 = 0;

    // duplicated template parameters
    // - annotation cannot be applied on template parameter, comment out the following lines
    //function test_7<
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T,
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T
    //>(): void {}
    //function test_7<
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T,
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T,
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T
    //>(): void {}
    //function test_7<
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T
    //>(
    //    @static_test { expect_message = { level = "LEVEL_ERROR"  , id = "DUPE_NAME", parameters = { id = "T" } } } T: T
    //): void {}
}

