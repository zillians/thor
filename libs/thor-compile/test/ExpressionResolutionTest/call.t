/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import pl;

class Foo
{
    public function h(       ) : void {}
    public function i(a:int32) : void {}

    public function j<T>(   ) : void {}
    public function k<T>(a:T) : void {}
}

function h(       ) : void {}
function i(a:int32) : void {}

function j<T>(   ) : void {}
function k<T>(a:T) : void {}

function main() : void
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CALL_NONFUNC", parameters={ id="vint32" } } }
    var vint32     : int32             ;
    var vFoo       : Foo               ;

    // call function
    h();
    i(1);
    j<int32>();
    k(1);
    k<int32>(1);

    vFoo.h();
    vFoo.i(1);
    vFoo.j<int32>();
    vFoo.k(1);
    vFoo.k<int32>(1);

    // call non function
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="no_symbol" } } } no_symbol();
    @static_test { expect_message={ level="LEVEL_ERROR", id="CALL_NONFUNC"         , parameters={id="pl"        } } } pl();
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="vint32()"  } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="vint32"    } } } vint32();
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="invoke"    } } } Foo();
    @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="invoke"    } } } vFoo();
}
