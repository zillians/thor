/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Foo
{
    @static_test { resolution = "Foo.b"   } var b  :bool;
    @static_test { resolution = "Foo.i8"  } var i8 :int8;
    @static_test { resolution = "Foo.i16" } var i16:int16;
    @static_test { resolution = "Foo.i32" } var i32:int32;
    @static_test { resolution = "Foo.i64" } var i64:int64;
    @static_test { resolution = "Foo.f32" } var f32:float32;
    @static_test { resolution = "Foo.f64" } var f64:float64;
}

function test(): void
{
    var b  :bool;
    var i8 :int8;
    var i16:int16;
    var i32:int32;
    var i64:int64;
    var f32:float32;
    var f64:float64;

    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += b;
    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += i8;
    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += i16;
    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += i32;
    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += i64;
    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += f32;
    @static_test { expect_resolution = "Foo.b"   } @static_test { expect_type="bool"    } Foo.b   += f64;

    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += b;
    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += i8;
    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += i16;
    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += i32;
    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += i64;
    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += f32;
    @static_test { expect_resolution = "Foo.i8"  } @static_test { expect_type="int8"    } Foo.i8  += f64;

    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += b;
    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += i8;
    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += i16;
    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += i32;
    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += i64;
    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += f32;
    @static_test { expect_resolution = "Foo.i16" } @static_test { expect_type="int16"   } Foo.i16 += f64;

    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += b;
    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += i8;
    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += i16;
    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += i32;
    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += i64;
    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += f32;
    @static_test { expect_resolution = "Foo.i32" } @static_test { expect_type="int32"   } Foo.i32 += f64;

    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += b;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += i8;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += i16;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += i32;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += i64;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += f32;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += f64;
    @static_test { expect_resolution = "Foo.i64" } @static_test { expect_type="int64"   } Foo.i64 += f64;

    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += b;
    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += i8;
    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += i16;
    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += i32;
    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += i64;
    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += f32;
    @static_test { expect_resolution = "Foo.f32" } @static_test { expect_type="float32" } Foo.f32 += f64;

    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += b;
    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += i8;
    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += i16;
    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += i32;
    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += i64;
    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += f32;
    @static_test { expect_resolution = "Foo.f64" } @static_test { expect_type="float64" } Foo.f64 += f64;
}
