/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class cls1              {}
class cls2 extends cls1 {}
enum  en1 { e1, e2 }

function test(
    e  : en1
,   b  : bool
,   i8 : int8
,   i16: int16
,   i32: int32
,   i64: int64
,   f32: float32
,   f64: float64
,   c1 : cls1
,   c2 : cls2
//,   f1 : function(): void
//,   f2 : function(int8): void
//,   f3 : function(int8,int32): void
): void
{
    @static_test { expect_type=""                         } true ? null: null;
    @static_test { expect_type=""                         } true ? null: e   ;
    @static_test { expect_type=""                         } true ? null: b   ;
    @static_test { expect_type=""                         } true ? null: i8  ;
    @static_test { expect_type=""                         } true ? null: i16 ;
    @static_test { expect_type=""                         } true ? null: i32 ;
    @static_test { expect_type=""                         } true ? null: i64 ;
    @static_test { expect_type=""                         } true ? null: f32 ;
    @static_test { expect_type=""                         } true ? null: f64 ;
    @static_test { expect_type="cls1"                     } true ? null: c1  ;
    @static_test { expect_type="cls2"                     } true ? null: c2  ;
//    @static_test { expect_type=""                         } true ? null: f1  ;
//    @static_test { expect_type=""                         } true ? null: f2  ;
//    @static_test { expect_type=""                         } true ? null: f3  ;

    @static_test { expect_type=""                         } true ? e   : null;
    @static_test { expect_type="en1"                      } true ? e   : e   ;
    @static_test { expect_type="int32"                    } true ? e   : b   ;
    @static_test { expect_type="int32"                    } true ? e   : i8  ;
    @static_test { expect_type="int32"                    } true ? e   : i16 ;
    @static_test { expect_type="int32"                    } true ? e   : i32 ;
    @static_test { expect_type="int64"                    } true ? e   : i64 ;
    @static_test { expect_type="float32"                  } true ? e   : f32 ;
    @static_test { expect_type="float64"                  } true ? e   : f64 ;
    @static_test { expect_type=""                         } true ? e   : c1  ;
    @static_test { expect_type=""                         } true ? e   : c2  ;
//    @static_test { expect_type=""                         } true ? e   : f1  ;
//    @static_test { expect_type=""                         } true ? e   : f2  ;
//    @static_test { expect_type=""                         } true ? e   : f3  ;

    @static_test { expect_type=""                         } true ? b   : null;
    @static_test { expect_type="int32"                    } true ? b   : e   ;
    @static_test { expect_type="bool"                     } true ? b   : b   ;
    @static_test { expect_type="int32"                    } true ? b   : i8  ;
    @static_test { expect_type="int32"                    } true ? b   : i16 ;
    @static_test { expect_type="int32"                    } true ? b   : i32 ;
    @static_test { expect_type="int64"                    } true ? b   : i64 ;
    @static_test { expect_type="float32"                  } true ? b   : f32 ;
    @static_test { expect_type="float64"                  } true ? b   : f64 ;
    @static_test { expect_type=""                         } true ? b   : c1  ;
    @static_test { expect_type=""                         } true ? b   : c2  ;
//    @static_test { expect_type=""                         } true ? b   : f1  ;
//    @static_test { expect_type=""                         } true ? b   : f2  ;
//    @static_test { expect_type=""                         } true ? b   : f3  ;

    @static_test { expect_type=""                         } true ? i8  : null;
    @static_test { expect_type="int32"                    } true ? i8  : e   ;
    @static_test { expect_type="int32"                    } true ? i8  : b   ;
    @static_test { expect_type="int8"                     } true ? i8  : i8  ;
    @static_test { expect_type="int32"                    } true ? i8  : i16 ;
    @static_test { expect_type="int32"                    } true ? i8  : i32 ;
    @static_test { expect_type="int64"                    } true ? i8  : i64 ;
    @static_test { expect_type="float32"                  } true ? i8  : f32 ;
    @static_test { expect_type="float64"                  } true ? i8  : f64 ;
    @static_test { expect_type=""                         } true ? i8  : c1  ;
    @static_test { expect_type=""                         } true ? i8  : c2  ;
//    @static_test { expect_type=""                         } true ? i8  : f1  ;
//    @static_test { expect_type=""                         } true ? i8  : f2  ;
//    @static_test { expect_type=""                         } true ? i8  : f3  ;

    @static_test { expect_type=""                         } true ? i16 : null;
    @static_test { expect_type="int32"                    } true ? i16 : e   ;
    @static_test { expect_type="int32"                    } true ? i16 : b   ;
    @static_test { expect_type="int32"                    } true ? i16 : i8  ;
    @static_test { expect_type="int16"                    } true ? i16 : i16 ;
    @static_test { expect_type="int32"                    } true ? i16 : i32 ;
    @static_test { expect_type="int64"                    } true ? i16 : i64 ;
    @static_test { expect_type="float32"                  } true ? i16 : f32 ;
    @static_test { expect_type="float64"                  } true ? i16 : f64 ;
    @static_test { expect_type=""                         } true ? i16 : c1  ;
    @static_test { expect_type=""                         } true ? i16 : c2  ;
//    @static_test { expect_type=""                         } true ? i16 : f1  ;
//    @static_test { expect_type=""                         } true ? i16 : f2  ;
//    @static_test { expect_type=""                         } true ? i16 : f3  ;

    @static_test { expect_type=""                         } true ? i32 : null;
    @static_test { expect_type="int32"                    } true ? i32 : e   ;
    @static_test { expect_type="int32"                    } true ? i32 : b   ;
    @static_test { expect_type="int32"                    } true ? i32 : i8  ;
    @static_test { expect_type="int32"                    } true ? i32 : i16 ;
    @static_test { expect_type="int32"                    } true ? i32 : i32 ;
    @static_test { expect_type="int64"                    } true ? i32 : i64 ;
    @static_test { expect_type="float32"                  } true ? i32 : f32 ;
    @static_test { expect_type="float64"                  } true ? i32 : f64 ;
    @static_test { expect_type=""                         } true ? i32 : c1  ;
    @static_test { expect_type=""                         } true ? i32 : c2  ;
//    @static_test { expect_type=""                         } true ? i32 : f1  ;
//    @static_test { expect_type=""                         } true ? i32 : f2  ;
//    @static_test { expect_type=""                         } true ? i32 : f3  ;

    @static_test { expect_type=""                         } true ? i64 : null;
    @static_test { expect_type="int64"                    } true ? i64 : e   ;
    @static_test { expect_type="int64"                    } true ? i64 : b   ;
    @static_test { expect_type="int64"                    } true ? i64 : i8  ;
    @static_test { expect_type="int64"                    } true ? i64 : i16 ;
    @static_test { expect_type="int64"                    } true ? i64 : i32 ;
    @static_test { expect_type="int64"                    } true ? i64 : i64 ;
    @static_test { expect_type="float32"                  } true ? i64 : f32 ;
    @static_test { expect_type="float64"                  } true ? i64 : f64 ;
    @static_test { expect_type=""                         } true ? i64 : c1  ;
    @static_test { expect_type=""                         } true ? i64 : c2  ;
//    @static_test { expect_type=""                         } true ? i64 : f1  ;
//    @static_test { expect_type=""                         } true ? i64 : f2  ;
//    @static_test { expect_type=""                         } true ? i64 : f3  ;

    @static_test { expect_type=""                         } true ? f32 : null;
    @static_test { expect_type="float32"                  } true ? f32 : e   ;
    @static_test { expect_type="float32"                  } true ? f32 : b   ;
    @static_test { expect_type="float32"                  } true ? f32 : i8  ;
    @static_test { expect_type="float32"                  } true ? f32 : i16 ;
    @static_test { expect_type="float32"                  } true ? f32 : i32 ;
    @static_test { expect_type="float32"                  } true ? f32 : i64 ;
    @static_test { expect_type="float32"                  } true ? f32 : f32 ;
    @static_test { expect_type="float64"                  } true ? f32 : f64 ;
    @static_test { expect_type=""                         } true ? f32 : c1  ;
    @static_test { expect_type=""                         } true ? f32 : c2  ;
//    @static_test { expect_type=""                         } true ? f32 : f1  ;
//    @static_test { expect_type=""                         } true ? f32 : f2  ;
//    @static_test { expect_type=""                         } true ? f32 : f3  ;

    @static_test { expect_type=""                         } true ? f64 : null;
    @static_test { expect_type="float64"                  } true ? f64 : e   ;
    @static_test { expect_type="float64"                  } true ? f64 : b   ;
    @static_test { expect_type="float64"                  } true ? f64 : i8  ;
    @static_test { expect_type="float64"                  } true ? f64 : i16 ;
    @static_test { expect_type="float64"                  } true ? f64 : i32 ;
    @static_test { expect_type="float64"                  } true ? f64 : i64 ;
    @static_test { expect_type="float64"                  } true ? f64 : f32 ;
    @static_test { expect_type="float64"                  } true ? f64 : f64 ;
    @static_test { expect_type=""                         } true ? f64 : c1  ;
    @static_test { expect_type=""                         } true ? f64 : c2  ;
//    @static_test { expect_type=""                         } true ? f64 : f1  ;
//    @static_test { expect_type=""                         } true ? f64 : f2  ;
//    @static_test { expect_type=""                         } true ? f64 : f3  ;

    @static_test { expect_type="cls1"                     } true ? c1  : null;
    @static_test { expect_type=""                         } true ? c1  : e   ;
    @static_test { expect_type=""                         } true ? c1  : b   ;
    @static_test { expect_type=""                         } true ? c1  : i8  ;
    @static_test { expect_type=""                         } true ? c1  : i16 ;
    @static_test { expect_type=""                         } true ? c1  : i32 ;
    @static_test { expect_type=""                         } true ? c1  : i64 ;
    @static_test { expect_type=""                         } true ? c1  : f32 ;
    @static_test { expect_type=""                         } true ? c1  : f64 ;
    @static_test { expect_type="cls1"                     } true ? c1  : c1  ;
    @static_test { expect_type=""                         } true ? c1  : c2  ;
//    @static_test { expect_type=""                         } true ? c1  : f1  ;
//    @static_test { expect_type=""                         } true ? c1  : f2  ;
//    @static_test { expect_type=""                         } true ? c1  : f3  ;

    @static_test { expect_type="cls2"                     } true ? c2  : null;
    @static_test { expect_type=""                         } true ? c2  : e   ;
    @static_test { expect_type=""                         } true ? c2  : b   ;
    @static_test { expect_type=""                         } true ? c2  : i8  ;
    @static_test { expect_type=""                         } true ? c2  : i16 ;
    @static_test { expect_type=""                         } true ? c2  : i32 ;
    @static_test { expect_type=""                         } true ? c2  : i64 ;
    @static_test { expect_type=""                         } true ? c2  : f32 ;
    @static_test { expect_type=""                         } true ? c2  : f64 ;
    @static_test { expect_type=""                         } true ? c2  : c1  ;
    @static_test { expect_type="cls2"                     } true ? c2  : c2  ;
//    @static_test { expect_type=""                         } true ? c2  : f1  ;
//    @static_test { expect_type=""                         } true ? c2  : f2  ;
//    @static_test { expect_type=""                         } true ? c2  : f3  ;

//    @static_test { expect_type=""                         } true ? f1  : null;
//    @static_test { expect_type=""                         } true ? f1  : e   ;
//    @static_test { expect_type=""                         } true ? f1  : b   ;
//    @static_test { expect_type=""                         } true ? f1  : i8  ;
//    @static_test { expect_type=""                         } true ? f1  : i16 ;
//    @static_test { expect_type=""                         } true ? f1  : i32 ;
//    @static_test { expect_type=""                         } true ? f1  : i64 ;
//    @static_test { expect_type=""                         } true ? f1  : f32 ;
//    @static_test { expect_type=""                         } true ? f1  : f64 ;
//    @static_test { expect_type=""                         } true ? f1  : c1  ;
//    @static_test { expect_type=""                         } true ? f1  : c2  ;
//    @static_test { expect_type="function():void"          } true ? f1  : f1  ;
//    @static_test { expect_type=""                         } true ? f1  : f2  ;
//    @static_test { expect_type=""                         } true ? f1  : f3  ;
//
//    @static_test { expect_type=""                         } true ? f2  : null;
//    @static_test { expect_type=""                         } true ? f2  : e   ;
//    @static_test { expect_type=""                         } true ? f2  : b   ;
//    @static_test { expect_type=""                         } true ? f2  : i8  ;
//    @static_test { expect_type=""                         } true ? f2  : i16 ;
//    @static_test { expect_type=""                         } true ? f2  : i32 ;
//    @static_test { expect_type=""                         } true ? f2  : i64 ;
//    @static_test { expect_type=""                         } true ? f2  : f32 ;
//    @static_test { expect_type=""                         } true ? f2  : f64 ;
//    @static_test { expect_type=""                         } true ? f2  : c1  ;
//    @static_test { expect_type=""                         } true ? f2  : c2  ;
//    @static_test { expect_type=""                         } true ? f2  : f1  ;
//    @static_test { expect_type="function(int8):void"      } true ? f2  : f2  ;
//    @static_test { expect_type=""                         } true ? f2  : f3  ;
//
//    @static_test { expect_type=""                         } true ? f3  : null;
//    @static_test { expect_type=""                         } true ? f3  : e   ;
//    @static_test { expect_type=""                         } true ? f3  : b   ;
//    @static_test { expect_type=""                         } true ? f3  : i8  ;
//    @static_test { expect_type=""                         } true ? f3  : i16 ;
//    @static_test { expect_type=""                         } true ? f3  : i32 ;
//    @static_test { expect_type=""                         } true ? f3  : i64 ;
//    @static_test { expect_type=""                         } true ? f3  : f32 ;
//    @static_test { expect_type=""                         } true ? f3  : f64 ;
//    @static_test { expect_type=""                         } true ? f3  : c1  ;
//    @static_test { expect_type=""                         } true ? f3  : c2  ;
//    @static_test { expect_type=""                         } true ? f3  : f1  ;
//    @static_test { expect_type=""                         } true ? f3  : f2  ;
//    @static_test { expect_type="function(int8,int32):void"} true ? f3  : f3  ;
}

