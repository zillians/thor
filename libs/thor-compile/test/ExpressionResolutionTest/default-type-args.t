/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

@static_test { resolution="foo()"                        } function foo():                         void { }
@static_test { resolution="foo<X>()"                     } function foo<X>():                      void { }

@static_test { resolution="foo1(int32)"                  } function foo1(x:int32):                 void { }
@static_test { resolution="foo1<X>(X)"                   } function foo1<X>(x:X):                  void { }

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo2():void"  } } }
@static_test { resolution="foo2()"                       } function foo2():                        void { }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo2<X>():void"  } } }
@static_test { resolution="foo2<X=int32>()"              } function foo2<X=int32>():               void { }

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo3<X>():void"  } } }
@static_test { resolution="foo3<X>()"                    } function foo3<X>():                     void { }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo3<X,Y>():void"  } } }
@static_test { resolution="foo3<X, Y=int32>()"           } function foo3<X, Y=int32>():            void { }

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo4<X>(X):void"  } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo4<X>(X):void"  } } }
@static_test { resolution="foo4<X>(x:X)"                 } function foo4<X>(x:X):                  void { }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo4<X,Y>(X):void"  } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo4<X,Y>(X):void"  } } }
@static_test { resolution="foo4<X,Y=int32>(x:X)"         } function foo4<X,Y=int32>(x: X):         void { }

@static_test { resolution="foo5<W,X,Y,Z=int32>(w:W,x:X)" }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo5<W,X,Y,Z>(W,X):void"  } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function foo5<W,X,Y,Z>(W,X):void"  } } }
function foo5<W,X,Y,Z=int32>(w:W, x:X): void { }


function test(): void
{
    // no parameter to deduct from
    @static_test { expect_resolution="foo()"       } foo();
    @static_test { expect_resolution="foo<X>()"    } foo<int32>();

    // one parameter to deduct from
    @static_test { expect_resolution="foo1(int32)" } foo1(1);
    @static_test { expect_resolution="foo1<X>(X)" }  foo1<int32>(1);

    // no parameter to deduct from, with default value
    @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="foo2()"} } }
    @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="foo2"  } } }
    foo2();

    @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="foo3<_:int32>()"} } }
    @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="foo3<_:int32>"  } } }
    foo3<int32>();
  
    // one paramter to deduct from
    @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="foo4<_:int32>(int32)"} } }
    @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="foo4<_:int32>"       } } }
    foo4<int32>(1);

    @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="foo4(int32)"} } }
    @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="foo4"       } } }
    foo4(1);

    // only first two args can be deducted from parameters
    @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="foo5(int32, float64)"} } }
    @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="foo5"                } } }
    foo5(1, 2.0);

    @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="foo5<_:int32,_:float64>(int32, float64)"} } }
    @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="foo5<_:int32,_:float64>"                } } }
    foo5<int32,float64>(1, 2.0);

    @static_test { expect_resolution="foo5<W,X,Y,Z=int32>(w:W,x:X)" } foo5<int32,float64,int8>(1, 2.0);
}
