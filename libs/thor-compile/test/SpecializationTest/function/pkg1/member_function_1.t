/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= dummy;

class cls_1
{
    // generic versions
    @static_test { specialization_of = "" } function func_1_0<T>(                        ): void {}
    @static_test { specialization_of = "" } function func_1_1<T>(v:       T              ): void {}
    @static_test { specialization_of = "" } function func_1_1<U>(v: Dummy<U>             ): void {}
    @static_test { specialization_of = "" } function func_1_2<T>(v:       T , w: int8    ): void {}
    @static_test { specialization_of = "" } function func_1_2<U>(v:       U , w: int32   ): void {}
    @static_test { specialization_of = "" } function func_1_2<V>(v: int32   , w: Dummy<V>): void {}

    // specializations
    @static_test { specialization_of = "func_1_0<T>" } function func_1_0<T:             int8         >(): void {}
    @static_test { specialization_of = "func_1_0<T>" } function func_1_0<U:             int32        >(): void {}
    @static_test { specialization_of = "func_1_0<T>" } function func_1_0<W:       Dummy<int8 >       >(): void {}
    @static_test { specialization_of = "func_1_0<T>" } function func_1_0<X: Dummy<Dummy<int32> >     >(): void {}
    @static_test { specialization_of = "func_1_0<T>" } function func_1_0<Y:    function(     ): void >(): void {}
    @static_test { specialization_of = "func_1_0<T>" } function func_1_0<Z:    function(int8 ): int32>(): void {}

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION_PREV", parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<T:             int8         >(v:             int8         ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION"     , parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = ""            } function func_1_1<T:             int8         >(v:             T            ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION_PREV", parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<U:             int32        >(v:             int32        ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION"     , parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = ""            } function func_1_1<U:             int32        >(v:             U            ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION_PREV", parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<W:       Dummy<int8 >       >(v:       Dummy<int8 >       ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION"     , parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<W:       Dummy<int8 >       >(v:       W                  ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION_PREV", parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<X: Dummy<Dummy<int32> >     >(v: Dummy<Dummy<int32> >     ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION"     , parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<X: Dummy<Dummy<int32> >     >(v: X                        ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION_PREV", parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<Y:    function(     ): void >(v:    function(     ): void ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION"     , parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<Y:    function(     ): void >(v:    Y                     ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION_PREV", parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<Z:    function(int8 ): int32>(v:    function(int8 ): int32): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_SPECIALIZATION"     , parameters = { generic_decl_name = "function func_1_1<T>(T):void" } } }
    @static_test { specialization_of = "func_1_1<T>" } function func_1_1<Z:    function(int8 ): int32>(v:    Z                     ): void {}

    @static_test { specialization_of = "func_1_2<T>" } function func_1_2<T:int32>(x:T    , y:      int8  ): void {}
    @static_test { specialization_of = "func_1_2<U>" } function func_1_2<T:int32>(x:T    , y:      int32 ): void {}
    @static_test { specialization_of = "func_1_2<V>" } function func_1_2<T:int32>(x:int32, y:Dummy<int32>): void {}
}
