/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= dummy;

// generic versions
@static_test { specialization_of = "" } function func_0_0  (                                   ): void {}
@static_test { specialization_of = "" } function func_0_0_d(                                   ): void {}
@static_test { specialization_of = "" } function func_0_1  (v: int8                            ): void {}
@static_test { specialization_of = "" } function func_0_1_d(v: Dummy<int32>                    ): void {}
@static_test { specialization_of = "" } function func_0_2  (v: function(): void, w: int32      ): void {}
@static_test { specialization_of = "" } function func_0_2_d(v: int32           , w: Dummy<int8>): void {}

// specializations
@static_test { expect_message = { level = "LEVEL_ERROR", id = "NO_GENERIC_TEMPLATE" } }
@static_test { specialization_of = "" } function func_0_0_d<T: int8           >(                                   ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "NO_GENERIC_TEMPLATE" } }
@static_test { specialization_of = "" } function func_0_0_d<T: int8 , U: int32>(                                   ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "NO_GENERIC_TEMPLATE" } }
@static_test { specialization_of = "" } function func_0_1_d<T: int8           >(v: Dummy<int32>                    ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "NO_GENERIC_TEMPLATE" } }
@static_test { specialization_of = "" } function func_0_2_d<T: int8 , U: int32>(v: int32                           ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "NO_GENERIC_TEMPLATE" } }
@static_test { specialization_of = "" } function func_0_1_d<T: int8           >(v: Dummy<int32>    , w: Dummy<int8>): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "NO_GENERIC_TEMPLATE" } }
@static_test { specialization_of = "" } function func_0_2_d<T: int8 , U: int32>(v: int32           , w: Dummy<int8>): void {}
