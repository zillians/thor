/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= dummy;
import .= pkg0;

// generic versions
@static_test { specialization_of = "" } class cls_2  <T, U> {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_2_d<T, U> {}

// partially specialized versions of "cls_2"
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T:             int8         , U> {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U:             int32        , V> {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V:             cls_0        , W> {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W:       Dummy<int8 >       , X> {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X: Dummy<Dummy<int32> >     , Y> {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y:    function(     ): void , Z> {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z:    function(int8 ): int32, T> {}

@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T, U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U, V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V, W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W, X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X, Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y, Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z, T:    function(int8 ): int32> {}

// fully specialized versions of "cls_2"
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T:             int8         , U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U:             int8         , V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V:             int8         , W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W:             int8         , X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X:             int8         , Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y:             int8         , Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z:             int8         , T:    function(int8 ): int32> {}

@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T:             int32        , U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U:             int32        , V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V:             int32        , W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W:             int32        , X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X:             int32        , Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y:             int32        , Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z:             int32        , T:    function(int8 ): int32> {}

@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T:       Dummy<int8 >       , U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U:       Dummy<int8 >       , V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V:       Dummy<int8 >       , W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W:       Dummy<int8 >       , X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X:       Dummy<int8 >       , Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y:       Dummy<int8 >       , Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z:       Dummy<int8 >       , T:    function(int8 ): int32> {}

@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T: Dummy<Dummy<int8 > >     , U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U: Dummy<Dummy<int8 > >     , V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V: Dummy<Dummy<int8 > >     , W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W: Dummy<Dummy<int8 > >     , X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X: Dummy<Dummy<int8 > >     , Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y: Dummy<Dummy<int8 > >     , Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z: Dummy<Dummy<int8 > >     , T:    function(int8 ): int32> {}

@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T:    function(     ): void , U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U:    function(     ): void , V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V:    function(     ): void , W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W:    function(     ): void , X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X:    function(     ): void , Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y:    function(     ): void , Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z:    function(     ): void , T:    function(int8 ): int32> {}

@static_test { specialization_of = "cls_2<T,U>" } class cls_2<T:    function(int8 ): int32, U:             int8         > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<U:    function(int8 ): int32, V:             int32        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<V:    function(int8 ): int32, W:             cls_0        > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<W:    function(int8 ): int32, X:       Dummy<int8 >       > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<X:    function(int8 ): int32, Y: Dummy<Dummy<int32> >     > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Y:    function(int8 ): int32, Z:    function(     ): void > {}
@static_test { specialization_of = "cls_2<T,U>" } class cls_2<Z:    function(int8 ): int32, T:    function(int8 ): int32> {}

// incorrect specialized versions of "cls_2_d"
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_2_d<T: int8           > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_2_d<T: int8 , U       > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_2_d<T       , U: int32> {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_TEMPLATE_CLASS_DECLARATIONS" } }
@static_test { specialization_of = "" } class cls_2_d<T: int8 , U: int32> {}
