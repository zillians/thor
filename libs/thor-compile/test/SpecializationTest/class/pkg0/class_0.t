/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= dummy;

// generic versions
@static_test { specialization_of = "" } class cls_0         {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME_PREV" } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME_PREV" } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME_PREV" } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME_PREV" } }
@static_test { specialization_of = "" } class cls_0_d       {}

// specialize non-template "cls_0_d"
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME", parameters = { id = "cls_0_d" } } }
@static_test { specialization_of = "" } class cls_0_d<T: int8           > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME", parameters = { id = "cls_0_d" } } }
@static_test { specialization_of = "" } class cls_0_d<T: int8 , U       > {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME", parameters = { id = "cls_0_d" } } }
@static_test { specialization_of = "" } class cls_0_d<T       , U: int32> {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "DUPE_NAME", parameters = { id = "cls_0_d" } } }
@static_test { specialization_of = "" } class cls_0_d<T: int8 , U: int32> {}

