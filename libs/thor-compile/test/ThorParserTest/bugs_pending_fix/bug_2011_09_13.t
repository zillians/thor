/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_function():int32
{
    var x:int64 = 32;
    var y:int64 = 42;
    var a:int64;
    var b:int64;
    var c:int64;

    if (x > y)
    {
        a = y;
        a = a / 2;
    }
    else
    {
        // should enter here
        a = x;
        a = a - 2;
    }

    if (x > y + 100)
    {
        b = 1000;
        b = a - 100;
    }
    elif (x == y - 10)
    {
        // should enter here
        b = y - x;
        b = b * 2;
    }
    else
    {
        b = 100;
        b = 3;
    }

    if (x < y)
    {
        // should enter here
        c = 3;
    }
    elif (x > y)
    {
        c = 333;
    }
    else
    {
        c = 0;
    }
}
