/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class my_class
{
    // specify visibility only
    public var a:int32;
    private var b:int32;
    protected var c:int32;
    public function f1():void {}
    private function f2():void {}
    protected function f3():void {}

    // specify static-ness only
    static var d:int32;
    static var e:int32;
    static var f:int32;
    static function f4():void {}

    // specify both visibility and static-ness
    public static var g:int32;
    private static var h:int32;
    protected static var i:int32;
    public static function f5():void {}
    private static function f6():void {}
    protected static function f7():void {}
}
