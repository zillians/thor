#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

SET(sources
    "${CMAKE_CURRENT_SOURCE_DIR}/0_datatype.t"
    "${CMAKE_CURRENT_SOURCE_DIR}/1_name.t"
)

thor_compile_add_parser_test(terminal       0 ${sources})
thor_compile_add_parser_test(ast-terminal   1 ${sources})
thor_compile_add_parser_test(xform-terminal 2 ${sources})
thor_compile_add_parser_test(auto-terminal  3 ${sources})

UNSET(sources)
