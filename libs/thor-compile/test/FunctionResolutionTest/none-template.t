/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
//////////////////////////////////////////////////////////////////////////////
// some classes
//////////////////////////////////////////////////////////////////////////////

    class Foo {}
    class Bar {}
    class Base {}
    class Derived extends Base {}
    class Extended extends Derived {}

//////////////////////////////////////////////////////////////////////////////
// Function Declarations
//////////////////////////////////////////////////////////////////////////////

    // f()
    @static_test { resolution="f()"                               } function f() : void {}
    @static_test { resolution="f(int32)"                          } function f(a:int32) : void {}
    @static_test { resolution="f(int32,int32)"                    } function f(a:int32, b:int32) : void {}
    @static_test { resolution="f(int32,int32,int32)"              } function f(a:int32, b:int32, c:int32) : void {}

    // h()
    @static_test { resolution="h(int32)"                          } function h(a:int32) : void {}
    @static_test { resolution="h(Foo)"                            } function h(a:Foo) : void {}
    @static_test { resolution="h(int32, int32)"                   } function h(a:int32, b:int32) : void {}

    // i()
    @static_test { resolution="i(int16)"                          } function i(a:int16) : void {}
    @static_test { resolution="i(int32)"                          } function i(a:int32) : void {}
    @static_test { resolution="i(int64)"                          } function i(a:int64) : void {}
    @static_test { resolution="i(float32)"                        } function i(a:float32) : void {}
    @static_test { resolution="i(Foo)"                            } function i(a:Foo) : void {}
    @static_test { resolution="i(int32, int32)"                   } function i(a:int32, b:int32) : void {}

    // j()
    @static_test { resolution="j(int32, int64)"                   }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function j(int32,int64):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function j(int32,int64):void" } } }
    function j(a:int32, b:int64) : void {}
    @static_test { resolution="j(int64, int32)"                   }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function j(int64,int32):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function j(int64,int32):void" } } }
    function j(a:int64, b:int32) : void {}

    // k()
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function k(int32):void" } } }
    @static_test { resolution="k(int32)"                          } function k(a:int32) : void {}
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function k(float32):void" } } }
    @static_test { resolution="k(float32)"                        } function k(a:float32) : void {}

    // z() for function argument mismatch
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z(int32):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z(int32):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z(int32):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z(int32):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z(int32):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z(int32):void" } } }
    function z(a:int32) : void {}

//////////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////////

    function main() : void
    {
        var vint8      : int8              ;
        var vint16     : int16             ;
        var vint32     : int32             ;
        var vint64     : int64             ;
        var vfloat32   : float32           ;
        var vfloat64   : float64           ;
        var vFoo       : Foo               ;
        var vBar       : Bar               ;
        var vBase      : Base              ;
        var vDerived   : Derived           ;
        var vExtended  : Extended          ;

        //// no match
        @static_test { expect_resolution="", expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="no_such_function"}} }
        @static_test { expect_resolution="" } no_such_function();

        // exactly match
        @static_test { expect_resolution="f()"                  } f();
        @static_test { expect_resolution="f(int32)"             } f(vint32);
        @static_test { expect_resolution="f(int32,int32)"       } f(vint32, vint32);
        @static_test { expect_resolution="f(int32,int32,int32)" } f(vint32, vint32, vint32);

        // promotion
        @static_test { expect_resolution="i(int32)" } i(vint8);

        // standard conversion
        @static_test { expect_resolution="h(int32)" } h(vint64);
        @static_test { expect_resolution="h(int32)" } h(vfloat32);
        @static_test { expect_resolution="h(int32)" } h(vfloat64);

        // ambi
        @static_test { expect_resolution="", expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="j"}} }
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="j(int64, int64)"} } }
        j(vint64, vint64);
        @static_test { expect_resolution="", expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="j"}} }
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="j(int32, int32)"} } }
        j(vint32, vint32);

        // ambiguous
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="k(int64)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="k"} } }
        k(vint64);

        // argument mismatch
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z()"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z"} } } z();
                                                                                                                 z(vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z(int32, int32)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z"} } } z(vint32, vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z(float32, float32)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z"} } } z(vfloat32, vfloat32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z(class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z"} } } z(vFoo);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z(class Foo, class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z"} } } z(vFoo, vFoo);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z(class Foo, class Foo, class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z"} } } z(vFoo, vFoo, vFoo);

    }
