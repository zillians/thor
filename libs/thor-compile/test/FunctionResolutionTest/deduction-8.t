/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class     Base  {}
interface Iface {}

class DerivedA    extends Base                             {}
class DerivedB<T> extends Base                             {}
class DerivedC<T> extends DerivedA                         {}
class DerivedD    extends DerivedB<int32>                  {}
class DerivedE                            implements Iface {}
class DerivedF<T>                         implements Iface {}
class DerivedG<T> extends DerivedE                         {}
class DerivedH    extends DerivedF<int8 >                  {}

class AmbiguousA  extends Base            implements Iface {}
class AmbiguousB  extends DerivedA        implements Iface {}
class AmbiguousC  extends DerivedB<int32> implements Iface {}
class AmbiguousD  extends DerivedC<int64> implements Iface {}
class AmbiguousE  extends DerivedD        implements Iface {}
//class AmbiguousF  extends DerivedE        implements Iface {} // enable this if #1141 is fixed
//class AmbiguousG  extends DerivedF<int8 > implements Iface {} // enable this if #1141 is fixed

@static_test { resolution = "f(Base)"  } function f(v: Base ): void {}
@static_test { resolution = "f(Iface)" } function f(v: Iface): void {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
//@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
//@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Base):void" } } }
@static_test { resolution = "g(Base)"  } function g(v: Base ): void {}
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
//@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
//@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(Iface):void" } } }
@static_test { resolution = "g(Iface)" } function g(v: Iface): void {}

function test(): void
{
    var da   : DerivedA       ;
    var dbI32: DerivedB<int32>;
    var dcI16: DerivedC<int16>;
    var dd   : DerivedD       ;
    var de   : DerivedE       ;
    var dfI64: DerivedF<int64>;
    var dgI8 : DerivedG<int8 >;
    var dh   : DerivedH       ;

    var ambiA: AmbiguousA     ;
    var ambiB: AmbiguousB     ;
    var ambiC: AmbiguousC     ;
    var ambiD: AmbiguousD     ;
    var ambiE: AmbiguousE     ;
//    var ambiF: AmbiguousF     ;
//    var ambiG: AmbiguousG     ;

    @static_test { expect_resolution = "f(Base)"  } f(da   );
    @static_test { expect_resolution = "f(Base)"  } f(dbI32);

    @static_test { expect_resolution = "f(Base)"  } f(dcI16);
    @static_test { expect_resolution = "f(Base)"  } f(dd   );

    @static_test { expect_resolution = "f(Iface)" } f(de   );
    @static_test { expect_resolution = "f(Iface)" } f(dfI64);

    @static_test { expect_resolution = "f(Iface)" } f(dgI8 );
    @static_test { expect_resolution = "f(Iface)" } f(dh   );

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousA)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiA);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousB)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiB);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousC)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiC);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousD)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiD);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousE)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiE);
//    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousF)" } } }
//    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiF);
//    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(class AmbiguousG)" } } }
//    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"                   } } } g(ambiG);
}
