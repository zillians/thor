/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Dummy  <T   >                    {}

class Base   <T, U>                    {}
class Derived<T, U> extends Base<T, U> {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function f<T>(Base<_:Dummy<_:T>,_:Dummy<_:T>>):void" } } }
function f<T>(a:Base<Dummy<T>, Dummy<T> >) : void {}

function main() : void
{
    var vDDI32DI8: Derived<Dummy<int32>, Dummy<int8> >;

    @static_test { expect_resolution = "" }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "f(class Derived<T:Dummy<T:int32>,U:Dummy<T:int8>>)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "f" } } }
    f(vDDI32DI8);
}
