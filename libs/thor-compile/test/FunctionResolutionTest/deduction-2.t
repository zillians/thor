/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import dummy;

class DummyClass<T> {}

@static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function for_test<T>(dummy.DummyClass<_:T>):void" } } }
function for_test<T>(d: dummy.DummyClass<T>): void {}

function test(): int32 {
    var d: DummyClass<int32>;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "for_test(class DummyClass<T:int32>)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "for_test"                            } } }
    for_test(d);

    return 0;
}
