/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
//////////////////////////////////////////////////////////////////////////////
// some classes
//////////////////////////////////////////////////////////////////////////////

    class Foo {}
    class Bar {}
    class Base {}
    class Derived extends Base {}
    class Extended extends Derived {}
    class Complex<T> {}

//////////////////////////////////////////////////////////////////////////////
// 2 template functions
//////////////////////////////////////////////////////////////////////////////

    // general, general
    @static_test { resolution="f<T,U>(T,U)"                           } function f<T, U>(a:T, b:U) : void {}

    // general, non-template
    @static_test { resolution="f<T>(T,int16)"                         } function f<T>(a:T, b:int16) : void {}

    // special, special
    @static_test { resolution="f<int16,int16>(int16,int16)"           } function f<T:int16, U:int16>(a:T, b:U) : void {}

    // non-template, non-template
    @static_test { resolution="f(int16,int16)"                        } function f(a:int16, b:int16) : void {}

    // g
    @static_test { resolution="g<T>(T,T)"                             } function g<T>(a:T, b:T) : void {}
    @static_test { resolution="g<T,U>(T,U)"                           } function g<T,U>(a:T, b:U) : void {}

    @static_test { resolution="g<int16>(T,T)"                         } function g<T:int16>(a:T, b:T) : void {}
    @static_test { resolution="g<int16,int16>(T,U)"                   } function g<T:int16, U:int16>(a:T, b:U) : void {}

    // h
    @static_test { resolution="h<T>(T,T)" }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function h<T>(T,T):void" } } }
    function h<T>(a:T, b:T) : void {}

    // r
    @static_test { resolution="r(int32,int8)"                         } function r<T>(a:int32, b:int8) : void {}
    @static_test { resolution="r<T>(T,int32)"                         } function r<T>(a:T, b:int32) : void {}
    @static_test { resolution="r<T:int32>(T,int32)"                   } function r<T:int32>(a:T, b:int32) : void {}

    // z
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T1,T2>(T1,T2):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T1,T2>(T1,T2):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T1,T2>(T1,T2):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T1,T2>(T1,T2):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T1,T2>(T1,T2):void" } } }
    @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE_CANDIDATES", parameters={ candidate="function z<T1,T2>(T1,T2):void" } } }
    function z<T1, T2>(a:T1, b:T2) : void {}

//////////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////////

    function main() : void
    {
        var vint8        : int8              ;
        var vint16       : int16             ;
        var vint32       : int32             ;
        var vint64       : int64             ;
        var vfloat32     : float32           ;
        var vfloat64     : float64           ;
        var vFoo         : Foo               ;
        var vBar         : Bar               ;
        var vBase        : Base              ;
        var vDerived     : Derived           ;
        var vExtended    : Extended          ;
        var vComplex     : Complex<int32>    ;
        var vComplexBase : Complex<Base>     ;

        // full match after deduction
        @static_test { expect_resolution="f<T,U>(T,U)" } f(vint16, vint64);
        @static_test { expect_resolution="f<T,U>(T,U)" } f(vfloat64, vFoo);
        @static_test { expect_resolution="f<T,U>(T,U)" } f(vBar, vBase);

        // special is better than general
        @static_test { expect_resolution="f<T>(T,int16)" } f(vBar, vint16);

        // lower degree of freedom is better than higher degree of freedom while full match specialization
        @static_test { expect_resolution="g<int16>(T,T)" } g(vint16, vint16);

        // null literal could be accepted by template deduction by any class
        @static_test { expect_resolution="g<T>(T,T)" } g(vComplex, null);
        @static_test { expect_resolution="g<T>(T,T)" } g(vFoo, null);
        @static_test { expect_resolution="g<T>(T,T)" } g(vDerived, null);
        @static_test { expect_resolution="g<T>(T,T)" } g(null, vFoo);
        @static_test { expect_resolution="g<T>(T,T)" } g(null, vDerived);
        @static_test { expect_resolution="g<T>(T,T)" } g(null, vComplex);

        // null literal could be accepted by nested template deduction by any class
        @static_test { expect_resolution="g<T>(T,T)" } g(vComplexBase, null);
        @static_test { expect_resolution="g<T>(T,T)" } g(null, vComplexBase);

        // one type explicit specified, one deduced by param
        @static_test { expect_resolution="g<T,U>(T,U)" } g<int32>(vint32, vint64);

        // non-template is better then special
        @static_test { expect_resolution="f(int16,int16)" } f(vint16, vint16);

        // can not deduced
        @static_test { expect_resolution="" }
        @static_test { expect_message={level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="h(int32, int64)"} } }
        @static_test { expect_message={level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="h"} } }
        h(vint32, vint64);

        // fully qualified template instantiation
        @static_test { expect_resolution="f<T,U>(T,U)" } f<int64, int8>(vint32, vint64);
        @static_test { expect_resolution="f<int16,int16>(int16,int16)" } f<int16, int16>(vint32, vint64);

        // conversion rank is most important
        @static_test { expect_resolution="r<T:int32>(T,int32)" } r(vint32, vint16);

        // argument mismatch
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32,_:int32>()"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32,_:int32>"} } } z<int32, int32>();
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32,_:int32>(int32)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32,_:int32>"} } } z<int32, int32>(vint32);
                                                                                                                                  z<int32, int32>(vint32, vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32,_:int32>(int32, float32, int32)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32,_:int32>"} } } z<int32, int32>(vint32, vfloat32, vint32);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32,_:int32>(class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32,_:int32>"} } } z<int32, int32>(vFoo);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32,_:int32>(class Foo, class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32,_:int32>"} } } z<int32, int32>(vFoo, vFoo);
        @static_test { expect_message={ level="LEVEL_ERROR", id="FUNCTION_CANT_RESOLVE", parameters={id="z<_:int32,_:int32>(class Foo, class Foo, class Foo)"} } }
        @static_test { expect_message={ level="LEVEL_ERROR", id="UNDEFINED_SYMBOL_INFO", parameters={id="z<_:int32,_:int32>"} } } z<int32, int32>(vFoo, vFoo, vFoo);

    }
