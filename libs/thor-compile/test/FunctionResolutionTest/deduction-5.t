/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class X<T>
{
}

class X<T:int32>
{
}

class X<T:int64>
{
}

class X<T:X<int32> >
{
}

class X<T:X<int64> >
{
}

@static_test { resolution="complex_f_1" }
function complex_f<A,B,C,D>(a:A, b:B, c:C, d:D):void { }

@static_test { resolution="complex_f_2" }
function complex_f<T>(a:X<T>, b:T, c:T, d:T):void { }

@static_test { resolution="complex_f_3" }
function complex_f<T, U>(a:X<T>, b:T, c:X<U>, d:U):void { }
 
@static_test { resolution="complex_f_4" }
function complex_f<T, U>(a:T, b:X<T>, c:U, d:X<U>):void { }

@static_test { resolution="complex_f_5" }
function complex_f<T, U>(a:X<X<T> >, b:T, c:X<X<U> >, d:U):void { }

@static_test { resolution="complex_f_6" }
function complex_f<U>(a:X<X<int64> >, b:int64, c:X<X<U> >, d:U):void { }

function ForTest() : void
{
	var i:int32;
	var j:int64;
	var k:X<int32>;
	var l:X<int64>;
	var m:X<X<int32> >;
	var n:X<X<int64> >;
	
	@static_test { expect_resolution="complex_f_1" }
	complex_f(i, j, k, l);
	
	@static_test { expect_resolution="complex_f_2" }
	complex_f(k, i, i, i);

	@static_test { expect_resolution="complex_f_3" }
	complex_f(k, i, l, j);
	
	@static_test { expect_resolution="complex_f_4" }
	complex_f(i, k, j, l);
	
	@static_test { expect_resolution="complex_f_5" }
	complex_f(m, i, n, j);

	@static_test { expect_resolution="complex_f_6" }
	complex_f(n, j, n, j);
}
