/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
//////////////////////////////////////////////////////////////////////////////
// some classes
//////////////////////////////////////////////////////////////////////////////

    class Foo {}
    class Bar {}
    class Base {}
    class Derived extends Base {}
    class Extended extends Derived {}
    class Complex<T> {}

//////////////////////////////////////////////////////////////////////////////
// functions
//////////////////////////////////////////////////////////////////////////////

    @static_test { resolution = "f(int32,int64)" } function f(v: int32, w: int64 = 1L): void {}
    @static_test { resolution = "f(int64,int64)" } function f(v: int64, w: int32 = 1 ): void {}

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32):void"  } } }
    function g(v: int32                 ): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32,int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32,int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32,int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g(int32,int32):void"  } } }
    function g(v: int32, w: int32 = 1234): void {}

    @static_test { resolution = "g2(int32,int32)" }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g2(int32,int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g2(int32,int32):void"  } } }
    function g2(v: int32, w: int32 = 1234): void {}
    @static_test { resolution = "g2(int32,int32,int32)" }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g2(int32,int32,int32):void"  } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function g2(int32,int32,int32):void"  } } }
    function g2(v: int32, w: int32 = 1234, x: int32 = 4321): void {}

    @static_test { resolution = "h(int32)"  }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function h(int32):void"  } } }
    function h   (v: int32             ): void {}
    @static_test { resolution = "h<T>(T)"   }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function h<T>(T):void"   } } }
    function h<T>(v: T                 ): void {}
    @static_test { resolution = "h<T>(T,T)" }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function h<T>(T,T):void" } } }
    function h<T>(v: T    , w: T = null): void {}

    @static_test { resolution = "i<T,U>(U,T)" }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE_CANDIDATES", parameters = { candidate = "function i<T,U>(U,T):void" } } } function i<T, U>(w: U, t: T = 1234): void {}

    @static_test { resolution = "j<T>(T,T)" } function j<T>(v: T, w: T = 1234): void {}

//////////////////////////////////////////////////////////////////////////////
// main
//////////////////////////////////////////////////////////////////////////////

    function main() : void
    {
        var vint8      : int8              ;
        var vint16     : int16             ;
        var vint32     : int32             ;
        var vint64     : int64             ;
        var vfloat32   : float32           ;
        var vfloat64   : float64           ;
        var vFoo       : Foo               ;
        var vBar       : Bar               ;
        var vBase      : Base              ;
        var vDerived   : Derived           ;
        var vExtended  : Extended          ;
        var vComplex   : Complex<int32>    ;

        // default parameter should not take effect for candidate selection (compare first parameter)
        @static_test { expect_resolution = "f(int32,int64)" } f(vint32);
        @static_test { expect_resolution = "f(int64,int64)" } f(vint64);

        // ambiguous due to default parameter
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(int8)"  } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"        } } } g(vint8);
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(int16)" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"        } } } g(vint16);
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(int32)" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"        } } } g(vint32);
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g(int64)" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g"        } } } g(vint64);

        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g2(int32)" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g2"        } } } g2(vint32);
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "g2(int32, int32)" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "g2"               } } } g2(vint32, vint32);

        // non-template is better than templates (include matched with default parameter)
        @static_test { expect_resolution = "h(int32)" } h(vint32);

        // ambiguous due to default parameter (for template functions)
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "h(class Foo)"   } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "h"        } } } h(vFoo);

        // default parameter will not take effect during deduction (event if the value is not compatible with the parameter type)
        @static_test { expect_resolution = "" }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_CANT_RESOLVE", parameters = { id = "i(int32)" } } }
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNDEFINED_SYMBOL_INFO", parameters = { id = "i"        } } } i(vint32);
        @static_test { expect_resolution = "j<T>(T,T)" } j(vFoo);
    }
