/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function foo() : float32 { return 3.14 ; }
function bar() : (int32, int16) { return 0, 1; }

function main() : void
{
    // int literal
    @static_test { infer_type = "int32"   } var i1 = 1;
    @static_test { infer_type = "int32"   } var i2 = 300;
    @static_test { infer_type = "int32"   } var i3 = 77777;

    // float literal
    @static_test { infer_type = "float64" } var f1 = 3.14;

    // simple expr
    @static_test { infer_type = "int32"   } var i4 = i1 + i2;
    @static_test { infer_type = "int32"   } var i5 = i1 + i3;
    @static_test { infer_type = "int32"   } var i6 = i2 + i3;
    @static_test { infer_type = "int32"   } var i7 = i1 + i1;
    @static_test { infer_type = "float64" } var t2 = f1 + i1;

    // complex expr
    @static_test { infer_type = "float32" } var f3 = foo();
    @static_test { infer_type = "float32" } var f4 = foo() + 1;
    @static_test { infer_type = "float32" } var f5 = foo() + foo();

    // delay infer
    @static_test { infer_type = "int32" } var d1;
    @static_test { infer_type = "int32" } var d2;
    d1 = 1;
    d2 = d1;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "UNINFERRED_VAR_TYPE", parameters = { id = "e1" } } }
    var e1;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION_OF_MULTI_VALUE", parameters = { type = "(int32,int16)" } } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_EXPRESSION_OF_MULTI_VALUE", parameters = { type = "(int32,int16)" } } }
    e1 = bar();
}
