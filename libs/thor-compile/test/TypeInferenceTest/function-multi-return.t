/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Foo{}
class Bar{}

function f11() {
    if(true) {
        return 2;
    } else {
        return 1; 
    }
}
function f12() {
    if(true) {
        return 1;
    } else {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_RETURN_TYPE_INFER_CONFLICT", parameters = { new_type = "float64", old_type = "int32" } } }
        return 3.14; 
    }
}
function f13() {
    if(true) {
        return 4;
    } else {
        var o : Foo;
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_RETURN_TYPE_INFER_CONFLICT", parameters = { new_type = "Foo", old_type = "int32" } } }
        return o;
    }
}
function f14() {
    if(true) {
        return 4;
    } else {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_RETURN_TYPE_INFER_CONFLICT", parameters = { new_type = "Object", old_type = "int32" } } }
        return null; 
    }
}

function f21() {
    if(true) {
        var o : Foo;
        return o;
    } else {
        var o : Foo;
        return o;
    }
}
function f22() {
    if(true) {
        var o : Foo;
        return o;
    } else {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_RETURN_TYPE_INFER_CONFLICT", parameters = { new_type = "int32", old_type = "Foo" } } }
        return 1; 
    }
}
function f23() {
    if(true) {
        var o : Foo;
        return o;
    } else {
        var o : Bar;
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_RETURN_TYPE_INFER_CONFLICT", parameters = { new_type = "Bar", old_type = "Foo" } } }
        return o;
    }
}
function f24() {
    if(true) {
        var o : Foo;
        return o;
    } else {
        return null; 
    }
}

function f31() {
    if(true) {
        return null;
    } else {
        return null; 
    }
}
function f32() {
    if(true) {
        return null;
    } else {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "FUNCTION_RETURN_TYPE_INFER_CONFLICT", parameters = { new_type = "int32", old_type = "Object" } } }
        return 1; 
    }
}
function f33() {
    if(true) {
        return null;
    } else {
        var o : Foo;
        return o;
    }
}

