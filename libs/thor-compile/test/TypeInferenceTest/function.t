/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// primitive
function max(a : int32, b : int32)
{
    return a > b ? a : b ;
}

// void
function void_one  () { while (true) break   ; return void_two()  ; }
function void_two  () { for   ( ;; ) continue; return void_three(); }
function void_three() { while (true) break   ;                      }

// class
class Foo { }

@native
function class_a() : Foo ;
function class_b() { while (true) break   ; return class_a(); }
function class_c() { for   ( ;; ) continue; return class_b(); }

// multi
function multi_a() : (int32, int64) { return 0, 1 ; }
function multi_b()                  { return 0, 1l; }

function main() : void
{
    // primitive
    @static_test { infer_type = "int32"   } var c = max(1, 3);

    // void return of return
    @static_test { infer_type = "void"   } var v1 = void_one();
    @static_test { infer_type = "void"   } var v2 = void_two();
    @static_test { infer_type = "void"   } var v3 = void_three();

    // class
    @static_test { infer_type = "Foo"   } var u1 = class_a();
    @static_test { infer_type = "Foo"   } var u2 = class_b();
    @static_test { infer_type = "Foo"   } var u3 = class_c();

    // multi
    @static_test { infer_type = "int32" } var m1;
    @static_test { infer_type = "int64" } var m2;
    @static_test { infer_type = "int32" } var m3;
    @static_test { infer_type = "int64" } var m4;
    @static_test { infer_type = "int16" } var m5;

    m1, m2 = multi_a();
    (m3, m4), m5 = multi_b(), cast<int16>(1);
}
