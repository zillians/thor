/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <utility>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem/path.hpp>

#include "core/SharedPtr.h"

#include "language/ThorConfiguration.h"
#include "language/context/ConfigurationContext.h"
#include "language/stage/compiler/InitConfigurationStage.h"

namespace zillians { namespace language { namespace stage {

InitConfigurationStage::InitConfigurationStage()
{ }

InitConfigurationStage::~InitConfigurationStage()
{ }

const char* InitConfigurationStage::name()
{
    return "InitConfigurationStage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> InitConfigurationStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public = make_shared<po::options_description>();
    shared_ptr<po::options_description> option_desc_private = make_shared<po::options_description>();

    option_desc_public->add_options()
        ("project-path"        , po::value<std::string>(), "set the project root path")
        ("mode-resolution-test", po::value<std::string>(), "for resolution test, specify to disable context initializating")
    ;

    for (auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool InitConfigurationStage::parseOptions(po::variables_map& vm)
{
    should_init_context = (!hasConfigurationContext() && !vm.count("mode-resolution-test"));

    project_path.clear();
    if (should_init_context)
    {
        if (vm.count("project-path"))
        {
            project_path = vm["project-path"].as<std::string>();
        }
    }

    return true;
}

bool InitConfigurationStage::execute(bool& continue_execution)
{
    if (should_init_context)
    {
        ThorBuildConfiguration config;
        const auto load_success = (project_path.empty() ? config.loadDefault() : config.load(project_path));

        if (!load_success)
            return false;

        initConfigurationContext(std::move(config));
    }

    return true;
}

} } }
