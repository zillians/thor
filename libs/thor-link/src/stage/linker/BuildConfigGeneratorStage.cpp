/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/assert.hpp>
#include <boost/range/algorithm/for_each.hpp>

#include "utility/StringUtil.h"

#include "language/context/ParserContext.h"

#include "language/stage/linker/BuildConfigGeneratorStage.h"

namespace zillians { namespace language { namespace stage {

BuildConfigGeneratorStage::BuildConfigGeneratorStage()
{ }

BuildConfigGeneratorStage::~BuildConfigGeneratorStage()
{ }

const char* BuildConfigGeneratorStage::name()
{
    return "BuildConfigGeneratorStage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> BuildConfigGeneratorStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("project-path", po::value<std::string>(), "set the project root path")
        ("build-path", po::value<std::string>(), "set the build directory")
        ("binary-output-path", po::value<std::string>(), "set the binary output path")
		("verbose,v", "verbose mode, which dumps command")
        ("dump-llvm,l", "dump LLVM IR")
    ;

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool BuildConfigGeneratorStage::parseOptions(po::variables_map& vm)
{
    if(!hasGeneratorContext())
    {
        if(vm.count("project-path"))
            setGeneratorContext(new GeneratorContext(vm["project-path"].as<std::string>()));
        else
            setGeneratorContext(new GeneratorContext());
    }

    GeneratorContext& generator_context = getGeneratorContext();

    if(vm.count("project-path"))
    	generator_context.global_config.project_path = vm["project-path"].as<std::string>();

    if(vm.count("build-path"))
    	generator_context.global_config.build_path_override = vm["build-path"].as<std::string>();

    if(vm.count("binary-output-path"))
    	generator_context.global_config.binary_output_path_override = vm["binary-output-path"].as<std::string>();

    if (vm.count("verbose"))
    	generator_context.global_config.verbose = true;
    else
    	generator_context.global_config.verbose = false;

    if (vm.count("dump-llvm"))
        generator_context.global_config.dump_llvm = true;
    else
        generator_context.global_config.dump_llvm = false;

    return true;
}

bool BuildConfigGeneratorStage::execute(bool& continue_execution)
{
	GeneratorContext& generator_context = getGeneratorContext();

    if(!generator_context.global_config.loadDefault(false, false, true))
    {
        std::cerr << "Error: failed to load project configuration" << std::endl;
        return false;
    }
    generator_context.global_config.createDirectoriesIfNecessary();

	// TODO search the AST and find all required build configuration for CPU target

	// TODO search the AST and find all required build configuration for GPU target

//	std::vector<FunctionDecl*> gpu_functions;
//	ASTNodeHelper::foreachApply<FunctionDecl>(*getParserContext().tangle, [&](ASTNode& node) {
//		if(ASTNodeHelper::hasAnnotation(&node, L"gpu"))
//			gpu_functions.push_back(&node);
//	});
//
//	foreach(i, gpu_functions)
//	{
//		FunctionDecl* current_function = *i;
//		ASTNodeHelper::foreachApply<Block>(*current_function, [=](ASTNode& node) {
//			if(Annotation* ann_cuda = ASTNodeHelper::findAnnotation(L"cuda"))
//			{
//				foreach(j, ann_cuda->attribute_list)
//				{
//					if()
//				}
//			}
//		});
//	}

    BOOST_ASSERT(hasParserContext()                           && "parser context is required (workaround for current design to pass tangle)");
    BOOST_ASSERT(getParserContext().tangle         != nullptr && "null pointer exception");
    BOOST_ASSERT(getParserContext().tangle->config != nullptr && "null pointer exception");
    generator_context.emit_debug_info = getParserContext().tangle->config->emit_debug_info;
    generator_context.opt_level       = getParserContext().tangle->config->opt_level      ;

    boost::for_each(
        generator_context.global_config.manifest.arch.splitFlags(),
        [&generator_context](Architecture flag)
        {
            GeneratorContext::Configuration* config = new GeneratorContext::Configuration(flag);
            generator_context.target_config.push_back(config);
        }
    );

    return true;
}

} } }
