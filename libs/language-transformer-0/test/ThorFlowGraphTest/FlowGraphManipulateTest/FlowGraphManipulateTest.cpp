/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <array>

#define BOOST_TEST_MODULE ThorFlowGraphTest_FlowGraphManipulateTest
#define BOOST_TEST_MAIN
#include <boost/graph/adjacency_list.hpp>
#include <boost/test/unit_test.hpp>

#include "language/tree/basic/Literal.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/stage/transformer/detail/FlowSpliter.h"

using namespace zillians::language::stage::visitor::flow_spliter_detail;

BOOST_AUTO_TEST_SUITE( ThorFlowGraphTest_FlowGraphManipulateTestSuite )

BOOST_AUTO_TEST_CASE( ThorFlowGraphTest_FlowGraphManipulateTestCaseSimpleOptimization )
{
    using zillians::language::tree::Statement;
    using zillians::language::tree::ExpressionStmt;
    using zillians::language::tree::BinaryExpr;
    using zillians::language::tree::NumericLiteral;

    // erase empty block
    {
        graph_t fg;

        auto func_init = boost::add_vertex(fg); fg[func_init].tag = 0;
        auto b1        = boost::add_vertex(fg); fg[b1       ].tag = 1;
        auto b2        = boost::add_vertex(fg); fg[b2       ].tag = 2;
        auto b3        = boost::add_vertex(fg); fg[b3       ].tag = 3;
        auto b4        = boost::add_vertex(fg); fg[b4       ].tag = 4;
        auto func_end  = boost::add_vertex(fg); fg[func_end ].tag = 5;

        boost::add_edge(func_init, b1      , fg); fg[func_init].transition = TRANS_NORMAL;
        boost::add_edge(b1       , b2      , fg); fg[b1       ].transition = TRANS_NORMAL;
        boost::add_edge(b2       , b3      , fg); fg[b2       ].transition = TRANS_NORMAL;
        boost::add_edge(b3       , b4      , fg); fg[b3       ].transition = TRANS_NORMAL;
        boost::add_edge(b4       , func_end, fg); fg[b4       ].transition = TRANS_NORMAL;
                                                  fg[func_end ].transition = TRANS_NULL  ;

        optimize_erase_empty_or_null(fg);

        BOOST_CHECK_EQUAL(boost::num_vertices(fg), 1         );
        BOOST_CHECK_EQUAL(fg[0].tag              , 0         );
        BOOST_CHECK_EQUAL(fg[0].transition       , TRANS_NULL);
    }

    // merge blocks
    {
        graph_t fg;

        auto func_init = boost::add_vertex(fg); fg[func_init].tag = 0;
        auto b1        = boost::add_vertex(fg); fg[b1       ].tag = 1;
        auto b2        = boost::add_vertex(fg); fg[b2       ].tag = 2;
        auto b3        = boost::add_vertex(fg); fg[b3       ].tag = 3;
        auto b4        = boost::add_vertex(fg); fg[b4       ].tag = 4;
        auto func_end  = boost::add_vertex(fg); fg[func_end ].tag = 5;

        boost::add_edge(func_init, b1      , fg); fg[func_init].transition = TRANS_NORMAL;
        boost::add_edge(b1       , b2      , fg); fg[b1       ].transition = TRANS_NORMAL;
        boost::add_edge(b2       , b3      , fg); fg[b2       ].transition = TRANS_NORMAL;
        boost::add_edge(b3       , b4      , fg); fg[b3       ].transition = TRANS_NORMAL;
        boost::add_edge(b4       , func_end, fg); fg[b4       ].transition = TRANS_NORMAL;
                                                  fg[func_end ].transition = TRANS_NULL  ;

        const std::array<Statement*, 6> stmts = {
            new ExpressionStmt(new NumericLiteral(0)),
            new ExpressionStmt(new NumericLiteral(1)),
            new ExpressionStmt(new NumericLiteral(2)),
            new ExpressionStmt(new NumericLiteral(3)),
            new ExpressionStmt(new NumericLiteral(4)),
            new ExpressionStmt(new NumericLiteral(5)),
        };

        fg[func_init].statements.emplace_back(stmts[func_init]);
        fg[func_init].statements.emplace_back(stmts[b1       ]);
        fg[func_init].statements.emplace_back(stmts[b2       ]);
        fg[func_init].statements.emplace_back(stmts[b3       ]);
        fg[func_init].statements.emplace_back(stmts[b4       ]);
        fg[func_init].statements.emplace_back(stmts[func_end ]);

        BOOST_CHECK_EQUAL(boost::num_vertices(fg), 6);
        BOOST_CHECK_EQUAL(fg[0].tag, 0);

        optimize_merge_block(fg);

        BOOST_CHECK_EQUAL(boost::num_vertices(fg), 1);
        BOOST_CHECK_EQUAL(fg[0].tag              , 0);
        BOOST_CHECK_EQUAL_COLLECTIONS(fg[0].statements.cbegin(), fg[0].statements.cend(), stmts.cbegin(), stmts.cend());
    }
}

BOOST_AUTO_TEST_SUITE_END()
