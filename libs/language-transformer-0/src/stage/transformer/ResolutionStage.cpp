/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/logging/StringTable.h"
#include "language/stage/transformer/ResolutionStage.h"
#include "language/stage/transformer/visitor/ResolutionStageVisitor.h"
#include "language/stage/transformer/visitor/ResolutionStagePreambleVisitor.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"

namespace zillians { namespace language { namespace stage {

using tree::cast;

ResolutionStage::ResolutionStage() :
        debug(false),
        disable_type_inference(false),
        total_unresolved_count_type(std::numeric_limits<std::size_t>::max()),
        total_unresolved_count_symbol(std::numeric_limits<std::size_t>::max()),
        dump_ts(false),
        dump_graphviz(false),
        keep_going(false)
{ }

ResolutionStage::~ResolutionStage()
{ }

const char* ResolutionStage::name()
{
    return "Resolution Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ResolutionStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("no-type-inference", "disable type inference system so every type declaration must be made explicitly");

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
        ("debug-resolution-stage", "debug type conversion stage")
        ("dump-ts", "dump intermediate AST in Thor format")
        ("dump-ts-dir", po::value<std::string>(), "directory to dump intermediate AST in Thor format")
        ("dump-graphviz", po::bool_switch(), "dump intermediate AST in graphviz format")
        ("dump-graphviz-dir", po::value<std::string>(), "directory to dump intermediate AST in graphviz format")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ResolutionStage::parseOptions(po::variables_map& vm)
{
    debug = (vm.count("debug-resolution-stage") > 0);
    disable_type_inference = (vm.count("no-type-inference") > 0);
    dump_ts = (vm.count("dump-ts") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();

    if(vm.count("dump-ts-dir") > 0)
        dump_ts_dir = vm["dump-ts-dir"].as<std::string>();

    if(vm.count("dump-graphviz-dir") > 0)
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();

    // keep going to static test when testing resolution
    if(vm.count("mode-resolution-test") > 0)
        keep_going = true;

    return true;
}

bool ResolutionStage::hasProgress()
{
    // Well, we always return false. There is only one condition that 
    // we leave the resolution stage is that there is no progress.
    return false;
}

bool isPassFinished(const ResolutionStage::ResolveReport& resolve_report)
{
    return resolve_report.has_resolved     ||
           resolve_report.has_instantiated ;
}

bool ResolutionStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!hasParserContext())
        return false;

    LoggerWrapper::instance()->resetCounter();

    visitor::ResolutionStageVisitor::PersistantState state;

    {
        visitor::ResolutionStagePreambleVisitor preamble_visitor(state.specialization_grouper);

        preamble_visitor.visit(*getParserContext().tangle);
    }

    static int phase = 0;
    ++phase;

    int iteration = 0;
    resolve_report.reset();
    while(true)
    {
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, L"=== Resolution iteration : " << (phase-1) << "-" << iteration);
        if(dump_ts)
        {
            std::ostringstream oss;
            oss << "pre-resolution-" << (phase-1) << "-" << iteration << ".t";
            boost::filesystem::path p(dump_ts_dir);
            getParserContext().tangle->toSource(p / oss.str());
        }

        if(dump_graphviz)
        {
            std::ostringstream oss;
            oss << "pre-resolution-" << (phase-1) << "-" << iteration << ".dot";
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / oss.str());
            oss.str("");
            oss << "pre-resolution-" << (phase-1) << "-" << iteration << ".t";
            getParserContext().tangle->toSource(p / oss.str());
        }

        resolve_report = resolveSymbols(state);

        if (isPassFinished(resolve_report))
            state.specialization_grouper.ready();

        if(dump_ts)
        {
            std::ostringstream oss;
            oss << "post-resolution-" << (phase-1) << "-" << iteration << ".t";
            boost::filesystem::path p(dump_ts_dir);
            getParserContext().tangle->toSource(p / oss.str());
        }

        if(dump_graphviz)
        {
            std::ostringstream oss;
            oss << "post-resolution-" << (phase-1) << "-" << iteration << ".dot";
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / oss.str());
            oss.str("");
            oss << "post-resolution-" << (phase-1) << "-" << iteration << ".t";
            getParserContext().tangle->toSource(p / oss.str());
        }
        ++iteration;

        if(resolve_report.has_failed && !keep_going)
            break;

        if(!isPassFinished(resolve_report))
            break;
    }

    checkUninferredVariable();

    // remove trivial and redundant errors
    removeTrivialErrors();

    // report errors
    reportErrors();

    if(debug)
    {
        tree::visitor::PrettyPrintVisitor printer;
        printer.visit(*getParserContext().tangle);
    }

    if (keep_going) return true;
    return !(LoggerWrapper::instance()->hasError() || LoggerWrapper::instance()->hasFatal());
}

void ResolutionStage::checkUninferredVariable()
{
    tree::ASTNodeHelper::foreachApply<tree::VariableDecl>(
        *(getParserContext().tangle),
        [](tree::VariableDecl& var_decl) {
            if(var_decl.type == NULL)
            {
                LOG_MESSAGE(UNINFERRED_VAR_TYPE, &var_decl, _id(var_decl.name->toString()));
            }
        }, [](tree::ASTNode& node) -> bool {
            if(tree::FunctionDecl* func_decl = cast<tree::FunctionDecl>(&node))
            {
                return func_decl->isCompleted();
            }
            else if(tree::ClassDecl* cls_decl = cast<tree::ClassDecl>(&node))
            {
                return cls_decl->isCompleted();
            }
            else
            {
                return true;
            }
        }
    );
}

template<typename... Args>
ResolutionStage::ResolveReport ResolutionStage::resolveSymbols(Args&... args)
{
    LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "resolution stage trying to resolve symbols");

    visitor::ResolutionStageVisitor visitor(args...);
    visitor.visit(*getParserContext().tangle);

    const bool has_transform = visitor.hasTransforms();

    if(has_transform)
    {
        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, "Apply visitor transforms");
        visitor.applyTransforms();
    }

    return ResolveReport(visitor.getResolvedCount() != 0,
                         visitor.getFailedCount  () != 0,
                         visitor.getUnknownCount () != 0,
                         has_transform);
}

void ResolutionStage::removeTrivialErrors()
{
    if(unresolved_symbols.size() > 0 || unresolved_types.size() > 0)
    {
        auto it = unresolved_symbols.begin();
        std::vector<std::function<void()>> cleanup;

        while(it != unresolved_symbols.end())
        {
            // search for any ancestor of it in unresolved_symbols
            for(auto* unresolved_symbol : unresolved_symbols)
            {
                tree::ASTNode* parent = unresolved_symbol->parent;
                while(parent)
                {
                    if(unresolved_symbol == parent)
                    {
                        cleanup.push_back([=]{
                            unresolved_symbols.erase(parent);
                        });
                    }
                    parent = parent->parent;
                }
            }

            // search for any ancestor of it in unresolved_types
            for(auto* unresolved_type : unresolved_types)
            {
                tree::ASTNode* parent = unresolved_type->parent;
                while(parent)
                {
                    if(unresolved_type == parent)
                    {
                        cleanup.push_back([=]{
                            unresolved_types.erase(parent);
                        });
                    }
                    parent = parent->parent;
                }
            }

            // remove any ancestor of it since it's not leaf node, so the error is trivial due to unresolved descendant
            for(auto& cleaner : cleanup) cleaner();

            if(cleanup.size() > 0)
            {
                cleanup.clear();
                it = unresolved_symbols.begin();
            }
            else
            {
                ++it;
            }
        }

        it = unresolved_types.begin();

        while(it != unresolved_types.end())
        {
            // search for any ancestor of it in unresolved_symbols
            for(auto* unresolved_symbol : unresolved_symbols)
            {
                tree::ASTNode* parent = unresolved_symbol->parent;
                while(parent)
                {
                    if(unresolved_symbol == parent)
                    {
                        cleanup.push_back([=]{
                            unresolved_symbols.erase(parent);
                        });
                    }
                    parent = parent->parent;
                }
            }

            // search for any ancestor of it in unresolved_types
            for(auto* unresolved_type : unresolved_types)
            {
                tree::ASTNode* parent = unresolved_type->parent;
                while(parent)
                {
                    if(unresolved_type == parent)
                    {
                        cleanup.push_back([=]{
                            unresolved_types.erase(parent);
                        });
                    }
                    parent = parent->parent;
                }
            }

            // remove any ancestor of it since it's not leaf node, so the error is trivial due to unresolved descendant
            for(auto& cleaner : cleanup) cleaner();

            if(cleanup.size() > 0)
            {
                cleanup.clear();
                it = unresolved_types.begin();
            }
            else
            {
                ++it;
            }
        }
    }
}

void ResolutionStage::reportErrors()
{
    if(unresolved_symbols.size() > 0)
    {
        for(auto* unresolved_symbol : unresolved_symbols)
        {
            // avoid duplicate error message if a symbol is not resolved
            if(unresolved_types.count(unresolved_symbol) > 0)
                unresolved_types.erase(unresolved_symbol);

            LOG_MESSAGE(UNDEFINED_SYMBOL_INFO, unresolved_symbol, _id(tree::ASTNodeHelper::getNodeName(unresolved_symbol)));
        }
    }

    if(unresolved_types.size() > 0)
    {
        for(auto* unresolved_type : unresolved_types)
        {
            LOG_MESSAGE(UNDEFINED_TYPE_INFO, unresolved_type, _id(tree::ASTNodeHelper::getNodeName(unresolved_type)));
        }
    }
}

} } }
