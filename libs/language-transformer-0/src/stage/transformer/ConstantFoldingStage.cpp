/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "language/stage/transformer/ConstantFoldingStage.h"
#include "language/stage/transformer/visitor/ConstantFoldingStageVisitor.h"

namespace zillians { namespace language { namespace stage {

ConstantFoldingStage::ConstantFoldingStage()
: dump_graphviz(false),
  keep_going(false)
{ }

ConstantFoldingStage::~ConstantFoldingStage()
{ }

const char* 
ConstantFoldingStage::name()
{
    return "Constant Folding Stage";
}

std::pair<
          shared_ptr<po::options_description>, 
          shared_ptr<po::options_description>
         > 
ConstantFoldingStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

   	option_desc_private->add_options()
        ("debug-constant-folding-stage", "debug constant folding stage")
        //("dump-graphviz", "dump AST in graphviz format")
        //("dump-graphviz-dir", po::value<std::string>(), "dump AST in graphviz format")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool 
ConstantFoldingStage::parseOptions(po::variables_map& vm)
{
    debug = (vm.count("debug-constant-folding-stage") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }
    if(vm.count("mode-constant-folding") > 0)
    {
        keep_going = true;
    }
    
    return true;
}

bool 
ConstantFoldingStage::execute(bool& continue_execution)
{

    if(keep_going)
        continue_execution = true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    BOOST_ASSERT(parser_context.tangle && "no tangle!");

    if(parser_context.tangle)
    {
        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / "pre-constant-folding.dot");
            getParserContext().tangle->toSource(p / "pre-constant-folding.t");
        }

        visitor::ConstantFoldingStageVisitor folding_visitor;
        folding_visitor.visit(*parser_context.tangle);

        if(debug)
        {
            tree::visitor::PrettyPrintVisitor printer;
            printer.visit(*parser_context.tangle);
        }

        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / "post-constant-folding.dot");
            getParserContext().tangle->toSource(p / "post-constant-folding.t");
        }

        return true;
    }
    else
    {
        return false;
    }
}

} } } 
