/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/StringUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/tree/ASTNodeHelper.h"
#include "language/stage/transformer/detail/RestructureHelper.h"
#include "language/stage/transformer/detail/RestructureLambda0.h"

namespace zillians { namespace language { namespace stage { namespace detail {

using namespace language::tree;

namespace {

Identifier* createLambdaIdentifier(ASTNode& refer_node, const FunctionSpecifier& func)
{
    const auto&      parameter_types = func.getParameterTypes();
          auto*const return_type     = func.getReturnType();

    std::wstring argument_count_string = std::to_wstring(parameter_types.size());
    auto tid = new TemplatedIdentifier(TemplatedIdentifier::Usage::ACTUAL_ARGUMENT, new SimpleIdentifier(L"Lambda" + argument_count_string));

    tid->append(new TypenameDecl(new SimpleIdentifier(L"_"), ASTNodeHelper::clone(return_type)));
    for(auto& param_type : parameter_types)
    {
        tid->append(new TypenameDecl(new SimpleIdentifier(L"_"), ASTNodeHelper::clone(param_type)));
    }

    return ASTNodeHelper::createThorLangId(&refer_node, tid);
}

Identifier* createLambdaIdentifier(ASTNode& refer_node, const FunctionDecl& func)
{
    std::wstring argument_count_string = std::to_wstring(func.parameters.size());
    auto tid = new TemplatedIdentifier(TemplatedIdentifier::Usage::ACTUAL_ARGUMENT, new SimpleIdentifier(L"Lambda" + argument_count_string));

    tid->append(new TypenameDecl(new SimpleIdentifier(L"_"), ASTNodeHelper::clone(func.type)));
    for(auto& parameter : func.parameters)
    {
        tid->append(new TypenameDecl(new SimpleIdentifier(L"_"), ASTNodeHelper::clone(parameter->type)));
    }

    return ASTNodeHelper::createThorLangId(&refer_node, tid);
}

}

void RestructureLambda0::restruct(FunctionSpecifier& node)
{
    // restruct the function type into corresponding Lambda class type
    //
    // e.g. var f : lambda(int32) : void;
    //
    // ===> var f : Lambda1<void, int32>;

    if (!node.isLambda())
        return;

    transforms.push_back([&node]() {
        auto nid            = createLambdaIdentifier(node, node);
        auto type_specifier = new NamedSpecifier(nid);

        node.parent->replaceUseWith(node, *type_specifier);
    });
}

void RestructureLambda0::restruct(LambdaExpr& node)
{
    // prevent infinite restructure
    BlockExpr* existed_block = node.getOwner<BlockExpr>();
    if (!existed_block || existed_block->tag != "lambda_restruct_partial")
    {
        // restruct lambda expression to block expression
        //
        // e.g. var x = lambda(a: int32) : void {}
        //
        // ===> var x = {
        //                  var t : Lambda<void, int32>;
        //                  lambda(a: int32) : void {}  // just keep it for R1
        //                  t;
        //              }
        //
        transforms.push_back([&]() {
            ASTNode* node_parent     = node.parent;
            auto     lambda_function = node.getLambda();

            BlockExpr* block_expr = new BlockExpr(new NormalBlock(), "lambda_restruct_partial");

            block_expr->parent = node.parent;

            // var t : Lambda<>;
            VariableDecl* t_decl = NULL;
            {
                UUID uuid = UUID::random();
                std::wstring wstr_id = L"lambda_temp_" + s_to_ws(uuid.toString('_'));

                auto*const lambda_id = createLambdaIdentifier(node, *lambda_function);

                t_decl = new VariableDecl(
                    new SimpleIdentifier(wstr_id),
                    new NamedSpecifier(lambda_id),
                    false, false, false,
                    Declaration::VisibilitySpecifier::DEFAULT,
                    new ObjectLiteral(ObjectLiteral::LiteralType::NULL_OBJECT)
                );

                block_expr->block->appendObject(new DeclarativeStmt(t_decl));
            }

            // keep the original lambda statement
            {
                block_expr->block->appendObject(new ExpressionStmt(&node));
            }

            // t;
            {
                IdExpr* t_use = new IdExpr(new SimpleIdentifier(t_decl->name->toString()));
                block_expr->block->appendObject(new ExpressionStmt(t_use));
            }

            node_parent->replaceUseWith(node, *block_expr, false);

            ASTNodeHelper::foreachApply<ASTNode>(
                *block_expr,
                [&](ASTNode& n)
                {
                    if (!SourceInfoContext::get(&n))
                    {
                        ASTNodeHelper::propogateSourceInfo(n, node);
                    }
                },
                [&](ASTNode& n)
                {
                    return &n != &node;
                }
            );

            {
                std::vector<std::function<void()>> dummy_transforms;

                visitor::restructure_helper::splitVariableInitializer(*t_decl, dummy_transforms, false);

                BOOST_ASSERT(dummy_transforms.empty() && "we got real transforms from dummy!");
            }
        });
    }
}

} } } }
