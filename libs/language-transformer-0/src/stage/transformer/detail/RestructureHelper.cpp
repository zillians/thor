/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <array>
#include <functional>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>

#include <boost/assert.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>

#include "utility/UUIDUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/logging/LoggerWrapper.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/CastExpr.h"
#include "language/tree/expression/IdExpr.h"
#include "language/tree/expression/MemberExpr.h"
#include "language/tree/expression/TieExpr.h"
#include "language/tree/expression/UnaryExpr.h"
#include "language/tree/expression/UnpackExpr.h"
#include "language/tree/statement/Statement.h"
#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/ExpressionStmt.h"

#include "language/context/ResolverContext.h"
#include "language/stage/transformer/detail/MultiTypeHelper.h"
#include "language/stage/transformer/detail/RestructureHelper.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace restructure_helper {

using namespace ::zillians::language::tree;

namespace {

Identifier* buildIdentifierFromNewOperator(ASTNode* current)
{
    if(isa<IdExpr>(current))
    {
        return cast<IdExpr>(current)->getId()->clone();
    }
    else if(isa<MemberExpr>(current))
    {
        MemberExpr* member_expr = cast<MemberExpr>(current);
        Identifier *id = buildIdentifierFromNewOperator(member_expr->node);
        if(!isa<NestedIdentifier>(id))
        {
            NestedIdentifier* nested_id = new NestedIdentifier();
            nested_id->appendIdentifier(id);
            nested_id->appendIdentifier(member_expr->member->clone());
            return nested_id;
        }
        else
        {
            cast<NestedIdentifier>(id)->appendIdentifier(member_expr->member->clone());
            return id;
        }
    }
    else
    {
        UNREACHABLE_CODE();
        return NULL;
    }
}

template< typename CandidateType, typename InserterType>
void restructStaticOrGlobalVariableInit(VariableDecl& node, const CandidateType& candidates, const std::wstring& key, bool is_global, InserterType inserter)
{
    Block* init_block = NULL;

    boost::find_if(
        candidates | boost::adaptors::reversed,
        [=,&key,&init_block](typename CandidateType::const_reference candidate) -> bool{
            if(auto* init_function = cast<FunctionDecl>(candidate)){
                if(init_function->name->toString() == key && init_function->parameters.empty()){

                    BOOST_ASSERT(init_function->type && isa<PrimitiveSpecifier>(init_function->type) && cast<PrimitiveSpecifier>(init_function->type)->getKind() == PrimitiveKind::VOID_TYPE);
                    BOOST_ASSERT(!is_global || init_function->is_global_init);

                    init_block = init_function->block;
                    return true;
                }
            }
            return false;
        }
    );

    if(!init_block)
    {
        bool is_member = !is_global;
        bool is_static = !is_global;

        Identifier*    new_identifier = new SimpleIdentifier(key);
        TypeSpecifier* new_init_ts    = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
        Block*         new_init_block = new NormalBlock();
        FunctionDecl*  new_init       = new FunctionDecl(new_identifier, new_init_ts, is_member, is_static, false, false, Declaration::VisibilitySpecifier::DEFAULT, new_init_block);

        inserter(*new_init);

        new_init->is_global_init = true;

        init_block = new_init_block;

        ASTNodeHelper::propogateSourceInfo(*new_identifier, node); // propagate the source info
        ASTNodeHelper::propogateSourceInfo(*new_init_ts   , node); // propagate the source info
        ASTNodeHelper::propogateSourceInfo(*new_init_block, node); // propagate the source info
        ASTNodeHelper::propogateSourceInfo(*new_init      , node); // propagate the source info
    }

    auto*const new_identifier       = node.name->clone();
    auto*const new_assignee_expr    = new IdExpr(new_identifier);
    auto*const new_assignment_expr  = BinaryExpr::create<'='>(new_assignee_expr, node.initializer);
    auto*const new_expr_stmt        = new ExpressionStmt(new_assignment_expr);
    auto*const new_arch_guard_block = new NormalBlock({new_expr_stmt}, node.arch);
    auto*const new_arch_guard_expr  = new BlockExpr(new_arch_guard_block);
    auto*const new_arch_guard_stmt  = new ExpressionStmt(new_arch_guard_expr);

    setSplitReference(
        node                ,
        new_identifier      ,
        new_assignee_expr   ,
        new_assignment_expr ,
        new_expr_stmt       ,
        new_arch_guard_block,
        new_arch_guard_expr ,
        new_arch_guard_stmt
    );
    init_block->prependObject(new_arch_guard_stmt);

    ASTNodeHelper::propogateSourceInfo(*new_identifier      , node); // propagate the source info
    ASTNodeHelper::propogateSourceInfo(*new_assignee_expr   , node); // propagate the source info
    ASTNodeHelper::propogateSourceInfo(*new_assignment_expr , node); // propagate the source info
    ASTNodeHelper::propogateSourceInfo(*new_expr_stmt       , node); // propagate the source info
    ASTNodeHelper::propogateSourceInfo(*new_arch_guard_block, node); // propagate the source info
    ASTNodeHelper::propogateSourceInfo(*new_arch_guard_expr , node); // propagate the source info
    ASTNodeHelper::propogateSourceInfo(*new_arch_guard_stmt , node); // propagate the source info

    node.initializer = NULL;
}

struct UnpackCall
{
    template<typename ...ArgTypes>
    void operator()(ArgTypes &&...args)
    {
        return multi_type_helper::unpackCall(std::forward<ArgTypes>(args)...);
    }
};

struct MergeExpr
{
    template<typename ...ArgTypes>
    void operator()(ArgTypes &&...args)
    {
        return multi_type_helper::mergeExpr(std::forward<ArgTypes>(args)...);
    }
};

} // anonymous namespace

void splitVariableInitializer(VariableDecl& node)
{
    std::vector<std::function<void()>> transforms;

    splitVariableInitializer(node, transforms, false);

    BOOST_ASSERT(transforms.empty() && "no postpone transform but postponed any");
}

void splitVariableInitializer(VariableDecl& node, std::vector<std::function<void()>>& transforms, bool postpone/* = true*/)
{
    if (!node.initializer) return;

    // avoid initializer restructure of VariableDecl within EnumDecl
    if(isa<EnumDecl>(node.parent)) return;

    /*
       transform all initializer into a separate assignment binary expression
       for example (var a = ...):

             block
            /  |  \
           ......  decl_stmt
                       |
                    var_decl
                   /        \
                  a         (initializer)

       will be transformed into:

             block---------------+
            /  |  \               \
           ......  decl_stmt       expr_stmt
                       |               |
                    var_decl     binary_expr(=)
                   /        \         / \
                  a         (null)  a   (initializer)

       note that we skip parameters in FunctionDecl, which shouldn't be transformed
    */
    if(node.hasOwner<FunctionDecl>() && node.hasOwner<Block>())
    {
        auto f = [&](){
            DeclarativeStmt* anchor = cast<DeclarativeStmt>(node.parent);
            Block* parent = (anchor && anchor->parent) ? cast<Block>(anchor->parent) : NULL;

            BOOST_ASSERT(parent != NULL && anchor != NULL && "variable declaration has incorrect hierarchy");

            Identifier*     new_identifier      = ASTNodeHelper::clone(node.name);
            IdExpr*         new_id_expr         = new IdExpr(new_identifier);
            BinaryExpr*     new_assignment_expr = new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new_id_expr, node.initializer);
            ExpressionStmt* new_expr_stmt       = new ExpressionStmt(new_assignment_expr);

            setSplitReference(node, new_identifier, new_id_expr, new_assignment_expr, new_expr_stmt);
            parent->insertObjectAfter(anchor, new_expr_stmt, false);

            ASTNodeHelper::propogateSourceInfo(*new_identifier, node); // propagate the source info
            ASTNodeHelper::propogateSourceInfo(*new_id_expr, node); // propagate the source info
            ASTNodeHelper::propogateSourceInfo(*new_assignment_expr, node); // propagate the source info
            ASTNodeHelper::propogateSourceInfo(*new_expr_stmt, node); // propagate the source info

            node.initializer = NULL;
        };

        if(postpone)
            transforms.push_back(f);
        else
            f();
    }

    // transform static member variable initializer into an initialization statement in global constructor
    else if(node.is_static && isa<ClassDecl>(node.parent))
    {
        if(!ASTNodeHelper::hasNativeLinkage(&node))
        {
            auto f = [&](){
                ClassDecl& cls = *cast<ClassDecl>(node.parent);

                restructStaticOrGlobalVariableInit(node, cls.member_functions, L"__static_init", false, [&cls](FunctionDecl& new_init_function){
                    cls.addFunction(&new_init_function);
                });
            };

            if(postpone)
                transforms.push_back(f);
            else
                f();
        }
    }

    // transform global variable initializer into an initialization statement in global constructor
    else if(node.isGlobal())
    {
        if(!ASTNodeHelper::hasNativeLinkage(&node))
        {
            auto f = [&](){
                Source&     source      = *cast<Source>(node.parent);
                const auto& name_suffix = UUID::random().toString('_');

                restructStaticOrGlobalVariableInit(node, source.declares, L"__global_init_" + s_to_ws(name_suffix), true, [&source](FunctionDecl& new_init_function){
                    source.addDeclare(&new_init_function);
                });
            };

            if(postpone)
                transforms.push_back(f);
            else
                f();
        }
    }

    // transform all class member variable with initializer into a initialization statement in all constructors
    else if(!node.is_static && isa<ClassDecl>(node.parent))
    {
        ClassDecl* owner_class = node.getOwner<ClassDecl>();
        auto f = [&, owner_class](){
            for(tree::FunctionDecl* func: owner_class->member_functions)
            {
                BOOST_ASSERT(func && "null pointer exception");

                if(func->isConstructor())
                {
                    typedef boost::mpl::push_back<
                        tree::detail::ContextToCloneTypeList,
                        zillians::language::ResolvedSymbol
                    >::type CloneTypeList;
                    Identifier*     new_identifier      = ASTNodeHelper::clone(node.name);
                    Expression*     new_initializer     = ASTNodeHelper::clone<Expression, CloneTypeList>(node.initializer);
                    IdExpr*         new_id_expr         = new IdExpr(new_identifier);
                    BinaryExpr*     new_assignment_expr = new BinaryExpr(BinaryExpr::OpCode::ASSIGN, new_id_expr, new_initializer);
                    ExpressionStmt* new_expr_stmt       = new ExpressionStmt(new_assignment_expr);

                    setSplitReference(node, new_identifier, new_id_expr, new_assignment_expr, new_expr_stmt);
                    func->block->prependObject(new_expr_stmt);

                    ASTNodeHelper::propogateSourceInfo(*new_identifier, *func); // propagate the source info
                    ASTNodeHelper::propogateSourceInfo(*new_id_expr, *func); // propagate the source info
                    ASTNodeHelper::propogateSourceInfo(*new_assignment_expr, *func); // propagate the source info
                    ASTNodeHelper::propogateSourceInfo(*new_expr_stmt, *func); // propagate the source info
                }
            }
            node.initializer = NULL;
        };

        if(postpone)
            transforms.push_back(f);
        else
            f();
    }
}

std::array<tree::Statement*, 2> createVarWithInit(tree::VariableDecl& var, tree::Expression& value)
{
    NOT_NULL(var.name);

    auto*const init_lhs_name = var.name->clone();
    auto*const init_lhs      = new     IdExpr(init_lhs_name);
    auto*const init          = new BinaryExpr(BinaryExpr::OpCode::ASSIGN, init_lhs, &value);
    auto*const init_stmt     = new  ExpressionStmt(init);
    auto*const decl_stmt     = new DeclarativeStmt(&var);

    setSplitReference(var, init_lhs_name, init_lhs, init, init_stmt);

    ASTNodeHelper::tryPropogateSourceInfo(*init_lhs_name, value);
    ASTNodeHelper::tryPropogateSourceInfo(*init_lhs     , value);
    ASTNodeHelper::tryPropogateSourceInfo(*init         , value);
    ASTNodeHelper::tryPropogateSourceInfo(*init_stmt    , value);
    ASTNodeHelper::tryPropogateSourceInfo(*decl_stmt    , var  );

    return {decl_stmt, init_stmt};
}

bool restructureMultiType(BranchStmt& node, std::vector<std::function<void()>>& transforms)
{
    if (node.opcode != BranchStmt::OpCode::RETURN)
        return false;

    Expression* ret_expr = node.result;

    if (ret_expr == nullptr)
        return false;

    CallExpr* ret_expr_call = cast<CallExpr>(ret_expr);

    if (ret_expr_call == nullptr)
        return false;

    const Type* ret_type = ret_expr_call->getCanonicalType();

    if (ret_type == nullptr || !ret_type->isMultiType())
        return false;

    transforms.emplace_back(std::bind(UnpackCall(), std::ref(node), std::ref(*ret_expr_call)));

    return true;
}

bool restructureMultiType(BinaryExpr& node, std::vector<std::function<void()>>& transforms)
{
    Expression* lhs_expr = node.left ;
    Expression* rhs_expr = node.right;

    BOOST_ASSERT(lhs_expr && "null pointer exception");
    BOOST_ASSERT(rhs_expr && "null pointer exception");

    BlockExpr* lhs_expr_block = cast<BlockExpr>(lhs_expr);
     CallExpr* lhs_expr_call  = cast< CallExpr>(lhs_expr);
      TieExpr* lhs_expr_tie   = cast<  TieExpr>(lhs_expr);
    BlockExpr* rhs_expr_block = cast<BlockExpr>(rhs_expr);
     CallExpr* rhs_expr_call  = cast< CallExpr>(rhs_expr);
      TieExpr* rhs_expr_tie   = cast<  TieExpr>(rhs_expr);

#define MULTI_TYPE_ACTION_IMPL(r, _, action)                                          \
    if (BOOST_PP_CAT(lhs_expr_, BOOST_PP_SEQ_ELEM(0, action)) != nullptr &&           \
        BOOST_PP_CAT(rhs_expr_, BOOST_PP_SEQ_ELEM(1, action)) != nullptr)             \
    {                                                                                 \
        if (multi_type_helper::isMergable(                                            \
            *BOOST_PP_CAT(lhs_expr_, BOOST_PP_SEQ_ELEM(0, action)),                   \
            *BOOST_PP_CAT(rhs_expr_, BOOST_PP_SEQ_ELEM(1, action))))                  \
        {                                                                             \
            transforms.emplace_back(                                                  \
                std::bind(                                                            \
                    BOOST_PP_SEQ_ELEM(2, action)(),                                   \
                    std::ref(node),                                                   \
                    std::ref(*BOOST_PP_CAT(lhs_expr_, BOOST_PP_SEQ_ELEM(0, action))), \
                    std::ref(*BOOST_PP_CAT(rhs_expr_, BOOST_PP_SEQ_ELEM(1, action)))  \
                )                                                                     \
            );                                                                        \
            return true;                                                              \
        }                                                                             \
        else                                                                          \
        {                                                                             \
            return false;                                                             \
        }                                                                             \
    }                                                                                 \
    else
#define MULTI_TYPE_ACTION(actions)                                                    \
    BOOST_PP_SEQ_FOR_EACH(MULTI_TYPE_ACTION_IMPL, _, actions)                         \
    return false

    MULTI_TYPE_ACTION(
        ((block)(block)(MergeExpr ))
        ((block)(call )(UnpackCall))
        ((block)(tie  )(MergeExpr ))
        ((tie  )(block)(MergeExpr ))
        ((tie  )(call )(UnpackCall))
        ((tie  )(tie  )(MergeExpr ))
    );

#undef MULTI_TYPE_ACTION
#undef MULTI_TYPE_ACTION_IMPL
}

void restructureUnaryNewDirect(UnaryExpr& node)
{
    UUID uuid = UUID::random();
    std::wstring wstr_id = L"new_temp_" + s_to_ws(uuid.toString('_'));
    BlockExpr* block_expr = new BlockExpr(new NormalBlock(), "new_restructure");

    // the class name identifier can be a nested identifier or simple identifier or templated identifier
    // the identifier must be constructed from the RHS of new operator, which can be either a call expression or a primary expression or a member expression
    // for primary expression, the class name identifier can be just cloned from it
    // for member expression, the class name must be re-constructed even from nested member expression form
    // for call expression, the callee can be either a primary expression or yet another member expression, so it's a...recursive process
    // all in all, we have to convert nested member expression into nested identifier here
    Identifier* class_name_identifier = NULL;

    if(isa<CallExpr>(node.node))
    {
        class_name_identifier = buildIdentifierFromNewOperator(cast<CallExpr>(node.node)->node);
    }
    else if(isa<IdExpr>(node.node) || isa<MemberExpr>(node.node))
    {
        class_name_identifier = buildIdentifierFromNewOperator(node.node);
    }
    else
    {
        UNREACHABLE_CODE();
        return;
    }

    // var t:X;
    VariableDecl* t_decl = NULL;
    {
        // prepare variable t
        t_decl = new VariableDecl(
                new SimpleIdentifier(wstr_id),
                new NamedSpecifier(class_name_identifier->clone()),
                false, false, false,
                Declaration::VisibilitySpecifier::DEFAULT);

        block_expr->block->appendObject(new DeclarativeStmt(t_decl));
    }

    Type* t_decl_type = t_decl->getCanonicalType();

    // t = thor.lang.__createObject(size_literal, type_id_literal);
    {
        // prepare "t"
        IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id));
        ResolvedSymbol::set(t_use, t_decl);
        ResolvedType::set(t_use, t_decl_type);

        CallExpr* malloc_call = new CallExpr(ASTNodeHelper::useThorLangId(&node, new SimpleIdentifier(L"__createObject")));

        TypeIdLiteral* type_id_literal = new TypeIdLiteral(L"type_id_for_new");
        ResolvedSymbol::set(type_id_literal, t_decl);

        malloc_call->appendParameter(new NumericLiteral((int64)0)); // size
        malloc_call->appendParameter(type_id_literal); // type_id

        CastExpr* cast_expr = new CastExpr(malloc_call, new NamedSpecifier(class_name_identifier->clone()), CastExpr::CastMethod::STATIC);

        block_expr->block->appendObject(
                new ExpressionStmt(
                    new BinaryExpr(
                        BinaryExpr::OpCode::ASSIGN,
                        t_use,
                        cast_expr
                        )
                    )
                );
    }

    // t.new();
    {
        IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id));
        ResolvedSymbol::set(t_use, t_decl);
        ResolvedType::set(t_use, t_decl_type);

        CallExpr* constructor_call = new CallExpr(
                new MemberExpr(
                    t_use,
                    new SimpleIdentifier(L"new")
                    ));

        // append parameters
        if(isa<CallExpr>(node.node))
        {
            CallExpr* original_new_call_expr = cast<CallExpr>(node.node);
            for(auto& param : original_new_call_expr->parameters)
            {
                constructor_call->appendParameter(param);
            }
        }

        block_expr->block->appendObject(new ExpressionStmt(constructor_call));
    }

    // t;
    {
        IdExpr* t_use = new IdExpr(new SimpleIdentifier(wstr_id));
        ResolvedSymbol::set(t_use, t_decl);
        ResolvedType::set(t_use, t_decl_type);

        block_expr->block->appendObject(new ExpressionStmt(t_use));
    }

    if (node.get<stage::SourceInfoContext>())
    {
        ASTNodeHelper::foreachApply<ASTNode>(*block_expr, [&](ASTNode& n) {
                //setSplitReference(node, &n);
                ASTNodeHelper::propogateSourceInfo(n, node);
                });
    }

    node.parent->replaceUseWith(node, *block_expr);
}

void restructureUnaryNew(UnaryExpr& node, std::vector<std::function<void()>>& transforms)
{
    /*
       transform simple new operator to new function call
       for example: new X

        unary_expr(new)
              |
         primary_expr
              |
             "X"

       will be transformed into: { var t:X; t = thor.lang.__createObject(type_id_literal, size_literal); t.new(); t; }

                                                                         unary_expr(noop)
                                                                               |
                         --------------------------+----------------------block_expr------------------------+---------------------------------\
                        /                          |                                                        |                                  \
                   [0] /                       [1] |                                                    [2] |                               [3] \
                      /                            |                                                        |                                    \
                 decl_stmt                     expr_stmt                                                expr_stmt                             expr_stmt
                     |                             |                                                        |                                     |
               variable_decl                binary_expr("=")                                            call_expr                            primary_expr
              /            \               /                \                                          /                                          |
          id("t") type_specifier("t") primary_expr       call_expr                               member_expr                                   id("t")
                                           |            /         \                             /           \
                                        id("t")   member_expr  <empty parameter>          primary_expr   id("new")
                                                 /           \                                 |
                                             member_expr     templated_id("create<T>")      id("t")
                                            /           \             |               \
                                       member_expr    id("Object")   id("create")      \ (templated_type_list[0])
                                      /           \                                typename_decl
                                 primary_expr     id("lang")                       /           \
                                      |                                         id("T")         \ (specialized_type)
                                 id("thor")                                                      \
                                                                                             primary_expr
                                                                                                  |
                                                                                                 "X"

       for example: new X(a, b)
            unary_expr(new)
                  |
               call_expr
              /         +--------------------+
        primary_expr     \ (parameters[0])    \ (parameter[1])
             |            \                    \
          id("X")     primary_expr         primary_expr
                           |                    |
                        id("a")              id("b")

       will be transformed into: { var t:X; t = thor.lang.__createObject(type_id_literal, size_literal); t.new(a, b); t; }

                                                                         unary_expr(noop)
                                                                               |
                         --------------------------+----------------------block_expr------------------------+---------------------------------\
                        /                          |                                                        |                                  \
                   [0] /                       [1] |                                                    [2] |                               [3] \
                      /                            |                                                        |                                    \
                 decl_stmt                     expr_stmt                                                expr_stmt                             expr_stmt
                     |                             |                                                        |                                     |
               variable_decl                binary_expr("=")                                            call_expr                            primary_expr
              /            \               /                \                                          /         +-------------+                  |
          id("t") type_specifier("t") primary_expr       call_expr                               member_expr      \             \              id("t")
                                           |            /         \                             /           \      \ param[0]    \ param[1]
                                        id("t")   member_expr  <empty parameter>          primary_expr   id("new")  \             \
                                                 /           \                                 |                     \             \
                                             member_expr     templated_id("create<T>")      id("t")               primary_expr  primary_expr
                                            /           \             |               \                               |             |
                                       member_expr    id("Object")   id("create")      \ (templated_type_list[0])  id("a")       id("b")
                                      /           \                                typename_decl
                                 primary_expr     id("lang")                       /           \
                                      |                                         id("T")         \ (specialized_type)
                                 id("thor")                                                      \
                                                                                             primary_expr
                                                                                                  |
                                                                                                 "X"
    */

    if(node.opcode == UnaryExpr::OpCode::NEW)
    {
        transforms.push_back([&] {
            restructureUnaryNewDirect(node);
        });
    }
}

std::tuple<std::unordered_set<VariableDecl*>, ClassDecl*> getCaptureInfo(ASTNode& node, std::function<bool(tree::ASTNode&)> visit_condition/* = nullptr*/)
{
    std::unordered_set<VariableDecl*> captured_list;
    ClassDecl*                        captured_class = nullptr;

    ASTNodeHelper::foreachApply<ObjectLiteral>(
        node,
        [&](ObjectLiteral& literal)
        {
            if (!literal.isThisLiteral())
                return;

            captured_class = literal.getOwner<ClassDecl>();
            LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, L"Lambda restructure: detect capture class " << captured_class->name->toString());
        }
    );

    ASTNodeHelper::foreachApply<IdExpr>(
        node,
        [&](IdExpr& n)
        {
            // we only capture the variable in the outside scope, relative to current node
            // Note: We should not use n.getCanonicalType() since it will resolve
            // to TypeSpecifier instead of VariableDecl
            auto resolved = ResolvedSymbol::get(&n);
            if (!resolved || ASTNodeHelper::hasAncestor(resolved, &node)) return;

            if (isa<VariableDecl>(resolved))
            {
                auto resolved_variable = cast<VariableDecl>(resolved);

                // skip static member variable and global variable
                // Note: for local static variable, we have no way to resolve to it, so the local static
                //       variable will be copied into lambda class
                if (resolved_variable->is_static && resolved_variable->is_member) return;
                if (resolved_variable->isGlobal()) return;

                if (resolved_variable->is_member && resolved_variable->getOwner<ClassDecl>() == node.getOwner<ClassDecl>())
                {
                    BOOST_ASSERT((captured_class == NULL || captured_class == resolved_variable->getOwner<ClassDecl>()) && "How you access member variables from different classes");
                    captured_class = resolved_variable->getOwner<ClassDecl>();
                    LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, L"Lambda restructure: detect capture class " << captured_class->name->toString());
                }
                else
                {
                    const auto insertion_result = captured_list.insert(resolved_variable);

                    if (insertion_result.second)
                    {
                        LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, L"Lambda restructure: capture " << resolved_variable->name->toString());
                    }
                }
            }
            else if (isa<FunctionDecl>(resolved))
            {
                auto resolved_function = cast<FunctionDecl>(resolved);
                if (resolved_function->is_member && !resolved_function->is_static &&
                    (resolved_function->getOwner<ClassDecl>() == node.getOwner<ClassDecl>()))
                {
                    BOOST_ASSERT((captured_class == NULL || captured_class == resolved_function->getOwner<ClassDecl>()) && "How you access member functions from different classes");
                    captured_class = resolved_function->getOwner<ClassDecl>();
                    LOG4CXX_DEBUG(LoggerWrapper::TransformerStage, L"Lambda restructure: detect capture class " << captured_class->name->toString());
                }
            }
        },
        std::move(visit_condition)
    );

    return std::make_tuple(std::move(captured_list), captured_class);
}

void generateConstructor(ClassDecl& class_)
{
    SimpleIdentifier* name = new SimpleIdentifier(L"new");
    TypeSpecifier* type = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
    Block* block = (ASTNodeHelper::hasNativeLinkage(&class_)) ? NULL : new NormalBlock();

    FunctionDecl* default_constructor = new FunctionDecl(
        name,
        type,
        true, false, false, false,
        Declaration::VisibilitySpecifier::PUBLIC,
        block
    );

    class_.addFunction(default_constructor);

    if (class_.hasAnnotation(L"system")) default_constructor->addAnnotation(new Annotation(new SimpleIdentifier(L"system")));
    if (class_.hasAnnotation(L"native")) default_constructor->addAnnotation(new Annotation(new SimpleIdentifier(L"native")));

    ASTNodeHelper::propogateArchitecture(*default_constructor);
    ASTNodeHelper::propogateSourceInfoRecursively(*default_constructor, class_);
}

void generateCompleteDestructor(ClassDecl& class_)
{
    SimpleIdentifier* name = new SimpleIdentifier(L"delete");
    TypeSpecifier* type = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
    Block* block = (ASTNodeHelper::hasNativeLinkage(&class_)) ? NULL : new NormalBlock();

    FunctionDecl* complete_destructor = new FunctionDecl(
        name,
        type,
        true, false, false, false,
        Declaration::VisibilitySpecifier::PUBLIC,
        block
    );

    class_.addFunction(complete_destructor);

    if (class_.hasAnnotation(L"system")) complete_destructor->addAnnotation(new Annotation(new SimpleIdentifier(L"system")));
    if (class_.hasAnnotation(L"native")) complete_destructor->addAnnotation(new Annotation(new SimpleIdentifier(L"native")));

    ASTNodeHelper::propogateArchitecture(*complete_destructor);
    ASTNodeHelper::propogateSourceInfoRecursively(*complete_destructor, class_);
}

void tryGenerateDeleteDestructor(ClassDecl& class_)
{
    auto& member_functions = class_.member_functions;
    for(auto method = std::begin(member_functions); method != std::end(member_functions); ++method)
    {
        if ((*method)->isDestructor())
        {
            auto after_complete_destructor = method;
            ++after_complete_destructor;

            SimpleIdentifier* name = new SimpleIdentifier(L"__cxa_delete");
            TypeSpecifier* type = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
            Block* block = (ASTNodeHelper::hasNativeLinkage(&class_)) ? NULL : new NormalBlock();
            Annotations* annotations = new Annotations();

            FunctionDecl* delete_destructor = new FunctionDecl(
                    name,
                    type,
                    true, false, (*method)->is_virtual, false,
                    Declaration::VisibilitySpecifier::PUBLIC,
                    block);

            class_.insertFunction(after_complete_destructor, delete_destructor);

            if (class_.hasAnnotation(L"system")) delete_destructor->addAnnotation(new Annotation(new SimpleIdentifier(L"system")));
            if (class_.hasAnnotation(L"native")) delete_destructor->addAnnotation(new Annotation(new SimpleIdentifier(L"native")));

            ASTNodeHelper::propogateArchitecture(*delete_destructor);
            ASTNodeHelper::propogateSourceInfoRecursively(*delete_destructor, class_);
            break;
        }
    }
}

void tryGenerateSpecialMethods(ClassDecl& class_, std::vector<std::function<void()>>& actions)
{
    // determine whether the custom "ctor", "complete dtor", "delete dtor" exsit
    auto is_constructor = std::mem_fn(&FunctionDecl::isConstructor);
    auto is_complete_destructor = std::mem_fn(&FunctionDecl::isDestructor);
    auto is_delete_destructor = [](FunctionDecl* method){
        return method->name->toString() == L"__cxa_delete";
    };

    bool has_constructor = false, has_complete_destructor = false, has_delete_destructor = false;
    for(auto* method : class_.member_functions)
    {
        if     (is_constructor(method)        ) has_constructor         = true;
        else if(is_complete_destructor(method)) has_complete_destructor = true;
        else if(is_delete_destructor(method)  ) has_delete_destructor   = true;
    }


    // if there's no constructor, create a new default constructor for it
    if(!has_constructor)
    {
        actions.emplace_back([&](){
            generateConstructor(class_);
        });
    }

    if(!has_complete_destructor)
    {
        actions.emplace_back([&](){
            generateCompleteDestructor(class_);
        });
    }

    if(!has_delete_destructor)
    {
        actions.emplace_back([&](){
            tryGenerateDeleteDestructor(class_);
        });
    }
}


void tryGenerateSpecialMethods(ClassDecl& class_)
{
    std::vector<std::function<void()>> actions;
    tryGenerateSpecialMethods(class_, actions);
    for(auto& action : actions)
        action();
}

} } } } } // namespace zillians::language::stage::visitor::restructure_helper
