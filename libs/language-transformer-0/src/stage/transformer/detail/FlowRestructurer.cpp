/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstddef> // std::nullptr_t

#include <deque>
#include <functional>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/numeric.hpp>

#include "utility/RangeUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/Architecture.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/Block.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/IdExpr.h"
#include "language/tree/expression/MemberExpr.h"
#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/statement/SelectionStmt.h"

#include "language/stage/transformer/detail/AsyncHelper.h"
#include "language/stage/transformer/detail/FlowSpliter.h"
#include "language/stage/transformer/detail/FlowRestructurer.h"

namespace zillians { namespace language { namespace stage { namespace detail {

using block_t = visitor::flow_spliter_detail::block_t;
using  link_t = visitor::flow_spliter_detail:: link_t;
using graph_t = visitor::flow_spliter_detail::graph_t;

using ContextCloneList = boost::mpl::joint_view<
    tree::detail::ContextToCloneTypeList,
    boost::mpl::vector<
        zillians::language::ResolvedType,
        zillians::language::ResolvedSymbol
    >
>;

namespace {

std::set<block_t> get_flowed_blocks(const graph_t& graph)
{
    using boost::adaptors::filtered;
    using boost::adaptors::transformed;

    std::set<block_t> flowed_blocks;

    for (const auto& block : boost::make_iterator_range(boost::vertices(graph)))
        if (graph[block].transition == visitor::flow_spliter_detail::TRANS_MERGE)
            flowed_blocks.emplace(block);

    return std::move(flowed_blocks);
}

tree::Statement* get_flowed_statement(const graph_t& graph, const link_t& link)
{
    const auto& block      = boost::target(link, graph);
    const auto& block_info = graph[block];
    const auto& stmts      = block_info.statements;

    BOOST_ASSERT(stmts.size() == 1 && "flowed basic block should contains exactly one statement");

    return stmts.front();
}

block_t get_merge_target(const graph_t& graph, const block_t& flow_begin)
{
    BOOST_ASSERT(graph[flow_begin].transition == visitor::flow_spliter_detail::TRANS_FLOW && "unexpected transition");
    BOOST_ASSERT(boost::out_degree(flow_begin, graph) > 0 && "there must be at least one sub statement for flow");

    const auto& sub_flow_link  = *boost::begin(boost::out_edges(flow_begin, graph));
    const auto& sub_flow_block = boost::target(sub_flow_link, graph);

    BOOST_ASSERT(graph[sub_flow_block].transition == visitor::flow_spliter_detail::TRANS_MERGE && "unexpected transition");
    BOOST_ASSERT(boost::out_degree(sub_flow_block, graph) > 0 && "there must be at least link after flow");

    const auto& merge_link  = *boost::begin(boost::out_edges(sub_flow_block, graph));
    const auto& merge_block = boost::target(merge_link, graph);

    return merge_block;
}

template<typename node_type>
node_type* propogate_source_info_recursively(node_type* node, const tree::ASTNode& ref_node)
{
    NOT_NULL(node);

    tree::ASTNodeHelper::propogateSourceInfoRecursively(*node, ref_node);

    return node;
}

class persister_context
{
public:
             persister_context();
    explicit persister_context(const graph_t& graph);

    tree::ClassDecl* get_decl() const;

    tree::Identifier*    create_decl_name() const;
    tree::TypeSpecifier* create_decl_ts() const;
    tree::Identifier*    create_var_id() const;
    tree::VariableDecl*  create_var_decl() const;
    tree::Expression*    create_var_ref() const;

private:
    static tree::VariableDecl* generate_member_var(const tree::VariableDecl& used_var);

private:
    tree::ClassDecl*          persister_decl      = nullptr;
    const tree::Identifier*   persister_decl_name = nullptr;
    const tree::Identifier*   persister_uid       = tree::Identifier::randomId(L"persisted_");
};

class adaptor_builder
{
public:
    void build(const tree::FunctionDecl& the_origin_func, const graph_t& graph);

protected:
    using defer_actions_container = std::vector<std::function<void()>>;
    using block_decl_map          = std::map<block_t, tree::FunctionDecl*>;

    void prepare_state(const tree::FunctionDecl& the_origin_func, const graph_t& graph);

    std::pair<block_decl_map, tree::Block*> create_adaptor_skeletons(const graph_t& graph) const;
    block_decl_map::value_type              create_adaptor_skeleton_impl(const std::wstring& name_prefix, unsigned& serial, const block_t& block) const;
    void                                    append_adaptor_parameters(tree::FunctionDecl& decl) const;

    tree::Block* create_entry_linker(const tree::FunctionDecl& origin_func, const graph_t& graph, const block_t& entry_block, tree::FunctionDecl& entry_decl) const;

    std::deque<defer_actions_container::value_type> build_link(const graph_t& graph) const;
    defer_actions_container                         build_link_case   (const graph_t& graph, const block_t& source_block) const;
    defer_actions_container                         build_link_cond   (const graph_t& graph, const block_t& source_block) const;
    defer_actions_container                         build_link_flow   (const graph_t& graph, const block_t& source_block) const;
    defer_actions_container                         build_link_merge  (const graph_t& graph, const block_t& source_block) const;
    defer_actions_container                         build_link_normal (const graph_t& graph, const block_t& source_block) const;
    defer_actions_container                         build_link_null   (const graph_t& graph, const block_t& source_block) const;
    defer_actions_container                         build_link_unknown(const graph_t& graph, const block_t& source_block) const;

    template<typename async_id_var_container>
    tree::Statement* build_link_flow_impl_post_action(tree::Block& source_decl_block, const graph_t& graph, const block_t& flow_begin, const std::wstring& name_suffix, const async_id_var_container& async_id_vars) const;
    template<typename async_id_var_container>
    tree::Statement* build_link_flow_impl_depend_on_count(const tree::Identifier& post_async_id, const async_id_var_container& async_id_vars) const;
    tree::Statement* build_link_flow_impl_depend_on(const tree::Identifier& post_async_id, const tree::VariableDecl& async_id_var) const;
    tree::Statement* build_link_flow_impl_postpone_dependency_to(const tree::Identifier& post_async_id) const;

    defer_actions_container collect_cleanups() const;
    void                    propogate_info() const;
    void                    push_statements(const graph_t& graph) const;
    void                    finish_adaptors();
    void                    add_declarations() const;

    void push_statement_impl(const graph_t& graph, tree::FunctionDecl& decl, const block_t& block) const;

    void add_declaration_impl(tree::ClassDecl& decl) const;
    void add_declaration_impl(tree::FunctionDecl& decl) const;

    tree::Statement* create_dummy_return() const;

    template<typename source_type, typename defers_container_type, typename... var_name_arg_types>
    tree::VariableDecl* create_async_call(source_type& target_or_stmt, tree::Block& insert_to, defers_container_type&& defer_actions, var_name_arg_types&&... var_name_args) const;

    std::wstring create_async_id_var_name(const tree::Statement& origin_stmt , const std::wstring& suffix, unsigned& serial) const;
    std::wstring create_async_id_var_name(const block_t&         target_block, const std::wstring& suffix) const;

    std::pair<tree::VariableDecl*, tree::AsyncBlock*> create_async_call_skeleton(tree::Block& insert_to, const std::wstring& var_name) const;

    template<typename skeleton_type> void fill_async_call_skeleton(const block_t&         target_block, const skeleton_type& skeleton, std::nullptr_t                        ) const;
    template<typename skeleton_type> void fill_async_call_skeleton(      tree::Statement& origin_stmt , const skeleton_type& skeleton, defer_actions_container& defer_actions) const;

    tree::Block* create_normal_call(const block_t& target_block, const std::set<tree::VariableDecl*>& passed_vars) const;
    tree::Block* create_normal_call(const block_t* target_block, const std::set<tree::VariableDecl*>* passed_vars) const;
    tree::Block* create_normal_call(const block_t& target_block, const std::set<tree::VariableDecl*>& passed_vars, tree::Block& insert_to) const;

    tree::Statement* create_var_loader(const tree::VariableDecl& used_var) const;
    tree::Statement* create_var_storer(const tree::VariableDecl& used_var) const;

    void append_call_parameters(tree::CallExpr& call_expr, const block_t& target_block) const;

protected:
    const tree::FunctionDecl* origin_func         = nullptr;
    bool                      as_member           = false  ;
    bool                      as_static           = false  ;
    tree::Block*              entry_linker        = nullptr;
    block_decl_map            decl_mapping        = {}     ;
    persister_context         persister           = {};
};

persister_context::persister_context()
    : persister_decl(nullptr)
    , persister_decl_name(nullptr)
    , persister_uid(tree::Identifier::randomId(L"persisted_"))
{
}

persister_context::persister_context(const graph_t& graph)
    : persister_context()
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    std::set<const tree::VariableDecl*> used_vars;

    boost::for_each(
        boost::vertices(graph),
        [&graph, &used_vars](const block_t& block)
        {
            const auto& new_used_vars = graph[block].used_variables;

            used_vars.insert(boost::begin(new_used_vars), boost::end(new_used_vars));
        }
    );

    auto*const cls_name = tree::Identifier::randomId(L"persister_");
    auto*const cls_decl = new tree::ClassDecl(cls_name);

    for (tree::VariableDecl*const persisted_var_decl : used_vars | indirected | transformed(&persister_context::generate_member_var))
        cls_decl->addVariable(persisted_var_decl);

    persister_decl_name = cls_name;
    persister_decl      = cls_decl;
}

tree::ClassDecl* persister_context::get_decl() const
{
    return persister_decl;
}

tree::Identifier* persister_context::create_decl_name() const
{
    NOT_NULL(persister_decl_name);

    return persister_decl_name->clone();
}

tree::TypeSpecifier* persister_context::create_decl_ts() const
{
    NOT_NULL(persister_decl_name);

    auto*const persister_ts = new tree::NamedSpecifier(create_decl_name());

    return persister_ts;
}

tree::Identifier* persister_context::create_var_id() const
{
    NOT_NULL(persister_uid);

    return persister_uid->clone();
}

tree::VariableDecl* persister_context::create_var_decl() const
{
    auto*const persister_ts    = create_decl_ts();
    auto*const persister_param =  tree::VariableDecl::Creator()
                                 .name(create_var_id())
                                 .type(persister_ts)
                                 .create();

    return persister_param;
}

tree::Expression* persister_context::create_var_ref() const
{
    auto*const ref_expr = new tree::IdExpr(create_var_id());

    return ref_expr;
}

tree::VariableDecl* persister_context::generate_member_var(const tree::VariableDecl& used_var)
{
    const tree::Type*const used_var_type = used_var.getCanonicalType();
    BOOST_ASSERT(used_var_type != nullptr && "canonical type is not ready");

    auto*const persisted_var_decl =  tree::VariableDecl::Creator()
                                    .name(used_var.name->clone())
                                    .type(tree::ASTNodeHelper::createTypeSpecifierFrom(used_var_type))
                                    .visibility(tree::VariableDecl::VisibilitySpecifier::PUBLIC)
                                    .member()
                                    .create();
    ;

    return persisted_var_decl;
}

void adaptor_builder::build(const tree::FunctionDecl& the_origin_func, const graph_t& graph)
{
    using boost::adaptors::map_keys;
    using boost::adaptors::map_values;
    using boost::adaptors::indirected;

    prepare_state(the_origin_func, graph);

    auto defer_actions = build_link(graph);

    boost::push_back(defer_actions, collect_cleanups());

    propogate_info();
    push_statements(graph);
    finish_adaptors();
    add_declarations();

    for (const auto& defer_action : defer_actions)
        defer_action();
}

void adaptor_builder::prepare_state(const tree::FunctionDecl& the_origin_func, const graph_t& graph)
{
    persister   = persister_context(graph);
    origin_func = &the_origin_func;
    as_member   = the_origin_func.is_member;
    as_static   = !graph[boost::graph_bundle].need_this;

    std::tie(decl_mapping, entry_linker) = create_adaptor_skeletons(graph);
}

auto adaptor_builder::create_adaptor_skeletons(const graph_t& graph) const -> std::pair<block_decl_map, tree::Block*>
{
    using boost::adaptors::filtered;
    using boost::adaptors::transformed;

    NOT_NULL(origin_func      );
    NOT_NULL(origin_func->name);

    const auto*const sid = origin_func->name->getSimpleId();

    BOOST_ASSERT(sid != nullptr && "null pointer exception");

    const auto& flowed_blocks = get_flowed_blocks(graph);
    const auto& name_prefix   = sid->name + L'_';
    unsigned    serial        = 0;

    const auto& is_normal_block = [&flowed_blocks](const block_t& block)
                                  {
                                      return flowed_blocks.count(block) == 0;
                                  };

    const auto& blocks     = boost::vertices(graph);
    const auto& info_range =   blocks
                             | filtered(is_normal_block)
                             | transformed(
                                   std::bind(
                                       &adaptor_builder::create_adaptor_skeleton_impl,
                                       this,
                                       std::cref(name_prefix),
                                       std::ref(serial),
                                       std::placeholders::_1
                                   )
                               );

    block_decl_map mapping(boost::begin(info_range), boost::end(info_range));

    const auto& entry_block  = *blocks.first;
          auto& entry_decl   = *mapping.at(entry_block);
          auto* entry_linker = create_entry_linker(*origin_func, graph, entry_block, entry_decl);

    return {std::move(mapping), std::move(entry_linker)};
}

auto adaptor_builder::create_adaptor_skeleton_impl(const std::wstring& name_prefix, unsigned& serial, const block_t& block) const -> block_decl_map::value_type
{
    auto creator =  tree::FunctionDecl::Creator()
                   .name(tree::Identifier::randomId(name_prefix + std::to_wstring(serial++) + L'_'))
                   .type(origin_func->type->clone())
                   .block(new tree::NormalBlock)
                   .member(as_member)
                   .static_(as_member && as_static);

    auto*const decl = creator.create();

    append_adaptor_parameters(*decl);

    return {block, decl};
}

void adaptor_builder::append_adaptor_parameters(tree::FunctionDecl& decl) const
{
    auto*const persister_var = persister.create_var_decl();

    decl.appendParameter(persister_var);
}

tree::Block* adaptor_builder::create_entry_linker(const tree::FunctionDecl& origin_func, const graph_t& graph, const block_t& entry_block, tree::FunctionDecl& entry_decl) const
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    NOT_NULL(entry_decl.name);

    auto*const linker_block   = new tree::NormalBlock();

    auto*const persister_ts   = persister.create_decl_ts();
    auto*const persister_init = tree::UnaryExpr::create<'new'>(
                                    new tree::CallExpr(
                                        new tree::IdExpr(
                                            persister.create_decl_name()
                                        )
                                    )
                                );
    auto*const persister_id   = persister.create_var_id();
    auto*const persister_var  =  tree::VariableDecl::Creator()
                                .name(persister_id)
                                .type(persister_ts)
                                .init(persister_init)
                                .const_()
                                .create();
    auto*const persister_stmt = new tree::DeclarativeStmt(persister_var);

    auto*const call_expr      = new tree::CallExpr(
                                    new tree::IdExpr(
                                        entry_decl.name->clone()
                                    ),
                                    {
                                        new tree::IdExpr(persister_id->clone())
                                    }
                                );
    auto*const call_ret       = new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN, call_expr);

    linker_block->appendObject(persister_stmt);

    {
        const auto& used_vars = graph[entry_block].used_variables;

        for (const auto& parameter : origin_func.parameters | indirected)
        {
            // FIXME used_vars's value type should be pointer points to const value
            if (used_vars.count(const_cast<tree::VariableDecl*>(&parameter)) == 0)
                continue;

            NOT_NULL(parameter.name);

            auto*const set_expr = tree::BinaryExpr::create<'='>(
                                      new tree::MemberExpr(
                                          new tree::IdExpr(persister_id->clone()),
                                          parameter.name->clone()
                                      ),
                                      new tree::IdExpr(parameter.name->clone())
                                  );
            auto*const set_stmt = new tree::ExpressionStmt(set_expr);

            linker_block->appendObject(set_stmt);
        }
    }

    linker_block->appendObject(call_ret);

    return linker_block;
}

auto adaptor_builder::build_link(const graph_t& graph) const -> std::deque<defer_actions_container::value_type>
{
    using boost::adaptors::map_keys;

    std::deque<defer_actions_container::value_type> defer_actions;

    for (const auto& block : decl_mapping | map_keys)
    {
        const auto& transition = graph[block].transition;

        switch (transition)
        {
        case visitor::flow_spliter_detail::TRANS_CASE     : boost::push_back(defer_actions, build_link_case   (graph, block)); break;
        case visitor::flow_spliter_detail::TRANS_CONDITION: boost::push_back(defer_actions, build_link_cond   (graph, block)); break;
        case visitor::flow_spliter_detail::TRANS_FLOW     : boost::push_back(defer_actions, build_link_flow   (graph, block)); break;
        case visitor::flow_spliter_detail::TRANS_MERGE    : boost::push_back(defer_actions, build_link_merge  (graph, block)); break;
        case visitor::flow_spliter_detail::TRANS_NORMAL   : boost::push_back(defer_actions, build_link_normal (graph, block)); break;
        case visitor::flow_spliter_detail::TRANS_NULL     : boost::push_back(defer_actions, build_link_null   (graph, block)); break;
        case visitor::flow_spliter_detail::TRANS_UNKNOWN  : boost::push_back(defer_actions, build_link_unknown(graph, block)); break;
        default                                           : BOOST_ASSERT(false && "unexpected transition");                    break;
        }
    }

    return defer_actions;
}

auto adaptor_builder::build_link_case(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    BOOST_ASSERT(decl_mapping.count(source_block) && "no corresponding declaration of source block");

    const tree::FunctionDecl& source_decl      = *decl_mapping.at(source_block);
    tree::Expression*const    real_switch_expr = graph[source_block].cond_expr;

    NOT_NULL(source_decl.block);
    NOT_NULL(real_switch_expr );

    tree::Block&            source_decl_block = *source_decl.block;
    defer_actions_container defer_actions;

    // switch (0 /* dummy, replaced later */)
    // {
    //     ...
    // }
    tree::Expression*const switch_expr = new tree::NumericLiteral(0);
    tree::SwitchStmt*const switch_stmt = new tree::SwitchStmt(switch_expr);

    defer_actions.emplace_back([switch_expr, real_switch_expr] { switch_expr->parent->replaceUseWith(*switch_expr, *real_switch_expr); });

    for (const auto& link : boost::make_iterator_range(boost::out_edges(source_block, graph)))
    {
              auto*const real_case_expr  = graph[link].cond_expr;
        const auto&           case_block = boost::target(link, graph);
        const auto&           pass_vars  = graph[case_block].used_variables;

        if (real_case_expr != nullptr)
        {
            // case <case-expr>:
            //     flow_adaptor_N1(...);
            auto*const case_expr = new tree::NumericLiteral(0);
            auto*const case_sel  = new tree::Selection(
                                       case_expr,
                                       create_normal_call(case_block, pass_vars)
                                   );

            defer_actions.emplace_back([case_expr, real_case_expr]{ case_expr->parent->replaceUseWith(*case_expr, *real_case_expr); });
            switch_stmt->addCase(case_sel);
        }
        else
        {
            // default:
            //     flow_adaptor_N2(...);

            auto*const default_block = create_normal_call(case_block, pass_vars);

            switch_stmt->setDefaultCase(default_block);
        }
    }

    source_decl_block.appendObject(switch_stmt);

    return defer_actions;
}

auto adaptor_builder::build_link_cond(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    BOOST_ASSERT(decl_mapping.count(source_block) && "no corresponding declaration of source block");
    BOOST_ASSERT(boost::out_degree(source_block, graph) == 2 && "unexpected size of pass_vars_set");

    const auto& source_info = graph[source_block];
    const auto& source_decl = *decl_mapping.at(source_block);
    const auto& links       = boost::out_edges(source_block, graph);

    const auto&  true_block = boost::target(*boost::begin(links)           , graph);
    const auto& false_block = boost::target(*std::next(boost::begin(links)), graph);

    const auto&  true_info = graph[ true_block];
    const auto& false_info = graph[false_block];

    tree::Block& source_decl_block = *source_decl.block;

    // if (true /* dummy, replaced later */)
    //     flow_adaptor_N1(...);
    // else /* optional */
    //     flow_adaptor_N2(...);
    tree::Expression*const real_cond_expr = source_info.cond_expr;
    tree::Expression*const      cond_expr = new tree::NumericLiteral(false);
    tree::IfElseStmt*const      cond_stmt = new tree::IfElseStmt(
                                                new tree::Selection(
                                                    cond_expr,
                                                    create_normal_call(true_block, true_info.used_variables)
                                                ),
                                                create_normal_call(false_block, false_info.used_variables)
                                            );

    source_decl_block.appendObject(cond_stmt);

    return {
        [real_cond_expr, cond_expr]
        {
            cond_expr->parent->replaceUseWith(*cond_expr, *real_cond_expr);
        }
    };
}

auto adaptor_builder::build_link_flow(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    BOOST_ASSERT(decl_mapping.count(source_block) && "no corresponding declaration of source block");

    const tree::FunctionDecl& source_decl       = *decl_mapping.at(source_block);
    tree::Block&              source_decl_block = *source_decl.block;

    defer_actions_container defer_actions;
    unsigned                serial          = 0;
    const auto&             name_suffix     = L"_" + s_to_ws(UUID::random().toString('_'));
    const auto&             async_id_range  = boost::out_edges(source_block, graph)
                                            | transformed(std::bind(&get_flowed_statement, std::cref(graph), std::placeholders::_1))
                                            | indirected
                                            | transformed(
                                                  std::bind(
                                                      &adaptor_builder::create_async_call<
                                                          tree::Statement,
                                                          defer_actions_container&,
                                                          const std::wstring&,
                                                          unsigned&
                                                      >,
                                                      this,
                                                      std::placeholders::_1,
                                                      std::ref(source_decl_block),
                                                      std::ref(defer_actions),
                                                      std::cref(name_suffix),
                                                      std::ref(serial)
                                                  )
                                              );

    std::vector<tree::VariableDecl*> async_id_vars{boost::begin(async_id_range), boost::end(async_id_range)};
    auto*const                       post_async_stmt = build_link_flow_impl_post_action(source_decl_block, graph, source_block, name_suffix, async_id_vars);

    {
        std::set<tree::VariableDecl*> pass_vars;

        for (const auto& flowed_link : boost::make_iterator_range(boost::out_edges(source_block, graph)))
        {
            const auto& flowed_block = boost::target(flowed_link, graph);

            for (auto*const each_pass_var : graph[flowed_block].used_variables)
            {
                const auto& insertion_result = pass_vars.emplace(each_pass_var);
                const auto& is_inserted      = insertion_result.second;

                if (!is_inserted)
                    continue;

                source_decl_block.appendObject(create_var_storer(*each_pass_var));
            }
        }
    }

    return defer_actions;
}

auto adaptor_builder::build_link_merge(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    // For merge transition, new implementation should use invocation dependency between asyncs in 'build_link_flow'

    return {};
}

auto adaptor_builder::build_link_normal(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    BOOST_ASSERT(decl_mapping.count(source_block) && "no corresponding declaration of source block");
    BOOST_ASSERT(boost::out_degree(source_block, graph) == 1 && "normal transition should have exactly one link");

    const auto& source_decl  = *decl_mapping.at(source_block);
    const auto& target_link  = *boost::out_edges(source_block, graph).first;
    const auto& target_block = boost::target(target_link, graph);
    const auto& target_info  = graph[target_block];

    NOT_NULL(source_decl.block);

    tree::Block& source_decl_block = *source_decl.block;

    create_normal_call(target_block, target_info.used_variables, source_decl_block);

    return {};
}

auto adaptor_builder::build_link_null(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    BOOST_ASSERT(decl_mapping.count(source_block) && "no corresponding declaration of source block");

    auto*const dummy_return = create_dummy_return();

    if (dummy_return == nullptr)
        return {};

    const tree::FunctionDecl& source_decl = *decl_mapping.at(source_block);

    NOT_NULL(source_decl.block);

    tree::Block& source_decl_block = *source_decl.block;

    source_decl_block.appendObject(dummy_return);

    return {};
}

auto adaptor_builder::build_link_unknown(const graph_t& graph, const block_t& source_block) const -> defer_actions_container
{
    UNREACHABLE_CODE();

    return {};
}

template<typename async_id_var_container>
tree::Statement* adaptor_builder::build_link_flow_impl_post_action(tree::Block& source_decl_block, const graph_t& graph, const block_t& flow_begin, const std::wstring& name_suffix, const async_id_var_container& async_id_vars) const
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    const auto& merge_block = get_merge_target(graph, flow_begin);

    auto*const post_async_cond   = boost::accumulate(
                                       async_id_vars,
                                       static_cast<tree::Expression*>(new tree::NumericLiteral(true)),
                                       [](tree::Expression* lhs, tree::VariableDecl* async_id_var)
                                       {
                                           NOT_NULL(lhs);
                                           NOT_NULL(async_id_var);
                                           NOT_NULL(async_id_var->name);

                                           auto*const async_id_cloned = async_id_var->name->clone();

                                           return tree::BinaryExpr::create<'&&'>(
                                               lhs,
                                               tree::BinaryExpr::create<'>='>(
                                                   new tree::IdExpr(async_id_cloned),
                                                   new tree::NumericLiteral(tree::PrimitiveKind::INT64_TYPE, 0)
                                               )
                                           );
                                       }
                                   );
    auto*const post_async_block  = new tree::NormalBlock;
    auto*const post_async_stmt   = new tree::IfElseStmt(new tree::Selection(post_async_cond, post_async_block));

    auto*const post_async_id_var = create_async_call(merge_block, *post_async_block, nullptr, name_suffix);
    auto*const post_async_id     = post_async_id_var->name;

    NOT_NULL(post_async_id);

    auto*const post_async_action_cond  = tree::BinaryExpr::create<'>='>(
                                             new tree::IdExpr(post_async_id->clone()),
                                             new tree::NumericLiteral(tree::PrimitiveKind::INT64_TYPE, 0)
                                         );
    auto*const post_async_action_block = new tree::NormalBlock;
    auto*const post_async_action_stmt  = new tree::IfElseStmt(new tree::Selection(post_async_action_cond, post_async_action_block));

    auto*const post_async_dummy_return = create_dummy_return();

    post_async_action_block->appendObject(build_link_flow_impl_depend_on_count(*post_async_id, async_id_vars));
    post_async_action_block->appendObjects(
          async_id_vars
        | indirected
        | transformed(
              std::bind(
                  &adaptor_builder::build_link_flow_impl_depend_on,
                  this,
                  std::cref(*post_async_id),
                  std::placeholders::_1
              )
          )
    );
    post_async_action_block->appendObject(build_link_flow_impl_postpone_dependency_to(*post_async_id));

    post_async_block->appendObject(post_async_action_stmt);

    source_decl_block.appendObject(post_async_stmt);

    if (post_async_dummy_return != nullptr)
        source_decl_block.appendObject(post_async_dummy_return);

    return post_async_stmt;
}

template<typename async_id_var_container>
tree::Statement* adaptor_builder::build_link_flow_impl_depend_on_count(const tree::Identifier& post_async_id, const async_id_var_container& async_id_vars) const
{
    auto*const setter = new tree::ExpressionStmt(
                            new tree::CallExpr(
                                tree::ASTNodeHelper::useThorLangId(origin_func, new tree::SimpleIdentifier(L"__setInvocationDependOnCount")),
                                {
                                    new tree::IdExpr(post_async_id.clone()),
                                    new tree::NumericLiteral(tree::PrimitiveKind::INT32_TYPE, async_id_vars.size())
                                }
                            )
                        );

    return setter;
}

tree::Statement* adaptor_builder::build_link_flow_impl_depend_on(const tree::Identifier& post_async_id, const tree::VariableDecl& async_id_var) const
{
    NOT_NULL(async_id_var.name);

    auto*const setter = new tree::ExpressionStmt(
                            new tree::CallExpr(
                                tree::ASTNodeHelper::useThorLangId(origin_func, new tree::SimpleIdentifier(L"__setInvocationDependOn")),
                                {
                                    new tree::IdExpr(post_async_id.clone()),
                                    new tree::IdExpr(async_id_var.name->clone())
                                }
                            )
                        );

    return setter;
}

tree::Statement* adaptor_builder::build_link_flow_impl_postpone_dependency_to(const tree::Identifier& post_async_id) const
{
    auto*const setter = new tree::ExpressionStmt(
                            new tree::CallExpr(
                                tree::ASTNodeHelper::useThorLangId(origin_func, new tree::SimpleIdentifier(L"__postponeInvocationDependencyTo")),
                                {
                                    new tree::IdExpr(post_async_id.clone())
                                }
                            )
                        );

    return setter;
}

auto adaptor_builder::collect_cleanups() const -> defer_actions_container
{
    NOT_NULL(origin_func       );
    NOT_NULL(origin_func->block);

    std::vector<tree::ASTNode*> cleanup_nodes;

    tree::ASTNodeHelper::foreachApply<tree::ASTNode>(
        *origin_func->block,
        [&cleanup_nodes](tree::ASTNode& node)
        {
            // resolved symbol of ID literals should not be effected
            if (tree::isa<tree::IdLiteral>(node))
                return;

            const auto*const var_symbol = tree::cast_or_null<tree::VariableDecl>(ResolvedSymbol::get(&node));

            if (var_symbol == nullptr)
                return;

            // possibilities of VariableDecl
            // 1. Glboal     -> must not be effected -> no cleanup
            // 2. Enumerator -> must not be effected -> no cleanup
            // 3. Member     -> must not be effected -> no cleanup
            // 4. Local      -> may      be effected ->    cleanup
            // 5. Parameter  -> may      be effected ->    cleanup
            if (var_symbol->isGlobal() || var_symbol->isEnumerator() || var_symbol->is_member)
                return;

            cleanup_nodes.emplace_back(&node);
        }
    );

    if (cleanup_nodes.empty())
        return {};

    return {
        [cleanup_nodes]()
        {
            for (tree::ASTNode*const node : cleanup_nodes)
            {
                NOT_NULL(node);

                ResolvedSymbol::set(node, nullptr);
                ResolvedType  ::set(node, nullptr);
            }
        }
    };
}

void adaptor_builder::propogate_info() const
{
    using boost::adaptors::indirected;
    using boost::adaptors::map_values;

    NOT_NULL(origin_func );
    NOT_NULL(entry_linker);

    tree::ASTNodeHelper::propogateSourceInfoRecursively(*entry_linker, *origin_func);

    for (tree::FunctionDecl& decl : decl_mapping | map_values | indirected)
        tree::ASTNodeHelper::propogateSourceInfoRecursively(decl, *origin_func);

    if (origin_func->arch != Architecture::zero())
        for (tree::FunctionDecl& decl : decl_mapping | map_values | indirected)
            decl.arch = origin_func->arch;

    auto*const persister_decl = persister.get_decl();

    tree::ASTNodeHelper::propogateSourceInfoRecursively(*persister_decl, *origin_func);
    persister_decl->arch = origin_func->getActualArch();
}

void adaptor_builder::push_statements(const graph_t& graph) const
{
    for (const auto& mapping : decl_mapping)
    {
        const auto&              block = std::get<0>(mapping);
        tree::FunctionDecl*const decl  = std::get<1>(mapping);

        NOT_NULL(decl);

        push_statement_impl(graph, *decl, block);
    }
}

void adaptor_builder::finish_adaptors()
{
    NOT_NULL(origin_func               );
    NOT_NULL(origin_func->block        );
    NOT_NULL(origin_func->block->parent);
    NOT_NULL(entry_linker              );

    auto& origin_block = *origin_func->block;

    origin_block.parent->replaceUseWith(origin_block, *entry_linker);
}

void adaptor_builder::add_declarations() const
{
    using boost::adaptors::indirected;
    using boost::adaptors::map_values;

    boost::for_each(
          decl_mapping
        | map_values
        | indirected,
        [this](tree::FunctionDecl& decl)
        {
            add_declaration_impl(decl);
        }
    );

    add_declaration_impl(*persister.get_decl());
}

void adaptor_builder::push_statement_impl(const graph_t& graph, tree::FunctionDecl& decl, const block_t& block) const
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    tree::Block*const decl_block = decl.block;

    NOT_NULL(decl_block);

    decl_block->prependObjects(graph[block].statements);
    decl_block->prependObjects(
          graph[block].used_variables
        | indirected
        | transformed(
              std::bind(
                  &adaptor_builder::create_var_loader,
                  this,
                  std::placeholders::_1
              )
          )
        | transformed(
              std::bind(
                  &propogate_source_info_recursively<tree::Statement>,
                  std::placeholders::_1,
                  std::cref(*origin_func)
              )
          )
    );
}

void adaptor_builder::add_declaration_impl(tree::ClassDecl& decl) const
{
    NOT_NULL(origin_func);

    auto*const src = origin_func->getOwner<tree::Source>();

    BOOST_ASSERT(src != nullptr && "function declaration is not under any source");

    src->addDeclare(&decl);
}

void adaptor_builder::add_declaration_impl(tree::FunctionDecl& decl) const
{
    NOT_NULL(origin_func);

    auto*const owner = origin_func->getAnyOwner<tree::ClassDecl, tree::Source>();

    BOOST_ASSERT(owner != nullptr && "function declaration is not under any class/source");

    if (auto*const cls_decl = tree::cast<tree::ClassDecl>(owner))
        cls_decl->addFunction(&decl);
    else if (auto*const src = tree::cast<tree::Source>(owner))
        src->addDeclare(&decl);
    else
        UNREACHABLE_CODE();
}

tree::Statement* adaptor_builder::create_dummy_return() const
{
    const auto*const return_type = origin_func->type->getCanonicalType();
    BOOST_ASSERT(return_type != nullptr && "canonical type is not ready!?");

    auto*const dummy_return_value = tree::ASTNodeHelper::getDummyValue(*return_type);

    if (dummy_return_value == nullptr)
        return nullptr;

    auto*const dummy_return_stmt = new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN, dummy_return_value);

    return dummy_return_stmt;
}

template<typename source_type, typename defers_container_type, typename... var_name_arg_types>
tree::VariableDecl* adaptor_builder::create_async_call(source_type& target_or_stmt, tree::Block& insert_to, defers_container_type&& defer_actions, var_name_arg_types&&... var_name_args) const
{
    const auto&      var_name = create_async_id_var_name(target_or_stmt, std::forward<var_name_arg_types>(var_name_args)...);
    const auto&      skeleton = create_async_call_skeleton(insert_to, var_name);
          auto*const var_decl = std::get<0>(skeleton);

    fill_async_call_skeleton(target_or_stmt, skeleton, defer_actions);

    return var_decl;
}

std::wstring adaptor_builder::create_async_id_var_name(const block_t& target_block, const std::wstring& suffix) const
{
    return L"post_async_id_" + suffix;
}

std::wstring adaptor_builder::create_async_id_var_name(const tree::Statement& origin_stmt, const std::wstring& suffix, unsigned& serial) const
{
    return L"async_id_" + std::to_wstring(serial++) + suffix;
}

std::pair<tree::VariableDecl*, tree::AsyncBlock*> adaptor_builder::create_async_call_skeleton(tree::Block& insert_to, const std::wstring& var_name) const
{
    auto*const var_id   = new tree::SimpleIdentifier(var_name);
    auto*const var_type = new tree::PrimitiveSpecifier(tree::PrimitiveKind::INT64_TYPE);
    auto*const var_decl =  tree::VariableDecl::Creator()
                          .name(var_id)
                          .type(var_type)
                          .create();
    auto*const var_stmt = new tree::DeclarativeStmt(var_decl);

    auto*const init_expr = tree::BinaryExpr::create<'='>(
                               new tree::IdExpr(var_id->clone()),
                               new tree::NumericLiteral(tree::PrimitiveKind::INT64_TYPE, -1)
                           );
    auto*const init_stmt = new tree::ExpressionStmt(init_expr);

    auto*const async_block = new tree::AsyncBlock;
    auto*const async_expr  = new tree::BlockExpr(async_block);
    auto*const async_stmt  = new tree::ExpressionStmt(async_expr);

    async_block->is_implicit = true;
    set_async_alternative_id_var(*async_block, *var_decl);

    insert_to.appendObjects({var_stmt, init_stmt, async_stmt});

    return {var_decl, async_block};
}

template<typename skeleton_type>
void adaptor_builder::fill_async_call_skeleton(const block_t& target_block, const skeleton_type& skeleton, std::nullptr_t) const
{
    auto*const             var_decl    = std::get<0>(skeleton);
    tree::AsyncBlock*const async_block = std::get<1>(skeleton);

    NOT_NULL(var_decl   );
    NOT_NULL(async_block);

    BOOST_ASSERT(decl_mapping.count(target_block) && "no corresponding declaration of source block");

    const tree::FunctionDecl& target_decl = *decl_mapping.at(target_block);

    NOT_NULL(target_decl.name);

    auto*const async_call_expr = new tree::CallExpr(new tree::IdExpr(target_decl.name->clone()));
    auto*const async_call_stmt = new tree::ExpressionStmt(async_call_expr);

    append_call_parameters(*async_call_expr, target_block);

    async_block->appendObject(async_call_stmt);
}

template<typename skeleton_type>
void adaptor_builder::fill_async_call_skeleton(tree::Statement& origin_stmt, const skeleton_type& skeleton, defer_actions_container& defer_actions) const
{
    auto*const             var_decl    = std::get<0>(skeleton);
    tree::AsyncBlock*const async_block = std::get<1>(skeleton);

    NOT_NULL(var_decl   );
    NOT_NULL(async_block);

    const auto&      assignee_info = extract_assignment_info_from_asynced_stmt(origin_stmt);
    const auto*const assignee_decl = std::get<0>(assignee_info);

    if (assignee_decl == nullptr)
    {
        defer_actions.emplace_back(
            [async_block, &origin_stmt]
            {
                async_block->appendObject(&origin_stmt);
            }
        );
    }
    else
    {
        /*
         * From
         * @code
         * (lhs += 1) = call();
         * @endcode
         * to
         * @code
         * 0;
         * async -> persister.lhs = 0;
         * @endcode
         * then
         * @code
         * lhs += 1;                        // defer action!
         * async -> persister.lhs = call(); // defer action!
         * @endcode
         */
        auto*const async_block_stmt = async_block->getOwner<tree::Statement>();
        BOOST_ASSERT(async_block_stmt && "async block is not under any statement!?");

        auto*const owner_block = async_block_stmt->getOwner<tree::Block>();
        BOOST_ASSERT(owner_block && "async block is not under other blocks!?");

        auto*const origin_lhs = std::get<1>(assignee_info);
        auto*const origin_rhs = std::get<2>(assignee_info);

        auto*const lhs_placeholder = new tree::NumericLiteral(0);
        auto*const rhs_placeholder = new tree::NumericLiteral(0);

        auto*const pre_async_stmt = new tree::ExpressionStmt(lhs_placeholder);
        auto*const     async_stmt = new tree::ExpressionStmt(
                                        tree::BinaryExpr::create<'='>(
                                            new tree::MemberExpr(
                                                persister.create_var_ref(),
                                                assignee_decl->name->clone()
                                            ),
                                            rhs_placeholder
                                        )
                                    );

        async_block->appendObject(async_stmt);
        owner_block->insertObjectBefore(async_block_stmt, pre_async_stmt);

        defer_actions.emplace_back(
            [origin_lhs, origin_rhs, lhs_placeholder, rhs_placeholder]
            {
                lhs_placeholder->parent->replaceUseWith(*lhs_placeholder, *origin_lhs);
                rhs_placeholder->parent->replaceUseWith(*rhs_placeholder, *origin_rhs);
            }
        );
    }
}

tree::Block* adaptor_builder::create_normal_call(const block_t& target_block, const std::set<tree::VariableDecl*>& passed_vars) const
{
    auto*const block = new tree::NormalBlock();

    return create_normal_call(target_block, passed_vars, *block);
}

tree::Block* adaptor_builder::create_normal_call(const block_t* target_block, const std::set<tree::VariableDecl*>* passed_vars) const
{
    BOOST_ASSERT(
        (
            (passed_vars == nullptr && target_block == nullptr) ||
            (passed_vars != nullptr && target_block != nullptr)
        ) && "passed_vars and target_block must be both null or non-null"
    );

    if (passed_vars != nullptr && target_block != nullptr)
        return create_normal_call(*target_block, *passed_vars);

    const auto*const return_type = origin_func->type->getCanonicalType();
    BOOST_ASSERT(return_type != nullptr && "canonical type is not ready");

    auto*const dummy_value = tree::ASTNodeHelper::getDummyValue(*return_type);

    return new tree::NormalBlock{
        new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN, dummy_value)
    };
}

tree::Block* adaptor_builder::create_normal_call(const block_t& target_block, const std::set<tree::VariableDecl*>& passed_vars, tree::Block& insert_to) const
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    BOOST_ASSERT(decl_mapping.count(target_block) && "no corresponding declaration of target block");

    const tree::FunctionDecl& target_decl = *decl_mapping.at(target_block);

    auto*const call_expr = new tree::CallExpr(
                               new tree::IdExpr(
                                   target_decl.name->clone()
                               )
                           );
    auto*const call_ret  = new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN, call_expr);

    insert_to.appendObjects(passed_vars | indirected | transformed(std::bind(&adaptor_builder::create_var_storer, this, std::placeholders::_1)));
    insert_to.appendObject(call_ret);

    append_call_parameters(*call_expr, target_block);

    return &insert_to;
}

tree::Statement* adaptor_builder::create_var_loader(const tree::VariableDecl& used_var) const
{
    auto*const loader_init = new tree::MemberExpr(
                                 persister.create_var_ref(),
                                 used_var.name->clone()
                             );
    auto       creator       =  tree::VariableDecl::Creator()
                                .name(used_var.name->clone())
                                .init(loader_init)
                                ;

    if (used_var.is_const)
        creator = creator.const_();

    auto*const loader_decl = creator.create();
    auto*const loader_stmt = new tree::DeclarativeStmt(loader_decl);

    return loader_stmt;
}

tree::Statement* adaptor_builder::create_var_storer(const tree::VariableDecl& used_var) const
{
    NOT_NULL(used_var.name);

    auto*const set_expr = tree::BinaryExpr::create<'='>(
                              new tree::MemberExpr(
                                  persister.create_var_ref(),
                                  used_var.name->clone()
                              ),
                              new tree::IdExpr(used_var.name->clone())
                          );
    auto*const set_stmt = new tree::ExpressionStmt(set_expr);

    return set_stmt;
}

void adaptor_builder::append_call_parameters(tree::CallExpr& call_expr, const block_t& target_block) const
{
    auto*const persister_ref = persister.create_var_ref();

    call_expr.appendParameter(persister_ref);
}

}

void transform_flow(const tree::FunctionDecl& func_with_flow, const visitor::flow_spliter_detail::graph_t& graph)
{
    adaptor_builder builder;

    builder.build(func_with_flow, graph);
}

} } } }
