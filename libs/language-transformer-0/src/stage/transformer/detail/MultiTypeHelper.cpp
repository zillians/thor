/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <array>
#include <functional>
#include <string>

#include <boost/assert.hpp>
#include <boost/functional/factory.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/next_prior.hpp>
#include <boost/optional/optional.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/join.hpp>

#include "utility/RangeUtil.h"
#include "utility/UnicodeUtil.h"
#include "utility/UUIDUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Block.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/TieExpr.h"
#include "language/tree/expression/UnpackExpr.h"
#include "language/tree/statement/DeclarativeStmt.h"
#include "language/tree/statement/ExpressionStmt.h"
#include "language/tree/statement/Statement.h"

#include "language/stage/transformer/detail/MultiTypeHelper.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace multi_type_helper {

namespace {

tree::SimpleIdentifier* createUnpackVariable(const tree::ASTNode& source_ref, tree::Block& block, tree::Type* type)
{
    BOOST_ASSERT(type && "null pointer exception");

    const std::wstring &temp_name = L"unpack_temp_" + s_to_ws(UUID::random().toString('_'));

    tree::SimpleIdentifier* temp_decl_id = new tree::SimpleIdentifier(temp_name);
    tree::TypeSpecifier*    temp_decl_ts = tree::ASTNodeHelper::createTypeSpecifierFrom(type);

    BOOST_ASSERT(temp_decl_ts && "null pointer exception");

    tree::VariableDecl*    temp_decl      = new tree::VariableDecl(temp_decl_id, temp_decl_ts, false, false, false, tree::VariableDecl::VisibilitySpecifier::DEFAULT);
    tree::DeclarativeStmt* temp_decl_stmt = new tree::DeclarativeStmt(temp_decl);

    block.appendObject(temp_decl_stmt);

    tree::ASTNodeHelper::propogateSourceInfo(*temp_decl_id  , source_ref);
    tree::ASTNodeHelper::propogateSourceInfo(*temp_decl_ts  , source_ref);
    //ASTNodeHelper::foreachApply<ASTNode>( *temp_decl_ts, [&source_ref]( ASTNode &n ){
    //    ASTNodeHelper::propogateSourceInfo( n, source_ref );
    //} );
    tree::ASTNodeHelper::propogateSourceInfo(*temp_decl     , source_ref);
    tree::ASTNodeHelper::propogateSourceInfo(*temp_decl_stmt, source_ref);

    tree::SimpleIdentifier* temp_decl_ref_id = new tree::SimpleIdentifier(temp_name);

    ResolvedType  ::set(temp_decl_ref_id, type     );
    ResolvedSymbol::set(temp_decl_ref_id, temp_decl);

    tree::ASTNodeHelper::propogateSourceInfo(*temp_decl_ref_id, source_ref);

    return temp_decl_ref_id;
}

tree::UnpackExpr* createUnpacker(const tree::ASTNode& source_ref, tree::Block& block, tree::CallExpr& rhs_expr)
{
    using boost::adaptors::transformed;

    const tree::MultiType* rhs_expr_type_multi = tree::cast_or_null<tree::MultiType>(rhs_expr.getCanonicalType());

    BOOST_ASSERT(rhs_expr_type_multi && "we should not enter here if canonical type is not ready");

    tree::UnpackExpr* unpacker = new tree::UnpackExpr(&rhs_expr);

    unpacker->appendNames(
          rhs_expr_type_multi->types
        | transformed(std::bind(createUnpackVariable, std::cref(source_ref), std::ref(block), std::placeholders::_1))
    );

    tree::ExpressionStmt* unpacker_stmt = new tree::ExpressionStmt(unpacker);

    block.appendObject(unpacker_stmt);

    tree::ASTNodeHelper::propogateSourceInfo(*unpacker     , source_ref);
    tree::ASTNodeHelper::propogateSourceInfo(*unpacker_stmt, source_ref);

    return unpacker;
}

tree::TieExpr* mergeUnpackedVariables(const tree::ASTNode& source_ref, const tree::BinaryExpr::OpCode::type op, tree::Block& block, const tree::TieExpr& tied, tree::UnpackExpr& unpacker)
{
    BOOST_ASSERT(tied.tied_expressions.size() == unpacker.unpack_list.size() && "size mismatch");

    tree::TieExpr* merged_expr = new tree::TieExpr();

    for (const auto &item : make_zip_range(tied.tied_expressions, unpacker.unpack_list))
    {
        tree::Expression*       lhs_expr     = boost::get<0>(item);
        tree::SimpleIdentifier* rhs_name_ref = boost::get<1>(item);

        BOOST_ASSERT(lhs_expr && "null pointer exception");
        BOOST_ASSERT(rhs_name_ref && "null pointer exception");

        tree::SimpleIdentifier* rhs_name = rhs_name_ref->clone();

        BOOST_ASSERT(rhs_name && "null pointer exception");

        tree::IdExpr*     rhs_expr = new tree::IdExpr(rhs_name);
        tree::BinaryExpr* new_expr = new tree::BinaryExpr(op, lhs_expr, rhs_expr);

        merged_expr->tieExpression(new_expr);

        tree::ASTNodeHelper::propogateSourceInfo(*rhs_name, source_ref);
        tree::ASTNodeHelper::propogateSourceInfo(*rhs_expr, source_ref);
        tree::ASTNodeHelper::propogateSourceInfo(*new_expr, source_ref);
    }

    tree::ExpressionStmt* merged_stmt = new tree::ExpressionStmt(merged_expr);

    block.appendObject(merged_stmt);

    tree::ASTNodeHelper::propogateSourceInfo(*merged_expr, source_ref);
    tree::ASTNodeHelper::propogateSourceInfo(*merged_stmt, source_ref);

    return merged_expr;
}

tree::TieExpr* createUnpackedTie(const tree::ASTNode& source_ref, tree::Block& block, const tree::UnpackExpr& unpacker)
{
    std::vector<tree::Expression*> unpack_exprs;

    for (tree::SimpleIdentifier* unpack_name : unpacker.unpack_list)
    {
        BOOST_ASSERT(unpack_name && "null pointer exception");

        tree::SimpleIdentifier* unpack_name_cloned = unpack_name->clone();
        tree::IdExpr*           unpack_expr        = new tree::IdExpr(unpack_name_cloned);

        unpack_exprs.emplace_back(unpack_expr);

        tree::ASTNodeHelper::propogateSourceInfo(*unpack_name_cloned, source_ref);
        tree::ASTNodeHelper::propogateSourceInfo(*unpack_expr       , source_ref);
    }

    tree::TieExpr*        unpack_tie  = new tree::TieExpr(std::move(unpack_exprs));
    tree::ExpressionStmt* unpack_stmt = new tree::ExpressionStmt(unpack_tie);

    block.appendObject(unpack_stmt);

    tree::ASTNodeHelper::propogateSourceInfo(*unpack_tie , source_ref);
    tree::ASTNodeHelper::propogateSourceInfo(*unpack_stmt, source_ref);

    return unpack_tie;
}

template<typename RangeType = std::array<tree::Statement*, 0>>
void unpackCallImpl(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::CallExpr& rhs_expr, const RangeType& prepends = RangeType())
{
    tree::Block*     block    = new tree::NormalBlock();
    tree::BlockExpr* new_expr = new tree::BlockExpr(block, "multi-type-restructure");

    tree::ASTNodeHelper::propogateSourceInfo(*block   , node);
    tree::ASTNodeHelper::propogateSourceInfo(*new_expr, node);

    BOOST_ASSERT( SourceInfoContext::get(&node) );

    block->appendObjects(prepends);

    tree::UnpackExpr* unpacker = createUnpacker(node, *block, rhs_expr);

    BOOST_ASSERT(unpacker && "null pointer exception");

    mergeUnpackedVariables(node, node.opcode, *block, lhs_expr, *unpacker);

    BOOST_ASSERT(node.parent && "null pointer exception");

    node.parent->replaceUseWith(node, *new_expr);
}

void unpackCallImpl(const tree::ASTNode& source_ref, tree::CallExpr& ret_expr)
{
    tree::Block*     block    = new tree::NormalBlock();
    tree::BlockExpr* new_expr = new tree::BlockExpr(block, "multi-type-restructure");

    tree::ASTNodeHelper::propogateSourceInfo(*block   , source_ref);
    tree::ASTNodeHelper::propogateSourceInfo(*new_expr, source_ref);

    BOOST_ASSERT(ret_expr.parent && "null pointer exception");

    ret_expr.parent->replaceUseWith(ret_expr, *new_expr);

    tree::UnpackExpr* unpacker = createUnpacker(source_ref, *block, ret_expr);

    BOOST_ASSERT(unpacker && "null pointer exception");

    createUnpackedTie(source_ref, *block, *unpacker);
}

/**
 *  Transforms "a, b = 1, 2" into "(a = 1), (b = 2)"
 */
tree::TieExpr* mergeTie(const tree::ASTNode& source_info, const tree::BinaryExpr::OpCode::type op, tree::TieExpr& lhs_expr, tree::TieExpr& rhs_expr)
{
    BOOST_ASSERT(lhs_expr.tied_expressions.size() == rhs_expr.tied_expressions.size() && "amount of tied expression mismatch");

    tree::TieExpr* new_expr = new tree::TieExpr();

    tree::ASTNodeHelper::propogateSourceInfo(*new_expr, source_info);

    for (const auto& item : make_zip_range(lhs_expr.tied_expressions, rhs_expr.tied_expressions))
    {
        tree::Expression* lhs_tied_expr = boost::get<0>(item);
        tree::Expression* rhs_tied_expr = boost::get<1>(item);

        BOOST_ASSERT(lhs_tied_expr && "null pointer exception");
        BOOST_ASSERT(rhs_tied_expr && "null pointer exception");

        tree::BinaryExpr* new_tied_expr = new tree::BinaryExpr(op, lhs_tied_expr, rhs_tied_expr);

        new_expr->tieExpression(new_tied_expr);

        tree::ASTNodeHelper::propogateSourceInfo(*new_tied_expr, source_info);
    }

    return new_expr;
}

std::size_t getTypeCount(const tree::TieExpr& expr)
{
    return expr.tied_expressions.size();
}

template<typename NodeType>
boost::optional<std::size_t> getTypeCount(const NodeType& expr)
{
    const tree::Type* rhs_expr_type = expr.getCanonicalType();

    if (rhs_expr_type == nullptr)
        return boost::optional<std::size_t>();

    const tree::MultiType* rhs_expr_type_multi = rhs_expr_type->getAsMultiType();

    if (rhs_expr_type_multi == nullptr)
        return boost::optional<std::size_t>(1);

    return rhs_expr_type_multi->types.size();
}

template<typename NodeType1, typename NodeType2>
boost::tribool isTypeCountMatched(const NodeType1& expr1, const NodeType2& expr2)
{
    const boost::optional<std::size_t> &expr1_count = getTypeCount(expr1);
    const boost::optional<std::size_t> &expr2_count = getTypeCount(expr2);

    if (expr1_count && expr2_count)
        return *expr1_count == *expr2_count;
    else
        return boost::indeterminate;
}

}

tree::TieExpr& findEndingTieExpr(tree::BlockExpr& expr)
{
    BOOST_ASSERT(expr.tag == "multi-type-restructure" && "unexpected tag from block expression");

    BOOST_ASSERT(expr.block && "null pointer exception");
    BOOST_ASSERT(!expr.block->objects.empty() && "empty block expression");
    BOOST_ASSERT(expr.block->objects.back() && "null pointer exception");

    tree::ExpressionStmt* tie_expr_stmt = tree::cast<tree::ExpressionStmt>(expr.block->objects.back());

    BOOST_ASSERT(tie_expr_stmt && "last statement of block expression is not a expression");
    BOOST_ASSERT(tie_expr_stmt->expr && "null pointer exception");

    tree::TieExpr* tie_expr = tree::cast<tree::TieExpr>(tie_expr_stmt->expr);

    BOOST_ASSERT(tie_expr && "last expression is not a TieExpr after MultiType restruction");

    return *tie_expr;
}

bool isValidReturn(const tree::Type& type)
{
    if (const tree::MultiType* type_multi = type.getAsMultiType())
    {
        return std::none_of(type_multi->types.begin(), type_multi->types.end(), static_cast<bool(*)(const tree::ASTNode*)>(&tree::isa<tree::MultiType>));
    }

    return true;
}

boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::BlockExpr& rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::CallExpr&  rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::TieExpr&   lhs_expr, const tree::TieExpr&   rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::BlockExpr& rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::CallExpr&  rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }
boost::tribool isMergable(const tree::BlockExpr& lhs_expr, const tree::TieExpr&   rhs_expr) { return isTypeCountMatched(lhs_expr, rhs_expr); }

void unpackCall(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::CallExpr& rhs_expr)
{
    unpackCallImpl(node, lhs_expr, rhs_expr);
}

void unpackCall(tree::BinaryExpr& node, tree::BlockExpr& lhs_expr, tree::CallExpr& rhs_expr)
{
    tree::TieExpr& lhs_expr_tie = findEndingTieExpr(lhs_expr);

    unpackCallImpl(node, lhs_expr_tie, rhs_expr, boost::make_iterator_range(lhs_expr.block->objects.begin(), boost::prior(lhs_expr.block->objects.end())));
}

void unpackCall(tree::BranchStmt& node, tree::CallExpr& ret_expr)
{
    BOOST_ASSERT(node.opcode == tree::BranchStmt::OpCode::RETURN && "invalid operation on BranchStmt");
    BOOST_ASSERT(&ret_expr == node.result && "incorrect result expression");

    unpackCallImpl(node, ret_expr);
}

void mergeExpr(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::TieExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    tree::TieExpr* new_expr = mergeTie(node, node.opcode, lhs_expr, rhs_expr);

    BOOST_ASSERT(new_expr && "null pointer exception");
    BOOST_ASSERT(node.parent && "null pointer exception");

    node.parent->replaceUseWith(node, *new_expr);
}

void mergeExpr(tree::BinaryExpr& node, tree::TieExpr& lhs_expr, tree::BlockExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    tree::TieExpr& rhs_expr_tie = findEndingTieExpr(rhs_expr);
    tree::TieExpr* lhs_expr_new = mergeTie(node, node.opcode, lhs_expr, rhs_expr_tie);

    BOOST_ASSERT(lhs_expr_new && "null pointer exception");
    BOOST_ASSERT(rhs_expr_tie.parent && "null pointer exception");
    BOOST_ASSERT(node.parent && "null pointer exception");

    rhs_expr_tie.parent->replaceUseWith(rhs_expr_tie, *lhs_expr_new);
    node.parent->replaceUseWith(node, rhs_expr);

    ResolvedType::set(&rhs_expr, nullptr);
}

void mergeExpr(tree::BinaryExpr& node, tree::BlockExpr& lhs_expr, tree::TieExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    tree::TieExpr& lhs_expr_tie = findEndingTieExpr(lhs_expr);
    tree::TieExpr* rhs_expr_new = mergeTie(node, node.opcode, lhs_expr_tie, rhs_expr);

    BOOST_ASSERT(rhs_expr_new && "null pointer exception");
    BOOST_ASSERT(lhs_expr_tie.parent && "null pointer exception");
    BOOST_ASSERT(node.parent && "null pointer exception");

    lhs_expr_tie.parent->replaceUseWith(lhs_expr_tie, *rhs_expr_new);
    node.parent->replaceUseWith(node, lhs_expr);

    ResolvedType::set(&lhs_expr, nullptr);
}

void mergeExpr(tree::BinaryExpr& node, tree::BlockExpr& lhs_expr, tree::BlockExpr& rhs_expr)
{
    BOOST_ASSERT(&lhs_expr == node.left  && "incorrect LHS expression");
    BOOST_ASSERT(&rhs_expr == node.right && "incorrect RHS expression");

    tree::TieExpr& lhs_expr_tie = findEndingTieExpr(lhs_expr);
    tree::TieExpr& rhs_expr_tie = findEndingTieExpr(rhs_expr);

    tree::TieExpr* new_expr_tie = mergeTie(node, node.opcode, lhs_expr_tie, rhs_expr_tie);

    BOOST_ASSERT(new_expr_tie && "null pointer exception");

    tree::ExpressionStmt* new_expr_stmt = new tree::ExpressionStmt(new_expr_tie);

    tree::ASTNodeHelper::propogateSourceInfo(*new_expr_stmt, node);

    tree::Block*     new_expr_block = new tree::NormalBlock();
    tree::BlockExpr* new_expr       = new tree::BlockExpr(new_expr_block, "multi-type-restructure");

    tree::ASTNodeHelper::propogateSourceInfo(*new_expr_block, node);
    tree::ASTNodeHelper::propogateSourceInfo(*new_expr      , node);

    BOOST_ASSERT(lhs_expr.block && "null pointer exception");
    BOOST_ASSERT(rhs_expr.block && "null pointer exception");
    BOOST_ASSERT(!lhs_expr.block->objects.empty() && "empty block expression");
    BOOST_ASSERT(!rhs_expr.block->objects.empty() && "empty block expression");

    new_expr_block->appendObjects(
        boost::range::join(
            boost::make_iterator_range(lhs_expr.block->objects.begin(), boost::prior(lhs_expr.block->objects.end())),
            boost::make_iterator_range(rhs_expr.block->objects.begin(), boost::prior(rhs_expr.block->objects.end()))
        )
    );
    new_expr_block->appendObject(new_expr_stmt);

    BOOST_ASSERT(node.parent && "null pointer exception");

    node.parent->replaceUseWith(node, *new_expr);
}

} } } } }
