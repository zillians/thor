/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */


#include <vector>
#include <utility>
#include <initializer_list>

#include <boost/range/algorithm/find.hpp>

#include "language/context/ManglingStageContext.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/expression/ArrayExpr.h"
#include "language/tree/expression/BlockExpr.h"
#include "language/tree/expression/BinaryExpr.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/CastExpr.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/IdExpr.h"
#include "language/tree/expression/IndexExpr.h"
#include "language/tree/expression/IsaExpr.h"
#include "language/tree/expression/LambdaExpr.h"
#include "language/tree/expression/MemberExpr.h"
#include "language/tree/expression/TieExpr.h"
#include "language/tree/expression/TernaryExpr.h"
#include "language/tree/expression/UnaryExpr.h"
#include "language/tree/expression/UnpackExpr.h"
#include "language/tree/declaration/VariableDecl.h"

#include "language/stage/transformer/detail/ConstantFolder.h"
#include "language/stage/transformer/detail/EnumDependencyWalker.h"


namespace zillians { namespace language { namespace stage { namespace visitor {

namespace detail {

bool is_bool_int_literal(language::tree::NumericLiteral* literal)
{
    return literal != nullptr
           && (literal->primitive_kind.isBoolType() || literal->primitive_kind.isIntegerType());
}

bool EnumDependencyWalker::has_value(Enumerator* enumerator)
{
    return EnumIdManglingContext::get(enumerator) != nullptr;
}

template < typename ParentExpr >
auto EnumDependencyWalker::walk_by_steps(ParentExpr* parent, std::initializer_list<language::tree::Expression*> sub_exprs) -> Result
{
    bool all_determined, found_circular_dependency, folded;

    for(auto sub_expr : sub_exprs)
    {
        std::tie(all_determined, found_circular_dependency, folded) = walk_impl(sub_expr);

        if(!all_determined || found_circular_dependency)
            return Result(all_determined, found_circular_dependency, folded);
    }

    if(all_determined && folded)
        folded = detail::fold_constant(*parent);

    return Result(all_determined, found_circular_dependency, folded);
}

auto EnumDependencyWalker::walk_impl(language::tree::Expression* expr) -> Result
{
    using namespace language::tree;

    bool all_determined, found_circular_dependency, folded;

    // return immediately for unsupported expressions
    if(expr == nullptr || isa<ArrayExpr,BlockExpr,CallExpr,IndexExpr,TieExpr,UnpackExpr,LambdaExpr>(expr))
        return Result(all_determined=true, found_circular_dependency=false, folded=false);

    // base case for the constant literals and enumerators
    if(isa<IdExpr,MemberExpr>(expr))
    {
        // return if meet non-enum exprs or exprs we don't know its type yet
        auto* enumerator = expr->getConstantEnumerator();
        if(enumerator == nullptr)
            return Result(all_determined=true, found_circular_dependency=false, folded=false);

        if(boost::range::find(walked, enumerator) != walked.end())
        {
            self_referenced = enumerator;
            return Result(all_determined=false, found_circular_dependency=true, folded=false);
        }

        walked.push_back(enumerator);
        std::tie(all_determined , found_circular_dependency, folded) = walk_impl(enumerator->initializer);
        walked.pop_back();

        if(!all_determined || found_circular_dependency)
            return Result(all_determined, found_circular_dependency, folded);

        return Result(has_value(enumerator), found_circular_dependency, folded=true);
    }
    else if(auto* literal = cast<Literal>(expr))
    {
        folded = is_bool_int_literal(cast<NumericLiteral>(literal));

        return Result(all_determined=true, found_circular_dependency=false, folded);
    }
    else if(auto* binary_expr = cast<BinaryExpr>(expr))
    {
        return walk_by_steps(
            binary_expr
            , { binary_expr->left, binary_expr->right }
        );
    }
    else if(auto* ternary_expr = cast<TernaryExpr>(expr))
    {
        return walk_by_steps(
            ternary_expr
            , { ternary_expr->cond, ternary_expr->true_node, ternary_expr->false_node }
        );
    }
    else if(auto* unary_expr = cast<UnaryExpr>(expr))
    {
        return walk_by_steps(
            unary_expr, { unary_expr->node }
        );
    }
    else if(auto* cast_expr = cast<CastExpr>(expr))
    {
        return walk_by_steps(
            cast_expr, { cast_expr->node }
        );
    }
    else if(auto* isa_expr = cast<IsaExpr>(expr))
    {
        // we do not fold isa<T>() expressions
        std::tie(all_determined , found_circular_dependency, folded) = walk_impl(isa_expr->node);
        return Result(all_determined, found_circular_dependency, folded=false);
    }

    BOOST_ASSERT(false && "should not reach here");
    return Result(all_determined=false, found_circular_dependency=false, folded=false);
}

auto EnumDependencyWalker::walk(Enumerator* enumerator) -> Result
{
    self_referenced = nullptr;
    walked = { enumerator };

    return walk_impl(enumerator->initializer);
}

} // namespace detail

} } } } // namespace zillians::language::stage::visitor
