/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <fstream>
#include <iomanip>
#include <ostream>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/mpl/end.hpp>
#include <boost/mpl/insert_range.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/none.hpp>
#include <boost/optional/optional.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/sort.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/distance.hpp>
#include <boost/range/end.hpp>
#include <boost/range/iterator_range.hpp>

#include "utility/UnicodeUtil.h"
#include "utility/RangeUtil.h"
#include "utility/MemoryUtil.h"

#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/transformer/detail/FlowSpliter.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace flow_spliter_detail {

namespace {

using ContextToCloneList =
    boost::mpl::insert_range<
                        tree::detail::ContextToCloneTypeList       ,
        boost::mpl::end<tree::detail::ContextToCloneTypeList>::type,
        boost::mpl::vector<
            zillians::language::ResolvedType,
            zillians::language::ResolvedSymbol,
            zillians::language::SplitReferenceContext,
            zillians::language::SplitInverseReferenceContext
        >
    >::type;

enum
{
    BLOCK_FUNC_INIT, BLOCK_FUNC_END,
    BLOCK_FOR_INIT, BLOCK_FOR_COND, BLOCK_FOR_STEP, BLOCK_FOR_BODY, BLOCK_FOR_END,
    BLOCK_IF_COND, BLOCK_IF_TRUE, BLOCK_IF_ELIF, BLOCK_IF_FALSE, BLOCK_IF_END,
    BLOCK_FLOW_INIT, BLOCK_FLOW_SUB, BLOCK_FLOW_END,
    BLOCK_WHILE_COND, BLOCK_WHILE_BODY, BLOCK_WHILE_END,
    BLOCK_SWITCH_COND, BLOCK_SWITCH_CASE, BLOCK_SWITCH_DEFAULT, BLOCK_SWITCH_END,
    BLOCK_DEAD_CODE,
};

const char* get_tagged_name(const block_info_t& info)
{
    switch (info.tag)
    {
    case BLOCK_FUNC_INIT     : return "FUNC_INIT"     ;
    case BLOCK_FUNC_END      : return "FUNC_END"      ;
    case BLOCK_FOR_INIT      : return "FOR_INIT"      ;
    case BLOCK_FOR_COND      : return "FOR_COND"      ;
    case BLOCK_FOR_STEP      : return "FOR_STEP"      ;
    case BLOCK_FOR_BODY      : return "FOR_BODY"      ;
    case BLOCK_FOR_END       : return "FOR_END"       ;
    case BLOCK_IF_COND       : return "IF_COND"       ;
    case BLOCK_IF_TRUE       : return "IF_TRUE"       ;
    case BLOCK_IF_ELIF       : return "IF_ELIF"       ;
    case BLOCK_IF_FALSE      : return "IF_FALSE"      ;
    case BLOCK_IF_END        : return "IF_END"        ;
    case BLOCK_FLOW_INIT     : return "FLOW_INIT"     ;
    case BLOCK_FLOW_SUB      : return "FLOW_SUB"      ;
    case BLOCK_FLOW_END      : return "FLOW_END"      ;
    case BLOCK_WHILE_COND    : return "WHILE_COND"    ;
    case BLOCK_WHILE_BODY    : return "WHILE_BODY"    ;
    case BLOCK_WHILE_END     : return "WHILE_END"     ;
    case BLOCK_SWITCH_COND   : return "SWITCH_COND"   ;
    case BLOCK_SWITCH_CASE   : return "SWITCH_CASE"   ;
    case BLOCK_SWITCH_DEFAULT: return "SWITCH_DEFAULT";
    case BLOCK_SWITCH_END    : return "SWITCH_END"    ;
    case BLOCK_DEAD_CODE     : return "DEAD_CODE"     ;
    }

    return "unknown";
}

const char* get_transition_name(const block_info_t& info)
{
    switch (info.transition)
    {
    case TRANS_UNKNOWN  : return "unknown"  ;
    case TRANS_CASE     : return "case"     ;
    case TRANS_CONDITION: return "condition";
    case TRANS_FLOW     : return "flow"     ;
    case TRANS_MERGE    : return "merge"    ;
    case TRANS_NORMAL   : return "normal"   ;
    case TRANS_NULL     : return "null"     ;
    }

    return "unexpected";
}

struct info_writer_base
{
    explicit info_writer_base(const graph_t& graph) : graph(&graph) {}

    const graph_t* graph;

protected:
    void write_condition(std::ostream& out, const tree::Expression* cond_expr) const
    {
        if (cond_expr == nullptr)
            return;

        for (const auto& line : node_to_html_lines(*cond_expr))
            out << "<tr><td align=\"left\" bgcolor=\"yellow\">" << line << "</td></tr>";
    }

    static std::vector<std::string> node_to_html_lines(const tree::ASTNode& node)
    {
        const auto& wstr = node.toSource(0);
              auto   str = boost::copy_range<std::string>(wstr); // FIXME provide better way to support unicode

        boost::replace_all(str, "&", "&amp;");
        boost::replace_all(str, "<", "&lt;" );
        boost::replace_all(str, ">", "&gt;" );

        std::vector<std::string> lines;

        boost::split(lines, str, boost::is_any_of("\n"));

        return std::move(lines);
    }
};

struct block_info_writer : info_writer_base
{
    using info_writer_base::info_writer_base;

    void operator()(std::ostream& out, const block_t& block) const
    {
        const auto& block_info = (*graph)[block];

        out <<  R"block-info([shape=none fontname="Monospace" margin="0" label=)block-info"
                R"table-prefix(<<table border="1" cellborder="0" cellpadding="0">)table-prefix"
                R"raw-prefix(<tr><td cellpadding="4" align="left" bgcolor="gray">)raw-prefix" << std::dec << block << ' ' << get_tagged_name(block_info) << "</td></tr>"
            <<  R"raw-prefix(<tr><td align="left" bgcolor="pink">transition: )raw-prefix" << get_transition_name(block_info) << "</td></tr>";

        write_variable_row(out, block_info.decl_variables, "#00DDFF", "decl");
        write_variable_row(out, block_info.used_variables, "cyan"   , "used");

        write_condition(out, block_info.cond_expr);
        write_statements(out, block_info.statements);

        out << "</table>>]";
    }

private:
    template<typename range_type, typename str1_type, typename str2_type>
    void write_variable_row(std::ostream& out, const range_type& variables, const str1_type& bgcolor, const str2_type& prefix) const
    {
        using boost::adaptors::transformed;

        auto variable_names = boost::copy_range<std::vector<std::string>>(
                                  variables
                                | transformed(std::mem_fn(&tree::VariableDecl::getName))
                                | transformed(std::mem_fn(&tree::Identifier::toString))
                                | transformed(&boost::copy_range<std::string, std::wstring>) // variable name should be ASCII characters only, use primitive conversion
                            );

        if (variable_names.empty())
            return;

        boost::sort(variable_names);

        out << R"row-prefix(<tr><td align="left" bgcolor=")row-prefix" << bgcolor << "\">" << prefix << ':';

        for (const auto& variable_name : variable_names)
            out << ' ' << variable_name;

        out << "</td></tr>";
    }

    template<typename range_type>
    void write_statements(std::ostream& out, const range_type& stmts) const
    {
        using boost::adaptors::indirected;
        using boost::adaptors::transformed;

        for (const auto& lines : stmts | indirected | transformed(&node_to_html_lines))
            for (const auto& line : lines)
                out << "<tr><td align=\"left\">" << line << "</td></tr>";
    }
};

struct link_info_writer : info_writer_base
{
    using info_writer_base::info_writer_base;

    void operator()(std::ostream& out, const link_t& link) const
    {
        // TODO implement it.

        const auto& link_info = (*graph)[link];

        if (link_info.cond_expr == nullptr)
            return;

        out << R"table-prefix([label=<<table border="1" cellborder="0" cellpadding="0">)table-prefix";

        write_condition(out, link_info.cond_expr);

        out << "</table>>]";
    }
};

struct FlowGraphGeneratorVisitor : public zillians::language::tree::visitor::GenericDoubleVisitor
{
    CREATE_INVOKER(flowGraphInvoker, generate);

    explicit FlowGraphGeneratorVisitor(const std::wstring& func_name)
        : has_flow_(false)
        , flow_tree_nodes()
        , graph()
        , current()
        , func_end()
        , continue_break_stack()
    {
        REGISTER_ALL_VISITABLE_ASTNODE(flowGraphInvoker);
    }

    void generate(tree::ASTNode& node)
    {
        revisit(node);
    }

    void mark_flow_tree(tree::FunctionDecl& func)
    {
        flow_tree_nodes.clear();

        tree::ASTNodeHelper::foreachApply<tree::FlowBlock>(func, [this, &func](tree::FlowBlock& fb){
            tree::ASTNode* current_node = &fb;
            do {
                flow_tree_nodes.insert(current_node);
                current_node = current_node->parent;
            } while (current_node != &func);
        });

        tree::ASTNodeHelper::foreachApply<tree::BranchStmt>(func, [this, &func](tree::BranchStmt& bs){
            tree::ASTNode* current_node = &bs;
            tree::ASTNode* loop_parent = nullptr;

            switch(bs.opcode) {
                case tree::BranchStmt::OpCode::BREAK:
                case tree::BranchStmt::OpCode::CONTINUE:
                    loop_parent = bs.getAnyOwner<tree::ForStmt, tree::WhileStmt>();
                    if(loop_parent != nullptr && on_flow_tree(*loop_parent)) {
                        do {
                            flow_tree_nodes.insert(current_node);
                            current_node = current_node->parent;
                        } while (current_node != loop_parent);
                    }
                    break;
                case tree::BranchStmt::OpCode::RETURN:
                    break;
            }
        });
    }

    void generate(tree::FunctionDecl& node)
    {
        if(!node.isCompleted()) {
            return;
        }

        mark_flow_tree(node);

        BOOST_ASSERT(graph == nullptr && "already initialized!?");

        graph    = make_unique<flow_spliter_detail::graph_t>();
        current  = new_block(BLOCK_FUNC_INIT);
        func_end = new_block(BLOCK_FUNC_END );

        init_transition_null(func_end);

        if(node.block)
            visit(*node.block);

        init_transition_normal_if_unknown(current, func_end);
    }

    void generate(tree::ForStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(&node);
            return;
        }

        const auto& cond = new_block(BLOCK_FOR_COND);
        const auto& step = new_block(BLOCK_FOR_STEP);
        const auto& body = new_block(BLOCK_FOR_BODY);
        const auto& end  = new_block(BLOCK_FOR_END );

        push_continue_break(cond, end);

        // initialize
        ensure_dead_code();
        init_transition_normal(current, cond);

        // condition
        current = cond;
        init_transition_condition(current, node.cond, body, end);

        // loop body
        current = body;
        visit(*node.block);
        init_transition_normal_if_unknown(current, step);

        // step expression
        current = step;
        append_statement(new tree::ExpressionStmt(tree::ASTNodeHelper::clone<tree::Expression, ContextToCloneList>(node.step)));
        init_transition_normal(current, cond);

        // loop end
        current = end;

        pop_continue_break();
    }

    void generate(tree::IfElseStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(&node);
            return;
        }

        // initialize
        ensure_dead_code();

        const auto  if_prev  = current; // store block id for later use, do not use reference!
        const auto& if_false = new_block(BLOCK_IF_FALSE);
        const auto& if_end   = new_block(BLOCK_IF_END  );

        // on false
        current = if_false;
        if (node.else_block != nullptr)
            visit(*node.else_block);
        init_transition_normal_if_unknown(current, if_end);

        // helper to build links between if-elif-else
              auto  dst_false = if_false;
        const auto& add_cond  = [this, &if_end, &dst_false](const tree::Selection& selection, int block_tag) -> block_t
        {
            BOOST_ASSERT(selection.cond  != nullptr && "null pointer exception");
            BOOST_ASSERT(selection.block != nullptr && "null pointer exception");

                  auto  cond = new_block(BLOCK_IF_COND);
            const auto& body = new_block(block_tag    );

            // condition
            init_transition_condition(cond, selection.cond, body, dst_false);

            // block
            current = body;
            visit(*selection.block);
            init_transition_normal_if_unknown(current, if_end);

            // store last one for the next add_cond call
            dst_false = cond;

            return std::move(cond);
        };

        // on elifs
        for (const auto*const selection : node.elseif_branches | boost::adaptors::reversed)
        {
            BOOST_ASSERT(selection != nullptr && "null pointer exception");

            add_cond(*selection, BLOCK_IF_ELIF);
        }

        // on true
        BOOST_ASSERT(node.if_branch != nullptr && "null pointer exception");
        const auto& if_cond = add_cond(*node.if_branch, BLOCK_IF_TRUE);
        init_transition_normal(if_prev, if_cond);

        // if-else end
        current = if_end;
    }

    void generate(tree::FlowBlock& node)
    {
        has_flow_ = true;

        const auto& init = new_block(BLOCK_FLOW_INIT);
        const auto& end  = new_block(BLOCK_FLOW_END );

        // initialize
        ensure_dead_code();
        init_transition_normal(current, init);

        // flow
        init_transition_flow(init, end, node.objects);

        // flow end
        current = end;
    }

    void generate(tree::Statement& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(&node);
            return;
        }

        revisit(node);
    }

    void generate(tree::WhileStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(&node);
            return;
        }

        const auto& cond = new_block(BLOCK_WHILE_COND);
        const auto& body = new_block(BLOCK_WHILE_BODY);
        const auto& end  = new_block(BLOCK_WHILE_END );

        const auto& init = node.style == tree::WhileStmt::Style::WHILE ? cond : body;

        push_continue_break(init, end);

        // initialize
        ensure_dead_code();
        init_transition_normal(current, init);

        // condition
        current = cond;
        init_transition_condition(current, node.cond, body, end);

        // loop body
        current = body;
        visit(*node.block);
        init_transition_normal_if_unknown(current, cond);

        // loop end
        current = end;

        pop_continue_break();
    }

    void generate(tree::SwitchStmt& node)
    {
        if(!on_flow_tree(node)) {
            append_statement(&node);
            return;
        }

        BOOST_ASSERT(node.node != nullptr && "null pointer exception");

        const auto& cond = new_block(BLOCK_SWITCH_COND);
        const auto& end  = new_block(BLOCK_SWITCH_END );

        push_continue_break(end);

        // initialize
        ensure_dead_code();
        init_transition_normal(current, cond);

        // switch condition
        init_transition_case(cond, end, *node.node, node.cases, node.default_block);

        // switch end
        current = end;

        pop_continue_break();
    }

    void generate(tree::BranchStmt& node)
    {
        switch(node.opcode) {
            case tree::BranchStmt::OpCode::BREAK: {
                const auto& end = *get_current_continue_break().second;

                ensure_dead_code();
                init_transition_normal(current, end);
                break;
            }
            case tree::BranchStmt::OpCode::CONTINUE: {
                const auto& end = *get_current_continue_break().first;

                ensure_dead_code();
                init_transition_normal(current, end);
                break;
            }
            case tree::BranchStmt::OpCode::RETURN:
                append_statement(&node);
                init_transition_null(current);
                break;
        }
    }

    bool has_flow() const noexcept {
        return has_flow_;
    }

    std::unique_ptr<flow_spliter_detail::graph_t> steal_flow_graph() noexcept
    {
        return std::move(graph);
    }

private:
    void push_continue_break(block_t dst_break)
    {
        continue_break_stack.push(
            std::make_pair(
                boost::none,
                std::move(dst_break)
            )
        );
    }

    void push_continue_break(block_t dst_continue, block_t dst_break)
    {
        continue_break_stack.push(
            std::make_pair(
                std::move(dst_continue),
                std::move(dst_break   )
            )
        );
    }

    void pop_continue_break()
    {
        BOOST_ASSERT(!continue_break_stack.empty() && "stack is empty!?");

        continue_break_stack.pop();
    }

    std::pair<boost::optional<block_t>, boost::optional<block_t>> get_current_continue_break()
    {
        BOOST_ASSERT(!continue_break_stack.empty() && "stack is empty!?");

        return continue_break_stack.top();
    }

    bool on_flow_tree(tree::ASTNode& node) {
        return flow_tree_nodes.count(&node) != 0;
    }

    void append_statement(tree::Statement* stmt)
    {
        BOOST_ASSERT(stmt != nullptr && "null pointer exception");

        ensure_dead_code();

        (*graph)[current].statements.emplace_back(stmt);
    }

    block_t new_block(int tag)
    {
        BOOST_ASSERT(graph != nullptr && "null pointer exception");

        auto block = boost::add_vertex(*graph);

        (*graph)[block].tag = tag;

        return std::move(block);
    }

    link_t new_link(block_t from, block_t to)
    {
        BOOST_ASSERT(graph != nullptr && "null pointer exception");

              auto  insertion_result = boost::add_edge(from, to, *graph);
              auto& link             = insertion_result.first;
        const auto& is_inserted      = insertion_result.second;

        return std::move(link);
    }

    template<typename range_type>
    void init_transition_case(block_t from, block_t to, tree::Expression& cond_expr, const range_type& sub_selections, tree::Block* sub_default)
    {
        BOOST_ASSERT((*graph)[from].transition == TRANS_UNKNOWN && "unexpectedly change of transition");

        for (const tree::Selection*const sub_selection : sub_selections)
        {
            BOOST_ASSERT(sub_selection        != nullptr && "null pointer exception");
            BOOST_ASSERT(sub_selection->cond  != nullptr && "null pointer exception");
            BOOST_ASSERT(sub_selection->block != nullptr && "null pointer exception");

            const auto&      sub = new_block(BLOCK_SWITCH_CASE);
            const auto& link_sub = new_link(from, sub);

            (*graph)[link_sub].cond_expr = sub_selection->cond;

            current = sub;
            visit(*sub_selection->block);
            init_transition_normal_if_unknown(current, to);
        }

        const auto& def = new_block(BLOCK_SWITCH_DEFAULT);

        new_link(from, def);

        current = def;
        if (sub_default != nullptr)
            visit(*sub_default);
        init_transition_normal_if_unknown(current, to);

        (*graph)[from].cond_expr  = &cond_expr;
        (*graph)[from].transition = TRANS_CASE;
    }

    void init_transition_condition(block_t from, tree::Expression* cond_expr, block_t dst_true, block_t dst_false)
    {
        BOOST_ASSERT(cond_expr != nullptr && "null pointer exception");
        BOOST_ASSERT((*graph)[from].transition == TRANS_UNKNOWN && "unexpectedly change of transition");

        new_link(from, dst_true );
        new_link(from, dst_false);

        (*graph)[from].cond_expr  = cond_expr;
        (*graph)[from].transition = TRANS_CONDITION;
    }

    template<typename range_type>
    void init_transition_flow(block_t from, block_t to, const range_type& flowed_stmts)
    {
        BOOST_ASSERT((*graph)[from].transition == TRANS_UNKNOWN && "unexpectedly change of transition");

        for (tree::Statement*const flowed_stmt : flowed_stmts)
        {
            const auto& flow_sub = new_block(BLOCK_FLOW_SUB);

            // sub block
            current = flow_sub;
            append_statement(flowed_stmt);

            new_link(from    , flow_sub);
            new_link(flow_sub, to      );

            (*graph)[flow_sub].transition = TRANS_MERGE;
        }

        (*graph)[from].transition = TRANS_FLOW;
    }

    void init_transition_normal(block_t from, block_t to)
    {
        BOOST_ASSERT((*graph)[from].transition == TRANS_UNKNOWN && "unexpectedly change of transition");

        new_link(from, to);

        (*graph)[from].transition = TRANS_NORMAL;
    }

    void init_transition_normal_if_unknown(block_t from, block_t to)
    {
        if ((*graph)[from].transition != TRANS_UNKNOWN)
            return;

        init_transition_normal(from, to);
    }

    void init_transition_null(block_t block)
    {
        BOOST_ASSERT((*graph)[block].transition == TRANS_UNKNOWN && "unexpectedly change of transition");

        (*graph)[block].transition = TRANS_NULL;
    }

    void ensure_dead_code() {
        if ((*graph)[current].transition != TRANS_UNKNOWN)
            current = new_block(BLOCK_DEAD_CODE);
    }

    bool                     has_flow_;
    std::set<tree::ASTNode*> flow_tree_nodes;

    std::unique_ptr<flow_spliter_detail::graph_t>                             graph;
    block_t                                                                   current;
    block_t                                                                   func_end;
    std::stack<std::pair<boost::optional<block_t>, boost::optional<block_t>>> continue_break_stack;
};

std::set<tree::VariableDecl*> collect_declared_variables(const graph_t& graph, const block_t& block)
{
    std::set<tree::VariableDecl*> decl_variables;

    // defs in this basic block
    for (auto*const stmt : graph[block].statements)
    {
        tree::ASTNodeHelper::foreachApply<tree::VariableDecl>(
            *stmt,
            [&decl_variables](tree::VariableDecl& var_decl)
            {
                decl_variables.emplace(&var_decl);
            }
        );
    }

    return std::move(decl_variables);
}

std::set<tree::VariableDecl*> collect_referenced_variables(graph_t& graph, const block_t& block)
{
    std::set<tree::VariableDecl*> refed_variables;
    const auto&                   collector       = [&refed_variables](tree::ASTNode& node)
    {
        tree::ASTNodeHelper::foreachApply<tree::IdExpr>(
            node,
            [&refed_variables](tree::IdExpr& id)
            {
                auto*const symbol   = tree::ASTNodeHelper::getCanonicalSymbol(&id);
                auto*const var_decl = tree::cast_or_null<tree::VariableDecl>(symbol);

                if (var_decl != nullptr && var_decl->isLocal())
                    refed_variables.emplace(var_decl);
            }
        );
    };

    // refs in this basic block
    for (auto*const stmt : graph[block].statements)
        collector(*stmt);

    // refs in condition expressions on conditional statements
    if (auto*const cond_expr = graph[block].cond_expr)
        collector(*cond_expr);

    // refs in case expression of switch statement
    for (const auto& trans_info : boost::make_iterator_range(boost::out_edges(block, graph)))
        if (auto*const cond_expr = graph[trans_info].cond_expr)
            collector(*cond_expr);

    return std::move(refed_variables);
}

void init_used_variables(graph_t& graph, const block_t& block)
{
    auto& decl_variables = graph[block].decl_variables;
    auto& used_variables = graph[block].used_variables;

    decl_variables = collect_declared_variables(graph, block);
    used_variables = collect_referenced_variables(graph, block);

    for (const auto& decl_variable : decl_variables)
        used_variables.erase(decl_variable);
}

void init_used_variables(graph_t& graph)
{
    for (const auto& block : boost::make_iterator_range(boost::vertices(graph)))
        init_used_variables(graph, block);
}

bool propagate_used_variables(graph_t& graph, block_t from, block_t to)
{
    const auto& used_variables_from = graph[from].used_variables;
          auto& used_variables_to   = graph[to  ].used_variables;
    const auto& decl_variables_to   = graph[to  ].decl_variables;

    const auto& non_decl_variables = make_set_difference_range(used_variables_from, decl_variables_to);
    const auto& old_count          = used_variables_to.size();

    used_variables_to.insert(boost::const_begin(non_decl_variables), boost::const_end(non_decl_variables));

    return old_count != used_variables_to.size();
}

bool propagate_used_variables(graph_t& graph)
{
    bool had_propagated = false;

    for (const auto& block : boost::make_iterator_range(boost::vertices(graph)))
    {
        if (graph[block].transition == TRANS_FLOW)
        {
            auto flow_links = boost::make_iterator_range(boost::out_edges(block, graph));
            BOOST_ASSERT(!flow_links.empty() && "there must be at least one sub flow!");

            const auto& first_sub_flow = boost::target(flow_links.front(), graph);
            BOOST_ASSERT(boost::out_degree(first_sub_flow, graph) == 1 && "sub flow must contain exactly one merge transition to another block");

            const auto& merger = boost::target(*boost::out_edges(first_sub_flow, graph).first, graph);

            if (propagate_used_variables(graph, merger, first_sub_flow))
                had_propagated = true;
        }

        if (graph[block].transition != TRANS_MERGE)
        {
            for (const auto& link : boost::make_iterator_range(boost::out_edges(block, graph)))
            {
                const auto& target = boost::target(link, graph);

                if (propagate_used_variables(graph, target, block))
                    had_propagated = true;
            }
        }
    }

    return had_propagated;
}

using optimizer_t = std::function<bool(graph_t&, const block_t&)>;

bool loop_optimizations_single_step(graph_t& graph, const optimizer_t& optimizer)
{
    BOOST_ASSERT(optimizer != nullptr && "null pointer exception");

    bool has_removed_any = false;

    BOOST_ASSERT(boost::num_vertices(graph) >= 1 && "there must be at least one block for entry");

    // Important, use index to avoid invalidated iterators after boost::remove_vertex
    for (graph_t::vertices_size_type i = 1; i != boost::num_vertices(graph); )
    {
        const auto& block = boost::vertex(i, graph);

        if (optimizer(graph, block))
        {
            boost::clear_vertex(block, graph);
            boost::remove_vertex(block, graph);

            has_removed_any = true;
        }
        else
        {
            ++i;
        }
    }

    return has_removed_any;
}

bool loop_optimizations(graph_t& graph, const std::vector<optimizer_t>& optimizers)
{
    bool has_optimized = false;

    while (true)
    {
        bool has_optimized_inner = false;

        for (const auto& optimizer : optimizers)
            if (loop_optimizations_single_step(graph, optimizer))
                has_optimized_inner = true;

        if (!has_optimized_inner)
            break;

        has_optimized = true;
    }

    return has_optimized;
}

bool try_propagate_empty_null_transition(graph_t& graph, const block_t& block)
{
    const auto& block_info = graph[block];

    if (block_info.transition != TRANS_NULL)
        return false;

    if (!block_info.statements.empty())
        return false;

    for (const auto& in_link : boost::make_iterator_range(boost::in_edges(block, graph)))
    {
        const auto& source      = boost::source(in_link, graph);
              auto& source_info = graph[source];

        if (source_info.transition != TRANS_NORMAL)
            continue;

        source_info.transition = TRANS_NULL;

        boost::remove_edge(in_link, graph);
    }

    return false; // 'block' should not be removed now, return 'false'
}

bool try_merge_block(graph_t& graph, const block_t& block)
{
    auto& block_info = graph[block];

    const auto& in_links = boost::in_edges(block, graph);

    if (boost::distance(in_links) != 1)
        return false;

    const auto& in_link   = *boost::const_begin(in_links);
    const auto& pred      = boost::source(in_link, graph);
          auto& pred_info = graph[pred];

    if (pred_info.transition != TRANS_NORMAL)
        return false;

    if (boost::out_degree(pred, graph) != 1)
        return false;

    for (const auto& out_link : boost::make_iterator_range(boost::out_edges(block, graph)))
    {
        const auto& succ          = boost::target(out_link, graph);
              auto& out_link_info = graph[out_link];

        boost::add_edge(pred, succ, std::move(out_link_info), graph);
    }

    graph[pred].transition = graph[block].transition;
    graph[pred].cond_expr  = graph[block].cond_expr ;

    boost::push_back(graph[pred].statements, graph[block].statements);

    return true;
}

bool try_remove_dangling_empty_block(graph_t& graph, const block_t& block)
{
    const auto& block_info = graph[block];

    if (!block_info.statements.empty())
        return false;

    if (boost::in_degree(block, graph) != 0)
        return false;

    if (boost::out_degree(block, graph) != 0)
        return false;

    return true;
}

}

std::unique_ptr<graph_t> flow_split(tree::FunctionDecl& func)
{
    if(!func.is_task) {
        return nullptr;
    }

    FlowGraphGeneratorVisitor flow_graph_generator(func.name->toString());
    flow_graph_generator.visit(func);

    if(!flow_graph_generator.has_flow()) {
        return nullptr;
    }

    auto fg = flow_graph_generator.steal_flow_graph();

    // TODO optimize need_this(), current, all blocks splited from member function are need_this.
    if(func.is_member && !func.is_static)
        (*fg)[boost::graph_bundle].need_this = true;

    optimize_all(*fg);

    init_used_variables(*fg);
    while (propagate_used_variables(*fg));

    return std::move(fg);
}

bool optimize_erase_empty_or_null(graph_t& graph)
{
    return loop_optimizations(
        graph,
        {
            try_propagate_empty_null_transition,
            try_remove_dangling_empty_block    ,
        }
    );
}

bool optimize_merge_block(graph_t& graph)
{
    return loop_optimizations(
        graph,
        {
            try_merge_block,
        }
    );
}

bool optimize_all(graph_t& graph)
{
    return loop_optimizations(
        graph,
        {
            try_propagate_empty_null_transition,
            try_merge_block                    ,
            try_remove_dangling_empty_block    ,
        }
    );
}

void dump(const graph_t& graph, const char* filepath)
{
    std::ofstream output(filepath);

    return dump(graph, output);
}

void dump(const graph_t& graph, const std::string& filepath)
{
    std::ofstream output(filepath);

    return dump(graph, output);
}

void dump(const graph_t& graph, std::ostream& output)
{
    boost::write_graphviz(
        output,
        graph,
        block_info_writer(graph),
        link_info_writer(graph)
    );
}

} } } } }
