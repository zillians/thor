/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "language/stage/transformer/ImplicitConversionStage.h"
#include "language/stage/transformer/visitor/ImplicitConversionStageVisitor.h"

namespace zillians { namespace language { namespace stage {

ImplicitConversionStage::ImplicitConversionStage() :
    dump_graphviz(false),
    keep_going(false)
{ }

ImplicitConversionStage::~ImplicitConversionStage()
{ }

const char* ImplicitConversionStage::name()
{
    return "Implicit Conversion Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ImplicitConversionStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_private->add_options()
        ("debug-restructure-stage", "debug restructure stage")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ImplicitConversionStage::parseOptions(po::variables_map& vm)
{
    debug = (vm.count("debug-restructure-stage") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }

    if(vm.count("mode-implicit-cast") > 0)
    {
        keep_going = true;
    }

    return true;
}

bool ImplicitConversionStage::execute(bool& continue_execution)
{
    if(keep_going)
        continue_execution = true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    BOOST_ASSERT(parser_context.tangle && "no tangle!");

    static int phase = 0;
	++phase;

    report.reset();
    if(parser_context.tangle)
    {
        boost::filesystem::path graphviz_path(dump_graphviz_dir);

        // restructure the entire tree in multiple passes
        if(dump_graphviz)
        {
            std::stringstream ss;
            ss << "pre-implicit-conversion-" << (phase-1) << ".dot";
            tree::ASTNodeHelper::visualize(parser_context.tangle, graphviz_path / ss.str());
        }

        visitor::ImplicitConversionStageVisitor implicitConvert;
        implicitConvert.visit(*parser_context.tangle);

		report.cast_expr_collection = implicitConvert.getCastExprCollection();

        if(dump_graphviz)
        {
            std::stringstream ss;
            ss << "post-implicit-conversion-" << (phase-1) << ".dot";
            tree::ASTNodeHelper::visualize(parser_context.tangle, graphviz_path / ss.str());
        }

        if(debug)
        {
            tree::visitor::PrettyPrintVisitor printer;
            printer.visit(*parser_context.tangle);
        }

        return !(LoggerWrapper::instance()->hasError() || LoggerWrapper::instance()->hasFatal());
    }
    else
    {
        return false;
    }
}

bool ImplicitConversionStage::hasProgress()
{
    return (report.cast_expr_collection.size() > 0);
}

} } }
