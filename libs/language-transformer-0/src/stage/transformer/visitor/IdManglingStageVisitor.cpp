/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <tuple>

#include <boost/assert.hpp>

#include "language/context/ResolverContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/module/Tangle.h"

#include "language/stage/transformer/visitor/IdManglingStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using tree::cast;
using tree::isa;

namespace {

void updateIdLiteral(tree::FunctionIdLiteral& id_literal)
{
    if (!id_literal.tangle_uuid.is_nil())
        return;

    const auto*const symbol        = ResolvedSymbol::get(&id_literal);
    const auto*const tangle        = symbol != nullptr ? symbol->getOwner<tree::Tangle>() : nullptr;
    const auto*const function_decl = tangle != nullptr ? cast<tree::FunctionDecl>(symbol) : nullptr;

    BOOST_ASSERT(symbol        != nullptr && "resolved symbol not found");
    BOOST_ASSERT(tangle        != nullptr && "tangle not found");
    BOOST_ASSERT(function_decl != nullptr && "resolved symbol of function id literal is not a function!?");

    const auto& found_result = tangle->getIdOfFunction(*function_decl);
    const auto& offseted_id  = found_result.first;
    const auto& is_found     = found_result.second;
    BOOST_ASSERT(is_found && "no offseted id for required function!?");

    std::tie(id_literal.tangle_uuid, id_literal.local_id) = offseted_id;
}

void updateIdLiteral(tree::TypeIdLiteral& id_literal)
{
    if (!id_literal.tangle_uuid.is_nil())
        return;

    const auto*const symbol         = ResolvedSymbol::get(&id_literal);
    const auto*const tangle         = symbol != nullptr ? symbol->getOwner<tree::Tangle>() : nullptr;
    const auto*const canonical_type = (symbol != nullptr && isa<tree::VariableDecl>(symbol)) ? cast<tree::VariableDecl>(symbol)->getCanonicalType() : nullptr;
    const auto*const class_decl     = canonical_type != nullptr ? canonical_type->getAsClassDecl() : nullptr;

    BOOST_ASSERT(symbol     != nullptr && "resolved symbol not found");
    BOOST_ASSERT(tangle     != nullptr && "tangle not found");
    BOOST_ASSERT(class_decl != nullptr && "resolved symbol of type id literal is not a class!?");

    const auto& found_result = tangle->getIdOfType(*class_decl);
    const auto& offseted_id  = found_result.first;
    const auto& is_found     = found_result.second;
    BOOST_ASSERT(is_found && "no offseted id for required function!?");

    std::tie(id_literal.tangle_uuid, id_literal.local_id) = offseted_id;
}

}

bool IdManglingStageVisitor::traverseClassDecl(tree::ClassDecl& node)
{
    if (!node.isCompleted())
        return true;

    return base_visitor::traverseClassDecl(node);
}

bool IdManglingStageVisitor::traverseFunctionDecl(tree::FunctionDecl& node)
{
    if (!node.isCompleted())
        return true;

    return base_visitor::traverseFunctionDecl(node);
}

bool IdManglingStageVisitor::visitFunctionIdLiteral(tree::FunctionIdLiteral& node)
{
    updateIdLiteral(node);

    return base_visitor::visitFunctionIdLiteral(node);
}

bool IdManglingStageVisitor::visitTypeIdLiteral(tree::TypeIdLiteral& node)
{
    updateIdLiteral(node);

    return base_visitor::visitTypeIdLiteral(node);
}

} } } } // namespace 'zillians::language::stage::visitor'
