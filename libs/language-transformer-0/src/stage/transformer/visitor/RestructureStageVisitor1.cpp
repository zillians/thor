/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>

#include <boost/range/adaptor/filtered.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>
#include <boost/assert.hpp>

#include "utility/StringUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/context/ManglingStageContext.h"
#include "language/context/TransformerContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/transformer/detail/RestructureHelper.h"
#include "language/stage/transformer/visitor/RestructureStageVisitor1.h"
#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using tree::cast;
using tree::isa;

namespace {

void get_array_dimensions_impl(tree::ArrayExpr& node, std::vector<size_t>& dimensions)
{
    size_t dim = node.getDimension();
    dimensions.push_back(node.elements.size());
    BOOST_ASSERT(dim >= 1);
    if(dim > 1)
    {
        tree::ArrayExpr* elem = cast<tree::ArrayExpr>(node.elements[0]);
        BOOST_ASSERT(elem != nullptr);
        get_array_dimensions_impl(*elem, dimensions);
    }
}

// check only dim : if 1D
// check dim and recursivly element dim : if >= 2D 
bool check_array_dimensions(tree::ArrayExpr& node, const std::vector<size_t>& dimensions, size_t cur_dim_idx)
{
    size_t expected_size = dimensions[cur_dim_idx];
    if(node.elements.size() != expected_size)
    {
        LOG_MESSAGE(MULTIDIMENSION_ARRAY_SIZE_NOT_CONSISTENT, &node, _expect(std::to_wstring(expected_size)), _but_get(std::to_wstring(node.elements.size())));
        return false;
    }

    if(cur_dim_idx + 1 < dimensions.size())
    {
        for(auto e : node.elements)
        {
            tree::ArrayExpr* elem = cast<tree::ArrayExpr>(e);
            BOOST_ASSERT(elem != nullptr);
            if(!check_array_dimensions(*elem, dimensions, cur_dim_idx + 1))
                return false;
        }
    }

    return true;
}

// return dimension of the array
// return an empty array if the size is not consistent
std::vector<size_t> get_array_dimensions(tree::ArrayExpr& node)
{
    std::vector<size_t> dimensions;
    get_array_dimensions_impl(node, dimensions);
    bool check_result = check_array_dimensions(node, dimensions, 0);
    if(!check_result)
    {
        dimensions.clear();
        return dimensions;
    }
    BOOST_ASSERT(dimensions.size() == node.getDimension());
    return dimensions;
}

void set_one_array_elem(tree::SimpleIdentifier* _t, const std::vector<size_t>& indexes, tree::Expression* expr, tree::BlockExpr* block_expr)
{
    tree::CallExpr* set_elem_call = new tree::CallExpr(
        new tree::MemberExpr(new tree::IdExpr(tree::ASTNodeHelper::clone(_t)), new tree::SimpleIdentifier(L"set"))
    );
    for(auto e : indexes)
    {
        set_elem_call->appendParameter(new tree::NumericLiteral(int64(e)));
    }
    set_elem_call->appendParameter(expr);
    tree::ExpressionStmt* set_elem_value = new tree::ExpressionStmt(set_elem_call);
    block_expr->block->appendObject(set_elem_value);
    tree::ASTNodeHelper::foreachApply<tree::ASTNode>(*set_elem_value, [&expr](tree::ASTNode& n) {
        if(SourceInfoContext::get(&n) == nullptr)
            tree::ASTNodeHelper::propogateSourceInfo(n, *expr);
    });
}

// if reach the terminal expression, create 'arr.set(idx1, idx2, ..., value)'
// else, iterate each element in the array
void set_array_elements_impl(tree::SimpleIdentifier* _t, const std::vector<size_t>& dimensions, std::vector<size_t>& indexes, tree::Expression* expr, tree::BlockExpr* block_expr)
{
    if(indexes.size() == dimensions.size())
    {
        set_one_array_elem(_t, indexes, expr, block_expr);
    }
    else
    {
        tree::ArrayExpr* arr = cast<tree::ArrayExpr>(expr);
        BOOST_ASSERT(arr != nullptr);
        for(size_t i = 0; i < arr->elements.size(); ++i)
        {
            indexes.push_back(i);
            set_array_elements_impl(_t, dimensions, indexes, arr->elements[i], block_expr);
            indexes.pop_back();
        }
    }
}

void set_array_elements(tree::SimpleIdentifier* _t, tree::ArrayExpr* arr, const std::vector<size_t>& dimensions, tree::BlockExpr* block_expr)
{
    std::vector<size_t> indexes;
    set_array_elements_impl(_t, dimensions, indexes, arr, block_expr);
}

tree::Type* getThorLangObjectType(const tree::ASTNode& any)
{
    auto* tangle = any.getOwner<tree::Tangle>();
    BOOST_ASSERT( tangle != nullptr && "cannot find toppest tangle object" );

    auto* thor_lang_object = tangle->findThorLangObject();
    BOOST_ASSERT( thor_lang_object != nullptr && "cannot find declaration of thor.lang.Object under Tangle" );

    return thor_lang_object->getType();
}

} // anonymous namespace

RestructureStageVisitor1::RestructureStageVisitor1(bool no_system_bundle) : no_system_bundle(no_system_bundle)
{
    REGISTER_ALL_VISITABLE_ASTNODE(restructInvoker);
}

void RestructureStageVisitor1::restruct(tree::ASTNode& node)
{
    revisit(node);
}

void RestructureStageVisitor1::restruct(tree::FunctionDecl& node)
{
    if(isa<tree::TemplatedIdentifier>(node.name) && !cast<tree::TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    revisit(node);

    // Handle @bind_function_id annotation, which specifiy a variable to store the specific function id
    //
    // @bind_function_id { name = "fid" }
    // function foo();
    //
    // will add salts into constructor (if foo() is member function), that is,
    //
    // function new() {
    //     fid = FunctionIdLiteral(foo);
    // }
    //
    // If foo() is global function, the salt will be added in global_init(). If foo() is static class member function, the place is static_init().
    if (tree::Annotation* bind_function_id = node.findAnnotation(L"bind_function_id"))
    {
        transforms.push_back([&, bind_function_id](){
            // TODO: consider other cases (static class function, global function)

            // annotation->bound_variable->string_literal
            auto assigned_variable = cast<tree::StringLiteral>(bind_function_id->attribute_list[0].second);
            auto assigned_function = cast<tree::StringLiteral>(bind_function_id->attribute_list[1].second);

            // assign to variable fid = function_id_literal;
            auto function_id_literal = new tree::FunctionIdLiteral(L"invoke_function_id");
            ResolvedSymbol::set(function_id_literal, &node);

            auto left_expr = new tree::IdExpr(new tree::SimpleIdentifier(assigned_variable->value));
            auto binary_expr = new tree::BinaryExpr(tree::BinaryExpr::OpCode::ASSIGN, left_expr, function_id_literal);
            auto statement = new tree::ExpressionStmt(binary_expr);

            // Now, put the expression into right place
            if (node.is_member && !node.is_static)
            {
                // insert the expression into each constructor
                auto class_decl = node.getOwner<tree::ClassDecl>();
                for(auto* method : class_decl->member_functions)
                {
                    if (method->name->toString() != assigned_function->value) continue;
                    BOOST_ASSERT(method->block && "No, you specificy the init function but don't give me block");
                    method->block->appendObject(statement);
                }
            }

            // and remove the annotation 
            node.delAnnotation(L"bind_function_id");
        });
    }
}

void RestructureStageVisitor1::tryGenerateFactoryMethod(tree::ClassDecl& class_)
{
    // skip if cyclic inheritance
    if(!class_.isVirtualTableReady())
        return;

    // skip if meet abstract class
    if(class_.hasPureVirtual())
        return;

    // skip if class has no default ctor
    if(!class_.hasDefaultConstructor())
        return;

    // skip if meet non-system class
    if(!class_.hasAnnotation(L"system"))
        return;

    static const wchar_t* CREATOR_NAME = L"__create";

    static auto already_has_factory_method = [](tree::ClassDecl& class_){
        return boost::algorithm::any_of(
            class_.member_functions,
            [](tree::FunctionDecl* method){
                BOOST_ASSERT(method != nullptr);

                auto* identifier = method->name->getSimpleId();
                BOOST_ASSERT(identifier != nullptr);

                return identifier->name == CREATOR_NAME;
            }
        );
    };

    if(already_has_factory_method(class_))
        return;

    transforms.push_back([&](){
        // (1) create factory method
        auto* creator_name = new tree::SimpleIdentifier(CREATOR_NAME);
        auto* creator_type = new tree::NamedSpecifier(class_.name->clone());
        auto* creator_body = new tree::NormalBlock();

        bool is_member, is_static, is_virtual, is_override;
        auto* creator = new tree::FunctionDecl(
            creator_name,
            creator_type,
            is_member=true, is_static=true, is_virtual=false, is_override=false,
            tree::Declaration::VisibilitySpecifier::PRIVATE,
            creator_body
        );

        // (2) prepare the body of factory method
        // add statement: return new T;
        auto* new_object = new tree::UnaryExpr(
            tree::UnaryExpr::OpCode::NEW,
            new tree::IdExpr(class_.name->clone())
        );

        creator_body->appendObject(
            new tree::BranchStmt(tree::BranchStmt::OpCode::RETURN, new_object)
        );

        // (3) copy other information from class
        auto* inherited_annotations = clone_or_null(class_.getAnnotations());
        // force callable generating
        inherited_annotations->deleteAnnotation(L"system");
        inherited_annotations->deleteAnnotation(L"native");
        creator->setAnnotations(inherited_annotations);

        tree::ASTNodeHelper::propogateArchitecture(*creator);
        tree::ASTNodeHelper::foreachApply<tree::ASTNode>(
            *creator,
            [&class_](tree::ASTNode& sub_node){
                tree::ASTNodeHelper::propogateSourceInfo(sub_node, class_);
            }
        );

        // (4) add factory method into class, restructure after adding
        class_.addFunction(creator);
        restructure_helper::restructureUnaryNewDirect(*new_object);
    });
}

void RestructureStageVisitor1::restruct(tree::ClassDecl& node)
{
    using boost::adaptors::filtered;

    if(isa<tree::TemplatedIdentifier>(node.name) && !cast<tree::TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    revisit(node);

    if (!node.isVirtualTableReady() && node.buildVirtualTable())
        transforms.emplace_back([]{});

    tryGenerateFactoryMethod(node);
}

void RestructureStageVisitor1::restruct(tree::VariableDecl& node)
{
    revisit(node);

    if(node.initializer)
    {
        restructure_helper::splitVariableInitializer(node, transforms);
        status.split_reference_count++;
    }
    else
    {
        bool is_function_parameter = false;
        if (tree::FunctionDecl* func = cast<tree::FunctionDecl>(node.parent))
        {
            for(auto* param : func->parameters)
            {
                if (param == &node)
                {
                    is_function_parameter = true;
                    break;
                }
            }
        }

        // assign null initializer if the class type variable declaration does not have initializer
        // of course, it only applys to known resolved type
        tree::Type* resolved_type = node.getCanonicalType();
        if(resolved_type && !is_function_parameter && !SplitInverseReferenceContext::get(&node) && resolved_type->isRecordType())
        {
            auto class_decl = cast<tree::ClassDecl>(node.parent);
            if (!class_decl || (class_decl && !tree::ASTNodeHelper::hasNativeLinkage(class_decl)))
            {
                transforms.push_back([&](){
                    tree::Literal* literal = new tree::ObjectLiteral(tree::ObjectLiteral::LiteralType::NULL_OBJECT);
                    node.setInitializer(literal);

                    tree::ASTNodeHelper::propogateSourceInfo(*literal, node); // propagate the source info
                });
                status.assign_null_count++;
            }
        }
    }
}

void RestructureStageVisitor1::restruct(tree::ArrayExpr& node)
{
    // get dimension as arguments to call new()
    std::vector<size_t> dimensions = get_array_dimensions(node);
    // if dimensions is not consistent
    if(dimensions.empty())
    {
        terminate(); // stop the visitor
        return;
    }

    transforms.push_back([this, &node, dimensions](){
        // restruct
        //
        //      <int32>[12, 33, 75]
        //
        // to
        //
        //      {                                       // [1]
        //          var _t : Array{N-D}<int32>;         // [2]
        //          _t = new Array{N-D}<int32>(3, ...); // [3]
        //          _t.set(0, 0, 12);                   // [4]
        //          _t.set(0, 1, 33);                   // [4]
        //          _t.set(1, 0, 75);                   // [4]
        //          _t.set(1, 1, 75);                   // [4]
        //          _t;                                 // [5]
        //      }

        // [1] create block
        tree::BlockExpr* block_expr = new tree::BlockExpr(new tree::NormalBlock(), "array_expr");
        tree::ASTNodeHelper::propogateSourceInfo(*block_expr, node);
        tree::ASTNodeHelper::propogateSourceInfo(*block_expr->block, node);
        // [2] var _t : Array{N-D}<int32>;
        UUID uuid = UUID::random();
        tree::SimpleIdentifier* _t = new tree::SimpleIdentifier(L"array_temp_" + s_to_ws(uuid.toString('_')));
        tree::VariableDecl* var_decl = new tree::VariableDecl(tree::ASTNodeHelper::clone(_t), node.array_type, false, false, false, tree::Declaration::VisibilitySpecifier::DEFAULT);
        tree::DeclarativeStmt* decl_t = new tree::DeclarativeStmt(var_decl);
        tree::ASTNodeHelper::propogateSourceInfo(*_t, node);
        tree::ASTNodeHelper::propogateSourceInfo(*var_decl, node);
        tree::ASTNodeHelper::propogateSourceInfo(*decl_t, node);
        block_expr->block->appendObject(decl_t);
        // [3]
        // member thor.lang.Array
        tree::Identifier* array_type_id = node.array_type->getName();
        tree::NestedIdentifier* array_type_nid = cast<tree::NestedIdentifier>(array_type_id);
        BOOST_ASSERT(array_type_nid != nullptr);
        BOOST_ASSERT(array_type_nid->identifier_list.size() == 3);
        tree::TemplatedIdentifier* array_type_last_tid = cast<tree::TemplatedIdentifier>(array_type_nid->identifier_list[2]);
        BOOST_ASSERT(array_type_last_tid != nullptr);
        tree::CallExpr* new_call_expr = new tree::CallExpr(
                                            new tree::MemberExpr(
                                                new tree::MemberExpr(
                                                    new tree::IdExpr(new tree::SimpleIdentifier(L"thor")),
                                                    new tree::SimpleIdentifier(L"lang")
                                                ),
                                                tree::ASTNodeHelper::clone(array_type_last_tid)
                                            )
                                        );
        for(size_t e : dimensions)
        {
            new_call_expr->appendParameter(new tree::NumericLiteral(int64(e)));
        }
        tree::ExpressionStmt* assign_t = new tree::ExpressionStmt(new tree::BinaryExpr(
                    tree::BinaryExpr::OpCode::ASSIGN, // =
                    new tree::IdExpr(tree::ASTNodeHelper::clone(_t)), // _t
                    new tree::UnaryExpr(tree::UnaryExpr::OpCode::NEW, new_call_expr) // thor.lang.Array<T>
        ));
        tree::ASTNodeHelper::foreachApply<tree::ASTNode>(*assign_t, [&node](tree::ASTNode& n) {
            if(SourceInfoContext::get(&n) == nullptr)
                tree::ASTNodeHelper::propogateSourceInfo(n, node);
        });
        block_expr->block->appendObject(assign_t);
        // [4]
        set_array_elements(_t, &node, dimensions, block_expr);
        // [5]
        tree::ExpressionStmt* last_value= new tree::ExpressionStmt(new tree::IdExpr(tree::ASTNodeHelper::clone(_t)));
        block_expr->block->appendObject(last_value);
        tree::ASTNodeHelper::propogateSourceInfo(*last_value, node);
        tree::ASTNodeHelper::propogateSourceInfo(*last_value->expr, node);
        // replace use with
        node.parent->replaceUseWith(node, *block_expr);
    });
}

void RestructureStageVisitor1::restruct(tree::BlockExpr& node)
{
    if(node.tag == "new_restructure")
    {
        restructNew(node);
    }
    else
    {
        revisit(node);
    }
}

void RestructureStageVisitor1::restruct(tree::UnaryExpr& node)
{
    revisit(node);

    if(node.opcode == tree::UnaryExpr::OpCode::NEW)
    {
        restructure_helper::restructureUnaryNew(node, transforms);
        status.new_restruct_count++;
    }
}

void RestructureStageVisitor1::restruct(tree::IsaExpr& node)
{
    revisit(node);

    // Restructure base on node.type
    // 1.Primitive Type        => same type
    // 2.Derived ClassDecl     => null check
    // 3.not derived ClassDecl => dynamic cast + null check
    // 4.Other                 => same type

    tree::Type* node_type   = node.node->getCanonicalType();
    tree::Type* target_type = node.type->getCanonicalType();

    BOOST_ASSERT(target_type && node_type && "type or not is not resolved!");

    bool is_node_primitive   = node_type->isPrimitiveType();
    bool is_target_primitive = target_type->isPrimitiveType();

    bool is_node_enum = node_type->isEnumType();
    bool is_target_enum = target_type->isEnumType();

    tree::ClassDecl* node_cls_type   = node_type->getAsClassDecl();
    tree::ClassDecl* target_cls_type = target_type->getAsClassDecl();

    enum
    {
        SAME_TYPE_CHECK,
        NULL_CHECK,
        DYN_CHECK,
    } check_method = SAME_TYPE_CHECK;

    if((is_node_primitive || is_node_enum) || (is_target_primitive || is_target_enum))
    {
        check_method = SAME_TYPE_CHECK;
    }
    else if(node_type == target_type)
    {
        check_method = NULL_CHECK;
    }
    else if(node_cls_type != NULL && target_cls_type != NULL)
    {
        if(tree::ASTNodeHelper::isInheritedFrom(node_cls_type, target_cls_type))
        {
            check_method = NULL_CHECK;
        }
        else
        {
            check_method = DYN_CHECK;
        }
    }
    else
    {
        check_method = SAME_TYPE_CHECK;
    }

    if(check_method == SAME_TYPE_CHECK)
    {
        transforms.push_back([&, node_type, target_type](){
            // isa<int8 >(i8) -> { i8; true ;}
            // isa<int16>(i8) -> { i8; false;}

            tree::Tangle* tangle = node.getOwner<tree::Tangle>();

            BOOST_ASSERT(tangle && "no tangle!");

            bool isa_result = node_type == target_type;

            tree::Expression* result_expr = new tree::NumericLiteral(isa_result);

            tree::Block*     new_block    = new tree::NormalBlock();
            tree::BlockExpr* new_expr     = new tree::BlockExpr(new_block, "isa_restructure");

            tree::Statement* node_stmt    = new tree::ExpressionStmt(node.node);
            tree::Statement* result_stmt  = new tree::ExpressionStmt(result_expr);

            new_block->appendObject(node_stmt);
            new_block->appendObject(result_stmt);

            tree::Type* bool_type = tangle->internal->getPrimitiveType(tree::PrimitiveKind::BOOL_TYPE);

            ResolvedType::set(new_expr, bool_type);
            ResolvedType::set(result_expr, bool_type);

            tree::ASTNodeHelper::propogateSourceInfo(*result_expr, node);
            tree::ASTNodeHelper::propogateSourceInfo(*new_block, node);
            tree::ASTNodeHelper::propogateSourceInfo(*new_expr, node);
            tree::ASTNodeHelper::propogateSourceInfo(*node_stmt, node);
            tree::ASTNodeHelper::propogateSourceInfo(*result_stmt, node);

            node.parent->replaceUseWith(node, *new_expr);
        });
    }
    else
    {
        transforms.push_back([&, check_method](){
            // need_dyn_cast : isa<Cls2>(cls1) -> cast<Cls2>(cls1) != NULL
            // otherwise     : isa<Cls >(cls ) ->            cls   != null

            tree::Tangle* tangle = node.getOwner<tree::Tangle>();

            BOOST_ASSERT(tangle && "no tangle!");

            tree::Expression* null_checked_expr = NULL;

            if(check_method == DYN_CHECK)
            {
                // cls1 -> cast<Cls2>(cls1)
                null_checked_expr = new tree::CastExpr(node.node, node.type, tree::CastExpr::CastMethod::DYNAMIC);

                tree::ASTNodeHelper::propogateSourceInfo(*null_checked_expr, node);
            }
            else
            {
                // cls  -> cls
                null_checked_expr = node.node;
            }

            tree::Expression* new_null = new tree::ObjectLiteral(tree::ObjectLiteral::LiteralType::NULL_OBJECT);
            tree::Expression* new_expr = new tree::BinaryExpr(tree::BinaryExpr::OpCode::COMPARE_NE, null_checked_expr, new_null);

            ResolvedType::set(new_expr, tangle->internal->getPrimitiveType(tree::PrimitiveKind::BOOL_TYPE));

            tree::ASTNodeHelper::propogateSourceInfo(*new_null, node);
            tree::ASTNodeHelper::propogateSourceInfo(*new_expr, node);

            node.parent->replaceUseWith(node, *new_expr);
        });
    }
	status.isa_restruct_count++;
}

bool RestructureStageVisitor1::hasTransforms()
{
    return (transforms.size() > 0);
}

void RestructureStageVisitor1::applyTransforms()
{
    for(auto& transform : transforms)
    {
        transform();
    }
    transforms.clear();
}

void RestructureStageVisitor1::getStatus(unsigned& split_reference_count, unsigned& assign_null_count, unsigned& system_api_transform_count,
               unsigned& typeid_resolved_count, unsigned& new_restruct_count,
               unsigned& isa_restruct_count, unsigned& generated_function_count)
{
    split_reference_count = status.split_reference_count;
    assign_null_count = status.assign_null_count;
    system_api_transform_count = status.system_api_transform_count;
    typeid_resolved_count = status.typeid_resolved_count;
    new_restruct_count = status.new_restruct_count;
    isa_restruct_count = status.isa_restruct_count;
    generated_function_count = status.generated_function_count;
}

void RestructureStageVisitor1::restructNew(tree::BlockExpr& node)
{
    BOOST_ASSERT(isa<tree::DeclarativeStmt>(*node.block->objects.begin()) && "invalid new block expression found");

    tree::TypeSpecifier* specified_type = NULL;
    if(isa<tree::DeclarativeStmt>(*node.block->objects.begin()))
    {
        tree::Declaration* declaration = cast<tree::DeclarativeStmt>(*node.block->objects.begin())->declaration;
        if(declaration && isa<tree::VariableDecl>(declaration))
        {
            specified_type = cast<tree::VariableDecl>(declaration)->type;
        }
    }

    BOOST_ASSERT(specified_type != NULL && "invalid new block expression found");

    tree::CallExpr* object_create_call = NULL;
    if(isa<tree::ExpressionStmt>(*(++node.block->objects.begin())))
    {
        tree::Expression* expr = cast<tree::ExpressionStmt>( *(++node.block->objects.begin()) )->expr;
        if(expr && isa<tree::BinaryExpr>(expr))
        {
            tree::Expression* rhs_expr = cast<tree::BinaryExpr>(expr)->right;
            if(rhs_expr && isa<tree::CastExpr>(rhs_expr))
            {
                object_create_call = cast<tree::CallExpr>(cast<tree::CastExpr>(rhs_expr)->node);
            }
        }
    }

    BOOST_ASSERT(object_create_call != NULL && "invalid new block expression found");
    BOOST_ASSERT(object_create_call->parameters.size() == 2 && "invalid new block expression found");

    tree::Type* resolved_type = specified_type->getCanonicalType();

    if(resolved_type)
    {
        if(tree::ClassDecl* class_decl = resolved_type->getAsClassDecl())
        {
            // we don't need to process non-fully specialized class template
            if(isa<tree::TemplatedIdentifier>(class_decl->name) && !cast<tree::TemplatedIdentifier>(class_decl->name)->isFullySpecialized())
                return;

            if(true)
            {
                tree::TypeIdLiteral* type_id_literal = cast<tree::TypeIdLiteral>(object_create_call->parameters[1]);

                BOOST_ASSERT(type_id_literal != NULL && "invalid new block expression found");

                ResolvedType::set(type_id_literal, resolved_type);
                status.typeid_resolved_count++;
            }
        }
        else
        {
            UNREACHABLE_CODE();
        }
    }
    else
    {
        // if we cannot find the canonical type for this type specifier
        // this is probably a non-fully-specialize class template...
        // then just ignore it
    }
}

RestructureStageVisitor1::Status::Status() 
    : split_reference_count(0)
    , assign_null_count(0)
    , system_api_transform_count(0)
    , typeid_resolved_count(0)
    , new_restruct_count(0)
    , isa_restruct_count(0)
    , generated_function_count(0)
{}

} } } }
