/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "language/stage/transformer/FlowSplitStage.h"
#include "language/stage/transformer/visitor/FlowSplitStageVisitor.h"

namespace zillians { namespace language { namespace stage {

FlowSplitStage::FlowSplitStage()
: dump_graphviz(false),
  keep_going(false)
{ }

FlowSplitStage::~FlowSplitStage()
{ }

const char*
FlowSplitStage::name()
{
    return "Flow Split Stage";
}

std::pair<
          shared_ptr<po::options_description>,
          shared_ptr<po::options_description>
         >
FlowSplitStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

   	option_desc_private->add_options()
        ("debug-flow-split-stage", "debug flow split stage")
        //("dump-graphviz", "dump AST in graphviz format")
        //("dump-graphviz-dir", po::value<std::string>(), "dump AST in graphviz format")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool
FlowSplitStage::parseOptions(po::variables_map& vm)
{
    debug = (vm.count("debug-flow-split-stage") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }
    if(vm.count("mode-flow-split") > 0)
    {
        keep_going = true;
    }

    return true;
}

bool
FlowSplitStage::execute(bool& continue_execution)
{

    if(keep_going)
        continue_execution = true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    BOOST_ASSERT(parser_context.tangle && "no tangle!");

    if(parser_context.tangle)
    {
        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / "pre-flow-split.dot");
            getParserContext().tangle->toSource(p / "pre-flow-split.t");
        }

        visitor::FlowSplitStageVisitor split_visitor;
        split_visitor.visit(*parser_context.tangle);

        has_progress = split_visitor.hasTransforms();

        if (has_progress)
            split_visitor.applyTransforms();

        if(debug)
        {
            tree::visitor::PrettyPrintVisitor printer;
            printer.visit(*parser_context.tangle);
        }

        if(dump_graphviz)
        {
            boost::filesystem::path p(dump_graphviz_dir);
            tree::ASTNodeHelper::visualize(getParserContext().tangle, p / "post-flow-split.dot");
            getParserContext().tangle->toSource(p / "post-flow-split.t");
        }

        return true;
    }
    else
    {
        return false;
    }
}

} } }
