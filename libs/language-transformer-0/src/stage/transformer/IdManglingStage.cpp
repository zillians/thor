/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/UUIDUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/context/ParserContext.h"
#include "language/tree/ASTNodeFwd.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/transformer/IdManglingStage.h"
#include "language/stage/transformer/visitor/IdManglingStageVisitor.h"

namespace zillians { namespace language { namespace stage {

IdManglingStage::IdManglingStage() : disable_mangling(false), dump_graphviz(false)
{ }

IdManglingStage::~IdManglingStage()
{ }

const char* IdManglingStage::name()
{
    return "ID Mangling Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> IdManglingStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options();

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
    ("skip-id-mangling", "skip id mangling stage");

    return std::make_pair(option_desc_public, option_desc_private);
}

bool IdManglingStage::parseOptions(po::variables_map& vm)
{
    disable_mangling = (vm.count("skip-id-mangling") > 0);
    dump_graphviz = vm["dump-graphviz"].as<bool>();
    if(vm.count("dump-graphviz-dir") > 0)
    {
        dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }

    return true;
}

bool IdManglingStage::execute(bool& continue_execution)
{
    using tree::ASTNodeHelper;
    using tree::FunctionDecl;
    using tree::TemplatedIdentifier;
    using tree::isa;
    using tree::cast;

    UNUSED_ARGUMENT(continue_execution);

    if(disable_mangling)
        return true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    if(parser_context.tangle)
    {
        boost::filesystem::path graphviz_path(dump_graphviz_dir);
        if(dump_graphviz)
        {
            ASTNodeHelper::visualize(parser_context.tangle, graphviz_path / "pre-id-mangling.dot");
        }

        visitor::IdManglingStageVisitor mangler;
        mangler.traverse(*parser_context.tangle);

        if(dump_graphviz)
        {
            ASTNodeHelper::visualize(parser_context.tangle, graphviz_path / "post-id-mangling.dot");
        }

        return true;
    }
    else
    {
        return false;
    }
}

} } }
