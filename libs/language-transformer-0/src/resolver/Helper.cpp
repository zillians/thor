/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/resolver/Helper.h"

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"

namespace zillians { namespace language { namespace resolution { namespace helper {

bool is_all_resolved(const tree::TemplatedIdentifier& template_id)
{
    for(tree::TypenameDecl* typename_decl: template_id.templated_type_list)
    {
        tree::Type* resolved_type = typename_decl->getCanonicalType();

        if(resolved_type == nullptr)
        {
            return false;
        }
    }

    return true;
}

bool is_all_resolved(const tree::ClassDecl& cls_decl)
{
    BOOST_ASSERT(cls_decl.name && "null pointer exception");

    tree::TemplatedIdentifier* template_id = cls_decl.name->getTemplateId();

    BOOST_ASSERT(template_id != nullptr && "non-template declaration!");

    return is_all_resolved(*template_id);
}

bool is_all_resolved(const tree::FunctionDecl& func_decl)
{
    BOOST_ASSERT(func_decl.name && "null pointer exception");

    tree::TemplatedIdentifier* template_id = func_decl.name->getTemplateId();

    BOOST_ASSERT(template_id != nullptr && "non-template declaration!");

    return is_all_resolved(*template_id) && is_all_resolved(func_decl.parameters);
}

bool is_all_resolved(const tree::VariableDecl& var_decl)
{
    BOOST_ASSERT(var_decl.type && "null pointer exception");

    return var_decl.type->getCanonicalType() != nullptr;
}

bool is_template(const tree::Declaration& decl)
{
    BOOST_ASSERT(decl.name && "null pointer exception");

    return decl.name->getTemplateId() != nullptr;
}

bool is_generic_template(const tree::Declaration& decl)
{
    BOOST_ASSERT(decl.name && "null pointer exception");

    tree::TemplatedIdentifier* decl_tid = decl.name->getTemplateId();

    BOOST_ASSERT(decl_tid != nullptr && "non-template declaration!");

    for(tree::TypenameDecl* decl_td: decl_tid->templated_type_list)
    {
        BOOST_ASSERT(decl_td && "null pointer exception");

        if(decl_td->specialized_type != nullptr)
        {
            return false;
        }
    }

    return true;
}

} } } }
