#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

add_library(thor-language-transformer-0 ${ZILLIANS_PREFERED_LIB_TYPE}
    src/resolver/Deduction.cpp
    src/resolver/Deductor.cpp
    src/resolver/Helper.cpp
    src/resolver/Resolver.cpp
    src/resolver/Specialization.cpp
    src/stage/transformer/ConstantFoldingStage.cpp
    src/stage/transformer/FlowSplitStage.cpp
    src/stage/transformer/IdManglingStage.cpp
    src/stage/transformer/ImplicitConversionStage.cpp
    src/stage/transformer/NameManglingStage.cpp
    src/stage/transformer/ResolutionStage.cpp
    src/stage/transformer/RestructureStage0.cpp
    src/stage/transformer/RestructureStage1.cpp
    src/stage/transformer/detail/AsyncHelper.cpp
    src/stage/transformer/detail/ConstantFolder.cpp
    src/stage/transformer/detail/DefaultValueHelper.cpp
    src/stage/transformer/detail/EnumDependencyWalker.cpp
    src/stage/transformer/detail/EnumValueResolver.cpp
    src/stage/transformer/detail/FlowSpliter.cpp
    src/stage/transformer/detail/FlowRestructurer.cpp
    src/stage/transformer/detail/MultiTypeHelper.cpp
    src/stage/transformer/detail/ObjectReplicationHelper.cpp
    src/stage/transformer/detail/OperatorOverloadingConfig.cpp
    src/stage/transformer/detail/RestructureHelper.cpp
    src/stage/transformer/detail/RestructureLambda0.cpp
    src/stage/transformer/detail/SymbolTableChain.cpp
    src/stage/transformer/visitor/IdManglingStageVisitor.cpp
    src/stage/transformer/visitor/ImplicitConversionStageVisitor.cpp
    src/stage/transformer/visitor/NameManglingStageVisitor.cpp
    src/stage/transformer/visitor/NameManglingVisitor.cpp
    src/stage/transformer/visitor/ResolutionStagePreambleVisitor.cpp
    src/stage/transformer/visitor/ResolutionStageVisitor.cpp
    src/stage/transformer/visitor/RestructureStageLambdaVisitor1.cpp
    src/stage/transformer/visitor/RestructureStageVisitor0.cpp
    src/stage/transformer/visitor/RestructureStageVisitor1.cpp
    )

target_link_libraries(thor-language-transformer-0
    thor-common-utility
    thor-common-core
    thor-language-general
    thor-language-tree
    thor-language-stage-base
    )

ADD_SUBDIRECTORY(test)
