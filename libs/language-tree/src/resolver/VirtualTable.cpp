/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdint>

#include <functional>
#include <limits>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <map>
#include <tuple>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/assert.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/irange.hpp>

#include "utility/Foreach.h"
#include "utility/Functional.h"
#include "utility/RangeUtil.h"

#include "language/resolver/VirtualTable.h"

#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Type.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"

namespace zillians { namespace language {

namespace {

bool could_be_virtual(const tree::FunctionDecl& member_function)
{
    NOT_NULL(member_function.name);

    return tree::isa<tree::SimpleIdentifier>(member_function.name);
};

virtual_table::signature_type generate_signature(const tree::FunctionDecl& member_function)
{
    NOT_NULL(member_function.name);
    BOOST_ASSERT(tree::isa<tree::SimpleIdentifier>(member_function.name) && "only member function with simple identifier could be virtual");

    using boost::adaptors::transformed;

    const auto& parameter_types = member_function.parameters
                                | transformed(std::bind(&tree::VariableDecl::getCanonicalType, std::placeholders::_1))
                                ;

    return virtual_table::signature_type(
        member_function.name->toString(),
        {parameter_types.begin(), parameter_types.end()}
    );
}

virtual_table::entry_info_type generate_vtable_entry_info(const virtual_table::entry_type& entry)
{
    BOOST_ASSERT(!entry.overriden_decls.empty() && "no any declaration for virtual table entry!?");

    return {
        entry.is_pure ? nullptr : entry.overriden_decls.back (),
                                  entry.overriden_decls.front()
    };
}

virtual_table::segment_info_type generate_vtable_segment_info(const virtual_table::segment_type& segment)
{
    using boost::adaptors::transformed;

    const auto& entry_infos = segment.entries
                            | transformed(&generate_vtable_entry_info)
                            ;

    return {segment.source_class, {entry_infos.begin(), entry_infos.end()}};
}

}

virtual_table::signature_type::signature_type()
    : name()
    , parameter_types()
{
}

virtual_table::signature_type::signature_type(std::wstring&& name, std::vector<tree::relinkable_ptr<tree::Type>>&& parameter_types)
    : name(std::move(name))
    , parameter_types(std::move(parameter_types))
{
}

std::wstring virtual_table::signature_type::to_string() const
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    return    name
            + L'('
            + boost::join(
                    parameter_types
                  | transformed(std::mem_fn(&tree::relinkable_ptr<tree::Type>::get_ptr))
                  | transformed(std::mem_fn(&tree::Type::toString)),
                  L", "
              )
            + L')'
            ;
}

bool virtual_table::signature_type::operator==(const signature_type& rhs) const noexcept
{
    return std::tie(name, parameter_types) == std::tie(rhs.name, rhs.parameter_types);
}

bool virtual_table::signature_type::operator<(const signature_type& rhs) const noexcept
{
    return std::tie(name, parameter_types) < std::tie(rhs.name, rhs.parameter_types);
}

bool virtual_table::empty() const noexcept
{
    BOOST_ASSERT(segments_ready && "test for empty is non-sense if segments not ready");

    return segments.empty();
}

bool virtual_table::is_ready() const noexcept
{
    return segments_ready;
}

bool virtual_table::has_virtual() const noexcept
{
    BOOST_ASSERT(is_ready() && "using this function before ready is non-sense");

    return !override_states.empty();
}

std::pair<std::uint32_t, bool> virtual_table::get_virtual_index(const tree::FunctionDecl& member_function) const
{
    BOOST_ASSERT(is_ready() && "using this function before ready is non-sense");

    const auto& pos = function_indices.find(tree::make_relinkable(&member_function));

    if (pos == function_indices.end())
        return {std::numeric_limits<std::uint32_t>::max(), false};
    else
        return {pos->second, true};
}

bool virtual_table::has_pure_virtual() const
{
    BOOST_ASSERT(is_ready() && "using this function before ready is non-sense");

    using boost::adaptors::map_values;

    return boost::algorithm::any_of(
        override_states | map_values,
        std::mem_fn(&overriden_state::is_pure)
    );
}

std::set<const tree::FunctionDecl*> virtual_table::get_pure_virtuals() const
{
    BOOST_ASSERT(is_ready() && "using this function before ready is non-sense");

    using boost::adaptors::filtered;

    std::set<const tree::FunctionDecl*> pure_virtuals;

    for (const auto& segment : segments)
    {
        for (const auto& entry : segment.entries | filtered(std::mem_fn(&entry_type::is_pure)))
        {
            const auto& decls = entry.overriden_decls;

            BOOST_ASSERT(!decls.empty() && "there should be at least one declaration");

            pure_virtuals.emplace(decls.back());
        }
    }

    return pure_virtuals;
}

std::vector<const tree::FunctionDecl*> virtual_table::find_overridens(const tree::FunctionDecl& member_function) const
{
    BOOST_ASSERT(is_ready() && "using this function before ready is non-sense");

    std::vector<const tree::FunctionDecl*> overridens;

    if (could_be_virtual(member_function))
    {
        const auto& signature     = generate_signature(member_function);
        const auto& signature_pos = override_states.find(signature);

        if (signature_pos != override_states.end())
        {
            for (const auto& index : signature_pos->second.indices)
            {
                const auto& segment_index = index.segment;
                const auto&   entry_index = index.index  ;

                const auto& entry = segments[segment_index].entries[entry_index];
                const auto& decls = entry.overriden_decls;

                BOOST_ASSERT(!decls.empty() && "at least one declaration should be there");
                BOOST_ASSERT(decls.back() == &member_function && "member function is not the leaf override!?");

                if (decls.size() > 1)
                    overridens.emplace_back(*std::next(decls.rbegin(), 1));
            }
        }
    }

    return overridens;
}

auto virtual_table::generate_info() const -> std::vector<segment_info_type>
{
    BOOST_ASSERT(is_ready() && "using this function before ready is non-sense");

    using boost::adaptors::transformed;

    const auto& segment_infos_range = segments
                                    | transformed(&generate_vtable_segment_info)
                                    ;

    std::vector<segment_info_type> segment_infos(segment_infos_range.begin(), segment_infos_range.end());

    // primary base
    if (!segment_infos.empty())
        segment_infos.front().source_class = owner;

    return segment_infos;
}

bool virtual_table::build(const tree::ClassDecl& owner_class, const std::vector<const virtual_table*>& parent_vtables, const std::vector<tree::FunctionDecl*>& member_functions)
{
    BOOST_ASSERT(boost::algorithm::all_of(parent_vtables  , not_null()) && "null pointer exception");
    BOOST_ASSERT(boost::algorithm::all_of(member_functions, not_null()) && "null pointer exception");

    using boost::adaptors::filtered;
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    if (is_ready())
        return true;

    owner = &owner_class;

    const auto& filtered_member_functions = member_functions
                                          | indirected
                                          | filtered(&could_be_virtual)
                                          ;

    for (const signature_type& signature : filtered_member_functions | transformed(&generate_signature))
        if (!boost::algorithm::all_of(signature.parameter_types, not_null()))
            return false;

    for (const auto& parent_vtable : parent_vtables | indirected)
        inherit(parent_vtable);

    for (auto& member_function : filtered_member_functions)
        add_virtual(member_function);

    build_function_indices();

    segments_ready = true;

    return true;
}

void virtual_table::err_status() const
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    const auto& virtual_count = override_states.size();
    const auto& segment_count = segments.size();

    std::wcerr << L"virtual signature count: " << virtual_count << std::endl;

    for (const auto& entry : make_zip_range(zero_to(virtual_count), override_states))
    {
        const auto& idx       =             entry.get<0>() ;
        const auto& signature = std::get<0>(entry.get<1>());
        const auto& state     = std::get<1>(entry.get<1>());

        const auto& name            = signature.name;
        const auto& parameter_types = signature.parameter_types;

        std::wcerr << L"    #" << std::setw(2) << std::setfill(L'0') << idx;

        if (state.is_pure)
            std::wcerr << L"    <pure-virtual> ";
        else
            std::wcerr << L"                   ";

        std::wcerr << signature.to_string() << std::endl;
    }

    std::wcerr << L"virtual table segments" << std::endl;

    for (const auto& segment_tuple : make_zip_range(zero_to(segment_count), segments))
    {
        const auto& segment_idx     = segment_tuple.get<0>();
        const auto& segment         = segment_tuple.get<1>();
        const auto& segment_entries = segment.entries;

        for (const auto& entry_tuple : make_zip_range(zero_to(segment_entries.size()), segment_entries))
        {
            const auto& entry_idx = entry_tuple.get<0>();
            const auto& entry     = entry_tuple.get<1>();

            std::wcerr << L"    #" << std::setw(2) << std::setfill(L'0') << segment_idx
                       << L'-'     << std::setw(2) << std::setfill(L'0') <<   entry_idx;

            if (entry.is_pure)
                std::wcerr << L" <pure-virtual> ";
            else
                std::wcerr << L"                ";

            std::wcerr << entry.signature.to_string() << std::endl;
        }
    }
}

void virtual_table::inherit(const virtual_table& vtable)
{
    BOOST_ASSERT(!is_ready() && "re-building virtual table!?");

    const auto& offset = segments.size();

    boost::push_back(segments, vtable.segments);

    const auto& segment_indices = boost::irange(offset, segments.size());
    const auto& new_segments    = boost::make_iterator_range(segments, offset, 0);

    for (const auto& segment_tuple : make_zip_range(segment_indices, new_segments))
    {
        const auto& segment_index       = segment_tuple.get<0>();
        const auto& new_segment         = segment_tuple.get<1>();
        const auto& new_segment_entries = new_segment.entries;

        for (const auto& entry_tuple : make_zip_range(zero_to(new_segment_entries.size()), new_segment_entries))
        {
            const auto& entry_index =                         entry_tuple.get<0>() ;
                  auto& new_entry   = const_cast<entry_type&>(entry_tuple.get<1>());

            const auto& override_state_pos = override_states.find(new_entry.signature);

            if (override_state_pos == override_states.end())
            {
                override_states.emplace(
                    new_entry.signature,
                    overriden_state
                    {
                        new_entry.is_pure,
                        {
                            entry_index_type{segment_index, entry_index}
                        }
                    }
                );
            }
            else
            {
                if (new_entry.is_pure)
                    override_state_pos->second.is_pure = true;

                override_state_pos->second.indices.emplace_back(entry_index_type{segment_index, entry_index});
            }
        }
    }
}

void virtual_table::build_function_indices()
{
    if (segments.empty())
        return;

    const auto& main_entries = segments.front().entries;

    for (const auto& index_and_entry : make_zip_range(zero_to(main_entries.size()), main_entries))
    {
        const auto& index      = index_and_entry.get<0>();
        const auto& main_entry = index_and_entry.get<1>();

        function_indices.emplace(main_entry.overriden_decls.back(), index);
    }
}

void virtual_table::add_virtual(tree::FunctionDecl& member_function)
{
    using sigature_iterator = std::map<signature_type, overriden_state>::iterator;

    auto signature   = generate_signature(member_function);
    auto equal_range = override_states.equal_range(signature);

    const auto& is_already_virtual = equal_range.first != equal_range.second;

    if (!is_already_virtual && !member_function.is_virtual)
        return;

    // for each non-virtual methods, mark them as virtual if we can find them in virtual table
    if (!member_function.is_virtual)
        member_function.is_virtual = true;

    const auto& is_pure = !member_function.hasBody();

    if (is_already_virtual)
    {
        auto& state = equal_range.first->second;

        for (const auto& index_for_update : state.indices)
        {
            const auto& segment_index = index_for_update.segment;
            const auto&   entry_index = index_for_update.index;

            auto& entry = segments[segment_index].entries[entry_index];

            entry.is_pure         = is_pure;
            entry.overriden_decls.emplace_back(&member_function);
        }

        const auto& is_in_main_segment = state.indices.front().segment == 0;

        if (!is_in_main_segment)
        {
            auto&            main_segment         = segments.front();
            auto&            main_segment_entries = main_segment.entries;
            entry_index_type new_index            = {0u, main_segment_entries.size()};

            main_segment_entries.emplace_back(entry_type{std::move(signature), is_pure, {tree::make_relinkable(&member_function)}});

            equal_range.first->second.indices.emplace_front(std::move(new_index));
        }

        equal_range.first->second.is_pure = is_pure;
    }
    else
    {
        entry_type  new_entry =
        {
            signature,
            is_pure,
            {tree::make_relinkable(&member_function)}
        };

        if (segments.empty())
            segments.emplace_back(segment_type{tree::make_relinkable(owner), {}});

        auto&            segment         = segments.front();
        auto&            segment_entries = segment.entries;
        entry_index_type new_index       = {0u, segment_entries.size()};

        segment_entries.emplace_back(std::move(new_entry));

        override_states.emplace(
            std::move(signature),
            overriden_state
            {
                is_pure,
                {{std::move(new_index)}}
            }
        );
    }
}

} }
