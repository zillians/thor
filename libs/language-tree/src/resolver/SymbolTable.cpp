/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <vector>
#include <string>

#include <boost/logic/tribool.hpp>
#include <boost/foreach.hpp>

#include "core/Logger.h"

#include "language/tree/ASTNode.h"
#include "language/tree/module/Tangle.h"
#include "language/tree/module/Package.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/context/ParserContext.h"
#include "language/logging/StringTable.h"
#include "language/resolver/SymbolTable.h"

namespace zillians { namespace language {

static void set_package_import_status(tree::Identifier* pkg_id)
{
    tree::Tangle* tangle = getParserContext().tangle;
    if(tree::NestedIdentifier* nid = tree::cast<tree::NestedIdentifier>(pkg_id))
    {
        std::wstring s;
        for(auto i = nid->identifier_list.begin(); i != nid->identifier_list.end(); ++i)
        {
            if(i != nid->identifier_list.begin())
                s += L".";
            s += (*i)->toString();
            tree::Package* p = tangle->findPackage(s);
            BOOST_ASSERT_MSG(p != NULL, "inserted package not exists");
            if(i == nid->identifier_list.end() - 1)
            {
                p->setImportStatus(SymbolTable::ImportStatus::IMPORTED);
            }
            else if(p->getImportStatus() != SymbolTable::ImportStatus::IMPORTED)
            {
                p->setImportStatus(SymbolTable::ImportStatus::ON_PATH);
            }
        }
    }
    else
    {
        const std::wstring& str_ns = pkg_id->toString();
        tree::Package* p = tangle->findPackage(str_ns);
        BOOST_ASSERT_MSG(p != NULL, "inserted package not exists");
        p->setImportStatus(SymbolTable::ImportStatus::IMPORTED);
    }
}

SymbolTable::SymbolTable(const ImportStatus::value is)
    : clean(false)
    , base_tables()
    , import_status(is)
    , report_error(true)
{ }

SymbolTable::SymbolTable()
    : clean(false)
    , base_tables()
    , import_status(ImportStatus::NOT_PACKAGE)
    , report_error(true)
{ }

SymbolTable::EntryListSizeType SymbolTable::size() const
{
    return entry_list.size();
}

std::vector<SymbolTable*>::size_type SymbolTable::size_base() const
{
    return base_tables.size();
}

void SymbolTable::enter(tree::ASTNode* scope)
{
    scope_index_stack.push_back(Frame(scope, entry_list.size()));
}

void recursive_reset_package_import_status(tree::Package* p)
{
    SymbolTable::ImportStatus::value is = p->getImportStatus();
    if(is == SymbolTable::ImportStatus::ON_PATH || is == SymbolTable::ImportStatus::IMPORTED)
    {
        BOOST_FOREACH(tree::Package* child_package, p->children)
        {
            recursive_reset_package_import_status(child_package);
        }
        p->setImportStatus(SymbolTable::ImportStatus::NOT_IMPORTED);
    }
}

void reset_import_status()
{
    tree::Tangle* tangle = getParserContext().tangle;
    if(tangle == NULL) return;
    BOOST_FOREACH(tree::Package* p, tangle->root->children)
    {
        recursive_reset_package_import_status(p);
    }
}

void SymbolTable::leave(tree::ASTNode* scope)
{
    const Frame& f = scope_index_stack.back();
    BOOST_ASSERT(f.block == scope && "leave scope not match enter scope");
    EntryListSizeType index = f.frame_index;
    scope_index_stack.pop_back();

    size_t count_entry_to_remove = entry_list.size() - index;
    for(size_t i = 0; i != count_entry_to_remove; ++i)
    {
        remove_entry();
    }

    if(tree::isa<tree::Source>(scope))
    {
        base_tables.clear();
        reset_import_status();
    }
}

void SymbolTable::add_base(SymbolTable* base_table)
{
    BOOST_ASSERT(base_table != NULL);
    BOOST_ASSERT(base_table != this);
    base_tables.push_back(base_table);
}

void SymbolTable::insert_entry(const std::wstring& id, tree::ASTNode* node, const Entry::TAG tag)
{
    auto i = str_table.find(id);
    if(i != str_table.end())
    {
        EntryListType::size_type old_index = i->second;
        i->second = entry_list.size();
        Entry new_entry(node, id, old_index, tag);
        entry_list.push_back(new_entry);
    }
    else
    {
        EntryListType::size_type new_index = entry_list.size();
        str_table.insert(std::make_pair(id, new_index));
        Entry new_entry(node, id, new_index, tag);
        entry_list.push_back(new_entry);
    }
}

typedef bool IsAlias;

const std::wstring get_unqualified_name(tree::ASTNode* node, const IsAlias is_alias)
{
    if(tree::Package* pkg = tree::cast<tree::Package>(node))
    {
        return pkg->id->name;
    }
    else if(tree::Declaration* decl = tree::cast<tree::Declaration>(node))
    {
        const std::wstring& sid =
            tree::isa<tree::SimpleIdentifier>(decl->name) ?
            decl->name->toString() :
            tree::cast<tree::TemplatedIdentifier>(decl->name)->id->toString();
        return sid;
    }
    else if(tree::Import* import = tree::cast<tree::Import>(node))
    {
        if(is_alias)
        {
            return import->alias->toString();
        }
        else
        {
            return import->ns->toString();
        }
    }
    else if(tree::FunctionType* func_type = tree::cast<tree::FunctionType>(node))
    {
        return func_type->toString();
    }
    else
    {
        UNREACHABLE_CODE();
    }
}

void SymbolTable::remove_entry()
{
    Entry& e = entry_list.back();
    EntryListSizeType cur_index = entry_list.size() - 1;
    const std::wstring& s = e.id;

    if(e.prev_index == cur_index)
    {
        str_table.erase(s);
        entry_list.pop_back();
    }
    else
    {
        auto i = str_table.find(s);
        i->second = e.prev_index;
        entry_list.pop_back();
    }
}

SymbolTable::InsertResult SymbolTable::in_table_state(const std::wstring& id, tree::ASTNode* node)
{
    auto i = str_table.find(id);
    if(i == str_table.end())
        return InsertResult(InsertResult::OK);

    EntryListSizeType idx = i->second;
    const Entry& e = entry_list[idx];

    if(!scope_index_stack.empty() && scope_index_stack.back().frame_index > i->second)
    {
        if(report_error)
        {
            if(tree::isa<tree::Package>(e.symbol))
            {
                LOG_MESSAGE(SHADOW_PACKAGE, node, _id(id));
            }
            else
            {
                LOG_MESSAGE(SHADOW_DECLARE, node, _id(id));
                LOG_MESSAGE(SHADOW_DECLARE_PREV, e.symbol);
            }
        }
        return InsertResult(InsertResult::SHADOW, id);
    }
    else
    {
        if(report_error)
        {
            LOG_MESSAGE(DUPE_NAME, node, _id(id));
            LOG_MESSAGE(DUPE_NAME_PREV, e.symbol);
        }
        return InsertResult(InsertResult::DUPE, id);
    }
}

SymbolTable::InsertResult SymbolTable::insert(tree::Package* package)
{
    auto s = in_table_state(package->id->name, package);
    if(!s.is_ok())
        return s;

    insert_entry(package->id->name, package, Entry::PACKAGE);
    return InsertResult(InsertResult::OK);
}

SymbolTable::InsertResult SymbolTable::insert(tree::Declaration* decl)
{
    BOOST_ASSERT(!tree::isa<tree::NestedIdentifier>(decl->name));

    const std::wstring& sid =
        tree::isa<tree::SimpleIdentifier>(decl->name) ?
        decl->name->toString() :
        tree::cast<tree::TemplatedIdentifier>(decl->name)->id->toString();

    auto i = str_table.find(sid);
    if(i != str_table.end())
    {
        EntryListSizeType idx = i->second;
        tree::ASTNode* cur_entry = entry_list[idx].symbol;
        const bool both_class = tree::isa<tree::   ClassDecl>(cur_entry) && tree::isa<tree::   ClassDecl>(decl);
        const bool both_func  = tree::isa<tree::FunctionDecl>(cur_entry) && tree::isa<tree::FunctionDecl>(decl);
        const bool overoadable = both_class || both_func;
        if(!overoadable)
        {
            return in_table_state(sid, decl);
        }
    }

    insert_entry(sid, decl);
    return InsertResult(InsertResult::OK);
}

std::wstring get_first_package_name(tree::Identifier* id)
{
    if(tree::NestedIdentifier* nid = tree::cast<tree::NestedIdentifier>(id))
    {
        return nid->identifier_list[0]->toString();
    }
    else
    {
        return id->toString();
    }
}

SymbolTable::InsertResult SymbolTable::insert(tree::Import* import)
{
    // import package name
    BOOST_ASSERT(import->ns != NULL);
    const std::wstring& str_ns = import->ns->toString();
    tree::Tangle* tangle = getParserContext().tangle;
    tree::Package* package_scope = tangle->findPackage(str_ns);
    BOOST_ASSERT_MSG(package_scope != NULL, "inserted package not exists");
    const std::wstring& first_pkg_name = get_first_package_name(import->ns);
    tree::Package* first_package_scope = tangle->findPackage(first_pkg_name);
    bool is_package_name_insert = false;
    auto i = str_table.find(first_pkg_name);

    // if package name already in table
    if(i != str_table.end())
    {
        EntryListSizeType idx = i->second;
        const Entry& e = entry_list[idx];
        // and previos name is not inserted by a import statemen's ns, but its alias
        if(e.is_package())
        {
            set_package_import_status(import->ns);
        }
        else if(!e.is_import_ns())
        {
            // import with alias
            const InsertResult result = in_table_state(first_pkg_name, import);
            if(result.is_ok())
            {
                set_package_import_status(import->ns);
            }

            return result;
        }
        else
        {
            set_package_import_status(import->ns);
        }
    }
    else
    {
        insert_entry(first_pkg_name, first_package_scope, Entry::IMPORT_NS);
        is_package_name_insert = true;
        set_package_import_status(import->ns);
    }

    // import package alias
    if(import->alias != NULL)
    {
        // named alias
        if(import->alias->toString() != L"")
        {
            const std::wstring& str_alias = import->alias->toString();
            auto s2 = in_table_state(str_alias, import);
            if(!s2.is_ok())
            {
                if(is_package_name_insert)
                    remove_entry();
                return s2;
            }
            insert_entry(str_alias, package_scope);
        }
        // import . = x.y.z ;
        else
        {
            tree::Tangle* tangle = getParserContext().tangle;
            tree::Package* p = tangle->findPackage(str_ns);
            BOOST_ASSERT_MSG(p != NULL, "inserted package not exists");
            base_tables.push_back(&p->symbol_table);
        }
    }

    // set import status
    if(&getParserContext() == NULL)
        return InsertResult(InsertResult::OK);

    return InsertResult(InsertResult::OK);
}

// NOTE: the performance may can be improved here, with better implementation
SymbolTable::FindResult SymbolTable::find_in_base(const std::wstring& id) const
{
    FindResult result;
    BOOST_FOREACH(const SymbolTable* st, base_tables)
    {
        FindResult each_result = st->find(id);
        result.candidates.insert(result.candidates.end(), each_result.candidates.begin(), each_result.candidates.end());
    }
    result.is_found = !result.candidates.empty();
    return result;
}

bool visible(const SymbolTable::ImportStatus::value is, tree::ASTNode* n)
{
    tree::Package* p = tree::cast<tree::Package>(n);
    // entry is package
    if(p != NULL)
    {
        if(p->getImportStatus() == SymbolTable::ImportStatus::IMPORTED || p->getImportStatus() == SymbolTable::ImportStatus::ON_PATH)
            return true;
        else
            return false;
    }
    // entry is not package, but class & function & var
    else
    {
        if(is == SymbolTable::ImportStatus::IMPORTED)
            return true;
        else
            return false;
    }
}

SymbolTable::FindResult SymbolTable::find(const std::wstring& id) const
{
    if(!is_clean())
        return FindResult(boost::indeterminate);

    CollectionType result;
    if(import_status == ImportStatus::NOT_PACKAGE)
    {
        auto i = str_table.find(id);
        if(i == str_table.end())
        {
            return find_in_base(id);
        }

        EntryListSizeType index = i->second;

        while(true)
        {
            const Entry& e = entry_list[index];
            result.push_back(e.symbol);
            if(entry_list[index].prev_index == index)
                break;
            index = entry_list[index].prev_index;
        }

        BOOST_ASSERT(!result.empty());
    }
    else
    {
        auto i = str_table.find(id);
        if(i == str_table.end())
        {
            return find_in_base(id);
        }

        EntryListSizeType index = i->second;

        while(true)
        {
            const Entry& e = entry_list[index];
            if(visible(import_status, e.symbol))
                result.push_back(e.symbol);
            if(entry_list[index].prev_index == index)
                break;
            index = entry_list[index].prev_index;
        }

    }

    return FindResult(!result.empty(), result);
}

boost::tribool SymbolTable::has(const std::wstring& id) const
{
    if(!is_clean())
        return boost::indeterminate;

    if(str_table.count(id))
        return true;

    BOOST_FOREACH(const SymbolTable* st, base_tables)
    {
        if(st->has(id))
            return true;
    }

    return false;
}

void SymbolTable::set_clean()
{
    clean = true;
}

bool SymbolTable::is_clean() const
{
    BOOST_FOREACH(const SymbolTable* st, base_tables)
    {
        if(!st->is_clean())
            return false;
    }
    return clean;
}

void SymbolTable::print() const
{
    dump(std::wcerr);
}

void SymbolTable::dump(std::wostream& os) const
{
    std::vector<std::map<std::wstring, EntryListSizeType>::const_iterator> str_table_iter(entry_list.size(), str_table.end());

    os << L"base size = " << base_tables.size() << std::endl;
    os << L"hash size = " << str_table.size() << std::endl;
    for(auto i = str_table.begin(); i != str_table.end(); ++i)
    {
        os << L"    \"" << i->first << L"\" -> " << i->second << std::hex << std::endl;
        str_table_iter[i->second] = i;
    }

    os << L"entry size = " << entry_list.size() << std::endl;
    std::deque<Frame> frames(scope_index_stack.begin(), scope_index_stack.end());
    for(EntryListSizeType i = 0; i != entry_list.size(); ++i)
    {
        // print frame
        while(!frames.empty() && frames.front().frame_index == i)
        {
            os << L"  ------------------------------ " << std::hex << frames.front().block << " ------------------------------" << std::endl;
            frames.pop_front();
        }
        // print entry
        const Entry& e = entry_list[i];
        os << L"    [" << i << L"] : " << std::hex << e.symbol << L" " << e.prev_index << L" " ;
        if(str_table_iter[i] != str_table.end()) os << L" <= \"" << str_table_iter[i]->first << "\"";
        os << std::endl;
    }
    // print frame
    while(!frames.empty() && frames.front().frame_index == entry_list.size())
    {
        os << L"  ------------------------------ " << std::hex << frames.front().block << " ------------------------------" << std::endl;
        frames.pop_front();
    }
}

void SymbolTable::disable_report_error()
{
    report_error = false;
}

void SymbolTable::set_import_status(const SymbolTable::ImportStatus::value tag)
{
    import_status = tag;
}

SymbolTable::ImportStatus::value SymbolTable::get_import_status() const
{
    return import_status;
}

SymbolTable::FindResult::FindResult()
{
}

SymbolTable::FindResult::FindResult(const boost::tribool is_found)
    : is_found(is_found)
{
}

SymbolTable::FindResult::FindResult(const boost::tribool is_found, const SymbolTable::CollectionType& candidates)
    : is_found(is_found)
    , candidates(candidates)
{
}

} }
