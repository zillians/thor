/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/module/Internal.h"
#include "language/context/ResolverContext.h"

namespace zillians { namespace language { namespace tree {

namespace {

template<typename TiedRangeType>
std::tuple<bool, std::vector<Type*>> get_canonical_types(const TiedRangeType &tied_range)
{
    std::vector<Type*> types;

    for (const auto* tied: tied_range)
    {
        BOOST_ASSERT(tied && "null pointer exception");

        Type* canonical_expr_type = tied->getCanonicalType();

        if (canonical_expr_type == nullptr || canonical_expr_type->isTypenameType() )
            return std::make_tuple(false, std::vector<Type*>());

        types.emplace_back(canonical_expr_type);
    }

    return std::make_tuple(true, std::move(types));
}

}

Internal::Internal()
{
    VoidTypeSpecifier     = new PrimitiveSpecifier(PrimitiveKind::VOID_TYPE);
    BooleanTypeSpecifier  = new PrimitiveSpecifier(PrimitiveKind::BOOL_TYPE);
    Int8TypeSpecifier     = new PrimitiveSpecifier(PrimitiveKind::INT8_TYPE);
    Int16TypeSpecifier    = new PrimitiveSpecifier(PrimitiveKind::INT16_TYPE);
    Int32TypeSpecifier    = new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE);
    Int64TypeSpecifier    = new PrimitiveSpecifier(PrimitiveKind::INT64_TYPE);
    Float32TypeSpecifier  = new PrimitiveSpecifier(PrimitiveKind::FLOAT32_TYPE);
    Float64TypeSpecifier  = new PrimitiveSpecifier(PrimitiveKind::FLOAT64_TYPE);

    VoidTypeSpecifier   ->parent = this;
    BooleanTypeSpecifier->parent = this;
    Int8TypeSpecifier   ->parent = this;
    Int16TypeSpecifier  ->parent = this;
    Int32TypeSpecifier  ->parent = this;
    Int64TypeSpecifier  ->parent = this;
    Float32TypeSpecifier->parent = this;
    Float64TypeSpecifier->parent = this;

    VoidType     = new PrimitiveType(PrimitiveKind::VOID_TYPE);
    BooleanType  = new PrimitiveType(PrimitiveKind::BOOL_TYPE);
    Int8Type     = new PrimitiveType(PrimitiveKind::INT8_TYPE);
    Int16Type    = new PrimitiveType(PrimitiveKind::INT16_TYPE);
    Int32Type    = new PrimitiveType(PrimitiveKind::INT32_TYPE);
    Int64Type    = new PrimitiveType(PrimitiveKind::INT64_TYPE);
    Float32Type  = new PrimitiveType(PrimitiveKind::FLOAT32_TYPE);
    Float64Type  = new PrimitiveType(PrimitiveKind::FLOAT64_TYPE);

    VoidType   ->parent = this;
    BooleanType->parent = this;
    Int8Type   ->parent = this;
    Int16Type  ->parent = this;
    Int32Type  ->parent = this;
    Int64Type  ->parent = this;
    Float32Type->parent = this;
    Float64Type->parent = this;
}

PrimitiveType* Internal::getPrimitiveType(PrimitiveKind t) const
{
    switch(t)
    {
    case PrimitiveKind::VOID_TYPE   : return VoidType;
    case PrimitiveKind::BOOL_TYPE   : return BooleanType;
    case PrimitiveKind::INT8_TYPE   : return Int8Type;
    case PrimitiveKind::INT16_TYPE  : return Int16Type;
    case PrimitiveKind::INT32_TYPE  : return Int32Type;
    case PrimitiveKind::INT64_TYPE  : return Int64Type;
    case PrimitiveKind::FLOAT32_TYPE: return Float32Type;
    case PrimitiveKind::FLOAT64_TYPE: return Float64Type;
    default: UNREACHABLE_CODE(); return NULL;
    }
}

PrimitiveSpecifier* Internal::getPrimitiveTypeSpecifier(PrimitiveKind t) const
{
    switch(t)
    {
    case PrimitiveKind::VOID_TYPE: return VoidTypeSpecifier;
    case PrimitiveKind::BOOL_TYPE: return BooleanTypeSpecifier;
    case PrimitiveKind::INT8_TYPE: return Int8TypeSpecifier;
    case PrimitiveKind::INT16_TYPE: return Int16TypeSpecifier;
    case PrimitiveKind::INT32_TYPE: return Int32TypeSpecifier;
    case PrimitiveKind::INT64_TYPE: return Int64TypeSpecifier;
    case PrimitiveKind::FLOAT32_TYPE: return Float32TypeSpecifier;
    case PrimitiveKind::FLOAT64_TYPE: return Float64TypeSpecifier;
    default: UNREACHABLE_CODE(); return NULL;
    }
}


bool Internal::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(

        (VoidTypeSpecifier   )
        (BooleanTypeSpecifier)
        (Int8TypeSpecifier   )
        (Int16TypeSpecifier  )
        (Int32TypeSpecifier  )
        (Int64TypeSpecifier  )
        (Float32TypeSpecifier)
        (Float64TypeSpecifier)

        (VoidType            )
        (BooleanType         )
        (Int8Type            )
        (Int16Type           )
        (Int32Type           )
        (Int64Type           )
        (Float32Type         )
        (Float64Type         )
    );
}

bool Internal::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,

        (VoidTypeSpecifier   )
        (BooleanTypeSpecifier)
        (Int8TypeSpecifier   )
        (Int16TypeSpecifier  )
        (Int32TypeSpecifier  )
        (Int64TypeSpecifier  )
        (Float32TypeSpecifier)
        (Float64TypeSpecifier)

        (VoidType            )
        (BooleanType         )
        (Int8Type            )
        (Int16Type           )
        (Int32Type           )
        (Int64Type           )
        (Float32Type         )
        (Float64Type         )
    );
}

std::wostream& Internal::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(VoidTypeSpecifier && "null pointer exception");
    BOOST_ASSERT(BooleanTypeSpecifier && "null pointer exception");
    BOOST_ASSERT(Int8TypeSpecifier && "null pointer exception");
    BOOST_ASSERT(Int16TypeSpecifier && "null pointer exception");
    BOOST_ASSERT(Int32TypeSpecifier && "null pointer exception");
    BOOST_ASSERT(Int64TypeSpecifier && "null pointer exception");
    BOOST_ASSERT(Float32TypeSpecifier && "null pointer exception");
    BOOST_ASSERT(Float64TypeSpecifier && "null pointer exception");

    BOOST_ASSERT(VoidType && "null pointer exception");
    BOOST_ASSERT(BooleanType && "null pointer exception");
    BOOST_ASSERT(Int8Type && "null pointer exception");
    BOOST_ASSERT(Int16Type && "null pointer exception");
    BOOST_ASSERT(Int32Type && "null pointer exception");
    BOOST_ASSERT(Int64Type && "null pointer exception");
    BOOST_ASSERT(Float32Type && "null pointer exception");
    BOOST_ASSERT(Float64Type && "null pointer exception");

    output
        << out_source(*VoidTypeSpecifier, indent)
        << out_source(*BooleanTypeSpecifier, indent)
        << out_source(*Int8TypeSpecifier, indent)
        << out_source(*Int16TypeSpecifier, indent)
        << out_source(*Int32TypeSpecifier, indent)
        << out_source(*Int64TypeSpecifier, indent)
        << out_source(*Float32TypeSpecifier, indent)
        << out_source(*Float64TypeSpecifier, indent)

        << out_source(*VoidType, indent)
        << out_source(*BooleanType, indent)
        << out_source(*Int8Type, indent)
        << out_source(*Int16Type, indent)
        << out_source(*Int32Type, indent)
        << out_source(*Int64Type, indent)
        << out_source(*Float32Type, indent)
        << out_source(*Float64Type, indent);

    for (const Type* type : type_set)
        output << out_source(*type, indent);

    return output;
}

Internal* Internal::clone() const
{
    BOOST_ASSERT(false && "internal node should not be cloned!");

    return nullptr;
}

FunctionType* Internal::addFunctionType(FunctionDecl& func_decl)
{
    using boost::adaptors::transformed;

    const auto& has_this = func_decl.is_member && !func_decl.is_static;

    std::vector<Type*> param_types;

    boost::push_back(
        param_types,
          func_decl.parameters
        | transformed(std::mem_fn(&VariableDecl::getCanonicalType))
    );

    auto*const return_type = func_decl.type == nullptr ? nullptr : func_decl.type->getCanonicalType();
    auto*const  class_type = has_this ? func_decl.getOwner<ClassDecl>()->getCanonicalType() : nullptr;

    const auto& is_all_canonical_ready = boost::algorithm::all_of(param_types, not_null()) &&
                                         (
                                             (func_decl.type == nullptr && return_type == nullptr) ||
                                             (func_decl.type != nullptr && return_type != nullptr)
                                         ) &&
                                         (
                                             ( has_this && class_type != nullptr) ||
                                             (!has_this && class_type == nullptr)
                                         )
                                         ;

    if (!is_all_canonical_ready)
        return nullptr;

    return addFunctionType(std::move(param_types), return_type, class_type);
}

FunctionType* Internal::addFunctionType(const FunctionSpecifier& func_ts)
{
    using boost::adaptors::transformed;

    std::vector<Type*> param_types;

    boost::push_back(
        param_types,
          func_ts.getParameterTypes()
        | transformed(std::mem_fn(&TypeSpecifier::getCanonicalType))
    );

    auto*const return_type = func_ts.getReturnType() == nullptr ? nullptr : func_ts.getReturnType()->getCanonicalType();

    const auto& is_all_canonical_ready = boost::algorithm::all_of(param_types, not_null()) &&
                                         (
                                             (func_ts.getReturnType() == nullptr && return_type == nullptr) ||
                                             (func_ts.getReturnType() != nullptr && return_type != nullptr)
                                         )
                                         ;

    if (!is_all_canonical_ready)
        return nullptr;

    return addFunctionType(std::move(param_types), return_type, nullptr);
}

FunctionType* Internal::addFunctionType(std::vector<Type*>&& param_types, Type* return_type, RecordType* class_type)
{
    using boost::adaptors::transformed;

    BOOST_ASSERT(boost::algorithm::all_of(param_types, std::mem_fn(&Type::isCanonical)) && "all parameter type should be canonical");
    BOOST_ASSERT((return_type == nullptr || return_type->isCanonical()) && "return type should be canonical");
    BOOST_ASSERT(( class_type == nullptr ||  class_type->isCanonical()) &&  "class type should be canonical");

    auto*const function_type = new FunctionType();

    function_type->parameter_types = std::move(param_types);
    function_type->return_type     = return_type;
    function_type->class_type      = class_type;

    auto*const canonical_function_type = addType(function_type);

    return canonical_function_type;
}

PointerType* Internal::addPointerType(Type* pointeeType)
{
    auto*const pointer_type = new PointerType(pointeeType);

    return addType(pointer_type);
}

ReferenceType* Internal::addReferenceType(Type* pointeeType)
{
    auto*const reference_type = new ReferenceType(pointeeType);

    return addType(reference_type);
}

MultiType* Internal::addMultiType(const MultiSpecifier& multiTs)
{
    bool               is_all_ready = false;
    std::vector<Type*> canonical_types;

    std::tie(is_all_ready, canonical_types) = get_canonical_types(multiTs.getTypes());

    if (!is_all_ready)
        return nullptr;

    return addMultiType(std::move(canonical_types));
}

MultiType* Internal::addMultiType(const TieExpr& tieExpr)
{
    bool               is_all_ready = false;
    std::vector<Type*> canonical_types;

    std::tie(is_all_ready, canonical_types) = get_canonical_types(tieExpr.tied_expressions);

    if (!is_all_ready)
        return nullptr;

    return addMultiType(std::move(canonical_types));
}

MultiType* Internal::addMultiType(std::vector<Type*>&& canonical_types)
{
    BOOST_ASSERT(
        boost::algorithm::all_of(
            canonical_types,
            [](const Type* type)
            {
                BOOST_ASSERT(type && "null pointer exception");

                return type->getCanonicalType() == type;
            }
        ) && "canonical type contains non-canonical type"
    );

    auto*const multi_type = new MultiType(std::move(canonical_types));

    return addType(multi_type);
}

template<typename TypeType>
TypeType* Internal::addType(TypeType* type)
{
    NOT_NULL(type);
    BOOST_ASSERT(type->parent == nullptr && "parent of type should be internal only! except types for declarations");

    type->parent = this;

    const auto& insertion_result  = type_set.emplace(type);
    const auto& inserted_position = insertion_result.first;

    auto*const        real_type = *inserted_position;
    auto*const casted_real_type = cast<TypeType>(real_type);

    BOOST_ASSERT(casted_real_type != nullptr && "got unexpected type!");

    return casted_real_type;
}

} } }
