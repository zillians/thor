/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>

#include <set>
#include <utility>

#include <boost/algorithm/string/split.hpp>
#include <boost/blank.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>

#include "utility/UUIDUtil.h"
#include "utility/UnicodeUtil.h"

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/RelinkablePtr.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/module/Config.h"
#include "language/tree/module/Package.h"
#include "language/tree/module/Tangle.h"

namespace zillians { namespace language { namespace tree {

namespace {

template<typename DeclType>
DeclType* findThorLangDecl(const Tangle& tangle, const std::wstring& package_path, const std::wstring& class_name)
{
    if(const Package* thor_lang_package = tangle.findPackage(package_path))
    {
        for(const Source* source: thor_lang_package->sources)
        {
            BOOST_ASSERT(source && "null pointer exception");

            for(Declaration* decl: source->declares)
            {
                BOOST_ASSERT(decl && "null pointer exception");
                BOOST_ASSERT(decl->name && "null pointer exception");

                DeclType* expect_decl = cast<DeclType>(decl);

                if(expect_decl && expect_decl->name->toString() == class_name)
                {
                    return expect_decl;
                }
            }
        }
    }

    return nullptr;
}

template<typename data_type>
constexpr boost::blank getPayload(const data_type&) {
    return {};
}

int getPayload(const relinkable_ptr<const FunctionDecl>& func_decl)
{
    const auto& export_kind = tree::ASTNodeHelper::getExportKind(func_decl);

    return static_cast<int>(export_kind);
}

constexpr bool mergePayload(boost::blank&, const boost::blank&)
{
    return false;
}

bool mergePayload(int& lhs_payload, const int& rhs_payload)
{
    using ExportKind = tree::ASTNodeHelper::ExportKind;

    const auto& lhs_kind = static_cast<ExportKind>(lhs_payload);
    const auto& rhs_kind = static_cast<ExportKind>(rhs_payload);
    const auto& new_kind = tree::ASTNodeHelper::mergeExportKind(lhs_kind, rhs_kind);

    if (new_kind == lhs_kind)
        return false;

    lhs_payload = static_cast<int>(new_kind);

    return true;
}

constexpr bool shouldAddToIdMap(const boost::blank& payload)
{
    return true;
}

bool shouldAddToIdMap(const int& payload)
{
    using ExportKind = tree::ASTNodeHelper::ExportKind;

    return static_cast<ExportKind>(payload) != ExportKind::NoExport;
}

template<typename offset_counter_type, typename map_type, typename data_type>
std::pair<Tangle::OffsetedId, bool> addImpl(const UUID& tangle_id, offset_counter_type& offset_counter, map_type& data_to_id, data_type&& data)
{
    const auto& payload = getPayload(data);

    if (!shouldAddToIdMap(payload))
        return {{UUID::nil(), -1}, false};

    const auto& insertion_result   = data_to_id.emplace(data, std::make_pair(std::set<Tangle::OffsetedId>(), payload));
    const auto& is_inserted        = insertion_result.second;
    const auto& entry_pos          = insertion_result.first;
          auto& entry_offseted_ids = entry_pos->second.first;

    if (is_inserted)
    {
        const auto& offseted_id_insertion_result = entry_offseted_ids.emplace(tangle_id, offset_counter);
        const auto& is_offseted_id_inserted      = offseted_id_insertion_result.second;
        const auto& offseted_id_pos              = offseted_id_insertion_result.first;
        const auto& offseted_id                  = *offseted_id_pos;
        BOOST_ASSERT(is_offseted_id_inserted && "cannot insert new offseted id!?");

        ++offset_counter;

        return {offseted_id, true};
    }

          auto& entry_payload      = entry_pos->second.second;
    const auto& is_payload_updated = mergePayload(entry_payload, payload);

    return {*boost::begin(entry_offseted_ids), is_payload_updated};
}

template<typename map_type, typename data_type>
std::pair<Tangle::OffsetedId, bool> getIdOfImpl(const map_type& data_to_id, const data_type& data)
{
    const auto& pos = data_to_id.find(data);

    if (pos == boost::end(data_to_id))
        return {{UUID::nil(), -1}, false};

    const auto& offseted_ids = pos->second.first;
    BOOST_ASSERT(!offseted_ids.empty() && "entry without any offseted ID!?");

    return {*boost::begin(offseted_ids), true};
}

template<typename map_type>
void mergeDataToIdImpl(map_type& lhs, const map_type& rhs)
{
    for (const auto& rhs_entry : rhs)
    {
        const auto& insertion_result = lhs.insert(rhs_entry);
        const auto& lhs_entry_pos    = insertion_result.first;
        const auto& is_inserted      = insertion_result.second;

        if (is_inserted)
            continue;

        auto& lhs_entry        = *lhs_entry_pos;
        auto& lhs_offseted_ids = lhs_entry.second.first;
        auto& rhs_offseted_ids = rhs_entry.second.first;
        auto& lhs_payload      = lhs_entry.second.second;
        auto& rhs_payload      = rhs_entry.second.second;

        lhs_offseted_ids.insert(boost::begin(rhs_offseted_ids), boost::end(rhs_offseted_ids));
        mergePayload(lhs_payload, rhs_payload);
    }
}

}

Tangle::Tangle()
    : Tangle(UUID::nil())
{
}

Tangle::Tangle(const UUID& new_id)
    : config(new Config())
    , internal(new Internal())
    , root(new Package(new SimpleIdentifier(L"")))
    , id(new_id)
    , type_id_max(0)
    , func_id_max(0)
    , symb_id_max(0)
    , data_to_type_id()
    , data_to_func_id()
    , data_to_symb_id()
{
    config  ->parent = this;
    internal->parent = this;
    root    ->parent = this;
}

Tangle::Tangle(UUID&& new_id)
    : config(new Config())
    , internal(new Internal())
    , root(new Package(new SimpleIdentifier(L"")))
    , id(std::move(id))
    , type_id_max(0)
    , func_id_max(0)
    , symb_id_max(0)
    , data_to_type_id()
    , data_to_func_id()
    , data_to_symb_id()
{
    config  ->parent = this;
    internal->parent = this;
    root    ->parent = this;
}

void Tangle::markImported(bool is_imported)
{
    root->markImported(is_imported);
}

bool Tangle::merge(Tangle& rhs)
{
    if(!root->merge(*rhs.root))
    {
        return false;
    }

    mergeDataToIdImpl(data_to_type_id, rhs.data_to_type_id);
    mergeDataToIdImpl(data_to_func_id, rhs.data_to_func_id);
    mergeDataToIdImpl(data_to_symb_id, rhs.data_to_symb_id);

    return true;
}

bool Tangle::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (internal  )
        (root      )
    );
}

bool Tangle::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (internal)
        (root    )
    );
}

std::wostream& Tangle::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << L"// Tangle" << std::endl;

    output << out_source(*config  , indent) << std::endl;
    output << out_source(*internal, indent) << std::endl;
    output << out_source(*root    , indent) << std::endl;

    return output;
}

Tangle* Tangle::clone() const
{
    BOOST_ASSERT(false && "Tangle should not be cloned");

    Tangle* cloned = new Tangle();
    return cloned;
}

const UUID& Tangle::getId() const noexcept
{
    return id;
}

auto Tangle::addType    (const     ClassDecl& decl  ) -> std::pair<OffsetedId, bool> { return addImpl(id, type_id_max, data_to_type_id, make_relinkable(&decl)); }
auto Tangle::addFunction(const  FunctionDecl& decl  ) -> std::pair<OffsetedId, bool> { return addImpl(id, func_id_max, data_to_func_id, make_relinkable(&decl)); }
auto Tangle::addSymbol  (const StringLiteral& symbol) -> std::pair<OffsetedId, bool> { return addImpl(id, symb_id_max, data_to_symb_id, symbol.value          ); }

auto Tangle::getIdOfType    (const     ClassDecl& decl  ) const -> std::pair<OffsetedId, bool> { return getIdOfImpl(data_to_type_id, make_relinkable(&decl)); }
auto Tangle::getIdOfFunction(const  FunctionDecl& decl  ) const -> std::pair<OffsetedId, bool> { return getIdOfImpl(data_to_func_id, make_relinkable(&decl)); }
auto Tangle::getIdOfSymbol  (const StringLiteral& symbol) const -> std::pair<OffsetedId, bool> { return getIdOfImpl(data_to_symb_id, symbol.value          ); }

auto Tangle::getMappingOfType    () const noexcept -> const DataToIdMap<relinkable_ptr<const    ClassDecl>>& { return data_to_type_id; }
auto Tangle::getMappingOfFunction() const noexcept -> const DataToIdMap<relinkable_ptr<const FunctionDecl>>& { return data_to_func_id; }
auto Tangle::getMappingOfSymbol  () const noexcept -> const DataToIdMap<std::wstring                      >& { return data_to_symb_id; }

struct IsDot {
    bool operator()(const wchar_t c) const {
        return c == L'.';
    }
};

Package* Tangle::findPackage(const std::wstring& package_path) const
{
    if(package_path.empty())
    {
        return root;
    }
    else
    {
        std::vector<std::wstring> tokens;
        // boost::split take string as an l-value
        // can't boost::split(v, s_to_ws(...), ...);
        boost::split(tokens, package_path, IsDot());

        Package* current = root;
        for(auto& token : tokens)
        {
            if(Package* next = current->findPackage(token))
            {
                current = next;
            }
            else
            {
                return NULL;
            }
        }

        return current;
    }
}

ClassDecl* Tangle::findThorLangObject() const
{
    return findClass(L"thor.lang", L"Object");
}

FunctionDecl* Tangle::findThorLangFunction(const std::wstring& function_name) const
{
    return findThorLangDecl<FunctionDecl>(*this, L"thor.lang", function_name);
}

ClassDecl* Tangle::findClass(const std::wstring& package_path, const std::wstring& class_name) const
{
    return findThorLangDecl<ClassDecl>(*this, package_path, class_name);
}

} } }
