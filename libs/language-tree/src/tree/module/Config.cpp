/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iomanip>
#include <iostream>

#include "language/tree/module/Config.h"

namespace zillians { namespace language { namespace tree {

bool Config::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (emit_debug_info)
        (opt_level      )
    );
}

bool Config::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (emit_debug_info)
        (opt_level      )
    );
}

Config* Config::clone() const
{
    auto*const cloned_node = new Config();

    cloned_node->emit_debug_info = emit_debug_info;
    cloned_node->opt_level       = opt_level      ;

    return cloned_node;
}

std::wostream& Config::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << L"// Config" << std::endl;
    output << L"//     debug info  : " << std::boolalpha << emit_debug_info << std::endl;
    output << L"//     optimization: " << opt_level << std::endl;

    return output;
}

} } }
