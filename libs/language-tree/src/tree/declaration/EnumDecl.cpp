/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>

#include <boost/assert.hpp>

#include "language/context/ManglingStageContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

EnumDecl::EnumDecl(Identifier* name)
    : Declaration(name)
    , enum_type_(new EnumType(this))
{
    BOOST_ASSERT(name && "null enumeration name is not allowed");
}

void EnumDecl::addEnumeration(VariableDecl* decl)
{
    BOOST_ASSERT(!symbol_table.is_clean() && "you cannot modify values in enumeration after symbol table generation!");

    decl->parent = this;
    values.push_back(decl);
}

void EnumDecl::addEnumeration(SimpleIdentifier* tag, Expression* value/* = NULL*/)
{
    BOOST_ASSERT(tag && "null tag for enumeration is not allowed");
    BOOST_ASSERT(!symbol_table.is_clean() && "you cannot modify values in enumeration after symbol table generation!");

    tag->parent = this;
    if(value) value->parent = this;

    VariableDecl* decl = new VariableDecl(tag, new PrimitiveSpecifier(PrimitiveKind::INT32_TYPE), true, true, true, Declaration::VisibilitySpecifier::DEFAULT, value);
    decl->parent = this;

    values.push_back(decl);
}

EnumType* EnumDecl::getType() const
{
    return enum_type_;
}

EnumType* EnumDecl::getCanonicalType() const
{
    return enum_type_;
}

VariableDecl* EnumDecl::findEnumerator(int64 value) const
{
    using std::begin;
    using std::endl;

    const auto value_decl_pos = std::find_if(
        begin(values),
        end  (values),
        [value](VariableDecl* value_decl)
        {
            BOOST_ASSERT(value_decl && "null pointer exception");

            return value == value_decl->getEnumeratorValue();
        }
    );

    return value_decl_pos != values.end() ? *value_decl_pos : nullptr;
}

PrimitiveKind EnumDecl::getUnderlyingType() const
{
    const EnumDeclManglingContext* context = EnumDeclManglingContext::get(this);

    if (context != nullptr)
    {
        if (context->is_long_literal)
            return PrimitiveKind::INT64_TYPE;
        else
            return PrimitiveKind::INT32_TYPE;
    }

    UNREACHABLE_CODE();

    return PrimitiveKind::VOID_TYPE;
}

bool EnumDecl::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (values)
    );
}

bool EnumDecl::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (values)
    );
}

std::wstring EnumDecl::toString() const
{
    return L"enum " + Declaration::toString();
}

EnumDecl* EnumDecl::clone() const
{
    EnumDecl* cloned = new EnumDecl((name) ? name->clone() : NULL);

    if(annotations != NULL)
    {
        Annotations* anno = annotations->clone();
        cloned->setAnnotations(anno);
    }

    for(VariableDecl* value: values)
        cloned->addEnumeration(value->clone());

    cloned->arch         = arch;
    cloned->may_conflict = may_conflict;

    return cloned;
}

std::wostream& EnumDecl::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BaseNode::toSource(output, indent);

    output << out_indent(indent) << L"enum " << out_source(*name, indent) << std::endl;
    output << out_indent(indent) << L'{';

    if (!values.empty())
    {
              auto i    = values.cbegin();
        const auto iend = values.cend();

        const auto& out_enumerator = [&output, indent](const VariableDecl* decl)
        {
            BOOST_ASSERT(decl->name && "null pointer exception");

            output << out_indent(indent + 1) << out_source(*decl->name, indent);

            if (decl->initializer != nullptr)
                output << L" = " << out_source(*decl->initializer, indent);
        };

        output << std::endl;

        out_enumerator(*i);

        while (++i != iend)
        {
            output << L',' << std::endl;
            out_enumerator(*i);
        }
    }

    output << std::endl << out_indent(indent) << L'}';

    return output;
}

} } }
