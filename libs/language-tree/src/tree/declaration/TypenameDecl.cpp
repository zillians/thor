/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/declaration/TypenameDecl.h"

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/expression/Expression.h"
#include "language/context/ResolverContext.h"

namespace zillians { namespace language { namespace tree {

TypenameDecl::TypenameDecl(Identifier* name,
                           TypeSpecifier* specialized_type/* = NULL*/,
                           TypeSpecifier* default_type/* = NULL*/)
    : Declaration(name)
    , specialized_type(specialized_type)
    , default_type(default_type)
    , typename_type_(new TypenameType(this))
{
    BOOST_ASSERT(name && "null variable name is not allowed");

    if(specialized_type) specialized_type->parent = this;
    if(default_type) default_type->parent = this;
}

void TypenameDecl::setSpecializdType(TypeSpecifier* s)
{
    if(specialized_type) specialized_type->parent = NULL;
    if(s) s->parent = this;
    specialized_type = s;
}

void TypenameDecl::setDefaultType(TypeSpecifier* d)
{
    if(default_type) default_type->parent = NULL;
    if(d) d->parent = this;
    default_type = d;
}

bool TypenameDecl::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (specialized_type)
        (default_type    )
    );
}

bool TypenameDecl::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (specialized_type)
        (default_type    )
    );
}

std::wstring TypenameDecl::toString() const
{
    std::wstring result = Declaration::toString();

    if(specialized_type)
    {
        result += L':';
        result += specialized_type->toString();
    }
    else if(default_type)
    {
        UNIMPLEMENTED_CODE();

        result += L'=';
        result += L"<default-type>"; // implement it after 'default_type' is not a expression
    }

    return result;
}

TypenameDecl* TypenameDecl::clone() const
{
    TypenameDecl* cloned = new TypenameDecl(
            (name) ? name->clone() : NULL,
            (specialized_type) ? specialized_type->clone() : NULL,
            (default_type) ? default_type->clone() : NULL);

    if(annotations != NULL)
    {
        Annotations* anno = annotations->clone();
        cloned->setAnnotations(anno);
    }

    if(typename_type_ != nullptr)
    {
        TypenameType* tnt = typename_type_->clone();
        cloned->typename_type_ = tnt;
        cloned->typename_type_->parent = cloned;
        cloned->typename_type_->decl_  = cloned;
    }

    cloned->arch         = arch;
    cloned->may_conflict = may_conflict;

    return cloned;
}

TypenameType* TypenameDecl::getType() const
{
    return typename_type_;
}

Type* TypenameDecl::getCanonicalType() const
{
	if(this->specialized_type)
	{
		return this->specialized_type->getCanonicalType();
	}
	else
	{
        if(Type* resolved_type = ResolvedType::get(this))
        {
            if(resolved_type->getAsTypenameDecl() == this)
            {
                BOOST_ASSERT(false && "how come a typename decl can resolved to itself?");
                return resolved_type->getAsTypenameType();
            }
            else
                return resolved_type->getCanonicalType();
        }
        else
        {
            // NOTE! this might be used by template type deduct?
            // when a typename has no canonican type
            // just return the TypenameType, or what to return???
            return this->getType();
        }
	}
}

std::wostream& TypenameDecl::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(name && "null pointer exception");

    BaseNode::toSource(output, indent);

    output << out_source(*name, indent);

    if (specialized_type != nullptr)
        output << L':' << out_source(*specialized_type, indent);

    if (default_type != nullptr)
        output << L" = " << out_source(*default_type, indent);

    return output;
}

} } }
