/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <tuple>
#include <vector>

#include <boost/assert.hpp>
#include <boost/preprocessor.hpp>

#include "language/context/ResolverContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/Block.h"

namespace zillians { namespace language { namespace tree {

#define CREATOR_SET_AND_RET(func, type, field, var_name)           \
    auto FunctionDecl::Creator::func(type var_name) -> Creator     \
    {                                                              \
        Creator copy = *this;                                      \
        copy.field   = (var_name);                                 \
        return copy;                                               \
    }

#define CREATOR_FLAG_AND_RET(func, field)                          \
    auto FunctionDecl::Creator::func(bool value) -> Creator        \
    {                                                              \
        Creator copy = *this;                                      \
        copy.field   = value;                                      \
        return copy;                                               \
    }

CREATOR_SET_AND_RET(name      , Identifier*        , name_      , new_name      )
CREATOR_SET_AND_RET(type      , TypeSpecifier*     , type_      , new_type      )
CREATOR_SET_AND_RET(block     , Block*             , block_     , new_block     )
CREATOR_SET_AND_RET(visibility, VisibilitySpecifier, visibility_, new_visibility)

CREATOR_FLAG_AND_RET(member  , is_member  )
CREATOR_FLAG_AND_RET(static_ , is_static  )
CREATOR_FLAG_AND_RET(virtual_, is_virtual )
CREATOR_FLAG_AND_RET(override, is_override)

#undef CREATOR_FLAG_AND_RET
#undef CREATOR_SET_AND_RET

FunctionDecl* FunctionDecl::Creator::create() const
{
    return new FunctionDecl(
        name_, type_,
        is_member, is_static, is_virtual, is_override,
        visibility_, block_
    );
}

FunctionDecl::FunctionDecl(
    Identifier* name,
    TypeSpecifier* type,
    bool is_member,
    bool is_static,
    bool is_virtual,
    bool is_override,
    Declaration::VisibilitySpecifier::type visibility,
    Block* block
)
    : Declaration(name)
    , type(type)
    , is_member(is_member)
    , is_static(is_static)
    , is_virtual(is_virtual)
    , is_override(is_override)
    , visibility(visibility)
    , block(block)
    , is_lambda(false)
    , is_task(false)
    , is_global_init(false)
{
    if(type) type->parent = this;
    if(block) block->parent = this;

    if(!is_member || is_static)
    {
        BOOST_ASSERT(!is_virtual && "non-member function can't be declared as virtual");
        BOOST_ASSERT(!is_override && "non-member function can't be declared as override");
    }
}

void FunctionDecl::prependParameter(VariableDecl* parameter_decl)
{
    parameter_decl->parent = this;
    parameters.insert(parameters.begin(), parameter_decl);
}

void FunctionDecl::appendParameter(VariableDecl* parameter_decl)
{
    parameter_decl->parent = this;
    parameters.push_back(parameter_decl);
}

void FunctionDecl::appendParameter(SimpleIdentifier* name, TypeSpecifier* type, Expression* initializer)
{
    VariableDecl* parameter_decl = new VariableDecl(name, type, false, false, false, Declaration::VisibilitySpecifier::DEFAULT, initializer);
    parameter_decl->parent = this;
    parameters.push_back(parameter_decl);
}

void FunctionDecl::setReturnType(TypeSpecifier* return_type)
{
    if(type != nullptr)
        type->parent == nullptr;

    type = return_type;

    if(return_type != nullptr)
        return_type->parent = this;
}

bool FunctionDecl::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (parameters    )
        (type          )
        (is_member     )
        (is_static     )
        (is_virtual    )
        (is_override   )
        (visibility    )
        (block         )
        (is_lambda     )
        (is_task       )
        (is_global_init)
    );
}

bool FunctionDecl::isCompleted() const
{
    if( !isa<TemplatedIdentifier>(name) ||
        (isa<TemplatedIdentifier>(name) && cast<TemplatedIdentifier>(name)->isFullySpecialized()))
    {
        if(isa<ClassDecl>(parent))
        {
            return cast<ClassDecl>(parent)->isCompleted();
        }
        else
        {
            return true;
        }
    }

    return false;
}

bool FunctionDecl::isTemplated() const
{
    if(isa<TemplatedIdentifier>(name))
    {
        return true;
    }
    else if(auto parent_class = cast<ClassDecl>(parent))
    {
        return parent_class->isTemplated();
    }
    else
    {
        return false;
    }
}

bool FunctionDecl::isGlobalInit() const
{
    return is_global_init;
}

bool FunctionDecl::isConstructor() const
{
    BOOST_ASSERT(name && "null pointer exception");
    BOOST_ASSERT(name->getSimpleId() && "null pointer exception");

    SimpleIdentifier* sid = name->getSimpleId();

    return sid->name == L"new";
}

bool FunctionDecl::isDefaultConstructor() const
{
    return isConstructor() && parameters.empty();
}

bool FunctionDecl::isDestructor() const
{
    BOOST_ASSERT(name && "null pointer exception");
    BOOST_ASSERT(name->getSimpleId() && "null pointer exception");

    SimpleIdentifier* sid = name->getSimpleId();

    return sid->name == L"delete";
}

bool FunctionDecl::hasBody() const noexcept
{
    if (block != nullptr)
        return true;

    if (is_member)
    {
        NOT_NULL(parent);

        const auto*const cls_decl = cast<ClassDecl>(parent);

        BOOST_ASSERT(cls_decl != nullptr && "member function but cannot owner class is not found");

        if (cls_decl->is_interface)
            return false;
    }

    return ASTNodeHelper::hasNativeLinkage(this);
}

bool FunctionDecl::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (parameters)
        (block     )
        (type      )
    );
}

std::wstring FunctionDecl::toString() const {
    std::wstring desc = L"function " + Declaration::toString();

    desc += L'(';

    {
        bool has_param = false;

        for(VariableDecl* parameter: parameters)
        {
            BOOST_ASSERT(parameter && parameter->type && "null pointer exception");

            if(has_param)
            {
                desc += L',';
            }
            else
            {
                has_param = true;
            }

            desc += parameter->type->toString();
        }
    }

    desc += L"):";

    if(type)
    {
        desc += type->toString();
    }
    else
    {
        desc += L"<undermined>";
    }

    return desc;
}

FunctionDecl* FunctionDecl::clone() const
{
    FunctionDecl* cloned = new FunctionDecl(
            (name) ? name->clone() : NULL,
            (type) ? type->clone() : NULL,
            is_member, is_static, is_virtual, is_override, visibility,
            (block) ? block->clone() : NULL);

    cloned->arch           = arch;
    cloned->may_conflict   = may_conflict;
    cloned->is_lambda      = is_lambda;
    cloned->is_task        = is_task;
    cloned->is_global_init = is_global_init;

    if(annotations != NULL)
    {
        Annotations* anno = annotations->clone();
        cloned->setAnnotations(anno);
    }

    for(auto* param : parameters)
        cloned->appendParameter(param->clone());

    return cloned;
}

FunctionType* FunctionDecl::getType() const
{
    return getCanonicalType();
}

FunctionType* FunctionDecl::getCanonicalType() const
{
    Type* resolved = ResolvedType::get(this);
    // if this function is template, there will be no resolved_type to a FunctionType
    // TODO = =||| TBD
    if(resolved == nullptr) return nullptr;
    BOOST_ASSERT(resolved->isFunctionType());

    return resolved->getAsFunctionType()->getCanonicalType();
}

std::wostream& FunctionDecl::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BaseNode::toSource(output, indent);

    if (is_global_init)
        output << L"@global_init" << std::endl << out_indent(indent);

    if (is_member)
    {
        if (visibility != VisibilitySpecifier::DEFAULT)
            output << VisibilitySpecifier::toString(visibility) << L' ';
        if (is_static)
            output << L"static ";
        if (is_virtual)
            output << L"virtual ";
        if (is_override)
            output << L"override ";
    }

    if (is_lambda)
        output << L"lambda ";
    else if (is_task)
        output << L"task ";
    else
        output << L"function ";

    BOOST_ASSERT(name && "null pointer exception");

    output << out_source(*name, indent) << L'(';

    if (!parameters.empty())
    {
              auto i    = parameters.cbegin();
        const auto iend = parameters.cend();

        output << out_source(**i, indent);

        while (++i != iend)
            output << L", " << out_source(**i, indent);
    }

    output << L')';

    if (type != nullptr)
        output << L':' << out_source(*type, indent);

    if (block == nullptr)
        output << L';';
    else
        output << std::endl << out_source(*block, indent);

    return output;
}

} } }
