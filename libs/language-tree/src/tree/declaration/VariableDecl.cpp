/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ManglingStageContext.h"
#include "language/context/ResolverContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/statement/DeclarativeStmt.h"

namespace zillians { namespace language { namespace tree {

#define CREATOR_SET_AND_RET(func, type, field, var_name)           \
    auto VariableDecl::Creator::func(type var_name) -> Creator     \
    {                                                              \
        Creator copy = *this;                                      \
        copy.field   = (var_name);                                 \
        return copy;                                               \
    }

#define CREATOR_FLAG_AND_RET(func, field)                          \
    auto VariableDecl::Creator::func() -> Creator                  \
    {                                                              \
        Creator copy = *this;                                      \
        copy.field   = true;                                       \
        return copy;                                               \
    }

CREATOR_SET_AND_RET(name      , Identifier*        , name_      , new_name      )
CREATOR_SET_AND_RET(type      , TypeSpecifier*     , type_      , new_type      )
CREATOR_SET_AND_RET(init      , Expression*        , init_      , new_init      )
CREATOR_SET_AND_RET(visibility, VisibilitySpecifier, visibility_, new_visibility)

CREATOR_FLAG_AND_RET(member , is_member)
CREATOR_FLAG_AND_RET(static_, is_static)
CREATOR_FLAG_AND_RET(const_ , is_const )

#undef CREATOR_FLAG_AND_RET
#undef CREATOR_SET_AND_RET

VariableDecl* VariableDecl::Creator::create() const
{
    return new VariableDecl(
        name_, type_,
        is_member, is_static, is_const,
        visibility_,
        init_
    );
}

VariableDecl::VariableDecl(Identifier* name, TypeSpecifier* type, bool is_member, bool is_static, bool is_const, Declaration::VisibilitySpecifier::type visibility, Expression* initializer/* = NULL*/)
    : Declaration(name)
    , type(type)
    , is_member(is_member)
    , is_static(is_static)
    , is_const(is_const)
    , visibility(visibility)
    , initializer(initializer)
{
    BOOST_ASSERT(name && "null variable name is not allowed");

    if(type) type->parent = this;
    if(initializer) initializer->parent = this;
}

bool VariableDecl::hasInitializer() const noexcept
{
    return initializer != nullptr;
}

void VariableDecl::setInitializer(Expression* init)
{
    if(initializer) initializer->parent = NULL;
    initializer = init;
    if(initializer) initializer->parent = this;
}

bool VariableDecl::isLocal() const
{
    return !isGlobal() && !is_member;
}

bool VariableDecl::isParameter() const
{
    BOOST_ASSERT(parent != nullptr && "no parent!");

    return isa<FunctionDecl>(parent);
}

bool VariableDecl::isEnumerator() const
{
    BOOST_ASSERT(parent && "no parent!");

    return isa<EnumDecl>(parent);
}

int64 VariableDecl::getEnumeratorValue() const
{
    BOOST_ASSERT(isEnumerator() && "not a enumerator!");

    const auto *context = EnumIdManglingContext::get(this);

    BOOST_ASSERT(context && "enumerator value is not ready");

    return context->value;
}

EnumDecl* VariableDecl::getEnumeration() const
{
    BOOST_ASSERT(parent && "no parent!");

    return cast<EnumDecl>(parent);
}

bool VariableDecl::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (type       )
        (is_member  )
        (is_static  )
        (is_const   )
        (visibility )
        (initializer)
    );
}

bool VariableDecl::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (type       )
        (initializer)
    );
}

std::wstring VariableDecl::toString() const
{
    std::wstring result;

    if(EnumDecl* enum_decl = cast<EnumDecl>(parent))
    {
        result += enum_decl->toString();
        result += L'.';
        result += Declaration::toString();
    }
    else
    {
        result += L"var ";
        result += Declaration::toString();
        result += L':';

        if(type)
        {
            result += type->toString();
        }
        else
        {
            result += L"<undermined>";
        }
    }

    return result;
}

VariableDecl* VariableDecl::clone() const
{
    VariableDecl* cloned = new VariableDecl(
            (name) ? name->clone() : NULL,
            (type) ? type->clone() : NULL,
            is_member, is_static, is_const, visibility,
            (initializer) ? initializer->clone() : NULL);

    if(annotations != NULL)
    {
        Annotations* anno = annotations->clone();
        cloned->setAnnotations(anno);
    }

    cloned->arch         = arch;
    cloned->may_conflict = may_conflict;

    return cloned;
}

Type* VariableDecl::getType() const
{
    return type->getCanonicalType();
}

Type* VariableDecl::getCanonicalType() const
{
    if (const auto*const enum_decl = cast<EnumDecl>(parent))
        return enum_decl->getType();

    if (type == nullptr)
        return nullptr;

    if (const auto*const resolved_type = ResolvedType::get(type))
        return resolved_type->getCanonicalType();

    return nullptr;
}

std::wostream& VariableDecl::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(parent && "null pointer exception");
    BOOST_ASSERT(name && "null pointer exception");

    BaseNode::toSource(output, indent);

    if (is_member || isa<DeclarativeStmt>(parent))
    {
        if (is_static)
            output << L"static ";

        if (visibility != VisibilitySpecifier::DEFAULT)
            output << VisibilitySpecifier::toString(visibility) << L' ';

        if (is_const)
            output << L"const ";
        else
            output << L"var ";
    }

    output << out_source(*name, indent);

    if (type != nullptr)
        output << L':' << out_source(*type, indent);

    if (initializer != nullptr)
        output << L" = " << out_source(*initializer, indent);

    return output;
}

} } }
