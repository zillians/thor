/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "language/tree/ASTNodeFactory.h"
#include "language/tree/CustomizedSerialization.h"

#include <boost/serialization/export.hpp>

#define AST_NODE_SERIALIZE_EXPORT(r, _, node_name)  \
    BOOST_CLASS_EXPORT_IMPLEMENT(                   \
        zillians::language::tree:: node_name        \
    )

BOOST_PP_SEQ_FOR_EACH(AST_NODE_SERIALIZE_EXPORT, _, THOR_SCRIPT_AST_NODE_NAMES)

#undef AST_NODE_SERIALIZE_EXPORT
