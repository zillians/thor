/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

namespace zillians { namespace language { namespace tree { namespace visitor {

GenericDoubleVisitor::GenericDoubleVisitor()
{
    revisitor.user_visitor = this;
}

void GenericDoubleVisitor::revisit(ASTNode& node)
{
    revisitor.visit(node);
}

void GenericDoubleVisitor::terminateRevisit()
{
    revisitor.terminate();
}

GenericDoubleVisitor::ApplyVisitor::ApplyVisitor()
{
    REGISTER_ALL_VISITABLE_ASTNODE(applyInvoker);
}

//////////////////////////////////////////////////////////////////////
/// Basic

void GenericDoubleVisitor::ApplyVisitor::apply(ASTNode& node)
{
    UNUSED_ARGUMENT(node);
}

void GenericDoubleVisitor::ApplyVisitor::apply(Annotation& node)
{
    if(node.name) user_visitor->visit(*node.name);
    for(auto& attribute : node.attribute_list)
    {
        if(attribute.first) user_visitor->visit(*attribute.first);
        if(attribute.second) user_visitor->visit(*attribute.second);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(Annotations& node)
{
    for(auto* annotation : node.annotation_list)
    {
        user_visitor->visit(*annotation);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(Annotatable& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
}

void GenericDoubleVisitor::ApplyVisitor::apply(Block& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(AsyncBlock& node)
{
    if (node.hasTarget()) user_visitor->visit(*node.getTarget());
    if (node.hasBlock ()) user_visitor->visit(*node.getBlock ());
    if (node.hasGrid  ()) user_visitor->visit(*node.getGrid  ());

    for (Statement* object: node.objects)
        user_visitor->visit(*object);
}

void GenericDoubleVisitor::ApplyVisitor::apply(AtomicBlock& node)
{
    for (Statement* object: node.objects)
        user_visitor->visit(*object);
}

void GenericDoubleVisitor::ApplyVisitor::apply(FlowBlock& node)
{
    for (Statement* object: node.objects)
        user_visitor->visit(*object);
}

void GenericDoubleVisitor::ApplyVisitor::apply(LockBlock& node)
{
    for (Statement* object: node.objects)
        user_visitor->visit(*object);
}

void GenericDoubleVisitor::ApplyVisitor::apply(NormalBlock& node)
{
    for (Statement* object: node.objects)
        user_visitor->visit(*object);
}

void GenericDoubleVisitor::ApplyVisitor::apply(PipelineBlock& node)
{
    for (Statement* object: node.objects)
        user_visitor->visit(*object);
}


void GenericDoubleVisitor::ApplyVisitor::apply(Identifier& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(SimpleIdentifier& node)
{
    UNUSED_ARGUMENT(node);
}

void GenericDoubleVisitor::ApplyVisitor::apply(NestedIdentifier& node)
{
    for(auto* identifier : node.identifier_list)
    {
        user_visitor->visit(*identifier);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(TemplatedIdentifier& node)
{
    if(node.id) user_visitor->visit(*node.id);

    for(auto* templated : node.templated_type_list)
    {
        user_visitor->visit(*templated);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(TypeSpecifier&)
{
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(FunctionSpecifier& node)
{
    for (auto*const param_type : node.getParameterTypes())
    {
        user_visitor->visit(*param_type);
    }

    if (auto*const return_type = node.getReturnType())
    {
        user_visitor->visit(*return_type);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(MultiSpecifier& node)
{
    for (auto*const type: node.getTypes())
    {
        user_visitor->visit(*type);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(NamedSpecifier& node)
{
    user_visitor->visit(*node.getName());
}

void GenericDoubleVisitor::ApplyVisitor::apply(PrimitiveSpecifier& node) {}

//////////////////////////////////////////////////////////////////////
/// Type
void GenericDoubleVisitor::ApplyVisitor::apply(Type&          node) { UNUSED_ARGUMENT(node); UNREACHABLE_CODE(); }
void GenericDoubleVisitor::ApplyVisitor::apply(PrimitiveType& node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(PointerType&   node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(ReferenceType& node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(MultiType&     node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(DeclType&      node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(EnumType&      node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(RecordType&    node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(TypedefType&   node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(TypenameType&  node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(FunctionType&  node) {}

//////////////////////////////////////////////////////////////////////
/// Literal
void GenericDoubleVisitor::ApplyVisitor::apply(Literal&           node) { UNUSED_ARGUMENT(node); UNREACHABLE_CODE(); }
void GenericDoubleVisitor::ApplyVisitor::apply(FunctionIdLiteral& node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(NumericLiteral&    node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(ObjectLiteral&     node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(StringLiteral&     node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(IdLiteral&         node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(SymbolIdLiteral&   node) {}
void GenericDoubleVisitor::ApplyVisitor::apply(TypeIdLiteral&     node) {}

//////////////////////////////////////////////////////////////////////
/// Module
void GenericDoubleVisitor::ApplyVisitor::apply(Config& node)
{
}

void GenericDoubleVisitor::ApplyVisitor::apply(Internal& node)
{
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::VOID_TYPE   ))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::VOID_TYPE   ));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::BOOL_TYPE   ))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::BOOL_TYPE   ));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT8_TYPE   ))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::INT8_TYPE   ));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT16_TYPE  ))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::INT16_TYPE  ));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT32_TYPE  ))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::INT32_TYPE  ));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT64_TYPE  ))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::INT64_TYPE  ));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT32_TYPE))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT32_TYPE));
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT64_TYPE))  user_visitor->visit(*node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT64_TYPE));

    if(node.getPrimitiveType(PrimitiveKind::VOID_TYPE   ))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::VOID_TYPE   ));
    if(node.getPrimitiveType(PrimitiveKind::BOOL_TYPE   ))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::BOOL_TYPE   ));
    if(node.getPrimitiveType(PrimitiveKind::INT8_TYPE   ))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::INT8_TYPE   ));
    if(node.getPrimitiveType(PrimitiveKind::INT16_TYPE  ))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::INT16_TYPE  ));
    if(node.getPrimitiveType(PrimitiveKind::INT32_TYPE  ))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::INT32_TYPE  ));
    if(node.getPrimitiveType(PrimitiveKind::INT64_TYPE  ))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::INT64_TYPE  ));
    if(node.getPrimitiveType(PrimitiveKind::FLOAT32_TYPE))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::FLOAT32_TYPE));
    if(node.getPrimitiveType(PrimitiveKind::FLOAT64_TYPE))  user_visitor->visit(*node.getPrimitiveType(PrimitiveKind::FLOAT64_TYPE));

    for(auto* type : node.type_set) user_visitor->visit(*type);
}

void GenericDoubleVisitor::ApplyVisitor::apply(Tangle& node)
{
    if(node.internal) user_visitor->visit(*node.internal);
    if(node.root) user_visitor->visit(*node.root);
}

void GenericDoubleVisitor::ApplyVisitor::apply(Source& node)
{
    for(auto* import : node.imports)
    {
        user_visitor->visit(*import);
    }
    for(auto* decl : node.declares)
    {
        user_visitor->visit(*decl);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(Package& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.id) user_visitor->visit(*node.id);

    for(auto* child : node.children)
    {
        user_visitor->visit(*child);
    }
    for(auto* source : node.sources)
    {
        user_visitor->visit(*source);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(Import& node)
{
    if(node.ns) user_visitor->visit(*node.ns);
}

//////////////////////////////////////////////////////////////////////
/// Declaration
void GenericDoubleVisitor::ApplyVisitor::apply(Declaration& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(ClassDecl& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.name) user_visitor->visit(*node.name);
    if(node.base) user_visitor->visit(*node.base);
    if(node.getType()) user_visitor->visit(*node.getType());

    for(auto* impl : node.implements)
    {
        user_visitor->visit(*impl);
    }
    for(auto* attribute : node.member_variables)
    {
        user_visitor->visit(*attribute);
    }
    for(auto* method : node.member_functions)
    {
        user_visitor->visit(*method);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(EnumDecl& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.name) user_visitor->visit(*node.name);
    if(node.getType()) user_visitor->visit(*node.getType());

    for(auto* value : node.values)
    {
        user_visitor->visit(*value);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(FunctionDecl& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.name) user_visitor->visit(*node.name);

    for(auto* param : node.parameters)
    {
        user_visitor->visit(*param);
    }
    if(node.type) user_visitor->visit(*node.type);
    if(node.block) user_visitor->visit(*node.block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(TypedefDecl& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.name) user_visitor->visit(*node.name);
    if(node.getType()) user_visitor->visit(*node.getType());

    if(node.type) user_visitor->visit(*node.type);
}

void GenericDoubleVisitor::ApplyVisitor::apply(VariableDecl& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.name) user_visitor->visit(*node.name);

    if(node.initializer) user_visitor->visit(*node.initializer);
    if(node.type) user_visitor->visit(*node.type);
}

void GenericDoubleVisitor::ApplyVisitor::apply(TypenameDecl& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());
    if(node.name) user_visitor->visit(*node.name);
    if(node.getType()) user_visitor->visit(*node.getType());

    if(node.specialized_type) user_visitor->visit(*node.specialized_type);
    if(node.default_type) user_visitor->visit(*node.default_type);
}

//////////////////////////////////////////////////////////////////////
/// Statement
void GenericDoubleVisitor::ApplyVisitor::apply(Statement& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(DeclarativeStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.declaration) user_visitor->visit(*node.declaration);
}

void GenericDoubleVisitor::ApplyVisitor::apply(ExpressionStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.expr) user_visitor->visit(*node.expr);
}

void GenericDoubleVisitor::ApplyVisitor::apply(IterativeStmt& node)
{
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(ForStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.init) user_visitor->visit(*node.init);
    if(node.cond) user_visitor->visit(*node.cond);
    if(node.block) user_visitor->visit(*node.block);
    if(node.step) user_visitor->visit(*node.step);
}

void GenericDoubleVisitor::ApplyVisitor::apply(ForeachStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.var_decl) user_visitor->visit(*node.var_decl);
    if(node.range) user_visitor->visit(*node.range);
    if(node.block) user_visitor->visit(*node.block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(WhileStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.cond) user_visitor->visit(*node.cond);
    if(node.block) user_visitor->visit(*node.block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(Selection& node)
{
    if (node.cond ) user_visitor->visit(*node.cond );
    if (node.block) user_visitor->visit(*node.block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(SelectionStmt& node)
{
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(IfElseStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.if_branch) user_visitor->visit(*node.if_branch);
    for(Selection* elseif_branch: node.elseif_branches)
        user_visitor->visit(*elseif_branch);
    if(node.else_block) user_visitor->visit(*node.else_block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(SwitchStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.node) user_visitor->visit(*node.node);
    for(Selection* case_: node.cases)
        user_visitor->visit(*case_);
    if(node.default_block) user_visitor->visit(*node.default_block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(BranchStmt& node)
{
    if(node.getAnnotations()) user_visitor->visit(*node.getAnnotations());

    if(node.result) user_visitor->visit(*node.result);
}

//////////////////////////////////////////////////////////////////////
/// Expression
void GenericDoubleVisitor::ApplyVisitor::apply(Expression& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
}

void GenericDoubleVisitor::ApplyVisitor::apply(IdExpr& node)
{
    if(auto*const id = node.getId())
        user_visitor->visit(*id);
}

void GenericDoubleVisitor::ApplyVisitor::apply(LambdaExpr& node)
{
    if(auto*const lambda = node.getLambda())
        user_visitor->visit(*lambda);
}

void GenericDoubleVisitor::ApplyVisitor::apply(UnaryExpr& node)
{
    if(node.node) user_visitor->visit(*node.node);
}

void GenericDoubleVisitor::ApplyVisitor::apply(BinaryExpr& node)
{
    if(node.isRighAssociative())
    {
        if(node.right) user_visitor->visit(*node.right);
        if(node.left) user_visitor->visit(*node.left);
    }
    else
    {
        if(node.left) user_visitor->visit(*node.left);
        if(node.right) user_visitor->visit(*node.right);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(TernaryExpr& node)
{
    if(node.cond) user_visitor->visit(*node.cond);
    if(node.true_node) user_visitor->visit(*node.true_node);
    if(node.false_node) user_visitor->visit(*node.false_node);
}

void GenericDoubleVisitor::ApplyVisitor::apply(MemberExpr& node)
{
    if(node.node) user_visitor->visit(*node.node);
    if(node.member) user_visitor->visit(*node.member);
}

void GenericDoubleVisitor::ApplyVisitor::apply(SystemCallExpr& node)
{
    for (auto* param : node.parameters)
        if (param != nullptr)
            user_visitor->visit(*param);
}

void GenericDoubleVisitor::ApplyVisitor::apply(CallExpr& node)
{
    if(node.node) user_visitor->visit(*node.node);
    for(auto* param : node.parameters)
        user_visitor->visit(*param);
}

void GenericDoubleVisitor::ApplyVisitor::apply(ArrayExpr& node)
{
    if(node.array_type) user_visitor->visit(*node.array_type);
    if(node.element_type) user_visitor->visit(*node.element_type);
    for(auto* elem : node.elements)
        user_visitor->visit(*elem);
}

void GenericDoubleVisitor::ApplyVisitor::apply(CastExpr& node)
{
    if(node.node) user_visitor->visit(*node.node);
    if(node.type) user_visitor->visit(*node.type);
}

void GenericDoubleVisitor::ApplyVisitor::apply(BlockExpr& node)
{
    if(node.block) user_visitor->visit(*node.block);
}

void GenericDoubleVisitor::ApplyVisitor::apply(IsaExpr& node)
{
    if(node.node) user_visitor->visit(*node.node);
    if(node.type) user_visitor->visit(*node.type);
}

void GenericDoubleVisitor::ApplyVisitor::apply(TieExpr& node)
{
    for (Expression* tied_expression: node.tied_expressions)
    {
        BOOST_ASSERT(tied_expression && "null pointer exception");

        user_visitor->visit(*tied_expression);
    }
}

void GenericDoubleVisitor::ApplyVisitor::apply(IndexExpr& node)
{
    if(node.array) user_visitor->visit(*node.array);
    if(node.index) user_visitor->visit(*node.index);
}

void GenericDoubleVisitor::ApplyVisitor::apply(UnpackExpr& node)
{
    if(node.call) user_visitor->visit(*node.call);

    for (SimpleIdentifier* sid : node.unpack_list)
        user_visitor->visit(*sid);
}

void GenericDoubleVisitor::ApplyVisitor::apply(StringizeExpr& node)
{
    if(node.node) user_visitor->visit(*node.node);
}

} } } }

