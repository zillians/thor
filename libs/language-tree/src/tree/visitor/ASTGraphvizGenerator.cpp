/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>
#include <iostream>
#include <iomanip>

#include <boost/algorithm/string/replace.hpp>
#include <boost/assert.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <boost/range/iterator_range.hpp>

#include "core/Common.h"
#include "utility/UnicodeUtil.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ResolverContext.h"
#include "language/context/TransformerContext.h"
#include "language/tree/visitor/ASTGraphvizGenerator.h"
#include "language/stage/parser/context/SourceInfoContext.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace tree = language::tree;
//////////////////////////////////////////////////////////////////////////////
// ASTGraphvizNodeGenerator
//////////////////////////////////////////////////////////////////////////////

// graphviz has a bug, might be triggered with clustering in our case.
// sometimes. turn off the 'class clustering' might make it go away.
// see http://www.graphviz.org/mantisbt/view.php?id=2200
//     http://www.graphviz.org/mantisbt/view.php?id=2034
static const bool enable_class_clustering = false;
static const bool enable_function_clustering = true;

static std::wstring random_color()
{
    std::wstring result;
    for(int i = 0; i < 6; ++i)
    {
        result += (L'b' + rand() % 5);
    }
    return result;
}

void ASTGraphvizNodeGenerator::inc_level()
{
    ++level;
}

void ASTGraphvizNodeGenerator::dec_level()
{
    --level;
}

void ASTGraphvizNodeGenerator::print_indent()
{
    for(int i = 0; i < level; ++i)
        stream << L"    ";
}

void ASTGraphvizNodeGenerator::open_sub(const std::wstring& name)
{
    print_indent();
    stream << L"subgraph cluster_" << subgraph_serial_num++ << L" {" << std::endl;
    inc_level();
    print_indent();
    stream << L"style=filled; "
           << L"label=\"" << name << L"\"; "
           << L"fontsize=\"40\"; "
           << L"color=\"#" << random_color() << L"\";" << std::endl;
}

void ASTGraphvizNodeGenerator::close_sub()
{
    dec_level();
    print_indent(); stream << L"}" << std::endl;
}

void ASTGraphvizNodeGenerator::addNode(tree::ASTNode& node,
             std::wstring label,
             const std::wstring& shape,
             const std::wstring& borderColor,
             std::wstring fillColor)
{
    using namespace zillians::language::stage;

    std::wstring color = borderColor;
    int penwidth = 1;
    // error aware??
    if(node.parent == NULL)
    {
        color = L"red";
        penwidth = 4;
    }

    print_indent();
    stream << L"n" << std::hex << &node << L" [label=\"" << std::hex << &node << L" " << node.instanceName() << std::dec;
    if(label != L"") {
        boost::algorithm::replace_all(label, L"\"", L"\\\"");
        stream << L" : " << label ;
    }

    SourceInfoContext* source_info = SourceInfoContext::get(&node);
    if(source_info == NULL)
    {
        fillColor = L"green";
    }
    else
    {
        stream << L"(" << source_info->line << L":" << source_info->column << L")";
    }
    if(NameManglingContext* mangling = NameManglingContext::get(&node))
    {
        stream << L"[" << s_to_ws(mangling->mangled_name) << L"]";
    }
    stream << L"\"";

    if(shape       != L"") stream << L", shape=\""                       << shape       << "\"";
    if(color       != L"") stream << L", color=\""                       << color       << "\"";
    if(penwidth    != 1  ) stream << L", penwidth=\""                    << penwidth       << "\"";
    if(fillColor   != L"") stream << L", style=\"filled\", fillcolor=\"" << fillColor   << "\"";

    stream << L"];" << std::endl;
}

void ASTGraphvizNodeGenerator::label(tree::ASTNode& node)
{
    addNode(node);
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Identifier& node)
{
    addNode(node, node.toString());
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::UnaryExpr& node)
{
    addNode(node, tree::UnaryExpr::OpCode::toString(node.opcode));
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::BinaryExpr& node)
{
    addNode(node, tree::BinaryExpr::OpCode::toString(node.opcode));
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::TemplatedIdentifier& node)
{
    addNode(node, node.toString() + (node.type == tree::TemplatedIdentifier::Usage::FORMAL_PARAMETER ? L"FORMAL" : L"ACTUAL"));
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Import& node)
{
    print_indent();
    stream << L"n" << std::hex << &node << L" [label=\"" << node.instanceName() << std::dec;
    if(node.alias)
    {
        if(node.alias->isEmpty())
        {
            stream << L"[alias: . => " << node.ns->toString() << L"]";
        }
        else
        {
            stream << L"[alias: " << node.alias->toString() << L" => " << node.ns->toString() << L"]";
        }
    }
    else
    {
        stream << L"[import: " << node.ns->toString() << L"]";
    }
    stream << L"\"];" << std::endl;
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Package& node)
{
    print_indent();
    stream << L"n" << std::hex << &node << L" [label=\"" << node.instanceName() << std::dec;
    if(node.id->isEmpty())
        stream << L"[root_package]";
    else
        stream << L" : " << node.id->toString();
    stream << L"\"];" << std::endl;
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Block& node)
{
    SourceInfoContext* source_info = SourceInfoContext::get(&node);

    print_indent();
    stream << L"n" << std::hex << &node << L" [label=\"" << node.instanceName() << std::dec;

    if (source_info)
    {
        stream << L"(" << std::dec << source_info->line << L":" << source_info->column << L")";
    }

    stream << L"\"];" << std::endl;

    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::BlockExpr& node)
{
	addNode(node, s_to_ws(node.tag));
	revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::IsaExpr& node)
{
    addNode(node);
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::CastExpr& node)
{
    addNode(node, s_to_ws(node.instanceName()) + L'[' + s_to_ws(tree::CastExpr::CastMethod::toString(node.method)) + L']');
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::TypeSpecifier& node)
{
    addNode(node, node.toString());

    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::MultiSpecifier& node)
{
    addNode(node, L"(multi-type)");

    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Literal& node)
{
    addNode(node, node.toString());
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Type& node)
{
    addNode(node, node.toString());
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::FunctionType& node)
{
	std::wstring name = node.toString();

    addNode(node, name);
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::Declaration& node)
{
    if(node.name)
        addNode(node, node.name->toString());
    else
        addNode(node, L"(no-name)");
    revisit(node);
}

void ASTGraphvizNodeGenerator::label(tree::ClassDecl& node)
{
    if(node.is_interface)
    {
        if(enable_class_clustering) open_sub(L"interface " + node.name->toString());
        addNode(node, node.name->toString() + L" [interface]");
    }
    else
    {
        if(enable_class_clustering) open_sub(L"class " + node.name->toString());
        addNode(node, node.name->toString() + L" [class]");
    }

    revisit(node);
    if(enable_class_clustering) close_sub();
}

void ASTGraphvizNodeGenerator::label(tree::FunctionDecl& node)
{
    if(enable_function_clustering) open_sub(L"function " + node.name->toString() + L"()");

    addNode(node);
    revisit(node);

    if(enable_function_clustering) close_sub();
}

//////////////////////////////////////////////////////////////////////////////
// ASTGraphvizParentEdgeGenerator
//////////////////////////////////////////////////////////////////////////////

void ASTGraphvizParentEdgeGenerator::genParentEdge(tree::ASTNode& node)
{
    //addParentEdge(node.parent, &node);

    // ResolvedType
    if(auto* context = zillians::language::ResolvedType::get(&node))
    {
        addParentEdge(context, &node, L"ResolvedType", L"purple");
    }

    // ResolvedSymbol
    if(auto* context = zillians::language::ResolvedSymbol::get(&node))
    {
        addParentEdge(context, &node, L"ResolvedSymbol", L"orange");
    }

    // SplitReferenceContext
    if(auto* context = zillians::language::SplitReferenceContext::get(&node))
    {
        addParentEdge(context, &node, L"SplitReferenceContext", L"blue");
    }

    // InstantiatedFrom Context
    if(auto* context = zillians::language::InstantiatedFrom::get(&node))
    {
        addParentEdge(context, &node, L"InstantiatedFrom", L"green2");
    }

    if(auto* context = zillians::language::SpecializationOf::get(&node))
    {
        addParentEdge(context, &node, L"SpecializationOf", L"darksalmon");
    }

    revisit(node);
}

void ASTGraphvizParentEdgeGenerator::addParentEdge(tree::ASTNode* from, tree::ASTNode* to, const std::wstring& label, const std::wstring& edgeColor)
{
    // edge
    os_ << L"    n"
        << std::hex << from
        << L" -> n"
        << std::hex << to
        << std::dec;

    // attribute
    std::wstring color = edgeColor;
    if(color == L"") color = L"lightgray";

    os_ << L" [dir=\"back\", arrowtail=\"vee\"";
    if(label != L"") os_ << L", label=\""     << label << L"\"";
    if(label != L"") os_ << L", fontsize=\""  << 7     << L"\"";
    if(color != L"") os_ << L", color=\""     << color << L"\"";
    if(color != L"") os_ << L", fontcolor=\"" << color << L"\"";

    os_ << L"];" << std::endl;
}

//////////////////////////////////////////////////////////////////////////////
// ASTGraphvizChildEdgeGenerator
//////////////////////////////////////////////////////////////////////////////

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ASTNode& node)
{
    UNUSED_ARGUMENT(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Annotation& node)
{
    if(node.name) addChildEdge(&node, node.name, L"name");
    int index = 0;
    for(auto& attribute : node.attribute_list)
    {
        if(attribute.first)
        {
        	wchar_t tmp[64]; std::swprintf(tmp, 64, L"attribute_list[%d]->first", index);
        	addChildEdge(&node, attribute.first , tmp);
        }
        if(attribute.second)
        {
        	wchar_t tmp[64]; std::swprintf(tmp, 64, L"attribute_list[%d]->second", index);
        	addChildEdge(&node, attribute.second, tmp);
        }
        ++index;
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Annotations& node)
{
	int index = 0;
    for(auto& annotation : node.annotation_list)
    {
    	wchar_t tmp[64]; std::swprintf(tmp, 64, L"annotation_list[%d]", index++);
    	addChildEdge(&node, annotation, tmp);
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Block& node)
{
	int index = 0;
    for(auto& object : node.objects)
	{
    	wchar_t tmp[64]; std::swprintf(tmp, 64, L"objects[%d]", index++);
    	addChildEdge(&node, object, tmp);
	}

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Identifier& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::SimpleIdentifier& node)
{
    UNUSED_ARGUMENT(node);
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::NestedIdentifier& node)
{
	int index = 0;
    for(auto& identifier : node.identifier_list)
	{
    	wchar_t tmp[64]; std::swprintf(tmp, 64, L"identifier_list[%d]", index++);
    	addChildEdge(&node, identifier, tmp);
	}
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TemplatedIdentifier& node)
{
    if(node.id) addChildEdge(&node, node.id, L"id");

    int index = 0;
    for(auto& templated : node.templated_type_list)
    {
    	wchar_t tmp[64]; std::swprintf(tmp, 64, L"templated_type_list[%d]", index++);
        addChildEdge(&node, templated, tmp);
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Literal&)
{
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TypeSpecifier&)
{
    UNREACHABLE_CODE();
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::FunctionSpecifier& node)
{
    using boost::adaptors::indexed;

    for (auto param_types = node.getParameterTypes() | indexed(0); !param_types.empty(); param_types.pop_front())
    {
        const auto&      index      = param_types.begin().index();
              auto*const param_type = param_types.front();

        addChildEdge(&node, param_type, L"param[" + std::to_wstring(index) + L']');
    }

    if (auto*const return_type = node.getReturnType())
        addChildEdge(&node, return_type, L"return");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::MultiSpecifier& node)
{
    using boost::adaptors::indexed;

    for (auto types = node.getTypes() | indexed(0); !types.empty(); types.pop_front())
    {
        const auto&      index = types.begin().index();
              auto*const type  = types.front();

        addChildEdge(&node, type, L"type[" + std::to_wstring(index) + L']');
    }

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::NamedSpecifier& node)
{
    addChildEdge(&node, node.getName(), L"name");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::PrimitiveSpecifier& node) {}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::PrimitiveType& node)
{
    //UNREACHABLE_CODE();
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::PointerType& node)
{
    if(node.getPointeeType() != nullptr)
        addChildEdge(&node, node.getPointeeType(), L"pointee_type", L"pink");
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ReferenceType& node)
{
    if(node.getPointeeType() != nullptr)
        addChildEdge(&node, node.getPointeeType(), L"pointee_type", L"pink");
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::RecordType& node)
{
    if(node.getDecl() != nullptr)
        addChildEdge(&node, node.getDecl(), L"class_decl", L"pink");
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::EnumType& node)
{
    if(node.getDecl() != nullptr)
        addChildEdge(&node, node.getDecl(), L"enum_decl", L"pink");
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TypenameType& node)
{
    if(node.getDecl() != nullptr)
        addChildEdge(&node, node.getDecl(), L"typename_decl", L"pink");
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TypedefType& node)
{
    if(node.getDecl() != nullptr)
        addChildEdge(&node, node.getDecl(), L"typedef_decl", L"pink");
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::FunctionType& node)
{
    for(auto& type : node.parameter_types)
        addChildEdge(&node, type, L"parameter_types");

    if(node.return_type) addChildEdge(&node, node.return_type, L"return_type");
    revisit(node);
}

//////////////////////////////////////////////////////////////////////
/// Module
void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Internal& node)
{
    using namespace tree;

    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::VOID_TYPE   )) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::VOID_TYPE   ), L"VoidTy"    );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::BOOL_TYPE   )) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::BOOL_TYPE   ), L"BooleanTy" );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT8_TYPE   )) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::INT8_TYPE   ), L"Int8Ty"    );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT16_TYPE  )) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::INT16_TYPE  ), L"Int16Ty"   );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT32_TYPE  )) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::INT32_TYPE  ), L"Int32Ty"   );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::INT64_TYPE  )) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::INT64_TYPE  ), L"Int64Ty"   );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT32_TYPE)) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT32_TYPE), L"Float32Ty" );
    if(node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT64_TYPE)) addChildEdge(&node, node.getPrimitiveTypeSpecifier(PrimitiveKind::FLOAT64_TYPE), L"Float64Ty" );

    if(node.getPrimitiveType(PrimitiveKind::VOID_TYPE   )) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::VOID_TYPE   ), L"VoidTy"    );
    if(node.getPrimitiveType(PrimitiveKind::BOOL_TYPE   )) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::BOOL_TYPE   ), L"BooleanTy" );
    if(node.getPrimitiveType(PrimitiveKind::INT8_TYPE   )) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::INT8_TYPE   ), L"Int8Ty"    );
    if(node.getPrimitiveType(PrimitiveKind::INT16_TYPE  )) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::INT16_TYPE  ), L"Int16Ty"   );
    if(node.getPrimitiveType(PrimitiveKind::INT32_TYPE  )) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::INT32_TYPE  ), L"Int32Ty"   );
    if(node.getPrimitiveType(PrimitiveKind::INT64_TYPE  )) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::INT64_TYPE  ), L"Int64Ty"   );
    if(node.getPrimitiveType(PrimitiveKind::FLOAT32_TYPE)) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::FLOAT32_TYPE), L"Float32Ty" );
    if(node.getPrimitiveType(PrimitiveKind::FLOAT64_TYPE)) addChildEdge(&node, node.getPrimitiveType(PrimitiveKind::FLOAT64_TYPE), L"Float64Ty" );

    for(auto& type : node.type_set)
    {
        addChildEdge(&node, type, L"type_set");
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Tangle& node)
{
    if(node.config  ) addChildEdge(&node, node.config  , L"config"  );
    if(node.internal) addChildEdge(&node, node.internal, L"internal");
    if(node.root    ) addChildEdge(&node, node.root    , L"root"    );

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Source& node)
{
    int index = 0;
    for(auto& imported : node.imports)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"imports[%d]", index++);
        addChildEdge(&node, imported, tmp);
    }

    index = 0;
    for(auto& decl : node.declares)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"declares[%d]", index++);
        addChildEdge(&node, decl, tmp);
    }

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Package& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.id) addChildEdge(&node, node.id, L"id");

    int index = 0;
    for(auto& child : node.children)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"children[%d]", index++);
        addChildEdge(&node, child, tmp);
    }

    index = 0;
    for(auto& source : node.sources)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"sources[%d]", index++);
        addChildEdge(&node, source, tmp);
    }

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Import& node)
{
    if(node.ns) addChildEdge(&node, node.ns, L"ns");
    revisit(node);
}

//////////////////////////////////////////////////////////////////////
/// Declaration
void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Declaration& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ClassDecl& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");
    if(node.name)             addChildEdge(&node, node.name            , L"name");
    if(node.base)             addChildEdge(&node, node.base            , L"base");
    if(node.getType())        addChildEdge(&node, node.getType()       , L"type");

    int index = 0;
    for(auto& impl : node.implements)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"implements[%d]", index++);
        addChildEdge(&node, impl, tmp);
    }
    index = 0;
    for(auto& method : node.member_functions)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"member_functions[%d]", index++);
        addChildEdge(&node, method, tmp);
    }
    index = 0;
    for(auto& attribute : node.member_variables)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"member_variables[%d]", index++);
        addChildEdge(&node, attribute, tmp);
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::EnumDecl& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");
    if(node.name)        addChildEdge(&node, node.name       , L"name");
    if(node.getType())   addChildEdge(&node, node.getType()  , L"type");

    int index = 0;
    for(auto& value : node.values)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"values[%d]", index++);
        addChildEdge(&node, value, tmp);
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::FunctionDecl& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.name) addChildEdge(&node, node.name, L"name");
    int index = 0;
    for(auto& param : node.parameters)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"parameters[%d]", index++);
        addChildEdge(&node, param, tmp);
    }
    if(node.type ) addChildEdge(&node, node.type , L"type" );
    if(node.block) addChildEdge(&node, node.block, L"block");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TypedefDecl& node)
{
    if(node.type)      addChildEdge(&node, node.type     , L"def_type");
    if(node.name)      addChildEdge(&node, node.name     , L"name");
    if(node.getType()) addChildEdge(&node, node.getType(), L"type");

    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::VariableDecl& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.initializer) addChildEdge(&node, node.initializer, L"initializer");
    if(node.name) addChildEdge(&node, node.name, L"name");
    if(node.type) addChildEdge(&node, node.type, L"type");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TypenameDecl& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");
    if(node.name) addChildEdge(&node, node.name, L"name");

    if(node.specialized_type) addChildEdge(&node, node.specialized_type, L"specialized_type");
    if(node.default_type    ) addChildEdge(&node, node.default_type    , L"default_type");
    if(node.getType()       ) addChildEdge(&node, node.getType()       , L"typename_type");
    revisit(node);
}

//////////////////////////////////////////////////////////////////////
/// Statement
void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Statement& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::DeclarativeStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");
    if(node.declaration     ) addChildEdge(&node, node.declaration     , L"declaration");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ExpressionStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.expr) addChildEdge(&node, node.expr, L"expr");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ForStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.init)  addChildEdge(&node, node.init , L"init");
    if(node.cond)  addChildEdge(&node, node.cond , L"cond");
    if(node.block) addChildEdge(&node, node.block, L"block");
    if(node.step)  addChildEdge(&node, node.step , L"step");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ForeachStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.var_decl) addChildEdge(&node, node.var_decl, L"var_decl");
    if(node.range) addChildEdge(&node, node.range, L"range");
    if(node.block) addChildEdge(&node, node.block, L"block");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::WhileStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.cond) addChildEdge(&node, node.cond, L"cond");
    if(node.block) addChildEdge(&node, node.block, L"block");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Selection& node)
{
    if (node.cond ) addChildEdge(&node, node.cond , L"cond" );
    if (node.block) addChildEdge(&node, node.block, L"block");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::IfElseStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.if_branch) addChildEdge(&node, node.if_branch, L"if_branch");
    int index = 0;
    for(const auto* elseif_branch: node.elseif_branches)
    {
        wchar_t tmp[64];

        std::swprintf(tmp, 64, L"elseif_branches[%d]", index);

        addChildEdge(&node, elseif_branch, tmp);

        ++index;
    }
    if(node.else_block) addChildEdge(&node, node.else_block);
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::SwitchStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.node) addChildEdge(&node, node.node, L"node");
    int index = 0;
    for(const auto* case_: node.cases)
    {
        wchar_t tmp[64];

        std::swprintf(tmp,  64, L"case[%d]", index);

        addChildEdge(&node, case_, tmp);

        ++index;
    }
    if(node.default_block) addChildEdge(&node, node.default_block, L"default");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::BranchStmt& node)
{
    if(node.getAnnotations()) addChildEdge(&node, node.getAnnotations(), L"annotations");

    if(node.result) addChildEdge(&node, node.result, L"result");
    revisit(node);
}

//////////////////////////////////////////////////////////////////////
/// Expression
void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::Expression& node)
{
    UNUSED_ARGUMENT(node);
    UNREACHABLE_CODE();
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::IdExpr& node)
{
    addChildEdge(&node, node.getId(), L"identifier");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::LambdaExpr& node)
{
    addChildEdge(&node, node.getLambda(), L"lambda");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::UnaryExpr& node)
{
    if(node.node) addChildEdge(&node, node.node, L"node");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::BinaryExpr& node)
{
    if(node.isRighAssociative())
    {
        if(node.right) addChildEdge(&node, node.right, L"right");
        if(node.left) addChildEdge(&node, node.left, L"left");
    }
    else
    {
        if(node.left) addChildEdge(&node, node.left, L"left");
        if(node.right) addChildEdge(&node, node.right, L"right");
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TernaryExpr& node)
{
    if(node.cond) addChildEdge(&node, node.cond, L"cond");
    if(node.true_node) addChildEdge(&node, node.true_node, L"true_node");
    if(node.false_node) addChildEdge(&node, node.false_node, L"false_node");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::MemberExpr& node)
{
    if(node.node) addChildEdge(&node, node.node, L"node");
    if(node.member) addChildEdge(&node, node.member, L"member");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::CallExpr& node)
{
    if(node.node) addChildEdge(&node, node.node, L"node");
    int index = 0;
    for(auto& param : node.parameters)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"parameters[%d]", index++);
        addChildEdge(&node, param, tmp);
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::ArrayExpr& node)
{
    int index = 0;
    if(node.array_type) addChildEdge(&node, node.array_type, L"array_type");
    if(node.element_type) addChildEdge(&node, node.element_type, L"element_type");
    for(auto& elem : node.elements)
    {
        wchar_t tmp[64]; std::swprintf(tmp, 64, L"elements[%d]", index++);
        addChildEdge(&node, elem, tmp);
    }
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::CastExpr& node)
{
    if(node.node) addChildEdge(&node, node.node, L"node");
    if(node.type) addChildEdge(&node, node.type, L"type");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::BlockExpr& node)
{
    if(node.block) addChildEdge(&node, node.block, L"block");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::IsaExpr& node)
{
    if(node.node) addChildEdge(&node, node.node, L"node");
    if(node.type) addChildEdge(&node, node.type, L"type");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::TieExpr& node)
{
    const auto& tied_exprs_range = node.tied_expressions | boost::adaptors::indexed(0);

    for (auto i = tied_exprs_range.begin(); i != tied_exprs_range.end(); ++i)
    {
        wchar_t label[64]; std::swprintf(label, 64, L"tied_expr[%d]", i.index());

        const auto* tied_expr = *i;

        BOOST_ASSERT(tied_expr && "null pointer exception");

        addChildEdge(&node, tied_expr, label);
    }

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::IndexExpr& node)
{
    BOOST_ASSERT(node.array && "null pointer exception");
    BOOST_ASSERT(node.index && "null pointer exception");

    addChildEdge(&node, node.array, L"array");
    addChildEdge(&node, node.index, L"index");

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::UnpackExpr& node)
{
    addChildEdge(&node, node.call, L"call");

    const auto& unpack_list_range = node.unpack_list | boost::adaptors::indexed(0);

    for (auto i = unpack_list_range.begin(); i != unpack_list_range.end(); ++i)
    {
        wchar_t label[64]; std::swprintf(label, 64, L"unpack[%d]", i.index());

        const auto* unpack_name = *i;
        BOOST_ASSERT(unpack_name && "null pointer exception");

        addChildEdge(&node, unpack_name, label);
    }

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::SystemCallExpr& node)
{
    switch (node.call_type)
    {
    case tree::SystemCallExpr::CallType::ASYNC:
        BOOST_ASSERT(node.parameters.size() >= 2 && "unexpected parameter count, at least function id literal should be there");

        if (const auto*const assignee = node.parameters.front())
            addChildEdge(&node, assignee, L"assignee");

        addChildEdge(&node, node.parameters[1], L"decl_callee_id");

        {
            const auto& async_parameter_range = boost::make_iterator_range(node.parameters, 2, 0) | boost::adaptors::indexed(0);

            for (auto i = async_parameter_range.begin(); i != async_parameter_range.end(); ++i)
            {
                wchar_t label[64]; std::swprintf(label, 64, L"param[%d]", i.index());

                addChildEdge(&node, *i, label);
            }
        }

        break;

    default:
        UNREACHABLE_CODE();
        break;
    }

    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::genChildEdge(tree::StringizeExpr& node)
{
    addChildEdge(&node, node.node, L"stringize");
    revisit(node);
}

void ASTGraphvizChildEdgeGenerator::addChildEdge(const tree::ASTNode* parent, const tree::ASTNode* child, const std::wstring& label, const std::wstring& color)
{
    // edge
    os_ << L"    n"
        << std::hex << parent
        << L" -> n"
        << std::hex << child
        << std::dec;

    // attribute
    os_ << L" [";
    if(label != L""           ) os_ << L"label=\""      << label << L"\"";
    if(label != L""           ) os_ << L", fontsize=\"" << 7     << L"\"";
    if(color != L""           ) os_ << L", color=\""    << color << L"\"";
    if(child->parent != parent) os_ << L", penwidth=\"10\"";

    os_ << L"];" << std::endl;
}

} } } } // namespace 'zillians::language::stage::visitor'
