/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/statement/BranchStmt.h"

namespace zillians { namespace language { namespace tree {

const wchar_t* BranchStmt::OpCode::toString(type t)
{
    switch(t)
    {
    case BREAK   : return L"break";
    case CONTINUE: return L"continue";
    case RETURN  : return L"return";
    default      : UNREACHABLE_CODE(); return L"<non_op>";
    }
}

BranchStmt::BranchStmt() { }

BranchStmt::BranchStmt(OpCode::type opcode, Expression* result/* = nullptr*/) : opcode(opcode), result(result)
{
    BOOST_ASSERT(result == NULL || isa<Expression>(result));
    if(result) result->parent = this;
}

bool BranchStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (opcode)
        (result)
    );
}

bool BranchStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        replace_result,
        (result)
    );
}

BranchStmt* BranchStmt::clone() const
{
    auto cloned = new BranchStmt(opcode, clone_or_null(result));
    if(annotations) cloned->setAnnotations(annotations->clone());
    return cloned;
}

std::wostream& BranchStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << OpCode::toString(opcode);

    if (result != nullptr)
        output << L' ' << out_source(*result, indent);

    output << L';';

    return output;
}

} } }
