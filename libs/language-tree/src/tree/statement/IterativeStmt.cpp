/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/statement/IterativeStmt.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

IterativeStmt::IterativeStmt(Block* block) : block(block)
{
    if(block) block->parent = this;
}

bool IterativeStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (block)
    );
}

bool IterativeStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (block)
    );
}

ForStmt::ForStmt() : init(NULL), cond(NULL), step(NULL)
{ }

ForStmt::ForStmt(ASTNode* init, Expression* cond, Expression* step, Block* block) : IterativeStmt(block), init(init), cond(cond), step(step)
{
    BOOST_ASSERT(init && "null init for 'for statement' is not allowed");
    BOOST_ASSERT(cond && "null cond for 'for statement' is not allowed");
    BOOST_ASSERT(step && "null step for 'for statement' is not allowed");

    if(init) init->parent = this;
    if(cond) cond->parent = this;
    if(step) step->parent = this;
}

bool ForStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (init)
        (cond)
        (step)
    );
}

bool ForStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (init)
        (cond)
        (step)
    );
}

ForStmt* ForStmt::clone() const
{
    auto cloned = new ForStmt(
            (init) ? init->clone() : NULL,
            (cond) ? cond->clone() : NULL,
            (step) ? step->clone() : NULL,
            (block) ? block->clone() : NULL);
    if(annotations) cloned->setAnnotations(annotations->clone());
    return cloned;
}

std::wostream& ForStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << out_indent(indent) << L"for (";

    if (init != nullptr)
        output << out_source(*init, indent);

    output << L';';

    if (cond != nullptr)
        output << L' ' << out_source(*cond, indent);

    output << L';';

    if (step != nullptr)
        output << L' ' << out_source(*step, indent);

    output << L')';

    if (block == nullptr)
        output << L" ;";
    else
        output << std::endl << out_source(*block, indent);

    return output;
}

ForeachStmt::ForeachStmt() : var_decl(nullptr), range(nullptr)
{ }

ForeachStmt::ForeachStmt(VariableDecl* var_decl, Expression* range, Block* block) : IterativeStmt(block), var_decl(var_decl), range(range)
{
    BOOST_ASSERT(var_decl && "null var_decl for foreach statement is not allowed");
    BOOST_ASSERT(range && "null range for foreach statement is not allowed");

    var_decl->parent = this;
    range->parent = this;
}

bool ForeachStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (var_decl)
        (range   )
    );
}

bool ForeachStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (var_decl)
        (range   )
    );
}

ForeachStmt* ForeachStmt::clone() const
{
    auto cloned = new ForeachStmt(
            (var_decl) ? var_decl->clone() : NULL,
            (range) ? range->clone() : NULL,
            (block) ? block->clone() : NULL);
    if(annotations) cloned->setAnnotations(annotations->clone());
    return cloned;
}

std::wostream& ForeachStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(var_decl && "null pointer exception");
    BOOST_ASSERT(range && "null pointer exception");

    output << out_indent(indent) << L"foreach (" << out_source(*var_decl, indent) << L" in " << out_source(*range, indent) << L')';

    if (block == nullptr)
        output << L" ;";
    else
        output << std::endl << out_source(*block, indent);

    return output;
}

WhileStmt::WhileStmt() : style(Style::WHILE), cond(NULL)
{ }

WhileStmt::WhileStmt(Style::type style, Expression* cond, Block* block) : IterativeStmt(block), style(style), cond(cond)
{
    BOOST_ASSERT(cond && "null condition for while statement is not allowed");

    cond->parent = this;
}

bool WhileStmt::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (style)
        (cond )
    );
}

bool WhileStmt::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (cond)
    );
}

WhileStmt* WhileStmt::clone() const
{
    auto cloned = new WhileStmt(
            style,
            (cond) ? cond->clone() : NULL,
            (block) ? block->clone() : NULL);
    if(annotations) cloned->setAnnotations(annotations->clone());
    return cloned;
}

std::wostream& WhileStmt::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    if (style == Style::WHILE)
    {
        BOOST_ASSERT(cond && "null pointer exception");

        output << out_indent(indent) << L"while (" << out_source(*cond, indent) << L')';

        if (block == nullptr)
            output << L" ;";
        else
            output << std::endl << out_source(*block, indent);
    }
    else
    {
        BOOST_ASSERT(cond && "null pointer exception");

        output << out_indent(indent) << L"do" << std::endl;

        if (block == nullptr)
            output << out_indent(indent) << L'{' << std::endl
                   << out_indent(indent) << L'}';
        else
            output << out_source(*block, indent);

        output << L" while (" << out_source(*cond, indent) << L')';
    }

    return output;
}

} } }
