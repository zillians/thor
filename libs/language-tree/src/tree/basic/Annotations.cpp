/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <memory>
#include <ostream>
#include <string>
#include <sstream>

#include <boost/assert.hpp>
#include <boost/range/algorithm/remove_if.hpp>
#include <boost/range/iterator_range.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/basic/Identifier.h"

namespace zillians { namespace language { namespace tree {

Annotation::Annotation(SimpleIdentifier* name)
    : name(name)
{
    if(name) name->parent = this;
}

void Annotation::setName(SimpleIdentifier* s)
{
    if(name) name->parent = nullptr;
    if(s) s->parent = this;
    name = s;
}

void Annotation::appendKeyValue(SimpleIdentifier* key, ASTNode* value)
{
    key->parent = this;
    value->parent = this;
    attribute_list.push_back(std::make_pair(key, value));
}

bool Annotation::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (name          )
        (attribute_list)
    );
}

bool Annotation::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (attribute_list)
    );
}

Annotation* Annotation::clone() const
{
    Annotation* cloned = new Annotation(name ? name->clone() : nullptr);

    for(const auto& attribute: attribute_list)
    {
        cloned->appendKeyValue(
                (attribute.first) ? attribute.first->clone() : nullptr,
                (attribute.second) ? attribute.second->clone() : nullptr);
    }

    return cloned;
}

std::wostream& Annotation::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(name && "null pointer exception");

    output << L'@' << name->name;

    if (!attribute_list.empty())
    {
              auto i    = attribute_list.cbegin();
        const auto iend = attribute_list.cend();

        const auto attribute_to_source = [&output, indent](const std::pair<const SimpleIdentifier*,const ASTNode*>& attribute)
        {
            BOOST_ASSERT(attribute.first && "null pointer exception");

            output << attribute.first->name;

            if (attribute.second)
                output << L" = \"" << out_source(*attribute.second, indent) << L'"';
        };

        output << L"{ ";

        attribute_to_source(*i);

        while (++i != iend)
        {
            output << L", ";

            attribute_to_source(*i);
        }

        output << L" }";
    }

    return output;
}

bool Annotation::merge(Annotation& rhs)
{
    for(auto& rhs_attribute: rhs.attribute_list)
    {
        bool merged = false;

        for(auto& attribute: attribute_list)
        {
            if (rhs_attribute.first->isEqual(*(attribute.first)))
            {
                if (!rhs_attribute.second->isEqual(*(attribute.second)))
                {
                    return false;
                }
                else
                {
                    merged = true;

                    break;
                }
            }
        }

        if (!merged)
        {
            appendKeyValue(rhs_attribute.first, rhs_attribute.second);
        }
    }

    return true;
}

Annotations::Annotations() = default;

Annotations::Annotations(std::initializer_list<Annotation*> annotation_list)
    : Annotations()
{
    for (Annotation* annotation : annotation_list)
        appendAnnotation(annotation);
}

void Annotations::appendAnnotation(Annotation* annotation)
{
    annotation->parent = this;
    annotation_list.push_back(annotation);
}

void Annotations::removeAnnotation(Annotation* annotation)
{
    annotation_list.erase(
        std::remove(
            annotation_list.begin(),
            annotation_list.end(),
            annotation
        ),
        annotation_list.end()
    );
}

bool Annotations::hasAnnotation(const std::wstring& tag) const
{
    BOOST_ASSERT(!tag.empty() && "empty annotation tag");

    return findAnnotation(tag) != nullptr;
}

Annotation* Annotations::findAnnotation(const std::wstring& tag) const
{
    BOOST_ASSERT(!tag.empty() && "empty annotation tag");

    for(Annotation* annotation: annotation_list)
    {
        BOOST_ASSERT(annotation && "null pointer exception");

        if(annotation->name->toString() == tag)
        {
            return annotation;
        }
    }

    return nullptr;
}

namespace // un-named
{
    template <class Range>
    ASTNode* findAnnotationValueImpl(const Annotations& annotations, const Range& path)
    {
        auto path_range = boost::make_iterator_range(path);
        BOOST_ASSERT(!path_range.empty() && "path is empty!");


        Annotation* annotation = annotations.findAnnotation(path_range.front());

        for(path_range.pop_front(); annotation != nullptr && !path_range.empty(); path_range.advance_begin(1))
        {
            for(const auto& attribute: annotation->attribute_list)
            {
                BOOST_ASSERT(attribute.first && "null pointer exception");

                if(attribute.first->toString() == path_range.front())
                {
                    if(path_range.size() == 1)
                    {
                        return attribute.second;
                    }
                    else
                    {
                        annotation = cast_or_null<Annotation>(attribute.second);
                    }
                }
            }
        }

        return nullptr;
    }
} // un-named

Annotation* Annotations::findAnnotation(const std::vector<std::wstring>& path) const
{
    ASTNode* value = findAnnotationValueImpl(*this, path);

    return cast_or_null<Annotation>(value);
}

Annotation* Annotations::findAnnotation(std::initializer_list<std::wstring> path) const
{
    ASTNode* value = findAnnotationValueImpl(*this, path);

    return cast_or_null<Annotation>(value);
}

ASTNode* Annotations::findAnnotationValue(const std::vector<std::wstring>& path) const
{
    return findAnnotationValueImpl(*this, path);
}
ASTNode* Annotations::findAnnotationValue(std::initializer_list<std::wstring> path) const
{
    return findAnnotationValueImpl(*this, path);
}

void Annotations::deleteAnnotation(const std::wstring& tag)
{
    auto new_end = boost::remove_if(
        annotation_list,
        [&tag](const Annotation* annotation)
        {
            BOOST_ASSERT(annotation && "null pointer exception");

            return annotation->name->toString() == tag;
        }
    );

    annotation_list.erase(new_end, annotation_list.end());
}

bool Annotations::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (annotation_list)
    );
}

bool Annotations::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (annotation_list)
    );
}

Annotations* Annotations::clone() const
{
    Annotations* cloned = new Annotations();

    for(Annotation* annotation: annotation_list)
    {
        cloned->appendAnnotation(annotation ? annotation->clone() : nullptr);
    }

    return cloned;
}

std::wostream& Annotations::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    if (!annotation_list.empty())
    {
        output << L"// annotations" << std::endl << out_indent(indent);

        for (Annotation* annotation : annotation_list)
        {
            BOOST_ASSERT(annotation && "null pointer exception");

            output << out_source(*annotation, indent) << std::endl << out_indent(indent);
        }
    }

    return output;
}

bool Annotations::merge(Annotations& rhs)
{
    for(Annotation* rhs_annotation: rhs.annotation_list)
    {
        bool merged = false;

        for(Annotation* annotation: annotation_list)
        {
            if(rhs_annotation->name->isEqual(*annotation->name))
            {
                if(!rhs_annotation->merge(*annotation))
                {
                    return false;
                }

                merged = true;

                break;
            }
        }

        if(!merged)
        {
            appendAnnotation(rhs_annotation);
        }
    }

    return true;
}

Annotatable::Annotatable()
    : BaseNode()
    , annotations(nullptr)
{
}

Annotatable::Annotatable(ASTNode& parent)
    : BaseNode()
    , annotations(nullptr)
{
}

Annotations* Annotatable::getAnnotations() const noexcept
{
    return annotations;
}

void Annotatable::setAnnotations(Annotations* anns) noexcept
{
    if (annotations != nullptr) annotations->parent = nullptr;
    if (anns        != nullptr) anns       ->parent = this;

    annotations = anns;
}

bool Annotatable::hasAnnotation(const std::wstring& tag) const
{
    return findAnnotation(tag) != nullptr;
}

Annotation* Annotatable::findAnnotation(const std::wstring& tag) const
{
    if (annotations == nullptr)
        return nullptr;

    return annotations->findAnnotation(tag);
}

Annotation* Annotatable::findAnnotation(const std::vector<std::wstring>& path) const
{
    if (annotations == nullptr)
        return nullptr;

    return annotations->findAnnotation(path);
}

Annotation* Annotatable::findAnnotation(std::initializer_list<std::wstring> path) const
{
    if (annotations == nullptr)
        return nullptr;

    return annotations->findAnnotation(path);
}

ASTNode* Annotatable::findAnnotationValue(const std::vector<std::wstring>& path) const
{
    if (annotations == nullptr)
        return nullptr;

    return annotations->findAnnotationValue(path);
}

ASTNode* Annotatable::findAnnotationValue(std::initializer_list<std::wstring> path) const
{
    if (annotations == nullptr)
        return nullptr;

    return annotations->findAnnotationValue(path);
}

void Annotatable::addAnnotation(Annotation* anno)
{
    BOOST_ASSERT(anno != nullptr && "null pointer exception");

    if (annotations == nullptr)
    {
        annotations         = new Annotations;
        annotations->parent = this;

        if (stage::SourceInfoContext::get(anno) != nullptr)
            ASTNodeHelper::propogateSourceInfo(*annotations, *anno);
    }

    annotations->appendAnnotation(anno);
}

void Annotatable::delAnnotation(const std::wstring& tag)
{
    if (annotations == nullptr)
        return;

    annotations->deleteAnnotation(tag);
}

bool Annotatable::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (annotations)
    );
}

bool Annotatable::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (annotations)
    );
}

std::wostream& Annotatable::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    if (annotations != nullptr)
        output << out_source(*annotations, indent);

    return output;
}

} } }
