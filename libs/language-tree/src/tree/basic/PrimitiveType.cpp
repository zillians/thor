/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/logic/tribool.hpp>
#include "language/tree/basic/Type.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/module/Internal.h"
#include "language/context/ParserContext.h"

namespace zillians { namespace language { namespace tree {

PrimitiveType::PrimitiveType()
{
    primitive_kind_.setKindValue(PrimitiveKind::VOID_TYPE);
}

PrimitiveType::PrimitiveType(const PrimitiveKind k)
{
    primitive_kind_.setKindValue(k);
}

bool PrimitiveType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (primitive_kind_)
    );
}

bool PrimitiveType::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (primitive_kind_)
    );
}

std::wostream& PrimitiveType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    return output << getKind().toString();
}

PrimitiveType* PrimitiveType::clone() const
{
    UNIMPLEMENTED_CODE();
    return nullptr;
}

bool PrimitiveType::isSame(const Type& rhs) const
{
    if(!rhs.isPrimitiveType())
        return false;

    const PrimitiveType* rhs_pt = cast<PrimitiveType>(&rhs);
    return getKind() == rhs_pt->getKind();
}

bool PrimitiveType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    BOOST_ASSERT(isa<PrimitiveType>(&rhs));
    const PrimitiveType* pt_rhs = cast<PrimitiveType>(&rhs);
    return getKind() < pt_rhs->getKind();
}

PrimitiveType* PrimitiveType::getCanonicalType() const
{
    BOOST_ASSERT(isa<Internal>(parent) && "primitive type should under internal node only");

    return const_cast<PrimitiveType*>(this);
}

PrimitiveType* PrimitiveType::getArithmeticCompatibleType() const
{
    if (!primitive_kind_.isArithmeticCapable())
        return nullptr;

    return const_cast<PrimitiveType*>(this);
}

std::wstring PrimitiveType::toString() const
{
    return primitive_kind_.toString();
}

size_t PrimitiveType::getTypeClassSerialNumber() const
{
    return ASTNodeType::PrimitiveType;
}

Type::ConversionRank PrimitiveType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    PrimitiveKind target_kind = PrimitiveKind::VOID_TYPE;

    if (const PrimitiveType* target_primitive_type = cast<PrimitiveType>(&target))
    {
        target_kind = target_primitive_type->getKind();
    }
    else if (const EnumType* target_enum_type = cast<EnumType>(&target))
    {
        if(policy == Type::ConversionPolicy::Explicitly) {
            const EnumDecl* target_decl = target_enum_type->getAsEnumDecl();

            BOOST_ASSERT(target_decl && "null pointer exception");

            target_kind = target_decl->getUnderlyingType();

            BOOST_ASSERT((target_kind == PrimitiveKind::INT32_TYPE || target_kind == PrimitiveKind::INT64_TYPE) && "unexpected underlying type");

            Type::ConversionRank r = getKind().getConversionRank(target_kind);

            return r;
        }
    }

    return getKind().getConversionRank(target_kind);
}

} } }
