
/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <functional>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/phoenix/bind.hpp>
#include <boost/phoenix/core.hpp>
#include <boost/phoenix/operator/self.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/equal.hpp>
#include <boost/range/algorithm/lexicographical_compare.hpp>

#include "language/context/ParserContext.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/TypedefDecl.h"
#include "language/tree/ASTNodeHelper.h"

namespace zillians { namespace language { namespace tree {

namespace {

// NOTE:
//
// we do not always has ParserContext, for example, thor-make has no
// ParserContext() since it don't parse. However, resolution and
// getCanonicalType() need internal. There are two ways to get Tangle.
//
// (1) getParserContext().tangle
// (2) node.getOwner<Tangle>()
//
// (1) is fast, while (2) can be used without ParserContext.  So, we try
// getParserContext() first, if not exists, then we follow the owner.
//
// There is a very special situation: hasParserContext() is true,
//                                    while tangle is nullptr.
//
// it happened while deserializing the Tangle from the file, the ParserContext
// had been newed, but the deserialization is not finished, so the tangle(and
// internal) is not set.  And, when insert type to type_set by calling
// Type::create(), get_internal() will called, too. if this happen, return
// null.
Internal* get_internal(const ASTNode* ref)
{
    Internal* result = nullptr;
    if(hasParserContext() && getParserContext().tangle != nullptr)
    {
        return getParserContext().tangle->internal;
    }
    else
    {
        if(ref->getOwner<Tangle>() == nullptr)
            return nullptr;
        else
            return ref->getOwner<Tangle>()->internal;
    }
}

}

//////////////////////////////////////////////////////////////////////////////
// Type
//////////////////////////////////////////////////////////////////////////////

Type::Type() : resolved_type(nullptr)
{
}

bool Type::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
    );
}

bool Type::replaceUseWith(const ASTNode& from,
                          const ASTNode& to,
                          bool update_parent)
{
    AST_NODE_REPLACE(
        result,
    );
}

FunctionType* Type::getBy(FunctionDecl& func_decl)
{
    auto*const internal = get_internal(&func_decl);

    return internal->addFunctionType(func_decl);
}

PrimitiveType* Type::getBy(PrimitiveKind kind, const ASTNode& parent_ref)
{
    auto*const internal = get_internal(&parent_ref);

    return internal->getPrimitiveType(kind);
}

#define THOR_SCRIPT_DEF_TYPE_ACCESSOR(r, data, elem)                                                        \
                     bool  Type::BOOST_PP_CAT(   is, elem) () const { return  isa<      elem>(this); }      \
                     elem* Type::BOOST_PP_CAT(getAs, elem) ()       { return cast<      elem>(this); }      \
               const elem* Type::BOOST_PP_CAT(getAs, elem) () const { return cast<const elem>(this); }      \
    template<>       elem* Type::getAs<elem>               ()       { return BOOST_PP_CAT(getAs, elem)(); } \
    template<> const elem* Type::getAs<elem>               () const { return BOOST_PP_CAT(getAs, elem)(); }

BOOST_PP_SEQ_FOR_EACH(THOR_SCRIPT_DEF_TYPE_ACCESSOR, _, THOR_SCRIPT_TYPE_SEQ)

#undef THOR_SCRIPT_DEF_TYPE_ACCESSOR

bool Type::isVoidType         () const { return isPrimitiveType() && getAsPrimitiveType()->isVoidType         (); }
bool Type::isBoolType         () const { return isPrimitiveType() && getAsPrimitiveType()->isBoolType         (); }
bool Type::isIntegerType      () const { return isPrimitiveType() && getAsPrimitiveType()->isIntegerType      (); }
bool Type::isFloatType        () const { return isPrimitiveType() && getAsPrimitiveType()->isFloatType        (); }
bool Type::isArithmeticCapable() const { return isPrimitiveType() && getAsPrimitiveType()->isArithmeticCapable(); }

Declaration* Type::getAsDecl() const
{
    if(isRecordType  ()) return getAsClassDecl();
    if(isEnumType    ()) return getAsEnumDecl();
    if(isTypenameType()) return getAsTypenameDecl();
    if(isTypedefType ()) return getAsTypedefDecl();
    return nullptr;
}

ClassDecl* Type::getAsClassDecl() const
{
    if(!isRecordType())
        return nullptr;

    const RecordType* record_type = cast<const RecordType>(this);
    BOOST_ASSERT(isa<ClassDecl>(record_type->getDecl()));
    return cast<ClassDecl>(record_type->getDecl());
}

EnumDecl* Type::getAsEnumDecl() const
{
    if(!isEnumType())
        return nullptr;

    const EnumType* enum_type = cast<const EnumType>(this);
    BOOST_ASSERT(isa<EnumDecl>(enum_type->getDecl()));
    return cast<EnumDecl>(enum_type->getDecl());
}

TypenameDecl* Type::getAsTypenameDecl() const
{
    if(!isTypenameType())
        return nullptr;

    const TypenameType* typename_type = cast<const TypenameType>(this);
    BOOST_ASSERT(isa<TypenameDecl>(typename_type->getDecl()));
    return cast<TypenameDecl>(typename_type->getDecl());
}

TypedefDecl* Type::getAsTypedefDecl() const
{
    if(!isTypedefType())
        return nullptr;

    const TypedefType* typedef_type = cast<const TypedefType>(this);
    BOOST_ASSERT(isa<TypedefDecl>(typedef_type->getDecl()));
    return cast<TypedefDecl>(typedef_type->getDecl());
}

const unique_name_t& Type::getUniqueName() const noexcept
{
    return unique_name;
}

bool Type::isCanonical() const
{
    const Type* canonical_type = getCanonicalType();

    return this == canonical_type;
}

PrimitiveType* Type::getArithmeticCompatibleType() const
{
    return nullptr;
}

bool Type::TypeLess::operator()(Type* l, Type* r)
{
    return l->isLessThan(*r);
}

//////////////////////////////////////////////////////////////////////////////
// PointerType
//////////////////////////////////////////////////////////////////////////////

PointerType::PointerType() : pointee_type(nullptr)
{
}

PointerType::PointerType(Type* pointeeType) : pointee_type(pointeeType)
{
}

bool PointerType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (pointee_type)
    );
}

bool PointerType::replaceUseWith(const ASTNode& from,
                                 const ASTNode& to,
                                 bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (pointee_type)
    );
}

std::wostream& PointerType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(pointee_type && "null pointer exception");

    return output << out_source(*pointee_type, indent) << L'*';
}

PointerType* PointerType::clone() const
{
    // Only one PointerType of one pointee type is allowed
    // if you need multiple PointerType of the same pointee type
    // please check your design
    UNREACHABLE_CODE();
    return nullptr;
}

bool PointerType::isSame(const Type& rhs) const
{
    if(!rhs.isPointerType())
        return false;

    const PointerType* rhs_pt = cast<PointerType>(&rhs);
    return pointee_type->isSame(*rhs_pt->getPointeeType());
}

bool PointerType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;
    BOOST_ASSERT(isa<PointerType>(&rhs));

    const PointerType* pt_rhs = cast<PointerType>(&rhs);
    return getPointeeType()->isLessThan(*pt_rhs->getPointeeType());
}

std::wstring PointerType::toString() const
{
    return getPointeeType()->toString() + L"*";
}

Type* PointerType::getPointeeType() const
{
    return pointee_type;
}

void PointerType::setPointeeType(Type* pointeeType)
{
    pointee_type = pointeeType;
}

PointerType* PointerType::getCanonicalType() const
{
    UNIMPLEMENTED_CODE();
    return nullptr;
}

size_t PointerType::getTypeClassSerialNumber() const
{
    return ASTNodeType::PointerType;
}

Type::ConversionRank PointerType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    UNIMPLEMENTED_CODE();
    return ConversionRank::NotMatch;
}

//////////////////////////////////////////////////////////////////////////////
// ReferenceType
//////////////////////////////////////////////////////////////////////////////

ReferenceType::ReferenceType() : pointee_type(nullptr)
{
}

ReferenceType::ReferenceType(Type* pointeeType) : pointee_type(pointeeType)
{
}

bool ReferenceType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (pointee_type)
    );
}

bool ReferenceType::replaceUseWith(const ASTNode& from,
                                   const ASTNode& to,
                                   bool update_parent)
{
    AST_NODE_REPLACE(
        result,
        (pointee_type)
    );
}

std::wostream& ReferenceType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(pointee_type && "null pointer exception");

    return output << out_source(*pointee_type, indent) << L'&';
}

ReferenceType* ReferenceType::clone() const
{
    // Only one ReferenceType of one pointee type is allowed
    // if you need multiple ReferenceType of the same pointee type
    // please check your design
    UNREACHABLE_CODE();
    return nullptr;
}

bool ReferenceType::isSame(const Type& rhs) const
{
    if(!rhs.isPointerType())
        return false;

    const ReferenceType* rhs_pt = cast<ReferenceType>(&rhs);
    return pointee_type->isSame(*rhs_pt->getPointeeType());
}

bool ReferenceType::isLessThan(const Type& rhs) const
{
    if(getTypeClassSerialNumber() < rhs.getTypeClassSerialNumber())
        return true;
    if(rhs.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    BOOST_ASSERT(isa<ReferenceType>(&rhs));
    const ReferenceType* pt_rhs = cast<ReferenceType>(&rhs);

    return getPointeeType()->isLessThan(*pt_rhs->getPointeeType());
}

std::wstring ReferenceType::toString() const
{
    return getPointeeType()->toString() + L"&";
}

Type* ReferenceType::getPointeeType() const
{
    return pointee_type;
}

void ReferenceType::setPointeeType(Type* pointeeType)
{
    pointee_type = pointeeType;
}

ReferenceType* ReferenceType::getCanonicalType() const
{
    UNIMPLEMENTED_CODE();
    return nullptr;
}

size_t ReferenceType::getTypeClassSerialNumber() const
{
    return ASTNodeType::ReferenceType;
}

Type::ConversionRank ReferenceType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    UNIMPLEMENTED_CODE();
    return ConversionRank::NotMatch;
}

bool MultiType::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (types)
    );
}

bool MultiType::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (types)
    );
}

std::wostream& MultiType::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(types.size() > 1 && "not multiple type");

          auto i    = types.cbegin();
    const auto iend = types.cend();

    output << out_source(**i, indent);

    while (++i != iend)
        output << L',' << out_source(**i, indent);

    return output;
}

MultiType* MultiType::clone() const
{
    // There is exactly one MultiType each canonical type sequence
    // If you need multiple MultiType
    // PLEASE check your design
    UNREACHABLE_CODE();
    return nullptr;
}

bool MultiType::isSame(const Type& rhs) const
{
    if (!rhs.isMultiType())
        return false;

    const MultiType* rhs_pt = cast<MultiType>(&rhs);

    BOOST_ASSERT(rhs_pt && "null pointer exception");

    if (types.size() != rhs_pt->types.size())
        return false;

    return boost::equal(
        types,
        rhs_pt->types,
        [](const Type* lhs, const Type* rhs)
        {
            BOOST_ASSERT(lhs && "null pointer exception");
            BOOST_ASSERT(rhs && "null pointer exception");

            return lhs->isSame(*rhs);
        }
    );
}

bool MultiType::isLessThan(const Type& rhs_type) const
{
    namespace phx = boost::phoenix;

    if(getTypeClassSerialNumber() < rhs_type.getTypeClassSerialNumber())
        return true;
    if(rhs_type.getTypeClassSerialNumber() < getTypeClassSerialNumber())
        return false;

    const MultiType* rhs = cast<MultiType>(&rhs_type);

    BOOST_ASSERT(rhs && "null pointer exception");

    return boost::range::lexicographical_compare(
             types,
        rhs->types,
        phx::bind(
            &Type::isLessThan,
             phx::placeholders::arg1,
            *phx::placeholders::arg2
        )
    );
}

std::wstring MultiType::toString() const
{
    using boost::adaptors::transformed;

    return    L"("
            + boost::join(
                  types | transformed(std::mem_fn(&Type::toString)),
                  L","
              )
            + L")";
}

std::size_t MultiType::getTypeClassSerialNumber() const
{
    return ASTNodeType::MultiType;
}

Type::ConversionRank MultiType::getConversionRank(const Type& target, const Type::ConversionPolicy policy) const
{
    if (this == &target)
        return ConversionRank::ExactMatch;
    else
        return ConversionRank::NotMatch;
}

MultiType* MultiType::getCanonicalType() const
{
    return const_cast<MultiType*>(this);
}

MultiType::MultiType(const std::vector<Type*>& types)
    : types(types)
{
    BOOST_ASSERT(
        std::all_of(
            types.begin(),
            types.end(),
            std::mem_fn(&Type::isCanonical)
        ) && "all types in sequence must be canonical"
    );
}

MultiType::MultiType(std::vector<Type*>&& types)
    : types(std::move(types))
{
    BOOST_ASSERT(
        std::all_of(
            this->types.begin(),
            this->types.end(),
            std::mem_fn(&Type::isCanonical)
        ) && "all types in sequence must be canonical"
    );
}

} } }
