/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <limits>
#include <string>

#include <boost/assert.hpp>

#include "language/tree/basic/PrimitiveKind.h"

namespace zillians { namespace language { namespace tree {

PrimitiveKind::PrimitiveKind()
    : PrimitiveKind(VOID_TYPE)
{
}

PrimitiveKind::PrimitiveKind(const kind k)
    : kind_(k)
{
}

auto PrimitiveKind::getKindValue() const -> kind
{
    return kind_;
}

void PrimitiveKind::setKindValue(const kind v)
{
    kind_ = v;
}

std::wstring PrimitiveKind::toString() const
{
    switch(getKindValue())
    {
    case VOID_TYPE             : return L"void"   ;
    case BOOL_TYPE             : return L"bool"   ;
    case INT8_TYPE             : return L"int8"   ;
    case INT16_TYPE            : return L"int16"  ;
    case INT32_TYPE            : return L"int32"  ;
    case INT64_TYPE            : return L"int64"  ;
    case FLOAT32_TYPE          : return L"float32";
    case FLOAT64_TYPE          : return L"float64";
    default: UNREACHABLE_CODE(); return L"unknown";
    }
}

PrimitiveKind::operator kind() const
{
    return kind_;
}

bool PrimitiveKind::isVoidType() const noexcept
{
    return (getKindValue() == VOID_TYPE);
}

bool PrimitiveKind::isBoolType() const noexcept
{
    return (getKindValue() == BOOL_TYPE);
}

bool PrimitiveKind::isIntegerType() const noexcept
{
    return (getKindValue() >= INT8_TYPE && getKindValue() <= INT64_TYPE);
}

bool PrimitiveKind::isArithmeticCapable() const noexcept
{
    return (getKindValue() >= BOOL_TYPE && getKindValue() <= FLOAT64_TYPE);
}

bool PrimitiveKind::isFloatType() const noexcept
{
    return (getKindValue() == FLOAT32_TYPE || getKindValue() == FLOAT64_TYPE);
}

Type::ConversionRank PrimitiveKind::getConversionRank(PrimitiveKind target, const Type::ConversionPolicy policy) const
{
    if (kind_ == target.kind_)
        return Type::ConversionRank::ExactMatch;

    if (target.kind_ == INT32_TYPE && isIntegerType() && kind_ < INT32_TYPE)
        return Type::ConversionRank::Promotion;

    if (isArithmeticCapable() && target.isArithmeticCapable())
        return Type::ConversionRank::StandardConversion;

    return Type::ConversionRank::NotMatch;
}

auto PrimitiveKind::promote(const PrimitiveKind t1) const noexcept -> kind
{
    BOOST_ASSERT(   isArithmeticCapable());
    BOOST_ASSERT(t1.isArithmeticCapable());

    const auto lhs_kind_value =    getKindValue();
    const auto rhs_kind_value = t1.getKindValue();

    return std::max({INT32_TYPE, lhs_kind_value, rhs_kind_value});
}

int PrimitiveKind::byteSize() const
{
    switch(getKindValue())
    {
    case VOID_TYPE             : return  0;
    case BOOL_TYPE             : return  1;
    case INT8_TYPE             : return  1;
    case INT16_TYPE            : return  2;
    case INT32_TYPE            : return  4;
    case FLOAT32_TYPE          : return  4;
    case INT64_TYPE            : return  8;
    case FLOAT64_TYPE          : return  8;
    default: break;
    }
    return -1;
}

int PrimitiveKind::bitSize() const
{
    switch(getKindValue())
    {
    case VOID_TYPE             : return  0;
    case BOOL_TYPE             : return  8;
    case INT8_TYPE             : return  8;
    case INT16_TYPE            : return 16;
    case INT32_TYPE            : return 32;
    case FLOAT32_TYPE          : return 32;
    case INT64_TYPE            : return 64;
    case FLOAT64_TYPE          : return 64;
    default: break;
    }
    return -1;
}

bool PrimitiveKind::isImplicitConvertible(const PrimitiveKind to, bool& precision_loss) const
{
    if(this->isArithmeticCapable() && to.isArithmeticCapable())
    {
        if((kind_ ==   INT32_TYPE && to.kind_ == FLOAT32_TYPE) ||
           (kind_ == FLOAT32_TYPE && to.kind_ ==   INT32_TYPE) ||
           (kind_ ==   INT64_TYPE && to.kind_ == FLOAT64_TYPE) ||
           (kind_ == FLOAT64_TYPE && to.kind_ ==   INT64_TYPE))
        {
            precision_loss = true;
        }
        else
        {
            precision_loss = this->byteSize() > to.byteSize();
        }
        return true;
    }

    return false;
}

int64 PrimitiveKind::maxIntegerValue(const PrimitiveKind k)
{
    BOOST_ASSERT(
        (
            k.isBoolType() ||
            k.isIntegerType()
        ) && "unexpected integer type!"
    );

    switch (k)
    {
    case BOOL_TYPE : return std::numeric_limits<bool >::max();
    case INT8_TYPE : return std::numeric_limits<int8 >::max();
    case INT16_TYPE: return std::numeric_limits<int16>::max();
    case INT32_TYPE: return std::numeric_limits<int32>::max();
    case INT64_TYPE: return std::numeric_limits<int64>::max();
    default        : return 0;
    }
}

int64 PrimitiveKind::minIntegerValue(const PrimitiveKind k)
{
    BOOST_ASSERT(
        (
            k.isBoolType() ||
            k.isIntegerType()
        ) && "unexpected integer type!"
    );

    switch (k)
    {
    case BOOL_TYPE : return std::numeric_limits<bool >::min();
    case INT8_TYPE : return std::numeric_limits<int8 >::min();
    case INT16_TYPE: return std::numeric_limits<int16>::min();
    case INT32_TYPE: return std::numeric_limits<int32>::min();
    case INT64_TYPE: return std::numeric_limits<int64>::min();
    default        : return 0;
    }
}

} } }
