/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/expression/CastExpr.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

const char *CastExpr::CastMethod::toString(const method m)
{
    switch(m)
    {
    case STATIC : return "STATIC" ;
    case DYNAMIC: return "DYNAMIC";
    default     : return "UNKNOWN";
    }
}

CastExpr::CastExpr() {}

CastExpr::CastExpr(Expression* node, TypeSpecifier* type, CastMethod::method method/* = CastMethod::UNKNOWN*/) : node(node), type(type), method(method)
{
    BOOST_ASSERT(node && "null node for cast expression is not allowed");
    BOOST_ASSERT(type && "null type for cast expression is not allowed");

    node->parent = this;
    type->parent = this;
}

bool CastExpr::hasValue() const
{
    return true;
}

bool CastExpr::isRValue() const
{
    return node->isRValue();
}

bool CastExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (node  )
        (type  )
        (method)
    );
}

bool CastExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (node)
        (type)
    );
}

CastExpr* CastExpr::clone() const
{
    return new CastExpr(
            (node) ? node->clone() : NULL,
            (type) ? type->clone() : NULL,
            method);
}

std::wostream& CastExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    switch (method)
    {
    case CastMethod::STATIC : output << L"static_cast" ; break;
    case CastMethod::DYNAMIC: output << L"dynamic_cast"; break;
    default                 : output << L"unknown_cast"; break;
    }

    BOOST_ASSERT(node && "null pointer exception");
    BOOST_ASSERT(type && "null pointer exception");

    output << L'<' << out_source(*type, indent) << L">(" << out_source(*node, indent) << L')';

    return output;
}

} } }
