/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/expression/ArrayExpr.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

ArrayExpr::ArrayExpr() : array_type(nullptr), element_type(nullptr), dimension(0)
{
}

ArrayExpr::ArrayExpr(const ArrayExpr& other)
: array_type(clone_or_null(other.array_type))
  , element_type(clone_or_null(other.element_type))
  , dimension(other.dimension)
{
    if(array_type != nullptr)   array_type->parent = this;
    if(element_type != nullptr) element_type->parent = this;

    for(auto* elem : other.elements)
    {
        BOOST_ASSERT(elem != nullptr && "element experssions should never be null");
        appendElement(elem->clone());
    }
}

void ArrayExpr::setArrayType(NamedSpecifier* type)
{
    BOOST_ASSERT(type != nullptr && "nullptr argument is not allowed");

    array_type = type;
    type->parent = this;
}

void ArrayExpr::setElementType(TypeSpecifier* type)
{
    BOOST_ASSERT(type != nullptr && "nullptr argument is not allowed");

    element_type = type;
    type->parent = this;
}

TypeSpecifier* ArrayExpr::getElementType()
{
    return element_type;
}

void ArrayExpr::appendElement(Expression* element)
{
    BOOST_ASSERT(element && "null parameter for vector expression is not allowed");

    element->parent = this;
    elements.push_back(element);
}

void ArrayExpr::setDimension(size_t d)
{
    dimension = d;
}

size_t ArrayExpr::getDimension()
{
    return dimension;
}

bool ArrayExpr::hasValue() const
{
    return true;
}

bool ArrayExpr::isRValue() const
{
    return true;
}

bool ArrayExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (array_type  )
        (element_type)
        (elements    )
    );
}

bool ArrayExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (array_type  )
        (element_type)
        (elements    )
    );
}

ArrayExpr* ArrayExpr::clone() const
{
    return new ArrayExpr(*this);
}

std::wostream& ArrayExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << L'<' << dimension << L", ";
    if(element_type == nullptr)
        output << L"null";
    else
        output << out_source(*element_type, indent);
    output << L">[";

    if (!elements.empty())
    {
              auto i    = elements.cbegin();
        const auto iend = elements.cend();

        output << out_source(**i, indent);

        while (++i != iend)
            output << L", " << out_source(**i, indent);
    }

    output << L']';

    return output;
}

} } }
