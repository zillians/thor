/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <functional>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/range/algorithm/for_each.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/TieExpr.h"

namespace zillians { namespace language { namespace tree {

TieExpr::TieExpr(const std::vector<Expression*>& exprs)
    : tied_expressions()
{
    tieExpressions(exprs);
}

TieExpr::TieExpr(std::vector<Expression*>&& exprs)
    : tied_expressions(std::move(exprs))
{
    for (Expression* tied_expression: tied_expressions)
    {
        BOOST_ASSERT(tied_expression && "null pointer exception");

        tied_expression->parent = this;
    }
}

bool TieExpr::isLValue() const
{
    return std::all_of(tied_expressions.begin(), tied_expressions.end(), std::bind(&Expression::isLValue, std::placeholders::_1));
}

bool TieExpr::hasValue() const
{
    return true;
}

bool TieExpr::isRValue() const
{
    return std::all_of(tied_expressions.begin(), tied_expressions.end(), std::bind(&Expression::isRValue, std::placeholders::_1));
}

void TieExpr::tieExpression(Expression* expr)
{
    BOOST_ASSERT(expr && "null pointer exception");

    tied_expressions.emplace_back(expr);
    expr->parent = this;
}

bool TieExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (tied_expressions)
    );
}

bool TieExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (tied_expressions)
    );
}

std::wostream& TieExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(tied_expressions.size() > 1 && "TieExpr must contain more than 1 expression");

          auto i    = tied_expressions.cbegin();
    const auto iend = tied_expressions.cend();

    output << L'(' << out_source(**i, indent);

    while (++i != iend)
        output << L", " << out_source(**i, indent);

    output << L')';

    return output;
}

TieExpr* TieExpr::clone() const
{
    std::vector<Expression*> new_tied_expressions = tied_expressions;

    boost::for_each(
        new_tied_expressions,
        [](Expression*& new_tied_expression)
        {
            BOOST_ASSERT(new_tied_expression && "null pointer exception");

            new_tied_expression = new_tied_expression->clone();
        }
    );

    return new TieExpr(std::move(new_tied_expressions));
}

} } }
