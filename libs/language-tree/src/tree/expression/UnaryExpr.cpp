/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/UnaryExpr.h"

namespace zillians { namespace language { namespace tree {

const wchar_t* UnaryExpr::OpCode::toString(type t)
{
    switch(t)
    {
    case INCREMENT: return L"inc";
    case DECREMENT: return L"dec";
    case BINARY_NOT: return L"~";
    case LOGICAL_NOT: return L"!";
    case ARITHMETIC_NEGATE: return L"-";
    case NEW: return L"new";
    case NOOP: return L"<no-op>";
    case INVALID: return L"<invalid>";
    default: UNREACHABLE_CODE(); return L"<non_op>";
    }
}

UnaryExpr::UnaryExpr() { }

UnaryExpr::UnaryExpr(OpCode::type opcode, Expression* node) : opcode(opcode), node(node)
{
    BOOST_ASSERT(node && "null node for unary expression is not allowed");
    node->parent = this;
}

#define UNARY_EXPR_OP_MAPPING    \
    ((INCREMENT        )('++' )) \
    ((DECREMENT        )('--' )) \
    (( BINARY_NOT      )('~'  )) \
    ((LOGICAL_NOT      )('!'  )) \
    ((ARITHMETIC_NEGATE)('-'  )) \
    ((NEW              )('new')) \

#define UNARY_EXPR_OP_MAPPING_DEF(r, _, map)                           \
    template<>                                                         \
    UnaryExpr* UnaryExpr::create<BOOST_PP_SEQ_ELEM(1, map)>(           \
        Expression* sub_expr)                                          \
    {                                                                  \
        return new UnaryExpr(                                          \
            OpCode::BOOST_PP_SEQ_ELEM(0, map),                         \
            sub_expr                                                   \
        );                                                             \
    }

BOOST_PP_SEQ_FOR_EACH(UNARY_EXPR_OP_MAPPING_DEF, _, UNARY_EXPR_OP_MAPPING)

#undef UNARY_EXPR_OP_MAPPING_DEF
#undef UNARY_EXPR_OP_MAPPING

bool UnaryExpr::isNew() const noexcept
{
    return opcode == OpCode::NEW;
}

bool UnaryExpr::isLogical() const noexcept
{
    return opcode == OpCode::LOGICAL_NOT;
}

bool UnaryExpr::isBinary() const noexcept
{
    return opcode == OpCode::BINARY_NOT;
}

bool UnaryExpr::isArithmetic() const noexcept
{
    return opcode == OpCode::ARITHMETIC_NEGATE;
}

bool UnaryExpr::hasValue() const
{
    switch(opcode)
    {
    case OpCode::NOOP   :
    case OpCode::INVALID:
        return false;
    default:
        return true;
    }
}

bool UnaryExpr::isRValue() const
{
    switch(opcode)
    {
    case OpCode::INCREMENT:
    case OpCode::DECREMENT:
        return false;
    case OpCode::BINARY_NOT:
    case OpCode::LOGICAL_NOT:
    case OpCode::ARITHMETIC_NEGATE:
    case OpCode::NEW:
    case OpCode::NOOP:
    case OpCode::INVALID:
        return true;
    default: UNREACHABLE_CODE(); return false;
    }
}

bool UnaryExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (opcode)
        (node  )
    );
}

bool UnaryExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (node)
    );
}

UnaryExpr* UnaryExpr::clone() const
{
    return new UnaryExpr(opcode, clone_or_null(node));
}

std::wostream& UnaryExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    const bool is_keyword = opcode == OpCode::NEW;

    output << OpCode::toString(opcode);

    if (is_keyword)
        output << L' ';

    output << L'(' << out_source(*node, indent) << L')';

    return output;
}

} } }
