/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/expression/IdExpr.h"

namespace zillians { namespace language { namespace tree {

IdExpr::IdExpr()
    : id(nullptr)
{
}

IdExpr::IdExpr(Identifier* id)
    : id(id)
{
    BOOST_ASSERT(id != nullptr && "null pointer exception");

    id->parent = this;
}

Identifier* IdExpr::getId() const noexcept
{
    return id;
}

bool IdExpr::hasValue() const
{
    // IdExpr::hasValue and MemberExpr::hasValue should be the same logic

    // For example:
    // @code
    // // assume there is a class named foo in package pkg
    // function test()
    // {
    //     pkg         ; // canonical_type   == nullptr   => false
    //     pkg.foo     ; // canonical_symbol is ClassDecl => false
    //     pkg.foo.vi32; // all pass                      => true
    // }
    // @endcode

    const auto*const canonical_type   = getCanonicalType();

    if (canonical_type == nullptr)
        return false;

    const auto*const canonical_symbol = ASTNodeHelper::getCanonicalSymbol(this);

    if (canonical_symbol != nullptr && isa<ClassDecl, TypenameDecl, TypedefDecl, EnumDecl>(canonical_symbol))
        return false;

    return true;
}

bool IdExpr::isRValue() const
{
    return false;
}

bool IdExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (id)
    );
}

bool IdExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (id)
    );
}

IdExpr* IdExpr::clone() const
{
    BOOST_ASSERT(id != nullptr && "null pointer exception");

    return new IdExpr(id->clone());
}

std::wostream& IdExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    return output << out_source(*id, indent);
}

} } }
