/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/logic/tribool.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/IdExpr.h"
#include "language/tree/expression/MemberExpr.h"

namespace zillians { namespace language { namespace tree {

namespace {

template<ObjectLiteral::LiteralType::type expected_type>
bool isSpecificLiteralType(const Expression& expr)
{
    const auto*const literal = cast<ObjectLiteral>(&expr);

    if (literal == nullptr)
        return false;

    return literal->type == expected_type;
}

NumericLiteral* getConstantNumericLiteralImpl(const Expression& expr)
{
    if (const auto*const num_literal = cast<const NumericLiteral>(&expr))
        return const_cast<NumericLiteral*>(num_literal);

    return nullptr;
}

VariableDecl* getConstantEnumeratorImpl(const Expression& expr)
{
    if (VariableDecl* var_decl = cast_or_null<VariableDecl>(ASTNodeHelper::getCanonicalSymbol(&expr)))
    {
        if (var_decl->isEnumerator())
        {
            return var_decl;
        }
    }

    return nullptr;
}

template <typename T>
Expression* createImpl(const T& names)
{
    BOOST_ASSERT(names.size() != 0 && "no identifier to create expression");

    Expression* final_expression = new IdExpr(new SimpleIdentifier(*names.begin()));

    for (auto i = names.begin(); ++i != names.end(); )
    {
        const std::wstring& current_name = *i;

        final_expression =
            new MemberExpr(
                final_expression,
                new SimpleIdentifier(current_name)
            )
        ;
    }

    return final_expression;
}

}

Expression::Expression()
    : resolved_type(nullptr)
{
}

bool Expression::isNullLiteral() const
{
    return isSpecificLiteralType<ObjectLiteral::LiteralType::NULL_OBJECT>(*this);
}

bool Expression::isThisLiteral() const
{
    return isSpecificLiteralType<ObjectLiteral::LiteralType::THIS_OBJECT>(*this);
}

bool Expression::isConstantLiteral() const
{
    return  getConstantNumericLiteral() != nullptr ||
            getConstantEnumerator    () != nullptr;
}

NumericLiteral* Expression::getConstantNumericLiteral() const
{
    return getConstantNumericLiteralImpl(*this);
}

VariableDecl* Expression::getConstantEnumerator() const
{
    return getConstantEnumeratorImpl(*this);
}

bool Expression::isLValue() const
{
    return !isRValue();
}

boost::tribool Expression::isMultiValue() const
{
    const Type* type = getCanonicalType();

    if (type == nullptr)
        return boost::indeterminate;
    else
        return type->isMultiType();
}

Type* Expression::getCanonicalType() const
{
    ASTNode* resolved_symbol = ResolvedSymbol::get(this);
    Type*    resolved_type   = ResolvedType  ::get(this);

    if (resolved_symbol == nullptr && resolved_type == nullptr)
        return nullptr;

    if (resolved_symbol != nullptr)
        return ASTNodeHelper::getCanonicalType(resolved_symbol);

    if (Declaration* declaration = resolved_type->getAsDecl())
        return declaration->getCanonicalType();

    if (PrimitiveType* prim_type = resolved_type->getAsPrimitiveType())
        return prim_type;

    if (MultiType* multi_type = resolved_type->getAsMultiType())
        return multi_type->getCanonicalType();

    UNREACHABLE_CODE();
    return nullptr;
}

Expression* Expression::create(std::initializer_list<std::wstring> names)
{
    return createImpl(names);
}

Expression* Expression::create(const std::vector<std::wstring>& names)
{
    return createImpl(names);
}

} } }
