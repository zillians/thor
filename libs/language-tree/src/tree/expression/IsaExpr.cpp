/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/tree/ASTNode.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/expression/IsaExpr.h"
#include "language/tree/expression/Expression.h"

namespace zillians { namespace language { namespace tree {

IsaExpr::IsaExpr() : node(NULL), type(NULL) {}

IsaExpr::IsaExpr(Expression* node, TypeSpecifier* type) : node(node), type(type)
{
    BOOST_ASSERT(node && "null node for isa expression is not allowed");
    BOOST_ASSERT(type && "null type for isa expression is not allowed");

    node->parent = this;
    type->parent = this;
}

bool IsaExpr::hasValue() const
{
    return true;
}

bool IsaExpr::isRValue() const
{
    return node->isRValue();
}

bool IsaExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (node)
        (type)
    );
}

bool IsaExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (node)
        (type)
    );
}

IsaExpr* IsaExpr::clone() const
{
    return new IsaExpr(
            (node) ? node->clone() : NULL,
            (type) ? type->clone() : NULL);
}

std::wostream& IsaExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    BOOST_ASSERT(type && "null pointer exception");
    BOOST_ASSERT(node && "null pointer exception");

    output << L"isa<" << out_source(*type, indent) << L">(" << out_source(*node, indent) << L')';

    return output;
}

} } }
