/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/assert.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/expression/TernaryExpr.h"

namespace zillians { namespace language { namespace tree {

TernaryExpr::TernaryExpr() { }

TernaryExpr::TernaryExpr(Expression* cond, Expression* true_node, Expression* false_node) : cond(cond), true_node(true_node), false_node(false_node)
{
    BOOST_ASSERT(cond && "null condition for ternary expression is not allowed");
    BOOST_ASSERT(true_node && "null \"true node\" for ternary expression is not allowed");
    BOOST_ASSERT(false_node && "null \"false node\" for ternary expression is not allowed");

    cond->parent = this;
    true_node->parent = this;
    false_node->parent = this;
}

bool TernaryExpr::hasValue() const
{
    return true;
}

bool TernaryExpr::isRValue() const
{
    // it's R-value if either "true block" or "false block" is R-value
    return (true_node->isRValue() || false_node->isRValue());
}

bool TernaryExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (cond      )
        (true_node )
        (false_node)
    );
}

bool TernaryExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (cond      )
        (true_node )
        (false_node)
    );
}

TernaryExpr* TernaryExpr::clone() const
{
    return new TernaryExpr(
        clone_or_null(cond),
        clone_or_null(true_node),
        clone_or_null(false_node)
    );
}

std::wostream& TernaryExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output
        << L'(' << out_source(*cond, indent) << L") ? ("
        << out_source(*true_node, indent) << L") : ("
        << out_source(*false_node, indent) << L')';

    return output;
}

} } }
