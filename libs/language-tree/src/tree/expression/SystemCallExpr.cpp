/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <ostream>

#include <boost/range/adaptor/transformed.hpp>

#include "utility/Foreach.h"

#include "language/tree/ASTNode.h"
#include "language/tree/expression/SystemCallExpr.h"

namespace zillians { namespace language { namespace tree {

namespace {

const wchar_t* getCallTypeName(SystemCallExpr::CallType call_type) noexcept
{
    switch (call_type)
    {
    case SystemCallExpr::CallType::ASYNC : return L"async";
    case SystemCallExpr::CallType::REMOTE: return L"remote";
    default                              : return L"unknown";
    }
}

}

SystemCallExpr* SystemCallExpr::createAsync () { return new SystemCallExpr(CallType::ASYNC ); }
SystemCallExpr* SystemCallExpr::createRemote() { return new SystemCallExpr(CallType::REMOTE); }

SystemCallExpr::SystemCallExpr()
    : call_type(CallType::UNKNOWN)
    , parameters()
{
}

SystemCallExpr::SystemCallExpr(CallType call_type)
    : call_type(call_type)
    , parameters()
{
}

void SystemCallExpr::appendParameter(Expression* parameter)
{
    parameters.emplace_back(parameter);

    if (parameter != nullptr)
        parameter->parent = this;
}

void SystemCallExpr::prependParameter(Expression* parameter)
{
    parameters.insert(parameters.begin(), parameter);

    if (parameter != nullptr)
        parameter->parent = this;
}

bool SystemCallExpr::hasValue() const
{
    return true;
}

bool SystemCallExpr::isRValue() const
{
    return false;
}

bool SystemCallExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (call_type )
        (parameters)
    );
}

bool SystemCallExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (call_type )
        (parameters)
    );
}

SystemCallExpr* SystemCallExpr::clone() const
{
    using boost::adaptors::transformed;

    const auto& cloned_parameters = parameters | transformed(&clone_or_null<Expression>);

    return new SystemCallExpr(call_type, cloned_parameters);
}

std::wostream& SystemCallExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << getCallTypeName(call_type) << L'(';

    for_each_and_pitch(
        parameters,
        [this, &output, indent](Expression* parameter)
        {
            if (parameter == nullptr)
                output << L"<null>";
            else
                output << out_source(*parameter, indent);
        },
        [this, &output]
        {
            output << L", ";
        }
    );

    output << L')';

    return output;
}

} } }
