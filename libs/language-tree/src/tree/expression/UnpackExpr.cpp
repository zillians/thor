/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>
#include <vector>

#include <boost/assert.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include "language/tree/ASTNode.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/expression/CallExpr.h"
#include "language/tree/expression/UnpackExpr.h"

namespace zillians { namespace language { namespace tree {

UnpackExpr::UnpackExpr()
    : call(nullptr)
    , unpack_list()
{
}

UnpackExpr::UnpackExpr(CallExpr* call)
    : UnpackExpr(call, {})
{
}

UnpackExpr::UnpackExpr(CallExpr* call, std::vector<SimpleIdentifier*>&& unpack_list)
    : call(call)
    , unpack_list(std::move(unpack_list))
{
    BOOST_ASSERT(call && "null pointer exception");

    call->parent = this;

    for (SimpleIdentifier* unpack_name : this->unpack_list)
        unpack_name->parent = this;
}

void UnpackExpr::appendName(SimpleIdentifier* unpack_name)
{
    BOOST_ASSERT(unpack_name && "null pointer exception");

    unpack_list.emplace_back(unpack_name);

    unpack_name->parent = this;
}

bool UnpackExpr::hasValue() const { return false; }
bool UnpackExpr::isLValue() const { return false; }
bool UnpackExpr::isRValue() const { return false; }

bool UnpackExpr::isEqualImpl(const ASTNode& rhs) const
{
    AST_NODE_IS_EQUAL(
        (call       )
        (unpack_list)
    );
}

bool UnpackExpr::replaceUseWith(const ASTNode& from, const ASTNode& to, bool update_parent/* = true*/)
{
    AST_NODE_REPLACE(
        result,
        (call       )
        (unpack_list)
    );
}

std::wostream& UnpackExpr::toSource(std::wostream& output, unsigned indent/* = 0*/) const
{
    output << L"<unpack>(" << out_source(*call, indent);

    for (SimpleIdentifier* unpack_name : unpack_list)
    {
        BOOST_ASSERT(unpack_name && "null pointer exception");

        output << L", " << out_source(*unpack_name, indent);
    }

    return output;
}

UnpackExpr* UnpackExpr::clone() const
{
    BOOST_ASSERT(call && "null pointer exception");

    std::vector<SimpleIdentifier*> new_unpack_list;

    boost::push_back(new_unpack_list, unpack_list | boost::adaptors::transformed(std::mem_fn(&SimpleIdentifier::clone)));

    return new UnpackExpr(call->clone(), std::move(new_unpack_list));
}

} } }
