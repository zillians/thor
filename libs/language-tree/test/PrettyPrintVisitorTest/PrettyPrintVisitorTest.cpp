/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */


#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeFactory.h"
#include "language/tree/visitor/PrettyPrintVisitor.h"
#include "../ASTNodeSamples.h"

#include <iostream>
#include <string>
#include <limits>

#define BOOST_TEST_MODULE ThorTreeTest_PrettyPrintVisitorTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace zillians::language::tree;
using namespace zillians::language::tree::visitor;

BOOST_AUTO_TEST_SUITE( ThorTreeTest_PrettyPrintVisitorTestSuite )

BOOST_AUTO_TEST_CASE( ThorTreeTest_PrettyPrintVisitorTestCase1 )
{
    PrettyPrintVisitor printer;

    ASTNode* program = createSample1();
    printer.visit(*program);
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_PrettyPrintVisitorTestCase2 )
{
    PrettyPrintVisitor printer;

    ASTNode* program = createSample2();
    printer.visit(*program);
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_PrettyPrintVisitorTestCase3 )
{
    PrettyPrintVisitor printer;

    ASTNode* program = createSample3();
    printer.visit(*program);
}

BOOST_AUTO_TEST_CASE( ThorTreeTest_PrettyPrintVisitorTestCase4 )
{
    PrettyPrintVisitor printer;

    ASTNode* program = createSample4();
    printer.visit(*program);
}

BOOST_AUTO_TEST_SUITE_END()
