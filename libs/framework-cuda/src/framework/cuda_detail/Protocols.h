/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_CUDA_DETAIL_PROTOCOLS_H_
#define ZILLIANS_FRAMEWORK_CUDA_DETAIL_PROTOCOLS_H_

#include <cstddef>
#include <cstdint>

#include <functional>

namespace zillians { namespace framework { namespace cuda_detail {

struct invocation_t
{
    std::int64_t function_id;
    std::int64_t  session_id;

    std::function<void()>* done_notifier;

    std::int8_t parameters[];
};

namespace {

constexpr std::size_t SIZE_INVOCATION           = 128;
constexpr std::size_t SIZE_INVOCATION_HEADER    = sizeof(invocation_t);
constexpr std::size_t SIZE_INVOCATION_PARAMETER = SIZE_INVOCATION - SIZE_INVOCATION_HEADER;

}

} } }

#endif /* ZILLIANS_FRAMEWORK_CUDA_DETAIL_PROTOCOLS_H_ */
