/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>
#include <cstdint>

#include <iterator>
#include <ostream>
#include <stdexcept>
#include <string>
#include <utility>
#include <vector>

#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/seq/pop_front.hpp>
#include <boost/preprocessor/seq/seq.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/range/as_literal.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/regex.hpp>

#include <log4cxx/logger.h>

#include "language/Architecture.h"
#include "language/SystemFunction.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ResolverContext.h"
#include "language/tree/basic/PrimitiveKind.h"
#include "language/tree/basic/PrimitiveType.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/FunctionDecl.h"

#include "framework/detail/GlobalDispatcherGenerator.h"
#include "framework/detail/GlobalOffsetsComputor.h"

#include "framework/cuda_detail/GlobalDispatcherGenerator.h"
#include "framework/cuda_detail/Protocols.h"

namespace zillians { namespace framework { namespace cuda_detail {

global_dispatcher_generator::global_dispatcher_generator(
    const log4cxx::LoggerPtr&       new_logger        ,
    language::Architecture          new_arch          ,
    const detail::global_offsets_t& new_global_offsets,
    std::ostream&                   new_output
)
    : detail::global_dispatcher_generator(new_logger, new_arch, new_global_offsets)
    , global_offset_setters()
    , global_inits()
    , output(&new_output)
{
}

bool global_dispatcher_generator::load_skeleton()
{
    return true;
}

bool global_dispatcher_generator::set_global_offset(const std::string& name, std::int64_t offset)
{
    global_offset_setters.emplace_back(name, std::move(offset));

    return true;
}

bool global_dispatcher_generator::add_global_init(const FunctionDecl& func_decl)
{
    global_inits.emplace_back(&func_decl);

    return true;
}

bool global_dispatcher_generator::add_exported_function(boost::iterator_range<detail::func_mapping_t::right_const_iterator> exported_func_with_ids)
{
    using boost::adaptors::map_values;

    assert(!exported_func_with_ids.empty() && "there must be at least one function to be exported");

    const auto*const func_decl = exported_func_with_ids.front().first;
    const auto&      ids       = exported_func_with_ids | map_values;

    exported_functions.emplace_back(func_decl, boost::copy_range<std::vector<std::int64_t>>(ids));

    return true;
}

bool global_dispatcher_generator::finish()
{
#define GD_PLACEHOLDERS                  \
    (threads_per_block     )             \
    (invocation_size       )             \
    (invocation_header_size)             \
    (member_offset_func_id )             \
    (forward_declarations  )             \
    (system_init_id        )             \
    (global_offset_setters )             \
    (global_inits          )             \
    (exported_functions    )

// placeholder pattern: "/* $placeholder_name$ */"
#define GD_PLACEHOLDER_PATTERN_IMPL(elem)                 R"PATTERN(/\*[ ]\$)PATTERN" BOOST_PP_STRINGIZE(elem) R"PATTERN(\$[ ]\*/)PATTERN"
#define GD_PLACEHOLDER_PATTERN_ON_FIRST(elem)                 GD_PLACEHOLDER_PATTERN_IMPL(elem)
#define GD_PLACEHOLDER_PATTERN_ON_REMAINS(r, data, elem)  "|" GD_PLACEHOLDER_PATTERN_IMPL(elem)

#define GD_PLACEHOLDER_MATCH_RESULTS(r, data, elem)                         \
    if (BOOST_PP_SEQ_HEAD(data) == "/* $" BOOST_PP_STRINGIZE(elem) "$ */")  \
        return BOOST_PP_CAT(on_, elem)(std::move(BOOST_PP_SEQ_TAIL(data)));

    static const char SKELETON_CODE[] = {
#include "framework/cuda_detail/Kernel.gd.cu.inc"
    };

#ifdef CUDA_GLOBAL_DISPATCHER_DEBUG_OUTPUT
    std::string                                   debug_output   ;
    auto                                          debug_inserter = boost::iostreams::back_inserter(debug_output);
    auto                                          debug_tee      = boost::iostreams::tee(*output, debug_inserter);
    boost::iostreams::stream<decltype(debug_tee)> output_proxy(debug_tee);
#else
    auto& output_proxy = *output;
#endif

    try
    {
        boost::regex_replace(
            std::ostream_iterator<char>(output_proxy),
            boost::const_begin(SKELETON_CODE),
            boost::const_end(SKELETON_CODE),
            boost::regex(
                "("
                GD_PLACEHOLDER_PATTERN_ON_FIRST(BOOST_PP_SEQ_HEAD(GD_PLACEHOLDERS))
                BOOST_PP_SEQ_FOR_EACH(
                    GD_PLACEHOLDER_PATTERN_ON_REMAINS,
                    ,
                    BOOST_PP_SEQ_POP_FRONT(GD_PLACEHOLDERS)
                )
                ")"
            ),
            [this](const boost::match_results<const char*>& results, std::ostream_iterator<char> out)
            {
                const auto& matched_string = results[0];

                BOOST_PP_SEQ_FOR_EACH(
                    GD_PLACEHOLDER_MATCH_RESULTS,
                    (matched_string)(out),
                    GD_PLACEHOLDERS
                );

                assert(false && "unexpected placeholder!");

                return std::move(out);
            }
        );

        output_proxy.flush();

#ifdef CUDA_GLOBAL_DISPATCHER_DEBUG_OUTPUT
        LOG4CXX_DEBUG(logger, "generated source for cuda global dispatcher: " << debug_output);
#endif

        if (!output_proxy.good())
            return false;
    }
    catch (const std::runtime_error& e)
    {
        LOG4CXX_ERROR(logger, "fail to finalize global dispatcher generation: " << e.what());

        return false;
    }

#undef GD_PLACEHOLDER_MATCH_RESULTS

#undef GD_PLACEHOLDER_PATTERN_ON_REMAINS
#undef GD_PLACEHOLDER_PATTERN_ON_FIRST
#undef GD_PLACEHOLDER_PATTERN_IMPL

#undef GD_PLACEHOLDERS

    return true;
}

std::ostream_iterator<char> global_dispatcher_generator::on_threads_per_block(std::ostream_iterator<char> out)
{
    return boost::copy(boost::as_literal("32u"), std::move(out));
}

std::ostream_iterator<char> global_dispatcher_generator::on_invocation_size(std::ostream_iterator<char> out)
{
    return boost::copy(std::to_string(SIZE_INVOCATION) + 'u', std::move(out));
}

std::ostream_iterator<char> global_dispatcher_generator::on_invocation_header_size(std::ostream_iterator<char> out)
{
    return boost::copy(std::to_string(SIZE_INVOCATION_HEADER) + 'u', std::move(out));
}

std::ostream_iterator<char> global_dispatcher_generator::on_member_offset_func_id(std::ostream_iterator<char> out)
{
    const auto& function_id_offset = offsetof(invocation_t, function_id);

    return boost::copy(std::to_string(function_id_offset) + 'u', std::move(out));
}

std::ostream_iterator<char> global_dispatcher_generator::on_forward_declarations(std::ostream_iterator<char> out)
{
    out = boost::copy(boost::as_literal("// global offset setters\n"), std::move(out));
    for (const auto& name : global_offset_setters | boost::adaptors::map_keys)
    {
        out = boost::copy(boost::as_literal("extern \"C\" __device__ int64_t "), std::move(out));
        out = boost::copy(name                                                 , std::move(out));
        out = boost::copy(boost::as_literal(";\n")                             , std::move(out));
    }

    out = boost::copy(boost::as_literal("\n// global inits\n"), std::move(out));
    for (const auto*const func_decl : global_inits)
        out = declare_function(std::move(out), *func_decl);

    out = boost::copy(boost::as_literal("\n// exported functions\n"), std::move(out));
    for (const auto*const func_decl : exported_functions | boost::adaptors::map_keys)
        out = declare_function(std::move(out), *func_decl);

    return std::move(out);
}

std::ostream_iterator<char> global_dispatcher_generator::on_system_init_id(std::ostream_iterator<char> out)
{
    return boost::copy(std::to_string(language::system_function::system_initialization_id), std::move(out));
}

std::ostream_iterator<char> global_dispatcher_generator::on_global_offset_setters(std::ostream_iterator<char> out)
{
    for (const auto& entry : global_offset_setters)
    {
        const auto& name  = std::get<0>(entry);
        const auto& value = std::get<1>(entry);

        out = boost::copy(name                            , std::move(out));
        out = boost::copy(boost::as_literal(" = ")        , std::move(out));
        out = boost::copy(std::to_string(value)           , std::move(out));
        out = boost::copy(boost::as_literal(";\n        "), std::move(out));
    }

    return std::move(out);
}

std::ostream_iterator<char> global_dispatcher_generator::on_global_inits(std::ostream_iterator<char> out)
{
    out = boost::copy(boost::as_literal("\n    case ")                                   , std::move(out));
    out = boost::copy(std::to_string(language::system_function::global_initialization_id), std::move(out));
    out = boost::copy(boost::as_literal(":\n")                                           , std::move(out));

    for (const auto*const func_decl : global_inits)
    {
        const auto*const mangle_context = language::NameManglingContext::get(func_decl);
        assert(mangle_context != nullptr && "null pointer exception");

        const auto& mangled_name = mangle_context->mangled_name;

        out = boost::copy(boost::as_literal("        "), std::move(out));
        out = boost::copy(mangled_name                 , std::move(out));
        out = boost::copy(boost::as_literal("();\n")   , std::move(out));
    }

    out = boost::copy(boost::as_literal("        break;"), std::move(out));

    return std::move(out);
}

std::ostream_iterator<char> global_dispatcher_generator::on_exported_functions(std::ostream_iterator<char> out)
{
    for (const auto& entry : exported_functions)
    {
        const auto*const func_decl = std::get<0>(entry);
        const auto&      ids       = std::get<1>(entry);

        assert(func_decl != nullptr && "null pointer exception");
        assert(!ids.empty() && "no any id for exported function!?");

        for (const auto& id : ids)
        {
            out = boost::copy(boost::as_literal("\n    case "), std::move(out));
            out = boost::copy(std::to_string(id)              , std::move(out));
            out = boost::copy(boost::as_literal(":\n")        , std::move(out));
        }

        out = call_function(std::move(out), *func_decl);
        out = boost::copy(boost::as_literal("        break;"), std::move(out));
    }

    return std::move(out);
}

std::ostream_iterator<char> global_dispatcher_generator::declare_function(std::ostream_iterator<char> out, const FunctionDecl& func_decl)
{
    assert(func_decl.type != nullptr && "null pointer exception");

    const auto& ret_type_name = get_type_name(*func_decl.type);

    // TODO support functions contain parameters
    if (!func_decl.parameters.empty())
        throw std::logic_error("cuda global dispatcher does not support functions with parameters yet");

    // TODO support member functions
    if (func_decl.is_member)
        throw std::logic_error("cuda global dispatcher does not support member functions yet");

    const auto*const mangle_context = language::NameManglingContext::get(&func_decl);
    assert(mangle_context != nullptr && "null pointer exception");

    const auto& mangled_name = mangle_context->mangled_name;

    out = boost::copy(boost::as_literal("extern \"C\" __device__ "), std::move(out));
    out = boost::copy(ret_type_name                                , std::move(out));
    out = boost::copy(boost::as_literal(" ")                       , std::move(out));
    out = boost::copy(mangled_name                                 , std::move(out));
    out = boost::copy(boost::as_literal("();\n")                   , std::move(out));

    return std::move(out);
}

std::ostream_iterator<char> global_dispatcher_generator::call_function(std::ostream_iterator<char> out, const FunctionDecl& func_decl)
{
    // TODO support functions contain parameters
    if (!func_decl.parameters.empty())
        throw std::logic_error("cuda global dispatcher does not support functions with parameters yet");

    // TODO support member functions
    if (func_decl.is_member)
        throw std::logic_error("cuda global dispatcher does not support member functions yet");

    const auto*const mangle_context = language::NameManglingContext::get(&func_decl);
    assert(mangle_context != nullptr && "null pointer exception");

    const auto& mangled_name = mangle_context->mangled_name;

    out = boost::copy(boost::as_literal("        "), std::move(out));
    out = boost::copy(mangled_name                 , std::move(out));
    out = boost::copy(boost::as_literal("();\n")   , std::move(out));

    return std::move(out);
}

const std::string& global_dispatcher_generator::get_type_name(const language::tree::TypeSpecifier& ts)
{
    const auto*const resolved_type = language::ResolvedType::get(&ts);
    assert(resolved_type != nullptr && "resolved type is not ready!?");

    return get_type_name(*resolved_type);
}

const std::string& global_dispatcher_generator::get_type_name(const language::tree::Type& type)
{
    const auto*const canonical_type = type.getCanonicalType();
    assert(canonical_type != nullptr && "canonical type is not ready!?");

    if (const auto*const primitive_type = canonical_type->getAsPrimitiveType())
        return get_type_name(*primitive_type);

    // TODO support more types
    throw std::logic_error("not yet supported type in cuda global dispatcher generator");
}

const std::string& global_dispatcher_generator::get_type_name(const language::tree::PrimitiveType& primitive_type)
{
    using language::tree::PrimitiveKind;

    switch (primitive_type.getKind())
    {
    case PrimitiveKind::VOID_TYPE   : return type_cache.emplace(&primitive_type, "void"   ).first->second; break;
    case PrimitiveKind::INT8_TYPE   : return type_cache.emplace(&primitive_type, "int8_t" ).first->second; break;
    case PrimitiveKind::INT16_TYPE  : return type_cache.emplace(&primitive_type, "int16_t").first->second; break;
    case PrimitiveKind::INT32_TYPE  : return type_cache.emplace(&primitive_type, "int32_t").first->second; break;
    case PrimitiveKind::INT64_TYPE  : return type_cache.emplace(&primitive_type, "int64_t").first->second; break;
    case PrimitiveKind::FLOAT32_TYPE: return type_cache.emplace(&primitive_type, "float"  ).first->second; break;
    case PrimitiveKind::FLOAT64_TYPE: return type_cache.emplace(&primitive_type, "double" ).first->second; break;
    }

    throw std::logic_error("unexpected primitive type in cuda global dispatcher generator");
}

} } }
