/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>

#include <functional>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <boost/algorithm/string/erase.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/join.hpp>
#include <boost/system/error_code.hpp>

#include <log4cxx/level.h>
#include <log4cxx/logger.h>

#include "utility/Functional.h"
#include "utility/Process.h"

#include "language/Architecture.h"
#include "language/context/ManglingStageContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/module/Tangle.h"

#include "language/stage/serialization/detail/ASTSerializationHelper.h"

#include "framework/detail/GlobalOffsetsComputor.h"

#include "framework/cuda_detail/GlobalDispatcherGenerator.h"
#include "framework/cuda_detail/Kernel.h"

namespace zillians { namespace framework { namespace cuda_detail {

kernel::kernel(const path_t& ast_path, const std::string& arch_name, bool enable_server, bool enable_client)
    : verbose(false)
    , tangle(nullptr)
    , global_offsets()
    ,   func_mapping()
    ,   type_mapping()
    , string_mapping()
    , arch(language::Architecture(arch_name))
    , gd_root()
    , gd_dll(nullptr)
    , gd(nullptr)
    , logger(log4cxx::Logger::getLogger("framework.cuda.kernel"))
{
    if (arch.is_zero())
        throw std::runtime_error("no architecture for cuda");

    if (arch.is_any(~language::Architecture::cuda()))
        throw std::runtime_error("unexpected architecture for cuda");

    const auto& ast_path_str  = ast_path.string();
    auto*const  loaded_node   = language::stage::ASTSerializationHelper::deserialize(ast_path_str);
    auto*const  loaded_tangle = language::tree::cast_or_null<language::tree::Tangle>(loaded_node);

    if (loaded_tangle == nullptr)
        throw std::runtime_error("cannot load AST file: " + ast_path_str);

    tangle = loaded_tangle;

    std::tie(
        global_offsets,
          func_mapping,
          type_mapping,
        string_mapping
    ) = detail::compute_global_offsets(*tangle, arch, enable_server, enable_client);

    set_verbose(false);
}

bool kernel::is_initialized() const noexcept
{
    return gd != nullptr;
}

bool kernel::initialize(const path_t& ast_path, const path_t& dll_path, const std::vector<path_t>& dep_pathes)
{
    if (is_initialized())
        return false;

          auto  new_gd_root        = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
    const auto& path_gd_src        = new_gd_root / "gd.cu";
    const auto& path_gd_obj        = new_gd_root / "gd.o";
    const auto& path_gd_linked_obj = new_gd_root / "gd_linked.o";
    const auto& path_gd_so         = new_gd_root / "libgd.so";

    boost::filesystem::create_directories(new_gd_root);

    const std::string& compiler_cuda = "nvcc";
    const auto&        path_libs     = boost::range::join(std::make_pair(&dll_path, &dll_path + 1), dep_pathes);
    const auto&        flags_cuda    = " --cudart=shared -Xcompiler -fPIC -arch=" + arch.toString(":");

    // TODO add extra compilation flags by runtime config, for example, "-g" or "-O2"

    // step #0: ar x {lib}
    // step #1: nvcc --cudart=shared -arch=?? -Xcompiler -fPIC -dc -o gd.o /tmp/gd.cu
    // step #2: nvcc --cudart=shared -arch=?? -Xcompiler -fPIC -dlink -o gd_linked.o gd.o {lib}.o
    // step #3: nvcc --cudart=shared -arch=?? -Xcompiler -fPIC -shared -o libgd_cuda.so gd.o gd_linked.o {lib}.o

    std::string cmd_obj_extract = "cd " + new_gd_root.native();
    for (const auto& path_lib : path_libs)
        cmd_obj_extract += " && ar x \"" + path_lib.native() + "\"";

    std::string cmd_gd_dcompile = compiler_cuda
                                + flags_cuda
                                + " -o \"" + path_gd_obj.native() + "\""
                                + " -dc"
                                + " \"" + path_gd_src.native() + "\""
                                ;

    std::string cmd_gd_dlink    = compiler_cuda
                                + flags_cuda
                                + " -o \"" + path_gd_linked_obj.native() + "\""
                                + " -dlink"
                                + " " + new_gd_root.native() + "/*.o"
                                ;

    std::string cmd_gd_link     = compiler_cuda
                                + flags_cuda
                                + " -shared -o \"" + path_gd_so.native() + "\""
                                + " " + new_gd_root.native() + "/*.o"
                                ;

    bool        is_success             = false;
    const auto& remove_temp_on_failure = invoke_on_destroy(
        [&is_success, &new_gd_root]
        {
            if (is_success)
                return;

            boost::system::error_code ec;
            boost::filesystem::remove_all(new_gd_root, ec);
        }
    );

    {
        boost::iostreams::stream<boost::iostreams::file_descriptor_sink> stream_gd(path_gd_src);
        global_dispatcher_generator                                      gd_generator(logger, arch, global_offsets, stream_gd);

        if (!gd_generator.generate(*tangle, func_mapping))
        {
            LOG4CXX_ERROR(logger, L"fail to generate cuda global dispatcher");
            return false;
        }
    }

    if (zillians::system(cmd_obj_extract) != 0)
    {
        LOG4CXX_ERROR(logger, L"fail to extract native cuda object files");
        return false;
    }

    if (zillians::system(cmd_gd_dcompile) != 0)
    {
        LOG4CXX_ERROR(logger, L"fail to compile cuda global dispatcher to object file");
        return false;
    }

    if (zillians::system(cmd_gd_dlink) != 0)
    {
        LOG4CXX_ERROR(logger, L"fail to device link cuda global dispatcher");
        return false;
    }

    if (zillians::system(cmd_gd_link) != 0)
    {
        LOG4CXX_ERROR(logger, L"fail to generate cuda global dispatcher .so");
        return false;
    }

    dll_ptr new_gd_dll(path_gd_so.native());
    if (new_gd_dll == nullptr)
    {
        LOG4CXX_ERROR(logger, L"fail to load cuda global dispatcher");
        return false;
    }

    auto* new_gd = new_gd_dll.find_symbol<global_dispatcher_t>("_Z17global_dispatcherjPKaj");
    if (new_gd == nullptr)
    {
        LOG4CXX_ERROR(logger, L"fail to locate cuda global dispatcher");
        return false;
    }

    gd_root = std::move(new_gd_root);
    gd_dll  = std::move(new_gd_dll );
    gd      = std::move(new_gd     );

    is_success = true;

    return true;
}

bool kernel::finalize() noexcept
{
    if (!is_initialized())
        return false;

    gd     = nullptr;
    gd_dll = nullptr;

    boost::system::error_code ec;
    boost::filesystem::remove_all(gd_root, ec);

    gd_root.clear();

    return true;
}

bool kernel::is_verbose() const noexcept
{
    return verbose;
}

void kernel::set_verbose(bool new_verbose/* = true*/) noexcept
{
    verbose = new_verbose;

    logger->setLevel(verbose ? log4cxx::Level::getInfo() : log4cxx::Level::getError());
}

auto kernel::get_global_dispatcher() const noexcept -> global_dispatcher_t*
{
    return gd;
}

std::int64_t kernel::get_function_id(const std::string& name) const
{
    for (const auto& entry : func_mapping.right)
    {
              auto*const func_decl         = entry.first;
        const auto*const name_mangling_ctx = language::NameManglingContext::get(func_decl);

        if (name_mangling_ctx != nullptr)
            if (name_mangling_ctx->mangled_name == name)
                return entry.second;
    }

    return -1;
}

} } }
