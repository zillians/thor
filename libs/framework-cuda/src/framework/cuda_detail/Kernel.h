/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_CUDA_DETAIL_KERNEL_H_
#define ZILLIANS_FRAMEWORK_CUDA_DETAIL_KERNEL_H_

#include <cstdint>

#include <string>
#include <utility>
#include <vector>

#include <boost/filesystem/path.hpp>
#include <boost/noncopyable.hpp>

#include <log4cxx/logger.h>

#include "utility/DllPtr.h"

#include "language/Architecture.h"

#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace language { namespace tree {

struct Tangle;

} } }

namespace zillians { namespace framework { namespace cuda_detail {

class kernel : boost::noncopyable
{
private:
    using path_t              = boost::filesystem::path;
    using global_dispatcher_t = std::pair<bool, int>(unsigned, const std::int8_t*, unsigned);

public:
    kernel(const path_t& ast_path, const std::string& arch_name, bool enable_server, bool enable_client);

    bool is_initialized() const noexcept;
    bool initialize(const path_t& ast_path, const path_t& dll_path, const std::vector<path_t>& dep_pathes);
    bool finalize() noexcept;

    bool is_verbose() const noexcept;
    void set_verbose(bool new_verbose = true) noexcept;

    global_dispatcher_t* get_global_dispatcher() const noexcept;

    std::int64_t get_function_id(const std::string& name) const;

private:
    bool                          verbose;
    const language::tree::Tangle* tangle;
    detail::global_offsets_t      global_offsets;
    detail::  func_mapping_t        func_mapping;
    detail::  type_mapping_t        type_mapping;
    detail::string_mapping_t      string_mapping;

    language::Architecture arch;
    path_t                 gd_root;
    dll_ptr                gd_dll;
    global_dispatcher_t*   gd;

    log4cxx::LoggerPtr logger;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_CUDA_DETAIL_KERNEL_H_ */
