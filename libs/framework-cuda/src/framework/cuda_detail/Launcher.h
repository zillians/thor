/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_CUDA_DETAIL_LAUNCHER_H_
#define ZILLIANS_FRAMEWORK_CUDA_DETAIL_LAUNCHER_H_

#include <cstddef>
#include <cstdint>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <mutex>
#include <string>
#include <utility>

#include <boost/noncopyable.hpp>

#include <thrust/host_vector.h>

#include <log4cxx/logger.h>

#include "framework/cuda_detail/Protocols.h"

namespace zillians { namespace framework {

class executor_cuda;

namespace cuda_detail {

class kernel;

class launcher : boost::noncopyable
{
public:
    launcher(executor_cuda& new_executor, kernel& new_kern, int new_device);

    bool is_verbose() const noexcept;
    void set_verbose(bool new_verbose = true) noexcept;

    bool launch_batch();

public:
    bool add_invocation_from_external(const std::string& func_name);
    bool add_invocation_from_external(std::int64_t       func_id  );

    bool run_invocation_from_external(const std::string& func_name);
    bool run_invocation_from_external(std::int64_t       func_id  );

    std::size_t push_invocation_from_external(invocation_t&& invocation);

private:
    thrust::host_vector<invocation_t> prepare_invocations();

private:
    bool           verbose;
    executor_cuda* executor;
    kernel*        kern;

    int device;

    std::mutex                        invocations_from_external_guard;
    thrust::host_vector<invocation_t> invocations_from_external;

    log4cxx::LoggerPtr logger;
};

}

} }

#endif /* ZILLIANS_FRAMEWORK_CUDA_DETAIL_LAUNCHER_H_ */
