/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>

#include <string>
#include <vector>

#include <boost/swap.hpp>

#include <log4cxx/consoleappender.h>
#include <log4cxx/patternlayout.h>

#include "utility/MemoryUtil.h"

#include "language/ThorConfiguration.h"

#include "framework/Executor.h"
#include "framework/ExecutorCUDA.h"
#include "framework/cuda_detail/Kernel.h"
#include "framework/cuda_detail/Launcher.h"

namespace zillians { namespace framework {

executor_cuda::executor_cuda(int new_id)
    : executor_rt(new_id, "framework.cuda.executor")
    , device(0)
    , arch("sm_30")
    , exit_code()
    , kern(nullptr)
{
    // sample logged message with the following pattern layout:
    // ERROR [logger.name] [namespace::class_name::function_name] MESSAGE
    log4cxx::PatternLayoutPtr pattern_layout        = new log4cxx::PatternLayout("%-5p [%c] [%C::%M] %m%n");
    auto                      framework_cuda_logger = logger->getParent();

    assert(framework_cuda_logger != nullptr && "null pointer exception");

    framework_cuda_logger->removeAllAppenders();
    framework_cuda_logger->addAppender(new log4cxx::ConsoleAppender(pattern_layout));
}

executor_cuda::~executor_cuda() = default; // generate dtor here since header have only forward declaration (for kerner/launcher)

bool executor_cuda::initialize()
{
    auto new_kern = make_unique<cuda_detail::kernel>(
        ast_path                                                 ,
        arch                                                     ,
        export_type == EXPORT_ALL || export_type == EXPORT_SERVER, /* enable server */
        export_type == EXPORT_ALL || export_type == EXPORT_CLIENT  /* enable client */
    );

    if (verbose)
        new_kern->set_verbose(true);

    if (!new_kern->initialize(ast_path, dll_path, dep_pathes))
    {
        LOG4CXX_ERROR(logger, "fail to initialize x86 kernel");

        return false;
    }

    auto new_launcher = make_unique<cuda_detail::launcher>(*this, *new_kern, device);

    if (verbose)
        new_launcher->set_verbose(true);

    boost::swap(kern    , new_kern    );
    boost::swap(launcher, new_launcher);

    return true;
}

bool executor_cuda::finalize()
{
    assert(
        (
            (kern == nullptr && launcher == nullptr) ||
            (kern != nullptr && launcher != nullptr)
        ) && "corrupted state!"
    );

    if (kern == nullptr)
        return false;

    launcher = nullptr;

    if (!kern->finalize())
        LOG4CXX_ERROR(logger, "fail to finalize x86 kernel");

    kern = nullptr;

    return true;
}

const char* executor_cuda::get_binary_file_suffix() const
{
    static const std::string suffixed_by_cuda = std::string("_cuda") + language::THOR_EXTENSION_LIB;

    return suffixed_by_cuda.c_str();
}

void executor_cuda::set_arguments(const std::vector<std::wstring>& new_args)
{
    // TODO implement it
}

bool executor_cuda::call(std::int64_t func_id)
{
    assert(launcher != nullptr && "null pointer exception");

    return launcher->add_invocation_from_external(func_id);
}

bool executor_cuda::call(const std::string& func_name)
{
    assert(launcher != nullptr && "null pointer exception");

    return launcher->add_invocation_from_external(func_name);
}

bool executor_cuda::call(std::int64_t session_id, invocation_request&& request)
{
    assert(false && "not implemented yet! implement this function to accept invocations from remote");
    return false;
}

bool executor_cuda::call_and_wait(std::int64_t func_id)
{
    assert(launcher != nullptr && "null pointer exception");

    return launcher->run_invocation_from_external(func_id);
}

bool executor_cuda::call_and_wait(const std::string& func_name)
{
    assert(launcher != nullptr && "null pointer exception");

    return launcher->run_invocation_from_external(func_name);
}

int executor_cuda::get_exit_code()
{
    auto future = exit_code.get_future();

    return future.get();
}

void executor_cuda::set_exit_code(std::int32_t new_exit_code)
{
    exit_code.set_value(new_exit_code);
}

void executor_cuda::do_work()
{
    assert(launcher != nullptr && "not yet initialized!?");

    launcher->launch_batch();
}

} }
