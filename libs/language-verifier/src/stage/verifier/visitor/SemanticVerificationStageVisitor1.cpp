/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <stack>
#include <functional>
#include <unordered_map>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/any_of.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include "language/context/TransformerContext.h"
#include "language/logging/StringTable.h"
#include "language/logging/LoggerWrapper.h"
#include "language/stage/transformer/detail/AsyncHelper.h"
#include "language/stage/verifier/context/SemanticVerificationContext.h"
#include "language/stage/verifier/context/SemanticHackContext.h"
#include "language/stage/verifier/detail/ControlFlowVerifier.h"
#include "language/stage/verifier/visitor/NameVerificationVisitor.h"
#include "language/stage/verifier/visitor/SemanticVerificationStageVisitor1.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

using namespace zillians::language::tree;

namespace zillians { namespace language { namespace stage { namespace visitor {

namespace { // anonymous namespace for implementation detail

std::vector<const tree::VariableDecl*> getReferedConstVariables(const Expression& expr)
{
    const auto*const resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&expr);

    if (resolved_symbol)
    {
        const auto*const resolved_symbol_var = cast<VariableDecl>(resolved_symbol);

        if (resolved_symbol_var != nullptr && resolved_symbol_var->is_const)
            return {{resolved_symbol_var}};
        else
            return {};
    }

    const auto*const expr_ternary = cast<TernaryExpr>(&expr);

    if (expr_ternary != nullptr)
    {
        auto result = getReferedConstVariables(*expr_ternary-> true_node);

        boost::push_back(result, getReferedConstVariables(*expr_ternary->false_node));

        return std::move(result);
    }

    return {};
}

void checkAtomicExpr(Expression& node)
{
    ASTNode* resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&node);
    if(resolved_symbol == nullptr) return;
    VariableDecl* decl = cast<VariableDecl>(resolved_symbol);
    if(decl == nullptr) return;

    const bool is_global = decl->isGlobal();
    const bool is_member = decl->is_member;

    if(is_global || is_member)
    {
        Type* type = ASTNodeHelper::getCanonicalType(&node);
        BOOST_ASSERT_MSG(type != nullptr, "missing resolved type");
        PrimitiveType* prim_type = type->getAsPrimitiveType();
        if(prim_type == nullptr)
        {
            LOG_MESSAGE(UNSUPPORTED_ATOMIC_TYPE, &node, _name(decl->name->toString()));
            return;
        }
        PrimitiveKind kind = prim_type->getKind();
        // TODO RSTM library dose not support 16 bit. RSTM do support 8 bit and
        // bool. However, don't know why, 8 bit sometimes fail, see
        // https://github.com/zillians/platform_language/issues/983 for detail.
        // Currently, I just block bool and int 8, but it should be fixed and
        // allowed.
        if(
           kind != PrimitiveKind::BOOL_TYPE   &&
           kind != PrimitiveKind::INT8_TYPE   &&
           kind != PrimitiveKind::INT32_TYPE   &&
           kind != PrimitiveKind::INT64_TYPE   &&
           kind != PrimitiveKind::FLOAT32_TYPE &&
           kind != PrimitiveKind::FLOAT64_TYPE)
        {
            LOG_MESSAGE(UNSUPPORTED_ATOMIC_TYPE, &node, _name(decl->name->toString()));
            return;
        }
    }
}

void verifyAtomicType(Block& node)
{
    AtomicBlock* atomic_block = cast<AtomicBlock>(&node);
    if(atomic_block == nullptr)
        return;

    ASTNodeHelper::foreachApply<ASTNode>(*atomic_block, [&](ASTNode& astNode)
    {
        if(BinaryExpr* binary_expr = cast<BinaryExpr>(&astNode))
        {
            checkAtomicExpr(*binary_expr->left);
            checkAtomicExpr(*binary_expr->right);
        }
        else if(UnaryExpr* unary_expr = cast<UnaryExpr>(&astNode))
        {
            checkAtomicExpr(*unary_expr->node);
        }
    });
}

void verifyAsyncBlock(const Block& block)
{
    const auto*const async_block = cast<AsyncBlock>(&block);

    if (async_block == nullptr)
        return;

    detail::verify_async(*async_block);
}

} // anonymous namespace

SemanticVerificationStageVisitor1::VariableState::VariableState(Block* block)
    : state(Declared)
    , block(block)
    , first_read(NULL)
{
}

void SemanticVerificationStageVisitor1::VariableState::write() {
    switch(state) {
        case Declared  : state = Set      ; break;
        case Set       :                    break;
        case OK        :                    break;
        case UnInitRef :                    break;
        case NeverUsed :                    break;
    }
}

void SemanticVerificationStageVisitor1::VariableState::read(IdExpr* location) {
    if(first_read == NULL)
        first_read = location;

    switch(state) {
        case Declared  : state = UnInitRef; break;
        case Set       : state = OK       ; break;
        case OK        :                    break;
        case UnInitRef :                    break;
        case NeverUsed :                    break;
    }
}

void SemanticVerificationStageVisitor1::VariableState::end() {
    switch(state) {
        case Declared  : state = NeverUsed; break;
        case Set       : state = NeverUsed; break;
        case OK        :                    break;
        case UnInitRef :                    break;
        case NeverUsed :                    break;
    }
}

SemanticVerificationStageVisitor1::SemanticVerificationStageVisitor1()
{
    REGISTER_ALL_VISITABLE_ASTNODE(verifyInvoker);
}

void SemanticVerificationStageVisitor1::verify(ASTNode& node)
{
    revisit(node);
}

void SemanticVerificationStageVisitor1::verify(Package& node)
{
    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor1::verify(ClassDecl& node)
{
    arch_verifier.reportErrorIfArchDiffFromProject(node);

    {
        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
        if(tid != NULL && !tid->isFullySpecialized())
        {
            NameVerificationVisitor formalVisitor(nameShadowVerifier);

            formalVisitor.visit(node);

            return;
        }
    }

    bool keep_going = true;
    keep_going &= verifyValidClassToExtend(node);
    keep_going &= verifyValidInterfacesToImplement(node);
    if(keep_going) keep_going = verifyNoCyclicInheritance(node);
    // Perform next verification only if there is no cyclic inheritance
    if(keep_going) keep_going = verifyNoInterfaceReImplement(node);

    nameShadowVerifier.enter(node, keep_going);

    {
        ArchitectureVerifier::AutoArchPusher undo_later(arch_verifier, node.getActualArch());

        revisit(node);

        arch_verifier.verifyArchOfBaseAndInterfaces(node);
    }

    nameShadowVerifier.leave(node);

    verifyVirtualFunctions(node);
}

void SemanticVerificationStageVisitor1::verify(TypedefDecl& node)
{
    arch_verifier.reportErrorIfArchDiffFromProject(node);

    {
        ArchitectureVerifier::AutoArchPusher undo_later(arch_verifier, node.getActualArch());

        arch_verifier.reportErrorIfCrossArchReference(node, *node.type);
    }
}

void SemanticVerificationStageVisitor1::verify(BlockExpr& node)
{
    {
        ArchitectureVerifier::AutoArchPusher undo_later(arch_verifier, node.block->getActualArch());

        revisit(node);
    }

    verifyNotAbstractClass(node);
}

void SemanticVerificationStageVisitor1::verify(EnumDecl& node)
{
    arch_verifier.reportErrorIfArchDiffFromProject(node);

    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor1::verify(TypenameDecl& node)
{
    nameShadowVerifier.enter(node);

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor1::verify(Source& node)
{
    // only verify non-imported AST
    if(!node.is_imported)
    {
        revisit(node);
    }
}

void SemanticVerificationStageVisitor1::verify(ObjectLiteral& node)
{
    // INVALID_NONSTATIC_CALL
    // INVALID_NONSTATIC_REF
    verifyStaticAccessViolation(node);

    revisit(node);
}

void SemanticVerificationStageVisitor1::verify(MemberExpr& node)
{
    ASTNode* resolved_symbol = ResolvedSymbol::get(&node);
    if(resolved_symbol) // NOTE: NULL if package
    {
        // INVALID_ACCESS_PRIVATE
        // INVALID_ACCESS_PROTECTED
        if(isa<VariableDecl>(resolved_symbol))
            verifyVisibilityAccessViolation(&node, cast<VariableDecl>(resolved_symbol));
        else if(isa<FunctionDecl>(resolved_symbol))
            verifyVisibilityAccessViolation(&node, cast<FunctionDecl>(resolved_symbol));
    }

    // INVALID_NONSTATIC_CALL
    // INVALID_NONSTATIC_REF
    verifyStaticAccessViolation(node);

    revisit(node);

    arch_verifier.reportErrorIfCrossArchReference(node);
}

void SemanticVerificationStageVisitor1::verify(UnpackExpr& node)
{
    // update temporary variables' status in unpack expression
    for(auto temporary_variable : node.unpack_list)
    {
        // the temporary variables should be declared somewhere, and can be written
        auto* resolved_temporary_variable = ResolvedSymbol::get(temporary_variable);
        BOOST_ASSERT( resolved_temporary_variable != nullptr && "can not be resolved" );

        auto* temporary_variable_decl = cast<VariableDecl>(resolved_temporary_variable);
        BOOST_ASSERT( temporary_variable_decl != nullptr && "can not find declaration" );

        BOOST_ASSERT( temporary_variable_decl->getOwner<Block>() != nullptr && "temporary variable in unpack expression should be local" );

        auto temporary_variable_status = var_status.find( temporary_variable_decl );
        BOOST_ASSERT( temporary_variable_status != var_status.end() && "can not find status of temporary variable in unpack expression" );

        // variables in unpack expression are for iniailizing
        temporary_variable_status->second.write();
    }

    // INVALID_NONSTATIC_CALL
    // INVALID_NONSTATIC_REF
    verifyStaticAccessViolation(*node.call);

    revisit(node);
}

void SemanticVerificationStageVisitor1::verify(IdExpr& node)
{
    arch_verifier.reportErrorIfCrossArchReference(node);

    ASTNode* resolved_symbol = ResolvedSymbol::get(&node);
    if(resolved_symbol) // NOTE: NULL if package
    {
        if(isa<VariableDecl>(resolved_symbol))
        {
            VariableDecl* var_decl = cast<VariableDecl>(resolved_symbol);

            // INVALID_ACCESS_PRIVATE
            // INVALID_ACCESS_PROTECTED
            verifyVisibilityAccessViolation(&node, var_decl);

            auto i = var_status.find(var_decl);
            if(i != var_status.end())
            {
                /*
                 * if a var is just write, we take it as write, ex: v = 0
                 * if a var is just read , we take it as read , ex: v == 0
                 * if a var is write then read, we take it as write and read, ex: the v in x = v = 0
                 * if a var is read then write, we take it as read, ex: v += 1
                 */
                if(isWrite(node))
                    i->second.write();
                if(isRead(node))
                    i->second.read(&node);
            }

        }
        else if(isa<FunctionDecl>(resolved_symbol))
        {
            FunctionDecl* func_decl = cast<FunctionDecl>(resolved_symbol);

            // INVALID_ACCESS_PRIVATE
            // INVALID_ACCESS_PROTECTED
            verifyVisibilityAccessViolation(&node, func_decl);
        }
    }

    // INVALID_NONSTATIC_CALL
    // INVALID_NONSTATIC_REF
    verifyStaticAccessViolation(node);

    revisit(node);

}

void SemanticVerificationStageVisitor1::verify(CallExpr& node)
{
    for(auto& param : node.parameters)
    {
        ASTNode* resolved_symbol = ResolvedSymbol::get(param);
        VariableDecl * var_decl = NULL;
        if( resolved_symbol && (var_decl = cast<VariableDecl>(resolved_symbol)) )
        {
            if( isNormalLocalVariable(*var_decl) )
            {
                auto it = var_status.find(var_decl);
                if(it != var_status.end() && it->second.state == VariableState::UnInitRef)
                    LOG_MESSAGE(UNINIT_REF, &node, _var_id(var_decl->name->toString()));
            }
        }
    }

    revisit(node);
}

void SemanticVerificationStageVisitor1::verify(UnaryExpr& node)
{
    verifyModify(node);
}

void SemanticVerificationStageVisitor1::verify(BinaryExpr& node)
{
    verifyAssignment(node);
    verifyCompare(node);
    verifyNoDivideByZero(node);

    revisit(node);
}

void SemanticVerificationStageVisitor1::verify(TernaryExpr& node)
{
    verifyTernaryExprHasType(node);

    revisit(node);
}

void SemanticVerificationStageVisitor1::verify(VariableDecl& node)
{
    arch_verifier.reportErrorIfAny(node);

    nameShadowVerifier.enter(node);

    if (isNormalLocalVariable(node))
        var_status.insert(std::make_pair(&node, VariableState(*block_stack.rbegin())));

    revisit(node);

    nameShadowVerifier.leave(node);
}

void SemanticVerificationStageVisitor1::verify(FunctionDecl& node)
{
    arch_verifier.reportErrorIfAny(node);

    {
        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
        if(tid != NULL && !tid->isFullySpecialized())
        {
            NameVerificationVisitor formalVisitor(nameShadowVerifier);

            formalVisitor.visit(node);

            return;
        }
    }

    nameShadowVerifier.enter(node);

    {
        ArchitectureVerifier::AutoArchPusher undo_later(arch_verifier, node.getActualArch());

        revisit(node);
    }

    nameShadowVerifier.leave(node);

    // all-path-return
    if(!AllPathReturnVerifier::verifyFuncDeclImpl(node))
    {
        LOG_MESSAGE(NOT_ALL_PATH_RETURN, &node);
    }

    verifyCtorDtorMustReturnVoid(node);
}

void SemanticVerificationStageVisitor1::verify(Block& node)
{
    nameShadowVerifier.enter(node);
    block_stack.push_back(&node);

    revisit(node);

    verifyAsyncBlock(node);
    checkVariableStatus(node);

    block_stack.pop_back();
    nameShadowVerifier.leave(node);

    verifyAtomicType(node);
}

void SemanticVerificationStageVisitor1::verify(IfElseStmt& node)
{
    if(node.getAnnotations()) visit(*node.getAnnotations());

    // variable state check
    VarStateMapType orig_var_status = var_status;
    VarStateMapType result;

    // if branch
    visit(*node.if_branch);
    result.swap(var_status);
    var_status = orig_var_status;

    // if-else branch
    for(Selection* elseif_branch: node.elseif_branches)
    {
        visit(*elseif_branch);
        mergeVarState(result, var_status);
        var_status = orig_var_status;
    }

    // else block
    if(node.else_block != NULL)
    {
        visit(*node.else_block);
        mergeVarState(result, var_status);
    }
    else
    {
        mergeVarState(result, orig_var_status);
    }

    var_status = result;
}

void SemanticVerificationStageVisitor1::verify(SwitchStmt& node)
{
    Type* resolved_type = node.node->getCanonicalType();
    // switch on invalid type
    if(!(resolved_type->isIntegerType() || resolved_type->isEnumType()))
    {
        LOG_MESSAGE(SWITCH_ON_INVALID_TYPE, &node);
    }

    // MISSING_CASE
    if(resolved_type->isEnumType()) // NOTE: only need to handle enum case
    {
        EnumDecl* enum_decl = resolved_type->getAsEnumDecl();
        for(auto& value : enum_decl->values)
            SemanticVerificationEnumKeyContext_HasVisited::bind(value);
        for(auto* c : node.cases)
        {
            ASTNode* resolved_symbol = ResolvedSymbol::get(c->cond);
            if(isa<VariableDecl>(resolved_symbol))
                SemanticVerificationEnumKeyContext_HasVisited::unbind(resolved_symbol);
        }
        for(auto* value : enum_decl->values)
        {
            if(SemanticVerificationEnumKeyContext_HasVisited::is_bound(value))
            {
                if(!node.default_block)
                {
                    LOG_MESSAGE(MISSING_CASE, &node, _id(value->name->toString()));
                }
                SemanticVerificationEnumKeyContext_HasVisited::unbind(value);
            }
        }
    }

    if(node.getAnnotations()) visit(*node.getAnnotations());
    if(node.node) visit(*node.node);

    verifyCaseHasConstantExpression(node);
    verifyIntegerCaseValues(node);
    verifyCasesAreNotDuplicate(node);

    // variable state check
    VarStateMapType orig_var_status = var_status;
    VarStateMapType result;

    for(auto i = std::begin(node.cases); i != std::end(node.cases); ++i)
    {
        if(i != std::begin(node.cases))
            var_status = orig_var_status;

        visit(**i);

        if(i == std::begin(node.cases))
            result = var_status;
        else
            mergeVarState(result, var_status);
    }

    if(node.default_block != NULL)
    {
        var_status = orig_var_status;
        visit(*node.default_block);

        if(node.cases.empty())
            result = var_status;
        else
            mergeVarState(result, var_status);
    }
    else
    {
        mergeVarState(result, orig_var_status);
    }

    var_status = result;
}

void SemanticVerificationStageVisitor1::verifyModify(tree::UnaryExpr& unary)
{
    // exit directly for exprs which won't modify variables themselves
    if (unary.opcode != tree::UnaryExpr::OpCode::INCREMENT &&
        unary.opcode != tree::UnaryExpr::OpCode::DECREMENT)
    {
        return;
    }

    // not reference to any const variables
    auto const_variables = getReferedConstVariables(*unary.node);
    if (const_variables.empty())
    {
        return;
    }

    using boost::adaptors::transformed;
    auto const_variable_names = boost::algorithm::join(
        const_variables
        | transformed(std::mem_fn(&tree::Declaration::getName))
        | transformed(std::mem_fn(&tree::Identifier::toString)),
        L", "
    );

    const auto* form = (unary.opcode == tree::UnaryExpr::OpCode::INCREMENT ? L"increment" : L"decrement");
    LOG_MESSAGE(WRITE_CONST, &unary, _form(form), _var_id(const_variable_names));
}

void SemanticVerificationStageVisitor1::verifyAssignment(tree::BinaryExpr& binary)
{
    using boost::adaptors::transformed;

    if (!binary.isAssignment())
        return;

    if (binary.left->isRValue())
    {
        LOG_MESSAGE(WRITE_RVALUE, &binary);
    }
    else
    {
        {
            const auto*const init_node     = SplitReferenceContext::get(&binary);
            const auto&      const_vars    = getReferedConstVariables(*binary.left);
            const auto&      writing_const = boost::algorithm::any_of(const_vars, [init_node](const tree::VariableDecl* const_var) { return const_var != init_node; } );

            if (writing_const)
            {
                LOG_MESSAGE(
                    WRITE_CONST,
                    &binary,
                    _form(L"assignment"),
                    _var_id(
                        boost::algorithm::join(
                              const_vars
                            | transformed(std::mem_fn(&tree::Declaration::getName))
                            | transformed(std::mem_fn(&tree::Identifier::toString)),
                            L", "
                        )
                    )
                );
            }
        }

        if (auto*const rhs_var_decl = cast_or_null<tree::VariableDecl>(tree::ASTNodeHelper::getCanonicalSymbol(binary.right)))
        {
            if (isNormalLocalVariable(*rhs_var_decl))
            {
                auto it = var_status.find(rhs_var_decl);

                if (it != var_status.end() && it->second.state == VariableState::UnInitRef)
                    LOG_MESSAGE(UNINIT_REF, &binary, _var_id(rhs_var_decl->name->toString()));
            }
        }
    }
}

void SemanticVerificationStageVisitor1::verifyNotAbstractClass(BlockExpr& expr)
{
    if(expr.tag != "new_restructure")
        return;

    auto* class_type = expr.getCanonicalType();
    BOOST_ASSERT(class_type && class_type->getAsClassDecl() && "internal restruction error");

    auto* class_decl = class_type->getAsClassDecl();

    if (!class_decl->isVirtualTableReady())
        return;

    if (!class_decl->hasPureVirtual())
        return;

    const auto& all_pure_virtuals = class_decl->getPureVirtuals();

    BOOST_ASSERT(!all_pure_virtuals.empty() && "has pure virtual but no any is found");

    LOG_MESSAGE(INSTANTIATE_ABSTRACT_CLASS, &expr, _name(class_decl->name->toString()));

    for(const auto*const pure_virtual : all_pure_virtuals)
    {
        ASTNode* origin_class_node = pure_virtual->parent;

        BOOST_ASSERT(isa<ClassDecl>(origin_class_node) && "pure-virtual function is not declared in class");

        LOG_MESSAGE(INSTANTIATE_ABSTRACT_CLASS_VIRTUAL, &expr, _name(pure_virtual->name->toString()), _origin_class(cast<ClassDecl>(origin_class_node)->name->toString()));
    }
}

void SemanticVerificationStageVisitor1::verifyVirtualFunctions(const ClassDecl& cls)
{
    if (!cls.isVirtualTableReady())
        return;

    for (FunctionDecl*const member_function : cls.member_functions)
    {
        const auto& overridens = cls.vtable.find_overridens(*member_function);

        verifyCorrectlyOverriden(*member_function, overridens);
        verifyNoMixingOverriden(*member_function, overridens);
        boost::for_each(overridens, [this, member_function](const FunctionDecl*const overriden) { verifyIsReturnTypeCompatible(*member_function, *overriden); });
    }
}

void SemanticVerificationStageVisitor1::verifyIsReturnTypeCompatible(FunctionDecl& overrider, const FunctionDecl& overriden)
{
    BOOST_ASSERT(overriden.type && "null pointer exception");
    BOOST_ASSERT(overrider.type && "null pointer exception");

    Type* base_node    = overriden.type->getCanonicalType();
    Type* derived_node = overrider.type->getCanonicalType();

    BOOST_ASSERT(base_node && "no canonical type of return type on overriden");
    BOOST_ASSERT(derived_node && "no canonical type of return type on overrider");

    ClassDecl*    base_cls =    base_node->getAsClassDecl();
    ClassDecl* derived_cls = derived_node->getAsClassDecl();

    bool is_valid_overriden = false;

    if (base_cls != nullptr && derived_cls != nullptr)
    {
        is_valid_overriden = base_cls == derived_cls || ASTNodeHelper::isInheritedFrom(derived_cls, base_cls);
    }
    else if (base_cls == nullptr && derived_cls == nullptr)
    {
        is_valid_overriden = base_node == derived_node;
    }
    else
    {
        is_valid_overriden = false;
    }

    if(!is_valid_overriden)
    {
        BOOST_ASSERT(base_node->isRecordType() || base_node->isFunctionType() || base_node->isPrimitiveType());

        LOG_MESSAGE(
            INVALID_COVARIANCE,
            &overrider,
            _origin_return_type(
                base_node->isRecordType() ? base_node->getAsClassDecl()->name->toString() :
                base_node->isFunctionType() ? base_node->getAsFunctionType()->toString() :
                base_node->getAsPrimitiveType()->toString()
            ),
            _origin_class(cast<ClassDecl>(overriden.parent)->name->toString())
        );
    }
}

template<typename range_type>
void SemanticVerificationStageVisitor1::verifyCorrectlyOverriden(FunctionDecl& overrider, const range_type& overridens)
{
    if (!overrider.is_override)
        return;

    if (overridens.empty())
    {
        LOG_MESSAGE(OVERRIDEN_FUNCTION_DOES_NOT_OVERRIDEN, &overrider, _name(overrider.name->toString()));
    }
}

template<typename range_type>
void SemanticVerificationStageVisitor1::verifyNoMixingOverriden(FunctionDecl& overrider, const range_type& overridens)
{
    using boost::adaptors::filtered;

    if (overridens.empty())
        return;

    const auto& mixed_overridens =
          overridens
        | filtered(
              [&overrider](const FunctionDecl*const overriden_function)
              {
                  return !overrider.is_task != !overriden_function->is_task;
              }
          )
    ;

    if (mixed_overridens.empty())
        return;

    LOG_MESSAGE(MIXING_OVERRIDEN_OF_TASK_AND_FUNCTION, &overrider);

    for (const FunctionDecl*const mixed_overriden : mixed_overridens)
    {
        LOG_MESSAGE(MIXING_OVERRIDEN_OF_TASK_AND_FUNCTION_INFO, mixed_overriden);
    }
}

bool SemanticVerificationStageVisitor1::isWrite(IdExpr& node)
{
    if(BinaryExpr* bin_expr = cast<BinaryExpr>(node.parent))
    {
        // we take a = 1 as write but a += 1, a &= 1, ... as read
        if(bin_expr->opcode == BinaryExpr::OpCode::ASSIGN && bin_expr->left == &node)
        {
            return true;
        }
    }
    return false;
}

bool SemanticVerificationStageVisitor1::isRead(IdExpr& node)
{
    // for case: a = b = 0, func( b = 0 ), while ( b = x ), ...
    // we take b as write and read
    if( isWrite( node ) && isa<ExpressionStmt>(node.parent->parent) )
    {
        return false;
    }
    return true;
}

bool SemanticVerificationStageVisitor1::isNormalLocalVariable(VariableDecl& node)
{
    if(node.is_member)
        return false;

    if(isa<FunctionDecl>(node.parent))
        return false;

    if(node.isGlobal())
        return false;

    return true;
}

void SemanticVerificationStageVisitor1::checkVariableStatus(Block& block)
{
    for(auto& status : var_status)
    {
        if(status.second.block == &block)
            status.second.end();
    }

    for(auto i = var_status.begin(); i != var_status.end(); )
    {
        if(i->second.block == &block)
        {
            if(i->second.state == VariableState::NeverUsed)
                LOG_MESSAGE(VAR_NEVER_USED, i->first, _var_id(i->first->name->toString()));
            if(i->second.state == VariableState::UnInitRef)
                LOG_MESSAGE(UNINIT_REF, i->second.first_read, _var_id(i->first->name->toString()));
            var_status.erase(i++);
        }
        else
        {
            ++i;
        }
    }
}

void SemanticVerificationStageVisitor1::mergeVarState(VarStateMapType& lhs, VarStateMapType& rhs)
{
    BOOST_ASSERT(lhs.size() == rhs.size());

    auto l = lhs.begin();
    auto r = rhs.begin();
    for(; l != lhs.end(); ++l, ++r)
    {
        BOOST_ASSERT(l->first == r->first);
        if     (l->second.state == VariableState::UnInitRef || r->second.state == VariableState::UnInitRef) l->second.state = VariableState::UnInitRef;
        else if(l->second.state == VariableState::Declared  || r->second.state == VariableState::Declared ) l->second.state = VariableState::Declared;
        else if(l->second.state == VariableState::OK        || r->second.state == VariableState::OK       ) l->second.state = VariableState::OK;
        else                                                                                                l->second.state = VariableState::Set;
    }
}

bool SemanticVerificationStageVisitor1::verifyValidClassToExtend(ClassDecl& node)
{
    bool all_valid = true;

    if(node.base != NULL)
    {
        BOOST_ASSERT( !node.is_interface);

        Type* base_node = node.base->getCanonicalType();

        BOOST_ASSERT( base_node != NULL);

        if(!base_node->getAsClassDecl() || base_node->getAsClassDecl()->is_interface)
        {
            all_valid = false;

            LOG_MESSAGE(INVALID_BASE, &node, _derived_type(node.name->toString()), _base_type(node.base->toString()));
        }
    }

    return all_valid;
}

bool SemanticVerificationStageVisitor1::verifyValidInterfacesToImplement(ClassDecl& node)
{
    bool all_valid = true;

    for (TypeSpecifier* implement: node.implements)
    {
        BOOST_ASSERT(implement && "null pointer exception");

        Type* extended_node = implement->getCanonicalType();
        BOOST_ASSERT(extended_node && "no canonical type!");

        if(!extended_node->getAsClassDecl() || !extended_node->getAsClassDecl()->is_interface)
        {
            const wchar_t *derived_class = node.is_interface ? L"interface" : L"class";
            const wchar_t *impl_method = node.is_interface ? L"extends" : L"implements";

            all_valid = false;

            LOG_MESSAGE(INVALID_EXTEND, &node, _derived_class(derived_class), _derived_type(node.name->toString()), _impl_method(impl_method), _base_type(implement->toString()));
        }
    }

    return all_valid;
}

bool SemanticVerificationStageVisitor1::verifyNoCyclicInheritance(ClassDecl& node)
{
    if(ASTNodeHelper::isInheritedFrom(&node, &node))
    {
        LOG_MESSAGE(CYCLIC_INHERITANCE, &node, _class_type(node.name->toString()));

        return false;
    }
    else
    {
        return true;
    }
}

bool SemanticVerificationStageVisitor1::verifyNoInterfaceReImplement(ClassDecl& node)
{
    // This function will get into infinite recursive call if there is cyclic inheritance

    // Key is the interface implemented, value is implemented count
    std::unordered_map<ClassDecl*, unsigned> duplicate_interfaces;

    std::function<void(ClassDecl&)> interface_collector;

    // Collector which invokes recursively
    interface_collector = [&duplicate_interfaces, &interface_collector](ClassDecl& node){
        for(auto* impl : node.implements)
        {
            Type* node_current = impl->getCanonicalType();
            ClassDecl* class_current = node_current->getAsClassDecl();

            unsigned &impl_count = duplicate_interfaces.insert(std::make_pair(class_current, 0u)).first->second;
            ++impl_count;

            interface_collector(*class_current);
        }
    };

    // Collect all indirectly implemented interface
    for(TypeSpecifier* type_current = ASTNodeHelper::getTypeSpecifierFrom(&node); type_current; )
    {
        Type* node_current = type_current->getCanonicalType();
        ClassDecl* class_current = node_current->getAsClassDecl();

        interface_collector(*class_current);

        type_current = class_current->base;
    }

    // Check if directly implemented interfaces are found in indirectly implemented
    bool all_valid = true;

    for(auto& interface : duplicate_interfaces)
    {
        BOOST_ASSERT( interface.second > 0u);

        if( interface.second > 1)
        {
            all_valid = false;

            LOG_MESSAGE(INTERFACE_REIMPLEMENT, &node, _interface_type(interface.first->name->toString()), _class_type(node.name->toString()));
        }
    }

    return all_valid;
}

template<class T>
void SemanticVerificationStageVisitor1::verifyVisibilityAccessViolation(ASTNode* node_ref, T* node_decl)
{
    BOOST_ASSERT(node_ref && node_decl && "null pointer exception");
    BOOST_ASSERT(!node_decl->name->toString().empty() && "empty name");

    // INVALID_ACCESS_PRIVATE
    // INVALID_ACCESS_PROTECTED
    if(node_decl->is_member)
    {
        SemanticHackContext* semantic_hack = SemanticHackContext::get(node_ref);
        bool skip_private_access_check = (semantic_hack && semantic_hack->skip_private_access_check);
        bool skip_protected_access_check = (semantic_hack && semantic_hack->skip_protected_access_check);

        ClassDecl* ref_point = node_ref->getOwner<ClassDecl>();
        ClassDecl* decl_point = node_decl->template getOwner<ClassDecl>();
        if(decl_point && ref_point != decl_point)
        {
            switch(node_decl->visibility)
            {
            case Declaration::VisibilitySpecifier::PRIVATE:
                {
                    if (!skip_private_access_check)
                    {
                        LOG_MESSAGE(INVALID_ACCESS_PRIVATE, node_ref, _id(node_decl->name->toString()));
                    }
                }
                break;
            case Declaration::VisibilitySpecifier::PROTECTED:
            case Declaration::VisibilitySpecifier::DEFAULT:
                if(!ref_point || !ASTNodeHelper::isInheritedFrom(ref_point, decl_point))
                {
                    if (!skip_protected_access_check)
                    {
                        LOG_MESSAGE(INVALID_ACCESS_PROTECTED, node_ref, _id(node_decl->name->toString()));
                    }
                }
                break;
            case Declaration::VisibilitySpecifier::PUBLIC:
                break;
            }
        }
    }
}

template<typename ExprType>
bool SemanticVerificationStageVisitor1::verifyStaticAccessViolationHasObject(ExprType&)
{
    return false;
}

bool SemanticVerificationStageVisitor1::verifyStaticAccessViolationHasObject(MemberExpr& expr)
{
    BOOST_ASSERT(expr.node && "null pointer exception");

    if(ASTNode* callee_obj = ResolvedSymbol::get(expr.node))
    {
        if(isa<VariableDecl>(callee_obj))
        {
            return true;
        }
    }
    else if(isa<CallExpr>(expr.node))
    {
        return true;
    }
    else if(CastExpr* cast_expr = cast<CastExpr>(expr.node))
    {
        // destination type should be a class type
        return isa<RecordType>(cast_expr->type->getCanonicalType());
    }
    else if(BlockExpr* block_expr = cast<BlockExpr>(expr.node))
    {
        return (block_expr->tag == "new_restructure") ||
               (block_expr->tag == "array_expr") ||
               (block_expr->tag == "lambda_restruct");
    }

    return false;
}

template<typename ExprType>
void SemanticVerificationStageVisitor1::verifyStaticAccessViolation(ExprType& expr)
{
    ASTNode* callee_symbol = ResolvedSymbol::get(&expr);

    if(callee_symbol)
    {
        bool must_have_obj = false;

        if(FunctionDecl* member_func = cast<FunctionDecl>(callee_symbol))
        {
            must_have_obj = member_func->is_member && !member_func->is_static;
        }
        else if(VariableDecl* member_var = cast<VariableDecl>(callee_symbol))
        {
            must_have_obj = member_var->is_member && !member_var->is_static;
        }

        if(must_have_obj)
        {
            FunctionDecl* caller_func = expr.template getOwner<FunctionDecl>();

            BOOST_ASSERT(caller_func && "null pointer exception");

            if(caller_func->is_member && !caller_func->is_static)
            {
                ClassDecl* callee_cls = callee_symbol->getOwner<ClassDecl>();
                ClassDecl* caller_cls = cast<ClassDecl>(caller_func->parent);

                BOOST_ASSERT(callee_cls && "member function/variable is not under ClassDecl");
                BOOST_ASSERT(caller_cls && "member function/variable is not under ClassDecl");

                if(callee_cls == caller_cls || ASTNodeHelper::isInheritedFrom(caller_cls, callee_cls))
                {
                    must_have_obj = false;
                }
            }
        }

        if(must_have_obj && !verifyStaticAccessViolationHasObject(expr))
        {
            if(isa<CallExpr>(expr.parent))
            {
                LOG_MESSAGE(INVALID_NONSTATIC_CALL, &expr, _func_id(cast<Declaration>(callee_symbol)->name->toString()));
            }
            else
            {
                LOG_MESSAGE(INVALID_NONSTATIC_REF, &expr, _var_id(cast<Declaration>(callee_symbol)->name->toString()));
            }
        }
    }
}

void SemanticVerificationStageVisitor1::verifyTernaryExprHasType(TernaryExpr& node)
{
    if(ResolvedType::get(&node) == NULL)
    {
        LOG_MESSAGE(TERNARY_EXPRESSION_MISMATCH, &node);
    }
}

void SemanticVerificationStageVisitor1::verifyCaseHasConstantExpression(SwitchStmt& node)
{
    for (Selection* case_: node.cases)
    {
        BOOST_ASSERT(case_->cond && "null pointer exception");

        // we expect constant folding will do the other things and simplify the constant expression
        if (!case_->cond->isConstantLiteral())
        {
            LOG_MESSAGE(CASE_WITH_NON_CONSTANT_VALUE, case_->cond);
        }
    }
}

void SemanticVerificationStageVisitor1::verifyCasesAreNotDuplicate(SwitchStmt& node)
{
    // function get_value_of() returns pair of values
    // (1) pair.first: if skip checking this case
    // (2) pair.second: case value of this case
    auto get_value_of = [](Selection* case_) -> std::pair<bool, int64> {
        auto* condition = case_->cond;
        BOOST_ASSERT(condition != nullptr && "have no condition expression");
        if (auto* literal = condition->getConstantNumericLiteral())
        {
            auto* literal_type = literal->getCanonicalType();
            BOOST_ASSERT(literal_type != nullptr && "no associated literal type");

            if (!literal_type->isFloatType())
                return std::make_pair(false, literal->get<int64>());
        }
        else if (auto* variable_decl = condition->getConstantEnumerator())
        {
            auto* context = EnumIdManglingContext::get(variable_decl);
            BOOST_ASSERT(context != nullptr && "no context was bound");
            return std::make_pair(false, context->value);
        }
        return std::make_pair(true, -1);
    };

    std::map<int64, Selection*> registered_cases;
    for (auto* case_: node.cases)
    {
        bool skip; int64 case_value;
        std::tie(skip, case_value) = get_value_of(case_);
        if (skip) continue;

        auto registered = registered_cases.find(case_value);
        // no duplicate case value yet
        if (registered == registered_cases.end())
            registered_cases.insert(std::make_pair(case_value, case_));
        // same case value was used previously, report error
        else
        {
            LOG_MESSAGE(DUPL_CASE_VALUE, case_->cond);
            LOG_MESSAGE(DUPL_CASE_VALUE_PREV, registered->second->cond);
        }
    }
}

void SemanticVerificationStageVisitor1::verifyIntegerCaseValues(SwitchStmt& node)
{
    for (Selection* case_: node.cases)
    {
        auto* condition = case_->cond;
        BOOST_ASSERT(case_->cond != nullptr && "have no condition expressioncon");
        if (auto* literal = condition->getConstantNumericLiteral())
        {
            auto* literal_type = literal->getCanonicalType();
            BOOST_ASSERT(literal_type != nullptr && "no associated literal type");

            if (literal_type->isFloatType())
                LOG_MESSAGE(NON_INTEGER_CASE_VALUE, case_->cond);
        }
    }
}

void SemanticVerificationStageVisitor1::verifyNoDivideByZero(BinaryExpr& node)
{
    switch(node.opcode)
    {
    case BinaryExpr::OpCode::ARITHMETIC_DIV:
    case BinaryExpr::OpCode::ARITHMETIC_MOD:
    case BinaryExpr::OpCode::DIV_ASSIGN:
    case BinaryExpr::OpCode::MOD_ASSIGN:
        break;
    default:
        return;
    }

    BOOST_ASSERT(node.right && "null pointer exception");

    if (const NumericLiteral* numeric_constant = node.right->getConstantNumericLiteral())
    {
        const PrimitiveKind& primitive_kind = numeric_constant->primitive_kind;

        if (primitive_kind.isIntegerType() || primitive_kind.isBoolType())
        {
            if (numeric_constant->get<int64>() == 0)
            {
                LOG_MESSAGE(DIVIDE_BY_ZERO, &node);
            }
        }
    }
}

void SemanticVerificationStageVisitor1::verifyCompare(BinaryExpr& node)
{
    if(node.isComparison())
    {
        Type* l_type = node.left->getCanonicalType();
        Type* r_type = node.right->getCanonicalType();

        // give an error message if comparing with improper type
        if(l_type != r_type)
        {
            std::wstring l_str = l_type->toString();
            std::wstring r_str = r_type->toString();
            LOG_MESSAGE(CANT_COMPARE, &node, _lhs_type(l_str), _rhs_type(r_str));
        }

        // give a warning message if equal comparing with a float
        if( node.isEqualComparison() &&
            l_type->isFloatType() &&
            r_type->isFloatType() )
        {
            LOG_MESSAGE(EQUAL_COMPARE_WITH_FLOAT, &node);
        }
    }
}

void SemanticVerificationStageVisitor1::verifyCtorDtorMustReturnVoid(FunctionDecl& node)
{
    if (node.isConstructor() || node.isDestructor())
    {
        BOOST_ASSERT(node.type);
        BOOST_ASSERT(node.type->getCanonicalType());
        if(!(node.type->getCanonicalType()->isVoidType()))
        {
            const auto*const name = node.isConstructor() ? L"constructor" : L"destructor";

            LOG_MESSAGE(CTOR_DTOR_MUST_RETURN_VOID, &node, _name(name));
        }
    }
}

void SemanticVerificationStageVisitor1::applyCleanup()
{
    for(auto& cleaner : cleanup)
        cleaner();
    cleanup.clear();
}



} } } }
