/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/verifier/StaticTestVerificationStage.h"
#include "language/stage/verifier/visitor/StaticTestVerificationStageVisitor.h"
#include "language/context/ParserContext.h"

namespace zillians { namespace language { namespace stage {

StaticTestVerificationStage::StaticTestVerificationStage()
{ }

StaticTestVerificationStage::~StaticTestVerificationStage()
{ }

const char* StaticTestVerificationStage::name()
{
    return "Static Test Verification Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> StaticTestVerificationStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options();

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
        ("enable-static-test", "enable static test stage");

    return std::make_pair(option_desc_public, option_desc_private);
}

bool StaticTestVerificationStage::parseOptions(po::variables_map& vm)
{
    if(vm.count("enable-static-test"))
    {
        enabled = true;
    }
    return true;
}

bool StaticTestVerificationStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!enabled)
        return true;

    if(!hasParserContext())
        return false;

    ParserContext& parser_context = getParserContext();

    if(parser_context.tangle)
    {
        visitor::StaticTestVerificationStageVisitor verifier;
        verifier.visit(*parser_context.tangle);

        if(verifier.isAllMatch())
            return true;
        else
            return false;
    }
    else
    {
        return false;
    }
}

} } }
