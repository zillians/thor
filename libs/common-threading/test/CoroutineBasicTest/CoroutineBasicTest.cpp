/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "threading/Coroutine.h"

#include <iostream>
#include <string>
#include <limits>

#define BOOST_TEST_MODULE CoroutineBasicTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( CoroutineBasicTest )

struct CoroutineBasicTestCase1_Coroutine1
{
    struct State : public Coroutine
    {
        State(boost::asio::io_service& _io_service, int _iterations) : io_service(_io_service), timer(_io_service, boost::posix_time::milliseconds(200)), iterations(_iterations), current_iteration(0)
        { printf("State constructed, this = %p\n", this); }

        ~State()
        { printf("State destroyed, this = %p\n", this); }

        int current_iteration;
        int iterations;
        boost::asio::io_service& io_service;
        boost::asio::deadline_timer timer;
    };

    CoroutineBasicTestCase1_Coroutine1(boost::asio::io_service& _io_service, int _iterations) : state(new State(_io_service, _iterations))
    { }

    void operator() (const boost::system::error_code& ec = boost::system::error_code())
    {
        printf("this = %p, entered\n", state.get());
        CoroutineReenter(state.get())
        {
            CoroutineEntry:
            for (;state->current_iteration < state->iterations; ++state->current_iteration)
            {
                printf("this = %p, before current iteration = %d\n", state.get(), state->current_iteration);
                CoroutineYield state->timer.async_wait(*this);
                state->timer.expires_at(state->timer.expires_at() + boost::posix_time::milliseconds(200));
                printf("this = %p, after current iteration = %d\n", state.get(), state->current_iteration);
            }
        }
        printf("this = %p, leaved\n", state.get());
    }

    shared_ptr<State> state;
};

BOOST_AUTO_TEST_CASE( CoroutineBasicTestCase1 )
{
    boost::asio::io_service io_service;
    CoroutineBasicTestCase1_Coroutine1 c0(io_service, 10);
    io_service.post(c0);
    CoroutineBasicTestCase1_Coroutine1 c1(io_service, 5);
    io_service.post(c1);
    io_service.run();
}

BOOST_AUTO_TEST_SUITE_END()

