/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>

#include <tbb/tick_count.h>
#include <tbb/tbb_thread.h>
#include <boost/assert.hpp>
#include <boost/bind.hpp>

#include "core/AtomicQueue.h"

using std::cout;
using std::endl;
using namespace zillians;

#define numElements 256

const int numData = 10000000;

struct TestMsg
{
    int size;
};

void ThreadReader(atomic::AtomicPipe<int, numElements>* pipe)
{
    cout << "reader" << endl;

    int ret;
    int i = 0;
    while(i < numData)
    {
        if(pipe->read(&ret))
        {
            BOOST_ASSERT(ret == i);
            ++i;
        }
    }
}

void ThreadWriter(atomic::AtomicPipe<int, numElements>* pipe)
{
    cout << "writer" << endl;

    //TestMsg* message = new TestMsg;

    for(int i = 0; i < numData; ++i)
    {
        pipe->write(i, false);
        pipe->flush();
    }
}

int main()
{
    atomic::AtomicPipe<int, numElements> atomicPipe;

    tbb::tick_count start, end;
    start = tbb::tick_count::now();

    tbb::tbb_thread threadWriter(boost::bind(&ThreadWriter, &atomicPipe));
    tbb::tbb_thread threadReader(boost::bind(&ThreadReader, &atomicPipe));

    threadWriter.join();
    threadReader.join();

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;

    cout << "enqueue/dequeue: " << numData << " elements in " << total << " ms" << endl;
}
