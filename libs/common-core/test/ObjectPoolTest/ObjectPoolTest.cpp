/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "core/ObjectPool.h"
#include <iostream>
#include <string>
#include <limits>

#define BOOST_TEST_MODULE ObjectPoolTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( ObjectPoolTest )

#define TEST_NUM_POOLED_OBJECT 2048

class PooledObject : public ObjectPool<PooledObject>
{
};

BOOST_AUTO_TEST_CASE( ObjectPoolTestCase1 )
{
    std::vector<PooledObject*> objects;

    for(int i=0;i<TEST_NUM_POOLED_OBJECT;++i)
    {
        BOOST_CHECK_NO_THROW(objects.push_back(new PooledObject));
    }

    while(objects.size() > 0)
    {
        PooledObject* obj = objects.back();
        BOOST_CHECK_NO_THROW(delete obj);
        BOOST_CHECK_NO_THROW(objects.pop_back());
    }
}

class ConcurrentPooledObject : public ConcurrentObjectPool<ConcurrentPooledObject>
{
};

void allocationProc()
{
    try
    {
        std::vector<ConcurrentPooledObject*> objects;

        for(int i=0;i<TEST_NUM_POOLED_OBJECT;++i)
        {
            objects.push_back(new ConcurrentPooledObject);
        }

        while(objects.size() > 0)
        {
            ConcurrentPooledObject* obj = objects.back();
            objects.pop_back();
            delete obj;
        }

        objects.clear();
    }
    catch(std::exception& e)
    {
        BOOST_ERROR(e.what());
    }
    catch(...)
    {
        BOOST_ERROR("Unknown exception");
    }
}

BOOST_AUTO_TEST_CASE( ObjectPoolTestCase2 )
{
    boost::thread t0(boost::bind(allocationProc));
    boost::thread t1(boost::bind(allocationProc));
    boost::thread t2(boost::bind(allocationProc));

    if(t0.joinable()) BOOST_CHECK_NO_THROW(t0.join());
    if(t1.joinable()) BOOST_CHECK_NO_THROW(t1.join());
    if(t2.joinable()) BOOST_CHECK_NO_THROW(t2.join());
}

BOOST_AUTO_TEST_SUITE_END()
