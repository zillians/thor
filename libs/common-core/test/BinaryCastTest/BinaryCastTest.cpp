/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>
#include <limits>

#include "core/IntTypes.h"
#include "core/BinaryCast.h"

#define BOOST_TEST_MODULE BinaryCastTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( BinaryCastTest )

BOOST_AUTO_TEST_CASE( BinaryCastTestCase1 )
{
    // 1 bytes
    {
        int8 a = std::numeric_limits<int8>::max();
        int8 b = binary_cast<int8>(binary_cast<uint8>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint8 a = std::numeric_limits<uint8>::max();
        uint8 b = binary_cast<uint8>(binary_cast<int8>(a));
        BOOST_CHECK(a == b);
    }

    // 2 bytes
    {
        int16 a = std::numeric_limits<int16>::max();
        int16 b = binary_cast<int16>(binary_cast<uint16>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint16 a = std::numeric_limits<uint16>::max();
        uint16 b = binary_cast<uint16>(binary_cast<int16>(a));
        BOOST_CHECK(a == b);
    }

    // 4 bytes
    {
        int32 a = std::numeric_limits<int32>::max();
        int32 b = binary_cast<int32>(binary_cast<uint32>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint32 a = std::numeric_limits<uint32>::max();
        uint32 b = binary_cast<uint32>(binary_cast<int32>(a));
        BOOST_CHECK(a == b);
    }

    {
        float a = std::numeric_limits<float>::max() * 0.3f;
        float b = binary_cast<float>(binary_cast<int32>(a));
        BOOST_CHECK(a == b);
    }

    {
        float a = std::numeric_limits<float>::max() * 0.3f;
        float b = binary_cast<float>(binary_cast<uint32>(a));
        BOOST_CHECK(a == b);
    }

    {
        int32 a = std::numeric_limits<int32>::max();
        int32 b = binary_cast<int32>(binary_cast<float>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint32 a = std::numeric_limits<uint32>::max();
        uint32 b = binary_cast<uint32>(binary_cast<float>(a));
        BOOST_CHECK(a == b);
    }

    // 8 bytes
    {
        int64 a = std::numeric_limits<int64>::max();
        int64 b = binary_cast<int64>(binary_cast<double>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint64 a = std::numeric_limits<uint64>::max();
        uint64 b = binary_cast<uint64>(binary_cast<double>(a));
        BOOST_CHECK(a == b);
    }

    {
        int64 a = std::numeric_limits<int64>::max();
        int64 b = binary_cast<int64>(binary_cast<unsigned long long int>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint64 a = std::numeric_limits<uint64>::max();
        uint64 b = binary_cast<uint64>(binary_cast<unsigned long long int>(a));
        BOOST_CHECK(a == b);
    }

    {
        int64 a = std::numeric_limits<int64>::max();
        int64 b = binary_cast<int64>(binary_cast<long long int>(a));
        BOOST_CHECK(a == b);
    }

    {
        uint64 a = std::numeric_limits<uint64>::max();
        uint64 b = binary_cast<uint64>(binary_cast<long long int>(a));
        BOOST_CHECK(a == b);
    }

    {
        double a = std::numeric_limits<double>::max() * 0.3;
        double b = binary_cast<double>(binary_cast<long long int>(a));
        BOOST_CHECK(a == b);
    }

    {
        double a = std::numeric_limits<double>::max() * 0.3;
        double b = binary_cast<double>(binary_cast<long long int>(a));
        BOOST_CHECK(a == b);
    }
}

BOOST_AUTO_TEST_SUITE_END()
