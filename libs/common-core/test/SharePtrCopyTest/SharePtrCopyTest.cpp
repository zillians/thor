/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
/**
 * @file SharePtrCopyTest.cpp
 *
 * @date March 24, 2010 zac - Initial version created.
 */

#include "tbb/tick_count.h"
#include "core/Buffer.h"
#include <iostream>
#include <vector>

#define BOOST_TEST_MODULE SharePtrCopyTest
//#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians;
using namespace std;


int loop = 1024 * 1024;
unsigned int bufferSize = 8;


BOOST_AUTO_TEST_SUITE( SharePtrCopyTest )


BOOST_AUTO_TEST_CASE ( SharePtrCopy )
{
    shared_ptr<Buffer> source(new Buffer(bufferSize));
    shared_ptr<Buffer> dest;
//  std::vector<shared_ptr<Buffer> > vec;

    tbb::tick_count start, end;
    start = tbb::tick_count::now();

    for (int i = 0; i < loop; i ++) {
        dest = source;
        dest;
    }

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;
    cout << "SharePtrCopy total:" << total << endl;
}

BOOST_AUTO_TEST_CASE ( VoidPtrCopy )
{
    Buffer* source = new Buffer(bufferSize);
    Buffer* dest;

    std::vector<Buffer*> vec;
    tbb::tick_count start, end;
    start = tbb::tick_count::now();

    for (int i = 0; i < loop; i++) {
        dest = source;
        //vec.push_back(source);
    }

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;
    cout << "VoidPtrCopy total:" << total << endl;
}

BOOST_AUTO_TEST_CASE ( SharePtrVectorInsert )
{
    shared_ptr<Buffer> source(new Buffer(bufferSize));
    shared_ptr<Buffer> dest;
    std::vector<shared_ptr<Buffer> > vec;

    tbb::tick_count start, end;
    start = tbb::tick_count::now();

    for (int i = 0; i < loop; i ++) {
        //dest = source;
        //dest;
        vec.push_back(source);
    }

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;
    cout << "SharePtrVectorInsert total:" << total << endl;
}

BOOST_AUTO_TEST_CASE ( VoidPtrVectorInsert )
{
    Buffer* source = new Buffer(bufferSize);
    Buffer* dest;

    std::vector<Buffer*> vec;
    tbb::tick_count start, end;
    start = tbb::tick_count::now();

    for (int i = 0; i < loop; i++) {
        //dest = source;
        vec.push_back(source);
    }

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;
    cout << "VoidPtrVectorInsert total:" << total << endl;
}

BOOST_AUTO_TEST_CASE ( VectorPairCopy )
{
    std::vector<std::pair<void*, size_t> > vec;
    tbb::tick_count start, end;
    start = tbb::tick_count::now();

    for (int i = 0; i < loop; i++) {
        //dest = source;
        vec.push_back(std::make_pair((void*)0, (std::size_t)0));
    }

    end = tbb::tick_count::now();
    float total = (end - start).seconds()*1000.0;
    cout << "VectorPairCopy total:" << total << endl;
}

BOOST_AUTO_TEST_SUITE_END()

