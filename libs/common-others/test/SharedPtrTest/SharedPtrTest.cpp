/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cstdio>
#include <vector>
#include <boost/shared_ptr.hpp>

using namespace boost;

template<class T>
bool operator == (const shared_ptr<T> &p, const T* t)
{
    return (p.get() == t);
}

class A
{
    public:

    A() { printf("ctor\n"); }
    virtual ~A() { printf("dtor\n"); }
};

class B
{
public:
    void setContext(shared_ptr<void> context)
    {
        mContext = context;
    }

    shared_ptr<void> getContext()
    {
        return mContext;
    }

private:
    shared_ptr<void> mContext;
};

int main()
{
    /*
    std::vector< shared_ptr<int> > cont;
    int* n = new int; *n = 10;
    shared_ptr<int> p(n);
    cont.push_back(p);

    shared_ptr<int> xx = (int*)0;

    std::vector< shared_ptr<int> >::iterator i = cont.begin();

    if(*i == n)
    {
        printf("error!!!\n");
    }
    else
    {
        printf("success!!\n");
    }
    */

    B b;

    shared_ptr<void> x;

    {
        shared_ptr<void> p(new A());
        x = p;
    }

    b.setContext(shared_ptr<void>(new A()));

    return 0;
}
