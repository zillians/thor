/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>

#include "utility/UnicodeUtil.h"
#include "language/context/ManglingStageContext.h"
#include "language/stage/stub/ThorStubStage.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"
#include "language/tree/basic/DeclType.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"

using namespace zillians::language;
using namespace zillians::language::tree; // needed by CREATE_INVOKER

std::wstring project_name = L"awe";

class IndentWstream {
public:
    IndentWstream(std::wostream& v) : os(v), level(0) {}
    void increaseIndent() { ++level; }
    void decreaseIndent() { --level; }
    std::wostream& print_indent() { for(size_t i = 0; i != level; ++i) os << L"    "; return os; }
    std::wostream& operator()() { return print_indent(); }
    template <typename T> std::wostream& operator<<(const T& v) { os << v; return os; }
private:
    std::wostream& os;
    size_t level;
} ;

struct packages_and_function_name {
    packages_and_function_name(FunctionDecl* func)
    {
        packages = ASTNodeHelper::getParentScopePackages(func);
        if (isa<SimpleIdentifier>(func->name))
            func_name = func->name->toString();
        else if (TemplatedIdentifier* tid = cast<TemplatedIdentifier>(func->name))
            func_name = tid->id->toString();
    }
    bool operator<(const packages_and_function_name& rhs) const {
        if(packages < rhs.packages) return true;
        if(rhs.packages < packages) return false;
        if(func_name < rhs.func_name) return true;
        return false;
    }
    std::vector<Package*> packages;
    std::wstring func_name;
};

struct package_proxy {
    package_proxy(Package* p) : pkg(p) {}
    Package* pkg;
    std::map<std::wstring, package_proxy> children;
    std::map<std::wstring, std::set<FunctionDecl*>> funcs;
} ;

template<>
std::string get_stub_filename<stage::ThorStubStage::CLIENT_RPC_JS>(stage::ThorStubStage::var_map_t& var_map)
{
    return "rpc.js";
}

static std::wstring jsType(VariableDecl* var)
{
    Type* resolved_type = var->getCanonicalType();
    BOOST_ASSERT(resolved_type != nullptr);
    std::wstring typeStr = resolved_type->toString();
    std::wcerr << typeStr << std::endl;

    if(typeStr == L"int8") return L"Int8";
    if(typeStr == L"int16") return L"Int16";
    if(typeStr == L"int32") return L"Int32";
    if(typeStr == L"int64") return L"Int64";
    if(typeStr == L"float32") return L"Float32";
    if(typeStr == L"float64") return L"Float64";

    return L"unsupported_type";
}

class ArgConstraintPolicyI {
public:
    ArgConstraintPolicyI(IndentWstream& w) : _w(w) {}
    virtual void constraint(VariableDecl* v, size_t i) = 0;
protected:
    IndentWstream& _w;
} ;

class OverloadingArgConstaint : public ArgConstraintPolicyI {
public:
    OverloadingArgConstaint(IndentWstream& w) : ArgConstraintPolicyI(w) {}
    virtual void constraint(VariableDecl* v, size_t i) {
        _w << L"arguments[" << i << L"].constructor == " << jsType(v);
    }
} ;

class OnlyOneArgConstaint : public ArgConstraintPolicyI {
public:
    OnlyOneArgConstaint(IndentWstream& w) : ArgConstraintPolicyI(w) {}
    virtual void constraint(VariableDecl* v, size_t i) {
        Type* resolved = v->getCanonicalType();
        if(PrimitiveType* primitive_type = resolved->getAsPrimitiveType()) {
            cast<PrimitiveType>(resolved);
            if(primitive_type->isIntegerType() || primitive_type->isFloatType()) {
                _w << L"(typeof(arguments[" << i << L"]) == 'number' || arguments[" << i << L"].constructor == " << jsType(v) << L")";
            }
            else if(primitive_type->isBoolType()) {
                _w << L"typeof(arguments[" << i << L"]) == 'boolean'";
            }
            else {
                _w << L"typeof(arguments[" << i << L"]) == 'not_supported_yet'";
            }
        }
        else // not primitive type
        {
            UNIMPLEMENTED_CODE();
        }
    }
} ;

static void printArgsConstraints(FunctionDecl* func, ArgConstraintPolicyI& argCons, bool print_else, IndentWstream& w)
{
    w.print_indent() << L"//" << std::hex << func << std::endl;
    w.print_indent() << (print_else ? L"else " : L"") << L"if(arguments.length == " << func->parameters.size();

    for(size_t i = 0; i != func->parameters.size(); ++i)
    {
        w << L" && ";
        argCons.constraint(func->parameters[i], i);
    }
    w << L") {" << std::endl;
    w.increaseIndent();
    std::string mangled_func_name = NameManglingContext::get(func) != nullptr ? NameManglingContext::get(func)->mangled_name : "no_mangling_context";
    w.print_indent() << project_name << L".invoke_server_function(\"" << zillians::s_to_ws(mangled_func_name) << L"\"" << std::endl;
    w.increaseIndent();
    for(size_t i = 0; i != func->parameters.size(); ++i)
    {
        Type* resolved = func->parameters[i]->getCanonicalType();
        if(PrimitiveType* primitive_type = resolved->getAsPrimitiveType()) {
            if(primitive_type->isBoolType())
            {
                w.print_indent() << L", arguments[" << i << L"]" << std::endl;
            }
            else
            {
                w.print_indent() << L", arguments[" << i << L"].constructor == " << jsType(func->parameters[i]) << L" ? arguments[" << i << L"] : new " << jsType(func->parameters[i]) << L"(arguments[" << i << L"])" << std::endl;
            }
        }
    }
    w.decreaseIndent();
    w.print_indent() << L");\n";
    w.decreaseIndent();
    w.print_indent() << L"}\n";
}

static void printFunction(const std::pair<std::wstring, std::set<FunctionDecl*>>& name_funcs, IndentWstream& w)
{
    w.print_indent() << name_funcs.first << L" : function() {" << std::endl;
    w.increaseIndent();

    ArgConstraintPolicyI* argCons = nullptr;
    if(name_funcs.second.size() == 1)
        argCons = new OnlyOneArgConstaint(w);
    else
        argCons = new OverloadingArgConstaint(w);
    for(auto i = name_funcs.second.begin(); i != name_funcs.second.end(); ++i)
    {
        printArgsConstraints(*i, *argCons, i != name_funcs.second.begin(), w);
    }
    w.print_indent() << L"else { throw \"No match function\"; }" << std::endl;

    w.decreaseIndent();
    w.print_indent() << L"}" << std::endl;
}

static void printPackageFunction(package_proxy& pkg, const bool is_root, IndentWstream& w)
{
    // open
    if(is_root)
        ;
    else
        w << pkg.pkg->id->toString() << " : {" << std::endl;

    w.increaseIndent();

    bool is_first = true;

    // sub-packages
    for(auto& v : pkg.children) {
        w.print_indent();
        if(!is_first) w.print_indent() << L"," << std::endl;
        printPackageFunction(v.second, false, w);
        is_first = false;
    }
    // functions
    for(const auto& v : pkg.funcs) {
        if(!is_first) w.print_indent() << L"," << std::endl;
        printFunction(v, w);
        is_first = false;
    }

    // close
    w.decreaseIndent();
    if(is_root)
        ;
    else
        w.print_indent() << L"}" << std::endl;
}

static void add_func(const packages_and_function_name& f, const std::set<FunctionDecl*>& func_set, size_t i, package_proxy& p)
{
    if(f.packages.size() == i)
    {
        p.funcs[f.func_name] = func_set;
    }
    else
    {
        Package* cur_package = f.packages[i];
        auto iter = p.children.find(cur_package->id->toString());
        if(iter == p.children.end())
        {
            p.children.insert({cur_package->id->toString(), package_proxy(cur_package)});
        }
        auto iter2 = p.children.find(cur_package->id->toString());
        add_func(f, func_set, i + 1, iter2->second);
    }
}

static void construct_tree(const std::map<packages_and_function_name, std::set<FunctionDecl*>>& functions, package_proxy& root)
{
    for(const auto& v : functions)
    {
        add_func(v.first, v.second, 0, root);
    }
}

template<>
void print_stub<stage::ThorStubStage::CLIENT_RPC_JS>(Tangle* tangle, stage::ThorStubStage::var_map_t& var_map)
{
    // collect @server & @export
    std::map<packages_and_function_name, std::set<FunctionDecl*>> functions;
    ASTNodeHelper::foreachApply<FunctionDecl>(*tangle, [&functions](FunctionDecl& node) {
        if(node.hasAnnotation(L"server") || node.hasAnnotation(L"export")) {
            BOOST_ASSERT(isa<SimpleIdentifier>(node.name) || isa<TemplatedIdentifier>(node.name));
            if(!node.is_member) {
                functions[packages_and_function_name(&node)].insert(&node);
            }
        }
    });

    // construct package tree
    package_proxy root(tangle->root);
    construct_tree(functions, root);

    // gen
    IndentWstream w(std::wcout);
    std::wstring server_ws_url = L"ws://localhost:9003/chat";

    // gen js base type ctor function
    w << L"function Int8   (v) { this.int8    = v; }"                             << std::endl;
    w << L"function Int16  (v) { this.int16   = v; }"                             << std::endl;
    w << L"function Int32  (v) { this.int32   = v; }"                             << std::endl;
    w << L"function Int64  (lo, hi) { this.int64_lo = lo; if(hi == undefined) hi = 0; this.int64_hi = hi; }"  << std::endl;
    w << L"function Float32(v) { this.float32 = v; }"                             << std::endl;
    w << L"function Float64(v) { this.float64 = v; }"                             << std::endl;
    w << L""                                                                      << std::endl;

    w << L"var " << project_name << L" = {"                                        << std::endl;
    w.increaseIndent();

    w.print_indent() << "keyStr : \"ABCDEFGHIJKLMNOP\" +"                                                     << std::endl;
    w.print_indent() << "         \"QRSTUVWXYZabcdef\" +"                                                     << std::endl;
    w.print_indent() << "         \"ghijklmnopqrstuv\" +"                                                     << std::endl;
    w.print_indent() << "         \"wxyz0123456789+/\" +"                                                     << std::endl;
    w.print_indent() << "         \"=\""                                                                      << std::endl;
    w.print_indent() << ","                                                                                   << std::endl;
    w.print_indent() << "decode64 : function(input) {"                                                        << std::endl;
    w.print_indent() << "    var output = \"\";"                                                              << std::endl;
    w.print_indent() << "    var chr1, chr2, chr3 = \"\";"                                                    << std::endl;
    w.print_indent() << "    var enc1, enc2, enc3, enc4 = \"\";"                                              << std::endl;
    w.print_indent() << "    var i = 0;"                                                                      << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "    // remove all characters that are not A-Z, a-z, 0-9, +, /, or ="                 << std::endl;
    w.print_indent() << "    var base64test = /[^A-Za-z0-9\\+\\/\\=]/g;"                                      << std::endl;
    w.print_indent() << "    if (base64test.exec(input)) {"                                                   << std::endl;
    w.print_indent() << "        alert(\"There were invalid base64 characters in the input text.\\n\" +"       << std::endl;
    w.print_indent() << "              \"Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\\n\" +"   << std::endl;
    w.print_indent() << "              \"Expect errors in decoding.\");"                                      << std::endl;
    w.print_indent() << "    }"                                                                               << std::endl;
    w.print_indent() << "    input = input.replace(/[^A-Za-z0-9\\+\\/\\=]/g, \"\");"                             << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "    do {"                                                                            << std::endl;
    w.print_indent() << "        enc1 = awe.keyStr.indexOf(input.charAt(i++));"                               << std::endl;
    w.print_indent() << "        enc2 = awe.keyStr.indexOf(input.charAt(i++));"                               << std::endl;
    w.print_indent() << "        enc3 = awe.keyStr.indexOf(input.charAt(i++));"                               << std::endl;
    w.print_indent() << "        enc4 = awe.keyStr.indexOf(input.charAt(i++));"                               << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "        chr1 = (enc1 << 2) | (enc2 >> 4);"                                           << std::endl;
    w.print_indent() << "        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);"                                    << std::endl;
    w.print_indent() << "        chr3 = ((enc3 & 3) << 6) | enc4;"                                            << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "        output = output + String.fromCharCode(chr1);"                                << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "        if (enc3 != 64) {"                                                           << std::endl;
    w.print_indent() << "            output = output + String.fromCharCode(chr2);"                            << std::endl;
    w.print_indent() << "        }"                                                                           << std::endl;
    w.print_indent() << "        if (enc4 != 64) {"                                                           << std::endl;
    w.print_indent() << "            output = output + String.fromCharCode(chr3);"                            << std::endl;
    w.print_indent() << "        }"                                                                           << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "        chr1 = chr2 = chr3 = \"\";"                                                  << std::endl;
    w.print_indent() << "        enc1 = enc2 = enc3 = enc4 = \"\";"                                           << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "    } while (i < input.length);"                                                     << std::endl;
    w.print_indent() << "    return output;"                                                                  << std::endl;
    w.print_indent() << "}"                                                                                   << std::endl;
    w.print_indent() << ","                                                                                   << std::endl;
    w.print_indent() << "connect : function() {"                                                              << std::endl;
    w.print_indent() << "    url = \"" << server_ws_url << L"\";"                                             << std::endl;
    w.print_indent() << "    if (\"WebSocket\" in window) {"                                                  << std::endl;
    w.print_indent() << "        awe.ws = new WebSocket(url);"                                                << std::endl;
    w.print_indent() << "    } else if (\"MozWebSocket\" in window) {"                                        << std::endl;
    w.print_indent() << "        awe.ws = new MozWebSocket(url);"                                             << std::endl;
    w.print_indent() << "    } else {"                                                                        << std::endl;
    w.print_indent() << "        console.log(\"This Browser does not support WebSockets\");"                  << std::endl;
    w.print_indent() << "        return;"                                                                     << std::endl;
    w.print_indent() << "    }"                                                                               << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "    awe.ws.onopen = function(e) {"                                                   << std::endl;
    w.print_indent() << "        console.log(\"A connection to \"+url+\" has been opened.\");"                << std::endl;
    w.print_indent() << "        "                                                                            << std::endl;
    w.print_indent() << "        $(\"#server_url\").attr(\"disabled\",true);"                                 << std::endl;
    w.print_indent() << "        $(\"#toggle_connect\").html(\"Disconnect\");"                                << std::endl;
    w.print_indent() << "    };"                                                                              << std::endl;
    w.print_indent() << ""                                                                                    << std::endl;
    w.print_indent() << "    awe.ws.onerror = function(e) {"                                                  << std::endl;
    w.print_indent() << "        console.log(\"An error occured, see console log for more details.\");"       << std::endl;
    w.print_indent() << "        console.log(e);"                                                             << std::endl;
    w.print_indent() << "    };"                                                                              << std::endl;
    w.print_indent() << "    "                                                                                << std::endl;
    w.print_indent() << "    awe.ws.onclose = function(e) {"                                                  << std::endl;
    w.print_indent() << "        console.log(\"The connection to \"+url+\" was closed.\");"                   << std::endl;
    w.print_indent() << "    };"                                                                              << std::endl;
    w.print_indent() << "    "                                                                                << std::endl;
    w.print_indent() << "    awe.ws.onmessage = function(e) {"                                                << std::endl;
    w.print_indent() << "        console.log(\"Receive message from server via WebSocket.\");"                                       << std::endl;
    w.print_indent() << "        console.log(\"e.data is \" + t);"                                                                   << std::endl;
    w.print_indent() << "        var t = awe.decode64(e.data);"                                                                      << std::endl;
    w.print_indent() << "        var o = msgpack.unpack(t);"                                                                         << std::endl;
    w.print_indent() << "        console.log(\"base64decode result is \" + o);"                                                      << std::endl;
    w.print_indent() << "             if(o.length ==  1) window[o[0]]();"                                                            << std::endl;
    w.print_indent() << "        else if(o.length ==  2) window[o[0]](o[1]);"                                                        << std::endl;
    w.print_indent() << "        else if(o.length ==  3) window[o[0]](o[1], o[2]);"                                                  << std::endl;
    w.print_indent() << "        else if(o.length ==  4) window[o[0]](o[1], o[2], o[3]);"                                            << std::endl;
    w.print_indent() << "        else if(o.length ==  5) window[o[0]](o[1], o[2], o[3], o[4]);"                                      << std::endl;
    w.print_indent() << "        else if(o.length ==  6) window[o[0]](o[1], o[2], o[3], o[4], o[5]);"                                << std::endl;
    w.print_indent() << "        else if(o.length ==  7) window[o[0]](o[1], o[2], o[3], o[4], o[5], o[6]);"                          << std::endl;
    w.print_indent() << "        else if(o.length ==  8) window[o[0]](o[1], o[2], o[3], o[4], o[5], o[6], o[7]);"                    << std::endl;
    w.print_indent() << "        else if(o.length ==  9) window[o[0]](o[1], o[2], o[3], o[4], o[5], o[6], o[7], o[8]);"              << std::endl;
    w.print_indent() << "        else if(o.length == 10) window[o[0]](o[1], o[2], o[3], o[4], o[5], o[6], o[7], o[8], o[9]);"        << std::endl;
    w.print_indent() << "        else if(o.length == 11) window[o[0]](o[1], o[2], o[3], o[4], o[5], o[6], o[7], o[8], o[9], o[10]);" << std::endl;
    w.print_indent() << "        return;"                                                                     << std::endl;
    w.print_indent() << "    };"                                                                              << std::endl;
    w.print_indent() << "}"                                                                                   << std::endl;
    w.print_indent() << ","                                                                                   << std::endl;
    w.print_indent() << L"invoke_server_function : function()"                                                << std::endl;
    w.print_indent() << L"{"                                                                                  << std::endl;
    w.print_indent() << L"    if (awe.ws === undefined || awe.ws.readyState != 1) {"                          << std::endl;
    w.print_indent() << L"        console.log(\"Websocket is not avaliable for writing\");"                   << std::endl;
    w.print_indent() << L"        return;"                                                                    << std::endl;
    w.print_indent() << L"    }"                                                                              << std::endl;
    w.print_indent() << L""                                                                                   << std::endl;
    w.print_indent() << L"    var myObject = new Object;"                                                     << std::endl;
    w.print_indent() << L"    myObject.function_name = arguments[0]"                                          << std::endl;
    w.print_indent() << L"    myObject.args = new Array();"                                                   << std::endl;
    w.print_indent() << L""                                                                                   << std::endl;
    w.print_indent() << L"    for(i = 1; i != arguments.length; ++i)"                                         << std::endl;
    w.print_indent() << L"        myObject.args.push(arguments[i]);"                                          << std::endl;
    w.print_indent() << L""                                                                                   << std::endl;
    w.print_indent() << L"    var myByteArray = msgpack.pack(myObject);"                                      << std::endl;
    w.print_indent() << L"    "                                                                               << std::endl;
    w.print_indent() << L"    var ba = new Uint8Array(myByteArray.length)"                                    << std::endl;
    w.print_indent() << L"    for(var i = 0; i != myByteArray.length; i++) {"                                 << std::endl;
    w.print_indent() << L"        ba[i] = myByteArray[i];"                                                    << std::endl;
    w.print_indent() << L"    }"                                                                              << std::endl;
    w.print_indent() << L""                                                                                   << std::endl;
    w.print_indent() << L"    console.log(String(myByteArray));"                                              << std::endl;
    w.print_indent() << L"    console.log(\"sending message len :\" + myByteArray.length);"                   << std::endl;
    w.print_indent() << L"    console.log(ba);"                                                               << std::endl;
    w.print_indent() << L"    var m = ba.buffer;"                                                             << std::endl;
    w.print_indent() << L"    awe.ws.send(m);"                                                                << std::endl;
    w.print_indent() << L"}"                                                                                  << std::endl;
    w.print_indent() << L","                                                                                  << std::endl;

    // gen function and dispatch according to args
    w.decreaseIndent();

    printPackageFunction(root, true, w);

    // close root
    w << L"} // " << project_name;
}
