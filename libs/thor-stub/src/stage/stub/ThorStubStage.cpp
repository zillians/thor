/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/stub/ThorStubStage.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/tree/ASTNode.h"
#include "utility/Foreach.h"
#include "utility/UnicodeUtil.h"
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>

#define THOR_AST_EXTENSION ".ast"

using namespace zillians::language::tree;

namespace zillians { namespace language { namespace stage {

ThorStubStage::ThorStubStage() : use_stdout(false), stub_type(UNKNOWN_STUB)
{ }

ThorStubStage::~ThorStubStage()
{ }

const char* ThorStubStage::name()
{
    return "thor_script_stub_stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ThorStubStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());
    option_desc_public->add_options()
        ("stdout,s",                                                                              "output to stdout")
        ("output-path,p",     po::value<std::string>(),                                           "stub path")
        ("stub-type,s",       po::value<std::string>(),                                           "stub type")
        ("game-name,g",       po::value<std::string>(),                                           "game name")
        ("translator-uuid,t", po::value<std::string>()->default_value("DEFAULT_TRANSLATOR_UUID"), "translator UUID")
        ("module-uuid,m",     po::value<std::string>()->default_value("DEFAULT_MODULE_UUID"),     "module UUID");
    for(auto& option : option_desc_public->options()) option_desc_private->add(option);
    option_desc_private->add_options();
    return std::make_pair(option_desc_public, option_desc_private);
}

bool ThorStubStage::parseOptions(po::variables_map& vm)
{
    if(vm.count("input"))
    {
        std::vector<std::string> inputs = vm["input"].as<std::vector<std::string>>();
        for(auto& input : inputs)
        {
            boost::filesystem::path file_path(input);
            std::string extension = file_path.extension().generic_string();
            if(extension == THOR_AST_EXTENSION)
                ast_files.push_back(input);
            else
                BOOST_ASSERT(false && "file must have \".ast\" extension");
        }
    }
    else
        BOOST_ASSERT(false && "must specify input");
    use_stdout = vm.count("stdout");
    if(vm.count("output-path"))
        output_path = vm["output-path"].as<std::string>();
    if(vm.count("game-name"))
        var_map[L"game-name"] = s_to_ws(vm["game-name"].as<std::string>());
    if(vm.count("translator-uuid"))
        var_map[L"translator-uuid"] = s_to_ws(vm["translator-uuid"].as<std::string>());
    if(vm.count("module-uuid"))
        var_map[L"module-uuid"] = s_to_ws(vm["module-uuid"].as<std::string>());
    stub_type = UNKNOWN_STUB;
    if(vm.count("stub-type"))
    {
        std::string stub_type_name = vm["stub-type"].as<std::string>();
        if(stub_type_name == "GATEWAY_GAMECOMMAND_CLIENTCOMMANDOBJECT_H")          stub_type = GATEWAY_GAMECOMMAND_CLIENTCOMMANDOBJECT_H;
        else if(stub_type_name == "GATEWAY_GAMECOMMAND_CLOUDCOMMANDOBJECT_H")      stub_type = GATEWAY_GAMECOMMAND_CLOUDCOMMANDOBJECT_H;
        else if(stub_type_name == "GATEWAY_GAMECOMMAND_GAMECOMMANDTRANSLATOR_CPP") stub_type = GATEWAY_GAMECOMMAND_GAMECOMMANDTRANSLATOR_CPP;
        else if(stub_type_name == "GATEWAY_GAMECOMMAND_GAMEMODULE_MODULE")         stub_type = GATEWAY_GAMECOMMAND_GAMEMODULE_MODULE;
        else if(stub_type_name == "CLIENT_CLIENTSTUB_H")                           stub_type = CLIENT_CLIENTSTUB_H;
        else if(stub_type_name == "CLIENT_GAMEOBJECTS_H")                          stub_type = CLIENT_GAMEOBJECTS_H;
        else if(stub_type_name == "CLIENT_GAMESERVICE_CPP")                        stub_type = CLIENT_GAMESERVICE_CPP;
        else if(stub_type_name == "CLIENT_GAMESERVICE_H")                          stub_type = CLIENT_GAMESERVICE_H;
        else if(stub_type_name == "CLIENT_RPC_JS")                                 stub_type = CLIENT_RPC_JS;
        else
            BOOST_ASSERT(false && "reaching unreachable code");
    }
    return true;
}

bool ThorStubStage::execute(bool& continue_execution)
{
    std::wofstream file;
    std::wstreambuf *prev_rdbuf;
    if(!use_stdout)
    {
        std::string filename;
        switch(stub_type)
        {
        case CLIENT_RPC_JS:     filename = get_stub_filename<CLIENT_RPC_JS>(var_map); break;
        default:
            UNUSED_ARGUMENT(continue_execution);
            BOOST_ASSERT(false && "reaching unreachable code");
        }
        if(filename.empty())
            filename = "unnamed";
        auto concat_path = [&](std::string path, std::string filename) -> std::string {
                if(!path.empty() && path[path.length()-1] != '/')
                    return path + '/' + filename;
                return "";
            };
        file.open(concat_path(output_path, filename));
        prev_rdbuf = std::wcout.rdbuf();
        std::wcout.rdbuf(file.rdbuf());
    }
    for(auto& ast : ast_files)
    {
        tree::ASTNode* node = ASTSerializationHelper::deserialize(ast);
        if(node && tree::isa<tree::Tangle>(node))
        {
            tree::Tangle* tangle = tree::cast<tree::Tangle>(node);
            switch(stub_type)
            {
            case CLIENT_RPC_JS:     print_stub<CLIENT_RPC_JS>(tangle, var_map); break;
            default:
                UNUSED_ARGUMENT(continue_execution);
                BOOST_ASSERT(false && "reaching unreachable code");
            }
        }
    }
    if(!use_stdout)
    {
        std::wcout.rdbuf(prev_rdbuf);
        file.close();
    }
    UNUSED_ARGUMENT(continue_execution);
    return true;
}

} } }
