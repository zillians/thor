/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>

#include <tuple>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/iterator_range.hpp>

#include <log4cxx/logger.h>

#include "utility/UnicodeUtil.h"

#include "language/Architecture.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/module/Tangle.h"

#include "framework/detail/GlobalDispatcherGenerator.h"
#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace framework { namespace detail {

using language::tree::ClassDecl;
using language::tree::FunctionDecl;
using language::tree::TemplatedIdentifier;
using language::tree::Tangle;

namespace {

std::vector<FunctionDecl*> get_global_init_functions(language::Architecture arch, const Tangle& tangle)
{
    using language::tree::cast;

    std::vector<FunctionDecl*> global_init_functions;

    language::tree::ASTNodeHelper::foreachApply<FunctionDecl>(
        const_cast<Tangle&>(tangle),
        [arch, &tangle, &global_init_functions](FunctionDecl& node)
        {
            if (!node.is_global_init)
                return;

            // skip all functions not in current architecture
            if ((node.arch & arch).is_zero())
                return;

            // skip static initialization functions of non-fully specialized class templates
            if (const ClassDecl* parent = cast<ClassDecl>(node.parent))
                if (const auto*const parent_tid = cast<TemplatedIdentifier>(parent->name))
                    if (!parent_tid->isFullySpecialized())
                        return;

            global_init_functions.push_back(&node);
        }
    );

    return global_init_functions;
}

}

global_dispatcher_generator::global_dispatcher_generator(const log4cxx::LoggerPtr& new_logger, language::Architecture new_arch, const detail::global_offsets_t& new_global_offsets)
    : arch(std::move(new_arch))
    , global_offsets(&new_global_offsets)
    , logger(new_logger)
{
}

bool global_dispatcher_generator::generate(const Tangle& tangle, const detail::func_mapping_t& exported_funcs)
{
    if (!load_skeleton())
    {
        LOG4CXX_ERROR(logger, L"cannot load skeleton");
        return false;
    }

    if (!set_global_offsets())
    {
        LOG4CXX_ERROR(logger, L"cannot set global offsets");
        return false;
    }

    if (!set_global_inits(tangle))
    {
        LOG4CXX_ERROR(logger, L"cannot sett global inits");
        return false;
    }

    if (!set_exported_functions(exported_funcs))
    {
        LOG4CXX_ERROR(logger, L"cannot set exported functions");
        return false;
    }

    if (!finish())
    {
        LOG4CXX_ERROR(logger, L"cannot finish generation of global dispatcher");
        return false;
    }

    return true;
}

bool global_dispatcher_generator::set_global_offsets()
{
    for (const auto& entry : *global_offsets)
    {
        const auto& id         = entry.first;
        const auto& fid_offset = std::get<detail::global_offsets_idx_function>(entry.second);
        const auto& sid_offset = std::get<detail::global_offsets_idx_symbol  >(entry.second);
        const auto& tid_offset = std::get<detail::global_offsets_idx_type    >(entry.second);

        const auto& id_name_prefix = "__" + id.toString('_');
        const auto& fid_name       = id_name_prefix + "_fid";
        const auto& sid_name       = id_name_prefix + "_sid";
        const auto& tid_name       = id_name_prefix + "_tid";

        if (!set_global_offset(fid_name, fid_offset)) { LOG4CXX_ERROR(logger, L"cannot set global offset for " << s_to_ws(fid_name)); return false; }
        if (!set_global_offset(sid_name, sid_offset)) { LOG4CXX_ERROR(logger, L"cannot set global offset for " << s_to_ws(sid_name)); return false; }
        if (!set_global_offset(tid_name, tid_offset)) { LOG4CXX_ERROR(logger, L"cannot set global offset for " << s_to_ws(tid_name)); return false; }
    }

    return true;
}

bool global_dispatcher_generator::set_global_inits(const Tangle& tangle)
{
    const auto& global_inits = get_global_init_functions(arch, tangle);

    for (const auto*const global_init : global_inits)
    {
        if (!add_global_init(*global_init))
        {
            LOG4CXX_ERROR(logger, L"cannot add global init: " << global_init->name->toString());
            return false;
        }
    }

    return true;
}

bool global_dispatcher_generator::set_exported_functions(const detail::func_mapping_t& exported_funcs)
{
    using boost::adaptors::map_keys;

    const auto& exported_funcs_by_decl = exported_funcs.right;
          auto  i                      = exported_funcs_by_decl.begin();
    const auto& iend                   = exported_funcs_by_decl.end();

    while (i != iend)
    {
        auto next_i = exported_funcs_by_decl.upper_bound(i->first);

        assert(i != next_i && "there must be at least one element!");
        assert(boost::algorithm::all_of_equal(boost::make_iterator_range(i, next_i) | map_keys, i->first) && "range is not for the same function!?");

        if (!add_exported_function({i, next_i}))
        {
            LOG4CXX_ERROR(logger, L"cannot add exported function ");
            return false;
        }

        i = std::move(next_i);
    }

    return true;
}

} } }
