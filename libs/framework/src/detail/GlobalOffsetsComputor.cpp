/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>

#include <set>
#include <string>
#include <tuple>

#include <boost/range/adaptor/map.hpp>

#include "language/Architecture.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/module/Tangle.h"

#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace language { namespace tree {

class ClassDecl;
class FunctionDecl;

} } }

namespace zillians { namespace framework { namespace detail {

namespace {

global_offsets_t generate_runtime_global_offsets(const language::tree::Tangle& tangle)
{
    using boost::adaptors::map_values;

    enum MappingType
    {
        MAPPING_FUNC,
        MAPPING_SYMB,
        MAPPING_TYPE,
    };

    const auto& func_mapping = tangle.getMappingOfFunction();
    const auto& symb_mapping = tangle.getMappingOfSymbol();
    const auto& type_mapping = tangle.getMappingOfType();

    global_offsets_t global_offsets;

    const auto& register_offset = [&global_offsets](const std::set<language::tree::Tangle::OffsetedId>& offseted_ids, int type)
    {
        for (const auto& entry : offseted_ids)
        {
            const auto& tangle_id = entry.first;
            const auto& offset    = entry.second;

            const auto& insertion_result = global_offsets.emplace(tangle_id, std::make_tuple(-1, -1, -1));
                  auto& offsets          = insertion_result.first->second;
                  auto& origin_offset    = type == MAPPING_FUNC ? std::get<0>(offsets) :
                                           type == MAPPING_SYMB ? std::get<1>(offsets) :
                                                                  std::get<2>(offsets) ;

            if (origin_offset < offset)
                origin_offset = offset;
        }
    };

    for (const auto& entry : func_mapping | map_values)
        register_offset(entry.first, MAPPING_FUNC);

    for (const auto& entry : symb_mapping | map_values)
        register_offset(entry.first, MAPPING_SYMB);

    for (const auto& entry : type_mapping | map_values)
        register_offset(entry.first, MAPPING_TYPE);

    std::int64_t last_func_offset = -1;
    std::int64_t last_symb_offset = -1;
    std::int64_t last_type_offset = -1;

    // data stored in mGlobalOffsets are the max offset for each tangle ID
    // transform it to runtime offset
    for (auto& entry : global_offsets)
    {
        const auto& next_last_func_offset = last_func_offset + 1 + std::get<0>(entry.second);
        const auto& next_last_symb_offset = last_symb_offset + 1 + std::get<1>(entry.second);
        const auto& next_last_type_offset = last_type_offset + 1 + std::get<2>(entry.second);

        std::get<0>(entry.second) = last_func_offset + 1;
        std::get<1>(entry.second) = last_symb_offset + 1;
        std::get<2>(entry.second) = last_type_offset + 1;

        last_func_offset = next_last_func_offset;
        last_symb_offset = next_last_symb_offset;
        last_type_offset = next_last_type_offset;
    }

    return std::move(global_offsets);
}

std::pair<func_mapping_t, bool> generate_function_mapping(const language::tree::Tangle& tangle, const global_offsets_t& global_offsets, language::Architecture arch, bool enable_server, bool enable_client)
{
    using export_kind_t = language::tree::ASTNodeHelper::ExportKind;

    func_mapping_t runtime_func_mapping;
    const auto&            func_mapping = tangle.getMappingOfFunction();

    for (const auto& entry : func_mapping)
    {
        const auto& func_decl    = entry.first;
        const auto& offseted_ids = entry.second.first;
        const auto& kind         = static_cast<export_kind_t>(entry.second.second);

        if (!func_decl->arch.is_any(arch))
            continue;

        const auto& is_current_export = (kind == export_kind_t::GeneralExport                 ) ||
                                        (kind == export_kind_t:: ClientExport && enable_client) ||
                                        (kind == export_kind_t:: ServerExport && enable_server)
                                        ;

        if (!is_current_export)
            continue;

        for (const auto& offseted_id : offseted_ids)
        {
            const auto& tangle_id = offseted_id.first;
            const auto& offset    = offseted_id.second;

            assert(global_offsets.count(tangle_id) > 0 && "global offset is not computed!?");

            const auto& global_offsets_pos = global_offsets.find(tangle_id);
            const auto& global_offset      = std::get<global_offsets_idx_function>(global_offsets_pos->second);
            const auto&   real_offset      = global_offset + offset;

            const auto& insertion_result = runtime_func_mapping.insert({real_offset, func_decl});
            const auto& is_inserted      = insertion_result.second;

            if (!is_inserted)
                return {{}, false};
        }
    }

    return {std::move(runtime_func_mapping), true};
}

std::pair<type_mapping_t, bool> generate_type_mapping(const language::tree::Tangle& tangle, const global_offsets_t& global_offsets, language::Architecture arch)
{
    type_mapping_t runtime_type_mapping;
    const auto&            type_mapping = tangle.getMappingOfType();

    for (const auto& entry : type_mapping)
    {
        const auto& class_decl   = entry.first;
        const auto& offseted_ids = entry.second.first;

        if (!class_decl->arch.is_any(arch))
            continue;

        for (const auto& offseted_id : offseted_ids)
        {
            const auto& tangle_id          = offseted_id.first;
            const auto& offset             = offseted_id.second;

            assert(global_offsets.count(tangle_id) > 0 && "global offset is not computed!?");

            const auto& global_offsets_pos = global_offsets.find(tangle_id);
            const auto& global_offset      = std::get<global_offsets_idx_type>(global_offsets_pos->second);
            const auto&   real_offset      = global_offset + offset;

            const auto& insertion_result = runtime_type_mapping.insert({real_offset, class_decl});
            const auto& is_inserted      = insertion_result.second;

            if (!is_inserted)
                return {{}, false};
        }
    }

    return {std::move(runtime_type_mapping), true};
}
std::pair<string_mapping_t, bool> generate_string_mapping(const language::tree::Tangle& tangle, const global_offsets_t& global_offsets)
{
    string_mapping_t string_mapping;
    const auto&      symb_mapping = tangle.getMappingOfSymbol();

    for (const auto& entry : symb_mapping)
    {
        const auto& string       = entry.first;
        const auto& offseted_ids = entry.second.first;

        for (const auto& offseted_id : offseted_ids)
        {
            const auto& tangle_id = offseted_id.first;
            const auto& offset    = offseted_id.second;

            assert(global_offsets.count(tangle_id) > 0 && "global offset is not computed!?");

            const auto& global_offsets_pos = global_offsets.find(tangle_id);
            const auto& global_offset      = std::get<global_offsets_idx_symbol>(global_offsets_pos->second);
            const auto&   real_offset      = global_offset + offset;

            const auto& insertion_result = string_mapping.insert({real_offset, string});
            const auto& is_inserted      = insertion_result.second;

            if (!is_inserted)
                return {{}, false};
        }
    }

    return {std::move(string_mapping), true};
}

}

std::tuple<
    global_offsets_t,
      func_mapping_t,
      type_mapping_t,
    string_mapping_t
> compute_global_offsets(const language::tree::Tangle& tangle, language::Architecture arch, bool enable_server, bool enable_client)
{
    bool             is_success       = false;
    auto             global_offsets   = generate_runtime_global_offsets(tangle);
      func_mapping_t   func_mapping;
      type_mapping_t   type_mapping;
    string_mapping_t string_mapping;

    const auto& failure_value = []
    {
        return std::make_tuple(
            global_offsets_t(),
              func_mapping_t(),
              type_mapping_t(),
            string_mapping_t()
        );
    };

    std::tie(func_mapping, is_success) = generate_function_mapping(tangle, global_offsets, arch, enable_server, enable_client);
    if (!is_success)
        return failure_value();

    std::tie(type_mapping, is_success) = generate_type_mapping(tangle, global_offsets, arch);
    if (!is_success)
        return failure_value();

    std::tie(string_mapping, is_success) = generate_string_mapping(tangle, global_offsets);
    if (!is_success)
        return failure_value();

    return std::make_tuple(
        std::move(global_offsets),
        std::move(  func_mapping),
        std::move(  type_mapping),
        std::move(string_mapping)
    );
}

} } }
