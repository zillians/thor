/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>

#include <exception>
#include <stdexcept>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <boost/any.hpp>

#include <log4cxx/level.h>
#include <log4cxx/logger.h>

#include "utility/MemoryUtil.h"
#include "framework/Executor.h"
#include "framework/ExecutorRemote.h"

namespace zillians { namespace framework {

executor::executor(int new_id, const std::string& logger_name)
    : verbose(false)
    , logger(log4cxx::Logger::getLogger(logger_name))
    , id(new_id)
    , stop_requested(true)
    , worker()
{
    set_verbose(false);
}

executor::~executor() = default;

std::int32_t executor::get_id() const noexcept
{
    return id;
}

bool executor::is_verbose() const noexcept
{
    return verbose;
}

void executor::set_verbose(bool new_verbose/* = true*/) noexcept
{
    verbose = new_verbose;

    logger->setLevel(verbose ? log4cxx::Level::getInfo() : log4cxx::Level::getError());
}

bool executor::set_hint(const std::string& key, const boost::any& value)
{
    assert(false && "unexpected hint");

    return false;
}

bool executor::initialize() { return true; }
bool executor::finalize  () { return true; }

bool executor::start()
{
    if (worker.joinable())
        return false;

    stop_requested = false;
    worker         = std::thread(&executor::looping, this);

    return true;
}

bool executor::stop()
{
    if (!worker.joinable())
        return false;

    std::thread dummy = std::move(worker);

    stop_requested = true;
    dummy.join();

    return true;
}

void executor::looping()
{
    try
    {
        while (!stop_requested)
            do_work();
    }
    catch (const std::runtime_error& e)
    {
        LOG4CXX_ERROR(logger, L"runtime error: " << e.what());
    }
    catch (const std::exception& e)
    {
        LOG4CXX_FATAL(logger, L"unhandled error: " << e.what());
    }
    catch (...)
    {
        LOG4CXX_FATAL(logger, L"thrown non-exception");
    }
}

executor_rt::executor_rt(int new_id, const std::string& logger_name)
    : executor(new_id, logger_name)
    , executor_remote(nullptr)
{

}

executor_rt::~executor_rt() = default;

bool executor_rt::initialize()
{
    executor_remote = make_unique<framework::executor_remote>(*this);
    return executor::initialize() && executor_remote->initialize();
}

bool executor_rt::finalize()
{
    if (!executor_remote->finalize())
        return false;

    return executor::finalize();
}

bool executor_rt::start()
{
    return executor::start() && executor_remote->start();
}

bool executor_rt::stop()
{
    return executor_remote->stop() && executor::stop();
}

int executor_rt::get_export_type() const noexcept
{
    return export_type;
}

void executor_rt::set_export_type(int new_export_type) noexcept
{
    export_type = new_export_type;
}

auto executor_rt::get_ast_path() const noexcept -> const path_t&
{
    return ast_path;
}

void executor_rt::set_ast_path(const path_t& new_ast_path)
{
    ast_path = new_ast_path;
}

auto executor_rt::get_dll_path() const noexcept -> const path_t&
{
    return dll_path;
}

void executor_rt::set_dll_path(const path_t& new_dll_path)
{
    dll_path = new_dll_path;
}

auto executor_rt::get_dep_pathes() const noexcept -> const std::vector<path_t>&
{
    return dep_pathes;
}

void executor_rt::set_dep_pathes(const std::vector<path_t>& new_dep_pathes)
{
    dep_pathes = new_dep_pathes;
}

framework::executor_remote& executor_rt::get_executor_remote() const
{
    return *executor_remote;
}

} } // namespace zillians::framework
