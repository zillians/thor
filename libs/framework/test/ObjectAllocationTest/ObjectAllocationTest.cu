/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

#include <cuda_runtime.h>
#include <stdio.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/adjacent_difference.h>
#include <thrust/scan.h>
#include <thrust/for_each.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/copy.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/binary_search.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <device_functions.h>
#include <ext/hash_set>

using namespace std;

#define CEILING(x,y) (((x) + (y) - 1) / (y))

#define MAX_OBJECT_COUNT 32*32*32*32  // 1M objects
#define TOTAL_FLAGS  (1+32+32*32+32*32*32)

const int64_t cMagicNumber = 0xFFEEFFAABBEE9933;

struct TrackerHeader
{
	uint64_t epoch;
    uint64_t type_id;
    uint64_t session_id;
    uint64_t database_id;
    uint32_t allocation_level; // TODO change to size
	uint32_t allocation_index;
	char* ptr;
	bool is_valid;
};

struct ObjectHeader
{
    int64_t magic_number;
    TrackerHeader* tracker_ptr;
};

struct Block64 {
	uint64_t t0;
	uint64_t t1;
	uint64_t t2;
	uint64_t t3;
	uint64_t t4;
	uint64_t t5;
	uint64_t t6;
	uint64_t t7;
};

struct Block128 {
	uint64_t t0;
	uint64_t t1;
	uint64_t t2;
	uint64_t t3;
	uint64_t t4;
	uint64_t t5;
	uint64_t t6;
	uint64_t t7;
	uint64_t t8;
	uint64_t t9;
	uint64_t t10;
	uint64_t t11;
	uint64_t t12;
	uint64_t t13;
	uint64_t t14;
	uint64_t t15;
};

__device__ TrackerHeader* header_blocks;
__device__ uint32_t* header_bit_flags; // size = 1 + 32 + 32*32 + 32*32*32

__global__ void configure_header_blocks(TrackerHeader* blocks, uint32_t* bit_flags)
{
	header_blocks = blocks;
	header_bit_flags = bit_flags;
}

__device__ Block64* b64_blocks;
__device__ uint32_t* b64_bit_flags; // size = 1 + 32 + 32*32 + 32*32*32

__global__ void configure_b64_blocks(Block64* blocks, uint32_t* bit_flags)
{
	b64_blocks = blocks;
	b64_bit_flags = bit_flags;
}

__device__ Block128* b128_blocks;
__device__ uint32_t* b128_bit_flags; // size = 1 + 32 + 32*32 + 32*32*32

__global__ void configure_b128_blocks(Block128* blocks, uint32_t* bit_flags)
{
	b128_blocks = blocks;
	b128_bit_flags = bit_flags;
}

#define ATOMIC_CASCADE_FLAGS 1

template<typename T>
__device__ T* allocate_block(uint32_t* bit_flags, T* blocks)
{
	// TODO rewrite in PTX assembly to further improve the performance
	uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;

	uint32_t f0 = bit_flags[0];
	uint32_t f0t;
	uint32_t msb0;

	f0t = (tid & 0x1U) ? __brev(f0) : f0;

	while(true)
	{
		asm("bfind.u32 %0, %1;\n\t" : "=r"(msb0) : "r"(f0t));

		if(msb0 == 0xFFFFFFFF)
			break;
		else
		{
			if(tid & 0x1U) msb0 = 31 - msb0;

			uint32_t offset1 = 1 + msb0;
			uint32_t f1 = bit_flags[offset1];
			uint32_t f1t;
			uint32_t msb1;

			f1t = (tid & 0x2U) ? __brev(f1) : f1;

			while(true)
			{
				asm("bfind.u32 %0, %1;\n\t" : "=r"(msb1) : "r"(f1t));

				if(msb1 == 0xFFFFFFFF)
					break;
				else
				{
					if(tid & 0x2U) msb1 = 31 - msb1;

					uint32_t offset2 = 1 + 32 + msb0 * 32 + msb1;
					uint32_t f2 = bit_flags[offset2];
					uint32_t f2t;
					uint32_t msb2;

					f2t = (tid & 0x4U) ? __brev(f2) : f2;

					while(true)
					{
						asm("bfind.u32 %0, %1;\n\t" : "=r"(msb2) : "r"(f2t));

						if(msb2 == 0xFFFFFFFF)
							break;
						else
						{
							if(tid & 0x4U) msb2 = 31 - msb2;

							uint32_t offset3 = 1 + 32 + 32 * 32 + msb0 * 32 * 32 + msb1 * 32 + msb2;
							uint32_t f3 = bit_flags[offset3];
							uint32_t f3t;
							uint32_t msb3;

							f3t = (tid & 0x8U) ? __brev(f3) : f3;

							while(true)
							{
								asm("bfind.u32 %0, %1;\n\t" : "=r"(msb3) : "r"(f3t));

								if(msb3 == 0xFFFFFFFF)
									break;
								else
								{
									if(tid & 0x8U) msb3 = 31 - msb3;

									uint32_t index = 32 * 32 * 32 * msb0 + 32 * 32 * msb1 + 32 * msb2 + msb3;
									uint32_t mask = 0xFFFFFFFF ^ (0x1U << (msb3));
									//printf("[%d] index = %d, mask = %x\n", tid, index, mask);

									uint32_t old_flag = atomicAnd(&bit_flags[offset3], mask);
									uint32_t result = old_flag & (~mask);
									if(result != 0x0U)
									{
										//printf("%d -> %d -> (%02d, %02d, %02d, %02d)\n", tid, index, msb0, msb1, msb2, msb3);
										//printf("%d: allocated msb0 = %u, offset1 = %u, f1t = %u, msb1 = %u, offset2 = %u, f2t = %u, msb2 = %u, offset3 = %u, f3t = %u, msb3 = %u, mask = %x\n", tid, msb0, offset1, f1t, msb1, offset2, f2t, msb2, offset3, f3t, msb3, mask);
										return &blocks[index];
									}
									else
									{
										//printf("%d: atomicAnd failed, old_flag = %x, result = %x\n", tid, old_flag, result);
										//f3 = bit_flags[offset3];
										f3 = old_flag;
										f3t = (tid & 0x8U) ? __brev(f3) : f3;
									}
								}
								//f3 ^= 0x1U << (msb3);
							}
#if !ATOMIC_CASCADE_FLAGS
							if(tid & 0x4U) msb2 = 31 - msb2;
#endif
						}

#if ATOMIC_CASCADE_FLAGS
						f2 ^= 0x1U << (msb2);
						uint32_t t = atomicAnd(&bit_flags[offset2], f2);
						f2 = t & f2;
						f2t = (tid & 0x4U) ? __brev(f2) : f2;
#else
						f2t ^= 0x1U << (msb2);
#endif
					}
#if !ATOMIC_CASCADE_FLAGS
					if(tid & 0x2U) msb1 = 31 - msb1;
#endif
				}

#if ATOMIC_CASCADE_FLAGS
				f1 ^= 0x1U << (msb1);
				uint32_t t = atomicAnd(&bit_flags[offset1], f1);
				f1 = t & f1;
				f1t = (tid & 0x2U) ? __brev(f1) : f1;
#else
				f1t ^= 0x1U << (msb1);
#endif
			}

#if !ATOMIC_CASCADE_FLAGS
			if(tid & 0x1U) msb0 = 31 - msb0;
#endif
		}

#if ATOMIC_CASCADE_FLAGS
		f0 ^= 0x1U << (msb0);
		uint32_t t = atomicAnd(&bit_flags[0], f0);
		f0 = t & f0;
		f0t = (tid & 0x1U) ? __brev(f0) : f0;
#else
		f0t ^= 0x1U << (msb0);
#endif
	}

	return NULL;
}

template<typename T>
__device__ void deallocate_block(uint32_t* bit_flags, T* blocks, T* block)
{
	uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;

	uint32_t index = (uint32_t)(block - &blocks[0]);
#if ATOMIC_CASCADE_FLAGS
	uint32_t local_offset3 = (index / 32);
	uint32_t local_offset2 = (local_offset3 / 32);
	uint32_t local_offset1 = (local_offset2 / 32);
	uint32_t local_offset0 = (local_offset1 / 32);

	uint32_t msb3 = index % 32;
	uint32_t msb2 = local_offset3 % 32;
	uint32_t msb1 = local_offset2 % 32;
	uint32_t msb0 = local_offset1 % 32;
	uint32_t local_mask3 = 0x1U << msb3;
	uint32_t local_mask2 = 0x1U << msb2;
	uint32_t local_mask1 = 0x1U << msb1;
	uint32_t local_mask0 = 0x1U << msb0;

	uint32_t offset0 = 0;
	uint32_t offset1 = 1 + local_offset1;
	uint32_t offset2 = 1 + 32 + local_offset2;
	uint32_t offset3 = 1 + 32 + 32 * 32 + local_offset3;
	//printf("%d: deallocated msb0 = %u, offset1 = %u, msb1 = %u, offset2 = %u, msb2 = %u, offset3 = %u, msb3 = %u, mask = %x\n", tid, msb0, offset1, msb1, offset2, msb2, offset3, msb3, local_mask3);
	atomicOr(&bit_flags[offset0], local_mask0);
	atomicOr(&bit_flags[offset1], local_mask1);
	atomicOr(&bit_flags[offset2], local_mask2);
	atomicOr(&bit_flags[offset3], local_mask3);
#else
	uint32_t bucket = index / 32;
	uint32_t mask = 0x1 << (index % 32);
	uint32_t offset3 = 1 + 32 + 32 * 32 + bucket;
	atomicOr(&bit_flags[offset3], mask);
#endif
}

__device__ char* global_heap;
__device__ uint32_t global_heap_size;
__device__ uint32_t global_heap_bucket_size;
__device__ uint32_t* global_heap_bucket;
__device__ uint32_t* global_heap_bucket_offset;
__device__ unsigned long long int global_heap_next_offset;

__global__ void configure_global_heap(char* heap, uint32_t heap_size, uint32_t* heap_bucket, uint32_t heap_bucket_size, uint32_t* heap_bucket_offset, unsigned long long int heap_next_offset = 0ULL)
{
	global_heap = heap;
	global_heap_size = heap_size;
	global_heap_bucket_size = heap_bucket_size;
	global_heap_bucket = heap_bucket;
	global_heap_bucket_offset = heap_bucket_offset;
	global_heap_next_offset = heap_next_offset;
}

__device__ char* allocate_from_global_heap(uint32_t size, TrackerHeader* header)
{
	uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;

	//size += sizeof(uint64_t);
	size = (size+127) & (~0x7f); // round-up to next multiple of 128 to avoid false cache line sharing
	//printf("%d -> allocate %d bytes\n", tid, size);
	unsigned long long int offset_mask = (0x1ULL << 32) | size;
//	printf("%d -> mask = %llu\n", tid, offset_mask);
	unsigned long long int offset = atomicAdd(&global_heap_next_offset, offset_mask);

	// TODO check overflow
	uint32_t last_allocated_size = (uint32_t)((offset << 32) >> 32);
	uint32_t last_bucket_index = (uint32_t)(offset >> 32);

//	printf("%d -> last_allocated_size = %u, last_bucket_index = %u\n", tid, last_allocated_size, last_bucket_index);
	if((unsigned long long int)last_allocated_size + (unsigned long long int)size > (unsigned long long int)global_heap_size || last_bucket_index >= global_heap_bucket_size)
	{
		// TODO ?
		printf("ERROR!!!\n");
		//atomicSub(&global_heap_next_offset, offset_mask);
		return NULL;
	}
	else
	{
		char* allocated_ptr = global_heap + last_allocated_size;
		global_heap_bucket[last_bucket_index] = size / 128;
		global_heap_bucket_offset[last_bucket_index] = last_allocated_size / 128;
		header->allocation_index = last_bucket_index;
		return allocated_ptr;
	}
}

__device__ void deallocate_from_global_heap(char* ptr)
{
	global_heap_bucket[((ObjectHeader*)ptr)->tracker_ptr->allocation_index] = 0;
}

#define HEAP_COMPACTION_THREADS_PER_BLOCK	256
#define HEAP_COMPACTION_WARP_SIZE			32
#define HEAP_COMPACTION_CHUNK_SIZE			128
#define HEAP_COMPACTION_MEMMOVE_UNIT_SIZE	4

__global__ void compact_global_heap_move_impl(
		uint32_t heap_bucket_size,
		uint32_t total_chunk_count,
		uint32_t* heap_bucket,
		uint32_t* heap_bucket_offset,
		uint32_t* heap_bucket_scanned,
		uint32_t* compaction_work_segments,
		uint32_t* compaction_work_indices,
		char* heap,
		char* heap_next
		)
{
	const uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
	const uint32_t warp_size = 32;
	const uint32_t warp_id = threadIdx.x / warp_size;
	const uint32_t warps_per_block = blockDim.x / warp_size;
	const uint32_t thread_lane = threadIdx.x & (warp_size - 1);
	uint32_t local_offset = thread_lane * HEAP_COMPACTION_MEMMOVE_UNIT_SIZE;
	char* local_heap = heap + local_offset;
	char* local_heap_next = heap_next + local_offset;

	for(int i=0;i<blockDim.x;i+=warps_per_block)
	{
		uint32_t idx = blockDim.x * blockIdx.x + i + warp_id;
		if(idx >= total_chunk_count) continue;

		uint32_t segment = compaction_work_segments[idx];
		uint32_t work_index = compaction_work_indices[idx];
		uint32_t bucket_size = heap_bucket[segment];
		uint32_t bucket_offset_dst = heap_bucket_scanned[segment] - bucket_size + work_index;
		uint32_t bucket_offset_src = heap_bucket_offset[segment] + work_index;

		char* src_ptr = local_heap + bucket_offset_src * HEAP_COMPACTION_CHUNK_SIZE;
		char* dst_ptr = local_heap_next + bucket_offset_dst * HEAP_COMPACTION_CHUNK_SIZE;

		uint32_t t = *((uint32_t*)src_ptr);
		*((uint32_t*)dst_ptr) = t;

//		if(thread_lane == 0 || thread_lane == 1)
//			printf("#%d -> %d -> move %d (%p)-> %d (%p) [idx=%d, segment=%d, work_index=%d, bucket_size=%d, offset=%d, t=%u]\n", i, tid, bucket_offset_src, src_ptr, bucket_offset_dst, dst_ptr, idx, segment, work_index, bucket_size, local_offset, t);

	}
}

__global__ void compact_global_heap_update_header_impl(
		uint32_t heap_bucket_size,
		uint32_t* heap_bucket,
		uint32_t* heap_bucket_offset,
		uint32_t* heap_bucket_scanned,
		char* heap,
		char* heap_next)
{
	const uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
	if(tid >= heap_bucket_size) return;

	uint32_t bucket_size = heap_bucket[tid];
	if(bucket_size > 0)
	{
		uint32_t chunk_offset = heap_bucket_offset[tid]; // chunk offset
		uint32_t scan_offset = heap_bucket_scanned[tid] - bucket_size;
		char* ptr = heap + chunk_offset * HEAP_COMPACTION_CHUNK_SIZE;
		if(((ObjectHeader*)ptr)->magic_number != cMagicNumber)
		{
			printf("%d: update corrupted header, ptr = %p, object header = {%lld, %p}\n", tid, ptr, ((ObjectHeader*)ptr)->magic_number, ((ObjectHeader*)ptr)->tracker_ptr);
		}
		else
		{
			TrackerHeader* header = ((ObjectHeader*)ptr)->tracker_ptr;
			char* new_ptr = heap_next + scan_offset * HEAP_COMPACTION_CHUNK_SIZE;
			//printf("%d: update header ptr from %p -> %p\n", tid, ptr, new_ptr);
			header->ptr = new_ptr;
		}
	}
}


struct is_valid_bucket
{
	__host__ __device__
	bool operator()(const uint32_t x)
	{
	  return x > 0;
	}
};

bool compact_global_heap(
		thrust::device_ptr<char> heap,
		thrust::device_ptr<char> heap_next,
		thrust::device_ptr<uint32_t> heap_bucket,
		thrust::device_ptr<uint32_t> heap_bucket_next,
		thrust::device_ptr<uint32_t> heap_bucket_offset,
		thrust::device_ptr<uint32_t> heap_bucket_offset_next,
		thrust::device_ptr<uint32_t> t_heap_bucket_scanned,
		thrust::device_ptr<uint32_t> t_compaction_work_segments,
		thrust::device_ptr<uint32_t> t_compaction_work_indices,
		unsigned long long int& heap_next_offset,
		bool& is_fully_compacted)
{
	uint32_t total_chunk_size = (heap_next_offset << 32) >> 32;
	uint32_t bucket_size = (uint32_t)(heap_next_offset >> 32);
	if(bucket_size == 0)
	{
		return true;
	}

//	for(int i=0;i<bucket_size;++i)
//	{
//		uint32_t t = heap_bucket[i];
//		printf("heap_bucket[%d] = %d\n", i, t);
//	}
//
//	for(int i=0;i<bucket_size;++i)
//	{
//		uint32_t t = heap_bucket_offset[i];
//		printf("heap_bucket_offset[%d] = %d\n", i, t);
//	}

	thrust::inclusive_scan(heap_bucket, heap_bucket + bucket_size, t_heap_bucket_scanned);

//	for(int i=0;i<bucket_size;++i)
//	{
//		uint32_t t = t_heap_bucket_scanned[i];
//		printf("heap_bucket_scanned[%d] = %d\n", i, t);
//	}

	uint32_t allocated_chunk_count = t_heap_bucket_scanned[bucket_size - 1];
//	printf("bucket_size = %u, allocated_chunk_size = %u, total_chunk_size = %u\n", bucket_size, allocated_chunk_count * 128, total_chunk_size);

	if(allocated_chunk_count * HEAP_COMPACTION_CHUNK_SIZE == total_chunk_size) // TODO the condition is wrong
	{
//		printf("fully compacted, return\n");
		is_fully_compacted = true;
		return true;
	}

	thrust::counting_iterator<int> counting_iter(0);
	thrust::upper_bound(t_heap_bucket_scanned, t_heap_bucket_scanned + bucket_size, counting_iter, counting_iter + allocated_chunk_count, t_compaction_work_segments);

	thrust::fill(t_compaction_work_indices, t_compaction_work_indices + allocated_chunk_count, 1);
	thrust::exclusive_scan_by_key(t_compaction_work_segments, t_compaction_work_segments + allocated_chunk_count, t_compaction_work_indices, t_compaction_work_indices);

//	for(int i=0;i<allocated_chunk_count;++i)
//	{
//		uint32_t t = t_compaction_work_segments[i];
//		printf("t_compaction_work_segments[%d] = %d\n", i, t);
//	}
//	for(int i=0;i<allocated_chunk_count;++i)
//	{
//		uint32_t t = t_compaction_work_indices[i];
//		printf("t_compaction_work_indices[%d] = %d\n", i, t);
//	}

	// compact heap
	if(true)
	{
		int threads = HEAP_COMPACTION_THREADS_PER_BLOCK;
		int blocks = CEILING(allocated_chunk_count, threads);

		compact_global_heap_move_impl<<<blocks,threads>>>(
				bucket_size,
				allocated_chunk_count,
				thrust::raw_pointer_cast(heap_bucket),
				thrust::raw_pointer_cast(heap_bucket_offset),
				thrust::raw_pointer_cast(t_heap_bucket_scanned),
				thrust::raw_pointer_cast(t_compaction_work_segments),
				thrust::raw_pointer_cast(t_compaction_work_indices),
				thrust::raw_pointer_cast(heap),
				thrust::raw_pointer_cast(heap_next)
				);

		cudaError_t error = cudaDeviceSynchronize();
		if(error != cudaSuccess)
		{
			printf("failed to move memory: %s\n", cudaGetErrorString(error));
			return false;
		}
	}

	// compact heap_buckets (which stores the size of each bucket)
	typedef thrust::tuple<thrust::device_ptr<uint32_t>, thrust::device_ptr<uint32_t> > iterator_tuple_t;
	typedef thrust::zip_iterator<iterator_tuple_t> iterator_t;

	iterator_t last = thrust::copy_if(
			thrust::make_zip_iterator(thrust::make_tuple(heap_bucket, t_heap_bucket_scanned)), // input first
			thrust::make_zip_iterator(thrust::make_tuple(heap_bucket + bucket_size, t_heap_bucket_scanned + bucket_size)), // input last
			heap_bucket,
			thrust::make_zip_iterator(thrust::make_tuple(heap_bucket_next, heap_bucket_offset_next)), // output
			is_valid_bucket());

	uint32_t valid_bucket_size = thrust::get<0>(last.get_iterator_tuple()) - heap_bucket_next;

	// since the bucket chunk offsets are inclusive, we need to subtract by its actual size to get the starting offset
	thrust::transform(heap_bucket_offset_next, heap_bucket_offset_next + valid_bucket_size, heap_bucket_next, heap_bucket_offset_next, thrust::minus<uint32_t>());

//	for(int i=0;i<valid_bucket_size;++i)
//	{
//		uint32_t t = heap_bucket_offset_next[i];
//		printf("heap_bucket_offset_next[%d] = %d\n", i, t);
//	}

	if(true)
	{
		int threads = HEAP_COMPACTION_THREADS_PER_BLOCK;
		int blocks = CEILING(bucket_size, threads);

		compact_global_heap_update_header_impl<<<blocks,threads>>>(
				bucket_size,
				thrust::raw_pointer_cast(heap_bucket),
				thrust::raw_pointer_cast(heap_bucket_offset),
				thrust::raw_pointer_cast(t_heap_bucket_scanned),
				thrust::raw_pointer_cast(heap),
				thrust::raw_pointer_cast(heap_next)
				);

		cudaError_t error = cudaDeviceSynchronize();
		if(error != cudaSuccess)
		{
			printf("failed to update tracker header: %s\n", cudaGetErrorString(error));
			return false;
		}
	}

	heap_next_offset = valid_bucket_size;
	heap_next_offset <<= 32;
	heap_next_offset |= (allocated_chunk_count * HEAP_COMPACTION_CHUNK_SIZE);

	return true;
}

__device__ char* allocate(int size)
{
	TrackerHeader* header = allocate_block<TrackerHeader>(header_bit_flags, header_blocks);
	if(!header) return NULL;

	size += sizeof(ObjectHeader);

	char* memory_block = NULL;
	if(size <= 64)
	{
		header->allocation_level = 0;
		memory_block = (char*)allocate_block<Block64>(b64_bit_flags, b64_blocks);
	}
	else if(size <= 128)
	{
		header->allocation_level = 1;
		memory_block = (char*)allocate_block<Block128>(b128_bit_flags, b128_blocks);
	}
	else
	{
		header->allocation_level = 2;
		memory_block = allocate_from_global_heap(size, header);
	}

	((ObjectHeader*)memory_block)->magic_number = cMagicNumber;
	((ObjectHeader*)memory_block)->tracker_ptr = header;

	header->ptr = memory_block;
	return memory_block + sizeof(ObjectHeader);
}


__device__ void deallocate(char* ptr)
{
	char* memory_block = ptr - sizeof(ObjectHeader);
	TrackerHeader* header = ((ObjectHeader*)memory_block)->tracker_ptr;

	// deallocate the physical memory
	if(header->allocation_level == 0)
	{
		deallocate_block<Block64>(b64_bit_flags, b64_blocks, (Block64*)memory_block);
	}
	else if(header->allocation_level == 1)
	{
		deallocate_block<Block128>(b128_bit_flags, b128_blocks, (Block128*)memory_block);
	}
	else
	{
		deallocate_from_global_heap(memory_block);
	}

	// deallocate the tracker header
	deallocate_block<TrackerHeader>(header_bit_flags, header_blocks, header);
}

__global__ void test_block_allocator(int count, uint32_t *parameters, char** allocated_ptr)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid >= count) return;

	uint32_t p = parameters[tid];
	if(p == 1)
	{
		if(allocated_ptr[tid] == NULL)
		{
			char* ptr = (char*)allocate_block<Block64>(b64_bit_flags, b64_blocks);
			if(ptr == NULL)
			{
				printf("%d: failed to allocated block\n", tid);
			}
			else
			{
				allocated_ptr[tid] = ptr;
			}
		}
	}
	else if(p == 2)
	{
		char* ptr = allocated_ptr[tid];
		if(tid != NULL)
		{
			deallocate_block<Block64>(b64_bit_flags, b64_blocks, (Block64*)ptr);
			allocated_ptr[tid] = NULL;
		}
	}
}

// use shared memory
__global__ void invoke(int count, uint32_t dummy_size, uint32_t *parameters, char** allocated_ptr)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid >= count) return;

	uint32_t p = parameters[tid];
	uint32_t allocation_size;
	if(p > 0)
		allocation_size = p;
	else
		allocation_size = dummy_size;

	char* ptr = allocate(allocation_size);
	allocated_ptr[tid] = ptr;
	if(!ptr)
	{
		printf("[%d] NULL tracker header\n", tid);
	}
	else
	{
		//printf("%d: allocated %p, for size = %d\n", tid, ptr, allocation_size);
		for(int i=0;i<allocation_size/4;++i)
		{
			((int*)ptr)[i] = tid;
		}

		if(p == 0)
		{
			deallocate(ptr);
		}
	}
}

__global__ void post_update(int count, uint32_t *parameters, char** allocated_ptr)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid >= count) return;

	char* ptr = allocated_ptr[tid];
	uint32_t p = parameters[tid];
	if(p > 0)
	{
		ptr -= sizeof(ObjectHeader);
		TrackerHeader* header = ((ObjectHeader*)ptr)->tracker_ptr;
		//printf("%d: post update %p -> %p\n", tid, ptr, header->ptr);
		allocated_ptr[tid] = header->ptr;
		char* relocated_ptr = header->ptr + sizeof(ObjectHeader);
		for(int i=0;i<p/4;++i)
		{
			int x = ((int*)relocated_ptr)[i];
			if( x != tid )
			{
				printf("%d: is corrupted (%d != %d) (i = %d)\n", tid, x, tid, i);
				break;
			}
		}
	}
}

int main(int argc, char** argv)
{
	uint32_t GLOBAL_HEAP_SIZE = 256*1024*1024;

	if(argc < 5)
		return -1;

	int count = atoi(argv[1]);
	int iterations = atoi(argv[2]);
	int size_per_allocation = atoi(argv[3]);
	int default_size_per_dummy_allocation = atoi(argv[4]);
	thrust::host_vector<TrackerHeader> h_header_blocks(MAX_OBJECT_COUNT);
	thrust::host_vector<uint32_t> h_header_bit_flags(TOTAL_FLAGS);

	for(int i=0;i<MAX_OBJECT_COUNT;++i)
	{
		h_header_blocks[i].ptr = NULL;
	}

	for(int i=0;i<TOTAL_FLAGS;++i)
	{
		h_header_bit_flags[i] = 0xFFFFFFFF;
	}

	thrust::device_vector<TrackerHeader> d_header_blocks = h_header_blocks;
	thrust::device_vector<uint32_t> d_header_bit_flags = h_header_bit_flags;
	thrust::device_vector<char> d_global_heap(GLOBAL_HEAP_SIZE); // 256MB
	thrust::device_vector<char> d_global_heap_next(GLOBAL_HEAP_SIZE); // 256MB

	thrust::device_ptr<char> dp_global_heap = d_global_heap.data();
	thrust::device_ptr<char> dp_global_heap_next = d_global_heap_next.data();

#define GLOBAL_HEAP_BUCKETS 32*32*32*32

	thrust::host_vector<uint32_t> h_heap_bucket(GLOBAL_HEAP_BUCKETS);
	for(int i=0;i<GLOBAL_HEAP_BUCKETS;++i)
	{
		h_heap_bucket[i] = 0;
	}

	thrust::device_vector<uint32_t> d_heap_bucket = h_heap_bucket;
	thrust::device_vector<uint32_t> d_heap_bucket_next = h_heap_bucket;
	thrust::device_vector<uint32_t> d_heap_bucket_offset(GLOBAL_HEAP_BUCKETS); // 1M
	thrust::device_vector<uint32_t> d_heap_bucket_offset_next(GLOBAL_HEAP_BUCKETS); // 1M
	thrust::device_vector<uint32_t> d_t_heap_bucket_scanned(GLOBAL_HEAP_BUCKETS); // 1M
	thrust::device_vector<uint32_t> d_t_compaction_work_segments(GLOBAL_HEAP_SIZE / HEAP_COMPACTION_CHUNK_SIZE); // 1M
	thrust::device_vector<uint32_t> d_t_compaction_work_indices(GLOBAL_HEAP_SIZE / HEAP_COMPACTION_CHUNK_SIZE); // 1M


	thrust::device_ptr<uint32_t> dp_heap_bucket = d_heap_bucket.data();
	thrust::device_ptr<uint32_t> dp_heap_bucket_next = d_heap_bucket_next.data();
	thrust::device_ptr<uint32_t> dp_heap_bucket_offset = d_heap_bucket_offset.data();
	thrust::device_ptr<uint32_t> dp_heap_bucket_offset_next = d_heap_bucket_offset_next.data();
	thrust::device_ptr<uint32_t> dp_t_heap_bucket_scanned = d_t_heap_bucket_scanned.data();
	thrust::device_ptr<uint32_t> dp_t_compaction_work_segments = d_t_compaction_work_segments.data();
	thrust::device_ptr<uint32_t> dp_t_compaction_work_indices = d_t_compaction_work_indices.data();

#define B64_BUCKETS 32*32*32*32   // 1M
#define B128_BUCKETS 32*32*32*32  // 1M

	thrust::device_vector<Block64> d_b64_blocks(B64_BUCKETS);
	thrust::device_vector<uint32_t> d_b64_bit_flags = h_header_bit_flags;

	thrust::device_vector<Block128> d_b128_blocks(B128_BUCKETS);
	thrust::device_vector<uint32_t> d_b128_bit_flags = h_header_bit_flags;

	thrust::device_ptr<TrackerHeader> dp_header_blocks = d_header_blocks.data();
	thrust::device_ptr<uint32_t> dp_header_bit_flags = d_header_bit_flags.data();

	thrust::device_ptr<Block64> dp_b64_blocks = d_b64_blocks.data();
	thrust::device_ptr<uint32_t> dp_b64_bit_flags = d_b64_bit_flags.data();

	thrust::device_ptr<Block128> dp_b128_blocks = d_b128_blocks.data();
	thrust::device_ptr<uint32_t> dp_b128_bit_flags = d_b128_bit_flags.data();

	cudaError_t error;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	configure_header_blocks<<<1,1>>>(
			thrust::raw_pointer_cast(dp_header_blocks),
			thrust::raw_pointer_cast(dp_header_bit_flags));

	configure_global_heap<<<1,1>>>(
			thrust::raw_pointer_cast(dp_global_heap),
			GLOBAL_HEAP_SIZE,
			thrust::raw_pointer_cast(dp_heap_bucket),
			GLOBAL_HEAP_BUCKETS,
			thrust::raw_pointer_cast(dp_heap_bucket_offset)
			);

	configure_b64_blocks<<<1,1>>>(
			thrust::raw_pointer_cast(dp_b64_blocks),
			thrust::raw_pointer_cast(dp_b64_bit_flags));

	configure_b128_blocks<<<1,1>>>(
			thrust::raw_pointer_cast(dp_b128_blocks),
			thrust::raw_pointer_cast(dp_b128_bit_flags));

//	thrust::host_vector<uint32_t> h_parameters(count);
//	for(int i=0;i<count;++i)
//	{
//		if(i % 4 == 0)
//		{
//			h_parameters[i] = 0;
//		}
//		else
//		{
//			h_parameters[i] = size_per_allocation;
//		}
//	}
//	thrust::host_vector<char*> h_allocated_ptr(count);
//	for(int i=0;i<count;++i)
//	{
//		h_allocated_ptr[i] = NULL;
//	}
//
//	thrust::device_vector<uint32_t> d_parameters = h_parameters;
//	thrust::device_vector<char*> d_allocated_ptr = h_allocated_ptr;
//
//	thrust::device_ptr<uint32_t> dp_parameters = d_parameters.data();
//	thrust::device_ptr<char*> dp_allocated_ptr = d_allocated_ptr.data();

	thrust::host_vector<uint32_t> h_parameters(count);
	for(int i=0;i<count;++i)
	{
		h_parameters[i] = 1;
	}

	thrust::host_vector<char*> h_allocated_ptr(count);
	for(int i=0;i<count;++i)
	{
		h_allocated_ptr[i] = NULL;
	}

	thrust::device_vector<uint32_t> d_parameters = h_parameters;
	thrust::device_vector<char*> d_allocated_ptr = h_allocated_ptr;

	thrust::device_ptr<uint32_t> dp_parameters = d_parameters.data();
	thrust::device_ptr<char*> dp_allocated_ptr = d_allocated_ptr.data();

	srand(0);

	for(int iter=0;iter<iterations;++iter)
	{
		int threads = 64;
		int blocks = CEILING(count, threads);

		cudaEventRecord(start, 0);
		test_block_allocator<<<blocks, threads>>>(count, thrust::raw_pointer_cast(dp_parameters), thrust::raw_pointer_cast(dp_allocated_ptr));
		cudaEventRecord(stop, 0);

		error = cudaEventSynchronize(stop);
		if(error != cudaSuccess)
		{
			printf("failed to launch: %s\n", cudaGetErrorString(error));
			return -1;
		}

		float elapsedTime;
		cudaEventElapsedTime(&elapsedTime , start, stop);
		printf("iteration = %d, allocation total time = %f ms\n", iter + 1, elapsedTime);

		if(true)
		{
			// check if allocated ptr is redundant
			h_allocated_ptr = d_allocated_ptr;
			__gnu_cxx::hash_set<uint64_t> set(count);
			for(int i=0;i<count;++i)
			{
				char* ptr = h_allocated_ptr[i];
				if(ptr != NULL)
				{
					if(!set.empty() && set.find((uint64_t)ptr) != set.end())
					{
						printf("Redundant ptr allocated, i = %d, ptr = %p\n", i, ptr);
					}
					else
					{
						set.insert((uint64_t)ptr);
					}
				}
			}
		}
		if(iter % 2 == 0) // free some blocks
		{
			int blocks_to_free = 0;
			for(int i=0;i<count;++i)
			{
				if(h_parameters[i] == 1 && h_allocated_ptr[i] != NULL)
				{
					if( ((float)rand() / (float)RAND_MAX) > ((float)iter/(float)iterations) )
					{
						h_parameters[i] = 2;
						++blocks_to_free;
					}
				}
			}
			printf("to free %d blocks in next iteration\n", blocks_to_free);
		}
		else // add all blocks back
		{
			int blocks_to_alloc = 0;
			for(int i=0;i<count;++i)
			{
				if(h_allocated_ptr[i] == NULL)
				{
					h_parameters[i] = 1;
					++blocks_to_alloc;
				}
			}
			printf("to alloc %d blocks in next iteration\n", blocks_to_alloc);
		}

		d_parameters = h_parameters;

//		printf("====== iteration %d ======\n", iter);
//		if(true)
//		{
//			int threads = 64;
//			int blocks = CEILING(count, threads);
//
//			cudaEventRecord(start, 0);
//			invoke<<<blocks, threads>>>(count, default_size_per_dummy_allocation, thrust::raw_pointer_cast(dp_parameters), thrust::raw_pointer_cast(dp_allocated_ptr));
//			//invoke<<<1, 16>>>(16, 200);
//			//invoke<<<1, 1>>>(1, 20000);
//			cudaEventRecord(stop, 0);
//
//			error = cudaEventSynchronize(stop);
//			if(error != cudaSuccess)
//			{
//				printf("failed to launch: %s\n", cudaGetErrorString(error));
//				return -1;
//			}
//
//			float elapsedTime;
//			cudaEventElapsedTime(&elapsedTime , start, stop);
//			printf("iteration = %d, allocation total time = %f ms\n", iter + 1, elapsedTime);
//		}
//
//		if(true)
//		{
//			unsigned long long int heap_next_offset = 0;
//			bool is_fully_compacted = false;
//
//			error = cudaMemcpyFromSymbol(&heap_next_offset, global_heap_next_offset, sizeof(global_heap_next_offset));
//			if(error != cudaSuccess)
//			{
//				printf("failed to copy from symbol: %s\n", cudaGetErrorString(error));
//				return -1;
//			}
//			//printf("heap_next_offset = %llu\n", heap_next_offset);
//
//			cudaEventRecord(start, 0);
//
//			compact_global_heap(
//					dp_global_heap,
//					dp_global_heap_next,
//					dp_heap_bucket,
//					dp_heap_bucket_next,
//					dp_heap_bucket_offset,
//					dp_heap_bucket_offset_next,
//					dp_t_heap_bucket_scanned,
//					dp_t_compaction_work_segments,
//					dp_t_compaction_work_indices,
//					heap_next_offset,
//					is_fully_compacted
//					);
//
//			if(!is_fully_compacted)
//			{
//				if(true)
//				{
//					int threads = 64;
//					int blocks = CEILING(count, threads);
//
//					post_update<<<blocks,threads>>>(count, thrust::raw_pointer_cast(dp_parameters), thrust::raw_pointer_cast(dp_allocated_ptr));
//					error = cudaDeviceSynchronize();
//					if(error != cudaSuccess)
//					{
//						printf("failed to perform post update: %s\n", cudaGetErrorString(error));
//						return -1;
//					}
//				}
//
//				thrust::swap(dp_global_heap, dp_global_heap_next);
//				thrust::swap(dp_heap_bucket, dp_heap_bucket_next);
//				thrust::swap(dp_heap_bucket_offset, dp_heap_bucket_offset_next);
//
//				configure_global_heap<<<1,1>>>(
//						thrust::raw_pointer_cast(dp_global_heap),
//						GLOBAL_HEAP_SIZE,
//						thrust::raw_pointer_cast(dp_heap_bucket),
//						GLOBAL_HEAP_BUCKETS,
//						thrust::raw_pointer_cast(dp_heap_bucket_offset),
//						heap_next_offset
//						);
//
//				error = cudaDeviceSynchronize();
//
//				if(error != cudaSuccess)
//				{
//					printf("failed to configure global heap: %s\n", cudaGetErrorString(error));
//					return -1;
//				}
//			}
//
//			uint32_t compacted_allocated_size = (heap_next_offset << 32) >> 32;
//			uint32_t compacted_allocated_buckets = heap_next_offset >> 32;
//
//			cudaEventRecord(stop, 0);
//
//			cudaEventSynchronize(stop);
//
//			float elapsedTime;
//			cudaEventElapsedTime(&elapsedTime , start, stop);
//			printf("iteration = %d, compaction total time = %f ms, total allocation = %u bytes (%u Mbytes) (in %u buckets)\n", iter + 1, elapsedTime, compacted_allocated_size, (compacted_allocated_size/1024)/1024, compacted_allocated_buckets);
//		}
	}

	return 0;
}
