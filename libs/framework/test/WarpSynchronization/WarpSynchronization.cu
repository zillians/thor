/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

#include <cuda_runtime.h>
#include <stdio.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;


//__shared__ uint32_t data[32*32];
__shared__ uint32_t dma[32*32];
__shared__ uint32_t op[32];
__shared__ uint32_t* ptr[32];

__device__ __forceinline__ void barrier_sync(const int name, const int num)
{
	asm volatile("bar.sync %0, %1;" : : "r"(name), "r"(num) : "memory" );
}

__device__ __forceinline__ void barrier_arrive(const int name, const int num)
{
	asm volatile("bar.arrive %0, %1;" : : "r"(name), "r"(num) : "memory" );
}

extern __device__ void dma_kernel(uint32_t* output);

__device__ void compute_kernel()
{
	const uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
	const uint32_t warp_size = 32;
	const uint32_t warp_id = threadIdx.x / warp_size;
	const uint32_t warps_per_block = blockDim.x / warp_size;
	const uint32_t thread_lane = threadIdx.x & (warp_size - 1);

	uint32_t local_tid = threadIdx.x - 32;

//		if(local_tid == 0)
//			completed = 32;
	op[local_tid] = 0;
	ptr[local_tid] = NULL;

	if(local_tid % 2 == 0)
	{
		// for the compute warp

		// fill the DMA op and data
		for(int i=0;i<32;++i)
			dma[local_tid * 32 + i] = local_tid;
		op[local_tid] = 1;
		ptr[local_tid] = &dma[local_tid * 32];

		// signal DMA warp to perform the DMA transfer
		barrier_arrive(1, 64);

		// wait for DMA to complete
		barrier_sync(2, 64);
	}

	op[local_tid] = 0;
	barrier_arrive(1, 64);
}

__global__ void launch(uint32_t* output)
{

//	__shared__ uint32_t completed;

	if(threadIdx.x < 32)
	{
		dma_kernel(output);
	}
	else
	{
		compute_kernel();
	}
}

#define TOTAL_ITERATIONS 1

int main()
{
	thrust::device_vector<uint32_t> h_output(32*32);
	thrust::device_vector<uint32_t> d_output = h_output;
	thrust::device_ptr<uint32_t> dp_output = d_output.data();

	for(int i=0;i<32*32;++i)
	{
		h_output[i] = 0;
	}

	for(int iter=0;iter<TOTAL_ITERATIONS;++iter)
	{
		launch<<<1, 64>>>(thrust::raw_pointer_cast(dp_output));
		cudaError_t result = cudaDeviceSynchronize();
		if(result != cudaSuccess)
		{
			std::cerr << "failed to launch" << std::endl;
			return -1;
		}
	}

	h_output = d_output;

	for(int i=0;i<32*32;++i)
	{
		std::cout << "d_output[" << i << "] = " << h_output[i] << std::endl;
	}

	return 0;
}
