/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "framework/buffer/BufferManager.h"
#include "framework/buffer/BufferNetwork.h"

#define BOOST_TEST_MODULE BufferNetworkTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

using namespace zillians::framework::buffer;

BOOST_AUTO_TEST_SUITE( BufferNetworkTest )

struct dummy_test_buffer1
{ };

BOOST_AUTO_TEST_CASE( BufferNetworkTestCase1 )
{
	// declaration for processor #0
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer1>(0, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer1>(0, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer1>(0, buffer_location_t::host_unpinned, 2);

	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer1>(0, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer1>(0, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer1>(0, buffer_location_t::host_unpinned, 2);

	// declaration for processor #1
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer1>(1, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer1>(1, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer1>(1, buffer_location_t::host_unpinned, 2);

	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer1>(1, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer1>(1, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer1>(1, buffer_location_t::host_unpinned, 2);

	// compile look-up table
	BOOST_CHECK(BufferNetwork::instance()->commit());

	// check the look-up table by querying compatible locations
	buffer_location_t::type location;
	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 0, location, 0));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 0, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 0, location, 2));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 1, location, 0));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 1, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 1, location, 2));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(1, 0, location, 0));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(1, 0, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(1, 0, location, 2));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(1, 1, location, 0));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(1, 1, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(1, 1, location, 2));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BufferNetwork::instance()->clearAll();
}

struct dummy_test_buffer2
{ };

BOOST_AUTO_TEST_CASE( BufferNetworkTestCase2 )
{
	// declaration for processor #0
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer2>(0, buffer_location_t::device_ptx_pinned_0, 2);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer2>(0, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer2>(0, buffer_location_t::host_unpinned, 0);

	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer2>(0, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer2>(0, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer2>(0, buffer_location_t::host_unpinned, 2);

	// declaration for processor #1
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer2>(1, buffer_location_t::device_ptx_pinned_0, 2);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer2>(1, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer2>(1, buffer_location_t::host_unpinned, 0);

	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer2>(1, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer2>(1, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer2>(1, buffer_location_t::host_unpinned, 2);

	// compile look-up table
	BufferNetwork::instance()->commit();

	// check the look-up table by querying compatible locations
	buffer_location_t::type location;
	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 0, location, 0));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 0, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 0, location, 2));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 1, location, 0));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 1, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 1, location, 2));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(1, 0, location, 0));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(1, 0, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(1, 0, location, 2));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(1, 1, location, 0));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(1, 1, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(1, 1, location, 2));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BufferNetwork::instance()->clearAll();
}

struct dummy_test_buffer3
{ };

BOOST_AUTO_TEST_CASE( BufferNetworkTestCase3 )
{
	// declaration for processor #0
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer3>(0, buffer_location_t::device_ptx_pinned_0, 2);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer3>(0, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer3>(0, buffer_location_t::host_unpinned, 0);

	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer3>(0, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer3>(0, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer3>(0, buffer_location_t::host_unpinned, 2);

	// declaration for processor #1
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer3>(1, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer3>(1, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareReadableLocation<dummy_test_buffer3>(1, buffer_location_t::host_unpinned, 2);

	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer3>(1, buffer_location_t::device_ptx_pinned_0, 0);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer3>(1, buffer_location_t::device_ptx_pinned_1, 1);
	BufferNetwork::instance()->declareWritableLocation<dummy_test_buffer3>(1, buffer_location_t::host_unpinned, 2);

	// compile look-up table
	BufferNetwork::instance()->commit();

	// check the look-up table by querying compatible locations
	buffer_location_t::type location;
	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(0, 0, location, 0));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(0, 0, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(0, 0, location, 2));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(0, 1, location, 0));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(0, 1, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(0, 1, location, 2));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(1, 0, location, 0));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(1, 0, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(1, 0, location, 2));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(1, 1, location, 0));
	BOOST_CHECK(location == buffer_location_t::host_unpinned);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(1, 1, location, 1));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_1);

	BOOST_CHECK(BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer3>(1, 1, location, 2));
	BOOST_CHECK(location == buffer_location_t::device_ptx_pinned_0);

	// check for non-declare buffer location should result in error
	BOOST_CHECK(!BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer1>(0, 0, location, 0));
	BOOST_CHECK(!BufferNetwork::instance()->queryCompatibleLocation<dummy_test_buffer2>(0, 0, location, 0));

	BufferNetwork::instance()->clearAll();
}

BOOST_AUTO_TEST_SUITE_END()
