/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>
#include <limits>

#include <tbb/tick_count.h>

#define BOOST_TEST_MODULE DispatchPerformanceTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "core/IntTypes.h"

using namespace zillians;
using namespace std;

BOOST_AUTO_TEST_SUITE( DispatchPerformanceTest )

struct GameObjectUpdate
{
	uint32 sessionId;
	uint32 objectId;
	float x;
	float y;
	float vx;
	float vy;
	uint64 timestamp;
	uint32 padding[8];
};

BOOST_AUTO_TEST_CASE( DispatchPerformanceTestCase1 )
{
#define TOTAL_GATEWAYS 10
#define TOTAL_UPDATES 1000000
	uint32*            mapping = new uint32[TOTAL_UPDATES];
	GameObjectUpdate*  updates = new GameObjectUpdate[TOTAL_UPDATES];

	uint32*            counters = new uint32[TOTAL_GATEWAYS];
	GameObjectUpdate** updates_to_gateways = new GameObjectUpdate*[TOTAL_GATEWAYS];

	for(int i=0;i<TOTAL_GATEWAYS;++i)
	{
		updates_to_gateways[i] = new GameObjectUpdate[TOTAL_UPDATES];
	}

	uint64 ts_current = std::rand() * std::rand();
	for(uint32 i=0;i<TOTAL_UPDATES;++i)
	{
		mapping[i] = std::rand() % TOTAL_GATEWAYS;
		updates[i].sessionId = i;
		updates[i].objectId = i;
		updates[i].x = 0.123f;
		updates[i].y = 0.456f;
		updates[i].vx = 0.135f;
		updates[i].vy = 0.246f;
		updates[i].timestamp = ts_current;
	}

	for(int i=0;i<TOTAL_GATEWAYS;++i)
	{
		counters[i] = 0;
	}

	tbb::tick_count start = tbb::tick_count::now();
	for(int i=0;i<TOTAL_UPDATES;++i)
	{
		uint32 which_gateway = mapping[i];
		uint32 current_count = counters[which_gateway];
		GameObjectUpdate* gateway_update_list = updates_to_gateways[which_gateway];
		gateway_update_list[current_count] = updates[i];
		++(counters[which_gateway]);
	}
	tbb::tick_count stop = tbb::tick_count::now();
	printf("dispatch %d updates to %d gateways takes %f ms\n", TOTAL_UPDATES, TOTAL_GATEWAYS, (stop-start).seconds() * 1000.0);

	for(int i=0;i<TOTAL_GATEWAYS;++i)
	{
		printf("counter[%d] = %d\n", i, counters[i]);
	}

	// clean up
	for(int i=0;i<TOTAL_GATEWAYS;++i)
	{
		delete[] updates_to_gateways[i];
	}
	delete[] updates_to_gateways;

	delete[] counters;

	delete[] updates;
	delete[] mapping;
}

BOOST_AUTO_TEST_SUITE_END()
