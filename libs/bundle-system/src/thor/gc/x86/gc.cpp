/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "thor/gc/gc.h"

#include "framework/proxies/X86.h"

namespace thor { namespace gc {

namespace proxies_x86 = zillians::framework::proxies::x86;

using ::thor::container::Vector;
using ::thor::lang::Object;

Vector<Object*>* __getActiveObjects()
{
    auto*const objs = Vector<Object*>::create();

    proxies_x86::gc_get_active_objects(*objs);

    return objs;
}

int64 __getGenerationId()
{
    return proxies_x86::gc_get_generation_id();
}

} }
