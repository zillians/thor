/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_CONTAINER_VECTOR_H_
#define THOR_CONTAINER_VECTOR_H_

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/GarbageCollectable.h"

#include <functional>

namespace thor { namespace container {

template < typename E >
class Vector;

template < typename E >
class VectorIterator : public thor::lang::Object, public thor::lang::Cloneable
{
    static_assert(thor::lang::is_non_void_thor_type<E>::value, "invalid template type argument");

public:
    using value_type       = E;
    using vector_iter_type = value_type*;

    VectorIterator(Vector<value_type>* c)
    : target( c )
    {
        current = target->_data;
        end     = target->_data + target->_size;
    }

    virtual ~VectorIterator() { }

    static VectorIterator<value_type>* create(Vector<value_type>* c);

    virtual VectorIterator* clone()
    {
        VectorIterator* ret = create(target);
        ret->current = current;
        return ret;
    }

    bool hasNext()
    {
        return current != end;
    }

    value_type next()
    {
        return *current++;
    }

private:
    Vector<value_type>* target;
    vector_iter_type    current; 
    vector_iter_type    end;
} ;

template < typename E >
class Vector : public thor::lang::Object, public thor::lang::GarbageCollectable, public thor::lang::Cloneable
{
    static_assert(thor::lang::is_non_void_thor_type<E>::value, "invalid template type argument");

public:
    using value_type = E;
    friend class VectorIterator<value_type>;

    Vector()
        : _data(nullptr)
        , _size(0)
        , _capacity(0)
    {
    }

    Vector(int64 init_size)
        : _data(NULL)
        , _size(init_size)
        , _capacity(init_size)
    {
        _data = new value_type[init_size];
    }

    virtual ~Vector()
    {
        delete _data;
    }

    static Vector* create();

    static Vector* create(int64 init_size);

    virtual Vector* clone()
    {
        Vector* ret = create(_size);
        for(size_t i = 0; i < _size; ++i)
        {
            ret->_data[i] = _data[i];
        }
        return ret;
    }

    void pushBack(value_type v)
    {
        if(_capacity == 0 || _size + 1 > _capacity)
        {
            extendCapacity();
        }
        _data[_size] = v;
        ++_size;
    }

    value_type get(int64 index)
    {
        return _data[index];
    }

    void set(int64 index, value_type v)
    {
        _data[index] = v;
    }

    void popBack()
    {
        if(_size == 0) return;
        --_size;
    }

    VectorIterator<value_type>* iterator()
    {
        return VectorIterator<value_type>::create(this);
    }

    int64 size()
    {
        return _size;
    }

    int64 capacity()
    {
        return _capacity;
    }

private:
    virtual void getContainedObjects(thor::lang::CollectableObject* collect_object)
    {
        collect_object->add(_data, _data + _size);
    }

    void extendCapacity()
    {
        if(_capacity == 0)
        {
            _capacity = 3;
        }
        else
        {
            _capacity *= 2;
        }
        value_type* new_data = new value_type[_capacity];
        for(size_t i = 0; i < _size; ++i)
        {
            new_data[i] = _data[i];
        }
        if(_data != nullptr)
        {
            delete _data;
        }
        _data = new_data;
    }

    value_type* _data;
    int64 _size;
    int64 _capacity;
} ;

} }

#endif /* THOR_CONTAINER_VECTOR_H_ */
