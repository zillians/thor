/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef THOR_CONTAINER_COMMON_H
#define THOR_CONTAINER_COMMON_H

#include <functional>

#include "thor/Thor.h"
#include "thor/lang/Language.h"

namespace thor { namespace container {

namespace detail {

    template < typename T >
    struct default_value
    {
        static_assert(thor::lang::is_builtin<T>::value, "invalid template type argument");

        static constexpr T value = 0;
    };

    template < typename T >
    struct default_value<T*>
    {
        using type = T*;

        static_assert(thor::lang::is_class<type>::value, "invalid template type argument");

        static constexpr type value = nullptr;
    };

    template < typename Key >
    struct object_equal_to
    {
        bool operator()(const Key& lhs, const Key& rhs) const
        {
            if(lhs == nullptr && rhs == nullptr)
                return true;
            else if(lhs == nullptr || rhs == nullptr)
                return false;
    
            return lhs->isEqual(rhs);
        }
    };
    
    template < typename Key >
    struct object_less
    {
        bool operator()(const Key& lhs, const Key& rhs) const
        {
            if(lhs == nullptr && rhs == nullptr)
            {
                return true;
            }
            else if(lhs == nullptr || rhs == nullptr)
            {
                return false;
            }
    
            return lhs->isLessThan(rhs);
        }
    };

    template < typename Key >
    struct object_hash
    {
        size_t operator()(const Key& lhs) const
        {
            if(lhs == nullptr)
                return 0;
    
            return lhs->hash();
        }
    };

    // type alias    
    template < typename Key >
    using KeyLessCmp = typename std::conditional<
        thor::lang::is_builtin<Key>::value
        , std::less<Key>
        , object_less<Key>
    >::type;

    template< typename T >
    using KeyIsEqual = typename std::conditional<
        thor::lang::is_builtin<T>::value
        , std::equal_to<T>
        , object_equal_to<T>
    >::type;
    
    template< typename T >
    using KeyHash = typename std::conditional<
        thor::lang::is_builtin<T>::value
        , std::hash<T>
        , object_hash<T>
    >::type;

} // namespace detail

} } // namespace thor::container

#endif /* THOR_CONTAINER_COMMON_H */
