/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_CONTAINER_HASH_MAP_H_
#define THOR_CONTAINER_HASH_MAP_H_

#include <unordered_map>
#include <vector>
#include <functional>

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/GarbageCollectable.h"
#include "thor/container/Common.h"

namespace thor { namespace container {

template < typename Key, typename Value >
struct HashMapEntry : public thor::lang::Object
{
    static_assert(thor::lang::is_non_void_thor_type<Key>::value  , "invalid template type argument");
    static_assert(thor::lang::is_non_void_thor_type<Value>::value, "invalid template type argument");

    using key_type   = Key;
    using value_type = Value;

    HashMapEntry(HashMapEntry<Key, Value>* e) : key(e->key), value(e->value) {}
    HashMapEntry(key_type k, value_type v) : key(k), value(v) {}

    static HashMapEntry<Key, Value>* create(key_type k, value_type v);

    key_type   key;
    value_type value;
} ;

template < typename Key, typename Value >
class HashMap;

template < typename Key, typename Value >
class HashMapIterator : public thor::lang::Object, public thor::lang::Cloneable
{
public:
    static_assert(thor::lang::is_non_void_thor_type<Key>::value  , "invalid template type argument");
    static_assert(thor::lang::is_non_void_thor_type<Value>::value, "invalid template type argument");

    using key_type                 = Key;
    using value_type               = Value;
    using entry_type               = HashMapEntry< key_type, value_type >;
    using map_type                 = HashMap< key_type, value_type >;

private:
    using internal_iterator_type   = typename map_type::internal_iterator_type;

public:
    HashMapIterator(map_type* c)
    {
        target = c;
        _cur = new internal_iterator_type;
        _end = new internal_iterator_type;

        *_cur = c->begin();
        *_end = c->end();
    }

    virtual ~HashMapIterator()
    {
        delete _cur;
        delete _end;
    }

    static HashMapIterator<Key, Value>* create(map_type* c);

    virtual HashMapIterator* clone()
    {
        HashMapIterator* ret = create(target);
        *(ret->_cur) = *_cur;
        return ret;
    }

    bool hasNext()
    {
        return *_cur != *_end;
    }

    entry_type* next()
    {
        auto previous = (*_cur)++;
        return entry_type::create(
            previous->first, previous->second
        );
    }

private:
    map_type*               target;
    internal_iterator_type* _cur;
    internal_iterator_type* _end;
};


template < typename Key, typename Value >
class HashMap : public thor::lang::Object, public thor::lang::GarbageCollectable, public thor::lang::Cloneable
{
public:
    static_assert(thor::lang::is_non_void_thor_type<Key>::value  , "invalid template type argument");
    static_assert(thor::lang::is_non_void_thor_type<Value>::value, "invalid template type argument");

    using key_type   = Key;
    using value_type = Value;

private:
    using iterator_type          = HashMapIterator< key_type, value_type >;
    using internal_map_type      = std::unordered_map<
                                       key_type
                                       , value_type
                                       , detail::KeyHash<Key>
                                       , detail::KeyIsEqual<Key>
                                   >;
    using internal_iterator_type = typename internal_map_type::iterator;

    friend class HashMapIterator< key_type, value_type >;

public:

    HashMap()
    {
        _map = new internal_map_type;
    }

    virtual ~HashMap()
    {
        if(_map) delete _map;
    }

    static HashMap* create();

    virtual HashMap* clone()
    {
        HashMap* ret = create();
        *(ret->_map) = *_map;
        return ret;
    }

    void set(key_type k, value_type v)
    {
        auto insert_result = _map->insert(std::make_pair(k, v));

        if(!insert_result.second)
            (insert_result.first)->second = v;
    }

    void remove(key_type k)
    {
        _map->erase(k);
    }

    iterator_type* iterator()
    {
        return iterator_type::create(this);
    }

    // info
    bool has(key_type k)
    {
        return _map->find(k) != _map->end();
    }

    bool empty()
    {
        return _map->empty();
    }

    int64 size()
    {
        return _map->size();
    }

private:
    internal_iterator_type begin()
    {
        return _map->begin();
    }

    internal_iterator_type end()
    {
        return _map->end();
    }

    value_type getImpl(key_type k)
    {
        return _map->find(k)->second;
    }

    value_type getDefaultValue()
    {
        return detail::default_value<value_type>::value;
    }

    virtual void getContainedObjects(thor::lang::CollectableObject* collect_object)
    {
        // Skip if the value_type is primitive
        for (auto it = _map->begin(); it != _map->end(); it++)
        {
            collect_object->add(it->first );
            collect_object->add(it->second);
        }
    }

private:
    internal_map_type* _map;
};

} }

#endif /* THOR_CONTAINER_HASH_MAP_H_ */
