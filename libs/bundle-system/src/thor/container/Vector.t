/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
 
import . = thor.unmanaged;
import . = thor.lang;
import . = thor.util;

/**
 * @brief Helper class which supports navigating Vector elements in foreach loop.
 *
 * In foreach loop, thor will call @c Vector 's  member function
 * @c iterator() to get a @c VectorIterator instance(shorten as @b it later).
 * Then thor calls @b it 's member function @c hasNext() to determine
 * to terminate executing loop or not. In each iteration, thor gets an
 * element by calling @b it 's member function @c next().
 *
 * @remark The behavior is undefined if the pointed @c Vector has been modified
 *         after iterator was returned.
 * @see Vector
 */
@cpu
@native { include = "thor/container/Vector.h" }
class VectorIterator<E> extends Object implements Cloneable
{
    /**
     * @brief constructor creates an iterator which points to a vector.
     *
     * @param c The target @c Vector this newly created iterator will point to.
     */
    @native
    public function new(c : Vector<E>) : void;

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete() : void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create iterators in C++.
     *
     * @param c The target vector to navigate.
     * @return An iterator instance which can work on @b c.
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(c : Vector<E>) : VectorIterator<E>
    {
        return new VectorIterator<E>(c);
    }
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * @brief Get a copy of @c this.
     *
     * Get an iterator which has same status as @c this, both of them
     * point to the same @c Vector and same element in it.
     *
     * @return A copy of @c this.
     * @remark This function performs @b shallow copy. That means
     *         the returned iterator will share the same vector with
     *         @c this.
     */
    @native
    public virtual function clone() : VectorIterator<E>;

    /**
     * @brief Tell if there is any element not visited yet in
     *        the target @c Vector @c this points to.
     *
     * @return A boolean value indicates visiting status.
     * @retval true there is another element not visited yet.
     * @retval false all elements are visited.
     */
    @native
    public function hasNext() : bool;

    /**
     * @brief Get the next unvisited element in @c Vector.
     *
     * @return next unvisited element.
     */
    @native
    public function next() : E;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var target  : Vector<E>;
    private var current : ptr_<int8>;
    private var end     : ptr_<int8>;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */
}

/**
 * @brief An array-like class which can contain components that can
 *        be accessed using an integer index.
 *
 * @c Vector is dynamic-sized array. Users can put new components
 * into it in runtime. Each @c Vector tries to optimize storage
 * management by maintaining a @c capacity(). The capacity is always at
 * least as large as the @c Vector @ size(). It is usually larger
 * because it prepares more rooms for upcoming added elements,
 * to avoid frequently request for memory from system. When addition
 * is made and there is no more spaces to keep the new element,
 * @c Vector doubles its capacity in order to allow increasing
 * size.
 */
@cpu
@native { include="thor/container/Vector.h" }
class Vector<E> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief constructor creates @c Vector with empty storage.
     *
     * This constructor creates a zero-capacity and zero-sized @c Vector
     */
    @native
    public function new() : void;

    /**
     * @brief constructor creates @c Vector by given size.
     *
     * @param init_size create a @c Vector with size = @b init_size
     *                  and capacity = @b init_size. All elements are
     *                  uninitialized.
     */
    @native
    public function new(init_size : int64) : void;

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete() : void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create empty vectors in C++.
     *
     * @return An empty vector instance
     * @remark This function is current workaround to create object through C++.
     */
    public static function create() : Vector<E>
    {
        return new Vector<E>();
    }

    /**
     * @brief Provides functionality to create vectors with specific size in C++.
     *
     * @return A vector instance with size = @c init_size
     *         and capacity = @c init_size
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(init_size : int64) : Vector<E>
    {
        return new Vector<E>(init_size);
    }
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * @brief Create a copy of @c this.
     *
     * @return a newly created @c Vector instance with same
     *         size, same capacity and same element values.
     * @remark This function performs @b shallow copy. That means
     *         only elements' values are copied. For non-primitive
     *         types, elements in the produced @c Vector share same
     *         objects with @c this.
     */
    @native
    public virtual function clone() : Vector<E>;

    /**
     * @brief Add a new value into @c this.
     *
     * Resize @c this to keep the pushed new value.
     *
     * @param v The new value will be placed into @c this.
     * @remark This member function causes memory reallocation
     *         while capacity is equal to the size. Any
     *         reallocation causes already-created iterators
     *         invalidated.
     * @see iterator
     */
    @native
    public function pushBack(v : E) : void;

    /**
     * @brief remove the last element.
     *
     * Remove the last element (at index [0, N), N is the size
     * of this) if @c this is not empty. Otherwise, @c this status
     * is unchanged.
     */
    @native
    public function popBack() : void;

    /**
     * @brief Assign a new value to the element at specified index.
     *
     * @param index Index of the element.
     * @param v     New value to overwrite the element.
     * @remark if the @c index is greater than or equal to
     *         @c size() or negative, function behavior is undefined.
     * @see size
     */
    @native
    public function set(index : int64, v : E) : void;

    /**
     * @brief Get element at specified index.
     *
     * @param index index of the element to return.
     * @return The value of requested element.
     * @remark if the @c index is greater than or equal to
     *         @c size() or negative, function behavior is undefined.
     * @see size
     */
    @native
    public function get(index : int64) : E;

    /**
     * @brief Returns an iterator.
     *
     * @return An iterator which points to @c this.
     * @see VectorIterator
     */
    @native
    public function iterator() : VectorIterator<E>;

    /**
     * @brief Returns the number of elements.
     *
     * @return The number of elements in @c this.
     */
    @native
    public function size() : int64;

    /**
     * @brief Returns the number of elements that can be held in currently allocated storage.
     *
     * @return Capacity of the currently allocated storage.
     */
    @native
    public function capacity() : int64;

    /**
     * Member function will be called by garbage collector to
     * keep track objects which are still referenced in native
     * implementations.
     *
     * @param o An object which collects still-referenced objects
     * @see CollectableObject
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    var _p        : ptr_<E>;
    var _size     : int64;
    var _capacity : int64;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */
}
