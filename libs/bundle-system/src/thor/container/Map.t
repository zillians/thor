/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
 
import . = thor.unmanaged;
import . = thor.lang;

/**
 * @brief A class wraps key and associated value together.
 *
 * Usually this class is instantiated by @c MapIterator to
 * put key and value as a pair in loop iterations. An entry
 * object should be used only for the duration of the
 * iteration.
 *
 * @remark The behavior is undefined if backing map has been
 *         modified after the entry was returned by the iterator.
 * @see MapIterator
 * @see Map
 */
@cpu
@native { include = "thor/container/Map.h" }
class MapEntry<Key, Value> extends Object
{
    /**
     * @brief construct a copy from given map entry.
     *
     * @param e The source entry to copy from.
     */
	@native
    public function new(e : MapEntry<Key, Value>): void;

    /**
     * @brief Wrap key and value together.
     *
     * @param k The map key.
     * @param v The associated value.
     */
	@native
    public function new(k : Key, v : Value): void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create entries in C++.
     *
     * @param k The map key.
     * @param v The associated value.
     * @return An entry instance which has given arguments.
     * @remark This function is current workaround to create object through C++.
     */
	public static function create(k : Key, v : Value): MapEntry<Key, Value>
	{
		return new MapEntry<Key, Value>(k, v);
	}
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * Wrapped map key
     */
    public var key   : Key;
    /**
     * Wrapped map value
     */
    public var value : Value;
}

/**
 * @brief Helper class which supports navigating key and values
 *        and put them together by using @c MapEntry as view.
 *
 * In foreach loop, Thor will call @c Map 's  member function
 * @c iterator() to get a @c MapIterator instance(shorten as @b it later).
 * Then Thor calls @b it 's member function @c hasNext() to determine
 * to terminate executing loop or not. In each iteration, Thor gets an
 * key-value pair by calling @b it 's member function @c next().
 *
 * @remark The behavior is undefined if backing map has been
 *         modified after the iterator was returned.
 * @see Map
 * @see MapEntry
 */
@cpu
@native { include = "thor/container/Map.h" }
class MapIterator<Key, Value> extends Object implements Cloneable
{
    /**
     * @brief constructor creates an iterator which points to a map.
     *
     * @param c The target @c Map this newly created iterator will point to.
     */
    @native
    public function new(c : Map<Key, Value>): void;

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete(): void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create iterators in C++.
     *
     * @param c The target map to navigate.
     * @return An iterator instance which can work on @b c.
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(c : Map<Key, Value>): MapIterator<Key, Value>
    {
        return new MapIterator<Key, Value>(c);
    }
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * @brief Get a copy of @c this.
     *
     * Get an iterator which has same status as @c this, both of them
     * point to the same @c Map and same element in it.
     *
     * @return A copy of @c this.
     * @remark This function performs @b shallow copy. That means
     *         the returned iterator will share the same map with
     *         @c this.
     */
    @native
    public virtual function clone(): MapIterator<Key, Value>;

    /**
     * @brief Tell if there is any entry not visited yet in
     *        the target @c Map @c this points to.
     *
     * @return A boolean value indicates visiting status.
     * @retval true if there is another key not visited yet.
     * @retval false means all key are visited.
     */
    @native
    public function hasNext(): bool;

    /**
     * @brief Get the next unvisited key and its value together as a pair in @c Map.
     *
     * @return A @c MapEntry instance which wraps unvisited key and
     *         associated value.
     */
    @native
    public function next(): MapEntry<Key, Value>;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    var target : Map<Key, Value>;
    var _cur   : ptr_<int8>;
    var _end   : ptr_<int8>;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */
}

/**
 * @brief A sorted associative container which maps a unique
 *        key to a value.
 *
 * Map is an associative container which stores immutable type
 * as keys. Map are indexed by keys and keep elements the keys
 * map to. Keys are sorted in map, to be used as a key, user
 * defined types should provide a public member function @b
 * named @c isLessThan for comparision. For example, a type @c
 * Foo is written as: @n
 *
 * @code
 * class Foo
 * {
 *     public function isLessThan(other: Foo): bool
 *     {
 *         return this.value < other.value;
 *     }
 *
 *     private var value: int32;
 * }
 * @endcode
 *
 * @c Foo should be convertible to @c isLessThan() 's parameter,
 * and the @c isLessThan() return type should be convertible
 * to bool. No limits on the value types.
 */
@cpu
@native { include = "thor/container/Map.h" }
class Map<Key, Value> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief constructor creates @c Map with no entries.
     */
    @native
    public function new(): void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create empty maps in C++.
     *
     * @return An empty map instance.
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(): Map<Key, Value>
    {
        return new Map<Key, Value>();
    }
    /**
     * @endcond THOR_WORKAROUNDS
     */

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete(): void;

    /**
     * @brief Create a copy of @c this.
     *
     * @return a newly created @c Map instance with same
     *         keys and values.
     * @remark This function performs @b shallow copy. That means
     *         only elements' values are copied. For non-primitive
     *         types, elements in the produced @c Map share same
     *         objects with @c this.
     */
    @native
    public virtual function clone(): Map<Key, Value>;

    /**
     * @brief Get a mapped value by a given key.
     *
     * @return A value-result pair, first value is the mapped
     *         and second boolean value indicates if the key
     *         was found in map or not.
     * @retval (any, true) Found key in map, and first
     *         is its value.
     * @retval (any, false) Can not find key in map,
     *         first value is meaningless.
     * @remark @c Map guarantees the time complexity is log(N)
     *         on finding keys in map. (N is number of total
     *         keys)
     */
    public function get(k : Key): (Value, bool)
    {
        if(has(k))
            return getImpl(k), true;
        else
            return getDefaultValue(), false;
    }

    /**
     * @brief Add a new key and its value.
     *
     * Add a new key into map and set its value. If the key is
     * already in map, rewrite the mapped value by passed one.
     *
     * @param k the key we want to add to map.
     * @param v the value we want to map by key @b k.
     * @remark @c Map guarantees the time complexity is log(N)
     *         on finding keys in map. (N is number of total
     *         keys)
     */
    @native
    public function set(k : Key, v : Value): void;

    /**
     * @brief Remove a key from map.
     *
     * Remove a key and the value it maps from map. If the key
     * is not find in map, do nothing.
     *
     * @param k the key we want to remove from map.
     * @remark @c Map guarantees the time complexity is log(N)
     *         on finding keys in map. (N is number of total
     *         keys)
     */
    @native
    public function remove(k : Key): void;

    /**
     * @brief Returns an iterator.
     *
     * @return An iterator which points to @c this.
     * @see MapIterator
     */
    @native
    public function iterator(): MapIterator<Key, Value>;

    /**
     * @brief Tell if the given key is in map.
     *
     * @param k The key we want to find.
     * @return Boolean value indicates if key is in map.
     * @retval true If the key is exist in map.
     * @retval false If the key is not in map.
     * @remark @c Map guarantees the time complexity is log(N)
     *         on finding keys in map. (N is number of total
     *         keys)
     */
    @native
    public function has(k : Key): bool;

    /**
     * @brief Tell if there is no keys in map.
     *
     * What this function does is equivalent to call: @n
     * @code
     * return this.size() == 0;
     * @endcode
     * @return Boolean value indicates if @c this has no
     *         keys.
     * @retval true At least one key is in map.
     * @retval false No keys stay in @c this.
     * @see size()
     */
    @native
    public function empty(): bool;

    /**
     * @brief Returns the numbers of keys in @c this.
     *
     * @return The number of keys in @c this.
     */
    @native
    public function size(): int64;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    @native
    private function getImpl(k : Key): Value;

    @native
    private function getDefaultValue(): Value;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */

    /**
     * Member function will be called by garbage collector to
     * keep track objects which are still referenced in native
     * implementations.
     *
     * @param o An object which collects still-referenced objects
     * @see CollectableObject
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject): void;

    // member (just a pointer here, so no matter the instantiation type)
    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    var _p : ptr_<Key>;
    /**
     * @endcond THOR_PRIVATE_IMPLEMENTATION
     */
}
