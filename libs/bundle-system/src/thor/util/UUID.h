/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_UTIL_UUID_H_
#define THOR_UTIL_UUID_H_

#include <array>

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/String.h"

namespace boost { namespace uuids {

struct uuid;

} }  // namespace boost::uuids

namespace thor { namespace util {

class UUID : public thor::lang::Object, public thor::lang::Cloneable
{
private:
             UUID() noexcept;
    virtual ~UUID() noexcept;

public:
    UUID* clone() noexcept override;
    int64 hash() noexcept override;

    bool  isNil() noexcept;
    void  setNil() noexcept;

    bool                isEqual(UUID* rhs) noexcept;
    thor::lang::String* toString() noexcept;

    static UUID* nil      () noexcept;
    static UUID* random   () noexcept;
    static UUID* parse    (thor::lang::String* text) noexcept;

    ////////////////////////////////////////////////////
    // C++ native interface, do not export to Thor
    static UUID*              fromId(const boost::uuids::uuid&  id) noexcept;
    static UUID*              fromId(      boost::uuids::uuid&& id) noexcept;
          boost::uuids::uuid&  getId()       noexcept;
    const boost::uuids::uuid&  getId() const noexcept;
    void                       setId(const boost::uuids::uuid&  id) noexcept;
    void                       setId(      boost::uuids::uuid&& id) noexcept;
    ////////////////////////////////////////////////////

public:
    static UUID* __create() noexcept;

private:
    std::array<int8, 16> data;
};

} }

#endif /* THOR_UTIL_UUID_H_ */
