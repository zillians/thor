/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import . = thor.unmanaged;
import . = thor.lang;

/**
 * @brief Class manipulates characters with stream like interface.
 *
 * StringBuilder provides the following functionalities:
 * - reverse - Reverse the characters order.
 * - append - Append string representation of values (for example, integer and floating point values)
 * - insert - Similar to append, except inserting string representation at specified position.
 * - toString - Dump current string representation to thor.lang.String.
 * .
 *
 * @see thor.lang.String
 * @see thor.lang.MutableString
 */
@cpu
@native
class StringBuilder extends Object
{
    /**
     * @brief Default constructor creates string representation which is the same as "".
     *
     * @see new(String)
     */
    @native
    public function new():void;

    /**
     * @brief Constructor creates string representation which is the same as @a s.
     *
     * @arg s String to be copied as initial string representation.
     *
     * @see new()
     */
    @native
    public function new(s:String):void;

    /**
     * @brief Destructor releases internal resources.
     */
    @native
    public function delete():void;

    /**
     * @brief Count the number of characters stored in string representation.
     *
     * @return Number of characters.
     *
     * @remark The return value will not be negative integer.
     */
    @native
    public function length():int32;

    /**
     * @brief Reverse the order of characters.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.reverse(); // builder represents "cba" after this
     * @endcode
     */
    @native
    public function reverse():void;

    /**
     * @brief Dump string representation as new thor.lang.String instance.
     *
     * For example:
     * @code
     * (new StringBuilder("abc")).toString(); // result is "abc"
     * @endcode
     *
     * @return The new thor.lang.String instance with the copied characters.
     *
     * @see toString(int32, int32)
     */
    @native
    public function toString():String;

    /**
     * @brief Dump string representation in [start, end) as new thor.lang.String instance.
     *
     * For example:
     * @code
     * (new StringBuilder("abc")).toString(1, 2); // result is "b"
     * @endcode
     *
     * @arg start The beginning of range to be copied.
     * @arg end   The post ending of range to be copied.
     *
     * @return The new thor.lang.String instance with the copied characters.
     * @retval non-null New string instance contains all characters in
     *                  [start, end).
     * @retval null     The specified range is invalid, for example, @a end < @a
     *                  < @a start.
     *
     * @remark The value of @a end could larger than the value returned by @a
     *         length(). In such case, the result will contain all characters
     *         start from @a start.
     *
     * @see toString(int32, int32)
     */
    @native
    public function toString(start:int32, end:int32):String;

    /**
     * @brief Append the given string.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append("abc "); // After this line, builder will be "abc "
     * builder.append("def "); // After this line, builder will be "abc def "
     * @endcode
     *
     * @arg other Another string to be appended.
     *
     * @see append(bool)
     * @see append(int8)
     * @see append(int16)
     * @see append(int32)
     * @see append(int64)
     * @see append(float32)
     * @see append(float64)
     */
    @native
    public function append(v:String):void;

    /**
     * @brief Append @c true or @c false.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(true ); // After this line, builder will be "true"
     * builder.append(false); // After this line, builder will be "truefalse"
     * @endcode
     *
     * @arg other Append @c true if value is @a true; @c false otherwise.
     *
     * @see append(int8)
     * @see append(int16)
     * @see append(int32)
     * @see append(int64)
     * @see append(float32)
     * @see append(float64)
     * @see append(String)
     */
    @native
    public function append(v:bool):void;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(cast<int8>(12)); // "12"
     * builder.append(cast<int8>(21)); // "1221"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @see append(bool)
     * @see append(int16)
     * @see append(int32)
     * @see append(int64)
     * @see append(float32)
     * @see append(float64)
     * @see append(String)
     */
    @native
    public function append(v:int8):void;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(cast<int16>(1234)); // "1234"
     * builder.append(cast<int16>(7777)); // "12347777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @see append(bool)
     * @see append(int8)
     * @see append(int32)
     * @see append(int64)
     * @see append(float32)
     * @see append(float64)
     * @see append(String)
     */
    @native
    public function append(v:int16):void;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(1234); // "1234"
     * builder.append(7777); // "12347777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @see append(bool)
     * @see append(int8)
     * @see append(int16)
     * @see append(int64)
     * @see append(float32)
     * @see append(float64)
     * @see append(String)
     */
    @native
    public function append(v:int32):void;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(cast<int64>(1234)); // "1234"
     * builder.append(cast<int64>(7777)); // "12347777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @see append(bool)
     * @see append(int8)
     * @see append(int16)
     * @see append(int32)
     * @see append(float32)
     * @see append(float64)
     * @see append(String)
     */
    @native
    public function append(v:int64):void;

    /**
     * @brief Append string representation of floating value.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(cast<float32>(12.34)); // "12.34"
     * builder.append(cast<float32>(77.77)); // "12.3477.77"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @see append(bool)
     * @see append(int8)
     * @see append(int16)
     * @see append(int32)
     * @see append(int64)
     * @see append(float64)
     * @see append(String)
     */
    @native
    public function append(v:float32):void;

    /**
     * @brief Append string representation of floating value.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.append(12.34); // "12.34"
     * builder.append(77.77); // "12.3477.77"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @see append(bool)
     * @see append(int8)
     * @see append(int16)
     * @see append(int32)
     * @see append(int64)
     * @see append(float32)
     * @see append(String)
     */
    @native
    public function append(v:float64):void;

    /**
     * @brief Append character from Unicode code-point specified by parameter.
     *
     * With this function, user could insert character value by specifying
     * Unicode code-point directly. @n
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.appendAsCharacter(cast<int8>(97)); // insert 'a'
     * builder.appendAsCharacter(cast<int8>(98)); // insert 'b'
     * builder.appendAsCharacter(cast<int8>(99)); // insert 'c'
     *
     * print(builder.toString() + "\n"); // this will print "abc" at single line on console
     * @endcode
     *
     * @arg v 8-bit integer value represents Unicode code-point.
     *
     * @remark If the required value of Unicode code-pointer is larger than
     *         maximum value of 8-bit signed integer. User may want to take a
     *         look at appendAsCharacter(int16) or appendAsCharacter(int32).
     *
     * @see appendAsCharacter(int16)
     * @see appendAsCharacter(int32)
     */
    @native
    public function appendAsCharacter(v:int8):void;

    /**
     * @brief Append character from Unicode code-point specified by parameter.
     *
     * With this function, user could insert character value by specifying
     * Unicode code-point directly. @n
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.appendAsCharacter(cast<int16>(169)); // insert '©' (copyright sign)
     * builder.appendAsCharacter(cast<int16>(174)); // insert '®' (register sign)
     *
     * print(builder.toString() + "\n"); // this will print "©®" (or similar output depends on locale) at single line on console
     * @endcode
     *
     * @arg v 16-bit integer value represents Unicode code-point.
     *
     * @remark If the required value of Unicode code-pointer is larger than
     *         maximum value of 8-bit signed integer. User may want to take a
     *         look at appendAsCharacter(int16) or appendAsCharacter(int32).
     *
     * @see appendAsCharacter(int16)
     * @see appendAsCharacter(int32)
     */
    @native
    public function appendAsCharacter(v:int16):void;

    /**
     * @brief Append character from Unicode code-point specified by parameter.
     *
     * With this function, user could insert character value by specifying
     * Unicode code-point directly. @n
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder();
     *
     * builder.appendAsCharacter(12486); // insert 'テ'
     * builder.appendAsCharacter(12473); // insert 'ス'
     * builder.appendAsCharacter(12488); // insert 'ト'
     * builder.appendAsCharacter(28204); // insert '測'
     * builder.appendAsCharacter(35430); // insert '試'
     *
     * print(builder.toString() + "\n"); // this will print "テスト測試" at single line on console
     * @endcode
     *
     * @arg v 32-bit integer value represents Unicode code-point.
     *
     * @see appendAsCharacter(int8)
     * @see appendAsCharacter(int16)
     */
    @native
    public function appendAsCharacter(v:int32):void;

    /**
     * @brief Insert the given string at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.append(1, "abc "); // After this line, builder will be "aabc bc"
     * builder.append(1, "def "); // After this line, builder will be "adef abc bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Another string to be inserted.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int8)
     * @see insert(int32, int16)
     * @see insert(int32, int32)
     * @see insert(int32, int64)
     * @see insert(int32, float32)
     * @see insert(int32, float64)
     */
    @native
    public function insert(offset:int32, v:String):void;

    /**
     * @brief Append @c true or @c false at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.insert(1, true ); // After this line, builder will be "atruebc"
     * builder.insert(1, false); // After this line, builder will be "afalsetruebc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Append @c true if value is @a true; @c false otherwise.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, int8)
     * @see insert(int32, int16)
     * @see insert(int32, int32)
     * @see insert(int32, int64)
     * @see insert(int32, float32)
     * @see insert(int32, float64)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:bool):void;

    /**
     * @brief Append string representation of integer value at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.insert(1, cast<int8>(12)); // "a12bc"
     * builder.insert(1, cast<int8>(21)); // "a2112bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Value to generate as string representation.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int16)
     * @see insert(int32, int32)
     * @see insert(int32, int64)
     * @see insert(int32, float32)
     * @see insert(int32, float64)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:int8):void;

    /**
     * @brief Append string representation of integer value at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.insert(1, cast<int16>(1234)); // "a1234bc"
     * builder.insert(1, cast<int16>(7777)); // "a77771234bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Value to generate as string representation.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int8)
     * @see insert(int32, int32)
     * @see insert(int32, int64)
     * @see insert(int32, float32)
     * @see insert(int32, float64)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:int16):void;

    /**
     * @brief Append string representation of integer value at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.insert(1, 1234); // "a1234bc"
     * builder.insert(1, 7777); // "a77771234bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Value to generate as string representation.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int8)
     * @see insert(int32, int16)
     * @see insert(int32, int64)
     * @see insert(int32, float32)
     * @see insert(int32, float64)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:int32):void;

    /**
     * @brief Append string representation of integer value at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.insert(1, cast<int64>(1234)); // "a1234bc"
     * builder.insert(1, cast<int64>(7777)); // "a77771234bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Value to generate as string representation.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int8)
     * @see insert(int32, int16)
     * @see insert(int32, int32)
     * @see insert(int32, float32)
     * @see insert(int32, float64)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:int64):void;

    /**
     * @brief Append string representation of floating value at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.insert(1, cast<float32>(12.34)); // "a12.34bc"
     * builder.insert(1, cast<float32>(77.77)); // "a77.7712.34bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Value to generate as string representation.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int8)
     * @see insert(int32, int16)
     * @see insert(int32, int32)
     * @see insert(int32, int64)
     * @see insert(int32, float64)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:float32):void;

    /**
     * @brief Append string representation of floating value at specified position.
     *
     * For example:
     * @code
     * var builder: StringBuilder = new StringBuilder("abc");
     *
     * builder.append(12.34); // "a12.34bc"
     * builder.append(77.77); // "a77.7712.34bc"
     * @endcode
     *
     * @arg offset The position to insert to.
     * @arg other  Value to generate as string representation.
     *
     * @remark The value of @a offset should be in range [0, length()]. Or the
     *         data is not changed.
     *
     * @see insert(int32, bool)
     * @see insert(int32, int8)
     * @see insert(int32, int16)
     * @see insert(int32, int32)
     * @see insert(int32, int64)
     * @see insert(int32, float32)
     * @see insert(int32, String)
     */
    @native
    public function insert(offset:int32, v:float64):void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    var stream : ptr_<int64>;
    /**
     * @endcond
     */
}
