/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import . = thor.lang;

/**
 * @brief Class encapsulates common operations for UUID.
 *
 * User could work with UUID using this class, for example, generate random
 * UUID or test for nil.
 *
 * The string representation used by this class are in the following form:
 * @verbatim
 * 01234567-0123-0123-0123-0123456789ab
 * @endverbatim
 */
@cpu
@native
@system
class UUID extends Object implements Cloneable
{
    /**
     * @brief Constructor creates new instance represents nil.
     */
    @native public function new(): void;

    /**
     * @brief Destructor releases resources.
     */
    @native public function delete(): void;

    /**
     * @brief Create new instance with the same UUID value.
     *
     * @return The created instance.
     */
    @native public override function clone(): UUID;

    /**
     * @brief Generate hash value according to UUID value.
     *
     * @return The generated hash value.
     *
     * @remark O(1) time complexity.
     */
    @native public override function hash(): int64;

    /**
     * @brief Test if this is nil.
     *
     * @return Boolean value indicates this is nil or not.
     * @retval true  This is nil.
     * @retval false This is not nil.
     *
     * @see setNil()
     */
    @native public function isNil(): bool;

    /**
     * @brief Set this to nil.
     *
     * @see isNil()
     */
    @native public function setNil(): void;

    /**
     * @brief Test if this is the same as the given one.
     *
     * @return Boolean indicates they are the same.
     * @retval true  They are the same.
     * @retval false They are not the same.
     */
    @native public function isEqual(rhs: UUID): bool;

    /**
     * @brief Dump the value to equivalent string representation.
     *
     * @return The generated string representation by contained value.
     */
    @native public function toString(): String;

    /**
     * @brief Create new instance represents value nil.
     *
     * @return The new instance.
     */
    @native public static function nil(): UUID;

    /**
     * @brief Create new instance with randomized value.
     *
     * @return The new instance.
     */
    @native public static function random(): UUID;

    /**
     * @brief Create new instance with value from given string representation.
     *
     * @return The new instance.
     * @retval null     The given string representation is not fit the expected form.
     * @retval non-null The new instance with the parsed value.
     */
    @native public static function parse(s: String): UUID;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _dummy_placeholder0: int64;
    private var _dummy_placeholder1: int64;
    /**
     * @endcond
     */
}
