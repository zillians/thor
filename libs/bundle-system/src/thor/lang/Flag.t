/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import thor.container;
import thor.unmanaged;

/**
 * @brief This class helps users define/parse command line options.
 *
 * One may pass command line options via @c --args option on launch of entry
 * task. A @a name-value @a pair @a option or a @a positional @a option can be
 * defined. A name-value pair option is given by a name string prefixed with
 * @c --, optionally followed by a ‘=’, and then a value string. A positional
 * option is a series of values. For example:
 * @code
 * thorc r test --args --arg1=val1 --arg2=val2         # name-value pairs with '='
 * thorc r test --args --arg1 val1 --arg2 val2         # name-value pairs without '='
 * thorc r test --args 1 2 3 4                         # positional options
 * thorc r test --args --arg1=val1 1 2 --arg2 val2 3 4 # mixed
 * @endcode
 *
 * Flag can support three types of values: integer, float, and string:
 * @code
 * thorc r test --args --arg=123        # integer value
 * thorc r test --args --arg=3.14159    # float value
 * thorc r test --args --arg="Zillians" # string value
 * @endcode
 *
 * The following code shows the basic usage:
 * @code
 * @entry
 * task hello_flag()
 * {
 *     const f = new Flag();                        // 1. Create Flag class instance
 *     if (!f.createInteger("tint","Test integer")) // 2. Define options via ``create`` series methods
 *         println("Failed to create option");
 *
 *     if (f.parse())                               // 3. Call parse().
 *     {
 *         const v : int64 = f.getInteger("tint");  // 4. Use ``get`` series methods to query values.
 *         println("Integer: \{v}");
 *         exit(0);
 *     }
 *     else
 *     {
 *         exit(-1);
 *     }
 * }
 * @endcode
 *
 * A name-pair option can be set as a required option with the third optional
 * parameter:
 * @code
 * f.createInteger("tint", "test", true); // required option
 * f.createFloat("tfloat", "test");       // default as optional
 * @endcode
 *
 * A positional option can be set as a required option as well. Additionally,
 * a required number of values can be specified:
 * @code
 * f.createPositionalInteger("pint", "test", 3, true); // 3 integers are required
 * @endcode
 */
@cpu
@system
class Flag extends Object
{
    /**
     * @brief Constructor which creates command line parser with no options.
     */
    @native
    public function new():void;

    /**
     * @brief Destructor releases resources.
     */
    @native
    public function delete():void;

    /**
     * @brief Define name-value pair option with type int64.
     *
     * @arg name        The name of option.
     * @arg description The option description that will be shown in helping text.
     * @arg required    Set if this option is required or not.
     *
     * @return Boolean value indicates such options is added successfully or not.
     * @retval true  Success.
     * @retval false If @a name is empty or option is already created.
     */
    @native
    public function createInteger(name:String, description:String, required:bool = false):bool;

    /**
     * @brief Define name-value pair option with type float64.
     *
     * @arg name        The name of option.
     * @arg description The option description that will be shown in helping text.
     * @arg required    Set if this option is required or not.
     *
     * @return Boolean value indicates such options is added successfully or not.
     * @retval true  Success.
     * @retval false If @a name is empty or option is already created.
     */
    @native
    public function createFloat(name:String, description:String, required:bool = false):bool;

    /**
     * @brief Define name-value pair option with type String.
     *
     * @arg name        The name of option.
     * @arg description The option description that will be shown in helping text.
     * @arg required    Set if this option is required or not.
     *
     * @return Boolean value indicates such option is added successfully or not.
     * @retval true  Success.
     * @retval false If @a name is empty or option is already created.
     */
    @native
    public function createString(name:String, description:String, required:bool = false):bool;

    /**
     * @brief Define positional command line option with type int64.
     *
     * @arg name        The name of the option.
     * @arg description The option description that will be shown in helping text.
     * @arg count       The maximum number of values can be given. -1 if no limit.
     * @arg required    Set if this option is required or not.
     *
     * @return Boolean value indicates such option is added successfully or not.
     * @retval true  Success.
     * @retval false If @a name is empty or option is already created.
     */
    @native
    public function createPositionalInteger(name:String, description:String, count:int32 = -1, required:bool = false):bool;

    /**
     * @brief Define positional command line option with type float64.
     *
     * @arg name        The name of the option.
     * @arg description The option description that will be shown in helping text.
     * @arg count       The maximum number of values can be given. -1 if no limit.
     * @arg required    Set if this option is required or not.
     *
     * @return Boolean value indicates such option is added successfully or not.
     * @retval true  Success.
     * @retval false If @a name is empty or option is already created.
     */
    @native
    public function createPositionalFloat(name:String, description:String, count:int32 = -1, required:bool = false):bool;

    /**
     * @brief Define positional command line option with type String.
     *
     * @arg name        The name of the option.
     * @arg description The option description that will be shown in helping text.
     * @arg count       The maximum number of values can be given. -1 if no limit.
     * @arg required    Set if this option is required or not.
     *
     * @return Boolean value indicates such option is added successfully or not.
     * @retval true  Success.
     * @retval false If @a name is empty or option is already created.
     */
    @native
    public function createPositionalString(name:String, description:String, count:int32 = -1, required:bool = false):bool;

    /**
     * @brief Parse the given command line options.
     *
     * Command line parsing will fail if:
     * - An undefined option is encountered.
     * - An required option is not given.
     * - The number of values given to a positional option exceeds the
     *   user-defined limit.
     * - The type of a given value mismatches the defined type.
     * .
     *
     * @return Boolean value indicates successfully parsing command line options.
     * @retval true  Successfully parse options.
     * @retval false Failed to parse options.
     *
     * @remark has/get series functions can be in use after this function return @c true.
     */
    @native
    public function parse():bool;

    /**
     * @brief Query if the name option is given.
     *
     * @arg name The name of option to be queried.
     *
     * @return Boolean value indicates option of @a name is given.
     * @retval true  Be given.
     * @retval false Not be given.
     */
    @native
    public function has(name:String):bool;

    /**
     * @brief Query the value of the @a name-value pair option with type int64.
     *
     * @arg name The name of the queried option.
     *
     * @return The given value is returned or an empty value is returned if
     *         the option is not given.
     */
    @native
    public function getInteger(name:String):int64;

    /**
     * @brief Query the value of the @a name-value pair option with type float64.
     *
     * @arg name The name of the queried option.
     *
     * @return The given value is returned or an empty value is returned if
     *         the option is not given.
     */
    @native
    public function getFloat(name:String):float64;

    /**
     * @brief Query the value of the @a name-value pair option with type String.
     *
     * @arg name The name of the queried option.
     *
     * @return The given value is returned or an empty value is returned if
     *         the option is not given.
     */
    @native
    public function getString(name:String):String;

    /**
     * @brief Query the value of the name positional option with type int64.
     *
     * @arg name The name of the queried option.
     *
     * @return The given value is returned as a thor.container.Vector. An
     *         empty vector is returned if the option is not given.
     */
    @native
    public function getPositionalInteger(name:String):thor.container.Vector<int64>;

    /**
     * @brief Query the value of the name positional option with type float64.
     *
     * @arg name The name of the queried option.
     *
     * @return The given value is returned as a thor.container.Vector. An
     *         empty vector is returned if the option is not given.
     */
    @native
    public function getPositionalFloat(name:String):thor.container.Vector<float64>;

    /**
     * @brief Query the value of the name positional option with type String.
     *
     * @arg name The name of the queried option.
     *
     * @return The given value is returned as a thor.container.Vector. An
     *         empty vector is returned if the option is not given.
     */
    @native
    public function getPositionalString(name:String):thor.container.Vector<String>;

    /**
     * @brief Retrieve the unprocessed command line options.
     *
     * @return A vector stores all command line options string.
     */
    @native
    public function getRaw():thor.container.Vector<String>;

    /**
     * @brief Generate help string for this parser.
     *
     * @return A structured helping text string according to the defined
     *         command line options.
     */
    @native
    public function help():String;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _thor_lang_flag_internal_1:thor.unmanaged.ptr_<int8>;
    /**
     * @endcond
     */
}
