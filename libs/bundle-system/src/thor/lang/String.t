/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import thor.unmanaged;

/**
 * @brief Built-in class holds character strings.
 *
 * All string literal in Thor, for example "abc", are instances of this
 * class. @n
 * Strings are immutable, that is, you cannot modify the value of it. Use
 * MutableString for such purposes. @n
 * String class provide the following functionalities:
 * - comparison.
 * - character/string search.
 * - sub-string.
 * - case conversion.
 * .
 *
 * @remark Current implementation for case insensitive handling may fail under
 *         some locale, for example, Greek letter "Σ" may not be handled
 *         correctly.
 *
 * @see MutableString
 */
@cpu
@native
@system
@replicable
class String extends Object implements Cloneable
{
    /**
     * @brief Default constructor which creates empty string (equivalent to "").
     */
    @native
    public function new():void;

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public virtual function delete():void;

    /**
     * @brief Generate hash value based on character string representation.
     *
     * Implementation of String.clone is different from the origin one. The
     * generated value is based on character string representation, for example
     * "abc". So it's possible that two String instance generate the same hash
     * value.
     *
     * @return Hashed value.
     *
     * @remark O(n) time complexity.
     */
    @native
    public override function hash():int64;

    /**
     * @brief Create new instance with the same character string representation.
     *
     * @return New instance with the same character string representation.
     */
    @native
    public override function clone():String;

    /**
     * @brief Test if this instance and given one have the same character string representation.
     *
     * For example:
     * @code
     * "abc".isEqual("abc"); // true
     * "abc".isEqual("def"); // false
     * "abc".isEqual("ABC"); // false
     * @endcode
     *
     * @arg rhs String to compare to.
     *
     * @return Boolean value indicates this and @a rhs are same or not.
     * @retval true  They are the same.
     * @retval false They are not the same.
     *
     * @remark The comparison is case sensitive. If case insensitive is
     *         required, please take a look on the overloaded version.
     *
     * @see isEqual(String, bool)
     * @see isLessThan
     */
    @native
    public function isEqual(rhs:String):bool;

    /**
     * @brief Test if this instance and given one have the same character string representation.
     *
     * This is overloaded function to compare strings in case sensitive or
     * insensitive way. According to @a ignore_case.
     *
     * For example:
     * @code
     * "abc".isEqual("ABC", true ); // true
     * "abc".isEqual("ABC", false); // false
     * @endcode
     *
     * @arg rhs         String to compare to.
     * @arg ignore_case Flag indicates comparison should be case insensitive
     *                  or not.
     *
     * @return Boolean value indicates this and @a rhs are same or not.
     * @retval true  They are the same.
     * @retval false They are not the same.
     *
     * @see isEqual(String)
     * @see isLessThan
     */
    @native
    public function isEqual(rhs:String, ignore_case:bool):bool;

    /**
     * @brief Test if instance's character string representation is less than the given one's.
     *
     * For example:
     * @code
     * "abc".isLessThan("abc");  // false
     * "abc".isLessThan("abcd"); // true
     * "abc".isLessThan("def");  // true
     * @endcode
     *
     * @arg rhs String to compare to.
     *
     * @return Boolean value indicates this is less than @a rhs.
     * @retval true  This is lesser.
     * @retval false This is not lesser.
     *
     * @remark The comparison is case sensitive.
     * @remark Comparison is done in lexicographical way.
     *
     * @see isEqual(String)
     * @see isEqual(String, bool)
     */
    @native
    public function isLessThan(rhs:String):bool;

    /**
     * @brief Count the number of characters stored.
     *
     * @return Number of characters.
     *
     * @remark The return value will not be negative integer.
     */
    @native
    public function length():int32;

    /**
     * @brief Create concatenated string with this followed by the given one.
     *
     * For example:
     * @code
     * var new_str = "abc".concate("def");
     * new_str.isEqual("abcdef"); // true
     * @endcode
     *
     * @arg other Another string to be appended at the end.
     *
     * @return The concatenated string.
     */
    @native
    public function concate(other:String):String;

    /**
     * @brief Get the index of first occurrence of give string.
     *
     * For example:
     * @code
     * "abcdef"    .find("cd"); // 2
     * "abcdefcdef".find("cd"); // 2
     * @endcode
     *
     * @arg candidate The string to search for.
     *
     * @return The index of first occurrence.
     * @retval Non-negative-value The index.
     * @retval Negative-value     No occurrence of @a candidate.
     */
    @native
    public function find(candidate:String):int32;

    /**
     * @brief Compare character string representation with another one.
     *
     * This function compares character string representation, then returns
     * negative value, zero, or positive value. @n
     * Negative value means the instance is less than the given one; positive
     * value means greater than; otherwise, zero is returned.
     *
     * For example:
     * @code
     * "def".compareTo("abc"); // > 0
     * "def".compareTo("def"); //   0
     * "def".compareTo("ghi"); // < 0
     * @endcode
     *
     * @arg other String to be compared to.
     *
     * @return Integer value indicates this is less than, equal to, or greater
     *         than @a other.
     * @retval Negative-value This is less than @a other.
     * @retval 0              This is equal to @a other.
     * @retval Positive-value This is greater than @a other.
     *
     * @remark The comparison is case sensitive.
     * @remark Comparison is done in lexicographical way.
     *
     * @see compareTo(String, bool)
     */
    @native
    public function compareTo(other:String):int32;

    /**
     * @brief Compare character string representation with another one.
     *
     * This function compares character string representation (case
     * insensitive), then returns negative value, zero, or positive value. @n
     * Negative value means the instance is less than the given one; positive
     * value means greater than; otherwise, zero is returned.
     *
     * This is overloaded function to compare strings in case sensitive or
     * insensitive way. According to @a ignore_case.
     *
     * For example:
     * @code
     * "def".compareTo("abc", true ); // >  0
     * "def".compareTo("def", true ); //    0
     * "def".compareTo("DEF", false); // != 0
     * "def".compareTo("ghi", true ); // <  0
     * @endcode
     *
     * @arg other       String to be compared to.
     * @arg ignore_case Flag indicates comparison should be case insensitive
     *                  or not.
     *
     * @return Integer value indicates this is less than, equal to, or greater
     *         than @a other.
     * @retval Negative-value This is less than @a other.
     * @retval 0              This is equal to @a other.
     * @retval Positive-value This is greater than @a other.
     *
     * @remark Comparison is done in lexicographical way.
     *
     * @see compareTo
     */
    @native
    public function compareTo(other:String, ignore_case:bool):int32;

    /**
     * @brief Test if this instance has suffix the same as given string.
     *
     * For example:
     * @code
     * "abc".endsWith(  "c"); // true
     * "abc".endsWith( "bc"); // true
     * "abc".endsWith("abc"); // true
     * "abc".endsWith(  "a"); // false
     * @endcode
     *
     * @arg suffix Suffix to test for.
     *
     * @return Boolean value indicates suffix is the same or not.
     * @retval true  This instance has given suffix.
     * @retval false This instance has no given suffix.
     *
     * @see startsWith(String)
     * @see startsWith(String, int32)
     */
    @native
    public function endsWith(suffix:String):bool;

    /**
     * @brief Search of the first occurrence of given string.
     *
     * For example:
     * @code
     * "abcabcabc".indexOf("a" ); // 0
     * "abcabcabc".indexOf("b" ); // 1
     * "abcabcabc".indexOf("bc"); // 1
     * @endcode
     *
     * @arg str Given string to search for.
     *
     * @return Index of first character of the first occurrence.
     * @retval -1        Given string is not found.
     * @retval Otherwise The index.
     *
     * @see indexOf(String, int32)
     */
    @native
    public function indexOf(str:String):int32;

    /**
     * @brief Search of the first occurrence of given string, starts at the given index.
     *
     * For example:
     * @code
     * "abcabcabc".indexOf("a"    ); // 0
     * "abcabcabc".indexOf("a" , 1); // 3
     * "abcabcabc".indexOf("a" , 4); // 6
     * @endcode
     *
     * @arg str       Given string to search for.
     * @arg fromIndex The begining of search.
     *
     * @return Index of first character of the first occurrence.
     * @retval -1        Given string is not found.
     * @retval Otherwise The index.
     *
     * @see indexOf(String)
     */
    @native
    public function indexOf(str:String, fromIndex:int32):int32;

    /**
     * @brief Search of the last occurrence of given string.
     *
     * For example:
     * @code
     * "abcabcabc".lastIndexOf("a" ); // 6
     * "abcabcabc".lastIndexOf("b" ); // 7
     * "abcabcabc".lastIndexOf("bc"); // 7
     * @endcode
     *
     * @arg str Given string to search for.
     *
     * @return Index of first character of the last occurrence.
     * @retval -1        Given string is not found.
     * @retval Otherwise The index.
     *
     * @see lastIndexOf(String, int32)
     */
    @native
    public function lastIndexOf(str:String):int32;

    /**
     * @brief Search of the last occurrence of given string, starts backword at the given index.
     *
     * For example:
     * @code
     * "abcabcabc".lastIndexOf("a"    ); // 6
     * "abcabcabc".lastIndexOf("a" , 1); // 0
     * "abcabcabc".lastIndexOf("a" , 4); // 3
     * @endcode
     *
     * @arg str       Given string to search for.
     * @arg fromIndex The begining of search.
     *
     * @return Index of first character of the last occurrence.
     * @retval -1        Given string is not found.
     * @retval Otherwise The index.
     *
     * @see lastIndexOf(String)
     */
    @native
    public function lastIndexOf(str:String, fromIndex:int32):int32;

    /**
     * @brief Test if the specified regions of strings are the same, could be case sensitive or insensitive.
     *
     * This function will compare case sensitively or insensitively @a len
     * characters between two regions. One is this current string starts at
     * @a toffset, another one is @a other starts at @a ooffset. @n Then return
     * boolean value indicates the two regions are the same or not.
     *
     * @arg ignoreCase Flag indicates comparison should be in case sensitive way or not.
     * @arg toffset    Start position of comparison for this string.
     * @arg other      Given string to compare to.
     * @arg ooffset    Start position of comparison for @a other.
     * @arg len        Number of characters to be compared.
     *
     * @return Boolean value indicates the two regions are the same or not.
     * @retval true  Two regions are the same.
     * @retval false Two regions are not the same.
     *
     * @see regionMatches(int32, String, int32, int32)
     */
    @native
    public function regionMatches(ignoreCase:bool, toffset:int32, other:String, ooffset:int32, len:int32):bool;

    /**
     * @brief Test if the specified regions of strings are the same.
     *
     * This function will compare @a len characters between two regions. One
     * is this current string starts at @a toffset, another one is @a other
     * starts at @a ooffset. @n Then return boolean value indicates the two
     * regions are the same or not.
     *
     * @arg toffset Start position of comparison for this string.
     * @arg other   Given string to compare to.
     * @arg ooffset Start position of comparison for @a other.
     * @arg len     Number of characters to be compared.
     *
     * @return Boolean value indicates the two regions are the same or not.
     * @retval true  Two regions are the same.
     * @retval false Two regions are not the same.
     *
     * @remark The comparison is case sensitive.
     *
     * @see regionMatches(bool, int32, String, int32, int32)
     */
    @native
    public function regionMatches(toffset:int32, other:String, ooffset:int32, len:int32):bool;

    /**
     * @brief Test if this instance has prefix the same as given string.
     *
     * For example:
     * @code
     * "abc".startsWith("a"  ); // true
     * "abc".startsWith("ab" ); // true
     * "abc".startsWith("abc"); // true
     * "abc".startsWith("c"  ); // false
     * @endcode
     *
     * @arg prefix Prefix to test for.
     *
     * @return Boolean value indicates prefix is the same or not.
     * @retval true  This instance has given prefix.
     * @retval false This instance has no given prefix.
     *
     * @see endsWith
     * @see startsWith(String, int32)
     */
    @native
    public function startsWith(prefix:String):bool;

    /**
     * @brief Test if this instance has prefix the same as given string.
     *
     * This function will test for prefix which starts at position specified by
     * @a toffset.
     *
     * For example:
     * @code
     * "abc".startsWith("bc", 0); // false
     * "abc".startsWith("bc", 1); // true
     * "abc".startsWith("bc", 2); // false
     * "abc".startsWith("bc", 3); // false
     * @endcode
     *
     * @arg prefix  Prefix to test for.
     * @arg toffset Prefix should start from.
     *
     * @return Boolean value indicates prefix is the same or not.
     * @retval true  This instance has given prefix.
     * @retval false This instance has no given prefix.
     *
     * @see endsWith
     * @see startsWith(String)
     */
    @native
    public function startsWith(prefix:String, toffset:int32):bool;

    /**
     * @brief Create new instance with all characters start at @a beginIndex.
     *
     * @arg beginIndex The first character to be copied.
     *
     * @return The new instance contains copied characters.
     *
     * @see substring(int32, int32)
     */
    @native
    public function substring(beginIndex:int32):String;

    /**
     * @brief Create new instance with all characters start at @a beginIndex and end of @a endIndex.
     *
     * @arg beginIndex The first character to be copied.
     * @arg endIndex   Next character of the last one to be copied.
     *
     * @return The new instance contains copied characters.
     *
     * @see substring(int32)
     */
    @native
    public function substring(beginIndex:int32, endIndex:int32):String;

    /**
     * @brief Create new instance with all characters converted to lower case.
     *
     * @return The new instance with lower case characters.
     *
     * @see toUpperCase()
     */
    @native
    public function toLowerCase():String;

    /**
     * @brief Create new instance with all characters converted to upper case.
     *
     * @return The new instance with upper case characters.
     *
     * @see toLowerCase()
     */
    @native
    public function toUpperCase():String;

    /**
     * @brief Create new instance with all characters, except leading and trailing whitespaces.
     *
     * For example:
     * @code
     * "abc"  .trim(); // "abc"
     * "  abc".trim(); // "abc"
     * "abc  ".trim(); // "abc"
     * @endcode
     *
     * @return The new instance without leading and trailing whitespaces.
     */
    @native
    public function trim():String;

    /**
     * @brief Create new instance with the same character string representation.
     *
     * @return Created new instance.
     */
    @native
    public function toString():String;

    /**
     * @brief Overload operator '+' to create new instance with @c true or @c false appended.
     *
     * For example:
     * @code
     * "abc " + true ; // "abc true"
     * "abc " + false; // "abc false"
     * @endcode
     *
     * @arg other Append @c true if value is @a true; @c false otherwise.
     *
     * @return MutableString contains characters of origin one with @c true or @c false appended.
     *
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     */
    public function __add__(other:bool):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance with string representation of integer value appended.
     *
     * For example:
     * @code
     * "abc " + cast<int8>(12); // "abc 12"
     * "abc " + cast<int8>(21); // "abc 21"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return MutableString contains characters of origin one with value string appended.
     *
     * @see __add__(bool)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     */
    public function __add__(other:int8):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance with string representation of integer value appended.
     *
     * For example:
     * @code
     * "abc " + cast<int16>(1234); // "abc 1234"
     * "abc " + cast<int16>(7777); // "abc 7777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return MutableString contains characters of origin one with value string appended.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     */
    public function __add__(other:int16):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance with string representation of integer value appended.
     *
     * For example:
     * @code
     * "abc " + 1234; // "abc 1234"
     * "abc " + 7777; // "abc 7777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return MutableString contains characters of origin one with value string appended.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     */
    public function __add__(other:int32):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance with string representation of integer value appended.
     *
     * For example:
     * @code
     * "abc " + cast<int64>(1234); // "abc 1234"
     * "abc " + cast<int64>(7777); // "abc 7777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return MutableString contains characters of origin one with value string appended.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     */
    public function __add__(other:int64):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance with string representation of floating value appended.
     *
     * For example:
     * @code
     * "abc " + cast<float32>(12.34); // "abc 12.34"
     * "abc " + cast<float32>(77.77); // "abc 77.77"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return MutableString contains characters of origin one with value string appended.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float64)
     * @see __add__(String)
     */
    public function __add__(other:float32):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance with string representation of floating value appended.
     *
     * For example:
     * @code
     * "abc " + 12.34; // "abc 12.34"
     * "abc " + 77.77; // "abc 77.77"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return MutableString contains characters of origin one with value string appended.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(String)
     */
    public function __add__(other:float64):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Overload operator '+' to create new instance which concates this and the given one.
     *
     * For example:
     * @code
     * "abc " + "abc"; // "abc abc"
     * "abc " + "def"; // "abc def"
     * @endcode
     *
     * @arg other Another string to be concated.
     *
     * @return MutableString contains concated characters.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     */
    public function __add__(other:String):MutableString
    {
        return (new MutableString(this)).concate(other);
    }

    /**
     * @brief Customized serialization for native data.
     *
     * @see deserialize(ReplicationDecoder, String)
     */
    @native protected static function serialize  (encoder:ReplicationEncoder, instance:String):bool;

    /**
     * @brief Customized deserialization for native data.
     *
     * @see serialize(ReplicationEncoder, String)
     */
    @native protected static function deserialize(decoder:ReplicationDecoder, instance:String):bool;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    // dummy placeholder
    private var string_internal:thor.unmanaged.ptr_<int8>;
    /**
     * @endcond
     */
}

/**
 * @brief Built-in class holds character strings, but is mutable.
 *
 * For string operations, immutable string may produce bad performance in some
 * cases. For that, Thor provides @c MutableString, which is mutable. @n
 *
 * This class may change the underlying data by operations, which gains
 * performance by reusing resources.
 *
 * @see String
 */
@cpu
@system
class MutableString extends String 
{
    /**
     * @brief Constructor creates instance with the same character string as the given one.
     */
    @native
    public function new(s:String):void;

    /**
     * @brief Destructor which releases all resources hold by instance.
     */
    @native
    public function delete():void;

    /**
     * @brief Create new instance with the same character string representation.
     *
     * @return New instance with the same character string representation.
     */
    @native
    public override function clone():MutableString;

    /**
     * @brief Append @c true or @c false.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(true ); // "abc true"
     * (new MutableString("abc ")).concate(false); // "abc false"
     * @endcode
     *
     * @arg other Append @c true if value is @a true; @c false otherwise.
     *
     * @return This instance.
     *
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    @native
    public function concate(other:bool):MutableString;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(cast<int8>(12)); // "abc 12"
     * (new MutableString("abc ")).concate(cast<int8>(21)); // "abc 21"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    @native
    public function concate(other:int8):MutableString;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(cast<int16>(1234)); // "abc 1234"
     * (new MutableString("abc ")).concate(cast<int16>(7777)); // "abc 7777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    @native
    public function concate(other:int16):MutableString;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(1234); // "abc 1234"
     * (new MutableString("abc ")).concate(7777); // "abc 7777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    @native
    public function concate(other:int32):MutableString;

    /**
     * @brief Append string representation of integer value.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(cast<int64>(1234)); // "abc 1234"
     * (new MutableString("abc ")).concate(cast<int64>(7777)); // "abc 7777"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    @native
    public function concate(other:int64):MutableString;

    /**
     * @brief Append string representation of floating value.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(cast<float32>(12.34)); // "abc 12.34"
     * (new MutableString("abc ")).concate(cast<float32>(77.77)); // "abc 77.77"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float64)
     * @see concate(String)
     */
    @native
    public function concate(other:float32):MutableString;

    /**
     * @brief Append string representation of floating value.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate(12.34); // "abc 12.34"
     * (new MutableString("abc ")).concate(77.77); // "abc 77.77"
     * @endcode
     *
     * @arg other Value to generate as string representation.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(String)
     */
    @native
    public function concate(other:float64):MutableString;

    /**
     * @brief Append the given string.
     *
     * For example:
     * @code
     * (new MutableString("abc ")).concate("abc"); // "abc abc"
     * (new MutableString("abc ")).concate("def"); // "abc def"
     * @endcode
     *
     * @arg other Another string to be concated.
     *
     * @return This instance.
     *
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     */
    @native
    public function concate(other:String):MutableString;

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:bool):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:int8):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:int16):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:int32):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:int64):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float64)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:float32):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(String)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:float64):MutableString
    {
        return this.concate(other);
    }

    /**
     * @brief Overload operator '+' with the same operation as concate, for convenience.
     *
     * @arg other Value to generate as string representation.
     *
     * @see __add__(bool)
     * @see __add__(int8)
     * @see __add__(int16)
     * @see __add__(int32)
     * @see __add__(int64)
     * @see __add__(float32)
     * @see __add__(float64)
     * @see concate(bool)
     * @see concate(int8)
     * @see concate(int16)
     * @see concate(int32)
     * @see concate(int64)
     * @see concate(float32)
     * @see concate(float64)
     * @see concate(String)
     */
    public function __add__(other:String):MutableString
    {
        return this.concate(other);
    }
}

/**
 * @cond THOR_PRIVATE_IMPLEMENTATION
 */
@cpu
@native
function __getStringLiteral(symbol_id:int64):String;
/**
 * @endcond
 */
