/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

/*
// TODO kernel launch helpers on accelerators
@native function accLaunch<F            >(dim_x:int64, dim_y:int64, dim_z:int64, function:F                     ):int32;
@native function accLaunch<F, T0        >(dim_x:int64, dim_y:int64, dim_z:int64, function:F, p0:T0              ):int32;
@native function accLaunch<F, T0, T1    >(dim_x:int64, dim_y:int64, dim_z:int64, function:F, p0:T0, p1:T1       ):int32;
@native function accLaunch<F, T0, T1, T2>(dim_x:int64, dim_y:int64, dim_z:int64, function:F, p0:T0, p1:T1, p2:T2):int32;
*/

/**
 * @brief Terminate Thor VM
 *
 * User needs to call this function in order to terminate a Thor VM. @n
 * That is, entry function will not terminate execution after returned by
 * design.
 *
 * @arg exit_code Exit code will be propagate as Thor VM's exit code.
 *
 * @remark This function will not return.
 * @remark Thor VM will try to exit as soon as possible, but there are
 *         some cases it can't. User should not expect the termination will
 *         take effect right after this call.
 */
@native
function exit(exit_code:int32):void;

/**
 * @cond THOR_PRIVATE_IMPLEMENTATION
 */
@native
function __cuda_getCurrentInvocationId():int32;

@native function __postponeInvocationDependencyTo(postpone_to_id: int64): void;
@native function __setInvocationDependOnCount(invocation_id:int64, count:int32): void;
@native function __setInvocationDependOn(invocation_id:int64, depend_on_id:int64):void;
/**
 * @endcond
 */

/**
 * @brief THOR_FEATURE_NOT_YET_IMPLEMENTATED
 */
@export
@cpu
@kernel
function invokeKernelWrapper(f: lambda(): void)
{
    //f();
    //__removeFromRootSet(cast<Object>(f));
}
/**
 * @endcond
 */

/**
 * @cond THOR_PRIVATE_IMPLEMENTATION
 */
@native @cpu function __invokeRemotely(domain: Domain, adaptor_id: int64, encoder: ReplicationEncoder): bool;

@native { include="thor/lang/Process.h" }
function __RSTMSystemInitialize():void;

@native { include="thor/lang/Process.h" }
function __resetRandomSeed(): void;
/**
 * @endcond
 */

/**
 * @cond THOR_FEATURE_NOT_YET_IMPLEMENTATED
 */
@native { include="thor/lang/Process.h" }
function lock<A>(a:A, io1:int32):void;

@native { include="thor/lang/Process.h" }
function lock<A, B>(a:A, io1:int32, b:B, io2:int32):void;

@native { include="thor/lang/Process.h" }
function lock<A, B, C>(a:A, io1:int32, b:B, io2:int32, c:C, io3:int32):void;

@native { include="thor/lang/Process.h" }
function lock<A, B, C, D>(a:A, io1:int32, b:B, io2:int32, c:C, io3:int32, d:D, io4:int32):void;

@native { include="thor/lang/Process.h" }
function lock<A, B, C, D, E>(a:A, io1:int32, b:B, io2:int32, c:C, io3:int32, d:D, io4:int32, e:E, io5:int32):void;

@native { include="thor/lang/Process.h" }
function unlock<A>(a:A, io1:int32):void;

@native { include="thor/lang/Process.h" }
function unlock<A, B>(a:A, io1:int32, b:B, io2:int32):void;

@native { include="thor/lang/Process.h" }
function unlock<A, B, C>(a:A, io1:int32, b:B, io2:int32, c:C, io3:int32):void;

@native { include="thor/lang/Process.h" }
function unlock<A, B, C, D>(a:A, io1:int32, b:B, io2:int32, c:C, io3:int32, d:D, io4:int32):void;

@native { include="thor/lang/Process.h" }
function unlock<A, B, C, D, E>(a:A, io1:int32, b:B, io2:int32, c:C, io3:int32, d:D, io4:int32, e:E, io5:int32):void;
/**
 * @endcond
 */
