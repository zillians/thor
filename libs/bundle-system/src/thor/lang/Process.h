/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_PROCESS_H_
#define THOR_LANG_PROCESS_H_

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/Domain.h"
#include "thor/lang/Lambda.h"
#include "thor/lang/Replication.h"
#include "thor/container/Vector.h"
#include <type_traits>

namespace thor { namespace lang {

void exit(int32 exit_code);

bool __invokeRemotely(Domain* domain, int64 adaptor_id, ReplicationEncoder* encoder);

template<typename A>
void lock(A a, int32 io1)
{
}

template<typename A, typename B>
void lock(A a, int32 io1, B b, int32 io2)
{
}

template<typename A, typename B, typename C>
void lock(A a, int32 io1, B b, int32 io2, C c, int32 io3)
{
}

template<typename A, typename B, typename C, typename D>
void lock(A a, int32 io1, B b, int32 io2, C c, int32 io3, D d, int32 io4)
{
}

template<typename A, typename B, typename C, typename D, typename E>
void lock(A a, int32 io1, B b, int32 io2, C c, int32 io3, D d, int32 io4, E e, int32 io5)
{
}

template<typename A>
void unlock(A a, int32 io1)
{
}

template<typename A, typename B>
void unlock(A a, int32 io1, B b, int32 io2)
{
}

template<typename A, typename B, typename C>
void unlock(A a, int32 io1, B b, int32 io2, C c, int32 io3)
{
}

template<typename A, typename B, typename C, typename D>
void unlock(A a, int32 io1, B b, int32 io2, C c, int32 io3, D d, int32 io4)
{
}

template<typename A, typename B, typename C, typename D, typename E>
void unlock(A a, int32 io1, B b, int32 io2, C c, int32 io3, D d, int32 io4, E e, int32 io5)
{
}

} }

#endif /* THOR_LANG_PROCESS_H_ */
