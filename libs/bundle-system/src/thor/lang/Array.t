/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import . = thor.unmanaged;

/**
 * @brief Iterator iterates over the whole array.
 *
 * Thor provides this class in order to support iteration on array with
 * foreach loop. User may not want to use this class directly. @n
 *
 * The following code shows how to use iterator directly:
 * @code
 * var arr: Array<int32> = new Array(1234);
 * ... // initialize array data
 *
 * for (var i: ArrayIterator<int32> = arr.iterator(); i.hasNext(); )
 * {
 *     var element: int32 = i.next();
 *     print("element = \{element}\n");
 * }
 * @endcode
 *
 * @see Array<T>
 */
@cpu
@native { include = "thor/lang/Array.h" }
class ArrayIterator<E> extends Object
{
    /**
     * @brief Constructor initializes data to iterate over the whole array.
     */
    @native
    public function new(c : Array<E>) : void;

    /**
     * @brief Destructor releases resources for iteration.
     */
    @native
    public function delete() : void;

    /**
     * @brief Test if there is remained element to be iterated.
     *
     * @return Boolean value indicates there is remained element.
     * @retval true  There is remained element.
     * @retval false No more element to be iterated.
     *
     * @see next()
     */
    @native
    public function hasNext() : bool;

    /**
     * @brief Get next element in iteration.
     *
     * @return The next element.
     *
     * @remark The return value is undefined if hasNext() returns @c false.
     *
     * @see hasNext()
     */
    @native
    public function next() : E;

    /**
     * @brief Array to be iterated.
     */
    private var target : Array<E>;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var current: ptr_<int8>;
    private var end    : ptr_<int8>;
    /**
     * @endcond
     */
}

/**
 * @brief Dynamic allocated array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see ArrayIterator<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates array with zero-length.
     *
     * @see new(int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates array with given element count.
     *
     * @arg size The number of elements that allocated array should contain.
     *
     * @see new()
     */
    @native
    public function new(size:int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0:int64) : Array<T>
    {
        return new Array<T>(size_0);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * By shallow copy, the underlying elements is copied instead of cloned.
     * That is, the new instance contains the same references for object types
     * and the same values for other types.
     *
     * For example, with the following code:
     * @code
     * import thor.util;
     *
     * class Foo
     * {
     *     public var value: int32 = 0;
     * }
     *
     * function get_foo_value(f: Foo): String
     * {
     *     if (f == null)
     *         return "null";
     *     else
     *         return thor.util.Convert.toString(f.value);
     * }
     *
     * function print_data(arr_int32: Array<int32>, arr_foo: Array<Foo>)
     * {
     *     var i0 = arr_int32[0];
     *     var i1 = arr_int32[1];
     *     var f0 = arr_foo  [0];
     *     var f1 = arr_foo  [1];
     *
     *     print("i0: " +               i0  + "\n");
     *     print("i1: " +               i1  + "\n");
     *     print("f0: " + get_foo_value(f0) + "\n");
     *     print("f2: " + get_foo_value(f1) + "\n");
     * }
     *
     * @entry
     * task test_entry()
     * {
     *     var arr_int32 =      [        0,              1 ];
     *     var arr_foo   = <Foo>[new Foo(), cast<Foo>(null)];
     *
     *     var cloned_arr_int32 = arr_int32.clone();
     *     var cloned_arr_foo   = arr_foo  .clone();
     *
     *     // modifying array element!
     *     cloned_arr_int32[0]       = 1;
     *     cloned_arr_foo  [0].value = 1;
     *
     *     print_data(       arr_int32,        arr_foo);
     *     print_data(cloned_arr_int32, cloned_arr_foo);
     *
     *     exit(0);
     * }
     * @endcode
     *
     * The output will be the following, you will see the first elements of
     * int32 arrays are different:
     * @verbatim
     * i0: 0
     * i1: 1
     * f0: 1
     * f2: null
     * i0: 1
     * i1: 1
     * f0: 1
     * f2: null
     * @endverbatim
     */
    @native
    public virtual function clone() : Array<T>;

    /**
     * @brief Get the element by index.
     *
     * @arg index The index of element.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, T)
     */
    @native
    public function get(index:int64):T;

    /**
     * @brief Set the element by index.
     *
     * @arg index The index of element.
     * @arg v     The value to set to.
     *
     * @see get(int64)
     */
    @native
    public function set(index:int64, v:T):void;

    /**
     * @brief Get the size of array.
     *
     * @return The size of array.
     */
    @native
    public function size():int64;

    /**
     * @brief Create iterator which iterates the whole array elements.
     *
     * @return The new iterator which is ready for iteration.
     */
    public function iterator() : ArrayIterator<T>
    {
        return new ArrayIterator<T>(this);
    }

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _data: ptr_<int8>;
    /**
     * @endcond
     */

    private var _size: int64; /**< @brief The size of array. */
}

/**
 * @brief Dynamic allocated 2-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array2D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 2-dimension array with zero-length.
     *
     * @see new(int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 2-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64) : Array2D<T>
    {
        return new Array2D<T>(size_0, size_1);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array2D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 2-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
}

/**
 * @brief Dynamic allocated 3-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array3D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 3-dimension array with zero-length.
     *
     * @see new(int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 3-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64) : Array3D<T>
    {
        return new Array3D<T>(size_0, size_1, size_2);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array3D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 3-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
}

/**
 * @brief Dynamic allocated 4-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array4D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 4-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 4-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64) : Array4D<T>
    {
        return new Array4D<T>(size_0, size_1, size_2, size_3);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array4D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 4-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
}

/**
 * @brief Dynamic allocated 5-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array5D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 5-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 5-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     * @arg size_4 The size of 5th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64) : Array5D<T>
    {
        return new Array5D<T>(size_0, size_1, size_2, size_3, size_4);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array5D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 5-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4 : int64; /**< @brief The 5th dimension size of array. */
}

/**
 * @brief Dynamic allocated 6-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array6D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 6-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 6-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4 * @c size_5.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     * @arg size_4 The size of 5th dimension.
     * @arg size_5 The size of 6th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64) : Array6D<T>
    {
        return new Array6D<T>(size_0, size_1, size_2, size_3, size_4, size_5);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array6D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 6-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4 : int64; /**< @brief The 5th dimension size of array. */
    private var _size_5 : int64; /**< @brief The 6th dimension size of array. */
}

/**
 * @brief Dynamic allocated 7-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array7D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 7-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 7-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4 * @c size_5 * @c size_6.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     * @arg size_4 The size of 5th dimension.
     * @arg size_5 The size of 6th dimension.
     * @arg size_6 The size of 7th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64) : Array7D<T>
    {
        return new Array7D<T>(size_0, size_1, size_2, size_3, size_4, size_5, size_6);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array7D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 7-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4 : int64; /**< @brief The 5th dimension size of array. */
    private var _size_5 : int64; /**< @brief The 6th dimension size of array. */
    private var _size_6 : int64; /**< @brief The 7th dimension size of array. */
}

/**
 * @brief Dynamic allocated 8-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array8D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 8-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 8-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4 * @c size_5 * @c size_6 * @c size_7.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     * @arg size_4 The size of 5th dimension.
     * @arg size_5 The size of 6th dimension.
     * @arg size_6 The size of 7th dimension.
     * @arg size_7 The size of 8th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64) : Array8D<T>
    {
        return new Array8D<T>(size_0, size_1, size_2, size_3, size_4, size_5, size_6, size_7);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array8D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg idx_7 The index of 8th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg idx_7 The index of 8th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 8-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4 : int64; /**< @brief The 5th dimension size of array. */
    private var _size_5 : int64; /**< @brief The 6th dimension size of array. */
    private var _size_6 : int64; /**< @brief The 7th dimension size of array. */
    private var _size_7 : int64; /**< @brief The 8th dimension size of array. */
}

/**
 * @brief Dynamic allocated 9-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array10D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array9D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 9-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 9-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4 * @c size_5 * @c size_6 * @c size_7 * @c size_8.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     * @arg size_4 The size of 5th dimension.
     * @arg size_5 The size of 6th dimension.
     * @arg size_6 The size of 7th dimension.
     * @arg size_7 The size of 8th dimension.
     * @arg size_8 The size of 9th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64, size_8 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64, size_8 : int64) : Array9D<T>
    {
        return new Array9D<T>(size_0, size_1, size_2, size_3, size_4, size_5, size_6, size_7, size_8);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array9D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg idx_7 The index of 8th dimension.
     * @arg idx_8 The index of 9th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, idx_8 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg idx_7 The index of 8th dimension.
     * @arg idx_8 The index of 9th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, idx_8 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 9-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4 : int64; /**< @brief The 5th dimension size of array. */
    private var _size_5 : int64; /**< @brief The 6th dimension size of array. */
    private var _size_6 : int64; /**< @brief The 7th dimension size of array. */
    private var _size_7 : int64; /**< @brief The 8th dimension size of array. */
    private var _size_8 : int64; /**< @brief The 9th dimension size of array. */
}

/**
 * @brief Dynamic allocated 10-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array11D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array10D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 10-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 10-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4 * @c size_5 * @c size_6 * @c size_7 * @c size_8 * @c size_9.
     *
     * @arg size_0 The size of 1st dimension.
     * @arg size_1 The size of 2nd dimension.
     * @arg size_2 The size of 3rd dimension.
     * @arg size_3 The size of 4th dimension.
     * @arg size_4 The size of 5th dimension.
     * @arg size_5 The size of 6th dimension.
     * @arg size_6 The size of 7th dimension.
     * @arg size_7 The size of 8th dimension.
     * @arg size_8 The size of 9th dimension.
     * @arg size_8 The size of 9th dimension.
     * @arg size_9 The size of 10th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64, size_8 : int64, size_9 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64, size_8 : int64, size_9 : int64) : Array10D<T>
    {
        return new Array10D<T>(size_0, size_1, size_2, size_3, size_4, size_5, size_6, size_7, size_8, size_9);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array10D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg idx_7 The index of 8th dimension.
     * @arg idx_8 The index of 9th dimension.
     * @arg idx_9 The index of 10th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, idx_8 : int64, idx_9 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0 The index of 1st dimension.
     * @arg idx_1 The index of 2nd dimension.
     * @arg idx_2 The index of 3rd dimension.
     * @arg idx_3 The index of 4th dimension.
     * @arg idx_4 The index of 5th dimension.
     * @arg idx_5 The index of 6th dimension.
     * @arg idx_6 The index of 7th dimension.
     * @arg idx_7 The index of 8th dimension.
     * @arg idx_8 The index of 9th dimension.
     * @arg idx_9 The index of 10th dimension.
     * @arg v     The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, idx_8 : int64, idx_9 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 10-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0 : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1 : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2 : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3 : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4 : int64; /**< @brief The 5th dimension size of array. */
    private var _size_5 : int64; /**< @brief The 6th dimension size of array. */
    private var _size_6 : int64; /**< @brief The 7th dimension size of array. */
    private var _size_7 : int64; /**< @brief The 8th dimension size of array. */
    private var _size_8 : int64; /**< @brief The 9th dimension size of array. */
    private var _size_9 : int64; /**< @brief The 10th dimension size of array. */
}

/**
 * @brief Dynamic allocated 11-dimension array in run-time.
 *
 * This class represents array and provides functionality to access contained
 * elements. For example.
 * - get/set elements through index.
 * - iterate elements through iterator.
 * - etc.
 * .
 *
 * @see Array<T>
 * @see Array2D<T>
 * @see Array3D<T>
 * @see Array4D<T>
 * @see Array5D<T>
 * @see Array6D<T>
 * @see Array7D<T>
 * @see Array8D<T>
 * @see Array9D<T>
 * @see Array10D<T>
 */
@cpu
@native { include="thor/lang/Array.h" }
class Array11D<T> extends Object implements GarbageCollectable, Cloneable
{
    /**
     * @brief Default constructor allocates 11-dimension array with zero-length.
     *
     * @see new(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    private function new():void;

    /**
     * @brief Constructor allocates 11-dimension array with given element count which is computed by dimensions.
     *
     * The element count is equal to @c size_0 * @c size_1 * @c size_2 * @c size_3 * @c size_4 * @c size_5 * @c size_6 * @c size_7 * @c size_8 * @c size_9 * @c size_10.
     *
     * @arg size_0  The size of 1st dimension.
     * @arg size_1  The size of 2nd dimension.
     * @arg size_2  The size of 3rd dimension.
     * @arg size_3  The size of 4th dimension.
     * @arg size_4  The size of 5th dimension.
     * @arg size_5  The size of 6th dimension.
     * @arg size_6  The size of 7th dimension.
     * @arg size_7  The size of 8th dimension.
     * @arg size_8  The size of 9th dimension.
     * @arg size_8  The size of 9th dimension.
     * @arg size_9  The size of 10th dimension.
     * @arg size_10 The size of 11th dimension.
     *
     * @see new()
     */
    @native
    public function new(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64, size_8 : int64, size_9 : int64, size_10 : int64):void;

    /**
     * @brief Destructor releases underlying allocated memory.
     */
    @native
    public function delete():void;

    /**
     * @cond THOR_WORKAROUNDS
     *
     * @brief Provides functionality to create array in C++.
     *
     * @remark This function is current workaround to create object through C++.
     */
    public static function create(size_0 : int64, size_1 : int64, size_2 : int64, size_3 : int64, size_4 : int64, size_5 : int64, size_6 : int64, size_7 : int64, size_8 : int64, size_9 : int64, size_10 : int64) : Array11D<T>
    {
        return new Array11D<T>(size_0, size_1, size_2, size_3, size_4, size_5, size_6, size_7, size_8, size_9, size_10);
    }
    /**
     * @endcond
     */

    /**
     * @brief Create new instance using shallow copy.
     *
     * @see Array<T>.clone()
     */
    @native
    public virtual function clone() : Array11D<T>;

    /**
     * @brief Get the element by indices.
     *
     * @arg idx_0  The index of 1st dimension.
     * @arg idx_1  The index of 2nd dimension.
     * @arg idx_2  The index of 3rd dimension.
     * @arg idx_3  The index of 4th dimension.
     * @arg idx_4  The index of 5th dimension.
     * @arg idx_5  The index of 6th dimension.
     * @arg idx_6  The index of 7th dimension.
     * @arg idx_7  The index of 8th dimension.
     * @arg idx_8  The index of 9th dimension.
     * @arg idx_9  The index of 10th dimension.
     * @arg idx_10 The index of 11th dimension.
     *
     * @return The element indicated by index.
     *
     * @see set(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, T)
     */
    @native
    public function get(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, idx_8 : int64, idx_9 : int64, idx_10 : int64):T;

    /**
     * @brief Set the element by indices.
     *
     * @arg idx_0  The index of 1st dimension.
     * @arg idx_1  The index of 2nd dimension.
     * @arg idx_2  The index of 3rd dimension.
     * @arg idx_3  The index of 4th dimension.
     * @arg idx_4  The index of 5th dimension.
     * @arg idx_5  The index of 6th dimension.
     * @arg idx_6  The index of 7th dimension.
     * @arg idx_7  The index of 8th dimension.
     * @arg idx_8  The index of 9th dimension.
     * @arg idx_9  The index of 10th dimension.
     * @arg idx_10 The index of 11th dimension.
     * @arg v      The value to set to.
     *
     * @see get(int64, int64, int64, int64, int64, int64, int64, int64, int64, int64, int64)
     */
    @native
    public function set(idx_0 : int64, idx_1 : int64, idx_2 : int64, idx_3 : int64, idx_4 : int64, idx_5 : int64, idx_6 : int64, idx_7 : int64, idx_8 : int64, idx_9 : int64, idx_10 : int64, v:T):void;

    /**
     * @brief Get the size of specific dimension of 11-dimension array.
     *
     * @arg idx The index of dimension.
     *
     * @return The size of @c idx dimension.
     *
     * @remark Dimension index is 0-indexed.
     */
    @native
    public function size(idx : int64):int64;

    /**
     * @brief Implement this function to make garbage collector know the underlying objects.
     *
     * @arg o C++ only interface which collects objects referenced by native classes.
     */
    @native
    private virtual function getContainedObjects(o: CollectableObject) : void;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _internal1:int64;
    /**
     * @endcond
     */

    private var _size_0  : int64; /**< @brief The 1st dimension size of array. */
    private var _size_1  : int64; /**< @brief The 2nd dimension size of array. */
    private var _size_2  : int64; /**< @brief The 3rd dimension size of array. */
    private var _size_3  : int64; /**< @brief The 4th dimension size of array. */
    private var _size_4  : int64; /**< @brief The 5th dimension size of array. */
    private var _size_5  : int64; /**< @brief The 6th dimension size of array. */
    private var _size_6  : int64; /**< @brief The 7th dimension size of array. */
    private var _size_7  : int64; /**< @brief The 8th dimension size of array. */
    private var _size_8  : int64; /**< @brief The 9th dimension size of array. */
    private var _size_9  : int64; /**< @brief The 10th dimension size of array. */
    private var _size_10 : int64; /**< @brief The 11th dimension size of array. */
}
