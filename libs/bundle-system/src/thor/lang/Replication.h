/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_REPLICATION_H_
#define THOR_LANG_REPLICATION_H_

#include <cstddef>

#include <deque>
#include <utility>
#include <map>
#include <memory>

#include "thor/lang/Language.h"

namespace thor { namespace lang {

class ReplicationBase : public Object
{
public:
    // helper types for primitive type headers
    enum class PrimitiveTypeHeader : int8
    {
        Bool, Int8, Int16, Int32, Int64,
        Float32, Float64, Raw,
        PrimitiveTypeHeaderEnd,
    };

    // helper types for object related markers
    enum class ObjectMarker : int8
    {
        Null = static_cast<int8>(PrimitiveTypeHeader::PrimitiveTypeHeaderEnd),
        Reference, Begin, End,
        ObjectMarkerEnd,
    };

    ReplicationBase();
    explicit ReplicationBase(std::unique_ptr<std::deque<int8>>&& new_content);
    ~ReplicationBase();

protected:
    std::unique_ptr<std::deque<int8>> content;

    static_assert(sizeof(content) == sizeof(std::intptr_t), "data member should have same size as raw pointer");
};

class ReplicationEncoder : public ReplicationBase
{
public:
    ReplicationEncoder();
    ~ReplicationEncoder() override;

    bool put(bool b);

    bool put(int8  i8 );
    bool put(int16 i16);
    bool put(int32 i32);
    bool put(int64 i64);

    bool put(float32 f32);
    bool put(float64 f64);

    bool put(Object* o);

    // C++ only interface to put raw data
    bool put(std::size_t count, const void* buffer);

    // C++ only interface to exchange internal buffer
    std::unique_ptr<std::deque<int8>> stealContent() noexcept;

private:
    using ObjectToIdMapType = std::map<Object*, std::size_t>;

    std::pair<bool, std::size_t> getSerializationId(Object* object);
    bool putReference(std::size_t id);
    bool putNull();

    std::unique_ptr<ObjectToIdMapType> object_to_id;

    static_assert(sizeof(object_to_id) == sizeof(std::intptr_t), "data member should have same size as raw pointer");
};

class ReplicationDecoder : public ReplicationBase
{
public:
    ReplicationDecoder();
    explicit ReplicationDecoder(ReplicationEncoder* encoder);
    ~ReplicationDecoder() override;

    static ReplicationDecoder* __create();

    bool isEnded();

    void setContent(std::unique_ptr<std::deque<int8>>&& new_content);

    template<typename T> bool               isNext(T unused);
    template<typename T> std::pair<bool, T> nativeGet(T);

#define DECALRE_DECODER_IS_AND_GET(type, name)                     \
public:                                                            \
    bool                             isNext ## name();             \
    std::pair<bool, type>            nativeGet ## name();          \
private:                                                           \
    type                             nativeGet ## name ##Impl()

    DECALRE_DECODER_IS_AND_GET(bool   , Bool   );
    DECALRE_DECODER_IS_AND_GET(int8   , Int8   );
    DECALRE_DECODER_IS_AND_GET(int16  , Int16  );
    DECALRE_DECODER_IS_AND_GET(int32  , Int32  );
    DECALRE_DECODER_IS_AND_GET(int64  , Int64  );
    DECALRE_DECODER_IS_AND_GET(float32, Float32);
    DECALRE_DECODER_IS_AND_GET(float64, Float64);

#undef DECALRE_DECODER_IS_AND_GET

public:
    bool                     isNextNull();
    bool                     isNextObject();
    std::pair<bool, Object*> nativeGetObject();
private:
    Object*                  nativeGetObjectImpl();
    void                     eatNullMark();

public:
    // C++ only interface to get raw data
    std::pair<std::size_t, bool> eatRawBegin();
    std::size_t                  getRawData(std::size_t count, void* buffer);

private:
    using IdToObjectMapType = std::map<std::size_t, Object*>;

    void registerDeserialized(Object* object);

    std::unique_ptr<IdToObjectMapType>                id_to_object;
    std::unique_ptr<std::deque<int8>::const_iterator> read_pos;

    static_assert(sizeof(id_to_object) == sizeof(std::intptr_t), "data member should have same size as raw pointer");
    static_assert(sizeof(read_pos)     == sizeof(std::intptr_t), "data member should have same size as raw pointer");
};


} }

#endif /* THOR_LANG_REPLICATION_H_ */
