/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_DEBUG_H_
#define THOR_LANG_DEBUG_H_

#ifdef __CUDACC__
    #include "thor/Thor.cuh"
    #define CUDA_DEVICE __device__
#else // __CUDACC__
    #include "thor/Thor.h"
    #include "thor/lang/String.h"
    #define CUDA_DEVICE
#endif // __CUDACC__

namespace thor { namespace lang {

CUDA_DEVICE void print(bool v);
CUDA_DEVICE void print(int8 v);
CUDA_DEVICE void print(int16 v);
CUDA_DEVICE void print(int32 v);
CUDA_DEVICE void print(int64 v);
CUDA_DEVICE void print(float32 v);
CUDA_DEVICE void print(float64 v);
#ifndef __CUDACC__
CUDA_DEVICE void print(String* v);
#endif // __CUDACC__
CUDA_DEVICE void println(bool v);
CUDA_DEVICE void println(int8 v);
CUDA_DEVICE void println(int16 v);
CUDA_DEVICE void println(int32 v);
CUDA_DEVICE void println(int64 v);
CUDA_DEVICE void println(float32 v);
CUDA_DEVICE void println(float64 v);
#ifndef __CUDACC__
CUDA_DEVICE void println(String* v);
#endif // __CUDACC__

} }

#endif /* THOR_LANG_DEBUG_H_ */

