/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

import . = thor.unmanaged;

/**
 * @brief Base class for lambda holders.
 *
 * This class is used internally and do not provide any functionality for
 * Thor. So, user may get wrong to start if trying to use this class.
 */
@cpu
@native { include = "thor/lang/Lambda.h" }
class Lambda extends Object
{
}

/**
 * @brief Lambda holder for zero-argument lambdas which returns @a R.
 *
 * The lambda holder is provided to define variable type for lambdas. For
 * example:
 * @code
 * var lambda_1: Lambda0<void>;
 * var lambda_2: lambda(): void; // equivalent to Lambda0<void>
 * @endcode
 *
 * @remark It's suggested to use keyword one (on @a lambda_2) to declare lambda
 *         holders.
 *
 * @see Lambda1<R, T0>
 * @see Lambda2<R, T0, T1>
 */
@cpu
@native { include = "thor/lang/Lambda.h" }
class Lambda0<R> extends Lambda
{
    /**
     * @brief Constructor creates lambda holders according to the given lambda object.
     *
     * This constructor is provided for lambda feature provided by Thor.
     * Users should not use this function directly.
     */
    @native
    public function new<T>(o: T): void;

    /**
     * @brief Destructor releases referenced lambda object.
     */
    @native
    public function delete(): void;

    /**
     * @brief Helper to invoke the real lambda function from lambda object.
     *
     * User could call the real lambda function by the following code:
     * @code
     * var l = lambda(): void { ... };
     * l();        // equivalent to @c l.invoke()
     * l.invoke(); // equivalent to @c l()
     * @endcode
     *
     * @remark Thor will generate ".invoke" for objects, that is,
     *         similar functionality of functors, by providing this function.
     */
    @native
    @export
    public function invoke(): R;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _invoke : int64;     // function pointer
    private var _obj    : Object;    // lambda object
    /**
     * @endcond
     */
}

/**
 * @brief Lambda holder for one-argument lambdas which returns @a R.
 *
 * The lambda holder is provided to define variable type for lambdas. For
 * example:
 * @code
 * var lambda_1: Lambda1<void, int32>;
 * var lambda_2: lambda(int32): void;  // equivalent to Lambda1<void, int32>
 * @endcode
 *
 * @remark It's suggested to use keyword one (on @a lambda_2) to declare lambda
 *         holders.
 *
 * @see Lambda0<R>
 * @see Lambda2<R, T0, T1>
 */
@cpu
@native { include = "thor/lang/Lambda.h" }
class Lambda1<R, T0> extends Lambda
{
    /**
     * @brief Constructor creates lambda holders according to the given lambda object.
     *
     * This constructor is provided for lambda feature provided by Thor.
     * Users should not use this function directly.
     */
    @native
    public function new<T>(o: T): void;

    /**
     * @brief Destructor releases referenced lambda object.
     */
    @native
    public function delete(): void;

    /**
     * @brief Helper to invoke the real lambda function from lambda object.
     *
     * User could call the real lambda function by the following code:
     * @code
     * var l = lambda(int32): void { ... };
     * l(1);        // equivalent to @c l.invoke(1)
     * l.invoke(1); // equivalent to @c l(1)
     * @endcode
     *
     * @remark Thor will generate ".invoke" for objects, that is,
     *         similar functionality of functors, by providing this function.
     */
    @native
    @export
    public function invoke(t0: T0): R;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _invoke : int64;     // function pointer
    private var _obj    : Object;    // lambda object
    /**
     * @endcond
     */
}

/**
 * @brief Lambda holder for two-argument lambdas which returns @a R.
 *
 * The lambda holder is provided to define variable type for lambdas. For
 * example:
 * @code
 * var lambda_1: Lambda2<void, int32, float32>;
 * var lambda_2: lambda(int32, float32): void;  // equivalent to Lambda2<void, int32, float32>
 * @endcode
 *
 * @remark It's suggested to use keyword one (on @a lambda_2) to declare lambda
 *         holders.
 *
 * @see Lambda0<R>
 * @see Lambda1<R, T0>
 */
@cpu
@native { include = "thor/lang/Lambda.h" }
class Lambda2<R, T0, T1> extends Lambda
{
    /**
     * @brief Constructor creates lambda holders according to the given lambda object.
     *
     * This constructor is provided for lambda feature provided by Thor.
     * Users should not use this function directly.
     */
    @native
    public function new<T>(o: T): void;

    /**
     * @brief Destructor releases referenced lambda object.
     */
    @native
    public function delete(): void;

    /**
     * @brief Helper to invoke the real lambda function from lambda object.
     *
     * User could call the real lambda function by the following code:
     * @code
     * var l = lambda(int32, float32): void { ... };
     * l(1, 1.1f);        // equivalent to @c l.invoke(1, 1.1f)
     * l.invoke(1, 1.1f); // equivalent to @c l(1, 1.1f)
     * @endcode
     *
     * @remark Thor will generate ".invoke" for objects, that is,
     *         similar functionality of functors, by providing this function.
     */
    @native
    @export
    public function invoke(t0: T0, t1: T1): R;

    /**
     * @cond THOR_PRIVATE_IMPLEMENTATION
     */
    private var _invoke : int64;     // function pointer
    private var _obj    : Object;    // lambda object
    /**
     * @endcond
     */
}
