/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_ARRAY_H_
#define THOR_LANG_ARRAY_H_

#include <cstdlib>

#include <array>
#include <numeric>
#include <algorithm>
#include <functional>
#include <cstring>

#include "thor/Thor.h"
#include "thor/lang/Language.h"
#include "thor/lang/GarbageCollectable.h"
#include "thor/util/Macro.h"

#define PP_INDEX(x, y) x[y]

#define PP_REPEAT(n, macro) PP_CAT(PP_REPEAT_, n)(macro)
#define PP_REPEAT_1( macro)                     macro( 1)
#define PP_REPEAT_2( macro) PP_REPEAT_1( macro) macro( 2)
#define PP_REPEAT_3( macro) PP_REPEAT_2( macro) macro( 3)
#define PP_REPEAT_4( macro) PP_REPEAT_3( macro) macro( 4)
#define PP_REPEAT_5( macro) PP_REPEAT_4( macro) macro( 5)
#define PP_REPEAT_6( macro) PP_REPEAT_5( macro) macro( 6)
#define PP_REPEAT_7( macro) PP_REPEAT_6( macro) macro( 7)
#define PP_REPEAT_8( macro) PP_REPEAT_7( macro) macro( 8)
#define PP_REPEAT_9( macro) PP_REPEAT_8( macro) macro( 9)
#define PP_REPEAT_10(macro) PP_REPEAT_9( macro) macro(10)
#define PP_REPEAT_11(macro) PP_REPEAT_10(macro) macro(11)

#define PP_MAKE_ARRAY_NAME(rank) PP_CAT(PP_MAKE_ARRAY_NAME_, rank)()
#define PP_MAKE_ARRAY_NAME_1()  Array
#define PP_MAKE_ARRAY_NAME_2()  Array2D
#define PP_MAKE_ARRAY_NAME_3()  Array3D
#define PP_MAKE_ARRAY_NAME_4()  Array4D
#define PP_MAKE_ARRAY_NAME_5()  Array5D
#define PP_MAKE_ARRAY_NAME_6()  Array6D
#define PP_MAKE_ARRAY_NAME_7()  Array7D
#define PP_MAKE_ARRAY_NAME_8()  Array8D
#define PP_MAKE_ARRAY_NAME_9()  Array9D
#define PP_MAKE_ARRAY_NAME_10() Array10D
#define PP_MAKE_ARRAY_NAME_11() Array11D

#define PP_STATIC_IF(cond) PP_STATIC_IF_(cond)
#define PP_STATIC_IF_(cond) PP_STATIC_IF__(cond)
#define PP_STATIC_IF__(cond) PP_CAT(PP_BLOCK_, cond)

#define PP_BLOCK_0(codes)
#define PP_BLOCK_1(codes) codes

#define PP_ARRAY_ENABLE(rank) PP_CAT(PP_ARRAY_ENABLE_, rank)()
#define PP_ARRAY_ENABLE_1()  1
#define PP_ARRAY_ENABLE_2()  0
#define PP_ARRAY_ENABLE_3()  0
#define PP_ARRAY_ENABLE_4()  0
#define PP_ARRAY_ENABLE_5()  0
#define PP_ARRAY_ENABLE_6()  0
#define PP_ARRAY_ENABLE_7()  0
#define PP_ARRAY_ENABLE_8()  0
#define PP_ARRAY_ENABLE_9()  0
#define PP_ARRAY_ENABLE_10() 0
#define PP_ARRAY_ENABLE_11() 0

#define PP_MAKE_ARRAY_TEMPLATE(rank)                                                      \
    template <typename T>                                                                 \
    class PP_MAKE_ARRAY_NAME(rank)                                                        \
    : public thor::lang::Object,                                                          \
      public thor::lang::GarbageCollectable,                                              \
      public thor::lang::Cloneable                                                        \
    {                                                                                     \
        static_assert(                                                                    \
            thor::lang::is_non_void_thor_type<T>::value,                            \
            "invalid template type argument"                                              \
        );                                                                                \
                                                                                          \
        using value_type = T;                                                             \
                                                                                          \
        value_type*              _data;                                                   \
        std::array<int64, rank> _sizes;                                                   \
                                                                                          \
        PP_MAKE_ARRAY_NAME(rank)()                                                        \
        : _data{nullptr}                                                                  \
        { }                                                                               \
                                                                                          \
        int64 all_size() {                                                                \
            return std::accumulate(                                                       \
                _sizes.begin(), _sizes.end(),                                             \
                 1, std::multiplies<int64>()                                              \
            );                                                                            \
        }                                                                                 \
                                                                                          \
        int64 calculate_index(PP_MAKE_LIST(rank, int64 idx_, PP_CAT)) {                   \
            /* index in mapped 1-d array is calculated from formula:    */                \
            /*     (((idx_0 * size1) + idx_1) * size2) + idx_2) ...     */                \
            /* NOTE: not count in size0                                 */                \
            std::array<int64, rank> indices{{PP_MAKE_LIST(rank, idx_, PP_CAT)}};          \
            auto next_idx = indices.begin();                                              \
            return std::accumulate(                                                       \
                _sizes.begin() + 1, _sizes.end(), *next_idx++,                            \
                [&](int64 result, int64 next_size) {                                      \
                    return (result * next_size) + *next_idx++;                            \
                }                                                                         \
            );                                                                            \
        }                                                                                 \
                                                                                          \
        virtual void getContainedObjects(thor::lang::CollectableObject* collect) {        \
            collect->add(_data, _data + all_size());                                      \
        }                                                                                 \
    public:                                                                               \
        PP_MAKE_ARRAY_NAME(rank)(PP_MAKE_LIST(rank, int64 size_, PP_CAT))                 \
        : _sizes{{PP_MAKE_LIST(rank, size_, PP_CAT)}} {                                   \
            const std::size_t total_size = sizeof(value_type) * all_size();               \
            _data = (value_type*)::malloc(total_size);                                    \
            ::memset(_data, total_size, 0);                                               \
        }                                                                                 \
                                                                                          \
        ~PP_MAKE_ARRAY_NAME(rank)() {                                                     \
            ::free((void*)_data);                                                         \
            _sizes.fill(0);                                                               \
        }                                                                                 \
                                                                                          \
        static PP_MAKE_ARRAY_NAME(rank)* create(PP_MAKE_LIST(rank, int64 size_, PP_CAT)); \
                                                                                          \
        virtual PP_MAKE_ARRAY_NAME(rank)* clone() {                                       \
            PP_MAKE_ARRAY_NAME(rank)* ret = create(PP_MAKE_LIST(rank, _sizes, PP_INDEX)); \
            std::copy(_data, _data + all_size(), ret->_data);                             \
            return ret;                                                                   \
        }                                                                                 \
                                                                                          \
        value_type get(PP_MAKE_LIST(rank, int64 idx_, PP_CAT)) {                          \
            return _data[calculate_index(PP_MAKE_LIST(rank, idx_, PP_CAT))];              \
        }                                                                                 \
                                                                                          \
        void set(PP_MAKE_LIST(rank, int64 idx_, PP_CAT), value_type v) {                  \
            _data[calculate_index(PP_MAKE_LIST(rank, idx_, PP_CAT))] = v;                 \
        }                                                                                 \
                                                                                          \
        int64 size() {                                                                    \
            return all_size();                                                            \
        }                                                                                 \
                                                                                          \
        int64 size(int64 dim) {                                                           \
            if(0 <= dim && dim < rank)                                                    \
                return _sizes[dim];                                                       \
            return 0;                                                                     \
        }                                                                                 \
                                                                                          \
        /* following should be enable only in Array<T>  */                                \
        PP_STATIC_IF(PP_ARRAY_ENABLE(rank)) (                                             \
            friend ArrayIterator<T>;                                                      \
        )                                                                                 \
    };

namespace thor { namespace lang {

template < typename T >
class Array;

template < typename T >
class ArrayIterator : public thor::lang::Object
{
    static_assert(thor::lang::is_non_void_thor_type<T>::value, "invalid template type argument");

public:
    using value_type      = T;
    using array_iter_type = value_type*;

    ArrayIterator(Array<T>* c)
    : target( c )
    {
        current = target->_data;
        end = target->_data + target->_sizes[0];
    }

    virtual ~ArrayIterator() { }

    static ArrayIterator<T>* create(Array<T>* c);

    bool hasNext()
    {
        return current != end;
    }

    value_type next()
    {
        return *current++;
    }

private:
    Array<value_type>* target;
    array_iter_type current;
    array_iter_type end;
};

PP_REPEAT(11, PP_MAKE_ARRAY_TEMPLATE)

} } // namespace thor::lang

// undef MACRO symbols
#undef PP_INDEX

#undef PP_REPEAT
#undef PP_REPEAT_1
#undef PP_REPEAT_2
#undef PP_REPEAT_3
#undef PP_REPEAT_4
#undef PP_REPEAT_5
#undef PP_REPEAT_6
#undef PP_REPEAT_7
#undef PP_REPEAT_8
#undef PP_REPEAT_9
#undef PP_REPEAT_10
#undef PP_REPEAT_11

#undef PP_MAKE_ARRAY_NAME
#undef PP_MAKE_ARRAY_NAME_1
#undef PP_MAKE_ARRAY_NAME_2
#undef PP_MAKE_ARRAY_NAME_3
#undef PP_MAKE_ARRAY_NAME_4
#undef PP_MAKE_ARRAY_NAME_5
#undef PP_MAKE_ARRAY_NAME_6
#undef PP_MAKE_ARRAY_NAME_7
#undef PP_MAKE_ARRAY_NAME_8
#undef PP_MAKE_ARRAY_NAME_9
#undef PP_MAKE_ARRAY_NAME_10
#undef PP_MAKE_ARRAY_NAME_11

#undef PP_STATIC_IF
#undef PP_STATIC_IF_
#undef PP_STATIC_IF__

#undef PP_BLOCK_0
#undef PP_BLOCK_1

#undef PP_ARRAY_ENABLE
#undef PP_ARRAY_ENABLE_1
#undef PP_ARRAY_ENABLE_2
#undef PP_ARRAY_ENABLE_3
#undef PP_ARRAY_ENABLE_4
#undef PP_ARRAY_ENABLE_5
#undef PP_ARRAY_ENABLE_6
#undef PP_ARRAY_ENABLE_7
#undef PP_ARRAY_ENABLE_8
#undef PP_ARRAY_ENABLE_9
#undef PP_ARRAY_ENABLE_10
#undef PP_ARRAY_ENABLE_11

#undef PP_MAKE_ARRAY_TEMPLATE 

#endif /* THOR_LANG_ARRAY_H_ */
