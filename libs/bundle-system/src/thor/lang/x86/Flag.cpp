/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <climits>

#include <memory>

#include <boost/program_options.hpp>
#include <boost/algorithm/string/join.hpp>

#include "thor/lang/Flag.h"

#include "framework/proxies/X86.h"

namespace po = boost::program_options;
using thor::container::Vector;

namespace thor { namespace lang {

namespace proxies_x86 = ::zillians::framework::proxies::x86;

namespace
{
    std::string ws_to_s(const std::wstring& ws)
    {
        size_t buf_size = ws.size()*MB_LEN_MAX;
        std::unique_ptr<char []> buf(new char[buf_size]);
        wcstombs(buf.get(),ws.c_str(),buf_size);

        return std::string(buf.get());
    }

    std::wstring s_to_ws(const std::string& s)
    {
        size_t buf_size = s.size();
        std::unique_ptr<wchar_t []> buf(new wchar_t[buf_size]);
        mbstowcs(buf.get(),s.c_str(),buf_size);

        return std::wstring(buf.get());
    }

    Vector<String*>* vector_to_thor_vector(const std::vector<std::wstring>& vec)
    {
        Vector<String*>* thor_vec = Vector<String*>::create();
        for(auto& val : vec)
        {
            thor_vec->pushBack(String::fromString(val));
        }
        return thor_vec;
    }
}

struct Flag::FlagImpl
{
    FlagImpl() : option("Available Options")
    {
        option.add_options()
            ("help", "produce help message");
    }
    
    template<class T>
    bool addOption(String* name, String* description, bool required)
    {
        std::string sname = ws_to_s(*name->data);
        if(!isDefined(sname))
        {
            if(required)
            {
                option.add_options()
                    (sname.c_str(), po::wvalue<T>()->required(), ws_to_s(*description->data).c_str());
            }
            else
            {
                option.add_options()
                    (sname.c_str(), po::wvalue<T>(), ws_to_s(*description->data).c_str());
            }

            return true;
        }
        return false;
    }

    template<class T>
    bool addPositionalOption(String* name, String* description, int count, bool required)
    {
        std::string sname = ws_to_s(*name->data);
        if(addOption<std::vector<T>>(name,description,required))
        {
            pos_option.add(sname.c_str(),count);
            return true;
        }
        return false;
    }

    template<class T>
    T get(String* name)
    {
        std::string name_str = ws_to_s(*name->data);
        try
        {
            if(flag_val.count(name_str))
            {
                return flag_val[name_str].as<T>();
            }
            else
            {
                return T();
            }
        }
        catch(const boost::bad_any_cast& e)
        {
            std::cerr<<"thor.lang.Flag::get(): access type error: "<<name_str<<std::endl;
            return T();
        }
    }

    bool isDefined(const std::string& name)
    {
        return option.find_nothrow(name,false) != nullptr;
    }

    po::options_description option;
    po::positional_options_description pos_option;
    po::variables_map flag_val;
};

Flag::Flag() : impl(new FlagImpl)
{
}

Flag::~Flag()
{
}

bool Flag::createInteger(String* name, String* description, bool required)
{
    return impl->addOption<int64>(name,description,required);
}

bool Flag::createFloat(String* name, String* description, bool required)
{
    return impl->addOption<double>(name,description,required);
}

bool Flag::createString(String* name, String* description, bool required)
{
    return impl->addOption<std::wstring>(name,description,required);
}

bool Flag::createPositionalInteger(String* name, String* description, int32 count, bool required)
{
    return impl->addPositionalOption<int64>(name,description,count,required);
}

bool Flag::createPositionalFloat(String* name, String* description, int32 count, bool required)
{
    return impl->addPositionalOption<double>(name,description,count,required);
}

bool Flag::createPositionalString(String* name, String* description, int32 count, bool required)
{
    return impl->addPositionalOption<std::wstring>(name,description,count,required);
}

int64 Flag::getInteger(String* name)
{
    return impl->get<int64>(name);
}

double Flag::getFloat(String* name)
{
    return impl->get<double>(name);
}

String* Flag::getString(String* name)
{
    return String::fromString(impl->get<std::wstring>(name));
}

Vector<int64>* Flag::getPositionalInteger(String* name)
{
    Vector<int64>* thor_vec = Vector<int64>::create();
    std::vector<int64> vec = impl->get<std::vector<int64>>(name);
    for(auto& val : vec)
    {
        thor_vec->pushBack(val);
    }
    return thor_vec;
}

Vector<double>* Flag::getPositionalFloat(String* name)
{
    Vector<double>* thor_vec = Vector<double>::create();
    std::vector<double> vec = impl->get<std::vector<double>>(name);
    for(auto& val : vec)
    {
        thor_vec->pushBack(val);
    }
    return thor_vec;
}

Vector<String*>* Flag::getPositionalString(String* name)
{
    return vector_to_thor_vector(impl->get<std::vector<std::wstring>>(name));
}

Vector<String*>* Flag::getRaw()
{
    const auto& args = proxies_x86::get_arguments();

    return vector_to_thor_vector(args);
}

bool Flag::parse()
{
    try
    {
        const std::vector<std::wstring>& wargs = proxies_x86::get_arguments();
        po::store(po::basic_command_line_parser<wchar_t>(wargs).
                options(impl->option).positional(impl->pos_option).run(),impl->flag_val);
        po::notify(impl->flag_val);
    }
    catch(const po::error &e)
    {
        std::cerr << "thor.lang.Flag parse error: "<< e.what() << "\n";
        return false;
    }

    return true;
}

bool Flag::has(String* name)
{
    std::string name_str = ws_to_s(*name->data);
    return impl->flag_val.count(name_str) != 0;
}

String* Flag::help()
{
    std::ostringstream oss;
    oss << impl->option << std::endl;
    return String::fromString(s_to_ws(oss.str()));
}

} }
