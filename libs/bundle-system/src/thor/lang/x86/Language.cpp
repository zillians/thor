/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <memory>

#include "thor/lang/Language.h"
#include "thor/lang/Domain.h"
#include "thor/lang/Process.h"

#include "framework/proxies/X86.h"

namespace thor { namespace lang {

namespace proxies_x86 = ::zillians::framework::proxies::x86;

Object::Object() {
}

Object::~Object() {
}

int64 Object::hash()
{
    return (int64)this;
}

Object* __createObject(int64 size, int64 type_id)
{
    assert(size > 0 && "object size must be greater than 0!");

    auto*const     ptr = proxies_x86::allocate_object(static_cast<std::size_t>(size), type_id);
    auto*const obj_ptr = reinterpret_cast<Object*>(ptr);

    return obj_ptr;
}

void __addToRootSet(int8* ptr)
{
    proxies_x86::gc_shared_root_set_add(reinterpret_cast<void**>(ptr));
}

void __removeFromRootSet(int8* ptr)
{
    proxies_x86::gc_shared_root_set_remove(reinterpret_cast<void**>(ptr));
}

void __addToGlobalRootSet(int8* ptr)
{
    proxies_x86::gc_global_root_set_add(reinterpret_cast<void**>(ptr));
}

void __removeFromGlobalRootSet(int8* ptr)
{
    proxies_x86::gc_global_root_set_remove(reinterpret_cast<void**>(ptr));
}

// dynamic cast
byte* __dynCastImpl(byte* object, int64 target_type_id)
{
    return reinterpret_cast<byte*>(proxies_x86::dyn_cast(object, target_type_id));
}

} }
