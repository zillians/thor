/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>

#include <utility>

#include "framework/proxies/X86.h"
#include "framework/proxies/x86/RootSetPtr.h"
#include "utility/UUIDUtil.h"

#include "thor/lang/Domain.h"
#include "thor/lang/Process.h"

namespace thor { namespace lang {

namespace proxies_x86 = ::zillians::framework::proxies::x86;

namespace {

Domain* get_or_create_domain_object(int64 session_id)
{
    auto*const domain = proxies_x86::get_domain_object(session_id);
    if (domain != nullptr)
        return domain;

    const auto& created = proxies_x86::make_root_set_ptr(Domain::create());
    created->setSessionId(session_id);

    return proxies_x86::add_domain_object(session_id, created.get());
}

} // anonymous namespace

Domain::Domain()
    : session_id(-1)
{
}

Domain::~Domain()
{
}

Domain* Domain::local()
{
    return get_or_create_domain_object(-1);
}

Domain* Domain::caller()
{
    const auto session_id = proxies_x86::get_current_invocation_session_id();

    return get_or_create_domain_object(session_id);
}

int64 Domain::getSessionId()
{
    return session_id;
}

void Domain::setSessionId(int64 id)
{
    session_id = id;
}

::thor::util::UUID* Domain::listen(String* endpoint, ConnCallback* conn_cb, ConnCallback* disconn_cb, ErrCallback* err_cb)
{
    if (endpoint == nullptr)
        return ::thor::util::UUID::nil();

    assert(endpoint->data != nullptr && "null pointer exception");

    proxies_x86::gc_shared_root_set_add(conn_cb   );
    proxies_x86::gc_shared_root_set_add(disconn_cb);
    proxies_x86::gc_shared_root_set_add(err_cb    );

    auto       raw_id        = proxies_x86::domain_listen(*endpoint->data, conn_cb, disconn_cb, err_cb);
    auto*const connection_id = ::thor::util::UUID::__create();
    connection_id->setId(std::move(raw_id));

    return connection_id;
}

::thor::util::UUID* Domain::connect(String* endpoint, ConnCallback* conn_cb, ConnCallback* disconn_cb, ErrCallback* err_cb)
{
    if (endpoint == nullptr)
        return ::thor::util::UUID::nil();

    assert(endpoint->data != nullptr && "null pointer exception");

    proxies_x86::gc_shared_root_set_add(conn_cb   );
    proxies_x86::gc_shared_root_set_add(disconn_cb);
    proxies_x86::gc_shared_root_set_add(err_cb    );

    auto       raw_id        = proxies_x86::domain_connect(*endpoint->data, conn_cb, disconn_cb, err_cb);
    auto*const connection_id = ::thor::util::UUID::__create();
    connection_id->setId(std::move(raw_id));

    return connection_id;
}

bool Domain::cancel(::thor::util::UUID* id)
{
    return proxies_x86::domain_cancel(id->getId());
}

void __domainOnConnect()
{
    const char*const parameters = proxies_x86::get_current_invocation_parameters();

    const auto       session_id = *reinterpret_cast<const std::int64_t*        >(parameters                                    );
    const auto&      uuid       = *reinterpret_cast<const zillians::UUID*      >(parameters + sizeof(session_id)               );
          auto*const callback   = *reinterpret_cast<Domain::ConnCallback*const*>(parameters + sizeof(session_id) + sizeof(uuid));

    if (callback == nullptr)
    {
        return;
    }

    const auto& thor_uuid = proxies_x86::make_root_set_ptr(::thor::util::UUID::fromId(uuid));

    callback->invoke(thor_uuid.get(), get_or_create_domain_object(session_id));
}

void __domainOnDisconnect()
{
    const char*const parameters = proxies_x86::get_current_invocation_parameters();

    static_assert(
        sizeof(void*) == sizeof(Domain::ConnCallback*) && sizeof(void*) == sizeof(Domain::ErrCallback*),
        "pointers are in different size"
    );

    const auto       session_id = *reinterpret_cast<const std::int64_t*        >(parameters                                    );
    const auto&      uuid       = *reinterpret_cast<const zillians::UUID*      >(parameters + sizeof(session_id)               );
          auto*const target     = *reinterpret_cast<Domain::ConnCallback*const*>(parameters + sizeof(session_id) + sizeof(uuid));

    proxies_x86::gc_shared_root_set_remove(target);

    if (target == nullptr)
    {
        return;
    }

    const auto& thor_uuid = proxies_x86::make_root_set_ptr(::thor::util::UUID::fromId(uuid));

    target->invoke(thor_uuid.get(),get_or_create_domain_object(session_id));
}

void __domainOnError()
{
    const char*const parameters = proxies_x86::get_current_invocation_parameters();

    static_assert(
        sizeof(void*) == sizeof(Domain::ConnCallback*) && sizeof(void*) == sizeof(Domain::ErrCallback*),
        "pointers are in different size"
    );

    const auto&      uuid   = *reinterpret_cast<const zillians::UUID*     >(parameters                               );
    const auto&      error  = *reinterpret_cast<const DomainError*        >(parameters + sizeof(uuid)                );
          auto*const target = *reinterpret_cast<Domain::ErrCallback*const*>(parameters + sizeof(uuid) + sizeof(error));

    proxies_x86::gc_shared_root_set_remove(target);

    if (target == nullptr)
    {
        return;
    }

    const auto& thor_uuid = proxies_x86::make_root_set_ptr(::thor::util::UUID::fromId(uuid));

    target->invoke(thor_uuid.get(), error);
}

} }
