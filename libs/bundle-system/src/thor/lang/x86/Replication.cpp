/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>

#include <algorithm>
#include <deque>
#include <iterator>
#include <limits>
#include <memory>
#include <new>
#include <tuple>
#include <type_traits>
#include <utility>

#include <boost/mpl/at.hpp>
#include <boost/mpl/has_key.hpp>
#include <boost/mpl/map.hpp>
#include <boost/mpl/pair.hpp>

#include "thor/Thor.h"
#include "thor/lang/Replication.h"
#include "thor/lang/Language.h"

#include "utility/MemoryUtil.h"

#include "framework/proxies/X86.h"

namespace thor { namespace lang {

namespace proxies_x86 = ::zillians::framework::proxies::x86;

namespace {

    using PrimitiveHeaderMap = boost::mpl::map<
        boost::mpl::pair<bool,    std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Bool   >>,
        boost::mpl::pair<int8,    std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Int8   >>,
        boost::mpl::pair<int16,   std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Int16  >>,
        boost::mpl::pair<int32,   std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Int32  >>,
        boost::mpl::pair<int64,   std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Int64  >>,
        boost::mpl::pair<float32, std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Float32>>,
        boost::mpl::pair<float64, std::integral_constant<ReplicationBase::PrimitiveTypeHeader, ReplicationBase::PrimitiveTypeHeader::Float64>>
    >::type;

    template<typename T>
    void put_impl(std::deque<int8>& output, const T& object)
    {
        auto*const object_begin = reinterpret_cast<const int8*>(&object);

        output.insert(output.end(), object_begin, object_begin + sizeof(object));
    }

    template<typename T>
    bool put_impl_nothrow(std::deque<int8>& output, const T& object)
    {
        try
        {
            put_impl(output, object);
        }
        catch (const std::bad_alloc&)
        {
            return false;
        }

        return true;
    }

    // ReplicationData related manipulators
    template<typename Primitive>
    void put_with_header(std::deque<int8>& output, const Primitive& value)
    {
        static_assert(boost::mpl::has_key<PrimitiveHeaderMap, Primitive>::value, "unsupported type");

        // get associated header value
        const auto& header = boost::mpl::at<PrimitiveHeaderMap, Primitive>::type::value;

        // write header & entity
        put_impl(output, header);
        put_impl(output, value );
    }

    // ReplicationData related manipulators
    template<typename Primitive>
    bool put_with_header_nothrow(std::deque<int8>& output, const Primitive& value)
    {
        try
        {
            put_with_header(output, value);
        }
        catch (const std::bad_alloc&)
        {
            return false;
        }

        return true;
    }

    template<typename T, typename const_iterator>
    T peek(const_iterator bytes_i, const_iterator bytes_iend)
    {
        T           object;
        const auto& peek_count = std::min({sizeof(object), static_cast<std::size_t>(std::distance(bytes_i, bytes_iend))});

        std::copy(bytes_i, std::next(bytes_i, peek_count), reinterpret_cast<int8*>(&object));

        return object;
    }

    template<typename T, typename const_iterator>
    T get(const_iterator& bytes_i, const_iterator bytes_iend)
    {
        T           object;
        const auto& get_count = std::min({sizeof(object), static_cast<std::size_t>(std::distance(bytes_i, bytes_iend))});

        std::copy(bytes_i, std::next(bytes_i, get_count), reinterpret_cast<int8*>(&object));

        bytes_i += get_count;

        return object;
    }

}

ReplicationBase::ReplicationBase()
    : content(::zillians::make_unique<std::deque<int8>>())
{
}

ReplicationBase::ReplicationBase(std::unique_ptr<std::deque<int8>>&& new_content)
    : content(std::move(new_content))
{
    assert(content != nullptr && "null pointer exception");
}

ReplicationBase::~ReplicationBase() = default;

ReplicationEncoder::ReplicationEncoder()
    : ReplicationBase()
    , object_to_id(::zillians::make_unique<ObjectToIdMapType>())
{
}

ReplicationEncoder::~ReplicationEncoder() = default;

bool ReplicationEncoder::put(std::size_t count, const void* buffer)
{
    if (std::numeric_limits<int64>::max() < count)
        return false;

    try
    {
        put_impl(*content, PrimitiveTypeHeader::Raw);
        put_with_header(*content, static_cast<int64>(count));

        const auto*const byte_begin = reinterpret_cast<const int8*>(buffer);
        const auto*const byte_end   = byte_begin + count;

        content->insert(content->end(), byte_begin, byte_end);
    }
    catch (const std::bad_alloc&)
    {
        return false;
    }

    return true;
}

bool ReplicationEncoder::put(bool b)
{
    return put_with_header_nothrow(*content, b);
}

bool ReplicationEncoder::put(int8 i8)
{
    return put_with_header_nothrow(*content, i8);
}

bool ReplicationEncoder::put(int16 i16)
{
    return put_with_header_nothrow(*content, i16);
}

bool ReplicationEncoder::put(int32 i32)
{
    return put_with_header_nothrow(*content, i32);
}

bool ReplicationEncoder::put(int64 i64)
{
    return put_with_header_nothrow(*content, i64);
}

bool ReplicationEncoder::put(float32 f32)
{
    return put_with_header_nothrow(*content, f32);
}

bool ReplicationEncoder::put(float64 f64)
{
    return put_with_header_nothrow(*content, f64);
}

bool ReplicationEncoder::put(Object* object)
{
    if (object == nullptr)
        return putNull();

    // (1) determine if this object was serialized
    bool        is_serialized = false;
    std::size_t id            = std::numeric_limits<std::size_t>::max();

    std::tie(is_serialized, id) = getSerializationId(object);
    if (is_serialized)
        return putReference(id);

    const auto&      type_id    = proxies_x86::get_type_id(object);
          auto*const serializer = proxies_x86::get_object_serializer(type_id);

    assert(serializer != nullptr && "can not file serializer for this type");
    if (serializer == nullptr)
        return false;

    // (2) this object have not been serialized, prepare to call it's method 'serialize'
    try
    {
        put_impl(*content, ObjectMarker::Begin);
        put_with_header(*content, type_id);
    }
    catch (const std::bad_alloc&)
    {
        return false;
    }

    if (!serializer(this, object))
        return false;

    return put_impl_nothrow(*content, ObjectMarker::End);
}

std::unique_ptr<std::deque<int8>> ReplicationEncoder::stealContent() noexcept
{
    return std::move(content);
}

std::pair<bool, std::size_t> ReplicationEncoder::getSerializationId(Object* object)
{
    const auto& insertion_result = object_to_id->emplace(object, object_to_id->size());
    const auto& is_inserted      = insertion_result.second;
    const auto& id               = insertion_result.first->second;

    return {!is_inserted, id};
}

bool ReplicationEncoder::putReference(std::size_t id)
{
    try
    {
        put_impl(*content, ObjectMarker::Reference);
        put_impl(*content, id                     );
    }
    catch (const std::bad_alloc&)
    {
        return false;
    }

    return true;
}

bool ReplicationEncoder::putNull()
{
    return put_impl_nothrow(*content, ObjectMarker::Null);
}

ReplicationDecoder::ReplicationDecoder()
    : ReplicationBase()
    , id_to_object(::zillians::make_unique<IdToObjectMapType>())
{
}

ReplicationDecoder::ReplicationDecoder(ReplicationEncoder* encoder)
    : ReplicationBase(encoder->stealContent())
    , id_to_object(::zillians::make_unique<IdToObjectMapType>())
    , read_pos(::zillians::make_unique<std::deque<int8>::const_iterator>(content->begin()))
{
}

ReplicationDecoder::~ReplicationDecoder() = default;

bool ReplicationDecoder::isEnded()
{
    return *read_pos == content->end();
}

void ReplicationDecoder::setContent(std::unique_ptr<std::deque<int8>>&& new_content)
{
    assert(new_content != nullptr && "null pointer exception");

    content  = std::move(new_content);
    read_pos = ::zillians::make_unique<std::deque<int8>::const_iterator>(content->begin());
}
                                                                                    \
#define DEFINE_DECODER_IS_AND_GET(type, name)                                       \
    template<>                                                                      \
    bool ReplicationDecoder::isNext<type>(type)                                     \
    {                                                                               \
        return isNext ## name();                                                    \
    }                                                                               \
    bool ReplicationDecoder::isNext ## name()                                       \
    {                                                                               \
        const auto& header = peek<PrimitiveTypeHeader>(*read_pos, content->cend()); \
        return header == PrimitiveTypeHeader::name;                                 \
    }                                                                               \
    template<>                                                                      \
    std::pair<bool, type> ReplicationDecoder::nativeGet<type>(type)                 \
    {                                                                               \
        return nativeGet ## name();                                                 \
    }                                                                               \
    std::pair<bool, type> ReplicationDecoder::nativeGet ## name()                   \
    {                                                                               \
        if (!isNext ## name())                                                      \
            return {false, type()};                                                 \
                                                                                    \
        type result = nativeGet ## name ## Impl();                                  \
                                                                                    \
        return {true, result};                                                      \
    }                                                                               \
    type ReplicationDecoder::nativeGet ## name ## Impl()                            \
    {                                                                               \
        const auto& header = get<PrimitiveTypeHeader>(*read_pos, content->cend());  \
        assert(header == PrimitiveTypeHeader::name && "invalid header");            \
                                                                                    \
        type result = get<type>(*read_pos, content->cend());                        \
                                                                                    \
        return result;                                                              \
    }

DEFINE_DECODER_IS_AND_GET(bool   , Bool   )
DEFINE_DECODER_IS_AND_GET(int8   , Int8   )
DEFINE_DECODER_IS_AND_GET(int16  , Int16  )
DEFINE_DECODER_IS_AND_GET(int32  , Int32  )
DEFINE_DECODER_IS_AND_GET(int64  , Int64  )
DEFINE_DECODER_IS_AND_GET(float32, Float32)
DEFINE_DECODER_IS_AND_GET(float64, Float64)

#undef DEFINE_DECODER_IS_AND_GET

bool ReplicationDecoder::isNextNull()
{
    const auto& marker = peek<ObjectMarker>(*read_pos, content->cend());

    return marker == ObjectMarker::Null;
}

template<>
bool ReplicationDecoder::isNext<Object*>(Object*)
{
    return isNextObject();
}

bool ReplicationDecoder::isNextObject()
{
    const auto& marker = peek<ObjectMarker>(*read_pos, content->cend());

    return  marker == ObjectMarker::Reference ||
            marker == ObjectMarker::Begin;
}

template<>
std::pair<bool, Object*> ReplicationDecoder::nativeGet<Object*>(Object*)
{
    return nativeGetObject();
}

std::pair<bool, Object*> ReplicationDecoder::nativeGetObject()
{
    const auto& marker = peek<ObjectMarker>(*read_pos, content->cend());

    switch (marker)
    {
    case ObjectMarker::Reference:
    case ObjectMarker::Begin    :
        break;

    case ObjectMarker::Null:
        get<ObjectMarker>(*read_pos, content->cend());
        return { true, nullptr};

    default:
        return {false, nullptr};
    }

    Object*const object = nativeGetObjectImpl();

    return {object != nullptr, object};
}

void ReplicationDecoder::eatNullMark()
{
    const auto& marker = get<ObjectMarker>(*read_pos, content->cend());

    assert(marker == ObjectMarker::Null && "we expect marker is null here");
}

Object* ReplicationDecoder::nativeGetObjectImpl()
{
    const auto& marker = get<ObjectMarker>(*read_pos, content->cend());

    switch (marker)
    {
    case ObjectMarker::Reference:
    {
        const auto& id    = get<std::size_t>(*read_pos, content->cend());
        const auto& found = id_to_object->find(id);

        assert(found != id_to_object->end() && "can not find previous deserialized object");
        if (found == id_to_object->end())
            return nullptr;

        return found->second;
    }

    case ObjectMarker::Begin:
        break;

    default:
        return nullptr;
    }

    const auto& type_id_result = nativeGetInt64();
    const auto& type_id_found  = type_id_result.first;
    const auto& type_id        = type_id_result.second;

    if (!type_id_found)
        return nullptr;

    // (1) prepare to call object's creator
    // (2) prepare to call object's deserializer
    auto*const creator      = proxies_x86::get_object_creator(type_id);
    auto*const deserializer = proxies_x86::get_object_deserializer(type_id);

    if (creator == nullptr)
        return nullptr;

    if (deserializer == nullptr)
        return nullptr;

    Object*const object = creator();

    // pre register to prevent self reference
    registerDeserialized(object);

    const auto& is_deserialized_successfully = deserializer(this, object);

    if (!is_deserialized_successfully)
        return nullptr;

    const auto& end_marker = get<ObjectMarker>(*read_pos, content->cend());

    if (end_marker != ObjectMarker::End)
        return nullptr;

    return object;
}

std::pair<std::size_t, bool> ReplicationDecoder::eatRawBegin()
{
    if (get<PrimitiveTypeHeader>(*read_pos, content->cend()) != PrimitiveTypeHeader::Raw)
        return {0, false};

    if (!isNextInt64())
        return {0, false};

    const auto& raw_size_result = nativeGetInt64();
    const auto& raw_size_found  = raw_size_result.first;
    const auto& raw_size        = raw_size_result.second;

    assert(raw_size_found && "raw buffer size missing");
    if (!raw_size_found)
        return {0, false};

    assert(raw_size >= 0 && "buffer size should not be negative integer!");
    if (raw_size < 0)
        return {0, false};

    if (std::numeric_limits<std::size_t>::max() < raw_size)
        return {0, false};

    return {static_cast<std::size_t>(raw_size), true};
}

std::size_t ReplicationDecoder::getRawData(std::size_t count, void* buffer)
{
          auto*const ptr        = reinterpret_cast<int8*>(buffer);
    const auto&      write_size = std::min<std::size_t>({count, std::distance(*read_pos, content->cend())});

    std::copy(*read_pos, std::next(*read_pos, write_size), ptr);

    return write_size;
}

void ReplicationDecoder::registerDeserialized(Object* object)
{
    id_to_object->emplace(id_to_object->size(), object);
}

} }
