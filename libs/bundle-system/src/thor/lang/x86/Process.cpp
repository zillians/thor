/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdlib>
#include <ctime>

#include <boost/preprocessor/seq/for_each.hpp>

#include "thor/lang/Process.h"
#include "thor/lang/Domain.h"
#include "thor/lang/Lambda.h"
#include "thor/lang/Replication.h"
#include "thor/container/Vector.h"

#include "framework/proxies/X86.h"

#include "rstm/api/library.hpp"

namespace thor { namespace lang {

namespace proxies_x86 = ::zillians::framework::proxies::x86;

void exit(int32 exit_code)
{
    proxies_x86::exit(exit_code);
}

bool __postponeInvocationDependencyTo(int64 postpone_to_id)
{
    return proxies_x86::postpone_invocation_dependency_to(postpone_to_id);
}

bool __setInvocationDependOnCount(int64 invocation_id, int32 count)
{
    return proxies_x86::set_invocation_depend_on_count(invocation_id, count);
}

bool __setInvocationDependOn(int64 invocation_id, int64 depend_on_id)
{
    return proxies_x86::set_invocation_depend_on(invocation_id, depend_on_id);
}

int64 __addInvocation(int64 function_id, int8* ret_ptr)
{
    return proxies_x86::add_invocation(function_id, ret_ptr);
}

int64 __reserveInvocation(int64 function_id)
{
    return proxies_x86::reserve_invocation(function_id);
}

bool __commitInvocation(int64 inv_id)
{
    return proxies_x86::commit_invocation(inv_id);
}

bool __abortInvocation(int64 inv_id)
{
    return proxies_x86::abort_invocation(inv_id);
}

bool __setInvocationReturnPtr(int64 inv_id, int8* ptr)
{
    return proxies_x86::set_invocation_return_ptr(inv_id, ptr);
}

#define PARAMETER_VALUE_TYPES          \
    (bool   )                          \
    (int8   )                          \
    (int16  )                          \
    (int32  )                          \
    (int64  )                          \
    (float32)                          \
    (float64)                          \
    (int8*  ) /* use 'int8*' in order to work with interfaces */

#define APPEND_INVOCATION_PARAM_DEF(r, _, type)                                 \
    int64 __appendInvocationParameter(int64 inv_id, int64 offset, type value)   \
    {                                                                           \
        return proxies_x86::append_invocation_parameter(inv_id, offset, value); \
    }

BOOST_PP_SEQ_FOR_EACH(APPEND_INVOCATION_PARAM_DEF, _, PARAMETER_VALUE_TYPES)

#undef APPEND_INVOCATION_PARAM_DEF
#undef PARAMETER_VALUE_TYPES

bool __invokeRemotely(Domain* domain, int64 adaptor_id, ReplicationEncoder* encoder)
{
    assert(encoder != nullptr && "parameter encoding failure!");
    if (encoder == nullptr)
        return false;

    const auto& session_id = domain->getSessionId();
          auto  data       = encoder->stealContent();

    proxies_x86::add_invocation(adaptor_id, session_id, std::move(data));

    return true;
}

void __remoteInvoker()
{
          auto  remote_info = proxies_x86::steal_replication_data();
          auto& data        = remote_info.first;
    const auto& adaptor_id  = remote_info.second;

    if (data == nullptr)
        return;

    auto*const adaptor = proxies_x86::get_remote_adaptor(adaptor_id);
    if (adaptor == nullptr)
        return;

    auto*const decoder = ReplicationDecoder::__create();
    decoder->setContent(std::move(data));

    adaptor(decoder);
}

void __invokeKernel(Object* domain, int64 function_id, int32 dim_x, int32 dim_y, int32 dim_z, Lambda0<void>* callback)
{

}

void __RSTMSystemInitialize()
{
    TM_SYS_INIT();
}

void __resetRandomSeed()
{
    srand((unsigned)time(nullptr));
}

} }

namespace stm {
    template bool         stm_read<bool        >(bool        *, TxThread*);
    template std::int8_t  stm_read<std::int8_t >(std::int8_t *, TxThread*);
    template std::int32_t stm_read<std::int32_t>(std::int32_t*, TxThread*);
    template std::int64_t stm_read<std::int64_t>(std::int64_t*, TxThread*);
    template float        stm_read<float       >(float       *, TxThread*);
    template double       stm_read<double      >(double      *, TxThread*);
    template std::int8_t* stm_read<std::int8_t*>(std::int8_t**, TxThread*);

    template void stm_write<bool        >(bool        *, bool        , TxThread*);
    template void stm_write<std::int8_t >(std::int8_t *, std::int8_t , TxThread*);
    template void stm_write<std::int32_t>(std::int32_t*, std::int32_t, TxThread*);
    template void stm_write<std::int64_t>(std::int64_t*, std::int64_t, TxThread*);
    template void stm_write<float       >(float       *, float       , TxThread*);
    template void stm_write<double      >(double      *, double      , TxThread*);
    template void stm_write<std::int8_t*>(std::int8_t**, std::int8_t*, TxThread*);
}

