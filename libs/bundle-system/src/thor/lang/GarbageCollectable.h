/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef THOR_LANG_GARBAGECOLLECTABLE_H_
#define THOR_LANG_GARBAGECOLLECTABLE_H_

#include <type_traits>
#include <vector>

#include "thor/Thor.h"
#include "thor/lang/Language.h"

namespace thor { namespace lang {

// Define this structure to ease the mangling
// PS. we do not make CollectableObject inherit from Object. If we do so, we need to link to libsystem.so for thor-vm
// which turns out to be ugly.
struct CollectableObject
{
    template<typename T, typename std::enable_if< ::thor::lang::is_class<T>::value>::type * = nullptr>
    void add(T t)
    {
        objects.push_back(t);
    }

    template<typename T, typename std::enable_if<!::thor::lang::is_class<T>::value>::type * = nullptr>
    void add(T t)
    {
    }

    template<typename iterator, typename std::enable_if< ::thor::lang::is_class<typename std::remove_reference<decltype(*std::declval<iterator>())>::type>::value>::type * = nullptr>
    void add(const iterator& beg, const iterator& end)
    {
        objects.insert(objects.end(), beg, end);
    }

    template<typename iterator, typename std::enable_if<!::thor::lang::is_class<typename std::remove_reference<decltype(*std::declval<iterator>())>::type>::value>::type * = nullptr>
    void add(const iterator& beg, const iterator& end)
    {
    }

	std::vector<void*> objects;
};

struct GarbageCollectable
{
	virtual void getContainedObjects(CollectableObject* collect_object) = 0;
};

} }


#endif /* THOR_LANG_GARBAGECOLLECTABLE_H_ */
