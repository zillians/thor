/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

/**
 * @brief Floating point value indicates that the function's
 *        result value is too large in magnitude to be
 *        representable with its return type.
 */
@cpu
@native
function hugeValue(): float64;

/**
 * @brief Floating point value represents positive infinity.
 * @return Positive infinity.
 */
@cpu
@native
function infinity(): float64;

/**
 * @brief Floating point value represents Not-a-Number.
 * @return Not-a-Number.
 */
@cpu
@native
function NaN(): float64;

/**
 * @brief Base for all floating point types(float32, float64).
 */
@cpu
@native
function radix(): float64;

/**
 * @brief Return value corresponding to the current rounding
 *        mode.
 * @retval -1 undetermined.
 * @retval 0  toward zero
 * @retval 1  to nearest
 * @retval 2  toward positive infinity
 * @retval 3  toward negative infinity
 */
@cpu
@native
function roundMode(): int32;

/**
 * @brief Determines if the given floating point number @b arg has finite
 *        value i.e. it is normal, subnormal or zero, but not infinite or
 *        NaN.
 *
 * @param arg Floating point value.
 * @retval true @b arg has finite value.
 * @retval false @b arg is infinite.
 */
@cpu
@native
function isFinite(arg: float32): bool;

/**
 * @brief Determines if the given floating point number @b arg has finite
 *        value i.e. it is normal, subnormal or zero, but not infinite or
 *        NaN.
 *
 * @param arg Floating point value.
 * @retval true @b arg has finite value.
 * @retval false @b arg is infinite.
 */
@cpu
@native
function isFinite(arg: float64): bool;

/**
 * @brief Determines if the given floating point number @b arg is
 *        positive or negative infinity.
 *
 * @param arg Floating point value.
 * @retval true @b arg is infinite.
 * @retval false @b arg has finite value.
 */
@cpu
@native
function isInfinite(arg: float32): bool;

/**
 * @brief Determines if the given floating point number @b arg is
 *        positive or negative infinity.
 *
 * @param arg Floating point value.
 * @retval true @b arg is infinite.
 * @retval false @b arg has finite value.
 */
@cpu
@native
function isInfinite(arg: float64): bool;

/**
 * @brief Determines if the given floating point number @b arg is
 *        not-a-number (NaN).
 *
 * @param arg Floating point value.
 * @retval true @b arg is NaN.
 * @retval false Otherwise.
 */
@cpu
@native
function isNaN(arg: float32): bool;

/**
 * @brief Determines if the given floating point number @b arg is
 *        not-a-number (NaN).
 *
 * @param arg Floating point value.
 * @retval true @b arg is NaN.
 * @retval false Otherwise.
 */
@cpu
@native
function isNaN(arg: float64): bool;

/**
 * @brief Determines if the given floating point number @b arg is
 *        normal, i.e. is neither zero, subnormal, infinite, nor
 *        NaN.
 *
 * @param arg Floating point value.
 * @retval true @b arg is normal.
 * @retval false Otherwise.
 */
@cpu
@native
function isNormal(arg: float32): bool;

/**
 * @brief Determines if the given floating point number @b arg is
 *        normal, i.e. is neither zero, subnormal, infinite, nor
 *        NaN.
 *
 * @param arg Floating point value.
 * @retval true @b arg is normal.
 * @retval false Otherwise.
 */
@cpu
@native
function isNormal(arg: float64): bool;

/**
 * @brief Compute arc cosine of @b arg.
 * @param arg Floating point value.
 * @return Arc cosine of @b arg in radians.
 * @retval [0, π] If @b arg is inside the range [-1.0, 1.0].
 * @retval NaN() If @b arg is outside the range [-1.0, 1.0].
 */
@cpu
@native 
function acos(arg: float64): float64;

/**
 * @brief Compute arc sine of @b arg.
 * @param arg Floating point value.
 * @return Arc sine of @b arg in radians.
 * @retval [0, π] If @b arg is inside the range [-1.0, 1.0].
 * @retval NaN() if @b arg is outside the range [-1.0, 1.0].
 */
@cpu
@native 
function asin(arg: float64): float64;

/**
 * @brief Compute arc tangent of @b arg.
 * @param arg Floating point value.
 * @return Arc tangent of @b arg in radians in the range [-π/2, π/2].
 */
@cpu
@native 
function atan(arg: float64): float64;

/**
 * @brief Compute the inverse tangent of @code y/x @endcode using the signs of arguments to
 *        correctly determine quadrant.
 * @param x Floating point value.
 * @param y Floating point value.
 *
 * @return Arc tangent of @code y/x @endcode in radians in the range [-π, π].
 */
@cpu
@native 
function atan2(y: float64, x: float64): float64;

/**
 * @brief Compute cosine of @b arg.
 * @param arg Floating point value representing angle in radians.
 * @return Cosine of @b arg.
 * @retval [-1.0, 1.0] For normal floating point values.
 * @retval NaN() If @b arg is infinite.
 */
@cpu
@native 
function cos(arg: float64): float64;

/**
 * @brief Compute sine of @b arg.
 * @param arg Floating point value representing angle in radians.
 * @return Sine of @b arg.
 * @retval [-1.0, 1.0] For normal floating point values.
 * @retval NaN() If @b arg is infinite.
 */
@cpu
@native 
function sin(arg: float64): float64;

/**
 * @brief Compute tangent of @b arg.
 * @param arg Floating point value representing angle in radians.
 * @return Tangent of @b arg.
 * @retval NaN() if @b arg is infinite.
 * @retval Other Tangent of @b arg.
 */
@cpu
@native 
function tan(arg: float64): float64;

/**
 * @brief Compute hyperbolic area cosine of @b arg.
 * @param arg Floating point value.
 * @return Inverse hyperbolic cosine of arg.
 */
@cpu
@native 
function acosh(arg: float64): float64;

/**
 * @brief Compute area hyperbolic sine of @b arg.
 * @param arg Floating point value.
 * @return Inverse hyperbolic sine of @b arg.
 */
@cpu
@native 
function asinh(arg: float64): float64;

/**
 * @brief Compute area hyperbolic tangent of @b arg.
 * @param arg Floating point value.
 * @return Inverse hyperbolic tangent of @b arg.
 */
@cpu
@native 
function atanh(arg: float64): float64;

/**
 * @brief Compute hyperbolic cosine of @b arg.
 * @param arg Floating point value.
 * @return Hyperbolic cosine of @b arg.
 */
@cpu
@native 
function cosh(arg: float64): float64;

/**
 * @brief Compute hyperbolic sine of @b arg.
 * @param arg Floating point value.
 * @return Hyperbolic sine of @b arg.
 */
@cpu
@native 
function sinh(arg: float64): float64;

/**
 * @brief Compute hyperbolic tangent of @b arg.
 * @param arg Floating point value.
 * @return Hyperbolic tangent of @b arg.
 */
@cpu
@native 
function tanh(arg: float64): float64;

/**
 * @brief Compute the e (Euler's number, @c 2.7182818) raised to
 *        the given power @b arg.
 * @param arg Floating point value.
 * @return e Raised to the power @b arg.
 * @retval hugeValue() If the result is too large for @b float64
 *         to hold it.
 * @retval Other A normal floating point value.
 */
@cpu
@native 
function exp(arg: float64): float64;

/**
 * @brief Compute 2 raised to the given power @b n.
 * @param n Floating point value.
 * @return 2 raised to the @b n.
 * @retval hugeValue() If the result is too large for @b float64
 *         to hold it.
 * @retval Other A normal floating point value.
 */
@cpu
@native 
function exp2(n: float64): float64;

/**
 * @brief Compute the e (Euler's number, @c 2.7182818) raised to
 *        the given power @b arg, minus 1.
 *
 * This function is more accurate than the expression'
 * @code exp(arg)-1 @endcode if @b arg is close to zero.
 *
 * @param arg Floating point value.
 * @return <i>e<sup><b>arg</b></sup>-1</i>
 * @retval hugeValue() If the result is too large for @b float64
 *         to hold it.
 * @retval Other A normal floating point value.
 */
@cpu
@native 
function expm1(arg: float64): float64;

/**
 * @brief Special value returned by ilogb()
 *
 * The return value indicates the argument of ilogb() is 0.
 */
@cpu
@native
function ilogb0(): float64;

/**
 * @brief Special value returned by ilogb()
 *
 * The return value indicates the argument of ilogb() is NaN().
 */
@cpu
@native
function ilogbNaN(): float64;

/**
 * @brief Extract the value of the exponent from the floating-point
 *        argument @b arg, and returns it as a signed integer value.
 *
 * Formally, the result is the integral part of <i>log<sub>r</sub></i>|<i>arg</i>|
 * as a signed integral value, for non-zero @b arg, where r is radix().
 *
 * @param arg Floating point value.
 * @return The floating-point exponent, cast to integer.
 * @retval ilogb0() If @b arg is zero.
 * @retval 2<sup>31</sup>-1 If @b arg is infinite(the largest value @b int64 can represents).
 * @retval ilogbNaN() If @b arg is NaN.
 * @retval Other A normal integer value.
 * @remark If the result cannot be represented as @b int64, the result is undefined.
 */
@cpu
@native 
function ilogb(arg: float64): int64;

/**
 * @brief Multiply an floating point value arg by @c 2 raised to power exp.
 * @param arg Floating point value.
 * @param iexp Integer value.
 * @return Returns <i>arg×2<sup>exp</sup></i>.
 * @retval hugeValue() If the result is too large for @b float64
 *         to hold it.
 * @retval Other A normal floating point value.
 */
@cpu
@native 
function ldexp(arg: float64, iexp: int64): float64;

/**
 * @brief Compute the natural (base e) logarithm of @b arg.
 * @param arg Floating point value.
 * @return Natural logarithm of @b arg.
 * @retval NaN() If @b arg is negative.
 * @retval -hugeValue() If @b arg is @c 0.
 * @retval Other A normal floating point value.
 */
@cpu
@native 
function log(arg: float64): float64;

/**
 * @brief Compute the common (base @c 10) logarithm of @b arg.
 * @param arg Floating point value.
 * @return Base @c 10 logarithm of @b arg.
 * @retval NaN() If @b arg is negative.
 * @retval -hugeValue() If @b arg is @c 0.
 * @retval A normal floating point value for other cases.
 */
@cpu
@native 
function log10(arg: float64): float64;

/**
 * @brief Compute the natural (base @c e) logarithm of @code 1+arg
 *        @endcode.
 *
 * This function is more precise than the expression @code log(1+arg)
 * @endcode if @c arg is close to zero.
 *
 * @param arg Floating point value.
 * @return <i>ln(1+arg)</i>
 * @retval NaN() If @b arg is negative.
 * @retval -hugeValue() If @b arg is @c 0.
 * @retval A normal floating point value for other cases.
 */
@cpu
@native 
function log1p(arg: float64): float64;

/**
 * @brief Compute the base @c 2 logarithm of @b arg.
 * @param arg Floating point value.
 * @return <i>log<sub>2</sup>(arg)</i>
 * @retval NaN() If @b arg is negative.
 * @retval -hugeValue() If @b arg is @c 0.
 * @retval A normal floating point value for other cases.
 */
@cpu
@native 
function log2(arg: float64): float64;

/**
 * @brief Extract the value of the exponent from the floating-point
 *        argument @b arg, and returns it as a floating-point value.
 *
 * Formally, the result is the integral part of
 * <i>log<sub>r</sub></i>|<i>arg</i>| as a signed floating-point
 * value, for non-zero arg, where r is radix(). If @b arg is
 * subnormal, it is treated as though it was normalized.
 *
 * @param arg Floating piont value.
 * @return The floating-point exponent.
 * @remark Domain or range error may occur if @b arg is zero.
 */
@cpu
@native 
function logb(arg: float64): float64;

/**
 * @brief Multiply an floating point value x by radix()
 *        raised to power @b n.
 * @param arg Floating point value.
 * @param n Integer value.
 * @return x×radix()<sup>exp</sup>.
 * @retval hugeValue() If the result is too large for @b float64
 *         to hold it.
 */
@cpu
@native 
function scalbn(x: float64, n: int64): float64;

/**
 * @brief Compute cubic root of @b arg.
 * @param arg Floating point value.
 * @return Cubic root of arg.
 */
@cpu
@native 
function cbrt(arg: float64): float64;

/**
 * @brief Compute the absolute value of a floating point value
 *        @b arg.
 * @param arg Floating point value.
 * @return The absolute value of arg (|arg|).
 */
@cpu
@native 
function fabs(arg: float64): float64;

/**
 * @brief Compute the square root of the sum of the squares of
 *        @b x and @b y, without undue overflow or underflow at
 *        intermediate stages of the computation.
 *
 * This is the length of the hypotenuse of a right-angled
 * triangle with sides of length @b x and @b y, or the distance
 * of the point (@b x, @b y) from the origin (0,0), or the
 * magnitude of a complex number @b x+i @b y.
 *
 * @param x Floating point value.
 * @param y Floating point value.
 * @return The hypotenuse of a right-angled triangle.
 */
@cpu
@native 
function hypot(x: float64, y: float64): float64;

/**
 * @brief Compute the value of @b base raised to the power @b n.
 *
 * @param base Floating point value.
 * @param n Floating point value.
 * @return base raised by power.
 * @retval NaN() If base == 0 and n <= 0, or base < 0 and n is not an integer.
 * @retval hugeValue() If an overflow takes place.
 * @retval A normal floating point value for other cases.
 */
@cpu
@native 
function pow(base: float64, n: float64): float64;

/**
 * @brief Compute square root of @b arg.
 * @param arg Floating point value.
 * @return Square root of @b arg.
 * @retval NaN() If @b arg is negative.
 * @retval A Non-negative floating point value.
 */
@cpu
@native 
function sqrt(arg: float64): float64;

/**
 * @brief Compute the <a href="http://en.wikipedia.org/wiki/Error_function">error function</a> of @b arg.
 * @param arg Floating point value.
 * @return The value of the error function of @b arg.
 */
@cpu
@native 
function erf(arg: float64): float64;

/**
 * @brief Compute the <a href="http://en.wikipedia.org/wiki/Complementary_error_function">complementary error function</a> of @b arg.
 * @param arg Floating point value.
 * @return The value of the complementary error function of @b arg.
 */
@cpu
@native 
function erfc(arg: float64): float64;

/**
 * @brief Compute the natural logarithm of the absolute value of the <a href="http://en.wikipedia.org/wiki/Gamma_function">gamma function</b> of @b arg.
 * @param arg Floating point value.
 * @return The value of the logarithm of the gamma function of @b arg.
 * @retval Logarithm of the factorial of @code arg-1 @endcode If @b arg is a natural number.
 * @retval A normal floating point value for other cases.
 */
@cpu
@native 
function lgamma(arg: float64): float64;

/**
 * @brief Compute the <a href="http://en.wikipedia.org/wiki/Gamma_function">gamma function</a> of arg.
 * @param arg Floating point value.
 * @return The value of the gamma function of @b arg,
 * @retval Factorial of @code arg-1 @endcode If @b arg is a natural number.
 * @retval A normal floating point value for other cases.
 */
@cpu
@native 
function tgamma(arg: float64): float64;

/**
 * @brief Compute nearest integer not less than @b arg.
 * @param arg Floating point value.
 * @return Nearest integer not less than @b arg
 */
@cpu
@native 
function ceil(arg: float64): float64;

/**
 * @brief Compute nearest integer not greater than @b arg.
 * @param arg Floating point value.
 * @return Nearest integer not greater than @b arg.
 */
@cpu
@native 
function floor(arg: float64): float64;

/**
 * @brief Round the floating-point argument @b arg to an
 *        integer value in floating-point format, using
 *        the current rounding mode.
 * @param arg Floating point value.
 * @return The integer result of rounding @b arg.
 */
@cpu
@native 
function nearbyint(arg: float64): float64;

/**
 * @brief Round the floating-point argument @b arg to an
 *        integer value in floating-point format, using the
 *        current rounding mode.
 * @param arg Floating point value.
 * @return The integer result of rounding @b arg.
 */
@cpu
@native 
function rint(arg: float64): float64;

/**
 * @brief Round the floating-point argument @b arg to an
 *        integer value in floating-point format, using the
 *        current rounding mode.
 * @param arg Floating point value.
 * @return The integer result of rounding @b arg.
 */
@cpu
@native 
function irint(arg: float64): int64;

/**
 * @brief Compute nearest integer to @b arg. Number is
 *        rounded away from zero in halfway cases.
 * @param arg Floating point value.
 * @return Nearest integer to @b arg.
 */
@cpu
@native 
function round(arg: float64): float64;

/**
 * @brief Compute nearest integer not greater in magnitude
 *        than @b arg.
 * @param arg Floating point value.
 * @return Nearest integer not greater in magnitude than @b arg.
 */
@cpu
@native 
function trunc(arg: float64): float64;

/**
 * @brief Compute the remainder of the division operation
 *        @code x/y @endcode.
 *
 * Specifically, the returned value is @code x - n*y @endcode,
 * where n is @code x/y @endcode with its fractional part
 * truncated. The returned value will have the same sign as @b x.
 *
 * @param x Floating point value.
 * @param y Floating point value.
 * @return Remainder of dividing arguments. The returned
 *         value will have the same sign as @b x.
 */
@cpu
@native 
function mod(x: float64, y: float64): float64;

/**
 * @brief Compute the signed remainder of the floating
 *        point division operation @code x/y @endcode.
 *
 * Specifically, the returned value is @code x - n*y @endcode,
 * where n is @code x/y @endcode rounded to the nearest integer,
 * or the nearest even integer if @code x/y @endcode is halfway
 * between two integers. In contrast to @c fmod(), the returned
 * value is not guaranteed to have the same sign as @b x. If the
 * returned value is @c 0, it will have the same sign as @b x.
 *
 * @param x Floating point value.
 * @param y Floating point value.
 * @return Remainder of dividing arguments.
 */
@cpu
@native 
function remainder(x: float64, y: float64): float64;

/**
 * @brief Compose a floating point value with the magnitude
 *        of @b x and the sign of @b y.
 * @param x Floating point value.
 * @param y Floating point value.
 * @return Floating point value with the magnitude of @b x and
 *         the sign of y
 */
@cpu
@native 
function copysign(x: float64, y: float64): float64;

/**
 * @brief Return the next representable value of @b from in
 *        the direction of @b to. If @b from equals to @b to,
 *        @b to is returned.
 * @param from Floating point value.
 * @param to Floating point value.
 * @return The next representable value of @b from in the
 *         direction of @b to.
 */
@cpu
@native 
function nextafter(from: float64, to: float64): float64;

/**
 * @brief Return the next representable value of @b from in
 *        the direction of @b to. If @b from equals to @b to,
 *        @b to is returned.
 * @param from Floating point value.
 * @param to Floating point value.
 * @return The next representable value of @b from in the
 *         direction of @b to.
 */
@cpu
@native 
function nexttoward(from: float64, to: float64): float64;

/**
 * @brief Return the positive difference between x and y.
 *
 * This could be implemented as @code max (x - y, 0) @endcode,
 * so if @code x ≤ y @endcode, the result is always equals to
 * @c 0, otherwise it is @code x - y @endcode.
 *
 * @param x Floating point value.
 * @param y Floating point value.
 * @return The positive difference value.
 */
@cpu
@native 
function dim(x: float64, y: float64): float64;

/**
 * @brief Return the larger of two floating point arguments.
 * @param x Floating point values.
 * @param y Floating point values.
 * @return The larger of two floating point values.
 */
@cpu
@native 
function max(x: float64, y: float64): float64;

/**
 * @brief Return the smaller of two floating point arguments.
 * @param x Floating point values.
 * @param y Floating point values.
 * @return The smaller of two floating point values.
 */
@cpu
@native 
function min(x: float64, y: float64): float64;

/**
 * @brief The ma function computes @code (x*y) + z @endcode,
 *        rounded as one ternary operation, according to the
 *        rounding mode characterized by the value of
 *        roundMode().
 * @param x Floating point values.
 * @param y Floating point values.
 * @param z Floating point values.
 * @returnode (x*y) + z @endcode, rounded as one ternary
 *         operation.
 */
@cpu
@native 
function ma(x: float64, y: float64, z: float64): float64;
