/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cfloat>
#include <cmath>

#include "thor/Thor.h"
#include "thor/util/Macro.h"

#define PP_MAKE_ARG_DECL(type, name) type name

#define PP_MAKE_MACRO_ADAPTOR(return_type, adaptor, macro) \
    return_type adaptor() { return macro; }

#define PP_MAKE_ADAPTOR(return_type, arg_type_list, adaptor, adaptee)             \
    return_type adaptor(                                                          \
        PP_TRANSFORM(                                                             \
            arg_type_list,                                                        \
            (PP_MAKE_LIST(PP_LIST_SIZE(arg_type_list), arg, PP_CAT)),             \
            PP_MAKE_ARG_DECL                                                      \
        )                                                                         \
    ) { return adaptee(PP_MAKE_LIST(PP_LIST_SIZE(arg_type_list), arg, PP_CAT)); }

namespace thor { namespace math {

/// introduce C macros as function
PP_MAKE_MACRO_ADAPTOR(float64, hugeValue, HUGE_VALL  )
PP_MAKE_MACRO_ADAPTOR(float64, infinity , INFINITY   )
PP_MAKE_MACRO_ADAPTOR(float64, NaN      , NAN        )
PP_MAKE_MACRO_ADAPTOR(float64, radix    , FLT_RADIX  )
PP_MAKE_MACRO_ADAPTOR(int64  , roundMode, FLT_ROUNDS )
PP_MAKE_MACRO_ADAPTOR(float64, ilogb0   , FP_ILOGB0  )
PP_MAKE_MACRO_ADAPTOR(float64, ilogbNaN , FP_ILOGBNAN)


///        return type | parameter type list        | adaptor   | adaptee
PP_MAKE_ADAPTOR(bool,    (float32)                  , isFinite  , std::isfinite  )
PP_MAKE_ADAPTOR(bool,    (float64)                  , isFinite  , std::isfinite  )
PP_MAKE_ADAPTOR(bool,    (float32)                  , isInfinite, std::isinf     )
PP_MAKE_ADAPTOR(bool,    (float64)                  , isInfinite, std::isinf     )
PP_MAKE_ADAPTOR(bool,    (float32)                  , isNaN     , std::isnan     )
PP_MAKE_ADAPTOR(bool,    (float64)                  , isNaN     , std::isnan     )
PP_MAKE_ADAPTOR(bool,    (float32)                  , isNormal  , std::isnormal  )
PP_MAKE_ADAPTOR(bool,    (float64)                  , isNormal  , std::isnormal  )


PP_MAKE_ADAPTOR(float64, (float64)                  , cos       , std::cos       )
PP_MAKE_ADAPTOR(float64, (float64)                  , exp       , std::exp       )
PP_MAKE_ADAPTOR(float64, (float64)                  , erf       , std::erf       )
PP_MAKE_ADAPTOR(float64, (float64)                  , log       , std::log       )
PP_MAKE_ADAPTOR(float64, (float64)                  , sin       , std::sin       )
PP_MAKE_ADAPTOR(float64, (float64)                  , tan       , std::tan       )
PP_MAKE_ADAPTOR(float64, (float64)                  , acos      , std::acos      )
PP_MAKE_ADAPTOR(float64, (float64)                  , asin      , std::asin      )
PP_MAKE_ADAPTOR(float64, (float64)                  , atan      , std::atan      )
PP_MAKE_ADAPTOR(float64, (float64)                  , cosh      , std::cosh      )
PP_MAKE_ADAPTOR(float64, (float64)                  , cbrt      , std::cbrt      )
PP_MAKE_ADAPTOR(float64, (float64)                  , ceil      , std::ceil      )
PP_MAKE_ADAPTOR(float64, (float64)                  , erfc      , std::erfc      )
PP_MAKE_ADAPTOR(float64, (float64)                  , exp2      , std::exp2      )
PP_MAKE_ADAPTOR(float64, (float64)                  , fabs      , std::fabs      )
PP_MAKE_ADAPTOR(float64, (float64)                  , log2      , std::log2      )
PP_MAKE_ADAPTOR(float64, (float64)                  , rint      , std::rint      )
PP_MAKE_ADAPTOR(float64, (float64)                  , sinh      , std::sinh      )
PP_MAKE_ADAPTOR(float64, (float64)                  , sqrt      , std::sqrt      )
PP_MAKE_ADAPTOR(float64, (float64)                  , tanh      , std::tanh      )
PP_MAKE_ADAPTOR(float64, (float64)                  , acosh     , std::acosh     )
PP_MAKE_ADAPTOR(float64, (float64)                  , asinh     , std::asinh     )
PP_MAKE_ADAPTOR(float64, (float64)                  , atanh     , std::atanh     )
PP_MAKE_ADAPTOR(float64, (float64)                  , expm1     , std::expm1     )
PP_MAKE_ADAPTOR(float64, (float64)                  , floor     , std::floor     )
PP_MAKE_ADAPTOR(float64, (float64)                  , log1p     , std::log1p     )
PP_MAKE_ADAPTOR(float64, (float64)                  , log10     , std::log10     )
PP_MAKE_ADAPTOR(float64, (float64)                  , round     , std::round     )
PP_MAKE_ADAPTOR(float64, (float64)                  , trunc     , std::trunc     )
PP_MAKE_ADAPTOR(float64, (float64)                  , iRound    , std::llround   )
PP_MAKE_ADAPTOR(float64, (float64)                  , lgamma    , std::lgamma    )
PP_MAKE_ADAPTOR(float64, (float64)                  , tgamma    , std::tgamma    )
PP_MAKE_ADAPTOR(float64, (float64)                  , nearbyint , std::nearbyint )
PP_MAKE_ADAPTOR(float64, (float64, int64)           , scalbn    , std::scalbn    )
PP_MAKE_ADAPTOR(float64, (float64, int64)           , ldexp     , std::ldexp     )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , dim       , std::fdim      )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , mod       , std::fmod      )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , max       , std::fmax      )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , min       , std::fmin      )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , pow       , std::pow       )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , atan2     , std::atan2     )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , hypot     , std::hypot     )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , copysign  , std::copysign  )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , nextafter , std::nextafter )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , remainder , std::remainder )
PP_MAKE_ADAPTOR(float64, (float64, float64)         , nexttoward, std::nexttoward)
PP_MAKE_ADAPTOR(float64, (float64, float64, float64), ma        , std::fma       )

PP_MAKE_ADAPTOR(int64, (float64), ilogb, std::ilogb )
PP_MAKE_ADAPTOR(int64, (float64), irint, std::llrint)

/// WARING! here was a known bug of std::logb() in libc(version 2.15)
/// link: https://sourceware.org/bugzilla/show_bug.cgi?id=14343
/// so test cases which calls this function will definitely fail
/// until we find alternate or fixed version libc
PP_MAKE_ADAPTOR(float, (float64), logb, std::logb)

} } // namespace 'thor::math'

#undef PP_MAKE_ARG_DECL
#undef PP_MAKE_ADAPTOR
#undef PP_MAKE_MACRO_ADAPTOR
