/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

/**
 * Constructed lambda
 */
class my_lambda
{
	public function new(in_a: A) : void
	{
		a = in_a;
	}

	public function invoke(x : int32) : int32
	{
		return a.member + x;
	}

	var a : A;
}

class A
{
	public function new() : void
	{
		// var f = lambda(x: int32) : int32
		//         {
		//              return member + x;
		//         };
		// f(10);

		var _f = new my_lambda(this);
		var f = new Lambda1<int32, int32>(_f);
		output = f.invoke(10);
	}

	public var member : int32 = 100;
	public var output : int32;
}

@entry
task test1() : void
{
	var a = new A();

	if (a.output != 110) exit(-1);
	exit(0);
}
