/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
enum Type {
    B, D1, D2
}

//
class B implements Cloneable
{
    public var from_b: int32;

    public virtual function get_type(): Type
    {
        return Type.B;
    }

    public function new(i: int32): void
    {
        from_b = i;
    }

    public virtual function clone(): Cloneable
    {
        return new B(from_b);
    }
}

//
class Shared // not cloneable
{
    public var value: int32;
    public function new(i:int32)
    {
        value = i;
    }
}

class D1 extends B
{
    public var from_d1: int32;
    public var shared: Shared;

    public virtual function get_type(): Type
    {
        return Type.D1;
    }

    public function new(i: int32, s:Shared): void
    {
        super(i-1);
        from_d1 = i;
        shared = s;
    }

    public virtual function clone(): Cloneable
    {
        return new D1(from_d1, shared);
    }
}

//
class D2 extends D1
{
    public var from_d2: int32;

    public virtual function get_type(): Type
    {
        return Type.D2;
    }

    public function new(i: int32, s:Shared): void
    {
        super(i-1, s);
        from_d2 = i;
    }

    public virtual function clone(): Cloneable
    {
        return new D2(from_d2, shared);
    }
}

//
@entry
task test(): void
{
    var shared = new Shared(99);
    var source = new D2(2, shared);

    var mediate: B = source;
    var cloned = cast<D2>(mediate.clone());

    // check for the data members
    if(cloned.from_b  != 0)
        exit(1);

    if(cloned.from_d1 != 1)
        exit(2);

    if(cloned.from_d2 != 2)
        exit(3);

    // check for virtual method call
    if(mediate.get_type() != Type.D2)
        exit(4);

    // check for the reference members
    if(cloned.shared != source.shared)
        exit(5);

    if(cloned.shared.value != 99)
        exit(6);

    source.shared.value += 1;
    if(cloned.shared.value != 100)
        exit(7);

    exit(0);
}
