#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

thor_add_usecase_test(
    NAME           flag-test-integer
    ENTRY          test_integer
    EXTRA_RUN_OPTS "--args --tint=12345"
)

thor_add_usecase_test(
    NAME           flag-test-float
    ENTRY          test_float
    EXTRA_RUN_OPTS "--args --tfloat=1.0"
)

thor_add_usecase_test(
    NAME           flag-test-string
    ENTRY          test_string
    EXTRA_RUN_OPTS "--args --tstring=HelloThor"
)

thor_add_usecase_test(
    NAME           flag-test-has
    ENTRY          test_has
    EXTRA_RUN_OPTS "--args --t1=1 --t2=2.0 --t3=Hello"
)

thor_add_usecase_test(
    NAME           flag-test-help
    ENTRY          test_help
)

thor_add_usecase_test(
    NAME           flag-test-required-integer-failed
    ENTRY          test_required_integer_failed
)

thor_add_usecase_test(
    NAME           flag-test-required-integer-success
    ENTRY          test_required_integer_success
    EXTRA_RUN_OPTS "--args --req=12345"
)

thor_add_usecase_test(
    NAME           flag-test-unknown-options
    ENTRY          test_unknown_options
    EXTRA_RUN_OPTS "--args --xd=AAA"
)

thor_add_usecase_test(
    NAME           flag-test-empty-option
    ENTRY          test_empty_option
)

thor_add_usecase_test(
    NAME           flag-test-positional-integer
    ENTRY          test_positional_integer
    EXTRA_RUN_OPTS "--args 0 1 2 3 4"
)

thor_add_usecase_test(
    NAME           flag-test-positional-float
    ENTRY          test_positional_float
    EXTRA_RUN_OPTS "--args 0.1 1.1 2.1 3.1 4.1"
)

thor_add_usecase_test(
    NAME           flag-test-positional-string
    ENTRY          test_positional_string
    EXTRA_RUN_OPTS "--args AAA BBB CCC DDD EEE"
)

thor_add_usecase_test(
    NAME           flag-test-multi-positional
    ENTRY          test_multi_positional
    EXTRA_RUN_OPTS "--args 12121 33333 3.14159 0.333 123.456 abc EFG hij AAA"
)

thor_add_usecase_test(
    NAME           flag-test-positional-out-of-bound
    ENTRY          test_positional_out_of_bound
    EXTRA_RUN_OPTS "--args 1 2 3"
)

thor_add_usecase_test(
    NAME           flag-test-create-duplicate
    ENTRY          test_create_duplicate
)

thor_add_usecase_test(
    NAME           flag-test-get-failed
    ENTRY          test_get_failed
)

thor_add_usecase_test(
    NAME           flag-test-get-raw
    ENTRY          test_get_raw
    EXTRA_RUN_OPTS "--args --this -is so ^weird. Please ---do--it--yourself."
)
