/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;
import .= thor.container;
import .= thor.lang;

var execute_success : int32 =  0;
var execute_failure : int32 = -1;

function test_nCopies() : int32
{
    var n = 10;

    var expect = 777;
    var values = nCopies< Vector<int32> >(n, expect);    

    if(values.size() != n)
        return execute_failure;

    for(var idx = 0; idx < values.size(); ++idx) 
    {
        if(values.get(idx) != expect) 
            return execute_failure;
    }

    return execute_success;
}

function test_fill() : int32
{
    var n = 10;
    var values = new Vector<int32>( n );

    var expect = 777;
    fill(values, expect);

    if(values.size() != n)
        return execute_failure;

    for(var idx = 0; idx < values.size(); ++idx )
    {
        if(values.get(idx) != expect)
            return execute_failure;
    }

    return execute_success;
}

function test_replaceAll() : int32
{
    var n = 10;

    var values = new Vector<int32>();
    for(var cnt = 0; cnt < n; ++cnt)
        values.pushBack(-1);

    var expect = 777;
    for(var idx = 0; idx < values.size(); ++idx)
    {
        if(idx % 2 == 0) 
            values.set(idx, expect);
    }

    var found = replaceAll(values, -1, expect);
    if(found != (0 < n))
        return execute_failure;

    for(var it = values.iterator(); it.hasNext(); )
    {
        if(it.next() != expect)
            return execute_failure;
    }

    return execute_success;
}

function test_rotate() : int32
{
    var n = 1000;

    var integers = new Vector<int32>;
    for(var idx = 0; idx < n; ++idx)
        integers.pushBack(idx);

    var step : int32 = -n / 3;
    var rotated_integers = new Vector<int32>;
    for(var idx = 0; idx < n; ++idx) 
        rotated_integers.pushBack((n + idx - step) % n);

    rotate(integers, step);

    if(integers.size() != n)
        return execute_failure;
 
    if(integers.size() != rotated_integers.size())
        return execute_failure;    

    for(var idx = 0; idx < n; ++idx)
    {
        if(integers.get(idx) != rotated_integers.get(idx)) 
            return execute_failure;
    }

    return execute_success;
}

function test_shuffle() : int32
{
    var values = new Vector<int32>();
    for(var v = 1; v <= 30; ++v)
        values.pushBack(v);

    var shuffled = new Vector<int32>();
    for(var v in values)
        shuffled.pushBack(v);

    shuffle(shuffled);

    for(var idx = 0; idx < values.size(); ++idx)
        if(values[idx] != shuffled[idx])
            return execute_success;

    return execute_failure;
}

function test_binary_search() : int32
{
    var arr = [1,2,3,4,5];
    if(binarySearch(arr, 3) != 2)
        return execute_failure;

    if(binarySearch(arr, 6) != 5)
        return execute_failure;

    if(binarySearch(arr, 1) != 0)
        return execute_failure;

    if(binarySearch(arr, 0) != -1)
        return execute_failure;

    return execute_success;
}

@entry
task main() : void
{
    if(test_nCopies()       == execute_failure) exit(1);
    if(test_fill()          == execute_failure) exit(2);
    if(test_replaceAll()    == execute_failure) exit(3);
    if(test_rotate()        == execute_failure) exit(4);
    if(test_shuffle()       == execute_failure) exit(5);
    if(test_binary_search() == execute_failure) exit(6);

    exit(execute_success);
}
