/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;
import .= thor.lang;

@entry
task main() : void
{
    var exit_success : int32 = 0;

    var builder : StringBuilder = new StringBuilder;
    if( !builder.toString().isEqual("") ) exit(1);
    if( !(builder.length() == 0) ) exit(2);
    builder.reverse();
    if( !builder.toString().isEqual("") ) exit(3);
    if( !(builder.length() == 0) ) exit(4);

    builder = new StringBuilder("hello");
    if( !builder.toString().isEqual("hello") ) exit(5);
    if( !(builder.length() == 5) ) exit(6);
    builder.reverse();

    if( !builder.toString().isEqual("olleh") ) exit(7);
    if( !(builder.length() == 5) ) exit(8);

    builder.reverse();
    if( !builder.toString().isEqual("hello") ) exit(9);
    builder.append( " world!" );
    if( !builder.toString().isEqual("hello world!") ) exit(10);
    if( !(builder.length() == 12) ) exit(11);
    
    if( !builder.toString(3, 9).isEqual("lo wor") ) exit(12);
    
    var i8 : int8 = 10; // '\n'
    builder.appendAsCharacter( i8 );
    if( !(builder.length() == 13) ) exit(13);
    // followings are true if internal representation is using ASCII
    if( !builder.toString().isEqual("hello world!\n") ) exit(14);
    builder.appendAsCharacter( 91 ); // '['
    if( !(builder.length() == 14) ) exit(15);
    if( !builder.toString().isEqual("hello world!\n[") ) exit(16);


    builder = new StringBuilder("hello");

    i8 = 1;
    builder.append( i8 );
    if( !(builder.length() == 6) ) exit(17);
    if( !builder.toString().isEqual("hello1") ) exit(18);

    builder.append( 23 );
    if( !(builder.length() == 8) ) exit(19);
    if( !builder.toString().isEqual("hello123") ) exit(20);

    builder.append( -456 );
    if( !(builder.length() == 12) ) exit(21);
    if( !builder.toString().isEqual("hello123-456") ) exit(22);

    builder.append( 0.89f );
    if( !(builder.length() == 16) ) exit(23);
    if( !builder.toString().isEqual("hello123-4560.89") ) exit(24);

    builder.insert( 12, "-7" );
    if( !(builder.length() == 18) ) exit(25);
    if( !builder.toString().isEqual("hello123-456-70.89") ) exit(26);
    
    builder.insert( 0, 99.7 );
    if( !(builder.length() == 22) ) exit(27);
    if( !builder.toString().isEqual("99.7hello123-456-70.89") ) exit(28);

    exit(exit_success);
}
