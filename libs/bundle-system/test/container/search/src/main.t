/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.container;
import .= thor.lang;
import .= thor.util;

class Integer 
{
   private var value : int64;

   public function new(v: int64) : void
   {
      this.value = v;
   }

   public function compareTo( other: Integer ) : int64
   {
      return this.value - other.value;
   }
}

@entry
task main() : void
{
   var exit_success : int32 = 0;
   var exit_failure : int32 = -1;
   
   var part : int64 = 1000;
   var n : int64 = part * 3;
   //var n_minus_1 :int64 = n - 1;
   var idx : int64;
/*
   var float_gen : Random<float32, Uniform> = new Random<float32, Uniform>( 1.0f, cast<float32>(n_minus_1) );
   var floats : Vector<float32> = new Vector<float32>;
  
   var expected_float_max : float32 = n;
   var expected_float_min : float32 = 0; 
   for( idx = 0; idx != part; ++idx )
      floats.pushBack( float_gen.next() );
   floats.pushBack( expected_float_max );
   for( idx = 0; idx != part; ++idx )
      floats.pushBack( float_gen.next() );
   floats.pushBack( expected_float_min );
   for( idx = 0; idx != part; ++idx )
      floats.pushBack( float_gen.next() );

   var float_min : float32 = min<float32>( floats );
   if( !(float_min == expected_float_min) ) exit(exit_failure);

   var float_max : float32 = max<float32>( floats );
   if( !(float_max == expected_float_max) ) exit(exit_failure);


   var integer_gen : Random<int64, Uniform> = new Random<int64, Uniform>( 1L, n - 1 ); 
   var integers : Vector<Integer> = new Vector<Integer>;
   
   var expected_integer_max : Integer = new Integer( n );
   var expected_integer_min : Integer = new Integer( 0 );
   for( idx = 0; idx != part; ++idx )
      integers.pushBack( new Integer(integer_gen.next()) );
   integers.pushBack( expected_integer_max );
   for( idx = 0; idx != part; ++idx )
      integers.pushBack( new Integer(integer_gen.next()) );
   integers.pushBack( expected_integer_min );
   for( idx = 0; idx != part; ++idx )
      integers.pushBack( new Integer(integer_gen.next()) );

   var integer_min : Integer = min<Integer>( integers );
   if( !(integer_min.compareTo(expected_integer_min) == 0) ) exit(exit_failure);

   var integer_max : Integer = max<Integer>( integers );
   if( !(integer_max.compareTo(expected_integer_max) == 0) ) exit(exit_failure);
*/
   var sorted_values : Vector<int64> = new Vector<int64>;
   var expected_position : int64 = n / 2;
   for( idx = 0; idx < n; ++idx )
      sorted_values.pushBack( idx );

   var position : int64 = binarySearch( sorted_values, expected_position );
   if( !(position == expected_position) ) exit(exit_failure);

   position = binarySearch( sorted_values, -1 );
   if( !(position == -1) ) exit(exit_failure);

   position = binarySearch( sorted_values, n );
   if( !(position == sorted_values.size()) ) exit(exit_failure);

   for( idx = 0; idx < n; ++idx )
      sorted_values.set( idx, idx );

   var sub_list_length : int64 = 20;
   if( !(2*sub_list_length < n) ) exit(exit_failure);

   var sub_list : Vector<int64> = new Vector<int64>;
   for( idx = 0; idx < sub_list_length; ++idx )
      sub_list.pushBack( sub_list_length - idx );

   var position_chooser : Random<int64,Uniform> = new Random<int64,Uniform>( 0L, (n/2)-sub_list_length );
   expected_position = position_chooser.next();
   var expected_position2 = position_chooser.next();

   // set first sublist
   for( idx = 0; idx < sub_list_length; ++idx )
      sorted_values.set( idx + expected_position, sub_list.get(idx) );
   // set second sublist
   for( idx = 0; idx < sub_list_length; ++idx )
      sorted_values.set( idx + n/2 + expected_position2, sub_list.get(idx) );

   position = indexOfSubList(sorted_values, sub_list);
   if( !(position == expected_position) ) exit(exit_failure);

   position = lastIndexOfSubList(sorted_values, sub_list);
   if( !(position == n/2 + expected_position2) ) exit(exit_failure);

   sorted_values.set( 0, -1 );
   sorted_values.set( part, -1 );
   position = indexOf( sorted_values, -1 );
   if( !(position == 0) ) exit(exit_failure);

   position = lastIndexOf( sorted_values, -1 );
   if( !(position == part) ) exit(exit_failure);

   exit(exit_success);
}
