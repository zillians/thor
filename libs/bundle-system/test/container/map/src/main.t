/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;
import thor.util;

var execute_success : int32 = 0;
var execute_failure : int32 = -1;

class Integer
{
    public function new(init_value: int32): void
    {                                                                                                                                         
        internal_value = init_value;
    }   

    public function getValue(): int32
    {
        return internal_value;
    }

    private var internal_value: int32;
}

function get_map_iterator(): thor.container.MapIterator<String, Integer>
{
    var str_int_map = new thor.container.Map<String, Integer>();
    for(var new_value = 0; new_value < 5; ++new_value)
    {
        var key   = thor.util.Convert.toString(new_value);
        var value = new Integer(new_value);

        str_int_map.set(key, value);
    }

    return str_int_map.iterator();
}

function pass_or_print(counter: int32, it: thor.container.MapIterator<String, Integer>): void
{
    if (counter > 0)
    {
        async -> pass_or_print(counter - 1, it); 
        return;
    }                                                                                                                                         

    while (it.hasNext())
    {   
        const str_and_int = it.next();
        const key   = str_and_int.key;
        const value = str_and_int.value.getValue();

        print("(\{key}, \{value}) ");
    }   
    print("\n");

    exit(execute_success);
}

class Foo
{
    public function new(value:int32):void 
    {
        x = value;
    }
    
    public function isLessThan(rhs:Foo):bool
    {
        return x < rhs.x;
    }
    public function isEqual(rhs:Foo):bool
    {
        return x == rhs.x;
    }
    public function hash():int64
    {
        return x;
    }
    public var x:int32;
}

function test_access() : void
{
    ////////////////////////////////////////
    // int map
    ////////////////////////////////////////
    var int_map = new thor.container.Map<int32, int32>();

    int_map.set(1, 2);
    int_map.set(2, 4); int_map.set(3, 6);

    if(int_map.size() != 3) exit(1);
    var value;
    var result;
    value, result = int_map.get(1);
    if(!result || value != 2) exit(2);
    value, result = int_map.get(2);
    if(!result || value != 4) exit(3);
    value, result = int_map.get(3);
    if(!result || value != 6) exit(4);


    ////////////////////////////////////////
    // Foo map
    ////////////////////////////////////////
    var map_foo:thor.container.Map<Foo, int32>;
    map_foo = new thor.container.Map<Foo, int32>();
    
    var fa:Foo = new Foo(7);
    var fb:Foo = new Foo(8);
    var fc:Foo = new Foo(9);
    
    map_foo.set(fa, 1);
    map_foo.set(fb, 2);
    map_foo.set(fc, 3);
    
    if(map_foo.size()  != 3) exit(5);
    var foo;
    foo, result = map_foo.get(fa);
    if(!result || foo != 1) exit(6);
    foo, result = map_foo.get(fb);
    if(!result || foo != 2) exit(7);
    foo, result = map_foo.get(fc);
    if(!result || foo != 3) exit(8);
}

@entry
task main(): void
{
    test_access();

    pass_or_print(100, get_map_iterator());
}
