#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

thor_bundle_declare(
    NAME         system
    TARGET_NAME  thor-bundle-system
    OUTPUT_DIR   ${THORC_BUNDLE_PATH}
    DEPENDS      thor-toolchain
    DEPENDED     thor-collection
)

thor_bundle_add_arch_x86(
    INCLUDE_DIRS    ${THOR_INCLUDE_DIRS}
                    ${THOR_SYSTEM_BUNDLE_INCLUDE_DIR}
    SOURCES         ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/Language.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/String.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/Process.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/Domain.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/Debug.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/Flag.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/x86/Replication.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/math/x86/Math.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/util/x86/Convert.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/util/x86/Random.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/util/x86/UUID.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/util/x86/StringBuilder.cpp
                    ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/gc/x86/gc.cpp
)

IF(ENABLE_FEATURE_CUDA)
    thor_bundle_add_arch_cuda(
        SM_VERSIONS     30
        INCLUDE_DIRS    ${THOR_INCLUDE_DIRS}
                        ${THOR_SYSTEM_BUNDLE_INCLUDE_DIR}
        SOURCES         ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/cuda/Language.cu
                        ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/cuda/Process.cu
                        ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/cuda/Domain.cu
                        ${CMAKE_CURRENT_SOURCE_DIR}/src/thor/lang/cuda/Debug.cu
    )
ENDIF()

thor_bundle_define(
    INST_DEST thor/bundle
    INST_COMP thor
)

ADD_SUBDIRECTORY(test)
