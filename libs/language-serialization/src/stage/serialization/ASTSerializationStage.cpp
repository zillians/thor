/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "language/context/ParserContext.h"

#include "language/stage/serialization/ASTSerializationStage.h"
#include "language/stage/serialization/detail/ASTSerializationHelper.h"

namespace zillians { namespace language { namespace stage {

ASTSerializationStage::ASTSerializationStage() : enabled(false)
{ }

ASTSerializationStage::~ASTSerializationStage()
{ }

const char* ASTSerializationStage::name()
{
    return "AST Serialization Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ASTSerializationStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("emit-ast", po::value<std::string>(), "emit AST file");

    for(auto& option : option_desc_public->options())
        option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ASTSerializationStage::parseOptions(po::variables_map& vm)
{
    enabled = (vm.count("emit-ast") > 0);
    if(enabled)
    {
        ast_file = vm["emit-ast"].as<std::string>();
    }

    return true;
}

bool ASTSerializationStage::execute(bool& continue_execution)
{
    if(!enabled)
        return true;

    if(!hasParserContext())
        return false;

    if(!ASTSerializationHelper::serialize(ast_file, getParserContext().tangle))
        return false;

    //std::cerr << "serialize(" << ast_file << ") : " << getParserContext().tangle->dumpOffsetIds() << std::endl;
    // update the AST file last modification time-stamp if necessary
    if(getParserContext().last_modification_time > 0)
    {
        boost::filesystem::path p = ast_file;
        if(boost::filesystem::exists(p))
            boost::filesystem::last_write_time(p, getParserContext().last_modification_time);
    }

    UNUSED_ARGUMENT(continue_execution);

    return true;
}

} } }
