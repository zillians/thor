/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <fstream>
#include <string>

#include <boost/range/adaptor/map.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include "language/stage/serialization/detail/ASTSerializationHelper.h"
#include "language/stage/serialization/visitor/ASTDeserializationStageVisitor.h"
#include "language/stage/serialization/visitor/ASTSerializationStageVisitor.h"
#include "language/tree/ASTNode.h"

namespace zillians { namespace language { namespace stage {

bool ASTSerializationHelper::serialize(const std::string& filename, tree::ASTNode* node)
{
    std::ofstream ofs(filename);

    return serialize(ofs, node);
}

bool ASTSerializationHelper::serialize(std::ostream& output, tree::ASTNode* node)
{
    if(!output) return false;

    // serialize through boost archive
    boost::archive::binary_oarchive oa(output);
    tree::ASTNode* to_serialize = node;
    oa << to_serialize;

    // serialize all objects attached to ContextHub
    // see ASTSerializationStageVisitor::FullDeserializer, which defines the context object types needed to be serialized
    visitor::ASTSerializationStageVisitor<boost::archive::binary_oarchive> serialzer(oa);
    serialzer.visit(*to_serialize);
    return true;
}

tree::ASTNode* ASTSerializationHelper::deserialize(const std::string& filename)
{
    std::ifstream ifs(filename);
    if(!ifs.good())
    {
        std::cerr << "Can not open file `" << filename << "` to read" << std::endl;
        return NULL;
    }

    // de-serialize through boost archive
    boost::archive::binary_iarchive ia(ifs);
    tree::ASTNode* from_serialize = NULL;
    ia >> from_serialize;

    // de-serialize all objects attached to ContextHub
    // see ASTDeserializationStageVisitor::FullDeserializer, which defines the context object types needed to be de-serialized
    visitor::ASTDeserializationStageVisitor<boost::archive::binary_iarchive> deserialzer(ia);
    deserialzer.visit(*from_serialize);

    return from_serialize;
}

bool ASTSerializationHelper::serialize_bundle_ast(const std::string& filename, const BundleASTType& bundle_ast)
{
    std::ofstream ofs(filename);

    return serialize_bundle_ast(ofs, bundle_ast);
}

bool ASTSerializationHelper::serialize_bundle_ast(std::ostream& output, const BundleASTType& bundle_ast)
{
    if (!output)
        return false;

    // serialize through boost archive
    boost::archive::binary_oarchive oa(output);

    oa << bundle_ast;

    // serialize all objects attached to ContextHub
    // see ASTSerializationStageVisitor::FullDeserializer, which defines the context object types needed to be serialized
    visitor::ASTSerializationStageVisitor<boost::archive::binary_oarchive> serialzer(oa);

    serialzer.visit(*bundle_ast.get_tangle());

    for (const auto& decls : bundle_ast.get_grouped_may_conflict_decls() | boost::adaptors::map_values)
        for (auto*const decl : decls)
            serialzer.visit(*decl);

    return true;
}

auto ASTSerializationHelper::deserialize_bundle_ast(const std::string& filename) -> BundleASTType
{
    std::ifstream ifs(filename);

    if (!ifs.good())
    {
        std::cerr << "Can not open file `" << filename << "` to read" << std::endl;

        return BundleASTType::create_empty();
    }

    // de-serialize through boost archive
    boost::archive::binary_iarchive ia(ifs);
    BundleASTType                   bundle_ast = BundleASTType::create_empty();

    ia >> bundle_ast;

    if (bundle_ast.get_tangle() == nullptr)
    {
        std::cerr << "Tangle is null" << std::endl;

        return std::move(bundle_ast);
    }

    // de-serialize all objects attached to ContextHub
    // see ASTDeserializationStageVisitor::FullDeserializer, which defines the context object types needed to be de-serialized
    visitor::ASTDeserializationStageVisitor<boost::archive::binary_iarchive> deserialzer(ia);

    deserialzer.visit(*bundle_ast.get_tangle());

    for (const auto& decls : bundle_ast.get_grouped_may_conflict_decls() | boost::adaptors::map_values)
        for (auto*const decl : decls)
            deserialzer.visit(*decl);

    return std::move(bundle_ast);
}

} } }
