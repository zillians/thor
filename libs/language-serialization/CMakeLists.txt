#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

add_library(thor-language-serialization ${ZILLIANS_PREFERED_LIB_TYPE}
    src/stage/serialization/ASTSerializationStage.cpp
    src/stage/serialization/ASTDeserializationStage.cpp
    src/stage/serialization/BundleASTMergeStage.cpp
    src/stage/serialization/detail/ASTSerializationHelper.cpp
    )
target_link_libraries(thor-language-serialization
    thor-language-tree-refactor
    )
set_property(
    TARGET thor-language-serialization
    APPEND
    PROPERTY COMPILE_FLAGS "-g0 -O3"
    )

target_link_libraries(thor-language-serialization
    thor-common-utility
    thor-common-core
    thor-language-general
    thor-language-tree
    thor-language-stage-base
    )
