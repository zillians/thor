/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/make/detail/ThorDependencyParser.h"
#include "language/grammar/WhiteSpace.h"

namespace zillians { namespace language { namespace stage {

template<typename Iterator>
bool getImportedPackagesImpl(Iterator begin, Iterator end, std::vector<std::wstring>& v)
{
    return qi::phrase_parse(begin, end,
            // Begin grammar
            (
                /*  -( L"module" > *(boost::spirit::unicode::char_ - ';') > L';' )
                >*/ *( L"import"  >
                        (
                            // import . = a.b.c;
                              (*unicode::char_(L".") >> L'=' >> boost::spirit::as_wstring[*unicode::char_(L"0-9a-zA-Z_.")][boost::phoenix::push_back(boost::phoenix::ref(v), boost::spirit::qi::_1)] >> L';')
                            // import c = a.b.c;
                            | (*unicode::char_(L"0-9a-zA-Z_") >> L'=' >> boost::spirit::as_wstring[*unicode::char_(L"0-9a-zA-Z_.")][boost::phoenix::push_back(boost::phoenix::ref(v), boost::spirit::qi::_1)] >> L';')
                            // import a.b.c;
                            | (boost::spirit::as_wstring[*unicode::char_(L"0-9a-zA-Z_.")][boost::phoenix::push_back(boost::phoenix::ref(v), boost::spirit::qi::_1)] >> L';')
                        )
                   )
                > *( boost::spirit::unicode::char_ )
                > qi::eoi
            ),
            // End grammar
            grammar::detail::WhiteSpace<Iterator>());
}

bool getImportedPackages(pos_iterator_type begin, pos_iterator_type end, std::vector<std::wstring>& v)
{
    return getImportedPackagesImpl(begin, end, v);
}

} } }
