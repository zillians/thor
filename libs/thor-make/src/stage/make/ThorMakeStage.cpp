/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/UnicodeUtil.h"
#include "utility/Filesystem.h"
#include "utility/sha1.h"

#include "language/tree/ASTNode.h"
#include "language/tree/module/Tangle.h"

#include "language/stage/serialization/detail/ASTSerializationHelper.h"

#include "language/toolchain/ThorToolBase.h"
#include "language/stage/make/ThorMakeStage.h"
#include "language/stage/make/detail/ThorBuildGraph.h"

namespace zillians { namespace language { namespace stage {

namespace po = boost::program_options;

ThorMakeStage::ThorMakeStage()
    : force_rebuild(false)
    , build_as_a_whole(false)
    , emit_debug_info(false)
    , opt_level(0)
{
    config.loadDefault(true, false, false);
}

ThorMakeStage::~ThorMakeStage()
{ }

const char* ThorMakeStage::name()
{
    return "make_stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> ThorMakeStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("verbose,v", "verbose mode, which dumps compile command")
        ("force-rebuild,f", "force rebuild everything")
        ("whole,w", "build everything as a whole")
        ("jobs,j", po::value<int>(), "allow N jobs at once; automatically decided if not specified")
        ("project-path", po::value<std::string>(), "set the project root path")
        ("build-path", po::value<std::string>(), "set the build directory")
        ("static-test-parse", "Run into static test for script parsing")
        ("static-test-constant-folding", "Run into static test for constant folding")
        ("static-test-s0", "Run into static test for semantic checking (s0)")
        ("static-test-xform", "Run into static test for AST transforming")
        ("static-test-cast", "Run into static test for implicit casting")
        ("static-test-s1", "Run into static test for semantic checking (s1)")
        ("emit-debug-info", po::bool_switch(), "generate debug info for debugging if on")
        ("optimization-level,O", po::value<unsigned>()->default_value(0u), "Set the optimization level (0~3), and the default opt level for release build is 2 while for debug build is 0. However, in debug build, you could explicitly set optimizeation level which would overwritten the default value.")
        ("debug-tool,g", po::value<std::string>()->implicit_value("thor-compile"), "Debug specific Thor tool. e.g: thor-make debug --debug-tool=thor-compile")
        ("debug-tool-cmd", po::value<std::string>(), "Custom debugger command to debug specific tool from --debug-tool")
        ("timing,t", po::bool_switch(), "Timing elapsed time of each stage")
    ;

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
        ("dump-ts", "dump intermediate AST in Thor format")
        ("dump-graphviz", po::bool_switch(), "dump intermediate AST in graphviz format")
        ("prepend-package", po::value<std::string>(), "prepend package to all sources")
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool ThorMakeStage::parseOptions(po::variables_map& vm)
{
    if (vm.count("force-rebuild"))
        force_rebuild = true;

    if (vm.count("whole"))
        build_as_a_whole = true;

    emit_debug_info = vm["emit-debug-info"   ].as<bool>();
    opt_level       = vm["optimization-level"].as<unsigned>();

    if (vm.count("static-test-parse"))
        config.static_test_case = "static-test-parse";

    if (vm.count("static-test-constant-folding"))
        config.static_test_case = "static-test-constant-folding";

    if (vm.count("static-test-s0"))
        config.static_test_case = "static-test-s0";

    if (vm.count("static-test-xform"))
        config.static_test_case = "static-test-xform";

    if (vm.count("static-test-cast"))
        config.static_test_case = "static-test-cast";

    if (vm.count("static-test-s1"))
        config.static_test_case = "static-test-s1";

    if (vm.count("prepend-package"))
        config.prepend_package = vm["prepend-package"].as<std::string>();
    else
        config.prepend_package = "";

    if (vm.count("jobs"))
    {
        if (vm["jobs"].empty())
            config.max_parallel_jobs = 0;
        else
            config.max_parallel_jobs = vm["jobs"].as<int>();
    }
    else
    {
        config.max_parallel_jobs = 1;
    }

    if (vm.count("verbose"))
        config.verbose = true;
    else
        config.verbose = false;

    if (vm.count("dump-ts"))
        config.dump_ts = true;
    else
        config.dump_ts = false;

    config.dump_graphviz = vm["dump-graphviz"].as<bool>();

    if (vm.count("project-path"))
        config.project_path = vm["project-path"].as<std::string>();

    if (vm.count("build-path"))
        config.build_path_override = vm["build-path"].as<std::string>();

    if (vm.count("debug-tool"))
        config.debug_tool = vm["debug-tool"].as<std::string>();

    if (vm.count("debug-tool-cmd"))
        config.debug_tool_cmd = vm["debug-tool-cmd"].as<std::string>();

    config.timing = vm["timing"].as<bool>();

    return true;
}

void ThorMakeStage::generateCompileParameters(std::vector<boost::filesystem::path>& sources, std::vector<boost::filesystem::path>& imports, /*OUT*/ std::vector<boost::filesystem::path>& outputs, /*OUT*/ std::string& parameters, /*OUT*/ bool& is_dirty)
{
    is_dirty = false;
    std::stringstream ss;

    ss << " '--root-dir=" << config.source_path.string() << "'";

//    if(config.build_type == ThorBuildConfiguration::BuildType::DEBUG)
//      parameters += " --debug";

    if(emit_debug_info)
        ss << " --emit-debug-info";

    if(opt_level > 0)
        ss << " --optimization-level=" << opt_level;

    if(config.manifest.no_system)
        ss << " --no-system";

    // timing
    if(config.timing)
        ss << " --timing";

    ss << " --arch-flag=" << config.manifest.arch.getFlags();
    std::string concated_file_paths;

    // source files
    std::time_t last_modification_time_on_all_inputs = 0;
    for(auto& source : sources)
    {
        boost::filesystem::path p = source;
        if(boost::filesystem::exists(p))
        {
            std::time_t t = boost::filesystem::last_write_time(p);
            if(last_modification_time_on_all_inputs < t) last_modification_time_on_all_inputs = t;
        }

        ss << " '" << source.string() << "'";

        concated_file_paths += source.string() + "&";
    }

    // ast files
    for(auto& to_import : imports)
    {
        boost::filesystem::path p = to_import;
        if(boost::filesystem::exists(p))
        {
            std::time_t t = boost::filesystem::last_write_time(p);
            if(last_modification_time_on_all_inputs < t) last_modification_time_on_all_inputs = t;
        }

        parameters += " '--dep-bundle-asts=" + to_import.string() + "'";

        concated_file_paths += to_import.string() + "&";
    }

    if(config.static_test_case != "static-test-parse")
    {
        // ast files
        for(auto& to_import : imports)
        {
            boost::filesystem::path p = to_import;
            if(boost::filesystem::exists(p))
            {
                std::time_t t = boost::filesystem::last_write_time(p);
                if(last_modification_time_on_all_inputs < t) last_modification_time_on_all_inputs = t;
            }

            ss << " '--dep-bundle-asts=" << to_import.string() << "'";

            concated_file_paths += to_import.string() + "&";
        }
    }

    // output files
    std::string sha1_on_concated_file_paths = sha1::sha1(concated_file_paths);
    boost::filesystem::path ast_to_emit = config.build_path / (sha1_on_concated_file_paths + THOR_EXTENSION_AST);
//    boost::filesystem::path bc_to_emit = config.build_path / (sha1_on_concated_file_paths + THOR_EXTENSION_BC);
//    boost::filesystem::path cpp_to_emit = config.build_path / (sha1_on_concated_file_paths + THOR_EXTENSION_CPP);

    if(config.static_test_case.empty())
    {
        ss << " '--emit-ast=" << ast_to_emit.string() << "'";
        outputs.push_back(ast_to_emit);

        // check if we really need to build this
        if(boost::filesystem::exists(ast_to_emit))
        {
            std::time_t t = boost::filesystem::last_write_time(ast_to_emit);
            if(t != last_modification_time_on_all_inputs)
                is_dirty = true;
        }
        else
        {
            is_dirty = true;
        }

        if(config.dump_ts)
        {
            ss << " '--dump-ts'";
            if(!config.dump_ts_dir.empty())
                ss << " '--dump-ts-dir=" << config.dump_ts_dir.string() << "'";
        }

        if(config.dump_graphviz)
        {
            ss << " '--dump-graphviz'";
            if(!config.dump_graphviz_dir.empty())
                ss << " '--dump-graphviz-dir=" << config.dump_graphviz_dir.string() << "'";
        }

        if(!config.prepend_package.empty())
        {
            ss << " '--prepend-package=" << config.prepend_package << "'";
        }
    }
    else
    {
        if(config.static_test_case == "static-test-parse")
        {
            ss << " --mode-parse";
        }
        else if(config.static_test_case == "static-test-constant-folding")
        {
            ss << " --mode-constant-folding";
        }
        else if(config.static_test_case == "static-test-s0")
        {
            ss << " --mode-semantic-verify-0";
        }
        else if(config.static_test_case == "static-test-xform")
        {
            ss << " --mode-xform";
        }
        else if(config.static_test_case == "static-test-cast")
        {
            ss << " --mode-implicit-cast";
        }
        else if(config.static_test_case == "static-test-s1")
        {
            ss << " --mode-semantic-verify-1";
        }

        ss << " --enable-static-test";

        is_dirty = true;
    }

    parameters = ss.str();
}

bool ThorMakeStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!config.loadDefault(false, true, true))
    {
        std::cerr << "Error: failed to load project configuration" << std::endl;
        return false;
    }
    config.createDirectoriesIfNecessary();

    // find all sources under the source directory
    std::vector<boost::filesystem::path> all_sources = Filesystem::collect_files(config.source_path, THOR_EXTENSION_SOURCE, true);

    // find all bundle AST files, which is extracted by thor-bundle
    std::vector<boost::filesystem::path> all_imports;
    for(auto& bundle : config.manifest.deps.bundles)
    {
        boost::filesystem::path extract_to_path = config.extract_bundle_path / bundle.name;

        std::vector<boost::filesystem::path> ast_files = Filesystem::collect_files(extract_to_path, THOR_EXTENSION_AST, true);
        all_imports.insert(all_imports.begin(), ast_files.begin(), ast_files.end());
    }

    // keep track of all generated outputs
    std::vector<boost::filesystem::path> all_outputs;

    bool result = true;

    if(build_as_a_whole)
    {
        std::string parameters;
        bool is_dirty = false;

        std::vector<boost::filesystem::path> outputs;
        generateCompileParameters(all_sources, all_imports, outputs, parameters, is_dirty);

        if(is_dirty || force_rebuild)
        {
            int ec = ThorToolBase::shell(config, THOR_COMPILER, parameters);
            if(ec != 0) result = false;
        }
        else
        {
            std::cout << "Compilations is update to date, skipped" << std::endl;
        }

        all_outputs.insert(all_outputs.end(), outputs.begin(), outputs.end());
    }
    else
    {
        ThorBuildGraph build_graph(config.source_path);

        // add all sources under the source directory
        for(auto& source : all_sources) build_graph.addSource(source);

        // add all bundle AST files, which is extracted by thor-bundle
        for(auto& to_import : all_imports) build_graph.addImport(to_import);


        result = build_graph.build([&](std::vector<boost::filesystem::path>& sources, std::vector<boost::filesystem::path>& imports, std::vector<boost::filesystem::path>& outputs) -> int {

            std::string parameters;
            bool is_dirty = false;

            generateCompileParameters(sources, imports, outputs, parameters, is_dirty);

            all_outputs.insert(all_outputs.end(), outputs.begin(), outputs.end());

            if(is_dirty || force_rebuild)
            {
                int ec = ThorToolBase::shell(config, THOR_COMPILER, parameters);
                return ec;
            }
            else
            {
                std::cout << "Compilations for:" << std::endl;
                for(auto& source : sources) {
                    std::cout << "\t'" << source.string() << "'" << std::endl;
                }
                for(auto& to_import : imports) {
                    std::cout << "\t'" << to_import.string() << "'" << std::endl;
                }
                std::cout << "are update to date, skipped" << std::endl;
                return 0;
            }
        });
    }

    // if all builds are successful, create the merged AST
    if(result)
    {
        tree::Tangle* tangle = NULL;
        boost::filesystem::path merged_ast_file = config.build_path / (config.manifest.name + THOR_EXTENSION_AST);

        if(Filesystem::is_regular_file(merged_ast_file))
            boost::filesystem::remove(merged_ast_file);

        for(auto& output : all_outputs)
        {
            if(output.extension() == THOR_EXTENSION_AST)
            {
                tree::ASTNode* deserialized = ASTSerializationHelper::deserialize(output.string());

                if(!deserialized || !tree::isa<tree::Tangle>(deserialized))
                {
                    std::cerr << "Error: failed to deserialize AST file: '" << output.string() << "'" << std::endl;
                    return false;
                }

                tree::Tangle* current_tangle = tree::cast<tree::Tangle>(deserialized);
                if(!current_tangle)
                {
                    std::cerr << "Error: invalid AST format: '" << output.string() << "'" << std::endl;
                    return false;
                }

                if(!tangle)
                    tangle = current_tangle;
                else
                    tangle->merge(*current_tangle);
            }

            if(tangle)
            {
                // serialize the merged AST
                if(!ASTSerializationHelper::serialize(merged_ast_file.string(), tangle))
                {
                    std::cerr << "Error: failed to serialize merged AST" << std::endl;
                    return false;
                }
                else
                {
                    all_outputs.push_back(merged_ast_file);
                }
            }
        }
    }

    // clean up stale files under build directory
    std::vector<boost::filesystem::path> all_files_under_build = Filesystem::collect_files(config.build_path, THOR_EXTENSION_AST, false);
    for(auto& filename : all_files_under_build)
    {
        // only clean up regular files
        if(Filesystem::is_regular_file(filename))
        {
            // avoid deleting all hidden files (.xxxxx like .cache)
            if(!filename.stem().empty())
            {
                // if we can't find it in the all outputs list, that should be a stale file, then we remove it
                if(std::find_if(
                        all_outputs.begin(),
                        all_outputs.end(),
                        [&](const boost::filesystem::path& x) { return boost::filesystem::equivalent(x, filename); }) == all_outputs.end())
                    boost::filesystem::remove(filename);
            }
        }
    }

    return result;
}

} } }
