/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.container;
import .= thor.util;

class Int32InvertCmp
{
    public function compare( lhs: int32, rhs: int32 ) : int32
    {
       return rhs - lhs;
    }
}

class Integer
{
    public var index : int32;
    public var value : int32;

    public function new( i:int32, v:int32 ) : void
    {
       index = i;
       value = v;
    }
    
    public function compareTo( rhs:Integer ) : int32
    {
        return value - rhs.value;
    }
}

class IntegerCmp
{
    public function compare( lhs:Integer, rhs:Integer ) : int32
    {
        var result : int32 = lhs.compareTo(rhs);
        if( result == 0 )
            return lhs.index - rhs.index;

        return result;
    }
}

@entry
task main() : void
{
    var exit_success : int32 = 0;
    var exit_failure : int32 = -1;

    var container_size : int32 = 1000000;
    var values : Vector<int32> = new Vector<int32>;
    var gen : Random<int32, Uniform> = new Random<int32, Uniform>( 1, container_size/2 );

    var cnt : int32 = 0;
    for( cnt = 0; cnt != container_size; ++cnt )
        values.pushBack( gen.next() );

    sort( values );
    if( !isAscending(values) ) exit(exit_failure);

    var invert_cmp : Int32InvertCmp = new Int32InvertCmp;
    reverse( values );

    if( !isAscending(values, invert_cmp) ) exit(exit_failure);

    for( cnt = 0; cnt != container_size; ++cnt )
        values.set( cnt, gen.next()  );
    
    partialSort( values, container_size/2 );
    var values2 : Vector<int32> = new Vector<int32>;
    
    for( cnt = 0; cnt != container_size/2; ++cnt )
        values2.pushBack( values.get(cnt) );
   
    if( !isAscending(values2) ) exit(exit_failure);

    for( cnt = 0; cnt != container_size/2; ++cnt )
        values2.set( cnt, gen.next() );

    for( cnt = 0; cnt != container_size/2; ++cnt )
        values2.pushBack( gen.next() );

    if( !(values.size() == values2.size()) )
        exit(exit_failure);

    copy( values2, values );
   
    for( cnt = 0; cnt != container_size; ++cnt )
       if( !(values.get(cnt) == values2.get(cnt)) ) exit(exit_failure);
    
    var objs : Vector<Integer> = new Vector<Integer>;
    for( cnt = 0; cnt != container_size; ++cnt );
        objs.pushBack( new Integer(cnt, gen.next()) );

    stableSort( objs );
    if( !isAscending(objs, new IntegerCmp) ) exit(exit_failure);

    exit(exit_success);   
}
