/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Foo
{
    public function toString() : String
    {
        return "a=\{a}";
    }
    public var a : int32 = 8;
}

@entry
task test():void
{
    var exit_success : int32 = 0;
    var exit_failure : int32 = -1;
    
    var a : int32 = 99;
    var s : String = "the value of a is: \{ a }";
    if( !(s.isEqual("the value of a is: 99")) )
        exit(exit_failure);

    a = -99;
    s = "the value of a is: \{ a } now";
    if( !(s.isEqual("the value of a is: -99 now")) )
        exit(exit_failure);

    var name : String = "John";
    a = 75;
    s = "the weight of \{ name } is \{ a } kg"; 
    if( !(s.isEqual("the weight of John is 75 kg")) )
        exit(exit_failure);

    name = "Snow White";
    s = "\{name} is the most beautiful woman in the world";
    if( !(s.isEqual("Snow White is the most beautiful woman in the world")) )
        exit(exit_failure);

    a = 10;
    var b : float32 = 1.5;
    var c : String = "12";
    s = " \{a} + \{b} != \{c} ";
    if( !s.isEqual(" 10 + 1.5 != 12 ") )
        exit(exit_failure);

    var foo_obj = new Foo();
    s = "super \{foo_obj.a}!";
    if( !s.isEqual("super 8!") )
        exit(exit_failure);

    s = "\{foo_obj}";
    if( !s.isEqual("a=8") )
        exit(exit_failure);

    exit(exit_success);
}
