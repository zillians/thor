/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;

class QueueInt32
{
        
        public function new() : void
        {
                pushStak = new thor.container.Vector<int32>();
                popStak = new thor.container.Vector<int32>();
                mSize = 0 ;
                mode = true;
        }
        
        public function enQueue(a:int32):void
        {
                if(mode==true)
                {
                        pushStak.pushBack(a);
                }
                else
                {
                        var i:int32;
                        for(i = mSize -1 ; i >=0; --i)
                        {
                                pushStak.pushBack( popStak.get(i) );
                                popStak.popBack();
                        }
                        pushStak.pushBack(a);
                        mode = true;
                }
                ++mSize;
        }

        public function deQueue():int32
        {
                var result:int32;
                --mSize;
                if(mode == false)
                {
                        result = popStak.get(mSize);
                        popStak.popBack();
                        return result;
                }
                else
                {
                        mode = false;
                        var i:int32;
                        for(i = mSize ; i >=0; --i )
                        {
                                popStak.pushBack( pushStak.get(i) );
                                pushStak.popBack();
                        }
                        result = popStak.get(mSize);
                        popStak.popBack();
                        return result;

                }
        }
        
        public function nextValue():int32
        {
                if(mode == true)
                        return pushStak.get(0);
                else
                        return popStak.get(mSize-1);
        }
        public function size():int32
        {
                return mSize; 
        }

        var pushStak:thor.container.Vector<int32>;
        var popStak:thor.container.Vector<int32>;

        var mSize:int32;
        var mode:bool;
}

