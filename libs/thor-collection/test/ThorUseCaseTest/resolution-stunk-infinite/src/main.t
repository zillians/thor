/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= test;
import .= thor.container;
import .= thor.util;

class Foo
{
    public var val:int32 = 0;  
}

function print_vec<T>(ar : Vector<T>) : void
{
    for(var val in ar) 
    {   
        print("\{val} ");
    }   
    print("\n");
}
function print_vec(ar : Vector<Foo>): void
{
    for(var f in ar) 
    {   
        print("\{f.val} ");
    }   
    print("\n");
}

@entry 
task main() : void
{
    const cSize:int32 = 20; 
    var t1 = new Vector<int32>(cSize);
    var generator = new Random<int32,Uniform>(0,100); 
    for(var i = 0; i < t1.size(); ++i)
    {   
        t1[i] = generator.next();
    }   
    print("Before: \n");
    print_vec(t1);
    qs(t1);
    print("After: \n");
    print_vec(t1);

    exit(0);
}
