/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@entry
task test1() : void
{
    while (true) break;
    for(;;) break;
    do 
        break; 
    while(true);
    exit(0);
}

@entry
task test2() : void
{
    var x = 0;
    var z = 0;
    var y = 0;
    while (true)
    {
        break;
        x = 3;
    }

    do {
        break;
        y = 5;
    } while (true);

    for (;;) 
    {
        break;
        z = 4;
    }

    

    exit((x == 0 && y == 0 && z == 0) ? 0 : 1);
}

@entry
task test3() : void
{
    while (true)
    {
        exit(0);
    }

    exit(1);
}

@entry
task test4() : void
{
    var x = 0;
    var y = 0;
    var z = 0;

    while (true)
    {
        x += 4;
        break;
    }

    for(;;)
    {
        y += 3;
        break;
    }

    do {
        z += 5;
        break;
    } while (true);

    exit((x == 4 && y == 3 && z == 5) ? 0 : 1);
}

@entry
task test5() : void
{
    var x = 0;
    while (true)
    {
        if (x >= 5) break;
        ++x;
    }

    var y = 0;
    for(;;)
    {
        if (y>=3) break;
        ++y;
    }

    var z = 0;
    do {
        if (z>=1) break;
        ++z;
    } while (true);

    exit((x == 5 && y == 3 && z == 1) ? 0 : 1);
}

@entry
task test6() : void
{
    for(;;)
    {
        exit(0);
    }

    exit(1);
}

@entry
task test7() : void
{
    var a = 0;

    while (true) {
        ++a;
        while (true) {
            ++a;
            
            for (;;) {
                ++a;
                break;
                ++a;
            }

            while (true) {  
                ++a;
                break;
                ++a;
            }

            break;
            ++a;
        }

        for (;;) {
            ++a;
            while( true) {
                ++a;
                break;
                ++a;
            }
            break;
            ++a;
        }

        do {
            ++a;
            while(true) {
                ++a;
                break;
                ++a;
            }
            break;
            ++a;
        } while (true);

        break;
        ++a;

        do {
            ++a; break;
        } while (true);

        while (true) { ++a; break; }
        for (;;) { ++a; break; }
    }

    exit((a == 8) ? 0 : 1);
}

@entry
task test8() : void
{
    do {
        exit(0);
    } while (true);

    exit(1);
}

@entry
task test9() : void
{
    var f = true;
    while (f)
        break;

    exit(0);
}
