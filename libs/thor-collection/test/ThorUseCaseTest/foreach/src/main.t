/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.lang;
import thor.container;

class X {
    public function new (vv : int32) {
        v = vv ;
    }
    public function isLessThan(rhs : X) : bool {
        return v < rhs.v;
    }
    public var v : int32 ;
}

@entry
task main() : void {
    var m = new thor.container.Map<int32, int32>();
    m.set(1, 2);
    m.set(2, 4);
    m.set(3, 6);

    var key_sum = 0;
    var value_sum = 0;
    for(var e in m) {
        key_sum += e.key;
        value_sum += e.value;
    }

    if (key_sum != 6) exit(1);
    if (value_sum != 12) exit(2);

    var xm = new thor.container.Map<X, X>();
    xm.set(new X(1), new X(2));
    xm.set(new X(2), new X(4));
    xm.set(new X(3), new X(6));

    key_sum = value_sum = 0;
    for(var e in xm) {
        key_sum += e.key.v;
        value_sum += e.value.v;
    }
    if (key_sum != 6) exit(3);
    if (value_sum != 12) exit(4);

    var a = new thor.container.Vector<int32>(3);
    a[0] = 2;
    a[1] = 3;
    a[2] = 5;

    var sum = 0;
    for(var e in a) {
        sum += e;
    }
    if (sum != 10) exit(5);

    var xa = new thor.container.Vector<X>(3);
    xa[0] = new X(20);
    xa[1] = new X(30);
    xa[2] = new X(50);

    sum = 0;
    for(var e in xa) {
        sum += e.v;
    }
    if (sum != 100) exit(6);

    var gg = new thor.lang.Array<int32>(4);
    gg = [9, 1, 3, 8];

    sum = 0;
    for(var e in gg) {
        sum += e;
    }
    if (sum != 21) exit(7);

    sum = 0;
    for(var e in [8, 1, 3, 9]) {
        sum += e;
    }
    if (sum != 21) exit(8);

    exit(0);
}

