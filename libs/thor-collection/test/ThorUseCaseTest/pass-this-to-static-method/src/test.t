/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Base {
	public var w : int32;
	public var x : int32;
	public var z : int32;
}

class Derived extends Base {
	public function new() : void {
		InitializeToZero.apply( this );
	}
}

class InitializeToZero {
	public static function apply( b : Base ) : void {
		b.w = 123;
		b.x = 999;
		b.z = 77777;
	}
}

@entry
task main() : void {
	var b :Base = new Base();
	InitializeToZero.apply( b );

	var d :Base = new Derived();

	exit(0);
}
