/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.util;

@entry
task main() : void
{
    var i1 : int32;
    var i2 : int32;

    var s1 : String;
    var s2 : String;

    i1 = 0; s1 = "";
    i1, s1 = is();
    if( i1 != 123 || !s1.isEqual("456") )
        exit(1);

    i1 = 0; s1 = "";
    s1, i1 = si();
    if( !s1.isEqual("123") || i1 != 456 )
        exit(2);

    i1 = i2 = 0;
    i1, i2 = ii();
    if( i1 != 123 || i2 != 456 )
        exit(3);

    s1 = s2 = "";
    s1, s2 = ss();
    if( !s1.isEqual("123") || !s2.isEqual("456") )
        exit(4);

    i1 = i2 = 0; s1 = "";
    i1, i2, s1 = iis();
    if( i1 != 123 || i2 != 456 || !s1.isEqual("789") )
        exit(5);

    i1 = i2 = 0; s1 = "";
    i1, s1, i2 = isi();
    if( i1 != 123 || !s1.isEqual("456") || i2 != 789 )
        exit(6);

    i1 = i2 = 0; s1 = "";
    i1, s1, s2 = iss();
    if( i1 != 123 || !s1.isEqual("456") || !s2.isEqual("789") )
        exit(7);

    i1 = i2 = 0; s1 = "";
    s1, i1, i2 = sii();
    if( !s1.isEqual("123") || i1 != 456 || i2 != 789 )
        exit(8);

    i1 = 0; s1 = s2 = "";
    s1, i1, s2 = sis();
    if( !s1.isEqual("123") || i1 != 456 || !s2.isEqual("789") )
        exit(9);

    s1 = s2 = ""; i1 = 0;
    s1, s2, i1 = ssi();
    if( !s1.isEqual("123") || !s2.isEqual("456") || i1 != 789 )
        exit(10);

    var s3 : String;
    s1 = s2 = s3 = "";
    s1, s2, s3 = sss();
    if( !s1.isEqual("123") || !s2.isEqual("456") || !s3.isEqual("789") )
        exit(11);

    var i3 : int32;
    i1 = i2 = i3 = 0;
    i1, i2, i3 = iii();
    if( i1 != 123 || i2 != 456 || i3 != 789 )
        exit(12);

    var c1 : int8;
    var c2 : int8;
    var c3 : int8;

    c1 = c2 = 0; i1 = 0;
    c1, c2, i1 = ret114();
    if( c1 != 1 || c2 != 2 || i1 != 3 )
        exit(13);

    c1 = c2 = 0; i1= 0;
    c1, i1, c2 = ret141();
    if( c1 != 1 || i1 != 2 || c2 != 3 )
        exit(14);

    c1 = 0; i1 = i2 = 0;
    c1, i1, i2 = ret144();
    if( c1 != 1 || i1 != 2 || i2 != 3 )
        exit(15);

    i1 = 0; c1 = c2 = 0;
    i1, c1, c2 = ret411();
    if( i1 != 1 || c1 != 2 || c2 != 3 )
        exit(16);

    i1 = i2 = 0; c1 = 0;
    i1, c1, i2 = ret414();
    if( i1 != 1 || c1 != 2 || i2 != 3 )
        exit(17);

    i1 = i2 = 0; c1 = 0;
    i1, i2, c1 = ret441();
    if( i1 != 1 || i2 != 2 || c1 != 3 )
        exit(18);

    i1 = i2 = i3 = 0;
    i1, i2, i3 = ret444();
    if( i1 != 1 || i2 != 2 || i3 != 3 )
        exit(19);

    c1 = c2 = c3 = 0;
    c1, c2, c3 = ret111();
    if( c1 != 1 || c2 != 2 || c3 != 3 )
        exit(20);

    exit(0);
}

function ss() : (String,String)
{
    return ("123","456");
}

function is() : (int32,String)
{
    return (123,"456");
}

function si() : (String, int32)
{
    return ("123",456);
}

function ii() : (int32, int32)
{
    return (123,456);
}

function iis() : (int32, int32, String)
{
    return (123,456,"789");
}

function isi() : (int32, String, int32)
{
    var s = "456";
    return (123,s,789);
}

function iss() : (int32, String, String)
{
    return (123,"456","789");
}

function sii() : (String, int32, int32)
{
    return ("123",456,789);
}

function sis() : (String, int32, String)
{
    return ("123",456,"789");
}

function ssi() : (String, String, int32)
{
    return ("123","456",789);
}

function sss() : (String, String, String)
{
    return ("123","456","789");
}

function iii() : (int32, int32, int32)
{
    return (123,456,789);
}

function ret111() : (int8,int8,int8)
{
    return (1,2,3);
}

function ret114() : (int8,int8,int32)
{
    return (1,2,3);
}

function ret141() : (int8,int32,int8)
{
    return (1,2,3);
}

function ret144() : (int8, int32, int32)
{
    return (1,2,3);
}

function ret411() : (int32, int8, int8)
{
    return (1,2,3);
}

function ret414() : (int32, int8, int32)
{
    return (1,2,3);
}

function ret441() : (int32, int32, int8)
{
    return (1,2,3);
}

function ret444() : (int32,int32,int32)
{
    return (1,2,3);
}
