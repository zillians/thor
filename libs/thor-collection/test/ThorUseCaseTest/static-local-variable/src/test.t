/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function get() : int32
{
	static var x: int32 = 10;
	static var y: int32 = x + 1;
	++y;
	return y;
}

class A
{
	public function new() : void { ref = 12; }
	public var ref : int64;
}

function getA() : A
{
	static var a: A = new A();
	++a.ref;
	return a;
}

class B
{
	public function new() : void
	{
		static var count : int32 = 10;
		++count;

		id = count;
	}

	public var id: int32;
}

@entry
task test1() : void
{
	if (get() != 12) exit(-1);
	if (get() != 13) exit(-2);

	exit(0);	
}

@entry
task test2() : void
{
	if (getA().ref != 13) exit(-1);
	if (getA().ref != 14) exit(-2);
	
	exit(0);
}

@entry
task test3() : void
{
	var b1 : B = new B();
	var b2 : B = new B();

	if (b1.id != 11) exit(-1);
	if (b2.id != 12) exit(-2);

	exit(0);
}
