/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import p1;

@entry
task main():void
{
        var a:int32;
        var b:int32;
        var c:int32;
        var d:float32;
        d = 3.1415926;

        a = 50;
        b = 50;

        c = p1.add(a,b);

        a = -1;
        b = 50; 

        for(var i:int32= 0; i < 6000; ++i)
        {
                for(var j:int32 = -6000; j < 6000; ++j)
                {
                        c = p1.mul(i,j);
                        if(c != (i*j))
                                exit(-1); 
                }
        }

        exit(0); 
}
