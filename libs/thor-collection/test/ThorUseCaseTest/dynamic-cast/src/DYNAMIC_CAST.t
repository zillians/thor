/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test1_for_base<T>(from: T, origin: base): int32
{
    var i1: iface1  = cast<iface1 >(from);
    var i2: iface2  = cast<iface2 >(from);
    var i3: iface3  = cast<iface3 >(from);
    var b : base    = cast<base   >(from);
    var d : derived = cast<derived>(from);
    
    if(from != null)
    {
             if(i1 != origin) return - 1;
        else if(i2 != null  ) return - 2;
        else if(i3 != origin) return - 3;
        else if(b  != origin) return - 4;
        else if(d  != null  ) return - 5;
        else                  return   0;
    }
    else
    {
             if(i1 != null  ) return - 6;
        else if(i2 != null  ) return - 7;
        else if(i3 != null  ) return - 8;
        else if(b  != null  ) return - 9;
        else if(d  != null  ) return -10;
        else                  return   0;
    }
}

function test1_for_derived<T>(from: T, origin: derived): int32
{
    var i1: iface1  = cast<iface1 >(from);
    var i2: iface2  = cast<iface2 >(from);
    var i3: iface3  = cast<iface3 >(from);
    var b : base    = cast<base   >(from);
    var d : derived = cast<derived>(from);
    
    if(from != null)
    {
             if(i1 != origin) return -101;
        else if(i2 != origin) return -102;
        else if(i3 != origin) return -103;
        else if(b  != origin) return -104;
        else if(d  != origin) return -105;
        else                  return    0;
    }
    else
    {
             if(i1 != null  ) return -106;
        else if(i2 != null  ) return -107;
        else if(i3 != null  ) return -108;
        else if(b  != null  ) return -109;
        else if(d  != null  ) return -110;
        else                  return    0;
    }
}

@entry
task test1(): void
{
    var i1_null: iface1  = null;
    var i2_null: iface2  = null;
    var i3_null: iface3  = null;
    var  b_null: base    = null;
    var  d_null: derived = null;
    
    var  b_new : base    = new base();
    var  d_new : derived = new derived();
    
    var result : int32 = 0;
    
    result = test1_for_base   <iface1 >(i1_null,  b_null); if(result != 0) exit(result);
    result = test1_for_base   <iface1 >( b_new ,  b_new ); if(result != 0) exit(result);
    
    result = test1_for_base   <iface3 >(i3_null,  b_null); if(result != 0) exit(result);
    result = test1_for_base   <iface3 >( b_new ,  b_new ); if(result != 0) exit(result);
    
    result = test1_for_base   <base   >( b_null,  b_null); if(result != 0) exit(result);
    result = test1_for_base   <base   >( b_new ,  b_new ); if(result != 0) exit(result);
    
    result = test1_for_derived<iface1 >(i1_null,  d_null); if(result != 0) exit(result);
    result = test1_for_derived<iface1 >( d_new ,  d_new ); if(result != 0) exit(result);
    
    result = test1_for_derived<iface2 >(i2_null,  d_null); if(result != 0) exit(result);
    result = test1_for_derived<iface2 >( d_new ,  d_new ); if(result != 0) exit(result);
    
    result = test1_for_derived<iface3 >(i3_null,  d_null); if(result != 0) exit(result);
    result = test1_for_derived<iface3 >( d_new ,  d_new ); if(result != 0) exit(result);
    
    result = test1_for_derived<base   >( b_null,  d_null); if(result != 0) exit(result);
    result = test1_for_derived<base   >( d_new ,  d_new ); if(result != 0) exit(result);
    
    result = test1_for_derived<derived>( d_null,  d_null); if(result != 0) exit(result);
    result = test1_for_derived<derived>( d_new ,  d_new ); if(result != 0) exit(result);

    exit(0);
}

