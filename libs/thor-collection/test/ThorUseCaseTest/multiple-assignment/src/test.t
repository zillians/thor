/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function bar(x : int32) : int32
{
    if (x != 8) return -1;
    return 0;
}

class Foo
{
    public var a:int32;
    public var b:int32;
    public var c:int32;

    private static var d:int32 = 1;
    private static var e:int32 = 2;
    private static var f:int32 = 3;

    public function new():void
    {  
        a = b = c = 3;
    }

    public static function initDEF(): int32
    {  
        d = e = f = 7;
        if (d != 7 || e != 7 || f != 7) return -1;
        return 0;
    }

    public function check() : int32
    {
        a = 99;
        b = 8;
        if (bar(a = b) != 0) return -1;
        return 0;
    }
}


@entry
task test1():void
{
   var a:int32;
   var b:int32;
   var c:int32;

   a = b = c = 8;
   if (a != 8 || b != 8 || c != 8) exit(-1);

   if (Foo.initDEF() != 0) exit(-2);

   var foo:Foo = new Foo();
   if (foo.a != 3 || foo.b != 3 || foo.c != 3) exit(-3);

   if (foo.check() != 0) exit(-4);
   
   foo.a = 4;
   if (bar(foo.a = foo.b) != 0) exit(-5);

   exit(0);
}
