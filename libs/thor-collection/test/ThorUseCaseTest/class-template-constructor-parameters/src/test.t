/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class A<T>
{   
    public function new(t : T) : void {
		member = t;
    }
	public var member : T;
}

class B 
{
	public function new(i : C) : void
	{
		c = i;
	}
	public var value : int32 = 1222;
	public var c : C;
}

class C
{
	public var value : int64 = 999;
}

@entry
task main() : void
{
    var x : A<B> = new A<B>(new B(new C()));
	if (x.member.value != 1222) exit(-1);
	if (x.member.c.value != 999) exit(-2);
    exit(0);
}
