/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class parent                {}
class child  extends parent {}
class dummy                 {}

function func_return_void(): void {          }

function test_bool(): int32
{
    var b  :bool   = false; 

    if( isa<void               >(b  )) return -13;
    if(!isa<bool               >(b  )) return - 1;
    if( isa<int8               >(b  )) return - 2;
    if( isa<int16              >(b  )) return - 3;
    if( isa<int32              >(b  )) return - 4;
    if( isa<int64              >(b  )) return - 5;
    if( isa<float32            >(b  )) return - 6;
    if( isa<float64            >(b  )) return - 7;
    if( isa<function(    ):void>(b  )) return - 8;
    if( isa<function(int8):void>(b  )) return - 9;
    if( isa<parent             >(b  )) return -10;
    if( isa<child              >(b  )) return -11;
    if( isa<dummy              >(b  )) return -12;

    return 0;
}

function test_integer(): int32
{
    var i8 :int8    = 0;
    var i16:int16   = 0;
    var i32:int32   = 0;
    var i64:int64   = 0;

    if( isa<void               >(i8 )) return -113;
    if( isa<bool               >(i8 )) return -101;
    if(!isa<int8               >(i8 )) return -102;
    if( isa<int16              >(i8 )) return -103;
    if( isa<int32              >(i8 )) return -104;
    if( isa<int64              >(i8 )) return -105;
    if( isa<float32            >(i8 )) return -106;
    if( isa<float64            >(i8 )) return -107;
    if( isa<function(    ):void>(i8 )) return -108;
    if( isa<function(int8):void>(i8 )) return -109;
    if( isa<parent             >(i8 )) return -110;
    if( isa<child              >(i8 )) return -111;
    if( isa<dummy              >(i8 )) return -112;

    if( isa<void               >(i16)) return -213;
    if( isa<bool               >(i16)) return -201;
    if( isa<int8               >(i16)) return -202;
    if(!isa<int16              >(i16)) return -203;
    if( isa<int32              >(i16)) return -204;
    if( isa<int64              >(i16)) return -205;
    if( isa<float32            >(i16)) return -206;
    if( isa<float64            >(i16)) return -207;
    if( isa<function(    ):void>(i16)) return -208;
    if( isa<function(int8):void>(i16)) return -209;
    if( isa<parent             >(i16)) return -210;
    if( isa<child              >(i16)) return -211;
    if( isa<dummy              >(i16)) return -212;

    if( isa<void               >(i32)) return -313;
    if( isa<bool               >(i32)) return -301;
    if( isa<int8               >(i32)) return -302;
    if( isa<int16              >(i32)) return -303;
    if(!isa<int32              >(i32)) return -304;
    if( isa<int64              >(i32)) return -305;
    if( isa<float32            >(i32)) return -306;
    if( isa<float64            >(i32)) return -307;
    if( isa<function(    ):void>(i32)) return -308;
    if( isa<function(int8):void>(i32)) return -309;
    if( isa<parent             >(i32)) return -310;
    if( isa<child              >(i32)) return -311;
    if( isa<dummy              >(i32)) return -312;

    if( isa<void               >(i64)) return -413;
    if( isa<bool               >(i64)) return -401;
    if( isa<int8               >(i64)) return -402;
    if( isa<int16              >(i64)) return -403;
    if( isa<int32              >(i64)) return -404;
    if(!isa<int64              >(i64)) return -405;
    if( isa<float32            >(i64)) return -406;
    if( isa<float64            >(i64)) return -407;
    if( isa<function(    ):void>(i64)) return -408;
    if( isa<function(int8):void>(i64)) return -409;
    if( isa<parent             >(i64)) return -410;
    if( isa<child              >(i64)) return -411;
    if( isa<dummy              >(i64)) return -412;

    return 0;
}

function test_float(): int32
{
    var f32:float32 = 0;
    var f64:float64 = 0;

    if( isa<void               >(f32)) return -513;
    if( isa<bool               >(f32)) return -501;
    if( isa<int8               >(f32)) return -502;
    if( isa<int16              >(f32)) return -503;
    if( isa<int32              >(f32)) return -504;
    if( isa<int64              >(f32)) return -505;
    if(!isa<float32            >(f32)) return -506;
    if( isa<float64            >(f32)) return -507;
    if( isa<function(    ):void>(f32)) return -508;
    if( isa<function(int8):void>(f32)) return -509;
    if( isa<parent             >(f32)) return -510;
    if( isa<child              >(f32)) return -511;
    if( isa<dummy              >(f32)) return -512;

    if( isa<void               >(f64)) return -613;
    if( isa<bool               >(f64)) return -601;
    if( isa<int8               >(f64)) return -602;
    if( isa<int16              >(f64)) return -603;
    if( isa<int32              >(f64)) return -604;
    if( isa<int64              >(f64)) return -605;
    if( isa<float32            >(f64)) return -606;
    if(!isa<float64            >(f64)) return -607;
    if( isa<function(    ):void>(f64)) return -608;
    if( isa<function(int8):void>(f64)) return -609;
    if( isa<parent             >(f64)) return -610;
    if( isa<child              >(f64)) return -611;
    if( isa<dummy              >(f64)) return -612;

    return 0;
}

function test_class(): int32
{
    var null_parent:parent = null;
    var null_child :child  = null;
    var null_dummy :dummy  = null;

    var  new_parent:Object = new parent();
    var  new_child :Object = new child();
    var  new_dummy :Object = new dummy();

    if( isa<void               >(null_parent)) return - 713;
    if( isa<bool               >(null_parent)) return - 701;
    if( isa<int8               >(null_parent)) return - 702;
    if( isa<int16              >(null_parent)) return - 703;
    if( isa<int32              >(null_parent)) return - 704;
    if( isa<int64              >(null_parent)) return - 705;
    if( isa<float32            >(null_parent)) return - 706;
    if( isa<float64            >(null_parent)) return - 707;
    if( isa<function(    ):void>(null_parent)) return - 708;
    if( isa<function(int8):void>(null_parent)) return - 709;
    if( isa<parent             >(null_parent)) return - 710;
    if( isa<child              >(null_parent)) return - 711;
    if( isa<dummy              >(null_parent)) return - 712;

    if( isa<void               >( new_parent)) return - 813;
    if( isa<bool               >( new_parent)) return - 801;
    if( isa<int8               >( new_parent)) return - 802;
    if( isa<int16              >( new_parent)) return - 803;
    if( isa<int32              >( new_parent)) return - 804;
    if( isa<int64              >( new_parent)) return - 805;
    if( isa<float32            >( new_parent)) return - 806;
    if( isa<float64            >( new_parent)) return - 807;
    if( isa<function(    ):void>( new_parent)) return - 808;
    if( isa<function(int8):void>( new_parent)) return - 809;
    if(!isa<parent             >( new_parent)) return - 810;
    if( isa<child              >( new_parent)) return - 811;
    if( isa<dummy              >( new_parent)) return - 812;

    if( isa<void               >(null_child )) return - 913;
    if( isa<bool               >(null_child )) return - 901;
    if( isa<int8               >(null_child )) return - 902;
    if( isa<int16              >(null_child )) return - 903;
    if( isa<int32              >(null_child )) return - 904;
    if( isa<int64              >(null_child )) return - 905;
    if( isa<float32            >(null_child )) return - 906;
    if( isa<float64            >(null_child )) return - 907;
    if( isa<function(    ):void>(null_child )) return - 908;
    if( isa<function(int8):void>(null_child )) return - 909;
    if( isa<parent             >(null_child )) return - 910;
    if( isa<child              >(null_child )) return - 911;
    if( isa<dummy              >(null_child )) return - 912;

    if( isa<void               >( new_child )) return -1013;
    if( isa<bool               >( new_child )) return -1001;
    if( isa<int8               >( new_child )) return -1002;
    if( isa<int16              >( new_child )) return -1003;
    if( isa<int32              >( new_child )) return -1004;
    if( isa<int64              >( new_child )) return -1005;
    if( isa<float32            >( new_child )) return -1006;
    if( isa<float64            >( new_child )) return -1007;
    if( isa<function(    ):void>( new_child )) return -1008;
    if( isa<function(int8):void>( new_child )) return -1009;
    if(!isa<parent             >( new_child )) return -1010;
    if(!isa<child              >( new_child )) return -1011;
    if( isa<dummy              >( new_child )) return -1012;

    if( isa<void               >(null_dummy )) return -1113;
    if( isa<bool               >(null_dummy )) return -1101;
    if( isa<int8               >(null_dummy )) return -1102;
    if( isa<int16              >(null_dummy )) return -1103;
    if( isa<int32              >(null_dummy )) return -1104;
    if( isa<int64              >(null_dummy )) return -1105;
    if( isa<float32            >(null_dummy )) return -1106;
    if( isa<float64            >(null_dummy )) return -1107;
    if( isa<function(    ):void>(null_dummy )) return -1108;
    if( isa<function(int8):void>(null_dummy )) return -1109;
    if( isa<parent             >(null_dummy )) return -1110;
    if( isa<child              >(null_dummy )) return -1111;
    if( isa<dummy              >(null_dummy )) return -1112;

    if( isa<void               >( new_dummy )) return -1213;
    if( isa<bool               >( new_dummy )) return -1201;
    if( isa<int8               >( new_dummy )) return -1202;
    if( isa<int16              >( new_dummy )) return -1203;
    if( isa<int32              >( new_dummy )) return -1204;
    if( isa<int64              >( new_dummy )) return -1205;
    if( isa<float32            >( new_dummy )) return -1206;
    if( isa<float64            >( new_dummy )) return -1207;
    if( isa<function(    ):void>( new_dummy )) return -1208;
    if( isa<function(int8):void>( new_dummy )) return -1209;
    if( isa<parent             >( new_dummy )) return -1210;
    if( isa<child              >( new_dummy )) return -1211;
    if(!isa<dummy              >( new_dummy )) return -1212;

    return 0;
}

function test_call(): int32
{
    if(!isa<void               >(func_return_void())) return -1213;
    if( isa<bool               >(func_return_void())) return -1201;
    if( isa<int8               >(func_return_void())) return -1202;
    if( isa<int16              >(func_return_void())) return -1203;
    if( isa<int32              >(func_return_void())) return -1204;
    if( isa<int64              >(func_return_void())) return -1205;
    if( isa<float32            >(func_return_void())) return -1206;
    if( isa<float64            >(func_return_void())) return -1207;
    if( isa<function(    ):void>(func_return_void())) return -1208;
    if( isa<function(int8):void>(func_return_void())) return -1209;
    if( isa<parent             >(func_return_void())) return -1210;
    if( isa<child              >(func_return_void())) return -1211;
    if( isa<dummy              >(func_return_void())) return -1212;

    return 0;
}

//function test_function_type(): int32
//{
//    var null_f1: function(    ):void;
//    var null_f2: function(int8):void;
//    var      f1: function(    ):void = dummy_func_1;
//    var      f2: function(int8):void = dummy_func_2;;
//
//    if( isa<void               >(null_f1)) return -1313;
//    if( isa<bool               >(null_f1)) return -1301;
//    if( isa<int8               >(null_f1)) return -1302;
//    if( isa<int16              >(null_f1)) return -1303;
//    if( isa<int32              >(null_f1)) return -1304;
//    if( isa<int64              >(null_f1)) return -1305;
//    if( isa<float32            >(null_f1)) return -1306;
//    if( isa<float64            >(null_f1)) return -1307;
//    if( isa<function(    ):void>(null_f1)) return -1308;
//    if( isa<function(int8):void>(null_f1)) return -1309;
//    if( isa<parent             >(null_f1)) return -1310;
//    if( isa<child              >(null_f1)) return -1311;
//    if( isa<dummy              >(null_f1)) return -1312;
//
//    if( isa<void               >(     f1)) return -1413;
//    if( isa<bool               >(     f1)) return -1401;
//    if( isa<int8               >(     f1)) return -1402;
//    if( isa<int16              >(     f1)) return -1403;
//    if( isa<int32              >(     f1)) return -1404;
//    if( isa<int64              >(     f1)) return -1405;
//    if( isa<float32            >(     f1)) return -1406;
//    if( isa<float64            >(     f1)) return -1407;
//    if(!isa<function(    ):void>(     f1)) return -1408;
//    if( isa<function(int8):void>(     f1)) return -1409;
//    if( isa<parent             >(     f1)) return -1410;
//    if( isa<child              >(     f1)) return -1411;
//    if( isa<dummy              >(     f1)) return -1412;
//
//    if( isa<void               >(null_f2)) return -1513;
//    if( isa<bool               >(null_f2)) return -1501;
//    if( isa<int8               >(null_f2)) return -1502;
//    if( isa<int16              >(null_f2)) return -1503;
//    if( isa<int32              >(null_f2)) return -1504;
//    if( isa<int64              >(null_f2)) return -1505;
//    if( isa<float32            >(null_f2)) return -1506;
//    if( isa<float64            >(null_f2)) return -1507;
//    if( isa<function(    ):void>(null_f2)) return -1508;
//    if( isa<function(int8):void>(null_f2)) return -1509;
//    if( isa<parent             >(null_f2)) return -1510;
//    if( isa<child              >(null_f2)) return -1511;
//    if( isa<dummy              >(null_f2)) return -1512;
//
//    if( isa<void               >(     f2)) return -1613;
//    if( isa<bool               >(     f2)) return -1601;
//    if( isa<int8               >(     f2)) return -1602;
//    if( isa<int16              >(     f2)) return -1603;
//    if( isa<int32              >(     f2)) return -1604;
//    if( isa<int64              >(     f2)) return -1605;
//    if( isa<float32            >(     f2)) return -1606;
//    if( isa<float64            >(     f2)) return -1607;
//    if( isa<function(    ):void>(     f2)) return -1608;
//    if(!isa<function(int8):void>(     f2)) return -1609;
//    if( isa<parent             >(     f2)) return -1610;
//    if( isa<child              >(     f2)) return -1611;
//    if( isa<dummy              >(     f2)) return -1612;
//
//    return 0;
//}

@entry
task main(): void
{
    var result: int32;

    result = test_bool         (); if(result != 0) exit(result);
    result = test_integer      (); if(result != 0) exit(result);
    result = test_float        (); if(result != 0) exit(result);
    result = test_class        (); if(result != 0) exit(result);
//    result = test_function_type(); if(result != 0) exit(result);

    exit(0);
}

