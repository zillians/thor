/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef TEST_H
#define TEST_H

// unary function
template <typename Type>
void unary(Type a) { }

template <typename Type>
Type unary_ret(Type a) { }

// binary function
template <typename Type>
void binary_same(Type a, Type b) { }

template <typename Type>
Type binary_same_ret(Type a, Type b) { return a; }

template <typename First, typename Second>
void binary_diff(First a, Second b){ }

template <typename First, typename Second>
First binary_diff_ret1(First a, Second b){ return a; }

template <typename First, typename Second>
Second binary_diff_ret2(First a, Second b){ return b; }

#endif
