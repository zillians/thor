/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Base
{
    public virtual function foo() : int32
    {   
        return value;
    }   

    var value : int32 = 123;
}

class Derived extends Base 
{
    public virtual function foo() : int32
    {   
        return super.foo() + 1;
    }   
}

interface MyInt
{
    public function bar() : int32;
}

class Derived2 extends Derived implements MyInt
{
    public virtual function foo() : int32 
    {
        return super.foo() + 1;
    }
    
    public virtual function bar() : int32
    {
        return dd;
    }
    
    var dd : int32 = 99;
}

class Derived3 extends Derived2
{
    public virtual function foo() : int32
    {
        return super.foo() + 1;
    }

    public virtual function bar() : int32
    {
        return super.bar() + 1;
    }
}

class AnotherBase
{
    public virtual function getObject(): Base
    {
        return new Derived2();
    }
}

class AnotherDerived extends AnotherBase
{
    public virtual function foo() : int32
    {
        return super.getObject().foo() + 100;
    }

    public virtual function getObject(): Derived2
    {
        return new Derived3();
    }
}

@entry
task test1() : void
{
    var derived = new Derived;
    if (derived.foo() != 124) exit(-1);
    exit(0);
}

@entry
task test2() : void
{
    var d = new Derived2;
    if (d.foo() != 125) exit(-1);
    exit(0);
}

@entry
task test3() : void
{
    var d = new Derived3;
    if (d.foo() != 126) exit(-1);
    if (d.bar() != 100) exit(-2);
    exit(0);
}

@entry
task test4() : void
{
    var d = new AnotherDerived();
    if (d.foo() != 225) exit(-1);
    exit(0);
}
