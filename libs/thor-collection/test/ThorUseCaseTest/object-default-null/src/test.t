/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Dummy 
{
	public var member : Dummy;
	public static var static_member : Dummy;
}

var global : Dummy;

@entry
task test1() : void
{
	var local : Dummy;
	static var static_local : Dummy;

	// check local
	if (local != null) exit(-1);
	if (static_local != null) exit(-2);
	
	// check static member
	if (Dummy.static_member != null) exit(-3);

	// check member
	if ((new Dummy()).member != null) exit(-4);

	exit(0);
}
