/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class B
{
	public function new<T>(v : T) : void
	{
		value1 = v;
	}

	public var value1 : int32 = 100;
	public var value2 : int32 = 99;
}

class A extends B
{
	public function new() : void
	{
		super(1000);
	}
}

@entry
task test1() : void
{
	var b = new B(1999);
	var a = new A();

	if (b.value1 != 1999) exit(-1);
	if (b.value2 != 99) exit(-2);

	if (a.value1 != 1000) exit(-3);
	if (a.value2 != 99) exit(-4);

	exit(0);
}
