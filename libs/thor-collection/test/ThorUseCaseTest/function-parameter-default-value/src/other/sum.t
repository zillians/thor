/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= pl;

function do_get_value<T>(t: T): T {
    return t;
}

function do_get_value<T>(t: foo<T>): T {
    return t.get_value();
}

function do_sum_value<T, U, V>(t: T, u: U, v: V): int64 {
    return do_get_value(t) + do_get_value(u) + do_get_value(v);
}

function sum_simple(i32: int32 = 1234, i64: int64 = 4321, i16: int16 = 1111): int64 {
    return do_sum_value(i32, i64, i16);
}

function sum_class(
    f32: pl.foo<int32> = new foo<int32>(1234),
    f64: pl.foo<int64> = new foo<int64>(4321),
    f16: pl.foo<int16> = new foo<int16>(1111)
): int64 {
    return do_sum_value(f32, f64, f16);
}

function sum_nested(
    i32: int32 = (new foo<int32>(1234)).get_value(),
    i64: int64 = (new foo<int64>(4321)).get_value(),
    i16: int16 = (new foo<int16>(1111)).get_value()
): int64 {
    return do_sum_value(i32, i64, i16);
}

function sum_template_1<T, U, V>(
    v32: T = new foo<int32>(1234),
    v64: U = new foo<int64>(4321),
    v16: V = new foo<int16>(1111)
): int64 {
    return do_sum_value(v32, v64, v16);
}

function sum_template_2<T, U, V>(
    v32: T = 1234,
    v64: U = 4321,
    v16: V = 1111
): int64 {
    return do_sum_value(v32, v64, v16);
}
