/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class dummy_1 {
	public virtual function get_value(): int32 {
		return 1234;
	}
}

class dummy_2 extends dummy_1 {
	public virtual function get_value(): int32 {
		return 4444;
	}
}

class cls_1 {
	public static var static_value_1_1: int32   = 4321;
	public static var static_value_1_2: dummy_1 = new dummy_1();
	public static var static_value_1_3: dummy_1 = new dummy_2();

	public static const static_value_2_1: int32   = 4321;
	public static const static_value_2_2: dummy_1 = new dummy_1();
	public static const static_value_2_3: dummy_1 = new dummy_2();
}

class cls_2<T> {
	public static var static_value_1_1: int32   = 4321;
	public static var static_value_1_2: dummy_1 = new dummy_1();
	public static var static_value_1_3: dummy_1 = new T();

	public static const static_value_2_1: int32   = 4321;
	public static const static_value_2_2: dummy_1 = new dummy_1();
	public static const static_value_2_3: dummy_1 = new T();
}

@entry
task main_test(): void {
	if( cls_1.static_value_1_1             != 4321) exit(-1);
	if( cls_1.static_value_1_2.get_value() != 1234) exit(-2);
	if( cls_1.static_value_1_3.get_value() != 4444) exit(-3);

	if( cls_1.static_value_2_1             != 4321) exit(-4);
	if( cls_1.static_value_2_2.get_value() != 1234) exit(-5);
	if( cls_1.static_value_2_3.get_value() != 4444) exit(-6);

	if( cls_2<dummy_1>.static_value_1_1             != 4321) exit(-7);
	if( cls_2<dummy_1>.static_value_1_2.get_value() != 1234) exit(-8);
	if( cls_2<dummy_1>.static_value_1_3.get_value() != 1234) exit(-9);

	if( cls_2<dummy_1>.static_value_2_1             != 4321) exit(-10);
	if( cls_2<dummy_1>.static_value_2_2.get_value() != 1234) exit(-11);
	if( cls_2<dummy_1>.static_value_2_3.get_value() != 1234) exit(-12);

	if( cls_2<dummy_2>.static_value_1_1             != 4321) exit(-13);
	if( cls_2<dummy_2>.static_value_1_2.get_value() != 1234) exit(-14);
	if( cls_2<dummy_2>.static_value_1_3.get_value() != 4444) exit(-15);

	if( cls_2<dummy_2>.static_value_2_1             != 4321) exit(-16);
	if( cls_2<dummy_2>.static_value_2_2.get_value() != 1234) exit(-17);
	if( cls_2<dummy_2>.static_value_2_3.get_value() != 4444) exit(-18);

	exit(0);
}

