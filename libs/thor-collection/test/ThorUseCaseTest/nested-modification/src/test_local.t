/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_local(cond: bool): int32
{
    var local_value: int32 = 1234;
    var local_dummy: int32 = 4321;

    (local_value = 4321) = 9999;

    if (local_value != 9999)
        return 1;

    if (((local_value -= 3) += 3) != 9999)
        return 2;

    if (((local_value *= 2) /= 2) != 9999)
        return 3;

    if (--++local_value != 9999)
        return 4;

    if (++--local_value != 9999)
        return 5;

    // 'cond' should be false under this test => writing 1111 to 'local_dummy'
    if (((cond ? local_value : local_dummy) = 1111) != 1111)
        return 6;

    if (local_value != 9999)
        return 7;

    return 0;
}
