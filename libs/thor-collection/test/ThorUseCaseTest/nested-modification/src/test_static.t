/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class static_dummy
{
    public static var value: int32 = 1234;
    public static var dummy: int32 = 4321;
}

function test_static(cond: bool): int32
{
    (static_dummy.value = 4321) = 9999;

    if (static_dummy.value != 9999)
        return 1;

    if (((static_dummy.value -= 3) += 3) != 9999)
        return 2;

    if (((static_dummy.value *= 2) /= 2) != 9999)
        return 3;

    if (--++static_dummy.value != 9999)
        return 4;

    if (++--static_dummy.value != 9999)
        return 5;

    // 'cond' should be false under this test => writing 1111 to 'static_dummy.dummy'
    if (((cond ? static_dummy.value : static_dummy.dummy) = 1111) != 1111)
        return 6;

    if (static_dummy.value != 9999)
        return 7;

    return 0;
}
