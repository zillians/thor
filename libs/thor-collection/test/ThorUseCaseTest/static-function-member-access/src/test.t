/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class A
{
	public static var value : int32 = 10;

	public static function foo() : void
	{
		value = 1222;
	}
}


@entry
task test1() : void
{
	if (A.value != 10) exit(-1);
	A.foo();
	if (A.value != 1222) exit(-2);

	exit(0);
}


class A2
{
    public function hello() : void
    {
        B2.static_value = 333;
    }
}

class B2
{
    public static var static_value : int32 = 122;
}

@entry
task test2() : void
{
    if (B2.static_value != 122) exit(-1);
    var a : A2 = new A2();
    a.hello();
    if (B2.static_value != 333) exit(-2);
    exit(0);
}

