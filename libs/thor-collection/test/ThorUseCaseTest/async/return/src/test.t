/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class holder<T>
{
    public static function reset(): void
    {
         invalid_ret = cast<T>(0x7F7F7F7F);
                 ret = cast<T>(0x7F7F7F7F);
        expected_ret = 0;
    }

    public static var  invalid_ret: T;
    public static var          ret: T;
    public static var expected_ret: T;
}

class holder<T: Object>
{
    public static function reset(): void
    {
         invalid_ret = new Object();
                 ret = invalid_ret;
        expected_ret = new Object();
    }

    public static var  invalid_ret: T;
    public static var          ret: T;
    public static var expected_ret: T;
}

task wait_done<T>(): void
{
    if (holder<T>.ret == holder<T>.invalid_ret)
        async -> wait_done<T>();
    else if (holder<T>.ret == holder<T>.expected_ret)
        exit(0);
    else
        exit(-1);
}

function sum<T        >(          ): T { return 0    ; }
function sum<T: Object>(          ): T { return null ; }
function sum<T        >(a: T      ): T { return a    ; }
function sum<T        >(a: T, b: T): T { return a + b; }

function do_test<T>(          ): void { holder<T>.reset(); holder<T>.expected_ret = sum<T>(    ); async -> holder<T>.ret = sum<T>(    ); async -> wait_done<T>(); }
function do_test<T>(a: T      ): void { holder<T>.reset(); holder<T>.expected_ret = sum<T>(a   ); async -> holder<T>.ret = sum<T>(a   ); async -> wait_done<T>(); }
function do_test<T>(a: T, b: T): void { holder<T>.reset(); holder<T>.expected_ret = sum<T>(a, b); async -> holder<T>.ret = sum<T>(a, b); async -> wait_done<T>(); }

@entry task test_i08_0() { do_test<int8 >(    ); }
@entry task test_i08_1() { do_test<int8 >(1   ); }
@entry task test_i08_2() { do_test<int8 >(1, 2); }

@entry task test_i16_0() { do_test<int16>(          ); }
@entry task test_i16_1() { do_test<int16>(0xF7F7    ); }
@entry task test_i16_2() { do_test<int16>(0xF7F7, -2); }

@entry task test_i32_0() { do_test<int32>(          ); }
@entry task test_i32_1() { do_test<int32>(1234      ); }
@entry task test_i32_2() { do_test<int32>(1234, 4321); }

@entry task test_i64_0() { do_test<int64>(          ); }
@entry task test_i64_1() { do_test<int64>(7777      ); }
@entry task test_i64_2() { do_test<int64>(7777, 8765); }

@entry task test_f32_0() { do_test<float32>(        ); }
@entry task test_f32_1() { do_test<float32>(1.0     ); }
@entry task test_f32_2() { do_test<float32>(1.0, 4.5); }

@entry task test_f64_0() { do_test<float64>(               ); }
@entry task test_f64_1() { do_test<float64>(71234.7        ); }
@entry task test_f64_2() { do_test<float64>( 1234.8, 4442.8); }

@entry task test_obj_0() { do_test<Object>(            ); }
@entry task test_obj_1() { do_test<Object>(null        ); }
@entry task test_obj_2() { do_test<Object>(new Object()); }

@entry
task test_nested_lhs()
{
    holder<int32>.reset();

    const expected_ret: int32 = 1234;
    const  invalid_ret: int32 = 4321;

              holder<int32>.expected_ret                = sum(expected_ret);
    async -> (holder<int32>.         ret = invalid_ret) = sum(expected_ret);

    if (holder<int32>.ret != invalid_ret)
        exit(-2);
    else
        async -> wait_done<int32>();
}
