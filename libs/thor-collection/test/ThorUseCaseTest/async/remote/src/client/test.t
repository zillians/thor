/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.util;
import common;
import server;

var is_done: bool = false;

function to_value<T>(t: T): int64
{
    return t;
}

function to_value<T: common.iface>(t: T): int64
{
    return to_value(cast<common.foo>(t));
}

function to_value<T: common.foo>(t: T): int64
{
    const b = cast<common.bar>(t);

    if (b == null)
        return t.i64;

    return b.i64 + to_value(b.f);
}

function to_value<T: common.bar>(t: T): int64
{
    return to_value<common.foo>(t);
}

function to_value<T: String>(t: T): int64
{
    return thor.util.Convert.toInt64(t);
}

function value_info<T>(t: T): String
{
    return "\{t}";
}

function value_info<T: common.iface>(t: T): String
{
    return value_info(cast<common.foo>(t));
}

function value_info<T: common.foo>(t: T): String
{
    return t.dump_info();
}

function value_info<T: common.bar>(t: T): String
{
    return t.dump_info();
}

function value_info<T: String>(t: T): String
{
    return t;
}

function accepter<T, U>(t: T, u: U): void
{
    const t_info = value_info(t);
    const u_info = value_info(u);

    print("client.accepter, t_info = \{t_info}, u_info = \{u_info}\n");

    const t_value = to_value(t);
    const u_value = to_value(u);

    print("client.accepter, t_value = \{t_value}, u_value = \{u_value}\n");

    const count: int64 = t_value + u_value;

    if (count != 5555)
        exit(-1);

    async[Domain.caller()] -> server.accepter(count);
}

function test_impl_waiter(): void
{
    if (is_done)
        exit(0);
    else
        async -> test_impl_waiter();
}

function test_impl(endpoint: String): void
{
    Domain.connect(
        endpoint,
        null,
        null,
        lambda(id: thor.util.UUID, e: DomainError): void
        {
            exit(-101);
        }
    );

    async -> test_impl_waiter();
}

@entry task test_i32_i32  (): void { return test_impl("tcp://*:9888"); }
@entry task test_i16_i64  (): void { return test_impl("tcp://*:9889"); }
@entry task test_i64_foo  (): void { return test_impl("tcp://*:9890"); }
@entry task test_foo_bar  (): void { return test_impl("tcp://*:9891"); }
@entry task test_i32_iface(): void { return test_impl("tcp://*:9892"); }
@entry task test_i32_str  (): void { return test_impl("tcp://*:9893"); }
