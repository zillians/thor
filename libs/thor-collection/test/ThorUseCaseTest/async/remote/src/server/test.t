/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.util;
import client;
import common;

var client_instance: Domain = null;

function accepter(count: int64): void
{
    print("server.accepter, count = \{count}\n");

    async[Domain.caller()] -> { client.is_done = true; }

    if (count != 5555)
        exit(-4);
}

function from_value<T>(value: int64): T
{
    const t: T = value;

    print("server.from_value<T>, value = \{value}, t = \{t}\n");

    return t;
}

function from_value<T: common.iface>(value: int64): T
{
    const t   : T = new common.bar(value);
    const f       = cast<common.foo>(t);
    const info    = f.dump_info();

    print("server.from_value<common.foo>, value = \{value} => t = \{info}\n");

    return t;
}

function from_value<T: common.foo>(value: int64): T
{
    const t   : T = new T(value);
    const info    = t.dump_info();

    print("server.from_value<common.foo>, value = \{value} => t = \{info}\n");

    return t;
}

function from_value<T: common.bar>(value: int64): T
{
    const t   : T = new T(value);
    const info    = t.dump_info();

    print("server.from_value<common.bar>, value = \{value} => t = \{info}\n");

    return t;
}

function from_value<T: String>(value: int64): T
{
    return thor.util.Convert.toString(value);
}

function client_connected<T, U>(target: Domain): void
{
    print("client connected\n");

    if (client_instance != null)
        exit(-1);

    client_instance = target;

    async[client_instance] -> client.accepter(
        from_value<T>(1234),
        from_value<U>(4321)
    );
}

function client_disconnected(target: Domain): void
{
    print("client disconnected\n");

    if (client_instance == null  ) exit(-2);
    if (client_instance != target) exit(-3);
    else                           exit( 0);
}
