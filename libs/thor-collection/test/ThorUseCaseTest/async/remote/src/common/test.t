/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface iface {}

@replicable
class foo implements iface
{
    public function new(): void
    {
        i64 = 0;
    }

    public function new(new_i64: int64): void
    {
        i64 = new_i64;
    }

    public function dump_info(): String
    {
        const b = cast<bar>(this);

        if (b == null)
            return "foo{" + i64 + "}";
        else
            return b.dump_info();
    }

    public var i64: int64;
}

@replicable
class bar extends foo
{
    public function new(): void
    {
        super();

        f = null;
    }

    public function new(new_i64: int64): void
    {
        super(
            (new_i64 % 2 == 0) ? (new_i64 / 2)
                               : (new_i64 / 2) + 1
        );

        f = new foo(new_i64 / 2);
    }

    public function dump_info(): String
    {
        return "bar{" + i64 + ", " + (f == null ? "<null>" : f.dump_info()) + "}";
    }

    public var f: foo;
}
