#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

thor_add_usecase_test(
    NAME  simple-1
    ENTRY test1
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-2
    ENTRY test2
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-3
    ENTRY test3
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-4
    ENTRY test4
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-5
    ENTRY test5
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-6
    ENTRY test6
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-7
    ENTRY test7
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-8
    ENTRY test8
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-9
    ENTRY test9
    USE_MONITOR
)

thor_add_usecase_test(
    NAME  simple-10
    ENTRY test10
    USE_MONITOR
)
