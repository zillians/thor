/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var gExitCode : int32 = -123;

function do_waiting(): void
{
    if (gExitCode == -123)
        async -> do_waiting();
    else
        exit(gExitCode);
}

function start_waiting(): void
{
    async -> do_waiting();
}

class Dummy
{
    public function new(v : int32) { value = v; }
    public var value : int32;
}

function async1() : void
{
    gExitCode = 0;
}

@entry
task test1() : void
{
    async -> async1();

    return start_waiting();
}

function async2(v : int32) : void
{
    if (v == 10)
        gExitCode = 0;
    else
        gExitCode = 1;
}

@entry
task test2() : void
{
    async -> async2(10);

    return start_waiting();
}

function async3(d: Dummy): void
{
    if (d.value == 100)
        gExitCode = 0;
    else
        gExitCode = 1;
}

@entry
task test3() : void
{
    async -> async3(new Dummy(100));

    return start_waiting();
}

function async4(v: int8, d1: Dummy, w: int16, z: int64, d2: Dummy): void
{
    if (v == 10 && d1.value == 10000 && w == 32760 && z == 1152921504606846976l && d2.value == 88)
        gExitCode = 0;
    else
        gExitCode = 1;
}

@entry
task test4() : void
{
    var obj = new Dummy(88);

    async -> async4(10, new Dummy(10000), 32760, 1152921504606846976l, obj);

    return start_waiting();
}

class Dummy2
{
    public function foo() : void 
    {
        if (value == 1024)
            gExitCode = 0;
        else
            gExitCode = 1;
    }
    public var value : int32 = 1024;
}

@entry
task test5() : void
{
    var c = new Dummy2();

    async -> c.foo();
    
    return start_waiting();
}

interface Foo
{
    public function foo() : void;
}

class Base
{
    public var value : int32 = 888;
}

class Derived extends Base implements Foo
{
    public virtual function foo() : void
    {
        if (value == 888)
            gExitCode = 0;
        else
            gExitCode = 1;
    }
}

@entry
task test6() : void
{
    var f : Foo = new Derived();

    async -> f.foo();

    return start_waiting();
}

class MySelf
{
    public function clone() : MySelf
    {
        return new MySelf();
    }

    public virtual function foo() : void
    {
        if (value == 888)
            gExitCode = 0;
        else
            gExitCode = 1;
    }

    public var value : int32 = 888;
}

@entry
task test7() : void
{
    var m = new MySelf();

    async -> m.clone().clone().clone().foo();

    return start_waiting();
}

@entry
task test8() : void
{
    var f = lambda(v : int32) : void 
    {
        if (v == 9876)
            gExitCode = 0;
        else
            gExitCode = 1;
    };

    async -> f(9876);

    return start_waiting();
}

class Special
{
    public function foo(v : Special, m : int32, w : int32): void
    {
        if (value == v.value && value == 999 && m == 1024 && w == 4201)
            gExitCode = 0;
        else
            gExitCode = 1;
    }

    public function foo2() : void
    {
        // well, you may ask why I don't just call foo(this, 1024, 4201),
        // because this is a bug of lambda (hard to fix)
        var myself = this;

        async -> foo(myself, 1024, 4201);
    }

    public var value : int32 = 999;
}

@entry
task test9() : void
{
    var s = new Special();
    var x = 1024;

    async -> s.foo(s, x, 4201);

    return start_waiting();
}

@entry
task test10() : void
{
    var s = new Special();

    async -> s.foo2();
    
    return start_waiting();
}
