/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class foo
{
    public function new(the_expected: int32): void
    {
        value    = 0;
        expected = the_expected;
    }

    public function get_expected(): int32
    {
        return expected;
    }

    public task task_func()
    {
        flow -> {
            value = get_expected();
        }

        print("value    = \{value   }\n");
        print("expected = \{expected}\n");

        if (value == expected)
            exit(0);
        else
            exit(1);
    }

    private var value   : int32;
    private var expected: int32;
}

class bar
{
    public function new(the_expected: int32): void
    {
        value    = 0;
        expected = the_expected;

        print("value    = \{value   }\n");
        print("expected = \{expected}\n");
    }

    public function get_expected(): int32
    {
        return expected;
    }

    public static task task_func(b: bar)
    {
        flow -> {
            b.value = b.get_expected();
        }

        print("b.value    = \{b.value   }\n");
        print("b.expected = \{b.expected}\n");

        if (b.value == b.expected)
            exit(0);
        else
            exit(1);
    }

    private var value   : int32;
    private var expected: int32;
}

@entry
task test_main_1()
{
    const f = new foo(1234);

    async -> f.task_func();
}

@entry
task test_main_2()
{
    const b = new bar(1234);

    async -> bar.task_func(b);
}
