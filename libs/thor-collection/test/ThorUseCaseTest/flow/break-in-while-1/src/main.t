/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function add_one<T>(value: T): T
{
    return value + 1;
}

function sub_one<T>(value: T): T
{
    return value - 1;
}

@entry
task test_main()
{
    var   v1          = 10;
    const v1_expected =  0;
    var   v2          =  0;
    const v2_expected = 10;

    while (true)
    {
        if (v1 == v1_expected && v2 == v2_expected)
            break;

        if (v1 == v1_expected || v2 == v2_expected)
            break;

        flow -> {
            v1 = sub_one(v1);
            v2 = add_one(v2);
        }
    }

    print("v1 = \{v1}, expected = \{v1_expected}\n");
    print("v2 = \{v2}, expected = \{v2_expected}\n");

    if (v1 == v1_expected && v2 == v2_expected)
        exit(0);
    else
        exit(1);
}
