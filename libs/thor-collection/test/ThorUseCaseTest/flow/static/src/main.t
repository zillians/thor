/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var value   : int32 = 0;
var expected: int32 = 0;

function init(the_expected: int32): void
{
    value    = 0;
    expected = the_expected;
}

function get_expected(): int32
{
    return expected;
}

task task_func()
{
    flow -> {
        value = get_expected();
    }

    print("value    = \{value   }\n");
    print("expected = \{expected}\n");

    if (value == expected)
        exit(0);
    else
        exit(1);
}

@entry
task test_main()
{
    init(1234);

    async -> task_func();
}
