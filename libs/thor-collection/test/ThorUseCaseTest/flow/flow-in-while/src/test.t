/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// yentien (2014/01/17)
// This test exists because on the time I written this, thor-compile suffered a "Internal program error" compiling this. 

var x : int32 = 0;

task foo() : void
{
    x = 1;
}

@entry
task test()
{
    // var x : int32 = 3; //this makes it successfully compile
    while(x != 1)
    {
        x = 2; //make sure if the side effect of foo() didn't take into place, the while loop can be break out.
        flow -> {
            foo();
        }
    }

    if(x == 1)
        exit(0);
    else
        exit(-1);
}
