/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
const v1_expected: int32 = 1234;
const v2_expected: int32 = 4321;
const v3_expected: int32 = 4444;

function get_value<T>(value: T): T
{
    return value;
}

task task_func(max: int32)
{
    var is_entered   = false;
    var v1: int32    = 0;
    var v2: int32    = 0;
    var v3: int32    = 0;
    var count: int32 = 0;

    do {
        if (++count != max)
            continue;

        flow -> {
            v1 = get_value(v1_expected);
            v2 = get_value(v2_expected);
        }

        v3 = v3_expected;
    } while (false);

    if (count != max)
        exit(1);
    else if (v1 == v1_expected && v2 == v2_expected && v3 == v3_expected)
        exit(0);
    else
        exit(2);
}

@entry
task test_main() {
    async -> task_func(10);
}
