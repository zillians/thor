/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var produced_value: int32 = 0;
var is_produced   : bool  = false;

task produce(value: int32)
{
    produced_value = value;
    is_produced    = true;
}

task wait(value: int32)
{
    if (is_produced)
        exit(produced_value == value ? 0 : 1);
    else
        async -> wait(value);
}

task task_func(value: int32, non_used: int32)
{
    flow -> {
        produce(value);
    }
}

@entry
task test_main() {
    const value = 1234;

    async -> wait(1234);
    async -> task_func(1234, 5678);
}
