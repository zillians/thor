/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class Dummy
{
	public function new() : void
	{
		var f = lambda() : void
		{
			member1 = 9;
			var g = lambda() : void
			{
                var h = lambda() : void 
                {
                    var hh = lambda() : void {
                        member1 = 10;
                        member2 = 11;
                    };
                    hh();
                };
                h();
			};

			g();
		};

		f();
	}
    
    public function get_member1() { return member1; }
    public function get_member2() { return member2; }

	private var member1 : int32 = 8;
	protected var member2 : int32 = 8;
}


@entry
task test1() : void
{
	var first_level = 10;
	var f = lambda(x: int32) : int32
		{
			var second_level = 8;

			var g = lambda(y: int32) : int32
				{
                    var h = lambda() : int32
                    {
                        var hh = lambda() : int32
                        {
        					return first_level + second_level + y;
                        };
                        return hh();
                    };
                    return h();
				};

			return x + g(8);
		};
	
	if (f(9) != 35) exit(-1);

	exit(0);
}

@entry
task test2() : void
{
	var d = new Dummy();

	if (d.get_member1() != 10) exit(-1);
	if (d.get_member2() != 11) exit(-2);

	exit(0);
}
