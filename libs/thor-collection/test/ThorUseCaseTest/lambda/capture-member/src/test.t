/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var global_variable : int32 = 12;

function say_hello() : int32
{
	return 100;
}

class Puppy 
{
	public static function small() : int32 { return 9; }
	public static var value : int32 = 111;
}

class Dummy
{
	private function foo() : void
	{
		++member;
	}

	public static function dummy_static() : int32 { return 8; }

	public function new(): void
	{
		defer_function = lambda(x : int32) : int32
			{
				var local = 10;

				if (global_variable != 12) return -1;
				if (Dummy.dummy_static() != 8) return -2;
				if (Puppy.small() != 9) return -3;
				if (Puppy.value != 111) return -4;
				if (say_hello() != 100) return -5;

				foo();
				member += 10;

				return x + member + local;
			};
	}

    public function get_member() { return member; }

	var member : int32 = 10;
	public var defer_function : lambda(int32) : int32;
}

@entry
task test1() : void
{
	var d = new Dummy();

	if (d.get_member() != 10) exit(-1);
	if (d.defer_function(8) != 39) exit(-2);
	if (d.get_member() != 21) exit(-3);

	exit(0);
}
