/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function wrapper(f : lambda(int32, int32) : int32) : int32
{
	return f(10, 20);
}

@entry
task test1() : void
{
	var captured = 8;
	var f : lambda(int32, int32) : int32 = lambda(x: int32, y: int32) : int32
		{
			return x + y + captured;
		};

	if (wrapper(f) != 38) exit(-1);

	exit(0);
}

function get_lambda(input_value : int32) : lambda(int32) : int32
{
	return lambda(v: int32) : int32 {	return v + input_value; };
}

@entry
task test2() : void
{
	var f = get_lambda(12);

	if (f(8) != 20) exit(-1);
	exit(0);
}
