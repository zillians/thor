/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class A
{
    public function foo() : int32
    {
        var f = lambda() : int32 {
            if (this.foo2(this) != 9999) return 1;
            if (foo2(this) != 9999) return 2;
            if (this.bar != 1234) return 3;

            return 0;
        };

        return f();
    }

    public function cool() : int32
    {
        var f = lambda() : int32 {

            var g = lambda() : int32 {
                if (this.foo2(this) != 9999) return 1;
                if (foo2(this) != 9999) return 2;
                if (this.bar != 1234) return 3;

                return 0;
            };

            if (g() == 0 && this.bar == 1234) return 0;

            return g();
        };

        return f();
    }

    private function foo2(a : A) : int32
    {
        return a.beer;
    }

    private var bar : int32 = 1234;
    public var beer : int32 = 9999;
}

class B
{
    public function foo() : int32 
    {
        var f = lambda() : int32
        {
            return this.getObj().foo();
        };

        return f();
    }

    private function getObj() : A
    {
        return obj;
    }

    var obj : A = new A();
}

@entry
task test1() : void
{
    var a = new A();

    var result1 = a.foo();
    var result2 = a.cool();
    if (result1 != 0) exit(result1);
    if (result2 != 0) exit(result2);

    exit(0);
}

@entry
task test2() : void
{
    var b = new B();
    exit(b.foo());
}
