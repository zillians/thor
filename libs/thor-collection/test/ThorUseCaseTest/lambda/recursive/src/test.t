/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.lang;

@entry
task test1() : void
{
	// well, just test capture
	var zero = 0;
	var one = 1;

	var fib = lambda(x : int32) : int32 {
		if (x == 0) return zero;
		if (x == 1) return one;
		if (x == 2) return one;

		return fib(x-1) + fib(x-2);
	};

	if (fib(10) != 55) exit(-1);
	if (fib(5) != 5) exit(-2);

	exit(0);
}

class Dummy
{
    public function foo() : void
    {
        var count = lambda(x : int32) : void
        {
            if (x == 0 ) return;
            value = value + 1;
            count(x-1);
        };

        count(10);
    }

    public function get_value() { return value; }

    private var value : int32 = 10;
}

@entry
task test2() : void
{
    var d = new Dummy();
    d.foo();
    if (d.get_value() != 20) exit(-1);
    exit(0);
}

class Dummy2
{
    public function new() : void
    {
        funny = lambda(x: int32) : int32
        {
            if (x == 0) return x;
            return funny(x-1) + x;
        };

        damm = lambda(x: int32) : String
        {
            return "\{x}";
        };
    }

    public var funny : lambda(int32) : int32;
    public var damm  : lambda(int32) : String;
}

@entry
task test3() : void
{
    var d = new Dummy2();

    if ( d.funny(4) != 10       ) exit(-1);
    if (!d.damm (4).isEqual("4")) exit(-2);

    exit(0);
}
