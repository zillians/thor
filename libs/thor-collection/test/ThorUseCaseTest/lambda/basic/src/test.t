/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var global_value : int32 = 10;

class Dummy
{
	public static var member : int32 = 19;
	public static function getV() : int32 { return 7; }

	public var normal_member : int32 = 8;
}

function inc(x : int32) : int32
{
	return x + 1;
}

@entry
task test1() : void
{
	static var static_local = 8;
	var capture_me = 3;

	var dummy = new Dummy();

	var f : lambda(int32): int32 = lambda(x: int32) : int32
		{
			var local = 99;

			if (static_local != 8) return -1;
			if (Dummy.member != 19) return -2;
			if (global_value != 10) return -3;
			if (Dummy.getV() != 7) return -4;
			if (local != 99) return -4;
			if (dummy.normal_member != 8) return -5;

			static_local = 1;
			Dummy.member = 2;
			global_value = 3;
			dummy.normal_member = 4;

			return inc(x + capture_me + local);
		};

	if (f(5) != 108) exit(-1);

	if (static_local != 8) exit(-2);	// static local will not be modified
	if (Dummy.member != 2) exit(-3);
	if (global_value != 3) exit(-4);
	if (dummy.normal_member != 4) exit(-5);

	exit(0);
}

@entry
task test2() : void
{
	var d = new Dummy();

	// test pass class type parameter
	var f1 = lambda(dd : Dummy) : int32
	{
		if (dd.normal_member != 8) return -1;
		dd.normal_member = 100;
		return 0;
	};

	if (f1(d) != 0 || d.normal_member != 100) exit(-1);

	// test return class type 
	var f2 = lambda() : Dummy
	{
		return new Dummy();
	};

	var out = f2();
	if (out.normal_member != 8) exit(-2);

	exit(0);
}
