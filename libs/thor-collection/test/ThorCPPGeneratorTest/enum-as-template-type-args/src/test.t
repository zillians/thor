/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import thor.container;

// user defined enums
// underlying type is int32
enum Sex {
    Boy, Girl
}
// underlying type is int64
enum LongSex {
    Boy = 0L, Girl, Inter
}

@entry
task test(): void
{
    // instantiate C++ class template with user defined enums
    var user = new thor.container.Vector<Sex>;
    user.pushBack( Sex.Boy );

    //var user2 = new user_vector2;
    var user2 = new thor.container.Vector<LongSex>;
    user2.pushBack( LongSex.Boy );

    exit(0);
}
