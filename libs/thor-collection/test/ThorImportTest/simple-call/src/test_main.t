/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import import_test;
import import_test.ns_1;
import import_test.ns_2;

function test_call_for_primitive(): int32 {
    var i8  : int8  = 0x1;
    var i16 : int16 = 0x2;
    var i32 : int32 = 0x4;
    var i64 : int64 = 0x8;

    if(!isa<int8 >(import_test.add(i8 , i8 )) || i8  + i8  != import_test.add(i8 , i8 )) return - 1;
    if(!isa<int16>(import_test.add(i8 , i16)) || i8  + i16 != import_test.add(i8 , i16)) return - 2;
    if(!isa<int32>(import_test.add(i8 , i32)) || i8  + i32 != import_test.add(i8 , i32)) return - 3;
    if(!isa<int64>(import_test.add(i8 , i64)) || i8  + i64 != import_test.add(i8 , i64)) return - 4;
    if(!isa<int16>(import_test.add(i16, i8 )) || i16 + i8  != import_test.add(i16, i8 )) return - 5;
    if(!isa<int16>(import_test.add(i16, i16)) || i16 + i16 != import_test.add(i16, i16)) return - 6;
    if(!isa<int32>(import_test.add(i16, i32)) || i16 + i32 != import_test.add(i16, i32)) return - 7;
    if(!isa<int64>(import_test.add(i16, i64)) || i16 + i64 != import_test.add(i16, i64)) return - 8;
    if(!isa<int32>(import_test.add(i32, i8 )) || i32 + i8  != import_test.add(i32, i8 )) return - 9;
    if(!isa<int32>(import_test.add(i32, i16)) || i32 + i16 != import_test.add(i32, i16)) return -10;
    if(!isa<int32>(import_test.add(i32, i32)) || i32 + i32 != import_test.add(i32, i32)) return -11;
    if(!isa<int64>(import_test.add(i32, i64)) || i32 + i64 != import_test.add(i32, i64)) return -12;
    if(!isa<int64>(import_test.add(i64, i8 )) || i64 + i8  != import_test.add(i64, i8 )) return -13;
    if(!isa<int64>(import_test.add(i64, i16)) || i64 + i16 != import_test.add(i64, i16)) return -14;
    if(!isa<int64>(import_test.add(i64, i32)) || i64 + i32 != import_test.add(i64, i32)) return -15;
    if(!isa<int64>(import_test.add(i64, i64)) || i64 + i64 != import_test.add(i64, i64)) return -16;

    return 0;
}

function test_call_for_class(): int32 {
    var      cls_obj = new import_test     .cls();
    var ns_1_cls_obj = new import_test.ns_1.cls();
    var ns_2_cls_obj = new import_test.ns_2.cls();

    if(!isa<import_test     .cls>(import_test.compute(     cls_obj,      cls_obj))) return -101;
    if(!isa<import_test.ns_1.cls>(import_test.compute(     cls_obj, ns_1_cls_obj))) return -102;
    if(!isa<import_test.ns_2.cls>(import_test.compute(     cls_obj, ns_2_cls_obj))) return -103;
    if(!isa<import_test     .cls>(import_test.compute(ns_1_cls_obj,      cls_obj))) return -104;
    if(!isa<import_test.ns_1.cls>(import_test.compute(ns_1_cls_obj, ns_1_cls_obj))) return -105;
    if(!isa<import_test.ns_2.cls>(import_test.compute(ns_1_cls_obj, ns_2_cls_obj))) return -106;
    if(!isa<import_test     .cls>(import_test.compute(ns_2_cls_obj,      cls_obj))) return -107;
    if(!isa<import_test.ns_1.cls>(import_test.compute(ns_2_cls_obj, ns_1_cls_obj))) return -108;
    if(!isa<import_test.ns_2.cls>(import_test.compute(ns_2_cls_obj, ns_2_cls_obj))) return -109;

    return 0;
}

function test_call_for_mixed(): int32 {
    var cls_obj         = new import_test.ns_1.cls();
    var i8      : int8  = 1;
    var i32     : int32 = 2;

    if(!isa<int8                >(import_test.mixed(i8     , cls_obj)) || i8  != import_test.mixed(i8 , cls_obj)) return -201;
    if(!isa<int32               >(import_test.mixed(i32    , cls_obj)) || i32 != import_test.mixed(i32, cls_obj)) return -202;
    if(!isa<import_test.ns_1.cls>(import_test.mixed(cls_obj, i8     ))                                          ) return -203;
    if(!isa<import_test.ns_1.cls>(import_test.mixed(cls_obj, i32    ))                                          ) return -204;

    return 0;
}

function test_call_for_return_void_impl<T, U>(lhs: T, rhs: U): void {
    return import_test.return_void(lhs, rhs);
}

function test_call_for_return_void(): int32 {
    var i8  : int8            = 0x1;
    var i16 : int16           = 0x2;
    var i32 : int32           = 0x4;
    var i64 : int64           = 0x8;
    var cls : import_test.cls = new import_test.cls();

    test_call_for_return_void_impl(i8 , i8 );
    test_call_for_return_void_impl(i8 , i16);
    test_call_for_return_void_impl(i8 , i32);
    test_call_for_return_void_impl(i8 , i64);
    test_call_for_return_void_impl(i8 , cls);

    test_call_for_return_void_impl(i16, i8 );
    test_call_for_return_void_impl(i16, i16);
    test_call_for_return_void_impl(i16, i32);
    test_call_for_return_void_impl(i16, i64);
    test_call_for_return_void_impl(i16, cls);

    test_call_for_return_void_impl(i32, i8 );
    test_call_for_return_void_impl(i32, i16);
    test_call_for_return_void_impl(i32, i32);
    test_call_for_return_void_impl(i32, i64);
    test_call_for_return_void_impl(i32, cls);

    test_call_for_return_void_impl(i64, i8 );
    test_call_for_return_void_impl(i64, i16);
    test_call_for_return_void_impl(i64, i32);
    test_call_for_return_void_impl(i64, i64);
    test_call_for_return_void_impl(i64, cls);

    test_call_for_return_void_impl(cls, i8 );
    test_call_for_return_void_impl(cls, i16);
    test_call_for_return_void_impl(cls, i32);
    test_call_for_return_void_impl(cls, i64);
    test_call_for_return_void_impl(cls, cls);

    return 0;
}

@entry
function test_main(): int32 {
    var result: int32 = 0;

    result = test_call_for_primitive  (); if(result != 0) return result;
    result = test_call_for_class      (); if(result != 0) return result;
    result = test_call_for_mixed      (); if(result != 0) return result;
    result = test_call_for_return_void(); if(result != 0) return result;

    return 0;
}

