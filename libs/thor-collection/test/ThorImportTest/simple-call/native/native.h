/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#ifndef NATIVE_H
#define NATIVE_H

namespace ns_1 {

    class cls {
    };

}

namespace ns_2 {

    class cls {
    };

}

class cls {
};

 char add( char lhs,  char rhs);
short add( char lhs, short rhs);
  int add( char lhs,   int rhs);
 long add( char lhs,  long rhs);
short add(short lhs,  char rhs);
short add(short lhs, short rhs);
  int add(short lhs,   int rhs);
 long add(short lhs,  long rhs);
  int add(  int lhs,  char rhs);
  int add(  int lhs, short rhs);
  int add(  int lhs,   int rhs);
 long add(  int lhs,  long rhs);
 long add( long lhs,  char rhs);
 long add( long lhs, short rhs);
 long add( long lhs,   int rhs);
 long add( long lhs,  long rhs);

      ::cls compute(const       ::cls& lhs, const       ::cls& rhs);
::ns_1::cls compute(const       ::cls& lhs, const ::ns_1::cls& rhs);
::ns_2::cls compute(const       ::cls& lhs, const ::ns_2::cls& rhs);
      ::cls compute(const ::ns_1::cls& lhs, const       ::cls& rhs);
::ns_1::cls compute(const ::ns_1::cls& lhs, const ::ns_1::cls& rhs);
::ns_2::cls compute(const ::ns_1::cls& lhs, const ::ns_2::cls& rhs);
      ::cls compute(const ::ns_2::cls& lhs, const       ::cls& rhs);
::ns_1::cls compute(const ::ns_2::cls& lhs, const ::ns_1::cls& rhs);
::ns_2::cls compute(const ::ns_2::cls& lhs, const ::ns_2::cls& rhs);

       char mixed(             char  lhs, const ::ns_1::cls& rhs);
        int mixed(              int  lhs, const ::ns_1::cls& rhs);
::ns_1::cls mixed(const ::ns_1::cls& lhs,              char  rhs);
::ns_1::cls mixed(const ::ns_1::cls& lhs,               int  rhs);

void return_void(       char  lhs,        char  rhs);
void return_void(       char  lhs,       short  rhs);
void return_void(       char  lhs,         int  rhs);
void return_void(       char  lhs,        long  rhs);
void return_void(       char  lhs, const ::cls& rhs);
void return_void(      short  lhs,        char  rhs);
void return_void(      short  lhs,       short  rhs);
void return_void(      short  lhs,         int  rhs);
void return_void(      short  lhs,        long  rhs);
void return_void(      short  lhs, const ::cls& rhs);
void return_void(        int  lhs,        char  rhs);
void return_void(        int  lhs,       short  rhs);
void return_void(        int  lhs,         int  rhs);
void return_void(        int  lhs,        long  rhs);
void return_void(        int  lhs, const ::cls& rhs);
void return_void(       long  lhs,        char  rhs);
void return_void(       long  lhs,       short  rhs);
void return_void(       long  lhs,         int  rhs);
void return_void(       long  lhs,        long  rhs);
void return_void(       long  lhs, const ::cls& rhs);
void return_void(const ::cls& lhs,        char  rhs);
void return_void(const ::cls& lhs,       short  rhs);
void return_void(const ::cls& lhs,         int  rhs);
void return_void(const ::cls& lhs,        long  rhs);
void return_void(const ::cls& lhs, const ::cls& rhs);

#endif /* NATIVE_H */

