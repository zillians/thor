/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import import_test;
import import_test.ns;

function test_struct_get_1<target_type>(error_base: int32): int32 {
    var target = new target_type();

    target.get_inner_1().set_b  (true);
    target.get_inner_1().set_i8 (   8);
    target.get_inner_1().set_i16(   9);
    target.get_inner_1().set_i32(  10);
    target.get_inner_1().set_i64(  11);
    target.get_inner_1().set_f32(16.0);
    target.get_inner_1().set_f64(32.0);

    if(!target.get_inner_1().get_b  ()        ) return error_base - 0;
    if( target.get_inner_1().get_i8 () !=    8) return error_base - 1;
    if( target.get_inner_1().get_i16() !=    9) return error_base - 2;
    if( target.get_inner_1().get_i32() !=   10) return error_base - 3;
    if( target.get_inner_1().get_i64() !=   11) return error_base - 4;
    if( target.get_inner_1().get_f32() != 16.0) return error_base - 5;
    if( target.get_inner_1().get_f64() != 32.0) return error_base - 6;

    return 0;
}

function test_struct_get_2<target_type>(error_base: int32): int32 {
    var target = new target_type();

    target.get_inner_2().set_b  (false);
    target.get_inner_2().set_i8 (   12);
    target.get_inner_2().set_i16(   13);
    target.get_inner_2().set_i32(   14);
    target.get_inner_2().set_i64(   15);
    target.get_inner_2().set_f32( 64.0);
    target.get_inner_2().set_f64(128.0);

    if( target.get_inner_2().get_b  ()         ) return error_base - 0;
    if( target.get_inner_2().get_i8 () !=    12) return error_base - 1;
    if( target.get_inner_2().get_i16() !=    13) return error_base - 2;
    if( target.get_inner_2().get_i32() !=    14) return error_base - 3;
    if( target.get_inner_2().get_i64() !=    15) return error_base - 4;
    if( target.get_inner_2().get_f32() !=  64.0) return error_base - 5;
    if( target.get_inner_2().get_f64() != 128.0) return error_base - 6;

    return 0;
}

function test_struct_get_3<target_type>(error_base: int32): int32 {
    var target = new target_type();

    target.get_inner_3().set_b  ( true);
    target.get_inner_3().set_i8 (   16);
    target.get_inner_3().set_i16(   17);
    target.get_inner_3().set_i32(   18);
    target.get_inner_3().set_i64(   19);
    target.get_inner_3().set_f32(256.0);
    target.get_inner_3().set_f64(512.0);

    if(!target.get_inner_3().get_b  ()         ) return error_base - 0;
    if( target.get_inner_3().get_i8 () !=    16) return error_base - 1;
    if( target.get_inner_3().get_i16() !=    17) return error_base - 2;
    if( target.get_inner_3().get_i32() !=    18) return error_base - 3;
    if( target.get_inner_3().get_i64() !=    19) return error_base - 4;
    if( target.get_inner_3().get_f32() != 256.0) return error_base - 5;
    if( target.get_inner_3().get_f64() != 512.0) return error_base - 6;

    return 0;
}

function test_struct_set_1<target_type, inner_1_type>(error_base: int32): int32 {
    var target      = new target_type();
    var new_inner_1 = new inner_1_type();

    new_inner_1.set_b  ( false);
    new_inner_1.set_i8 (    20);
    new_inner_1.set_i16(    21);
    new_inner_1.set_i32(    22);
    new_inner_1.set_i64(    23);
    new_inner_1.set_f32(1024.0);
    new_inner_1.set_f64(2048.0);

    target.set_inner_1(new_inner_1);

    if( target.get_inner_1().get_b  ()          ) return error_base - 0;
    if( target.get_inner_1().get_i8 () !=     20) return error_base - 1;
    if( target.get_inner_1().get_i16() !=     21) return error_base - 2;
    if( target.get_inner_1().get_i32() !=     22) return error_base - 3;
    if( target.get_inner_1().get_i64() !=     23) return error_base - 4;
    if( target.get_inner_1().get_f32() != 1024.0) return error_base - 5;
    if( target.get_inner_1().get_f64() != 2048.0) return error_base - 6;

    return 0;
}

@entry
function test_main(): int32 {
    var result: int32 = 0;

    result = test_struct_get_1<import_test.ns.ori_class                         >(-30); if(result != 0) return result;
    result = test_struct_get_2<import_test.ns.ori_class                         >(-40); if(result != 0) return result;
    result = test_struct_get_3<import_test.ns.ori_class                         >(-50); if(result != 0) return result;
    result = test_struct_set_1<import_test.ns.ori_class , import_test.used_inner>(-60); if(result != 0) return result;
    result = test_struct_get_1<import_test.   ori_struct                        >(-70); if(result != 0) return result;
    result = test_struct_get_3<import_test.   ori_struct                        >(-80); if(result != 0) return result;
    result = test_struct_set_1<import_test.   ori_struct, import_test.used_inner>(-90); if(result != 0) return result;

    return 0;
}

