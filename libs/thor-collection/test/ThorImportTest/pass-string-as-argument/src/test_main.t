/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import import_test;

@entry
function test_main(): int32 {
    if(import_test.test_string (null) != -1) return -1;
    if(import_test.test_wstring(null) != -1) return -2;

    if(import_test.test_string ("abcdef") != 6) return -3;
    if(import_test.test_wstring("abcdef") != 6) return -4;

    // '一', '二', '三' are three bytes in UTF-8
    if(import_test.test_string ("abc一二三def") != 15) return -5;
    if(import_test.test_wstring("abc一二三def") !=  9) return -6;

    return 0;
}

