#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

#MESSAGE(STATUS "ROOT        : ${ROOT}")
#MESSAGE(STATUS "TYPE        : ${TYPE}")
#MESSAGE(STATUS "THOR_DRIVER : ${THOR_DRIVER}")
#MESSAGE(STATUS "SEQ_EXECUTOR: ${SEQ_EXECUTOR}")

IF(NOT ROOT)
    MESSAGE(FATAL_ERROR "missing root dir")
ELSEIF(NOT TYPE)
    MESSAGE(FATAL_ERROR "missing bundle type")
ELSEIF(NOT THOR_DRIVER)
    MESSAGE(FATAL_ERROR "missing thor driver executable")
ELSEIF(NOT SEQ_EXECUTOR)
    MESSAGE(FATAL_ERROR "missing sequential executor script")
ENDIF()

CONFIGURE_FILE("${ROOT}/manifest.xml" "${ROOT}/manifest.xml")

MACRO(append_command cmd)
    LIST(LENGTH cmds cmd_count)

    LIST(APPEND cmds "-DCMD_${cmd_count}:STRING=${cmd}")

    UNSET(cmd_count)
ENDMACRO()

IF(EXISTS "${ROOT}/native/Makefile")
    append_command("make -C native")
ENDIF()

append_command("${THOR_DRIVER} build    debug  --verbose")
append_command("${THOR_DRIVER} generate bundle --verbose")

EXECUTE_PROCESS(
    COMMAND ${CMAKE_COMMAND}
            ${cmds}
            -P "${SEQ_EXECUTOR}"
    WORKING_DIRECTORY "${ROOT}"
    RESULT_VARIABLE   result
)

IF(NOT "${result}" STREQUAL "0")
    MESSAGE(FATAL_ERROR "[bundle-builder.cmake] failed to build (${TYPE}) bundle in: ${ROOT}")
ENDIF()

MESSAGE(STATUS "[bundle-builder.cmake] built (${TYPE}) bundle in: ${ROOT}")
