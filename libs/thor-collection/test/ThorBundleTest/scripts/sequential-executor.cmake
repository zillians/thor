#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

#MESSAGE(STATUS "CMD_0    : ${CMD_0}")
#MESSAGE(STATUS "CMD_1    : ${CMD_1}")
#MESSAGE(STATUS "CMD_2    : ${CMD_2}")
#MESSAGE(STATUS "CMD_3    : ${CMD_3}")
#MESSAGE(STATUS "CMD_4    : ${CMD_4}")
#MESSAGE(STATUS "CMD_5    : ${CMD_5}")
#MESSAGE(STATUS "CMD_6    : ${CMD_6}")
#MESSAGE(STATUS "CMD_7    : ${CMD_7}")
#MESSAGE(STATUS "CMD_8    : ${CMD_8}")
#MESSAGE(STATUS "CMD_9    : ${CMD_9}")

SET(index 0)

WHILE(CMD_${index})
    SEPARATE_ARGUMENTS(cmd_list UNIX_COMMAND ${CMD_${index}})

    EXECUTE_PROCESS(
        COMMAND         ${cmd_list}
        RESULT_VARIABLE result
    )

    #MESSAGE(STATUS "result: ${result}")

    IF(result)
        MESSAGE(FATAL_ERROR "[sequential-executor.cmake] failed to run command: ${CMD_${index}}")
    ENDIF()

    MATH(EXPR index "${index} + 1")
ENDWHILE()

MESSAGE(STATUS "[sequential-executor.cmake] success after ${index} commands")
