/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import base;
import lib;

@entry
task do_test()
{
    const obj_1 = base.get_foo_i32(1234);
    const obj_2 = lib .get_foo_i32(5678);

    if (obj_1 == null)
        exit(1);

    if (obj_2 == null)
        exit(2);

    const foo_i32_1 = cast<base.foo<int32> >(obj_1);
    const foo_i32_2 = cast<base.foo<int32> >(obj_2);

    if (foo_i32_1 == null)
        exit(3);

    if (foo_i32_2 == null)
        exit(4);

    if (foo_i32_1.value != 1234)
        exit(5);

    if (foo_i32_2.value != 5678)
        exit(6);


    exit(0);
}
