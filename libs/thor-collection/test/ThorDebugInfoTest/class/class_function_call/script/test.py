#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")

'''
set_global_config(white_list = ['test.t', 'native.cpp'])
lazy_generate_mode('v', 'gg', 'y', 'this.y', 'ggyy', 'x', 'this.x', 'a.x', 'a.member.y', 'a->x', 'a->member->y', threshold=[0, 100], max_step=50)
'''

set_global_config(white_list = ['test.t', 'native.cpp'])
try:
        execute_check("run", 46)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 48)
        execute_check_ex("s", {"EXPECT_FILE":"native.cpp"}, 39)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 9)
        execute_check_ex("s", {"v":13, "EXPECT_FILE":"test.t"}, 11)
        execute_check_ex("s", {"v":13, "gg":3, "EXPECT_FILE":"test.t"}, 12)
        execute_check_ex("s", {"v":13, "gg":16, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"v":13, "y":16, "this.y":16, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"y":16, "this.y":16, "EXPECT_FILE":"test.t"}, 25)
        execute_check_ex("s", {"y":16, "this.y":16, "ggyy":1, "EXPECT_FILE":"test.t"}, 26)
        execute_check_ex("s", {"y":16, "this.y":16, "x":1, "this.x":1, "EXPECT_FILE":"test.t"}, 27)
        execute_check_ex("s", {"EXPECT_FILE":"native.cpp"}, 40)
        execute_check_ex("s", {"y":16, "this.y":16, "EXPECT_FILE":"test.t"}, 9)
        execute_check_ex("s", {"v":20, "EXPECT_FILE":"test.t"}, 11)
        execute_check_ex("s", {"v":20, "gg":3, "EXPECT_FILE":"test.t"}, 12)
        execute_check_ex("s", {"v":20, "gg":23, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"v":20, "y":23, "this.y":23, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 34)
        execute_check_ex("s", {"y":16, "this.y":16, "x":1, "this.x":1, "EXPECT_FILE":"test.t"}, 36)
        execute_check_ex("s", {"y":16, "this.y":16, "x":1, "this.x":1, "EXPECT_FILE":"test.t"}, 37)
        execute_check_ex("s", {"EXPECT_FILE":"native.cpp"}, 41)
        execute_check_ex("s", {"EXPECT_FILE":"native.cpp"}, 42)
        execute_check_ex("s", {"a.x":1, "a.member.y":23, "a->x":1, "a->member->y":23, "EXPECT_FILE":"test.t"}, 49)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 29)
        execute_check_ex("s", {"y":16, "this.y":16, "x":1, "this.x":1, "EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"y":16, "this.y":16, "x":11, "this.x":11, "EXPECT_FILE":"test.t"}, 32)
        execute_check_ex("s", {"a.x":11, "a.member.y":23, "a->x":11, "a->member->y":23, "EXPECT_FILE":"test.t"}, 50)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 51)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code
