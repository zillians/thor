/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class B
{
	public function new() : void
	{
		var gg : int32 = 3;
		gg += 1;
	}
	
	public function new(v : int32) : void
	{
		var gg : int32 = 3;
		gg = gg + v;
		y = gg;
	}

	public var y : int32;
}

class A extends B
{
	public function new() : void
	{
		super(13);
		
		var ggyy : int32 = 1;
		x = ggyy;
	}

	public function f(z : int32) : void
	{
		x = x + z;
	}

	public function set(m: B) : void
	{
		member = m;
	}

	public var x : int32;
	public var member : B;
}

@native
function createObject() : A;

@entry task test1() : int32
{
	var a : A = createObject();
	a.f(10);
	exit(0);
	return -1;
}
