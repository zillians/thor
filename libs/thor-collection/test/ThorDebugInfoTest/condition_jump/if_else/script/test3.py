#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test3")

set_global_config(white_list = ['test.t'])
#lazy_generate_mode('x', 'y', 'z', threshold=[0, 20], max_step=50)

try:
        execute_check("run", 77)
        execute_check_ex("s", {"x":10, "y":0, "EXPECT_FILE":"test.t"}, 78)
        execute_check_ex("s", {"x":10, "y":10, "EXPECT_FILE":"test.t"}, 80)
        execute_check_ex("s", {"x":10, "y":10, "EXPECT_FILE":"test.t"}, 82)
        execute_check_ex("s", {"x":5, "y":10, "EXPECT_FILE":"test.t"}, 83)
        execute_check_ex("s", {"x":5, "y":10, "EXPECT_FILE":"test.t"}, 85)
        execute_check_ex("s", {"x":5, "y":15, "EXPECT_FILE":"test.t"}, 87)
        execute_check_ex("s", {"x":5, "y":15, "EXPECT_FILE":"test.t"}, 89)
        execute_check_ex("s", {"x":5, "y":15, "z":20, "EXPECT_FILE":"test.t"}, 90)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 91)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code

