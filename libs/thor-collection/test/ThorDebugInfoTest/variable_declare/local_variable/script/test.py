#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")
set_global_config(white_list = ['test.t'])
try:
	execute_check("run",  3 )
	execute_check("n", 4, x = 34)
	execute_check("n", 5, x = 34, y = 3)
	execute_check("n", 7, x = 34, y = 3, z = 11)
	gdb.execute('quit')
except MismatchException, e:
	gdb.execute('quit 1') # quit with error code

