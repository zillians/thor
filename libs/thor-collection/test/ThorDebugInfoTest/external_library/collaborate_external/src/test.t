/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function my_print() : void;

@native
function my_print_value(x:int16) : void;

@native
function please_callback() : void;

@entry task test1() : int32
{
    my_print(); exit(0); return -1;
}

@entry task test2() : int32
{
    my_print_value(3000); exit(0); return -1;
}

@entry task test3() : int32
{
    please_callback(); exit(0); return -1;
}

function callback() : void
{
    var x : int16 = 3;
    var y : int16 = 8;

    x = x + y;
}
