#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")
set_global_config(white_list = ['test.t', 'print.cpp'])
#lazy_generate_mode(threshold=[0, 100], max_step=50)

try:
        execute_check("run", 11)
        execute_check_ex("s", {"EXPECT_FILE":"print.cpp"}, 5)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 14)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code

