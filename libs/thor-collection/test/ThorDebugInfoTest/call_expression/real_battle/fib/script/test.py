#!/usr/bin/python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import sys
sys.path.insert(0, "../../../")
sys.path.insert(0, "./script/")

import lib
from lib.baselib import *

#
# Run and check
#
gdb.Breakpoint("test.t:test1")
set_global_config(white_list = ['test.t'])
#lazy_generate_mode('index', 'i', 'pre1', 'pre2', threshold=[0, 50], max_step=50)

try:
        execute_check("run", 29)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 2)
        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 4)
        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 7)
        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 8)
        execute_check_ex("s", {"index":8, "pre1":1, "EXPECT_FILE":"test.t"}, 9)
        execute_check_ex("s", {"index":8, "pre1":1, "pre2":0, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":0, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":3, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":3, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":3, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":3, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":4, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":4, "pre1":2, "pre2":2, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":4, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":4, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":5, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":5, "pre1":3, "pre2":3, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":5, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":5, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":6, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":6, "pre1":5, "pre2":5, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":6, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":6, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":7, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":7, "pre1":8, "pre2":8, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":7, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":7, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":8, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 13)
        execute_check_ex("s", {"index":8, "i":8, "pre1":13, "pre2":13, "EXPECT_FILE":"test.t"}, 14)
        execute_check_ex("s", {"index":8, "i":8, "pre1":21, "pre2":13, "EXPECT_FILE":"test.t"}, 15)
        execute_check_ex("s", {"index":8, "i":8, "pre1":21, "pre2":13, "EXPECT_FILE":"test.t"}, 10)
        execute_check_ex("s", {"index":8, "i":9, "pre1":21, "pre2":13, "EXPECT_FILE":"test.t"}, 16)
        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 17)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 31)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 32)
        execute_check_ex("s", {"index":0, "EXPECT_FILE":"test.t"}, 19)
        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 19)
        execute_check_ex("s", {"index":7, "EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"index":7, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 19)
        execute_check_ex("s", {"index":6, "EXPECT_FILE":"test.t"}, 21)
        execute_check_ex("s", {"index":6, "EXPECT_FILE":"test.t"}, 23)
        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 19)
        execute_check_ex("s", {"index":5, "EXPECT_FILE":"test.t"}, 21)
except MismatchException, e:
        gdb.execute('quit 1') # quit with error code

#try:
#        execute_check("run", 29)
#        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 31)
#        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 2)
#        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 4)
#        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 7)
#        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 8)
#        execute_check_ex("s", {"index":8, "pre1":1, "EXPECT_FILE":"test.t"}, 9)
#        execute_check_ex("s", {"index":8, "pre1":1, "pre2":0, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":0, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":0, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":2, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":3, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":3, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":3, "pre1":1, "pre2":1, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":3, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":3, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":4, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":4, "pre1":2, "pre2":1, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":4, "pre1":2, "pre2":2, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":4, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":4, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":5, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":5, "pre1":3, "pre2":2, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":5, "pre1":3, "pre2":3, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":5, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":5, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":6, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":6, "pre1":5, "pre2":3, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":6, "pre1":5, "pre2":5, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":6, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":6, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":7, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":7, "pre1":8, "pre2":5, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":7, "pre1":8, "pre2":8, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":7, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":7, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":8, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 12)
#        execute_check_ex("s", {"index":8, "i":8, "pre1":13, "pre2":8, "EXPECT_FILE":"test.t"}, 13)
#        execute_check_ex("s", {"index":8, "i":8, "pre1":13, "pre2":13, "EXPECT_FILE":"test.t"}, 14)
#        execute_check_ex("s", {"index":8, "i":8, "pre1":21, "pre2":13, "EXPECT_FILE":"test.t"}, 15)
#        execute_check_ex("s", {"index":8, "i":8, "pre1":21, "pre2":13, "EXPECT_FILE":"test.t"}, 10)
#        execute_check_ex("s", {"index":8, "i":9, "pre1":21, "pre2":13, "EXPECT_FILE":"test.t"}, 16)
#        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 17)
#        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 32)
#        execute_check_ex("s", {"index":0, "EXPECT_FILE":"test.t"}, 19)
#        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 21)
#        execute_check_ex("s", {"index":8, "EXPECT_FILE":"test.t"}, 23)
#        execute_check_ex("s", {"EXPECT_FILE":"test.t"}, 19)
#        execute_check_ex("s", {"index":7, "EXPECT_FILE":"test.t"}, 21)
#except MismatchException, e:
#        gdb.execute('quit 1') # quit with error code
