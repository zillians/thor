/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.container;
import . = thor.lang;
import . = thor.gc;

class A
{
    public function new(): void
    {
        id = A.base + A.count;
        ++A.count;
    }

    public static var base: int32 = 11;
    public static var count: int32 = 0;
    public var id: int32;
}

class B
{
    public function new(): void
    {
        id = B.base + B.count;
        ++B.count;
    }

    public static var base: int32 = 11001;
    public static var count: int32 = 0;
    public var id: int32;
}

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------\n");
        thor.lang.print("     a_array_exist: \{     a_array_exist}\n");
        thor.lang.print("     b_array_exist: \{     b_array_exist}\n");
        thor.lang.print("elem_in_a_array_ok: \{elem_in_a_array_ok}\n");
        thor.lang.print("elem_in_b_array_ok: \{elem_in_b_array_ok}\n");
        thor.lang.print("--------------------- GC result\n");
    }

    public var      a_array_exist: bool = false;
    public var      b_array_exist: bool = false;
    public var elem_in_a_array_ok: bool = false;
    public var elem_in_b_array_ok: bool = false;
}

function check_result() : Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>) : Result
{
    var result = new Result();

    var obj_count = objs.size();
    var   a_count = 0;
    var   b_count = 0;

    for (var i = 0; i < objs.size(); ++i)
    {
        var obj: Object = objs.get(i);

        if (isa<Vector<A> >(obj))
        {
            var vv = cast<Vector<A> >(obj);
            if (vv.size() == 10)
            {
                var match = true;
                for (var j = 0; j < vv.size(); ++j)
                {
                    var o = vv.get(i);
                    if (o.id != A.base + i) match = false;
                }
                if (match) result.a_array_exist = true;
            }
        }
        else if (isa<Vector<B> >(obj))
        {
            var vv = cast<Vector<B> >(obj);
            if (vv.size() == 20)
            {
                var match = true;
                for (var j = 0; j < vv.size(); ++j)
                {
                    var o = vv.get(i);
                    if (o.id != B.base + i) match = false;
                }
                if (match) result.b_array_exist = true;
            }
        }
        else if (isa<A>(obj))
        {
            ++a_count;
        }
        else if (isa<B>(obj))
        {
            ++b_count;
        }
    }

    if (a_count == 10) result.elem_in_a_array_ok = true;
    if (b_count == 20) result.elem_in_b_array_ok = true;

    print("obj_count: \{obj_count}\n");
    print("  a_count: \{  a_count}\n");
    print("  b_count: \{  b_count}\n");

    result.print();

    return result;
}

function create_objects(): bool
{
    var local_v = new Vector<B>();
    for (var i = 0; i < 20; ++i)
    {
        local_v.pushBack(new B());
    }

    var r = check_result();
    return (r.a_array_exist && r.b_array_exist && r.elem_in_a_array_ok && r.elem_in_b_array_ok);
}

var v: Vector<A> = null;

@entry
task test1() : void
{
	v = new Vector<A>();
	for (var i = 0; i < 10; ++i)
	{
		v.pushBack(new A());
	}
    
    if (!create_objects()) exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            var r = check_result(objs);
            if (r.a_array_exist && !r.b_array_exist && r.elem_in_a_array_ok && !r.elem_in_b_array_ok)
                exit(0);
            else
                exit(2);
        }
    );
}
