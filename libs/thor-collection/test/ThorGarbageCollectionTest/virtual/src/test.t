/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.container;
import . = thor.lang;
import . = thor.gc;

class Product {}

interface Annoy
{
	function annoying() : void;
}

// double reference
class Base
{
	public function setRef(b : Base) : void
	{
		ref = b;
	}

	public var ref : Base;
}

class Master extends Base implements Annoy
{
	public function new() : void 
	{
		products = new Vector<Product>();
		var i : int32;
		for (i = 0; i < 10; ++i)
		{
			products.pushBack(new Product());
		}
	}

	public virtual function annoying() : void {}
	public var products : Vector<Product>;
}

class Slave extends Base implements Annoy
{
    public function new(_id: int32) { id = _id; }
	public virtual function annoying() : void {}

    public var id :int32;
}

class Result
{
    public function print(): void
    {
        thor.lang.print("GC result ---------------------------\n");
        thor.lang.print("    master_exist: \{    master_exist}\n");
        thor.lang.print("     slave_exist: \{     slave_exist}\n");
        thor.lang.print("poor_slave_exist: \{poor_slave_exist}\n");
        thor.lang.print("--------------------------- GC result\n");
    }

    public var     master_exist: bool = false;
    public var      slave_exist: bool = false;
    public var poor_slave_exist: bool = false;
}

function check_result(): Result
{
    return check_result(__getActiveObjects());
}

function check_result(objs: Vector<Object>): Result
{
    var r = new Result();

    for (var i = 0; i < objs.size(); ++i)
    {
        // master
        if (isa<Master>(objs.get(i)))
        {
            var obj_m = cast<Master>(objs.get(i));

            if (obj_m.products.size() != 10) continue;
            var my_slave = cast<Slave>(obj_m.ref);

            if (my_slave.id != 1130) continue;

            r.master_exist =true;
        }
        // slave
        else if (isa<Slave>(objs.get(i)))
        {
            var obj_s = cast<Slave>(objs.get(i));
            if (obj_s.id == 1130) r.slave_exist = true;
            if (obj_s.id == 9527) r.poor_slave_exist = true;
        }
    }

    r.print();

    return r;
}

function create_object() : bool
{
	var poor_slave : Base = new Slave(9527);
	poor_slave.setRef(poor_slave);

    var r = check_result();
    return (r.master_exist && r.slave_exist && r.poor_slave_exist);
}

var m : Base = null;
var s : Base = null;

@entry
task test1() : void
{
	m = new Master();
	s = new Slave(1130);

	m.setRef(s);
	s.setRef(m);

    if (!create_object())
        exit(1);

    __waitGC(
        lambda(objs: Vector<Object>): void
        {
            var r = check_result(objs);

            if (r.master_exist && r.slave_exist && !r.poor_slave_exist)
                exit(0);
            else
                exit(2);
        }
    );
}
