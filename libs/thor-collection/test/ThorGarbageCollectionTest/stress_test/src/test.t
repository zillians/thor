/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import . = thor.lang;
import . = thor.container;
import . = thor.gc;

class Dummy
{
    public function delete() : void
    {
        value = 1;
    }

    public var value : int32 = 13;
}

var gFinish : bool = false;
var gSuccess : bool = false;

function run_too_long() : void
{
    performAndWaitGC();
}

function check_me(d : Dummy): void
{
    gSuccess = (d.value == 13);
    gFinish = true;
}

@entry
task test1(): void
{
    /* 
     * Well, this case will cause no gc, we need to 
     * find out a way to handle it.
     */
    for (var i = 0; i < 1000; ++i)
    {
        async -> run_too_long();
    }

    async -> check_me(new Dummy());

    while (!gFinish);
    if (gSuccess) exit(0);
    else exit(1);
}
