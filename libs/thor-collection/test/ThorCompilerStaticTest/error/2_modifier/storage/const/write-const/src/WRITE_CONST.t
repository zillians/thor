/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function f(): void
{
    var   variable  : int32 = 0;
    const const_var : int32 = 1;
    const const_var2: int32 = 2;

    // assignment to const variable
    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="assignment", var_id="const_var"            } } }
    const_var = 999;

    var cond = false;
    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="assignment", var_id="const_var"            } } }
    (cond ? const_var : variable) = 999;

    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="assignment", var_id="const_var, const_var2"} } }
    (cond ? const_var : const_var2) = 999;

    (cond ? variable : variable) = 999; // should pass

    // increment to const variable
    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="increment", var_id="const_var"            } } }
    ++const_var;

    cond = true;
    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="increment", var_id="const_var"            } } }
    ++(cond ? const_var : variable);

    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="increment", var_id="const_var, const_var2"} } }
    ++(cond ? const_var : const_var2);

    ++(cond ? variable : variable); // should pass

    // decrement to const variable
    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="decrement", var_id="const_var"            } } }
    --const_var;

    cond = false;
    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="decrement", var_id="const_var"            } } }
    --(cond ? const_var : variable);

    @static_test { expect_message={ level="LEVEL_ERROR", id="WRITE_CONST", parameters={ form="decrement", var_id="const_var, const_var2"} } }
    --(cond ? const_var : const_var2);

    --(cond ? variable : variable); // should pass
}
