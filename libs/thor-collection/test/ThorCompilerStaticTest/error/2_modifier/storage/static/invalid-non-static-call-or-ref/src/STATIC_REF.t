/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import pl;
import pl.pl2;


var global_var: pl.dummy = null;

class cls_var extends parent {
	public static var static_var    : pl.dummy = null;
	public        var non_static_var: pl.dummy = null;

	public static function for_static_call(x: pl.dummy, y: pl.pl2.dummy): void {
		pl.dummy.static_var;
		pl.dummy.static_var.static_call();
		pl.dummy.static_var.non_static_call();
		pl.dummy.static_var.static_var;
		pl.dummy.static_var.non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
		pl.dummy.non_static_var; // should fail

		pl.pl2.dummy.static_var;
		pl.pl2.dummy.static_var.static_call();
		pl.pl2.dummy.static_var.non_static_call();
		pl.pl2.dummy.static_var.static_var;
		pl.pl2.dummy.static_var.non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
		pl.pl2.dummy.non_static_var; // should fail

		static_var;
		static_var.static_call();
		static_var.non_static_call();
		static_var.static_var;
		static_var.non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
		non_static_var; // should fail

		       parent_static_var;
		parent.parent_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "parent_non_static_var" } } }
		       parent_non_static_var; // should fail
		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "parent_non_static_var" } } }
		parent.parent_non_static_var; // should fail

		child_var.child_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "child_non_static_var" } } }
		child_var.child_non_static_var;

		x.static_var;
		x.static_var.static_call();
		x.static_var.non_static_call();
		x.static_var.static_var;
		x.static_var.non_static_var;
		x.non_static_var;
		x.non_static_var.static_call();
		x.non_static_var.non_static_call();
		x.non_static_var.static_var;
		x.non_static_var.non_static_var;

		y.static_var;
		y.static_var.static_call();
		y.static_var.non_static_call();
		y.static_var.static_var;
		y.static_var.non_static_var;
		y.non_static_var;
		y.non_static_var.static_call();
		y.non_static_var.non_static_call();
		y.non_static_var.static_var;
		y.non_static_var.non_static_var;

		(new pl.dummy()).static_var;
		(new pl.dummy()).static_var.static_call();
		(new pl.dummy()).static_var.non_static_call();
		(new pl.dummy()).static_var.static_var;
		(new pl.dummy()).static_var.non_static_var;
		(new pl.dummy()).non_static_var;
		(new pl.dummy()).non_static_var.static_call();
		(new pl.dummy()).non_static_var.non_static_call();
		(new pl.dummy()).non_static_var.static_var;
		(new pl.dummy()).non_static_var.non_static_var;

		(new pl.pl2.dummy()).static_var;
		(new pl.pl2.dummy()).static_var.static_call();
		(new pl.pl2.dummy()).static_var.non_static_call();
		(new pl.pl2.dummy()).static_var.static_var;
		(new pl.pl2.dummy()).static_var.non_static_var;
		(new pl.pl2.dummy()).non_static_var;
		(new pl.pl2.dummy()).non_static_var.static_call();
		(new pl.pl2.dummy()).non_static_var.non_static_call();
		(new pl.pl2.dummy()).non_static_var.static_var;
		(new pl.pl2.dummy()).non_static_var.non_static_var;

		global_var;
		global_var.static_call();
		global_var.non_static_call();
		global_var.static_var;
		global_var.non_static_var;

		pl.global_var;
		pl.global_var.static_call();
		pl.global_var.non_static_call();
		pl.global_var.static_var;
		pl.global_var.non_static_var;

		pl.pl2.global_var;
		pl.pl2.global_var.static_call();
		pl.pl2.global_var.non_static_call();
		pl.pl2.global_var.static_var;
		pl.pl2.global_var.non_static_var;
	}

	public function for_non_static_call(x: pl.dummy, y: pl.pl2.dummy): void {
		pl.dummy.static_var;
		pl.dummy.static_var.static_call();
		pl.dummy.static_var.non_static_call();
		pl.dummy.static_var.static_var;
		pl.dummy.static_var.non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
		pl.dummy.non_static_var; // should fail

		pl.pl2.dummy.static_var;
		pl.pl2.dummy.static_var.static_call();
		pl.pl2.dummy.static_var.non_static_call();
		pl.pl2.dummy.static_var.static_var;
		pl.pl2.dummy.static_var.non_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
		pl.pl2.dummy.non_static_var; // should fail

		static_var;
		static_var.static_call();
		static_var.non_static_call();
		static_var.static_var;
		static_var.non_static_var;
		non_static_var;
		non_static_var.static_call();
		non_static_var.non_static_call();
		non_static_var.static_var;
		non_static_var.non_static_var;

		       parent_static_var;
		parent.parent_static_var;
		       parent_non_static_var;
		parent.parent_non_static_var;

		child_var.child_static_var;

		@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "child_non_static_var" } } }
		child_var.child_non_static_var;

		x.static_var;
		x.static_var.static_call();
		x.static_var.non_static_call();
		x.static_var.static_var;
		x.static_var.non_static_var;
		x.non_static_var;
		x.non_static_var.static_call();
		x.non_static_var.non_static_call();
		x.non_static_var.static_var;
		x.non_static_var.non_static_var;

		y.static_var;
		y.static_var.static_call();
		y.static_var.non_static_call();
		y.static_var.static_var;
		y.static_var.non_static_var;
		y.non_static_var;
		y.non_static_var.static_call();
		y.non_static_var.non_static_call();
		y.non_static_var.static_var;
		y.non_static_var.non_static_var;

		(new pl.dummy()).static_var;
		(new pl.dummy()).static_var.static_call();
		(new pl.dummy()).static_var.non_static_call();
		(new pl.dummy()).static_var.static_var;
		(new pl.dummy()).static_var.non_static_var;
		(new pl.dummy()).non_static_var;
		(new pl.dummy()).non_static_var.static_call();
		(new pl.dummy()).non_static_var.non_static_call();
		(new pl.dummy()).non_static_var.static_var;
		(new pl.dummy()).non_static_var.non_static_var;

		(new pl.pl2.dummy()).static_var;
		(new pl.pl2.dummy()).static_var.static_call();
		(new pl.pl2.dummy()).static_var.non_static_call();
		(new pl.pl2.dummy()).static_var.static_var;
		(new pl.pl2.dummy()).static_var.non_static_var;
		(new pl.pl2.dummy()).non_static_var;
		(new pl.pl2.dummy()).non_static_var.static_call();
		(new pl.pl2.dummy()).non_static_var.non_static_call();
		(new pl.pl2.dummy()).non_static_var.static_var;
		(new pl.pl2.dummy()).non_static_var.non_static_var;

		global_var;
		global_var.static_call();
		global_var.non_static_call();
		global_var.static_var;
		global_var.non_static_var;

		pl.global_var;
		pl.global_var.static_call();
		pl.global_var.non_static_call();
		pl.global_var.static_var;
		pl.global_var.non_static_var;

		pl.pl2.global_var;
		pl.pl2.global_var.static_call();
		pl.pl2.global_var.non_static_call();
		pl.pl2.global_var.static_var;
		pl.pl2.global_var.non_static_var;
	}
}

class child_var extends cls_var {
	public static var child_static_var    : int32 = 1234;
	public        var child_non_static_var: int32 = 4321;
}

function for_ref_test(x: pl.dummy, y: pl.pl2.dummy): void {
	pl.dummy.static_var;
	pl.dummy.static_var.static_call();
	pl.dummy.static_var.non_static_call();
	pl.dummy.static_var.static_var;
	pl.dummy.static_var.non_static_var;

	@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
	pl.dummy.non_static_var; // should fail

	pl.pl2.dummy.static_var;
	pl.pl2.dummy.static_var.static_call();
	pl.pl2.dummy.static_var.non_static_call();
	pl.pl2.dummy.static_var.static_var;
	pl.pl2.dummy.static_var.non_static_var;

	@static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_NONSTATIC_REF", parameters = { var_id = "non_static_var" } } }
	pl.pl2.dummy.non_static_var; // should fail

	x.static_var;
	x.static_var.static_call();
	x.static_var.non_static_call();
	x.static_var.static_var;
	x.static_var.non_static_var;
	x.non_static_var;
	x.non_static_var.static_call();
	x.non_static_var.non_static_call();
	x.non_static_var.static_var;
	x.non_static_var.non_static_var;

	y.static_var;
	y.static_var.static_call();
	y.static_var.non_static_call();
	y.static_var.static_var;
	y.static_var.non_static_var;
	y.non_static_var;
	y.non_static_var.static_call();
	y.non_static_var.non_static_call();
	y.non_static_var.static_var;
	y.non_static_var.non_static_var;

	(new pl.dummy()).static_var;
	(new pl.dummy()).static_var.static_call();
	(new pl.dummy()).static_var.non_static_call();
	(new pl.dummy()).static_var.static_var;
	(new pl.dummy()).static_var.non_static_var;
	(new pl.dummy()).non_static_var;
	(new pl.dummy()).non_static_var.static_call();
	(new pl.dummy()).non_static_var.non_static_call();
	(new pl.dummy()).non_static_var.static_var;
	(new pl.dummy()).non_static_var.non_static_var;

	(new pl.pl2.dummy()).static_var;
	(new pl.pl2.dummy()).static_var.static_call();
	(new pl.pl2.dummy()).static_var.non_static_call();
	(new pl.pl2.dummy()).static_var.static_var;
	(new pl.pl2.dummy()).static_var.non_static_var;
	(new pl.pl2.dummy()).non_static_var;
	(new pl.pl2.dummy()).non_static_var.static_call();
	(new pl.pl2.dummy()).non_static_var.non_static_call();
	(new pl.pl2.dummy()).non_static_var.static_var;
	(new pl.pl2.dummy()).non_static_var.non_static_var;

	global_var;
	global_var.static_call();
	global_var.non_static_call();
	global_var.static_var;
	global_var.non_static_var;

	pl.global_var;
	pl.global_var.static_call();
	pl.global_var.non_static_call();
	pl.global_var.static_var;
	pl.global_var.non_static_var;

	pl.pl2.global_var;
	pl.pl2.global_var.static_call();
	pl.pl2.global_var.non_static_call();
	pl.pl2.global_var.static_var;
	pl.pl2.global_var.non_static_var;
}

