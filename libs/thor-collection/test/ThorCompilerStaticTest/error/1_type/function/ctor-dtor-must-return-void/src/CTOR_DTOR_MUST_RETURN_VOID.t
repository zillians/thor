/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
//normal constructor
class X1
{
    public function new(): void {}
}

class X2
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CTOR_DTOR_MUST_RETURN_VOID", parameters={ name="constructor" } } }
    public function new(): int32 { return 0; }
}

class X3
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CTOR_DTOR_MUST_RETURN_VOID", parameters={ name="constructor" } } }
    public function new(): X3 { return null; }
}

//normal destructor
class X4
{
    public function delete(): void {}
}

class X5
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CTOR_DTOR_MUST_RETURN_VOID", parameters={ name="destructor" } } }
    public function delete(): int32 { return 0; }
}

class X6
{
    @static_test { expect_message={ level="LEVEL_ERROR", id="CTOR_DTOR_MUST_RETURN_VOID", parameters={ name="destructor" } } }
    public function delete(): X6 { return null; }
}
