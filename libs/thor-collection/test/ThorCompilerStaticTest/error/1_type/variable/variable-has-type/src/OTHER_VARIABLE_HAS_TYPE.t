/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
enum the_enum {
    enum_value_1,
    enum_value_2
}

function function_2( arg1: int32, arg2: dummy_1) {
    var   local_1: int32   = 1234;
    var   local_2          = 4321;
    var   local_3: dummy_1 = new dummy_1;
    var   local_4          = new dummy_2;

    const local_1: int32   = 1234;
    const local_2          = 4321;
    const local_3: dummy_1 = new dummy_1;
    const local_4          = new dummy_2;
}

