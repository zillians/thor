/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class cls1              {}
class cls2 extends cls1 {}

function test(
    b  : bool
,   i8 : int8
,   i16: int16
,   i32: int32
,   i64: int64
,   f32: float32
,   f64: float64
,   c1 : cls1
,   c2 : cls2
//,   f1 : function(): void
//,   f2 : function(int8): void
//,   f3 : function(int8,int32): void
): void
{
                                                                                              true ? i8  : b  ;
                                                                                              true ? i8  : i8 ;
                                                                                              true ? i8  : i16;
                                                                                              true ? i8  : i32;
                                                                                              true ? i8  : i64;
                                                                                              true ? i8  : f32;
                                                                                              true ? i8  : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i8  : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i8  : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i8  : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i8  : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i8  : f3 ;

                                                                                              true ? i16 : b  ;
                                                                                              true ? i16 : i8 ;
                                                                                              true ? i16 : i16;
                                                                                              true ? i16 : i32;
                                                                                              true ? i16 : i64;
                                                                                              true ? i16 : f32;
                                                                                              true ? i16 : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i16 : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i16 : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i16 : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i16 : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i16 : f3 ;

                                                                                              true ? i32 : b  ;
                                                                                              true ? i32 : i8 ;
                                                                                              true ? i32 : i16;
                                                                                              true ? i32 : i32;
                                                                                              true ? i32 : i64;
                                                                                              true ? i32 : f32;
                                                                                              true ? i32 : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i32 : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i32 : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i32 : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i32 : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i32 : f3 ;

                                                                                              true ? i64 : b  ;
                                                                                              true ? i64 : i8 ;
                                                                                              true ? i64 : i16;
                                                                                              true ? i64 : i32;
                                                                                              true ? i64 : i64;
                                                                                              true ? i64 : f32;
                                                                                              true ? i64 : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i64 : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i64 : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i64 : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i64 : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? i64 : f3 ;

                                                                                              true ? f32 : b  ;
                                                                                              true ? f32 : i8 ;
                                                                                              true ? f32 : i16;
                                                                                              true ? f32 : i32;
                                                                                              true ? f32 : i64;
                                                                                              true ? f32 : f32;
                                                                                              true ? f32 : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f32 : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f32 : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f32 : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f32 : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f32 : f3 ;

                                                                                              true ? f64 : b  ;
                                                                                              true ? f64 : i8 ;
                                                                                              true ? f64 : i16;
                                                                                              true ? f64 : i32;
                                                                                              true ? f64 : i64;
                                                                                              true ? f64 : f32;
                                                                                              true ? f64 : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f64 : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f64 : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f64 : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f64 : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f64 : f3 ;

    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : b  ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : i8 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : i16;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : i32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : i64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : f32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : f64;
                                                                                              true ? c1  : c1 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c1  : f3 ;
                                                                                             
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : b  ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : i8 ;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : i16;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : i32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : i64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : f32;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : f64;
    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : c1 ;
                                                                                              true ? c2  : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? c2  : f3 ;

//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : b  ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : i8 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : i16;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : i32;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : i64;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : f32;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : f64;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : c1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : c2 ;
//                                                                                              true ? f1  : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f1  : f3 ;
//
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : b  ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : i8 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : i16;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : i32;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : i64;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : f32;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : f64;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : c1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : f1 ;
//                                                                                              true ? f2  : f2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f2  : f3 ;
//
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : b  ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : i8 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : i16;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : i32;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : i64;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : f32;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : f64;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : c1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : c2 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : f1 ;
//    @static_test { expect_message={ level="LEVEL_ERROR", id="TERNARY_EXPRESSION_MISMATCH" } } true ? f3  : f2 ;
//                                                                                              true ? f3  : f3 ;
}

