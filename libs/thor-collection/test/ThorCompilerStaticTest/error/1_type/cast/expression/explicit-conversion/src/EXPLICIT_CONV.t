/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface iface1 {}
interface iface2 {}

class     cls1                  {}
class     cls2   extends iface1 {}

function for_test(): void
{
    var i8 : int8  = 0;
    var i32: int32 = 0;

    var f1: function(): void;
    var f2: function(): int8;

    var i1: iface1 = null;
    var i2: iface2 = null;
    var c1: cls1   = null;
    var c2: cls2   = null;

                                                                                                                                                                  cast<int8            >(i8);
                                                                                                                                                                  cast<int32           >(i8);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "int8"            } } } cast<function(): void>(i8);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "int8"            } } } cast<function(): int8>(i8);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface1"   , rhs_type = "int8"            } } } cast<iface1          >(i8);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface2"   , rhs_type = "int8"            } } } cast<iface2          >(i8);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls1"     , rhs_type = "int8"            } } } cast<cls1            >(i8);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls2"     , rhs_type = "int8"            } } } cast<cls2            >(i8);

                                                                                                                                                                  cast<int8            >(i32);
                                                                                                                                                                  cast<int32           >(i32);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "int32"           } } } cast<function(): void>(i32);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "int32"           } } } cast<function(): int8>(i32);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface1"   , rhs_type = "int32"           } } } cast<iface1          >(i32);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface2"   , rhs_type = "int32"           } } } cast<iface2          >(i32);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls1"     , rhs_type = "int32"           } } } cast<cls1            >(i32);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls2"     , rhs_type = "int32"           } } } cast<cls2            >(i32);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int8"           , rhs_type = "function():void" } } } cast<int8            >(f1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int32"          , rhs_type = "function():void" } } } cast<int32           >(f1);
                                                                                                                                                                  cast<function(): void>(f1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "function():void" } } } cast<function(): int8>(f1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface1"   , rhs_type = "function():void" } } } cast<iface1          >(f1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface2"   , rhs_type = "function():void" } } } cast<iface2          >(f1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls1"     , rhs_type = "function():void" } } } cast<cls1            >(f1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls2"     , rhs_type = "function():void" } } } cast<cls2            >(f1);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int8"           , rhs_type = "function():int8" } } } cast<int8            >(f2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int32"          , rhs_type = "function():int8" } } } cast<int32           >(f2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "function():int8" } } } cast<function(): void>(f2);
                                                                                                                                                                  cast<function(): int8>(f2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface1"   , rhs_type = "function():int8" } } } cast<iface1          >(f2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class iface2"   , rhs_type = "function():int8" } } } cast<iface2          >(f2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls1"     , rhs_type = "function():int8" } } } cast<cls1            >(f2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "class cls2"     , rhs_type = "function():int8" } } } cast<cls2            >(f2);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int8"           , rhs_type = "class iface1"    } } } cast<int8            >(i1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int32"          , rhs_type = "class iface1"    } } } cast<int32           >(i1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "class iface1"    } } } cast<function(): void>(i1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "class iface1"    } } } cast<function(): int8>(i1);
                                                                                                                                                                  cast<iface1          >(i1);
                                                                                                                                                                  cast<iface2          >(i1);
                                                                                                                                                                  cast<cls1            >(i1);
                                                                                                                                                                  cast<cls2            >(i1);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int8"           , rhs_type = "class iface2"    } } } cast<int8            >(i2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int32"          , rhs_type = "class iface2"    } } } cast<int32           >(i2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "class iface2"    } } } cast<function(): void>(i2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "class iface2"    } } } cast<function(): int8>(i2);
                                                                                                                                                                  cast<iface1          >(i2);
                                                                                                                                                                  cast<iface2          >(i2);
                                                                                                                                                                  cast<cls1            >(i2);
                                                                                                                                                                  cast<cls2            >(i2);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int8"           , rhs_type = "class cls1"      } } } cast<int8            >(c1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int32"          , rhs_type = "class cls1"      } } } cast<int32           >(c1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "class cls1"      } } } cast<function(): void>(c1);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "class cls1"      } } } cast<function(): int8>(c1);
                                                                                                                                                                  cast<iface1          >(c1);
                                                                                                                                                                  cast<iface2          >(c1);
                                                                                                                                                                  cast<cls1            >(c1);
                                                                                                                                                                  cast<cls2            >(c1);

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int8"           , rhs_type = "class cls2"      } } } cast<int8            >(c2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "int32"          , rhs_type = "class cls2"      } } } cast<int32           >(c2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():void", rhs_type = "class cls2"      } } } cast<function(): void>(c2);
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "INVALID_CONV", parameters = { lhs_type = "function():int8", rhs_type = "class cls2"      } } } cast<function(): int8>(c2);
                                                                                                                                                                  cast<iface1          >(c2);
                                                                                                                                                                  cast<iface2          >(c2);
                                                                                                                                                                  cast<cls1            >(c2);
                                                                                                                                                                  cast<cls2            >(c2);
}

