/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
class cls
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_function                   (): void {}
                                                                                                              @entry public    static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_template_function_2<T1, T2>(): void {}
}

class cls<S1>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_template_function_2<T1, T2>(): void {}
}

class cls<S1, S2>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public    static task static_member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected static task static_member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_function                   (): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private          task        member_template_function_2<T1, T2>(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_template_function_1<T1    >(): void {}
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry private   static task static_member_template_function_2<T1, T2>(): void {}
}

interface iface
{
    // interface disallow the following:
    // - 'private' function
    // - template function
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry public           task        member_function                   (): void;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "ENTRY_MUST_BE_GLOBAL_OR_STATIC_MEMBER" } } @entry protected        task        member_function                   (): void;
}

