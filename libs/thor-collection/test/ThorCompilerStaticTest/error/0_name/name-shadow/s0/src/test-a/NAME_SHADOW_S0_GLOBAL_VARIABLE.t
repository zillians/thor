/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// test shadowing on global variable

// 1.global variable + enumeration variable
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_1: int32 = 0;
enum test_value_1
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1"} } }
    value_1
}

// 2.global variable + clsss template parameter
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_2: int32 = 0;
class test_value_2<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2"} } }
    value_2
> {}

// 3.global variable + class member variable
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_3: int32 = 0;
class test_value_3
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3"} } }
    var value_3: int32 = 0;
}

// 4.global variable + function template parameter
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_4: int32 = 0;
function test_value_4<
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_4"} } }
    value_4
>(): void {}

// 5.global variable + function parameter
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_5: int32 = 0;
function test_value_5(
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_5"} } }
    value_5: int32
): void {}

// 6.global variable + member function template parameter
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_6: in32 = 0;
class test_value_6_a
{
    function test_value_6_a<
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_6"} } }
        value_6
    >(): void {}
}
class test_value_6_b<X>
{
    function test_value_6_b<
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_6"} } }
        value_6
    >(): void {}
}

// 7.global variable + member function parameter
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
var value_7: in32 = 0;
class test_value_7_a
{
    function test_value_7_a(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_7"} } }
        value_7: int32
    ): void {}
}
class test_value_7_b<X>
{
    function test_value_7_b(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_7"} } }
        value_7: int32
    ): void {}
}

// 8.global variable + variable on block
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} } var value_8_a: int32 = 0;
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} } var value_8_b: int32 = 0;
@static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} } var value_8_c: int32 = 0;
function test_value_8(): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_8_a"} } }
    var value_8_a: int32 = 0;

    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_8_b"} } }
        var value_8_b: int32 = 0;
    }

    for(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_8_c"} } }
        var value_8_c: int32 = 0; ;
    );
}
