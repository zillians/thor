/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// test shadowing on member variable

// 1.member variable + function template parameter
class test_value_1_a<T>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1: int32 = 0;

    function test_value_1_a<
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1"} } }
        value_1
    >(): void {}
}

class test_value_1_b
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_1: int32 = 0;

    function test_value_1_b<
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1"} } }
        value_1
    >(): void {}
}

// 2.member variable + function parameter
class test_value_2_a<T>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_2_a_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_2_a_b: int32 = 0;

    function test_value_2_a_a<U>(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2_a_a"} } }
        value_2_a_a: int32
    ): void {}

    function test_value_2_a_b(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2_a_b"} } }
        value_2_a_b: int32
    ): void {}
}

class test_value_2_b
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_2_b_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_2_b_b: int32 = 0;

    function test_value_2_b_a<U>(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2_b_a"} } }
        value_2_b_a: int32
    ): void {}

    function test_value_2_b_b(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_2_b_b"} } }
        value_2_b_b: int32
    ): void {}
}

// 3.member variable + local variables
class test_value_3_a<T>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_a_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_a_b: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_a_c: int32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_b_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_b_b: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_a_b_c: int32 = 0;

    function test_value_3_a_a<U>(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_a_a"} } }
        var value_3_a_a_a: int32 = 0;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_a_b"} } }
            var value_3_a_a_b: int32 = 0;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_a_c"} } }
            var value_3_a_a_c: int32 = 0; ;
        );
    }

    function test_value_3_a_b(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_b_a"} } }
        var value_3_a_b_a: int32 = 0;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_b_b"} } }
            var value_3_a_b_b: int32 = 0;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_a_b_c"} } }
            var value_3_a_b_c: int32 = 0; ;
        );
    }
}

class test_value_3_b<T>
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_a_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_a_b: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_a_c: int32 = 0;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_b_a: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_b_b: int32 = 0;
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    var value_3_b_b_c: int32 = 0;

    function test_value_3_a_a<U>(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_b_a_a"} } }
        var value_3_b_a_a: int32 = 0;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_b_a_b"} } }
            var value_3_b_a_b: int32 = 0;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_b_a_c"} } }
            var value_3_b_a_c: int32 = 0; ;
        );
    }

    function test_value_3_a_b(): void
    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_b_b_a"} } }
        var value_3_b_b_a: int32 = 0;

        {
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_b_b_b"} } }
            var value_3_b_b_b: int32 = 0;
        }

        for(
            @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_3_b_b_c"} } }
            var value_3_b_b_c: int32 = 0; ;
        );
    }
}
