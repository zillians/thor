/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
// test shadowing on parameter

// 1.parameter + local variables
function test_value_1_a<T>(
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1_a: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1_b: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1_c: int32
): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a"} } }
    var value_1_a: int32 = 0;

    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b"} } }
        var value_1_b: int32 = 0;
    }

    for(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_c"} } }
        var value_1_c: int32 = 0; ;
    );
}

function test_value_1_b(
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1_a: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1_b: int32,
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE_PREV"} }
    value_1_c: int32
): void
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_a"} } }
    var value_1_a: int32 = 0;

    {
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_b"} } }
        var value_1_b: int32 = 0;
    }

    for(
        @static_test { expect_message = { level = "LEVEL_ERROR", id = "SHADOW_DECLARE", parameters = { id = "value_1_c"} } }
        var value_1_c: int32 = 0; ;
    );
}
