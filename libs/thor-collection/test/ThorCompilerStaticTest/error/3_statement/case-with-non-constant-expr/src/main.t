/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
enum e {VALUE}
class dummy {}
function for_test(): int32 { return 0;}

function test_literal()
{
    switch (1)
    {
    case    true:
    case   false: // actually 0
    case      10:
    case      11:
    }
}

function test_simple_const_expr()
{
    switch (1)
    {
    case 1 + 2    :
    case 1 - 2    :
    case 1 + 2 * 2:
    case 1 - 2 / 2:
    }
}

function test_simple_non_const_expr()
{
    var v = 1;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    switch (1)
    {
    case v + 2    :
    case v - 2    :
    case v + 2 * 2:
    case v - 2 / 2:
    }
}

function test_cast_const_expr()
{
    switch (1)
    {
    case cast<int8 >(      1)     :
    case cast<int16>(      2)     :
    case cast<int32>(      3) +  1:
    case cast<int32>(e.VALUE) + 10:
    }
}

function test_cast_non_const_expr()
{
    var v = 1;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    switch (1)
    {
    case cast<int8 >(v):
    case cast<int16>(v):
    case cast<int32>(v) + 1:
    }
}

function test_isa_const_expr()
{
    switch (1)
    {
    case isa<int32>(      1) +  10:
    case isa<int32>(e.VALUE) +  20:

    case isa<e    >(      1) + 110:
    case isa<e    >(e.VALUE) + 120:

    case isa<dummy>(      1) + 210:
    case isa<dummy>(e.VALUE) + 220:
    }
}

function test_isa_non_const_expr()
{
    var vi32: int32 =       1;
    var vd  : dummy =    null;
    var ve  : e     = e.VALUE;

    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    switch (1)
    {
    case isa<int32>(vi32):
    case isa<dummy>(vi32):
    case isa<e    >(vi32):
    case isa<int32>(vd  ):
    case isa<dummy>(vd  ):
    case isa<e    >(vd  ):
    case isa<int32>(ve  ):
    case isa<dummy>(ve  ):
    case isa<e    >(ve  ):
    }
}

function test_call_non_const_expr()
{
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    @static_test { expect_message = { level = "LEVEL_ERROR", id = "CASE_WITH_NON_CONSTANT_VALUE" } }
    switch (1)
    {
    case for_test():
    case for_test() + 1:
    }
}
