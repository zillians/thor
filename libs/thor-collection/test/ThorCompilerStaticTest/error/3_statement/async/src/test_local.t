/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
task test_local()
{
    var         local_i08: int8    = 1;
    var         local_i16: int16   = 2;
    var         local_i32: int32   = 3;
    var         local_i64: int64   = 4;
    var         local_f32: float32 = 1.0;
    var         local_f64: float64 = 2.0;
    var         local_obj: Object  = null;

    const const_local_i08: int8    = 1;
    const const_local_i16: int16   = 2;
    const const_local_i32: int32   = 3;
    const const_local_i64: int64   = 4;
    const const_local_f32: float32 = 1.0;
    const const_local_f64: float64 = 2.0;
    const const_local_obj: Object  = null;

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_i08 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_i16 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_i32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_i64 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_f32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_f64 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"}}
              local_obj = get_obj();

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_i08"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_i08 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_i16"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_i16 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_i32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_i32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_i64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_i64 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_f32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_f32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_f64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_f64 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_local_obj"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                             }}
        const_local_obj = get_obj();

    unused(      local_i08);
    unused(      local_i16);
    unused(      local_i32);
    unused(      local_i64);
    unused(      local_f32);
    unused(      local_f64);
    unused(      local_obj);
                          
    unused(const_local_i08);
    unused(const_local_i16);
    unused(const_local_i32);
    unused(const_local_i64);
    unused(const_local_f32);
    unused(const_local_f64);
    unused(const_local_obj);
}
