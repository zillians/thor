/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
task test_global()
{
    async -> global_i08 = get_i08();
    async -> global_i16 = get_i16();
    async -> global_i32 = get_i32();
    async -> global_i64 = get_i64();
    async -> global_f32 = get_f32();
    async -> global_f64 = get_f64();

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_i08"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_i08 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_i16"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_i16 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_i32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_i32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_i64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_i64 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_f32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_f32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_f64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_f64 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_global_obj"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
        const_global_obj = get_obj();
}
