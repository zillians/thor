/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
var         global_i08: int8    = 1;
var         global_i16: int16   = 2;
var         global_i32: int32   = 3;
var         global_i64: int64   = 4;
var         global_f32: float32 = 1.0;
var         global_f64: float64 = 2.0;
var         global_obj: Object  = null;
var         global_dum: Dummy   = null;

const const_global_i08: int8    = 1;
const const_global_i16: int16   = 2;
const const_global_i32: int32   = 3;
const const_global_i64: int64   = 4;
const const_global_f32: float32 = 1.0;
const const_global_f64: float64 = 2.0;
const const_global_obj: Object  = null;

class holder_public
{
    public var         member_i08: int8    = 1;
    public var         member_i16: int16   = 2;
    public var         member_i32: int32   = 3;
    public var         member_i64: int64   = 4;
    public var         member_f32: float32 = 1.0;
    public var         member_f64: float64 = 2.0;
    public var         member_obj: Object  = null;
}

class holder_public_const
{
    public const const_member_i08: int8    = 1;
    public const const_member_i16: int16   = 2;
    public const const_member_i32: int32   = 3;
    public const const_member_i64: int64   = 4;
    public const const_member_f32: float32 = 1.0;
    public const const_member_f64: float64 = 2.0;
    public const const_member_obj: Object  = null;
}

class holder_public_static
{
    public static var         member_i08: int8    = 1;
    public static var         member_i16: int16   = 2;
    public static var         member_i32: int32   = 3;
    public static var         member_i64: int64   = 4;
    public static var         member_f32: float32 = 1.0;
    public static var         member_f64: float64 = 2.0;
    public static var         member_obj: Object  = null;
}

class holder_public_static_const
{
    public static const const_member_i08: int8    = 1;
    public static const const_member_i16: int16   = 2;
    public static const const_member_i32: int32   = 3;
    public static const const_member_i64: int64   = 4;
    public static const const_member_f32: float32 = 1.0;
    public static const const_member_f64: float64 = 2.0;
    public static const const_member_obj: Object  = null;
}
