/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function test_static_member()
{
    async -> holder_public_static.member_i08 = get_i08();
    async -> holder_public_static.member_i16 = get_i16();
    async -> holder_public_static.member_i32 = get_i32();
    async -> holder_public_static.member_i64 = get_i64();
    async -> holder_public_static.member_f32 = get_f32();
    async -> holder_public_static.member_f64 = get_f64();

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i08"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_i08 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i16"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_i16 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_i32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_i64 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_f32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_f32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_f64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_f64 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_obj"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       holder_public_static_const.const_member_obj = get_obj();
}

function test_normal_member(hp_: holder_public, hpc: holder_public_const)
{
    async -> hp_.member_i08 = get_i08();
    async -> hp_.member_i16 = get_i16();
    async -> hp_.member_i32 = get_i32();
    async -> hp_.member_i64 = get_i64();
    async -> hp_.member_f32 = get_f32();
    async -> hp_.member_f64 = get_f64();

    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i08"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_i08 = get_i08();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i16"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_i16 = get_i16();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_i32 = get_i32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_i64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_i64 = get_i64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_f32"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_f32 = get_f32();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_f64"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_f64 = get_f64();
    async ->
        @static_test{expect_message={level = "LEVEL_ERROR", id = "WRITE_CONST"                      , parameters = { form="assignment", var_id="const_member_obj"}}}
        @static_test{expect_message={level = "LEVEL_ERROR", id = "INVALID_IMPLCIT_ASYNC_INVALID_LHS"                                                              }}
       hpc.const_member_obj = get_obj();
}
