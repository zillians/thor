/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
import .= thor.lang;

interface I1 {
    public function do_test(): void;

    // I1::do_test is virtual

    // pure-virtual from current
}

interface I2 extends I1 {
    public function do_test(): void;

    // I1::do_test is virtual
    // I2::do_test is virtual

    // pure-virtual from current
    // pure-virtual from parent
}

interface I3 extends I1 {
    // I1::do_test is virtual

    // pure-virtual from parent
}

interface I4 extends I3 {
    // I1::do_test is virtual

    // pure-virtual from parent
}

interface I5 extends I2 {
    // I1::do_test is virtual
    // I2::do_test is virtual

    // pure-virtual from parent
}

interface I6 {
    public function do_test(): void;
}

class C1 implements I1 {
    // I1::do_test is virtual

    // pure-virtual from parent
}

class C2 extends C1 implements I6 {
    // I1::do_test is virtual
    // I2::do_test is virtual

    // pure-virtual from parent
}

// non-abstract
class C3 implements I1 {
    public function do_test(): void {}
}

class C4 extends C3 implements I6 {
    // I2::do_test is virtual

    // pure-virtual from parent
}

// non-abstract
class C5 extends C3 implements I6 {
    public function do_test(): void {}
}

class C6 extends C4 {
    // I2::do_test is virtual

    // pure-virtual from parent
}

// non-abstract
class C7 extends C5 {
}

function test(): void {

    @static_test {
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS",
            parameters = {
                name = "C1"
            }
        },
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS_VIRTUAL",
            parameters = {
                name = "do_test",
                origin_class = "I1"
            }
        }
    }
    new C1(); // error


    @static_test {
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS",
            parameters = {
                name = "C2"
            }
        },
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS_VIRTUAL",
            parameters = {
                name = "do_test",
                origin_class = "I1"
            }
        },
         expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS_VIRTUAL",
            parameters = {
                name = "do_test",
                origin_class = "I6"
            }
        }

    }
    new C2(); // error

    new C3();

    @static_test {
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS",
            parameters = {
                name = "C4"
            }
        },
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS_VIRTUAL",
            parameters = {
                name = "do_test",
                origin_class = "I6"
            }
        }
    }
    new C4(); // errpr

    new C5();

    @static_test {
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS",
            parameters = {
                name = "C6"
            }
        },
        expect_message = {
            level      = "LEVEL_ERROR",
            id         = "INSTANTIATE_ABSTRACT_CLASS_VIRTUAL",
            parameters = {
                name = "do_test",
                origin_class = "I6"
            }
        }
    }
    new C6(); // error

    new C7();
}

