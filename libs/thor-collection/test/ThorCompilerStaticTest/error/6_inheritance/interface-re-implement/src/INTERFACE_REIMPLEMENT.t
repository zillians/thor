/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
interface I1 {}
interface I2 {}
interface I3 extends I1, I2 {}

@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I1", class_type="I4" } } }
interface I4 extends I1, I3 {}

@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I1", class_type="I5" } } }
interface I5 extends I3, I1 {}

@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I1", class_type="I6" } } }
@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I2", class_type="I6" } } }
@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I3", class_type="I6" } } }
interface I6 extends I3, I4 {}

@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I1", class_type="I7" } } }
@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I2", class_type="I7" } } }
@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I3", class_type="I7" } } }
interface I7 extends I4, I5 {}

@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I1", class_type="I8" } } }
@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I2", class_type="I8" } } }
@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I3", class_type="I8" } } }
interface I8 extends I5, I4 {}

@static_test{ expect_message={ level="LEVEL_ERROR", id="INTERFACE_REIMPLEMENT", parameters={ interface_type="I1", class_type="I9" } } }
interface I9 extends I1, I1 {}

