/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "I_1"
        }
    }
}
interface I_1 extends I_3 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "I_2"
        }
    }
}
interface I_2 extends I_1 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "I_3"
        }
    }
}
interface I_3 extends I_2 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "I_4"
        }
    }
}
interface I_4 extends I_5 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "I_5"
        }
    }
}
interface I_5 extends I_4 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "I_6"
        }
    }
}
interface I_6 extends I_6 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "C_1"
        }
    }
}
class C_1 extends C_3 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "C_2"
        }
    }
}
class C_2 extends C_1 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "C_3"
        }
    }
}
class C_3 extends C_2 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "C_4"
        }
    }
}
class C_4 extends C_5 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "C_5"
        }
    }
}
class C_5 extends C_4 {}

@static_test {
    expect_message = {
        level      = "LEVEL_ERROR",
        id         = "CYCLIC_INHERITANCE",
        parameters = {
            class_type = "C_6"
        }
    }
}
class C_6 extends C_6 {}
