/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
function g(a:int32):void {}

function f():void
{
    var x:int32;

    var y:int32;
    @static_test { expect_message={ level="LEVEL_WARNING", id="UNINIT_REF", parameters={ var_id="x" } } }
    y = x;

    @static_test { expect_message={ level="LEVEL_WARNING", id="UNINIT_REF", parameters={ var_id="x" } } }
    var z:int32 = x;

    @static_test { expect_message={ level="LEVEL_WARNING", id="UNINIT_REF", parameters={ var_id="x" } } }
    g(x);

    print(y);
    print(z);
}
