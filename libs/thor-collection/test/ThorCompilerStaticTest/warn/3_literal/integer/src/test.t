/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

function main(): int32
{
    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_OVERFLOW" } }
    11111111111111111111111111111111111111111111111111111L;

    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_OVERFLOW" } }
    0x10000000000000000;

    //The following literal should both not overflow and no parsing error//////
    0xFFFFFFFF;
    0xFFFFFFFFFFFF;
    -2147483648;
    31092839021839128;
    ///////////////////////////

    @static_test { expect_message={ level="LEVEL_WARNING", id="NUMERIC_LITERAL_OVERFLOW" } }
    9223372036854775808;
    return 0;
}
