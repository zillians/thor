/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cinttypes>
#include <iostream>
#include "thor/lang/Language.h"
using namespace thor::lang;

////////////////////////
// Utility
void assertion(std::int32_t error)
{
    std::cerr << "Error code: " << error << std::endl;
    throw error;
}

////////////////////////
// Class Declaration
class Empty : public Object
{
public:
    Empty();
};

class B : public Object
{
public:
    B();
    virtual std::int32_t foo();
    std::int32_t vb;
};

class C
{
public:
    C();
    virtual std::int32_t hello();
    virtual void set(std::int64_t);
    virtual std::int64_t get();
};

class A : public B, public C
{
public:
    A();
    virtual std::int32_t foo();
    virtual std::int32_t hello();
    virtual void set(std::int64_t);
    virtual std::int64_t get();
    std::int64_t va;
};

class Special : public Empty, public C
{
public:
    Special();
    virtual std::int32_t foo();
    virtual std::int32_t hello();
};

///////////////////////////////////////
// Test for covariant return type
class CI
{
public:
    CI();
    virtual C* get() = 0;
};

class CB : public Object
{
public:
    CB();
    virtual void dummy();
    std::int32_t dummy_var;
};

class CD : public CB, public CI
{
public:
    CD();
    void set_source(A* a);
    virtual A* get();
    A* class_AA;
};

/////////////////////////////////////
// Test for retrieving correct virtual table
class Animal : public Object  
{
public:
    Animal();
    virtual void yelp();
    virtual void run();
    std::int32_t v;
};

class Dog : public Animal
{
};

////////////////////////
// Testing
extern void thor_test1(B* b);
std::int32_t test1()
{
    B* b = new A();
    thor_test1(b);
    if (b->foo() != 2) assertion(2);
//    delete b;
	exit(0);
	return -1;
}

extern void thor_test2(Special* a);
std::int32_t test2()
{
    Special* a = new Special();
    thor_test2(a);
    if (a->foo() != 1) assertion(3);
    if (a->hello() != 2) assertion(4);
//    delete a;
	exit(0);
	return -1;
}

extern void thor_test3(C* c);
std::int32_t test3()
{
    C* c = new A();
    thor_test3(c);
    c->set(2888822222);
    if (c->get() != 2888822222) assertion(2);
//    delete c;
	exit(0);
	return -1;
}

extern void thor_test4(CI* i);
std::int32_t test4()
{
    CD* cd = new CD();
    A* a = new A();

    cd->set_source(a);
    
    a->set(1234567890);

    CI* i = static_cast<CI*>(cd);
    thor_test4(i);
    
    C* c = i->get();
    if (c->get() != 1234567890) assertion(2);

//    delete cd;
//    delete a;
	exit(0);
	return -1;
}

extern void thor_test5(Dog* d);
std::int32_t test5()
{
    Dog * d = new Dog();
    thor_test5(d);
    d->yelp();
    if (d->v != 100) assertion(3);
    d->run();
    if (d->v != 10) assertion(4);

//    delete d;
	exit(0);
	return -1;
}

class B6
{
public:
	virtual ~B6() {}
	virtual void hi() {}
	int z;
};

