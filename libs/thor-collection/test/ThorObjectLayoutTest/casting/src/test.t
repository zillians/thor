/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
@native
function assertion(e : int32) : void;

//////////////////////////
// For test 1
class A extends B
{
    // add a dummy function to have a vptr in object layout
    public virtual function dummy() : void {}
}

class B 
{
    public var vb : int32;
}

/////////////////////////
// Test functions
function thor_test1(a : A) : void
{
    a.vb = 12345;
    var b : B = a;

    if (b.vb != 12345) assertion(1);

    b.vb = 54321;
}

function thor_test2(b : B) : void
{
//    var a : A = b;
//    
//    a.vb = 989898;
//    if (b.vb != 989898) assertion(1);
//
//    b.vb = 112233;
}

@native
@entry
task test1() : int32;

@native
@entry
task test2() : int32;
