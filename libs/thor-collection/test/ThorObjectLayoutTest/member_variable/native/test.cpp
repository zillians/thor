/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */
#include <cinttypes>
#include <iostream>
#include "thor/lang/Language.h"
using namespace thor::lang;

////////////////////////
// Utility
void assertion(std::int32_t error)
{
    std::cerr << "Error code: " << error << std::endl;
    throw error;
}

////////////////////////
// Class Declaration
class B : public Object
{
public:
    B();
    std::int32_t vb1;
    std::int64_t vb2;
};

class A : public B
{
public:
    A();
    std::int32_t add(std::int32_t v);
    std::int64_t  factorial(std::int64_t v);
    std::int64_t fact;
    std::int32_t sum;
};


class VA : public B
{
public:
    VA();
    virtual void dummy();
    std::int32_t v;
};


class C : public Object
{
public:
    C();
    std::int32_t v;
};

class D : public C
{
public:
    D();
    std::int32_t vd;
};

class F : public Object
{
public:
    F();
    std::int64_t value;
};

class E : public Object
{
public:
    void set(F* f);
    E();
    F* get();
    
    std::int32_t value;
    F* member_f;
};

class WithStatic : public Object
{
public:
	WithStatic();
	static int value;
	int access_me;
};

////////////////////////
// Testing
extern void thor_test1(A* a);
std::int32_t test1()
{
    A* a = new A();
    thor_test1(a);

    a->vb1 = 2147483647;
    if (a->vb1 != 2147483647) assertion(5);
    a->vb2 = 2305843009213693951;
    if (a->vb2 != 2305843009213693951) assertion(6);
    
    a->add(46);
    a->factorial(20);
    if (a->fact != 2432902008176640000) assertion(7);
    if (a->sum != 2147483647) assertion(8);

    delete a;
	exit(0);
	return -1;
}

extern void thor_test2(VA* a);
std::int32_t test2()
{
    // This test case mere test whether the object layout compatible with gcc
    VA* a = new VA();
    
    thor_test2(a);
    if (a->vb1 != 2147483647) assertion(4);
    if (a->vb2 != 2305843009213693951) assertion(5);
    if (a->v != 1234576) assertion(6);
    
    delete a;
	exit(0);
	return -1;
}

extern void thor_test3(D* d);
std::int32_t test3()
{
    D* d = new D();
    
    thor_test3(d);
    if (d->v != 1992) assertion(3);
    if (d->vd != 2002) assertion(4);
	exit(0);
	return -1;
}


extern void thor_test4(E* e);
std::int32_t test4()
{
    E* e = new E();
    F* f = new F();
    e->set(f);

    thor_test4(e);
    
    if (e->value != 2147483647) assertion(1);
    if (e->member_f->value != 2305843009213693951) assertion(2);
    if (e->get()->value != 2305843009213693951) assertion(3);
	exit(0);
	return -1;
}

extern void thor_test5(WithStatic*);
std::int32_t test5()
{
	WithStatic* b = new WithStatic();
	if (b->access_me != 1234) assertion(2);
	exit(0);
	return -1;
}

