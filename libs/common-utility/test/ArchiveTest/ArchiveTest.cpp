/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdio>
#include <cstdlib>
#include <cstdlib>

#include <fstream>
#include <iostream>
#include <unordered_set>

#include <boost/type_traits.hpp>
#include <boost/mpl/if.hpp>
#include <boost/mpl/bool.hpp>
#include <boost/filesystem.hpp>
#define BOOST_TEST_MODULE ArchiveTest
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

#include "utility/archive/Archive.h"
#include "utility/UUIDUtil.h"

using namespace zillians;

BOOST_AUTO_TEST_SUITE( ArchiveTestSuit )

BOOST_AUTO_TEST_CASE( Archive_Test )
{
    // Random create several source files
    const int generated_file_count = 4;
    std::vector< boost::filesystem::path > sources;

    for (int i = 0; i < generated_file_count; i++)
    {
        // Create the file name with the random content
        UUID source_filename = UUID::random();
        boost::filesystem::path source_filepath = operator/(boost::filesystem::path("/tmp"), source_filename.toString());
        std::string command = "dd if=/dev/urandom of=" + source_filepath.generic_string() + " bs=1M count=1";
        system(command.c_str());

        std::cout << "Source file: " << source_filepath << std::endl;
        sources.push_back(source_filepath);
    }

    // Create a random archive name
    UUID archive_name = UUID::random();
    boost::filesystem::path archive_path = operator/(boost::filesystem::path("/tmp"), archive_name.toString() + std::string(".zip"));
    std::cout << "Archive file: " << archive_path << std::endl;

    // Archive the content
    {
        Archive ar(archive_path.generic_string(), ArchiveMode::ARCHIVE_FILE_COMPRESS);

        ar.setCompressLevel(9);
        BOOST_CHECK( ar.open() );
        // Add to archive
        for (int i = 0; i < generated_file_count; i++)
            ar.add(sources[i].generic_string());
        BOOST_CHECK( ar.close() );
    }

    // Extract the content
    std::vector<ArchiveItem> archive_items;
    {
        Archive ar(archive_path.generic_string(), ArchiveMode::ARCHIVE_FILE_DECOMPRESS);

        BOOST_CHECK( ar.open() );
        BOOST_CHECK( ar.extractAll(archive_items) );
        BOOST_CHECK( ar.close() );
    }

    // Check whether we have all the file names and check the content
    int match_count = 0;
    for (int i = 0; i < archive_items.size(); i++)
    {
        for (int j = 0; j < sources.size(); j++)
        {
            if (archive_items[i].filename == sources[i].generic_string())
            {
                match_count++;

                // Read the source buffer
                std::vector<unsigned char> source_buffer;
                std::ifstream source_file(sources[i].generic_string().c_str(), std::ios::in | std::ios::binary | std::ios::ate);
                source_buffer.resize( source_file.tellg() );
                source_file.seekg (0, std::ios::beg);
                source_file.read((char*)&source_buffer[0], source_buffer.size());
                source_file.close();

                // Compare with the extracted buffer
                BOOST_CHECK( archive_items[i].buffer == source_buffer );

                break;
            }
        }
    }

    BOOST_CHECK( match_count == generated_file_count );

    // Finally, remove the dirty files
    std::remove(archive_path.generic_string().c_str());
    for (int i = 0; i < sources.size(); i++)
    {
        std::remove(sources[i].generic_string().c_str());
    }
}

BOOST_AUTO_TEST_SUITE_END()
