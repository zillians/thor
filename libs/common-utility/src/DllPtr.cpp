/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifdef __unix__
#include <dlfcn.h>
#else
#error "not supported platform, please extends for your platform on need"
#endif

#include <cassert>
#include <cstddef>

#include <algorithm>
#include <string>

#include "utility/DllPtr.h"

namespace zillians {

dll_ptr::dll_ptr(const std::string& dll_path)
    : dll_ptr(dll_path.c_str())
{
}

dll_ptr::dll_ptr(const char* dll_path)
    : dll(dlopen(dll_path, RTLD_LAZY | RTLD_GLOBAL))
{
}

dll_ptr::dll_ptr(dll_ptr&& ref) noexcept
    : dll(ref.dll)
{
    ref.dll = nullptr;
}

dll_ptr::~dll_ptr()
{
    unload();
}

void* dll_ptr::load(const std::string& dll_path)
{
    return load(dll_path.c_str());
}

void* dll_ptr::load(const char* dll_path)
{
    assert(dll_path != nullptr && "null pointer exception");

    dll_ptr tmp(dll_path);

    swap(tmp);

    return dll;
}

void dll_ptr::unload()
{
    if (dll != nullptr)
        dlclose(dll);
}

dll_ptr& dll_ptr::operator=(std::nullptr_t)
{
    unload();

    return *this;
}

dll_ptr& dll_ptr::operator=(dll_ptr&& ref)
{
    unload();

        dll = ref.dll;
    ref.dll = nullptr;

    return *this;
}

void* dll_ptr::find_symbol(const char* name) noexcept
{
    return dlsym(dll, name);
}

void* dll_ptr::find_symbol(const std::string& name) noexcept
{
    return dlsym(dll, name.c_str());
}

dll_ptr::operator void*() const noexcept
{
    return dll;
}

void dll_ptr::swap(dll_ptr& ref) noexcept
{
    std::swap(dll, ref.dll);
}

}
