/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <algorithm>
#include <string>
#include <utility>

#include <boost/uuid/nil_generator.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid.hpp>

#include "utility/UUIDUtil.h"

namespace zillians {

UUID::UUID(const boost::uuids::uuid& impl) noexcept
    : boost::uuids::uuid(impl)
{
}

UUID::UUID(boost::uuids::uuid&& impl) noexcept
    : boost::uuids::uuid(std::move(impl))
{
}

UUID UUID::nil() noexcept
{
    return UUID(boost::uuids::nil_uuid());
}

UUID UUID::random()
{
    boost::uuids::random_generator generator;

    return UUID(generator());
}

UUID UUID::parse(const std::string& desc)
{
    boost::uuids::string_generator generator;

    return UUID(generator(desc));
}

std::string UUID::toString() const
{
    return to_string(*this);
}

std::string UUID::toString(const char delim) const
{
    std::string str = toString();

    std::replace(str.begin(), str.end(), '-', delim);

    return str;
}

}
