/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/UnicodeUtil.h"
#include <string.h>

#define UNKNOWN_CHAR '?'

#define BOOST_UTF8_BEGIN_NAMESPACE namespace zillians {
#define BOOST_UTF8_END_NAMESPACE }
#define BOOST_UTF8_DECL

#include <boost/detail/utf8_codecvt_facet.hpp>
#include "utility/detail/boost/utf8_codecvt_facet.cpp"

namespace zillians {

std::locale& get_posix_locale()
{
    static std::locale default_locale("POSIX");
    return default_locale;
}

std::locale& get_c_locale()
{
    static std::locale default_locale("C");
    return default_locale;
}

std::locale& get_utf8_locale()
{
    static utf8_codecvt_facet* utf8_facet = new utf8_codecvt_facet;
    static std::locale utf8_locale(std::locale(), utf8_facet);
    return utf8_locale;
}

std::locale& get_default_locale()
{
    static std::locale default_locale("");
    return default_locale;
}

void utf8_to_ucs4(const std::string& input, std::wstring& output)
{
    typedef boost::u8_to_u32_iterator<std::string::const_iterator> iterator_type;
    iterator_type first(input.begin());
    iterator_type last(input.end());
    std::copy(first, last, std::back_inserter(output));
}

void ucs4_to_utf8(const std::wstring& input, std::string& output)
{
    typedef boost::u32_to_u8_iterator<std::wstring::const_iterator> iterator_type;
    iterator_type first(input.begin());
    iterator_type last(input.end());
    std::copy(first, last, std::back_inserter(output));
}

void wcs_to_cstr(const wchar_t* src, char* dest)
{
    static std::locale ascii_locale;
    std::use_facet<std::ctype<wchar_t> >(ascii_locale).narrow(src, src + wcslen(src), UNKNOWN_CHAR, &dest[0]);
}

void cstr_to_wcs(const char* src, wchar_t* dest)
{
    static std::locale ascii_locale;
    std::use_facet<std::ctype<wchar_t> >(ascii_locale).widen(src, src + strlen(src), &dest[0]);
}

std::wstring s_to_ws(std::string s)
{
    std::wstring ws;
    wchar_t* wcs = new wchar_t[s.length() + 1];
    wcs[s.length()] = L'\0';
    cstr_to_wcs(s.c_str(), wcs);
    ws = wcs;
    delete []wcs;
    return ws;
}

std::string ws_to_s(std::wstring ws)
{
    std::string s;
    char* cstr = new char[ws.length() + 1];
    cstr[ws.length()] = '\0';
    wcs_to_cstr(ws.c_str(), cstr);
    s = cstr;
    delete []cstr;
    return s;
}

}
