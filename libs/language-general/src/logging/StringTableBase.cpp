/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <locale>
#include <utility>

#include "utility/StringUtil.h"

#include "core/Logger.h"

#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTableBase.h"

#include "language/context/ParserContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/module/Package.h"
#include "language/tree/module/Source.h"
#include "language/stage/parser/context/SourceInfoContext.h"

namespace zillians { namespace language  {

namespace {

const char*const DEFAULT_LC_MESSAGES = "en_US";

}

void StringTableBase::setLocale(std::locale& locale)
{
    mLocale = locale;
}

const wchar_t* StringTableBase::getLocaledMessage(uint32 id) const
{
    const auto& translation_pos = mTranslations.find(id);

    if (translation_pos == mTranslations.end())
        return L"";

    const auto& localed_msgs    = translation_pos->second;
    const auto& lc_message      = getLCMessage();
    const auto& localed_msg_pos = localed_msgs.find(lc_message);

    if (localed_msg_pos != localed_msgs.end())
        return localed_msg_pos->second;

    const auto& default_localed_msg_pos = localed_msgs.find(DEFAULT_LC_MESSAGES);

    if (default_localed_msg_pos != localed_msgs.end())
        return default_localed_msg_pos->second;

    return L"";
}

std::string StringTableBase::getLCMessage() const
{
    /***
     * According to the current locale setting, return the type of language setting.
     * If LC_ALL is set, the name of the local will be a string like "en_US.UTF8", otherwise, it
     * will present all facets.
     * For the first case, we just select the message in the specified language; as for the second one,
     * we choose LC_MESSAGES as the key to find out the current language.
     */
    const auto& lc_all            = mLocale.name();
    const auto& locale_categories = StringUtil::tokenize(lc_all, ";", false);

    // possibly the first case where the LC_ALL is set
    if (locale_categories.size() == 1)
        return StringUtil::tokenize(lc_all, ".", false)[0];

    // possibly the second case
    for (const auto locale_category : locale_categories)
    {
        // We will have something like this: LC_MESSAGES, en_US.UTF-8
        const auto& category = StringUtil::tokenize(locale_category, "=", false);

        if (category.size() != 2) continue;
        if (category[0] != "LC_MESSAGES") continue;

        // Now we have lang_encoding = "en_US.UTF-8"
        const auto& lang_encoding = category[1];

        // Strip out ".UTF-8", we only need "en_US"
        auto lang = StringUtil::tokenize(lang_encoding, ".", false)[0];
        return std::move(lang);
    }

    // well, not sure what happen, but we still choose a default language
    return DEFAULT_LC_MESSAGES;
}

void LoggerBase::setWrapper(LoggerWrapper* wrapper)
{
    mWrapper = wrapper;
}

void LoggerBase::setStringTable(StringTable* stringTable)
{
    mStringTable = stringTable;
}

std::pair<std::string, uint32> LoggerBase::getSourcePosition(const tree::ASTNode* attach_point, const tree::ASTNode* node, const log_params_base& params_base)
{
    using tree::cast;

    NOT_NULL(attach_point);
    NOT_NULL(node);

    std::string filename;
    uint32      line = 0;

    if (const auto*const pkg = cast<tree::Package>(node))
        filename = "<package:" + StringUtil::ws2s_ascii(pkg->id->name) + ">";
    else if (!params_base.optional_filename.empty())
        filename = params_base.optional_filename;
    else
        filename = node->getOwner<tree::Source>()->filename;

    if (const auto*const node_source_info = stage::SourceInfoContext::get(node))
        line = node_source_info->line;
    else if (const auto*const attach_point_soure_info = stage::SourceInfoContext::get(attach_point))
        line = attach_point_soure_info->line;
    else
        line = 0;

    return {std::move(filename), std::move(line)};
}

} } // namespace zillians::language
