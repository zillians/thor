/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>

#include <log4cxx/logstring.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/logger.h>

#include "utility/UnicodeUtil.h"

#include "language/logging/LoggerWrapper.h"
#include "language/logging/StringTable.h"

namespace zillians { namespace language  {

log4cxx::LoggerPtr LoggerWrapper::Compiler(log4cxx::Logger::getLogger("compiler"));
log4cxx::LoggerPtr LoggerWrapper::Resolver(log4cxx::Logger::getLogger("compiler.resolver"));
log4cxx::LoggerPtr LoggerWrapper::CompilerLogger(log4cxx::Logger::getLogger("compiler.compilelogger"));
log4cxx::LoggerPtr LoggerWrapper::ParserStage(log4cxx::Logger::getLogger("compiler.parser"));
log4cxx::LoggerPtr LoggerWrapper::TransformerStage(log4cxx::Logger::getLogger("compiler.transformer"));
log4cxx::LoggerPtr LoggerWrapper::GeneratorStage(log4cxx::Logger::getLogger("compiler.generator"));
log4cxx::LoggerPtr LoggerWrapper::DebugInfoGeneratorStage(log4cxx::Logger::getLogger("compiler.debuginfogenerator"));
log4cxx::LoggerPtr LoggerWrapper::MakeStage(log4cxx::Logger::getLogger("make"));
log4cxx::LoggerPtr LoggerWrapper::DepStage(log4cxx::Logger::getLogger("dep"));
log4cxx::LoggerPtr LoggerWrapper::DriverStage(log4cxx::Logger::getLogger("driver"));
log4cxx::LoggerPtr LoggerWrapper::VM(log4cxx::Logger::getLogger("vm"));


LoggerWrapper::LoggerWrapper() : mWarningDegree(0), mFatalCount(0), mErrorCount(0), mWarnCount(0)
{
    initialize();

    mStringTable = new StringTable();
    mLogger = new Logger();

    mLogger->setWrapper(this);
	
	try
	{	
	    mStringTable->setLocale(get_default_locale());
	}
	catch (std::runtime_error& e)
	{
		// Well, if the default locale setting is not acceptable by the system (maybe user set a wrong locale which does not
		// exist), we roll back to use a safe one.
	    mStringTable->setLocale(get_c_locale());
	}

    mLogger->setStringTable(mStringTable);
}

LoggerWrapper::~LoggerWrapper()
{
    mLogger->setStringTable(NULL);

    SAFE_DELETE(mStringTable);
    SAFE_DELETE(mLogger);
}

void LoggerWrapper::initialize()
{
    log4cxx::LogManager::getLoggerRepository()->setConfigured(true);
    log4cxx::LoggerPtr root = log4cxx::Logger::getRootLogger();
    // the default format is "%r [%t] %p %c %x - %m%n"
    // %r -> time
    // %t -> name of the thread
    // %p -> log level
    // %c -> logger name
    // %x -> nested context (usuallh null)
    // %m -> message
    // %n -> new line
    static const log4cxx::LogString default_pattern(LOG4CXX_STR("[%p] [%c] %m%n"));
    static const log4cxx::LogString simple_pattern(LOG4CXX_STR("%m%n"));

    log4cxx::LayoutPtr default_layout(new log4cxx::PatternLayout(default_pattern));
    log4cxx::AppenderPtr default_appender(new log4cxx::ConsoleAppender(default_layout));

    log4cxx::LayoutPtr simple_layout(new log4cxx::PatternLayout(simple_pattern));
    log4cxx::AppenderPtr simple_appender(new log4cxx::ConsoleAppender(simple_layout));

    // configure the default appender
    root->addAppender(simple_appender);

    // log level: TRACE -> DEBUG -> INFO -> WARN -> ERROR -> FATAL

    // configure the default log level
//    root->setLevel(log4cxx::Level::getDebug());
    root->setLevel(log4cxx::Level::getAll());

    // configure each logger independently
    Compiler->setLevel(log4cxx::Level::getError());
    Resolver->setLevel(log4cxx::Level::getError());
    TransformerStage->setLevel(log4cxx::Level::getError());
    GeneratorStage->setLevel(log4cxx::Level::getError());
    CompilerLogger->setLevel(log4cxx::Level::getWarn());
    DebugInfoGeneratorStage->setLevel(log4cxx::Level::getError());

}

uint32 LoggerWrapper::getWarningDegree()
{
    return mWarningDegree;
}

void LoggerWrapper::setWarningDegree(uint32 degree)
{
    mWarningDegree = degree;
}

Logger* LoggerWrapper::getLogger()
{
    return mLogger;
}

void LoggerWrapper::log(LogLevel level, uint32 id, const std::wstring& file, uint32 line, const std::wstring& message)
{
    log4cxx::LevelPtr log4cxx_level = nullptr;

    switch (level)
    {
    case LogLevel::INFO   :                                              log4cxx_level = log4cxx::Level::getInfo ();                break;
    case LogLevel::WARNING:                                              log4cxx_level = log4cxx::Level::getWarn (); ++mWarnCount ; break;
    case LogLevel::ERROR  :                                              log4cxx_level = log4cxx::Level::getError(); ++mErrorCount; break;
    case LogLevel::FATAL  :                                              log4cxx_level = log4cxx::Level::getFatal(); ++mFatalCount; break;
    default               : BOOST_ASSERT(false && "undefined log type"); log4cxx_level = log4cxx::Level::getFatal(); ++mFatalCount; break;
    }

    LOG4CXX_LOG(CompilerLogger, log4cxx_level, file << ":" << line << ": " << message);
}

void LoggerWrapper::resetCounter()
{
    mFatalCount = 0;
    mErrorCount = 0;
    mWarnCount = 0;
}

bool LoggerWrapper::hasFatal()
{
    return mFatalCount > 0;
}

bool LoggerWrapper::hasError()
{
    return mErrorCount > 0;
}

bool LoggerWrapper::hasWarning()
{
    return mWarnCount > 0;
}

} }

