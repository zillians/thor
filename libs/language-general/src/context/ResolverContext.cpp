/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <unordered_set>
#include <functional>
#include <memory>
#include <vector>

#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>

#include "core/Common.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/FunctionType.h"
#include "language/tree/basic/Identifier.h"
#include "language/tree/basic/Literal.h"
#include "language/tree/basic/TypeSpecifier.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/EnumDecl.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/declaration/TypedefDecl.h"
#include "language/tree/declaration/TypenameDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/expression/Expression.h"
#include "language/tree/RelinkablePtr.h"
#include "language/context/ResolverContext.h"

namespace zillians { namespace language {

using namespace language::tree;

namespace
{

bool isTemplateDeclarationAndNotFullySpecialized(const Declaration& decl)
{
    if(!isa<ClassDecl>(&decl) &&
       !isa<FunctionDecl>(&decl))
    {
        return false;
    }

    TemplatedIdentifier* decl_tid = cast<TemplatedIdentifier>(decl.name);
    if(decl_tid == nullptr) return false;
    if(decl_tid->isFullySpecialized()) return false;

    return true;
}

}

bool ResolvedTemplateMapTraitsBase::isValid(Declaration* node)
{
    return  node                        != nullptr &&
            node->name->getTemplateId() != nullptr;
}

std::vector<relinkable_ptr<Type>> ResolvedTemplateMapTraitsBase::generateKey(const std::vector<Type*>& specializations)
{
    return {boost::begin(specializations), boost::end(specializations)};
}

std::vector<relinkable_ptr<Type>> ResolvedTemplateMapTraitsBase::generateKey(Declaration& decl)
{
    using boost::adaptors::transformed;

    const auto*const tid = decl.name->getTemplateId();
    BOOST_ASSERT(tid && "null pointer exception");

    const auto& unique_names_range = tid->templated_type_list
                                   | transformed(std::mem_fn(&TypenameDecl::getCanonicalType))
                                   ;

    return {boost::begin(unique_names_range), boost::end(unique_names_range)};
}

bool InstantiatedFromTraits::isValid(const ValueType& ref)
{
    if (!ref.is_ptr())
        return false;

    auto*const ptr = ref.get_ptr();

    return isValid(ptr);
}

bool InstantiatedFromTraits::isValid(Declaration* ptr)
{
    return  ptr == nullptr ||
            isTemplateDeclarationAndNotFullySpecialized(*ptr);
}

bool SpecializationOfTraits::isValid(Declaration* node)
{
    return  node == nullptr ||
            isTemplateDeclarationAndNotFullySpecialized(*node);
}

bool ResolvedTypeTraits::isValid(ASTNode* node)
{
    if (node == nullptr) return true;

    if (isa<ClassDecl    >(node)) return true;
    if (isa<EnumDecl     >(node)) return true;
    if (isa<TypenameDecl >(node)) return true;
    if (isa<TypedefDecl  >(node)) return true;
    if (isa<FunctionDecl >(node)) return true;
    if (isa<FunctionType >(node)) return true;
    if (isa<TypeSpecifier>(node)) return true;
    if (isa<VariableDecl >(node)) return true;

    return false;
}

bool ResolvedSymbolTraits::isValid(const ValueType& ref)
{
    if (!ref.is_ptr())
        return false;

    auto*const ptr = ref.get_ptr();

    return isValid(ptr);
}

bool ResolvedSymbolTraits::isValid(ASTNode* node)
{
    if (node == nullptr) return true;

    if (isa<ClassDecl    >(node)) return true;
    if (isa<EnumDecl     >(node)) return true;
    if (isa<FunctionDecl >(node)) return true;
    if (isa<TypedefDecl  >(node)) return true;
    if (isa<VariableDecl >(node)) return true;
    if (isa<TypenameDecl >(node)) return true;
    if (isa<StringLiteral>(node)) return true;

    return false;
}

bool ResolvedPackageTraits::isValid(const ValueType& ref)
{
    if (!ref.is_ptr())
        return false;

    auto*const ptr = ref.get_ptr();

    return isValid(ptr);
}

bool ResolvedPackageTraits::isValid(Package* node)
{
    return true;
}

bool DefaultValueAsTraits::isValid(Expression* node)
{
    return true;
}

bool DefaultValueFromTraits::isValid(VariableDecl* node)
{
    if (node == nullptr)
        return false;

    if (!node->isParameter())
        return false;

    return true;
}

bool ResolvedType::isValid(const relinkable_ptr<Type>& ref)
{
    if (!ref.is_ptr())
        return is_type_unique_name(ref.get_unique_name());

    return true;
}

Type* ResolvedType::get(const ASTNode* node)
{
    NOT_NULL(node);

    if     (const auto*const type_specifier = cast<TypeSpecifier>(node)) { return type_specifier->resolved_type; }
    else if(const auto*const declaration    = cast<Declaration  >(node)) { return declaration   ->resolved_type; }
    else if(const auto*const expression     = cast<Expression   >(node)) { return expression    ->resolved_type; }
    else if(const auto*const type           = cast<Type         >(node)) { return type          ->resolved_type; }
    else if(const auto*const identifier     = cast<Identifier   >(node)) { return identifier    ->resolved_type; }
    else                                                                       { return nullptr; }
}

void ResolvedType::set(ASTNode* node, Type* ref)
{
    NOT_NULL(node);

    if     (auto*const type_specifier = cast<TypeSpecifier>(node)) { type_specifier->resolved_type = std::move(ref); }
    else if(auto*const declaration    = cast<Declaration  >(node)) { declaration   ->resolved_type = std::move(ref); }
    else if(auto*const expression     = cast<Expression   >(node)) { expression    ->resolved_type = std::move(ref); }
    else if(auto*const type           = cast<Type         >(node)) { type          ->resolved_type = std::move(ref); }
    else if(auto*const identifier     = cast<Identifier   >(node)) { identifier    ->resolved_type = std::move(ref); }
    else                                                                 { return; }
}

} } // namespace 'language::tree'
