/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/GlobalConstant.h"

namespace zillians { namespace language { namespace global_constant {

#ifdef __PLATFORM_MAC__           
const char *const DYN_CAST_IMPL = "_ZN4thor4lang13__dynCastImplEPcx";
#else
const char *const DYN_CAST_IMPL = "_ZN4thor4lang13__dynCastImplEPcl";
#endif

const char *const ROOTSET_ADDER   = "_ZN4thor4lang14__addToRootSetEPa";
const char *const ROOTSET_REMOVER = "_ZN4thor4lang19__removeFromRootSetEPa";

const char *const INVOCATION_ADDER          = "_ZN4thor4lang15__addInvocationElPa";
const char *const INVOCATION_RESERVER       = "_ZN4thor4lang19__reserveInvocationEl";
const char *const INVOCATION_COMMITER       = "_ZN4thor4lang18__commitInvocationEl";
const char *const INVOCATION_ABORTER        = "_ZN4thor4lang17__abortInvocationEl";

const char *const INVOCATION_APPENDER_BOOL  = "_ZN4thor4lang27__appendInvocationParameterEllb";
const char *const INVOCATION_APPENDER_INT8  = "_ZN4thor4lang27__appendInvocationParameterElla";
const char *const INVOCATION_APPENDER_INT16 = "_ZN4thor4lang27__appendInvocationParameterElls";
const char *const INVOCATION_APPENDER_INT32 = "_ZN4thor4lang27__appendInvocationParameterElli";
const char *const INVOCATION_APPENDER_INT64 = "_ZN4thor4lang27__appendInvocationParameterElll";
const char *const INVOCATION_APPENDER_FLT32 = "_ZN4thor4lang27__appendInvocationParameterEllf";
const char *const INVOCATION_APPENDER_FLT64 = "_ZN4thor4lang27__appendInvocationParameterElld";
const char *const INVOCATION_APPENDER_INT8P = "_ZN4thor4lang27__appendInvocationParameterEllPa";

const char *const INVOCATION_RET_PTR_SETTER = "_ZN4thor4lang24__setInvocationReturnPtrElPa";

} } }
