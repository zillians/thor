/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>
#include <vector>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "utility/UnicodeUtil.h"

#include "language/Architecture.h"

namespace zillians { namespace language {

namespace {

constexpr Architecture::flag_t ZERO           = 0b00000000000000000000;
constexpr Architecture::flag_t CPU_X86        = 0b00000000000000000001;
constexpr Architecture::flag_t CPU_ARM        = 0b00000000000000000010;
constexpr Architecture::flag_t OPENCL_11      = 0b00000000000000010000;
constexpr Architecture::flag_t OPENCL_12      = 0b00000000000000100000;
constexpr Architecture::flag_t CUDA_SM_10     = 0b00000001000000000000;
constexpr Architecture::flag_t CUDA_SM_12     = 0b00000010000000000000;
constexpr Architecture::flag_t CUDA_SM_13     = 0b00000100000000000000;
constexpr Architecture::flag_t CUDA_SM_20     = 0b00001000000000000000;
constexpr Architecture::flag_t CUDA_SM_30     = 0b00010000000000000000;
constexpr Architecture::flag_t CUDA_SM_35     = 0b00100000000000000000;

constexpr Architecture::flag_t CPU_ALL        = 0b00000000000000001111;
constexpr Architecture::flag_t OPENCL_ALL     = 0b00000000111111110000;
constexpr Architecture::flag_t CUDA_ALL       = 0b11111111000000000000;

constexpr Architecture::flag_t GPU_ALL        = OPENCL_ALL | CUDA_ALL;
constexpr Architecture::flag_t ALL            = CPU_ALL | GPU_ALL;

constexpr Architecture::flag_t ERROR          = Architecture::flag_t(0b1) << (CHAR_BIT * sizeof(Architecture::flag_t) - 1);
static_assert((ERROR & ALL) == ZERO, "overlap bit representations");

bool has_error_flag(const Architecture::flag_t& flag)
{
    return (flag & ERROR) != ZERO;
}

Architecture::flag_t get_arch_flag(const std::string& arch)
{
    if (arch.empty()       ) return ZERO;

    if (arch == "x86"      ) return CPU_X86;
    if (arch == "arm"      ) return CPU_ARM;
    if (arch == "cpu"      ) return CPU_ALL;

    if (arch == "opencl_11") return OPENCL_11;
    if (arch == "opencl_12") return OPENCL_12;
    if (arch == "opencl"   ) return OPENCL_ALL;

    if (arch == "sm_10"    ) return CUDA_SM_10;
    if (arch == "sm_12"    ) return CUDA_SM_12;
    if (arch == "sm_13"    ) return CUDA_SM_13;
    if (arch == "sm_20"    ) return CUDA_SM_20;
    if (arch == "sm_30"    ) return CUDA_SM_30;
    if (arch == "sm_35"    ) return CUDA_SM_35;
    if (arch == "cuda"     ) return CUDA_ALL;

    if (arch == "gpu"      ) return GPU_ALL;

    return ERROR;
}

Architecture::flag_t get_arch_flags(const std::string& archs, const std::string& delim)
{
    std::vector<std::string> arch_lists;

    boost::algorithm::split(
        arch_lists,
        archs,
        boost::algorithm::is_any_of(delim)
    );

    Architecture::flag_t flags = ZERO;

    for (const std::string& arch : arch_lists)
    {
        flags |= get_arch_flag(arch);
    }

    return flags;
}

std::string get_arch_string(const Architecture::flag_t flags, const std::string& delim)
{
    bool        is_first = true;
    std::string arch_string;

#define APPEND_FLAG(r, _, elem)                      \
    if (flags & BOOST_PP_SEQ_ELEM(0, elem))          \
    {                                                \
        if (is_first)                                \
            is_first = false;                        \
        else                                         \
            arch_string += delim;                    \
        arch_string +=  BOOST_PP_SEQ_ELEM(1, elem);  \
    }

    BOOST_PP_SEQ_FOR_EACH(
        APPEND_FLAG,
        _,
        ((CPU_X86   )("x86"      ))
        ((CPU_ARM   )("arm"      ))
        ((OPENCL_11 )("opencl_11"))
        ((OPENCL_12 )("opencl_12"))
        ((CUDA_SM_10)("sm_10"    ))
        ((CUDA_SM_12)("sm_12"    ))
        ((CUDA_SM_13)("sm_13"    ))
        ((CUDA_SM_20)("sm_20"    ))
        ((CUDA_SM_30)("sm_30"    ))
        ((CUDA_SM_35)("sm_35"    ))
    );

#undef APPEND_FLAG

    return arch_string;
}

} // anonymous namespace

Architecture::Architecture() noexcept
    : Architecture(ZERO)
{
}

Architecture::Architecture(flag_t flags) noexcept
    : flags(flags)
{
}

Architecture::Architecture(const std::string& arch)
    : flags(get_arch_flag(arch))
{
}

Architecture::Architecture(const std::wstring& arch)
    : Architecture(ws_to_s(arch))
{
}

bool Architecture::is_valid(const std::string& arch)
{
    return !has_error_flag(get_arch_flag(arch));
}

bool Architecture::is_valid(const std::wstring& arch)
{
    return is_valid(ws_to_s(arch));
}

bool Architecture::is_zero() const noexcept
{
    return flags == ZERO;
}

bool Architecture::is_not_zero() const noexcept
{
    return !is_zero();
}

bool Architecture::is_any(const Architecture arch) const noexcept
{
    return (flags & arch.flags) != 0;
}

bool Architecture::is_any_or_zero(const Architecture arch) const noexcept
{
    return is_any(arch) || is_zero();
}

#define DEFINE_ARCH_TESTER(r, _, pair)                                                                        \
    bool Architecture::BOOST_PP_CAT(is_, BOOST_PP_SEQ_ELEM(0, pair))() const noexcept                         \
    {                                                                                                         \
        return is_any(Architecture(BOOST_PP_SEQ_ELEM(1, pair)));                                              \
    }                                                                                                         \
    bool Architecture::BOOST_PP_CAT(BOOST_PP_CAT(is_, BOOST_PP_SEQ_ELEM(0, pair)), _or_zero)() const noexcept \
    {                                                                                                         \
        return is_zero() || BOOST_PP_CAT(is_, BOOST_PP_SEQ_ELEM(0, pair))();                                  \
    }                                                                                                         \
    bool Architecture::BOOST_PP_CAT(is_not_, BOOST_PP_SEQ_ELEM(0, pair))() const noexcept                     \
    {                                                                                                         \
        return !BOOST_PP_CAT(is_, BOOST_PP_SEQ_ELEM(0, pair))();                                              \
    }

BOOST_PP_SEQ_FOR_EACH(
    DEFINE_ARCH_TESTER,
    _,
    ((x86   )(CPU_X86   ))
    ((arm   )(CPU_ARM   ))
    ((cpu   )(CPU_ALL   ))
    ((opencl)(OPENCL_ALL))
    ((sm_10 )(CUDA_SM_10))
    ((sm_12 )(CUDA_SM_12))
    ((sm_13 )(CUDA_SM_13))
    ((sm_20 )(CUDA_SM_20))
    ((sm_30 )(CUDA_SM_30))
    ((sm_35 )(CUDA_SM_35))
    ((cuda  )(CUDA_ALL  ))
    ((gpu   )(GPU_ALL   ))
)

#undef DEFINE_ARCH_TESTER

void Architecture::reset() noexcept
{
    flags = ZERO;
}

bool Architecture::reset(const std::string& arch)
{
    auto new_flag = get_arch_flag(arch);
    if (has_error_flag(new_flag))
        return false;

    flags = new_flag;
    return true;
}

bool Architecture::reset(const std::wstring& arch)
{
    return reset(ws_to_s(arch));
}

bool Architecture::reset(const std::string& archs, const std::string& delim)
{
    auto new_flags = get_arch_flags(archs, delim);
    if (has_error_flag(new_flags))
        return false;

    flags = new_flags;
    return true;
}

bool Architecture::reset(const std::wstring& archs, const std::wstring& delim)
{
    return reset(ws_to_s(archs), ws_to_s(delim));
}

std::string Architecture::toString(const std::string& delim) const
{
    return get_arch_string(flags, delim);
}

std::wstring Architecture::toString(const std::wstring& delim) const
{
    return s_to_ws(toString(ws_to_s(delim)));
}

Architecture::flag_t Architecture::getFlags() const noexcept
{
    return flags;
}

std::vector<Architecture> Architecture::splitFlags(const Architecture interested/* = all()*/) const
{
    std::vector<Architecture> splitted_flags;

    for (flag_t current = 1; current != 0; current <<= 1)
        if (interested.flags & current)
            if (flags & current)
                splitted_flags.emplace_back(current);

    return splitted_flags;
}

Architecture Architecture::zero  () noexcept { return Architecture(ZERO      ); }
Architecture Architecture::x86   () noexcept { return Architecture(CPU_X86   ); }
Architecture Architecture::arm   () noexcept { return Architecture(CPU_ARM   ); }
Architecture Architecture::cpu   () noexcept { return Architecture(CPU_ALL   ); }
Architecture Architecture::opencl() noexcept { return Architecture(OPENCL_ALL); }
Architecture Architecture::cuda  () noexcept { return Architecture(CUDA_ALL  ); }
Architecture Architecture::gpu   () noexcept { return Architecture(GPU_ALL   ); }
Architecture Architecture::all   () noexcept { return Architecture(ALL       ); }

bool Architecture::operator==(const Architecture& rhs) const noexcept
{
    return flags == rhs.flags;
}

bool Architecture::operator!=(const Architecture& rhs) const noexcept
{
    return !(*this == rhs);
}

Architecture& Architecture::operator&=(const Architecture& rhs) noexcept
{
    flags &= rhs.flags;

    return *this;
}

Architecture& Architecture::operator|=(const Architecture& rhs) noexcept
{
    flags |= rhs.flags;

    return *this;
}

Architecture& Architecture::operator^=(const Architecture& rhs) noexcept
{
    flags ^= rhs.flags;

    return *this;
}

Architecture Architecture::operator&(const Architecture& rhs) const noexcept
{
    return Architecture(flags & rhs.flags);
}

Architecture Architecture::operator|(const Architecture& rhs) const noexcept
{
    return Architecture(flags | rhs.flags);
}

Architecture Architecture::operator^(const Architecture& rhs) const noexcept
{
    return Architecture(flags ^ rhs.flags);
}

Architecture Architecture::operator~() const noexcept
{
    return Architecture((~flags) & ALL);
}

Architecture Architecture::default_;

} }
