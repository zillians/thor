<?xml version="1.0"?>

<!--
This file is part of Thor.
Thor is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License, version 3,
as published by the Free Software Foundation.

Thor is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Thor.  If not, see <http://www.gnu.org/licenses/>.

If you want to develop any commercial services or closed-source products with
Thor, to adapt sources of Thor in your own projects without
disclosing sources, purchasing a commercial license is mandatory.

For more information, please contact Zillians, Inc.
<thor@zillians.com>
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="include_guard">ZILLIANS_LANGUAGE_STRINGTABLE_H_</xsl:variable>

<xsl:variable name="license">/**
 * Copyright (C) 2008-2012 Zillians, Inc. &lt;http://www.zillians.com/&gt;
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 */
</xsl:variable>

<xsl:variable name="header_prefix">
<xsl:copy-of select="$license"/>
#ifndef <xsl:copy-of select="$include_guard"/>
#define <xsl:copy-of select="$include_guard"/>

#include &lt;map&gt;
#include &lt;string&gt;
#include &lt;type_traits&gt;

#include &lt;boost/mpl/count.hpp&gt;
#include &lt;boost/mpl/vector.hpp&gt;

#include &quot;language/logging/StringTableBase.h&quot;

namespace zillians { namespace language { namespace tree {

struct ASTNode;

} } }

namespace zillians { namespace language {

</xsl:variable>

<xsl:variable name="header_suffix">
} }

#endif /* <xsl:copy-of select="$include_guard"/> */
</xsl:variable>

<xsl:variable name="source_prefix">
<xsl:copy-of select="$license"/>
#include &lt;string&gt;
#include &lt;utility&gt;

#include &lt;boost/algorithm/string/replace.hpp&gt;

#include &quot;utility/UnicodeUtil.h&quot;

#include &quot;core/IntTypes.h&quot;

#include &quot;language/context/LogInfoContext.h&quot;
#include &quot;language/logging/LoggerWrapper.h&quot;
#include &quot;language/logging/StringTable.h&quot;
#include &quot;language/tree/ASTNode.h&quot;
#include &quot;language/tree/ASTNodeHelper.h&quot;

namespace zillians { namespace language {
</xsl:variable>

<xsl:variable name="source_suffix">
} }
</xsl:variable>

<xsl:template name="log_tag">
  <xsl:text>log_tag_</xsl:text>
  <xsl:value-of select="@name"/>
</xsl:template>

<xsl:template name="def_log_tag_creator">
  <xsl:text>template&lt;typename string_type&gt; inline tagged_param&lt;struct </xsl:text>
  <xsl:call-template name="log_tag"/>
  <xsl:text>&gt; _</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>(string_type&amp;&amp; value) { return {std::forward&lt;string_type&gt;(value)}; }&#xa;</xsl:text>
</xsl:template>

<xsl:template name="log_params_field">
  <xsl:value-of select="@name"/>
</xsl:template>

<xsl:template name="def_log_params_field">
  <xsl:text>    std::wstring </xsl:text>
  <xsl:call-template name="log_params_field"/>
  <xsl:text>;&#xa;</xsl:text>
</xsl:template>

<xsl:template name="log_params">
  <xsl:text>log_params_</xsl:text>
  <xsl:value-of select="@name"/>
</xsl:template>

<xsl:template name="def_log_params">
  <xsl:text>&#xa;struct </xsl:text>
  <xsl:call-template name="log_params"/>
  <xsl:text> : log_params_base&#xa;{&#xa;</xsl:text>

  <!-- define required fields -->
  <xsl:for-each select="parameters/parameter[not(@name = following-sibling::*/@name)]">
    <xsl:call-template name="def_log_params_field"/>
  </xsl:for-each>

  <xsl:text>&#xa;</xsl:text>

  <!-- begin of contructor definition -->
  <xsl:text>    template&lt;typename ...log_tag_types&gt;&#xa;    explicit </xsl:text>
  <xsl:call-template name="log_params"/>
  <xsl:text>(tagged_param&lt;log_tag_types&gt;&amp;&amp;... tagged_params) : log_params_base()&#xa;    {&#xa;</xsl:text>

  <xsl:text>        using all_tags = boost::mpl::vector&lt;log_tag_types...&gt;;

        constexpr auto count_tag_optional_filename = boost::mpl::count&lt;all_tags, log_tag_optional_filename&gt;::type::value;
        static_assert(count_tag_optional_filename &lt;= 1, "duplicated parameter: optional_filename");

</xsl:text>

  <xsl:for-each select="parameters/parameter[not(@name = following-sibling::*/@name)]">
    <xsl:text>        static_assert(boost::mpl::count&lt;all_tags, </xsl:text>
    <xsl:call-template name="log_tag"/>
    <xsl:text>&gt;::type::value == 1, "missed or duplicated parameter: </xsl:text>
    <xsl:call-template name="log_params_field"/>
    <xsl:text>");&#xa;</xsl:text>
  </xsl:for-each>

  <xsl:text>
        static_assert(
            sizeof...(tagged_params) &lt;= </xsl:text>

  <xsl:value-of select="count(parameters/parameter[not(@name = following-sibling::*/@name)])"/>

  <xsl:text> + count_tag_optional_filename,
            "too many parameters, BUT ignore this if there is any error generated by previous static assertions"
        );

        ignore_returned_values(set_param(std::move(tagged_params))...);
    }

</xsl:text>
  <!-- end of contructor definition -->

  <!-- being of parameter setter -->
  <xsl:text>    using log_params_base::set_param;&#xa;</xsl:text>
  <xsl:for-each select="parameters/parameter[not(@name = following-sibling::*/@name)]">
    <xsl:text>    bool set_param(tagged_param&lt;</xsl:text>
    <xsl:call-template name="log_tag"/>
    <xsl:text>&gt;&amp;&amp; param) { </xsl:text>
    <xsl:call-template name="log_params_field"/>
    <xsl:text> = std::move(param.value); return true; }&#xa;</xsl:text>
  </xsl:for-each>
  <!-- end of parameter setter -->

  <xsl:text>};&#xa;</xsl:text>
</xsl:template>

<xsl:template name="log_id">
  <xsl:text>LOG_ID_</xsl:text>
  <xsl:value-of select="@name"/>
</xsl:template>

<xsl:template name="def_log_ids">
  <xsl:text>
namespace {

</xsl:text>

  <xsl:for-each select="messages/message">
    <xsl:text>constexpr uint32 </xsl:text>
    <xsl:call-template name="log_id"/>
    <xsl:text> = </xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>;&#xa;</xsl:text>
  </xsl:for-each>

  <xsl:text>&#xa;}&#xa;</xsl:text>
</xsl:template>

<xsl:template name="def_string_table">
  <xsl:text>
class StringTable : public StringTableBase
{
public:
    StringTable();
};
</xsl:text>
</xsl:template>

<xsl:template name="def_string_table_ctor">
  <xsl:text>
StringTable::StringTable()
{
    mTranslations = {</xsl:text>

  <xsl:for-each select="messages/message">
    <xsl:text>
        {
            </xsl:text>
    <xsl:call-template name="log_id"/>
    <xsl:text>,
            {&#xa;</xsl:text>

    <xsl:for-each select="translations/translation">
      <xsl:text>                {"</xsl:text>
      <xsl:value-of select="@locale"/>
      <xsl:text>", L"</xsl:text>
      <xsl:value-of select="."/>
      <xsl:text>"},&#xa;</xsl:text>
    </xsl:for-each>

    <xsl:text>            }&#xa;</xsl:text>

    <xsl:text>        },</xsl:text>
  </xsl:for-each>

  <xsl:text>
    };
}
</xsl:text>
</xsl:template>

<xsl:template name="def_logger_entry">
  <xsl:text>&#xa;void Logger::</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>(const tree::ASTNode* node, </xsl:text>
  <xsl:call-template name="log_params"/>
  <xsl:text>&amp;&amp; params)
{
    NOT_NULL(node);

    const auto*const attach_point = tree::ASTNodeHelper::getDebugAnnotationAttachPoint(node);
    NOT_NULL(attach_point);

    std::wstring localed_message = mStringTable->getLocaledMessage(</xsl:text>
  <xsl:call-template name="log_id"/>

  <xsl:text>);

    const auto&amp;  source_position = getSourcePosition(attach_point, node, params);
    const auto&amp;  filename        = source_position.first;
    const auto&amp;  line            = source_position.second;

</xsl:text>

  <xsl:for-each select="parameters/parameter[not(@name = following-sibling::*/@name)]">
    <xsl:text>    boost::algorithm::replace_all(localed_message, L"$</xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>$", params.</xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>);&#xa;</xsl:text>
  </xsl:for-each>

  <xsl:text>
    LogInfoContext::push_back(
        const_cast&lt;tree::ASTNode*&gt;(attach_point),
        LogInfo{
            L"LEVEL_</xsl:text>
  <xsl:value-of select="translate(@level, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <xsl:text>",
            L"</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>",
            {
</xsl:text>

  <xsl:for-each select="parameters/parameter[not(@name = following-sibling::*/@name)]">
    <xsl:text>                {L"</xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>", std::move(params.</xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>)},&#xa;</xsl:text>
  </xsl:for-each>

  <xsl:text>            }
        }
    );

</xsl:text>

  <xsl:text>    mWrapper->log(LoggerWrapper::LogLevel::</xsl:text>
  <xsl:value-of select="translate(@level, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
  <xsl:text>, </xsl:text>
  <xsl:call-template name="log_id"/>
  <xsl:text>, s_to_ws(filename), line, localed_message);&#xa;</xsl:text>

  <xsl:text>}&#xa;</xsl:text>
</xsl:template>

<xsl:template name="def_logger">
  <xsl:text>
class Logger : public LoggerBase
{
public:
</xsl:text>

  <xsl:for-each select="messages/message">
    <xsl:text>    void </xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>(const tree::ASTNode* node, </xsl:text>
    <xsl:call-template name="log_params"/>
    <xsl:text>&amp;&amp; params);&#xa;</xsl:text>
  </xsl:for-each>

  <xsl:text>};&#xa;</xsl:text>
</xsl:template>

</xsl:stylesheet>
