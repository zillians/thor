#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#
IF(Clang_FOUND)
    include_directories(
        ${Boost_INCLUDE_DIRS}
        ${Clang_INCLUDR_DIRS}
    )

    link_directories(
        ${Clang_LIB_DIRS}
    )

    add_definitions(
        ${Clang_CXX_FLAGS}
        ${LLVM_CPPFLAGS}
    )

    set(thor_import_grammars
        src/stage/import/grammar/CxxClassGrammar.cpp
        src/stage/import/grammar/CxxFunctionGrammar.cpp
        src/stage/import/grammar/CxxGetSetGrammar.cpp
        src/stage/import/grammar/CxxGuardSymbolGrammar.cpp
        src/stage/import/grammar/CxxIdentifierGrammar.cpp
        src/stage/import/grammar/CxxIncludeGrammar.cpp
        src/stage/import/grammar/CxxNamespaceGrammar.cpp
        src/stage/import/grammar/CxxOptionalScopeGrammar.cpp
        src/stage/import/grammar/CxxPackageGrammar.cpp
        src/stage/import/grammar/CxxParameterGrammar.cpp
        src/stage/import/grammar/CxxPimplGrammar.cpp
        src/stage/import/grammar/CxxTypeGrammar.cpp
        src/stage/import/grammar/TsClassGrammar.cpp
        src/stage/import/grammar/TsFunctionGrammar.cpp
        src/stage/import/grammar/TsGetSetGrammar.cpp
        src/stage/import/grammar/TsIdentifierGrammar.cpp
        src/stage/import/grammar/TsImportGrammar.cpp
        src/stage/import/grammar/TsPackageGrammar.cpp
        src/stage/import/grammar/TsParameterGrammar.cpp
        src/stage/import/grammar/TsPimplGrammar.cpp
        src/stage/import/grammar/TsTypeGrammar.cpp
    )

    add_executable(thor-import
        src/main.cpp
        src/ThorImport.cpp
        src/stage/import/Constants.cpp
        src/stage/import/ClangASTVisitor.cpp
        src/stage/import/ClangWrapper.cpp
        src/stage/import/CodeSnippes.cpp
        src/stage/import/CXXInfoCollector.cpp
        src/stage/import/TSInfo.cpp
        src/stage/import/TSInfoPostProcessings.cpp
        src/stage/import/TSWriter.cpp
        src/stage/import/grammar/Helpers.cpp
        src/stage/import/grammar/IdentifierGrammar.cpp
        ${thor_import_grammars}
    )

    set_property(
        SOURCE   ${thor_import_grammars}
        APPEND
        PROPERTY COMPILE_FLAGS "-g0"
    )

    IF(APPLE)
        target_link_libraries(thor-import
            ${Boost_LIBRARIES}
            ${LLVM_LDFLAGS}
            ${LLVM_ALL_LIBS} ${Clang_LIBS}
        )
    ELSE(APPLE)
        target_link_libraries(thor-import
            ${Boost_LIBRARIES}
            -Wl,--start-group ${LLVM_ALL_LIBS} ${Clang_LIBS} -Wl,--end-group
            ${LLVM_LDFLAGS}
        )
    ENDIF(APPLE)

    add_dependencies(thor-toolchain thor-import)

    install(TARGETS thor-import DESTINATION thor/bin COMPONENT thor)
ENDIF()
