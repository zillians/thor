/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>
#include <tuple>
#include <vector>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/program_options.hpp>

#include "language/ThorImport.h"
#include "language/stage/import/ClangWrapper.h"

namespace zillians { namespace language {

namespace po = boost::program_options;

namespace {

struct ThorImportOptions
{
    std::string              output_directory;
    std::vector<std::string> include_directories;
    std::vector<std::string> target_package;
    std::vector<std::string> parse_headers;

};

std::tuple<ThorImportOptions, bool> parse_options(int argc, const char** argv)
{
    bool                    success = true;
    ThorImportOptions import_options;

    const std::string op_include_dirs("include-dirs");
    const std::string op_output_dir("output-dir");
    const std::string op_target_package("target-package");
    const std::string op_parse_header("parse-header");

    po::options_description            ops;
    po::positional_options_description pos_ops;

    ops.add_options()
        ((op_include_dirs + ",I").c_str(), po::value<std::vector<std::string>>(), "alternative include directories")
        ((op_output_dir + ",D").c_str(), po::value<std::string>(), "output directory for generated wrapper")
        ((op_target_package + ",T").c_str(), po::value<std::string>(), "put the generated wrapper under this package")
        ((op_parse_header).c_str(), po::value<std::vector<std::string>>(), "generate wrapper for this header")
    ;

    pos_ops
        .add(op_parse_header.c_str(), -1)
    ;

    try
    {
        po::variables_map vm;
        po::store(
            po::command_line_parser(argc, argv)
                .options(ops)
                .positional(pos_ops)
                .run(),
            vm
        );

#define STORE_IMPORT_OPTIONS(name, target, optional)                            \
    if(vm.count(name))                                                          \
    {                                                                           \
        import_options.target = vm[name].as<decltype(import_options.target)>(); \
    }                                                                           \
    else if(!optional)                                                          \
    {                                                                           \
        success = false;                                                        \
    }

        STORE_IMPORT_OPTIONS(op_output_dir    , output_directory   , false)
        STORE_IMPORT_OPTIONS(op_include_dirs  , include_directories, true )
        STORE_IMPORT_OPTIONS(op_parse_header  , parse_headers      , false)

#undef STORE_IMPORT_OPTIONS

        if(vm.count(op_target_package))
        {
            boost::split(import_options.target_package, vm[op_target_package].as<std::string>(), boost::is_any_of("."));
        }
        else
        {
            success = false;
        }
    }
    catch (const po::error&)
    {
        success = false;
    }

    if(!success)
    {
        std::cout << ops;
    }

    return std::make_tuple(std::move(import_options), success);
}

} // anonymous namespace

int ThorImport::main(int argc, const char** argv)
{
    bool                    is_valid_options = false;
    ThorImportOptions import_options;

    std::tie(import_options, is_valid_options) = parse_options(argc, argv);

    if(is_valid_options)
    {
        stage::import::ClangWrapper clang_wrapper;

        if (clang_wrapper.parse(
                import_options.include_directories,
                import_options.target_package,
                import_options.parse_headers,
                import_options.output_directory
            ))
        {
            return 0;
        }
        else
        {
            std::cerr << "failed to parse headers or generate wrapper" << std::endl;

            return -1;
        }
    }
    else
    {
        std::cerr << "invalid options" << std::endl;

        return -1;
    }
}

} }
