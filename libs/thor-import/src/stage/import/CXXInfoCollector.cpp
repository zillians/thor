/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/assert.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/map.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/next_prior.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm/reverse.hpp>
#include <boost/variant.hpp>

#include <llvm/Support/Casting.h>

#include <clang/AST/Decl.h>
#include <clang/AST/DeclBase.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/AST/ASTContext.h>

#include "language/stage/import/CXXInfoCollector.h"
#include "language/stage/import/TSInfo.h"

namespace zillians { namespace language { namespace stage { namespace import {

// TODO better error reporting
#if 1
#define TS_DUMP_ERROR(message) std::cerr << message << std::endl
#else
#define TS_DUMP_ERROR(message) ((void)0)
#endif

namespace {

template<typename entry>
struct conversion_context_entry
{
    typedef typename boost::mpl::at_c<entry, 0>::type clang_type;
    typedef typename boost::mpl::at_c<entry, 1>::type ts_type;

    typedef std::map<
        typename std::add_pointer<
            typename std::add_const<clang_type>::type
        >::type,
        typename std::add_pointer<ts_type>::type
    > map_type;

    conversion_context_entry(map_type& map)
        : map(map)
    {
    }

    map_type& map;
};

template<typename... entries>
struct conversion_context : public conversion_context_entry<entries>...
{
    template<typename clang_type>
    struct selector
    {
        typedef boost::mpl::map<
            boost::mpl::pair<
                typename boost::mpl::at_c<entries, 0>::type,
                entries
            >...
        > selection_map;

        typedef typename boost::mpl::at<selection_map, clang_type>::type                                     entry_type;
        typedef typename boost::mpl::at_c<typename boost::mpl::at<selection_map, clang_type>::type, 1>::type ts_type;
    };

    template<typename... arg_types>
    conversion_context(const clang::ASTContext& ast_context, ts_package& root, std::set<ts_class*>& anonymous_type_name_requests, arg_types&... args)
        : conversion_context_entry<entries>(args)...
        , ast_context(&ast_context)
        , root(&root)
        , anonymous_type_name_requests(&anonymous_type_name_requests)
    {
    }

    template<typename clang_type>
    typename conversion_context_entry<typename selector<clang_type>::entry_type>::map_type& get_map()
    {
        return conversion_context_entry<typename selector<clang_type>::entry_type>::map;
    }

    template<typename clang_type>
    const typename conversion_context_entry<typename selector<clang_type>::entry_type>::map_type& get_map() const
    {
        return conversion_context_entry<typename selector<clang_type>::entry_type>::map;
    }

    template<typename clang_type>
    typename selector<clang_type>::ts_type* find(const clang_type& clang_decl) const;

    template<typename clang_type, typename ts_type>
    void insert(const clang_type& clang_decl, ts_type *ts_decl);

    const clang::ASTContext& get_ast_context() const noexcept
    {
        return *ast_context;
    }

    ts_package& get_root() const noexcept
    {
        return *root;
    }

    void request_anonymous_type(ts_class& ts_cls)
    {
        BOOST_ASSERT(anonymous_type_name_requests && "null pointer exception");

        anonymous_type_name_requests->insert(&ts_cls);
    }

    template<typename wrapped_name_generator_type>
    void register_anonymous_type(ts_class& ts_cls, const wrapped_name_generator_type& generator)
    {
        BOOST_ASSERT(anonymous_type_name_requests && "null pointer exception");

        const auto request_pos = anonymous_type_name_requests->find(&ts_cls);

        if(request_pos != anonymous_type_name_requests->end())
        {
            ts_class* ts_cls = *request_pos;

            BOOST_ASSERT(ts_cls && "null pointer exception");
            BOOST_ASSERT(ts_cls->wrapped_name.empty() && "anonymous type has name!");

            ts_cls->wrapped_name = generator();

            anonymous_type_name_requests->erase(request_pos);
        }
    }

private:
    const clang::ASTContext* ast_context;
    ts_package*              root;
    std::set<ts_class*>*     anonymous_type_name_requests;

};

typedef conversion_context<
    boost::mpl::vector<clang::CXXRecordDecl  , ts_class   >,
    boost::mpl::vector<clang::FunctionDecl   , ts_function>,
    boost::mpl::vector<clang::FieldDecl      , ts_getset  >
> conversion_context_impl;

#define TS_CONVERSION_CONTEXT                                          \
    conversion_context_impl(                                           \
        (BOOST_ASSERT(ast_context && "null exception"), *ast_context), \
        (BOOST_ASSERT(root && "null exception"), *root),               \
        anonymous_type_name_requests,                                  \
        cxx_ts_class_map,                                              \
        cxx_ts_function_map,                                           \
        cxx_ts_getset_map                                              \
    )

template<typename policy>
typename policy::ts_declaration* register_impl(conversion_context_impl context, const typename policy::clang_declaration& clang_decl)
{
    typename policy::ts_declaration* decl = nullptr;

    const bool may_valid_declaration =
        policy::verify_precondition(clang_decl) &&
        policy::is_valid(context, clang_decl);

    if(may_valid_declaration)
    {
        decl = context.find(clang_decl);

        if(decl == nullptr)
        {
            decl = policy::create_declaration(context, clang_decl);

            if(decl != nullptr)
            {
                context.insert(clang_decl, decl);
            }
        }
    }

    return decl;
}

template<typename policy>
void complete_impl(conversion_context_impl context, const typename policy::clang_declaration& clang_decl)
{
    const auto& name = clang_decl.getName();

    BOOST_ASSERT(clang_decl.getDefinition() == &clang_decl && "forward declaration should not be here!");

    typename policy::ts_declaration* decl = context.find(clang_decl);

    BOOST_ASSERT(decl && "declaration is not registered!");

    const std::string& error_message = policy::finish_declaration(context, clang_decl, *decl);

    if(!error_message.empty())
    {
        TS_DUMP_ERROR("cannot finish declaration (" << clang_decl.getQualifiedNameAsString() << ") due to : " << error_message);
    }
}

template<typename clang_declaration_type, typename ts_declaration_type>
struct basic_conversion_policy
{
    typedef clang_declaration_type clang_declaration;
    typedef ts_declaration_type    ts_declaration;

    typedef basic_conversion_policy<clang_declaration, ts_declaration> basic_policy;

    static bool verify_precondition(const clang_declaration& clang_decl);
};

struct class_conversion_policy : public basic_conversion_policy<clang::CXXRecordDecl, ts_class>
{
    static bool verify_precondition(const clang_declaration& clang_decl);
    static bool is_valid(conversion_context_impl& context, const clang_declaration& clang_decl);
    static ts_declaration* create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl);
    static std::string finish_declaration(const conversion_context_impl& context, const clang_declaration& clang_decl, ts_declaration& ts_decl);

private:
    static bool default_constructor_is_valid(const clang_declaration& clang_decl);

};

struct getset_conversion_policy : public basic_conversion_policy<clang::FieldDecl, ts_getset>
{
    static bool verify_precondition(const clang_declaration& clang_decl);
    static bool is_valid(conversion_context_impl& context, const clang_declaration& clang_decl);
    static ts_declaration* create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl);

};

struct function_conversion_policy : public basic_conversion_policy<clang::FunctionDecl, ts_function>
{
    static bool verify_precondition(const clang_declaration& clang_decl);
    static bool is_valid(conversion_context_impl& context, const clang_declaration& clang_decl);
    static ts_declaration* create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl);

};

//struct enum_conversion_policy : public basic_conversion_policy<clang::EnumDecl, ts_enum>
//{
//    static bool verify_precondition(const clang_declaration& clang_decl);
//    static bool is_valid(const clang_declaration& clang_decl);
//    static ts_declaration* create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl);
//    static std::string finish_declaration(const conversion_context_impl& context, const clang_declaration& clang_decl, ts_declaration& ts_decl);
//
//};
//
//struct union_conversion_policy : public basic_conversion_policy<clang::CXXRecordDecl, ts_class>
//{
//    static bool verify_precondition(const clang_declaration& clang_decl);
//    static bool is_valid(const clang_declaration& clang_decl);
//    static ts_declaration* create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl);
//    static std::string finish_declaration(const conversion_context_impl& context, const clang_declaration& clang_decl, ts_declaration& ts_decl);
//
//};
//
//struct typedef_conversion_policy : public basic_conversion_policy<clang::TypedefNameDecl, ts_typedef>
//{
//    static bool verify_precondition(const clang_declaration& clang_decl);
//    static bool is_valid(const clang_declaration& clang_decl);
//    static ts_declaration* create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl);
//
//};

const clang::DeclContext* get_name_closure(const clang::Decl& decl)
{
    const clang::DeclContext* current = decl.getDeclContext();

    if(current != nullptr)
    {
        if(current == llvm::dyn_cast<const clang::DeclContext>(&decl))
        {
            current = current->getParent();
        }

        const char* const kind_name = current->getDeclKindName();

        for(; current; current = current->getParent())
        {
            if( current->isNamespace() ||
                current->isRecord())
            {
                return current;
            }
        }
    }

    return nullptr;
}

std::tuple<std::vector<const clang::NamespaceDecl*>, std::vector<const clang::CXXRecordDecl*>> get_reversed_scope_stacks(const clang::Decl& decl)
{
    std::tuple<std::vector<const clang::NamespaceDecl*>, std::vector<const clang::CXXRecordDecl*>> result;

    std::vector<const clang::NamespaceDecl*>& ns_scopes     = std::get<0>(result);
    std::vector<const clang::CXXRecordDecl*>& record_scopes = std::get<1>(result);

    const clang::DeclContext* current = get_name_closure(decl);

    for(; current != nullptr; current = current->getParent())
    {
        if(const clang::CXXRecordDecl* record_decl = llvm::dyn_cast<const clang::CXXRecordDecl>(current))
        {
            if(!record_decl->isAnonymousStructOrUnion())
            {
                record_scopes.emplace_back(record_decl);
            }
        }
        else
        {
            break;
        }
    }

    for(; current != nullptr; current = current->getParent())
    {
        if(const clang::NamespaceDecl* ns_decl = llvm::dyn_cast<const clang::NamespaceDecl>(current))
        {
            ns_scopes.emplace_back(ns_decl);
        }
        else
        {
            break;
        }
    }

    return result;
}

bool has_non_type_template_parameter(const clang::TemplateDecl* template_decl)
{
    if(template_decl != nullptr)
    {
        for(const clang::NamedDecl* template_param_decl: *template_decl->getTemplateParameters())
        {
            if(llvm::isa<clang::NonTypeTemplateParmDecl>(template_param_decl))
            {
                return true;
            }
        }
    }

    return false;
}

std::tuple<ts_package*, std::vector<const clang::CXXRecordDecl*>> locate_package(ts_package& root, const clang::Decl &decl)
{
    auto reversed_scope_stacks = get_reversed_scope_stacks(decl);

    const auto& ns_scopes     = std::get<0>(reversed_scope_stacks);
          auto& record_scopes = std::get<1>(reversed_scope_stacks);

    ts_package* current_package = &root;

    boost::for_each(
        ns_scopes | boost::adaptors::reversed,
        [&current_package](const clang::NamespaceDecl* ns_decl)
        {
            ts_identifier* child_id = new ts_identifier();
            ts_package*    child    = new ts_package();

            child_id->name = ns_decl->getName();
            child->id      = child_id;
            child->depth   = current_package->depth + 1;
            child->scope   = current_package;

            current_package = *current_package->children.insert(child).first;
        }
    );

    boost::reverse(record_scopes);

    return std::make_tuple(current_package, std::move(record_scopes));
}

template<typename clang_decl_type>
ts_class* find_scope_class(const conversion_context_impl& context, const clang_decl_type& clang_decl)
{
    const clang::CXXRecordDecl* record_decl = llvm::cast<const clang::CXXRecordDecl>(clang_decl.getParent());

    while(record_decl != nullptr && record_decl->isAnonymousStructOrUnion())
    {
        record_decl = llvm::dyn_cast<const clang::CXXRecordDecl>(record_decl->getParent());
    }

    return record_decl == nullptr ? nullptr : context.find(*record_decl);
}

void reference_registerer(ts_package& current_package, const ts_type& type)
{
    struct type_visitor : public boost::static_visitor<boost::variant<ts_package*, ts_builtin>>
    {
        result_type operator()(const ts_none& none) const
        {
            return nullptr;
        }

        result_type operator()(const ts_decl* decl) const
        {
            return decl->get_package();
        }

        result_type operator()(ts_primitive primitive) const
        {
            return nullptr;
        }

        result_type operator()(ts_builtin builtin) const
        {
            return builtin;
        }
    };

    struct reference_setter : public boost::static_visitor<>
    {
        explicit reference_setter(ts_package& scope)
            : boost::static_visitor<>()
            , scope(&scope)
        {
        }

        result_type operator()(ts_package* package) const
        {
            BOOST_ASSERT(scope && "null pointer exception");

            if(package != nullptr && scope != package)
            {
                scope->refered_packages.insert(package);
            }
        }

        result_type operator()(ts_builtin builtin) const
        {
            BOOST_ASSERT(scope && "null pointer exception");

            scope->used_builtins.insert(builtin);
        }

        ts_package* scope;
    };

    auto visit_result = boost::apply_visitor(type_visitor(), type);
    boost::apply_visitor(reference_setter(current_package), visit_result);
}

template<typename decl_type>
bool is_public_member(const decl_type& decl)
{
    switch(decl.getAccess())
    {
    case clang::AS_public:
        return true;
    case clang::AS_none:
        {
            const auto* cls_decl = decl.getParent();

            BOOST_ASSERT(cls_decl && "null pointer exception");

            return cls_decl->isStruct() || cls_decl->isUnion();
        }
    }

    return false;
}

bool is_public_assignable(const clang::CXXRecordDecl& cls_decl)
{
    // FIXME also traverse the inheritance hierarchy to determine a class/struct/union is copy-assignable or not
    //
    // Reason: It seems that clang::CXXRecordDecl has not enough information for the following case
    // //////////////// begin of code //////////////////////////////////////
    // class base {
    // private:
    //     base& operator=(const base&); // declared as private!
    // };
    //
    // class derived : public base {};
    // ///////////////// end of code ///////////////////////////////////////
    //
    // In the case, compiler will give you an copy assignment operator for 'derived' if you used it.
    // Then you will get error since copy assignment operator from 'base' is private, during compilation.
    // But we need analysis this fact and mark 'derived' is NOT copy-assignable, in order to prevent further handling on copy-assignment!

    if(cls_decl.hasDeclaredCopyAssignment() && cls_decl.hasUserDeclaredCopyAssignment())
    {
        const clang::CXXMethodDecl*const copy_assign = cls_decl.getCopyAssignmentOperator(true);

        if(copy_assign == nullptr || !is_public_member(*copy_assign))
            return false;
    }

    return true;
}

struct parameter_type_policy
{
    typedef std::tuple<std::string, ts_type, ts_parameter::wrap_method> result_type;

    template<ts_parameter::wrap_method method>
    static result_type accept_class_impl(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record);

    static std::string unsupported_impl(const conversion_context_impl& context, const clang::QualType& qual_type);

    struct value
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );
        static result_type accept_array  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::ArrayType&   array  );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };

    struct pointer
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };

    struct reference
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };
};

struct result_type_policy
{
    typedef std::tuple<std::string, ts_type, ts_function::wrap_method> result_type;

    static std::string unsupported_impl(const conversion_context_impl& context, const clang::QualType& qual_type);

    struct value
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );
        static result_type accept_array  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::ArrayType&   array  );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };

    struct pointer
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };

    struct reference
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };
};

struct field_type_policy
{
    typedef std::tuple<std::string, ts_type, unsigned, unsigned> result_type;

    static std::string unsupported_impl(const conversion_context_impl& context, const clang::QualType& qual_type);

    struct value
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );
        static result_type accept_array  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::ArrayType&   array  );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };

    struct pointer
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };

    struct reference
    {
        static result_type accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin);
        static result_type accept_class  (const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType&  record );

        static std::string unsupported(const conversion_context_impl& context, const clang::QualType& qual_type);
    };
};

template<ts_parameter::wrap_method method>
auto parameter_type_policy::accept_class_impl(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    if(const clang::CXXRecordDecl* decl = llvm::dyn_cast<const clang::CXXRecordDecl>(record.getDecl()))
    {
        if(ts_class* ts_cls = context.find(*decl))
        {
            return std::make_tuple(std::string(), ts_cls, method);
        }
    }

    return std::make_tuple(
        "reference to invalid Thor class from C++ (" + qual_type.getAsString() + ")",
        ts_none(),
        ts_parameter::BY_PASS
    );
}

std::string parameter_type_policy::unsupported_impl(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return "invalid Thor type for parameter from C++ (" + qual_type.getAsString() + ')';
}

auto parameter_type_policy::value::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    switch(builtin.getKind())
    {
    case clang::BuiltinType::Void  : return std::make_tuple(std::string(), ts_primitive::VOID   , ts_parameter::BY_PASS);
    case clang::BuiltinType::Bool  : return std::make_tuple(std::string(), ts_primitive::BOOL   , ts_parameter::BY_PASS);
    case clang::BuiltinType::Char_S: return std::make_tuple(std::string(), ts_primitive::INT8   , ts_parameter::BY_PASS);
    case clang::BuiltinType::Short : return std::make_tuple(std::string(), ts_primitive::INT16  , ts_parameter::BY_PASS);
    case clang::BuiltinType::Int   : return std::make_tuple(std::string(), ts_primitive::INT32  , ts_parameter::BY_PASS);
    case clang::BuiltinType::Long  : return std::make_tuple(std::string(), ts_primitive::INT64  , ts_parameter::BY_PASS);
    case clang::BuiltinType::Float : return std::make_tuple(std::string(), ts_primitive::FLOAT32, ts_parameter::BY_PASS);
    case clang::BuiltinType::Double: return std::make_tuple(std::string(), ts_primitive::FLOAT64, ts_parameter::BY_PASS);
    }

    return std::make_tuple(
        "invalid builtin type (" + qual_type.getAsString() + ")",
        ts_none(),
        ts_parameter::BY_PASS
    );
}

auto parameter_type_policy::value::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return accept_class_impl<ts_parameter::VALUE>(context, qual_type, record);
}

auto parameter_type_policy::value::accept_array(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::ArrayType& array) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_parameter::BY_PASS
    );
}

std::string parameter_type_policy::value::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

auto parameter_type_policy::pointer::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    if(qual_type->getPointeeType().isConstQualified())
    {
        const clang::Type*const canonical_builtin = builtin.desugar().getCanonicalType().getTypePtr();

        if(canonical_builtin == context.get_ast_context().CharTy .getTypePtr()) return std::make_tuple(std::string(), ts_builtin::C_STRING , ts_parameter::C_STRING);
        if(canonical_builtin == context.get_ast_context().WCharTy.getTypePtr()) return std::make_tuple(std::string(), ts_builtin::C_WSTRING, ts_parameter::C_WSTRING);
    }

    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_parameter::BY_PASS
    );
}

auto parameter_type_policy::pointer::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return accept_class_impl<ts_parameter::POINTER>(context, qual_type, record);
}

std::string parameter_type_policy::pointer::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

auto parameter_type_policy::reference::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_parameter::BY_PASS
    );
}

auto parameter_type_policy::reference::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return accept_class_impl<ts_parameter::REFERENCE>(context, qual_type, record);
}

std::string parameter_type_policy::reference::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

std::string result_type_policy::unsupported_impl(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return "invalid Thor type for return value from C++ (" + qual_type.getAsString() + ")";
}

auto result_type_policy::value::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    switch(builtin.getKind())
    {
    case clang::BuiltinType::Void  : return std::make_tuple(std::string(), ts_primitive::VOID   , ts_function::BY_PASS);
    case clang::BuiltinType::Bool  : return std::make_tuple(std::string(), ts_primitive::BOOL   , ts_function::BY_PASS);
    case clang::BuiltinType::Char_S: return std::make_tuple(std::string(), ts_primitive::INT8   , ts_function::BY_PASS);
    case clang::BuiltinType::Short : return std::make_tuple(std::string(), ts_primitive::INT16  , ts_function::BY_PASS);
    case clang::BuiltinType::Int   : return std::make_tuple(std::string(), ts_primitive::INT32  , ts_function::BY_PASS);
    case clang::BuiltinType::Long  : return std::make_tuple(std::string(), ts_primitive::INT64  , ts_function::BY_PASS);
    case clang::BuiltinType::Float : return std::make_tuple(std::string(), ts_primitive::FLOAT32, ts_function::BY_PASS);
    case clang::BuiltinType::Double: return std::make_tuple(std::string(), ts_primitive::FLOAT64, ts_function::BY_PASS);
    }

    return std::make_tuple(
        "invalid builtin type (" + qual_type.getAsString() + ")",
        ts_none(),
        ts_function::BY_PASS
    );
}

auto result_type_policy::value::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    if(const clang::CXXRecordDecl* decl = llvm::dyn_cast<const clang::CXXRecordDecl>(record.getDecl()))
    {
        if(ts_class* ts_cls = context.find(*decl))
        {
            return std::make_tuple(std::string(), ts_cls, ts_function::COPY_CREATE);
        }
    }

    return std::make_tuple(
        "reference to invalid Thor class from C++ (" + qual_type.getAsString() + ")",
        ts_none(),
        ts_function::BY_PASS
    );
}

auto result_type_policy::value::accept_array(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::ArrayType& array) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_function::BY_PASS
    );
}

std::string result_type_policy::value::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

auto result_type_policy::pointer::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_function::BY_PASS
    );
}

auto result_type_policy::pointer::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_function::BY_PASS
    );
}

std::string result_type_policy::pointer::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

auto result_type_policy::reference::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_function::BY_PASS
    );
}

auto result_type_policy::reference::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_function::BY_PASS
    );
}

std::string result_type_policy::reference::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

std::string field_type_policy::unsupported_impl(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return "invalid Thor type for fields from C++ (" + qual_type.getAsString() + ')';
}

auto field_type_policy::value::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    switch(builtin.getKind())
    {
    case clang::BuiltinType::Bool  : return std::make_tuple(std::string(), ts_primitive::BOOL   , ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    case clang::BuiltinType::Char_S: return std::make_tuple(std::string(), ts_primitive::INT8   , ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    case clang::BuiltinType::Short : return std::make_tuple(std::string(), ts_primitive::INT16  , ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    case clang::BuiltinType::Int   : return std::make_tuple(std::string(), ts_primitive::INT32  , ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    case clang::BuiltinType::Long  : return std::make_tuple(std::string(), ts_primitive::INT64  , ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    case clang::BuiltinType::Float : return std::make_tuple(std::string(), ts_primitive::FLOAT32, ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    case clang::BuiltinType::Double: return std::make_tuple(std::string(), ts_primitive::FLOAT64, ts_getset::GETTER_BY_PASS, qual_type.isConstQualified() ? ts_getset::SETTER_NO_WRAP : ts_getset::SETTER_BY_PASS);
    }

    return std::make_tuple(
        "invalid builtin type (" + qual_type.getAsString() + ")",
        ts_none(),
        ts_getset::GETTER_NO_WRAP,
        ts_getset::SETTER_NO_WRAP
    );
}

auto field_type_policy::value::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    result_type result(std::string(), ts_type(), ts_getset::GETTER_NO_WRAP, ts_getset::SETTER_NO_WRAP);

    std::string& error_message   = std::get<0>(result);
    ts_type&     type            = std::get<1>(result);
    unsigned&    getter_wrapping = std::get<2>(result);
    unsigned&    setter_wrapping = std::get<3>(result);

    if(!qual_type.isConstQualified())
    {
        if(const clang::CXXRecordDecl* decl = llvm::dyn_cast<const clang::CXXRecordDecl>(record.getDecl()))
        {
            if(ts_class* ts_cls = context.find(*decl))
            {
                type = ts_cls;

                getter_wrapping = ts_getset::GETTER_ADDRESS_OF;
                setter_wrapping = is_public_assignable(*decl->getCanonicalDecl()) ? ts_getset::SETTER_COPY_ASSIGN : ts_getset::SETTER_NO_WRAP;
            }
            else
            {
                error_message = "invalid Thor type for fields which its type (" + qual_type.getAsString() + ") is not wrapped from C++";
            }
        }
        else
        {
            error_message = unsupported(context, qual_type);
        }
    }
    else
    {
        error_message = "invalid Thor type for fields from C++ const member (" + qual_type.getAsString() + ')';
    }

    return result;
}

auto field_type_policy::value::accept_array(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::ArrayType& array) -> result_type
{
    std::string error_message;
    ts_type     type;

    if(array.isConstantArrayType())
    {
        const clang::QualType& elem_qual_type = array.getElementType();

        unsigned getter_wrapping = ts_getset::GETTER_NO_WRAP;
        unsigned setter_wrapping = ts_getset::SETTER_NO_WRAP;

        if(const auto* builtin = elem_qual_type->getAs<clang::BuiltinType>())
        {
            std::tie(error_message, type, getter_wrapping, setter_wrapping) = accept_builtin(context, elem_qual_type, *builtin);
        }
        else if(const auto* record = elem_qual_type->getAs<clang::RecordType>())
        {
            std::tie(error_message, type, getter_wrapping, setter_wrapping) = accept_class(context, elem_qual_type, *record);
        }
        else
        {
            error_message = unsupported_impl(context, qual_type);
        }

        if(error_message.empty())
        {
            BOOST_ASSERT(array.isConstantArrayType() && "support constant size array only");

            if(getter_wrapping != ts_getset::GETTER_NO_WRAP) getter_wrapping |= ts_getset::GETTER_ARRAY_ELEM | ts_getset::GETTER_ARRAY_SIZE;
            if(setter_wrapping != ts_getset::SETTER_NO_WRAP) setter_wrapping |= ts_getset::SETTER_ARRAY_ELEM;

            return std::make_tuple
            (
                std::string(),
                std::move(type),
                getter_wrapping,
                setter_wrapping
            );
        }
    }
    else
    {
        error_message = unsupported_impl(context, qual_type);
    }

    return std::make_tuple
    (
        std::move(error_message),
        std::move(type),
        ts_getset::GETTER_NO_WRAP,
        ts_getset::SETTER_NO_WRAP
    );
}

std::string field_type_policy::value::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

auto field_type_policy::pointer::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_getset::GETTER_NO_WRAP,
        ts_getset::SETTER_NO_WRAP
    );
}

auto field_type_policy::pointer::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_getset::GETTER_NO_WRAP,
        ts_getset::SETTER_NO_WRAP
    );
}

std::string field_type_policy::pointer::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

auto field_type_policy::reference::accept_builtin(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::BuiltinType& builtin) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_getset::GETTER_NO_WRAP,
        ts_getset::SETTER_NO_WRAP
    );
}

auto field_type_policy::reference::accept_class(const conversion_context_impl& context, const clang::QualType& qual_type, const clang::RecordType& record) -> result_type
{
    return std::make_tuple
    (
        unsupported(context, qual_type),
        ts_none(),
        ts_getset::GETTER_NO_WRAP,
        ts_getset::SETTER_NO_WRAP
    );
}

std::string field_type_policy::reference::unsupported(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    return unsupported_impl(context, qual_type);
}

template<typename clang_decl_type>
std::string get_qualified_name_without_anonymous_scopes(const clang_decl_type& clang_decl)
{
    const auto& reversed_stacks = get_reversed_scope_stacks(clang_decl);

    const auto& reversed_ns_stack  = std::get<0>(reversed_stacks);
    const auto& reversed_cls_stack = std::get<1>(reversed_stacks);

    std::string result;

    const auto scope_appender = [&result](const clang::NamedDecl* decl)
    {
        BOOST_ASSERT(decl && "null point exception");

        result += "::";
        result += decl->getName().str();
    };

    boost::for_each(reversed_ns_stack  | boost::adaptors::reversed, scope_appender);
    boost::for_each(reversed_cls_stack | boost::adaptors::reversed, scope_appender);

    scope_appender(&clang_decl);

    return result;
}

std::string generate_name_for_anonymous_type_impl(const conversion_context_impl& context, const clang::FieldDecl& field_decl, const clang::QualType& qual_type)
{
    std::string type_name;

    if(qual_type->isRecordType())
    {
        type_name += "decltype(";
        type_name += get_qualified_name_without_anonymous_scopes(field_decl); // 'getQualifiedNameAsString' cannot supress anonymous struct scope: <anonymous class>
        type_name += ')';
    }
    else if(qual_type->isPointerType())
    {
        const clang::QualType& pointee_qual_type = qual_type->getPointeeType();

        type_name += "std::remove_pointer<";
        type_name += generate_name_for_anonymous_type_impl(context, field_decl, pointee_qual_type);
        type_name += ">::type";
    }
    else if(qual_type->isArrayType())
    {
        type_name += "std::remove_extent<decltype(";
        type_name += get_qualified_name_without_anonymous_scopes(field_decl); // 'getQualifiedNameAsString' cannot supress anonymous struct scope: <anonymous class>
        type_name += ")>::type";
    }
    else
    {
        BOOST_ASSERT(false && "unsupported type!");
    }

    return type_name;
}

std::string generate_name_for_anonymous_type(const conversion_context_impl& context, const clang::FieldDecl& field_decl)
{
    const clang::QualType& qual_type = field_decl.getType();

    std::string wrapped_name = generate_name_for_anonymous_type_impl(context, field_decl, qual_type);

    return wrapped_name;
}

template<typename policy>
typename policy::result_type get_ts_type(const conversion_context_impl& context, const clang::QualType& qual_type)
{
    typedef typename policy::result_type result_type;

    static_assert(std::is_same<std::string, typename std::tuple_element<0, result_type>::type>::value, "1st element must be std::string");
    static_assert(std::is_same<ts_type    , typename std::tuple_element<1, result_type>::type>::value, "2nd element must be ts_type");

    typedef typename policy::value     value_accepter;
    typedef typename policy::pointer   pointer_accepter;
    typedef typename policy::reference reference_accepter;

    result_type result;

    std::string& error_message = std::get<0>(result);
    ts_type&     type          = std::get<1>(result);

    if(const auto* builtin_type = qual_type->getAs<clang::BuiltinType>())
    {
        result = value_accepter::accept_builtin(context, qual_type, *builtin_type);
    }
    else if(const auto* cls_type = qual_type->getAs<clang::RecordType>())
    {
        result = value_accepter::accept_class(context, qual_type, *cls_type);
    }
    else if(const auto* array_type = context.get_ast_context().getAsArrayType(qual_type))
    {
        result = value_accepter::accept_array(context, qual_type, *array_type);
    }
    else if(const auto* pointer_type = qual_type->getAs<clang::PointerType>())
    {
        BOOST_ASSERT(qual_type.getTypePtr() && "null pointer exception");

        const clang::QualType& pointee_qual_type = qual_type.getTypePtr()->getPointeeType();

        if(const auto* builtin_type = pointee_qual_type->getAs<clang::BuiltinType>())
        {
            result = pointer_accepter::accept_builtin(context, qual_type, *builtin_type);
        }
        else if(const auto* cls_type = pointee_qual_type->getAs<clang::RecordType>())
        {
            result = pointer_accepter::accept_class(context, qual_type, *cls_type);
        }
        else
        {
            error_message = pointer_accepter::unsupported(context, qual_type);
        }
    }
    else if(const auto* reference_type = qual_type->getAs<clang::ReferenceType>())
    {
        BOOST_ASSERT(qual_type.getTypePtr() && "null pointer exception");

        const clang::QualType& pointee_qual_type = qual_type.getTypePtr()->getPointeeType();

        if(const auto* builtin_type = pointee_qual_type->getAs<clang::BuiltinType>())
        {
            result = reference_accepter::accept_builtin(context, qual_type, *builtin_type);
        }
        else if(const auto* cls_type = pointee_qual_type->getAs<clang::RecordType>())
        {
            result = reference_accepter::accept_class(context, qual_type, *cls_type);
        }
        else
        {
            error_message = reference_accepter::unsupported(context, qual_type);
        }
    }
    else
    {
        error_message = value_accepter::unsupported(context, qual_type);
    }

    return result;
}

ts_package& get_global_function_owner(const conversion_context_impl& context, const clang::FunctionDecl& function)
{
    ts_package*                              ts_pkg        = nullptr;
    std::vector<const clang::CXXRecordDecl*> record_scopes;

    std::tie(ts_pkg, record_scopes) = locate_package(context.get_root(), function);

    BOOST_ASSERT(ts_pkg && "null pointer exception");
    BOOST_ASSERT(record_scopes.empty() && "member functions should be handled by CXXRecordDecl");

    return *ts_pkg;
}

std::tuple<std::string, std::vector<std::tuple<std::string, ts_type, ts_parameter::wrap_method>>> get_parameter_types(const conversion_context_impl& context, const clang::FunctionDecl& function)
{
    std::string                                                                error_message;
    ts_type                                                                    temp_type;
    ts_parameter::wrap_method                                                  temp_wrap_method;
    std::vector<std::tuple<std::string, ts_type, ts_parameter::wrap_method>>   parameter_types;

    for(unsigned i = 0, iend = function.getNumParams(); i != iend && error_message.empty(); ++i)
    {
        const clang::ParmVarDecl* param_decl = function.getParamDecl(i);

        BOOST_ASSERT(param_decl && "null pointer exception");

        const clang::QualType& param_qual_type = param_decl->getType();

        std::tie(error_message, temp_type, temp_wrap_method) = get_ts_type<parameter_type_policy>(context, param_qual_type);

        if(error_message.empty())
        {
            parameter_types.emplace_back(
                param_decl->getName().str(),
                std::move(temp_type),
                std::move(temp_wrap_method)
            );
        }
        else
        {
            error_message = "[arg-" + boost::lexical_cast<std::string>(i) + "] " + error_message;
        }
    }

    return std::make_tuple(
        std::move(error_message),
        std::move(parameter_types)
    );
}

template<typename parameter_type_container, typename clang_decl_type>
std::tuple<std::string, ts_function*> create_function_impl(
    const conversion_context_impl&   context,
    ts_package&                      ts_pkg,
    const clang_decl_type&           wrapped,
    std::string&&                    name,
    const ts_type&                   result_type,
    ts_function::wrap_method         result_wrapping,
    const parameter_type_container&  parameter_types)
{
    ts_identifier* func_id = new ts_identifier();
    ts_function*   func    = new ts_function();

    func_id->name         = std::move(name);
    func->wrapped_name    = "::" + wrapped.getQualifiedNameAsString();
    func->id              = func_id;
    func->result_type     = result_type;
    func->result_wrapping = result_wrapping;
    func->scope           = &ts_pkg;

    ts_pkg.declarations.emplace_back(func);

    for(const auto& parameter: parameter_types)
    {
        ts_identifier* param_id = new ts_identifier();
        ts_parameter*  param    = new ts_parameter();

        param_id->name      = std::get<0>(parameter);
        param->id           = param_id;
        param->type         = std::get<1>(parameter);
        param->parameter_of = func;
        param->wrapping     = std::get<2>(parameter);

        func->parameters.emplace_back(param);

        reference_registerer(ts_pkg, param->type);
    }

    reference_registerer(ts_pkg, result_type);

    return std::make_tuple(std::string(), func);
}

std::tuple<std::string, ts_function*> create_function_impl(
    const conversion_context_impl&   context,
    ts_package&                      ts_pkg,
    const clang::FunctionDecl&       function)
{
    std::string error_result_type;
    std::string error_parameter_types;

    ts_type                                                                    result_type;
    ts_function::wrap_method                                                   result_wrapping;
    std::vector<std::tuple<std::string, ts_type, ts_parameter::wrap_method>>   parameter_types;

    std::tie(error_result_type    , result_type    , result_wrapping) = get_ts_type<result_type_policy>(context, function.getResultType());
    std::tie(error_parameter_types, parameter_types                 ) = get_parameter_types(context, function);

    if(!error_result_type.empty())
    {
        return std::make_tuple("[<result>] " + error_result_type, nullptr);
    }
    else if(!error_parameter_types.empty())
    {
        return std::make_tuple(std::move(error_parameter_types), nullptr);
    }
    else
    {
        return create_function_impl(
            context,
            ts_pkg,
            function,
            function.getName().str(),
            result_type,
            result_wrapping,
            parameter_types
        );
    }
}

template<typename... entries>
template<typename clang_type>
auto conversion_context<entries...>::find(const clang_type& clang_decl) const -> typename selector<clang_type>::ts_type*
{
    const auto& map = conversion_context_entry<typename selector<clang_type>::entry_type>::map;

    const clang_type* clang_canonical_decl = llvm::cast<const clang_type>(clang_decl.getCanonicalDecl());

    const auto ts_decl_pos = map.find(clang_canonical_decl);
    if(ts_decl_pos != map.end())
    {
        return ts_decl_pos->second;
    }
    else
    {
        return nullptr;
    }
}

template<typename... entries>
template<typename clang_type, typename ts_type>
void conversion_context<entries...>::insert(const clang_type& clang_decl, ts_type *ts_decl)
{
    BOOST_ASSERT(ts_decl && "null pointer exception");

    auto& map = conversion_context_entry<typename selector<clang_type>::entry_type>::map;

    const clang_type* clang_canonical_decl = llvm::cast<const clang_type>(clang_decl.getCanonicalDecl());

    BOOST_ASSERT(clang_canonical_decl && "null pointer exception");

    map.insert(std::make_pair(clang_canonical_decl, ts_decl));
}

template<typename clang_declaration_type, typename ts_declaration_type>
bool basic_conversion_policy<clang_declaration_type, ts_declaration_type>::verify_precondition(const clang_declaration& clang_decl)
{
    return !clang_decl.getName().startswith("__");
}

bool class_conversion_policy::verify_precondition(const clang_declaration& clang_decl)
{
    return  basic_policy::verify_precondition(clang_decl) &&
            (clang_decl.isClass() || clang_decl.isStruct());
}

bool class_conversion_policy::is_valid(conversion_context_impl& context, const clang_declaration& clang_decl)
{
    BOOST_ASSERT((clang_decl.isClass() || clang_decl.isStruct()) && "we should handle only classes/structs here");

    const char* error_message = nullptr;

    if(llvm::isa<clang::ClassTemplateSpecializationDecl>(clang_decl))
    {
        error_message = "Ignored C++ template class specialization for";
    }
    else if(has_non_type_template_parameter(clang_decl.getDescribedClassTemplate()))
    {
        error_message = "Ignored C++ template class contains non-type template parameter";
    }
    else if(const clang::ClassTemplateDecl* template_decl = clang_decl.getDescribedClassTemplate())
    {
        // TODO support template classes
        error_message = "TODO Ignored template class/struct";
    }
    else if(clang_decl.isAnonymousStructOrUnion())
    {
        error_message = "anonymous struct/union should not be handled here";
    }
    else if(llvm::isa<const clang::CXXRecordDecl>(clang_decl.getParent()) && find_scope_class(context, clang_decl) == nullptr)
    {
        error_message = "Ignored nested C++ class declaration due to parent class is not available in wrapper";
    }
    else
    {
        error_message = nullptr;
    }

    if(error_message != nullptr)
    {
        TS_DUMP_ERROR(error_message << " : " << clang_decl.getQualifiedNameAsString());
    }

    return error_message == nullptr;
}

auto class_conversion_policy::create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl) -> ts_declaration*
{
    ts_package*                              ts_pkg        = nullptr;
    std::vector<const clang::CXXRecordDecl*> record_scopes;

    std::tie(ts_pkg, record_scopes) = locate_package(context.get_root(), clang_decl);

    BOOST_ASSERT(ts_pkg && "null pointer exception");

    ts_class* origin_scope = record_scopes.empty() ? nullptr : context.find(*record_scopes.back()->getCanonicalDecl());

    BOOST_ASSERT(
        (
            ( record_scopes.empty() && origin_scope == nullptr) ||
            (!record_scopes.empty() && origin_scope != nullptr)
        ) && "origin declaration scope is not handled correctly"
    );

    ts_identifier* ts_cls_id = new ts_identifier();
    ts_class*      ts_cls    = new ts_class();

    ts_cls_id->name      = clang_decl.getName();
    ts_cls->id           = ts_cls_id;
    ts_cls->scope        = ts_pkg;
    ts_cls->origin_scope = origin_scope;

    auto& declarations = ts_pkg->declarations;

    if(origin_scope != nullptr)
    {
        const auto reversed_origin_scope_pos = std::find(declarations.rbegin(), declarations.rend(), ts_package::declaration_variant(origin_scope));

        BOOST_ASSERT(reversed_origin_scope_pos != declarations.rend() && "origin scope is not wrapped by thor-import");

        declarations.emplace(boost::prior(reversed_origin_scope_pos.base()), ts_cls);
    }
    else
    {
        declarations.emplace_back(ts_cls);
    }

    if(ts_cls_id->name.empty())
    {
        // type of current class is anonymous type
        context.request_anonymous_type(*ts_cls);
    }
    else
    {
        ts_cls->wrapped_name = "::" + clang_decl.getQualifiedNameAsString();
    }

    return ts_cls;
}

std::string class_conversion_policy::finish_declaration(const conversion_context_impl& context, const clang_declaration& clang_decl, ts_declaration& ts_cls)
{
    if(default_constructor_is_valid(clang_decl))
    {
        BOOST_ASSERT(ts_cls.id && "null pointer exception");

        ts_identifier* ctor_id = new ts_identifier();
        ts_function*   ctor    = new ts_function();

        ctor->id          = ctor_id;
        ctor->member_of   = &ts_cls;
        ctor->type        = ts_function::CONSTRUCTOR;

        ts_cls.constructors.emplace_back(ctor);
    }

    return std::string();
}

bool class_conversion_policy::default_constructor_is_valid(const clang_declaration& clang_decl)
{
    if(clang_decl.needsImplicitDefaultConstructor())
    {
        return true;
    }
    else if(clang_decl.hasDeclaredDefaultConstructor())
    {
        if(clang_decl.hasUserProvidedDefaultConstructor())
        {
            for(const clang::CXXConstructorDecl* constructor: boost::make_iterator_range(clang_decl.ctor_begin(), clang_decl.ctor_end()))
            {
                BOOST_ASSERT(constructor && "null pointer exception");

                if(constructor->isDefaultConstructor())
                {
                    return is_public_member(*constructor);
                }
            }
        }
        else
        {
            return true;
        }
    }

    return false;
}

bool getset_conversion_policy::verify_precondition(const clang_declaration& clang_decl)
{
    return basic_policy::verify_precondition(clang_decl);
}

bool getset_conversion_policy::is_valid(conversion_context_impl& context, const clang_declaration& clang_decl)
{
    return is_public_member(clang_decl);
}

auto getset_conversion_policy::create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl) -> ts_declaration*
{
    const std::string name = clang_decl.getParent()->getName().str();

    ts_class*       ts_cls  = find_scope_class(context, clang_decl);
    ts_declaration* ts_decl = nullptr;

    BOOST_ASSERT(ts_cls && "null pointer exception");
    BOOST_ASSERT(is_public_member(clang_decl) && "non-public member should be refused by pre-condition or validation");

    std::string error_message;
    ts_type     field_type;
    unsigned    getter_wrapping = ts_getset::GETTER_NO_WRAP;
    unsigned    setter_wrapping = ts_getset::SETTER_NO_WRAP;

    const clang::QualType& qual_type = clang_decl.getType();

    std::tie(
        error_message,
        field_type,
        getter_wrapping,
        setter_wrapping
    ) = get_ts_type<field_type_policy>(context, qual_type);

    if(error_message.empty())
    {
        BOOST_ASSERT(
            (
                getter_wrapping != ts_getset::GETTER_NO_WRAP ||
                setter_wrapping != ts_getset::SETTER_NO_WRAP
            ) && "wrap nothing but no error!"
        );

        ts_decl = new ts_getset();

        ts_decl->member_of       = ts_cls;
        ts_decl->scope           = ts_cls->scope;
        ts_decl->wrapped_name    = clang_decl.getName().str();
        ts_decl->getter_wrapping = getter_wrapping;
        ts_decl->setter_wrapping = setter_wrapping;
        ts_decl->type            = field_type;

        ts_cls->getsets.emplace_back(ts_decl);

        ts_package* ts_pkg = ts_cls->get_package();

        BOOST_ASSERT(ts_pkg != nullptr && "wrapper class is not one any package!");

        reference_registerer(*ts_pkg, ts_decl->type);

        if(ts_class** field_class_type = boost::get<ts_class*>(&ts_decl->type))
        {
            BOOST_ASSERT(*field_class_type && "null pointer exception");

            context.register_anonymous_type(**field_class_type, [&context, &clang_decl](){return generate_name_for_anonymous_type(context, clang_decl);});
        }
    }
    else
    {
        TS_DUMP_ERROR("Ignored C++ field (" << clang_decl.getQualifiedNameAsString() << ") due to : " << error_message);
    }

    return ts_decl;
}

bool function_conversion_policy::verify_precondition(const clang_declaration& clang_decl)
{
    return  basic_policy::verify_precondition(clang_decl) &&
            !clang_decl.isCXXClassMember();
}

bool function_conversion_policy::is_valid(conversion_context_impl& context, const clang_declaration& clang_decl)
{
    const char* error_message = nullptr;

    if(clang_decl.isOverloadedOperator())
    {
        error_message = "Ignored overloaded C++ operator";
    }

    if(error_message != nullptr)
    {
        TS_DUMP_ERROR(error_message << " : " << clang_decl.getQualifiedNameAsString());
    }

    return error_message == nullptr;
}

auto function_conversion_policy::create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl) -> ts_declaration*
{
    ts_package&           ts_owner    = get_global_function_owner(context, clang_decl);
    const clang::QualType result_type = clang_decl.getResultType();

    std::string    error_message;
    ts_function*   func = nullptr;

    std::tie(error_message, func) = create_function_impl(
        context,
        ts_owner,
        clang_decl
    );

    if(!error_message.empty())
    {
        TS_DUMP_ERROR("Ignored function (" << clang_decl.getQualifiedNameAsString() << ") due to : " << error_message);
    }

    return func;
}

//bool enum_conversion_policy::verify_precondition(const clang_declaration& clang_decl)
//{
//    return basic_policy::verify_precondition(clang_decl);
//}
//
//bool enum_conversion_policy::is_valid(const clang_declaration& clang_decl)
//{
//    const clang::QualType& qual_type = clang_decl.getPromotionType();
//
//    BOOST_ASSERT(qual_type->isBuiltinType() && "underlying type of enumeration is not an integer!");
//
//    std::string           error_message;
//    ts_type::variant_type ts_primitive_type;
//
//    std::tie(error_message, ts_primitive_type) = get_ts_builtin_type_variant(*qual_type->getAs<clang::BuiltinType>());
//
//    if(error_message.empty())
//    {
//        BOOST_ASSERT(
//            (
//                ts_primitive_type.which() == boost::mpl::find<ts_type::type_variant_list, ts_primitive>::type::pos::value
//            ) &&
//            "we got non-primitive type for Thor according to C++ builtin type"
//        );
//
//        switch(boost::get<ts_primitive>(ts_primitive_type))
//        {
//        case ts_primitive::VOID   : error_message = "unexpected underlying type (" "void"      ") for C++ enumeration"; break;
//        case ts_primitive::BOOL   : error_message = "unexpected underlying type (" "bool"      ") for C++ enumeration"; break;
//        case ts_primitive::INT8   : error_message = "unexpected underlying type (" "int8"      ") for C++ enumeration"; break;
//        case ts_primitive::INT16  : error_message = "unexpected underlying type (" "int16"     ") for C++ enumeration"; break;
//        case ts_primitive::INT32  :                                                                                     break;
//        case ts_primitive::INT64  : error_message = "unexpected underlying type (" "int64"     ") for C++ enumeration"; break;
//        case ts_primitive::FLOAT32: error_message = "unexpected underlying type (" "float32"   ") for C++ enumeration"; break;
//        case ts_primitive::FLOAT64: error_message = "unexpected underlying type (" "float64"   ") for C++ enumeration"; break;
//        default                   : error_message = "unexpected underlying type (" "<unknown>" ") for C++ enumeration"; break;
//        }
//    }
//
//    if(!error_message.empty())
//    {
//        TS_DUMP_ERROR(error_message << " : " << clang_decl.getQualifiedNameAsString());
//    }
//
//    return error_message.empty();
//}
//
//auto enum_conversion_policy::create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl) -> ts_declaration*
//{
//    ts_package* ts_pkg            = nullptr;
//    bool        has_ns_scope_only = false;
//
//    std::tie(ts_pkg, has_ns_scope_only) = locate_package(context.root, clang_decl);
//
//    BOOST_ASSERT(ts_pkg && "null pointer exception");
//
//    ts_declaration* ts_decl = &ts_pkg->add_enum(has_ns_scope_only ? clang_decl.getName() : "", false, &clang_decl);
//
//    return ts_decl;
//}
//
//std::string enum_conversion_policy::finish_declaration(const conversion_context_impl& context, const clang_declaration& clang_decl, ts_declaration& ts_decl)
//{
//    for(const clang::EnumConstantDecl* constant_decl: boost::make_iterator_range(clang_decl.enumerator_begin(), clang_decl.enumerator_end()))
//    {
//        BOOST_ASSERT(constant_decl && "null pointer exception");
//
//        std::string name         = constant_decl->getName().str();
//        const auto& ext_value_64 = constant_decl->getInitVal().getZExtValue();
//
//        ts_decl.add_enumerator(std::move(name), static_cast<int>(ext_value_64));
//    }
//
//    return std::string();
//}
//
//bool union_conversion_policy::verify_precondition(const clang_declaration& clang_decl)
//{
//    return  basic_policy::verify_precondition(clang_decl) &&
//            clang_decl.isUnion();
//}
//
//bool union_conversion_policy::is_valid(const clang_declaration& clang_decl)
//{
//    return true;
//}
//
//auto union_conversion_policy::create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl) -> ts_declaration*
//{
//    ts_package* ts_pkg            = nullptr;
//    bool        has_ns_scope_only = false;
//
//    std::tie(ts_pkg, has_ns_scope_only) = locate_package(context.root, clang_decl);
//
//    BOOST_ASSERT(ts_pkg && "null pointer exception");
//
//    ts_declaration* ts_decl = &ts_pkg->add_class(has_ns_scope_only ? clang_decl.getName() : "", false, &clang_decl);
//
//    return ts_decl;
//}
//
//std::string union_conversion_policy::finish_declaration(const conversion_context_impl& context, const clang_declaration& clang_decl, ts_declaration& ts_decl)
//{
//    for(const clang::FieldDecl* field: boost::make_iterator_range(clang_decl.field_begin(), clang_decl.field_end()))
//    {
//        BOOST_ASSERT(field && "null pointer exception");
//
//        std::string        error_message;
//        const std::string& field_name = field->getName().str();
//        ts_type            field_type;
//
//        std::tie(error_message, field_type) = get_ts_type(context, field->getType());
//
//        if(error_message.empty())
//        {
//            typedef std::vector<std::tuple<std::string, ts_type, cxx_no_wrapped>> parameter_type_container;
//
//            create_function_impl(context, *field, "get_" + field_name, field_type                 , parameter_type_container{                                                      }, ts_decl, true);
//            create_function_impl(context, *field, "set_" + field_name, ts_type(ts_primitive::VOID), parameter_type_container{std::make_tuple("value", field_type, cxx_no_wrapped())}, ts_decl, true);
//        }
//        else
//        {
//            TS_DUMP_ERROR("Ignored union value (" << field_name << ") due to : " << error_message);
//        }
//    }
//
//    return std::string();
//}
//
//bool typedef_conversion_policy::verify_precondition(const clang_declaration& clang_decl)
//{
//    return basic_policy::verify_precondition(clang_decl);
//}
//
//bool typedef_conversion_policy::is_valid(const clang_declaration& clang_decl)
//{
//    return true;
//}
//
//auto typedef_conversion_policy::create_declaration(conversion_context_impl& context, const clang_declaration& clang_decl) -> ts_declaration*
//{
//    enum
//    {
//        RENAME_DECLARATION,
//        GENERATE_TYPEDEF  ,
//        IGNORE_TYPEDEF    ,
//    };
//
//    class typedef_decision_visitor : public boost::static_visitor<int>
//    {
//    public:
//        typedef_decision_visitor(const clang::TypedefNameDecl& type_def)
//            : type_def(type_def)
//        {
//        }
//
//        int operator()(const ts_primitive primitive)
//        {
//            return GENERATE_TYPEDEF;
//        }
//
//        int operator()(const ts_builtin builtin)
//        {
//            BOOST_ASSERT(false && "unexpected rename on builtin types!");
//
//            return IGNORE_TYPEDEF;
//        }
//
//        int operator()(const ts_decl* decl)
//        {
//            const auto& decl_name = decl->get_id().get_name();
//
//            if(decl_name.empty())
//            {
//                return RENAME_DECLARATION;
//            }
//            else if(decl_name == type_def.getName())
//            {
//                return IGNORE_TYPEDEF;
//            }
//            else
//            {
//                return GENERATE_TYPEDEF;
//            }
//        }
//
//    private:
//        const clang::TypedefNameDecl& type_def;
//
//    };
//
//    class typedef_rename_visitor : public boost::static_visitor<>
//    {
//    public:
//        explicit typedef_rename_visitor(const clang::TypedefNameDecl& type_def)
//            : type_def(type_def)
//        {
//        }
//
//        result_type operator()(ts_class* cls) const
//        {
//            BOOST_ASSERT(
//                cls->is_global() &&
//                cls->get_owner().which() == (boost::mpl::find<ts_class::owner_variant_list, ts_package*>::type::pos::value) &&
//                "Thor class owner is not a package!"
//            );
//
//            ts_package* owner_package = boost::get<ts_package*>(cls->get_owner());
//
//            BOOST_ASSERT(owner_package && "null pointer exception");
//
//            owner_package->rename_class(type_def.getName(), *cls);
//        }
//
//        result_type operator()(ts_typedef* ts_type_def) const
//        {
//            BOOST_ASSERT(false && "not yet implemented");
//        }
//
//        result_type operator()(ts_enum* e) const
//        {
//            BOOST_ASSERT(false && "not yet implemented");
//        }
//
//        result_type operator()(ts_primitive primitive) const
//        {
//            BOOST_ASSERT(false && "unexpected rename on primitive types!");
//        }
//
//        result_type operator()(ts_builtin builtin) const
//        {
//            BOOST_ASSERT(false && "unexpected rename on builtin types!");
//        }
//
//    private:
//        const clang::TypedefNameDecl& type_def;
//
//    };
//
//    const clang::QualType& underlying_type = clang_decl.getUnderlyingType();
//
//    std::string           error_message;
//    ts_type::variant_type ts_type_variant;
//
//    std::tie(error_message, ts_type_variant) = get_ts_type_variant<false>(context, underlying_type);
//
//    if(error_message.empty())
//    {
//        typedef_decision_visitor decisioner(clang_decl);
//
//        switch(boost::apply_visitor(decisioner, ts_type_variant))
//        {
//        case GENERATE_TYPEDEF:
//            {
//                const clang::DeclContext& decl_context = *clang_decl.getDeclContext();
//
//                ts_package* owner_package     = nullptr;
//                bool        has_ns_scope_only = false;
//
//                std::tie(owner_package, has_ns_scope_only) = locate_package(context.root, clang_decl);
//
//                BOOST_ASSERT(owner_package && "null pointer exception");
//
//                ts_typedef& ts_type_def = owner_package->add_typedef(
//                    has_ns_scope_only ? clang_decl.getName() : "",
//                    ts_type(ts_type_variant),
//                    &clang_decl
//                );
//
//                return &ts_type_def;
//            }
//        case RENAME_DECLARATION:
//            {
//                typedef_rename_visitor renamer(clang_decl);
//
//                boost::apply_visitor(renamer, ts_type_variant);
//            }
//
//            break;
//        }
//    }
//    else
//    {
//        TS_DUMP_ERROR("Ignored typedef due to : " << error_message);
//    }
//
//    return nullptr;
//}

}

CXXInfoCollector::CXXInfoCollector(const clang::ASTContext& ast_context)
    : ast_context(&ast_context)
    , root(nullptr)
    , cxx_ts_class_map()
    , cxx_ts_function_map()
    , anonymous_type_name_requests()
{
    ts_identifier* new_root_2_id = new ts_identifier();
    ts_package*    new_root_2    = new ts_package();

    new_root_2->id = new_root_2_id;

    root = new_root_2;
}

bool CXXInfoCollector::enter_ns(const clang::NamespaceDecl& ns)
{
    BOOST_ASSERT(!ns.isAnonymousNamespace() && "anonymous namespace should be filtered");

    return !ns.isAnonymousNamespace();
}

void CXXInfoCollector::leave_ns(const clang::NamespaceDecl& ns)
{
}

bool CXXInfoCollector::enter_cls(const clang::CXXRecordDecl& cls)
{
    ts_class*const ts_cls = register_impl<class_conversion_policy>(TS_CONVERSION_CONTEXT, cls);

    return ts_cls != nullptr && cls.getDefinition() == &cls;
}

void CXXInfoCollector::leave_cls(const clang::CXXRecordDecl& cls)
{
    complete_impl<class_conversion_policy>(TS_CONVERSION_CONTEXT, cls);
}

bool CXXInfoCollector::register_field(const clang::FieldDecl& field)
{
    ts_getset*const ts_gs = register_impl<getset_conversion_policy>(TS_CONVERSION_CONTEXT, field);

    return ts_gs != nullptr;
}

bool CXXInfoCollector::register_function(const clang::FunctionDecl& function)
{
    ts_function*const ts_func = register_impl<function_conversion_policy>(TS_CONVERSION_CONTEXT, function);

    return ts_func != nullptr;
}

bool CXXInfoCollector::register_typedef(const clang::TypedefNameDecl& type_def)
{
//    ts_typedef*const ts_type_def = register_impl<typedef_conversion_policy>(TS_CONVERSION_CONTEXT, type_def);
//
//    return ts_type_def != nullptr;
    return false;
}

bool CXXInfoCollector::register_enum(const clang::EnumDecl& e)
{
//    ts_enum*const ts_e = register_impl<enum_conversion_policy>(TS_CONVERSION_CONTEXT, e);
//
//    if(ts_e != nullptr)
//    {
//        complete_impl<enum_conversion_policy>(TS_CONVERSION_CONTEXT, e);
//    }
//
//    return ts_e != nullptr;
    return false;
}

bool CXXInfoCollector::register_union(const clang::CXXRecordDecl& u)
{
//    ts_class*const ts_u = register_impl<union_conversion_policy>(TS_CONVERSION_CONTEXT, u);
//
//    if(ts_u != nullptr)
//    {
//        complete_impl<union_conversion_policy>(TS_CONVERSION_CONTEXT, u);
//    }
//
//    return ts_u != nullptr;
    return false;
}

ts_package* CXXInfoCollector::steal_root()
{
    BOOST_ASSERT(root && "null pointer exception");

    ts_package*const result = root;
    root = nullptr;

    return result;
}

bool CXXInfoCollector::has_unhandled_anonymous_type_name_request() const noexcept
{
    return !anonymous_type_name_requests.empty();
}

#undef  TS_CONVERSION_CONTEXT

} } } }
