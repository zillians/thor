/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>
#include <tuple>
#include <utility>
#include <vector>
#include <iostream>
#include <boost/assert.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/scope_exit.hpp>

#include <llvm/Support/Host.h>
#include <llvm/Support/Casting.h>

#include <clang/AST/ASTConsumer.h>

#include <clang/Basic/FileManager.h>
#include <clang/Basic/TargetOptions.h>
#include <clang/Basic/TargetInfo.h>

#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendOptions.h>
#include <clang/Frontend/Utils.h>

#include <clang/Parse/ParseAST.h>

#include "language/stage/import/ClangASTVisitor.h"
#include "language/stage/import/ClangWrapper.h"
#include "language/stage/import/CXXInfoCollector.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/TSInfoPostProcessings.h"
#include "language/stage/import/TSWriter.h"

namespace zillians { namespace language { namespace stage { namespace import {

namespace {

void package_increment_ns_depth(const unsigned depth, ts_package& package)
{
    package.depth += depth;

    for(ts_package* child: package.children)
    {
        BOOST_ASSERT(child && "null pointer exception");

        package_increment_ns_depth(depth, *child);
    }
}

class RootDeclHandler : public clang::ASTConsumer
{
public:
    RootDeclHandler(
        clang::DiagnosticConsumer& diagnostic_consumer,
        CXXInfoCollector& info_collector,
        TSWriter& writer,
        const std::vector<std::string>& ns_prefix,
        const std::vector<std::string>& headers);

    bool HandleTopLevelDecl(clang::DeclGroupRef decls) override;
    void HandleTranslationUnit(clang::ASTContext &Ctx) override;

private:
    clang::DiagnosticConsumer&      diagnostic_consumer;
    CXXInfoCollector&               info_collector;
    TSWriter&                       writer;
    std::vector<std::string>        ns_prefix;
    const std::vector<std::string>* headers;

};

// Implementations
RootDeclHandler::RootDeclHandler(clang::DiagnosticConsumer& diagnostic_consumer, CXXInfoCollector& info_collector, TSWriter& writer, const std::vector<std::string>& ns_prefix, const std::vector<std::string>& headers)
    : diagnostic_consumer(diagnostic_consumer)
    , info_collector(info_collector)
    , writer(writer)
    , ns_prefix(ns_prefix)
    , headers(&headers)
{
}

bool RootDeclHandler::HandleTopLevelDecl(clang::DeclGroupRef decls)
{
    ClangASTVisitor visitor(info_collector);

    for(clang::Decl* decl: decls)
    {
        // TODO Use visitor to recursively visit into it
        // TODO Implement filter to reject declarations from includes

        BOOST_ASSERT(decl && "null pointer exception");

        visitor.TraverseDecl(decl);
    }

    return true;
}

void RootDeclHandler::HandleTranslationUnit(clang::ASTContext &Ctx)
{
    if(diagnostic_consumer.getNumErrors() == 0)
    {
        ts_package* root = info_collector.steal_root();

        BOOST_ASSERT(root && "null pointer exception");

        recursively_rename_classes(*root);

        package_increment_ns_depth(static_cast<unsigned>(ns_prefix.size()), *root);

        for(const std::string& ns: ns_prefix | boost::adaptors::reversed)
        {
            BOOST_ASSERT(root && "null pointer exception");
            BOOST_ASSERT(root->depth >= 0 && "depth of namespace is incorrect!");

            ts_identifier* new_root_id = new ts_identifier();
            ts_package*    new_root    = new ts_package();

            new_root->id    = new_root_id;
            new_root->depth = root->depth - 1;

            new_root->children.insert(root);

            root->id->name = ns;
            root->scope    = new_root;

            root = new_root;
        }

        BOOST_SCOPE_EXIT((root))
        {
            root->wrapped_includes = nullptr;
        } BOOST_SCOPE_EXIT_END;

        root->wrapped_includes = headers;

        writer.write(*root);
    }

    ts_node::clean();
}

std::tuple<boost::filesystem::path, bool> generate_temporary_main_file(const std::vector<std::string>& headers)
{
    boost::filesystem::path main_file_path = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path().replace_extension(".cpp");
    boost::filesystem::ofstream main_file_stream(main_file_path);

    for(const std::string& header: headers)
    {
        main_file_stream << "#include <" << header << '>' << std::endl;
    }

    main_file_stream.flush();

    return std::make_tuple(std::move(main_file_path), static_cast<bool>(main_file_stream));
}

}

bool ClangWrapper::parse(
    const std::vector<std::string>& include_directories,
    const std::vector<std::string>& target_pkg,
    const std::vector<std::string>& headers,
    const boost::filesystem::path& output_dir)
{
    bool                    is_main_file_valid = false;
    boost::filesystem::path main_file_path;

    std::tie(main_file_path, is_main_file_valid) = generate_temporary_main_file(headers);

    if(!is_main_file_valid)
    {
        std::cerr << "failed to generate temporary main file for parsing" << std::endl;

        return false;
    }

    BOOST_SCOPE_EXIT((main_file_path))
    {
        boost::system::error_code ec;

        boost::filesystem::remove(main_file_path, ec);
    } BOOST_SCOPE_EXIT_END;

    {
        clang::CompilerInstance compiler;
        clang::TargetOptions    target_options;

        compiler.getLangOpts().CPlusPlus = 1;
        compiler.getLangOpts().Bool      = 1;

        compiler.createDiagnostics(0, nullptr);

        target_options.Triple = llvm::sys::getDefaultTargetTriple();
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
        compiler.setTarget(clang::TargetInfo::CreateTargetInfo(compiler.getDiagnostics(), &target_options));
#else
        compiler.setTarget(clang::TargetInfo::CreateTargetInfo(compiler.getDiagnostics(), target_options));
#endif
        compiler.createFileManager();
        compiler.createSourceManager(compiler.getFileManager());
        compiler.createPreprocessor();
        compiler.createASTContext();

        if(!include_directories.empty())
        {
            clang::HeaderSearchOptions header_search_options;
            clang::PreprocessorOptions preprocessor_options;
            clang::FrontendOptions frontend_options;

            // TODO handle fixed include pathes from compiler
            for(const std::string& include_directory: include_directories)
            {
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
                header_search_options.AddPath(include_directory, clang::frontend::Angled, true, false, false);
#else
                header_search_options.AddPath(include_directory, clang::frontend::Angled, true, false, false);
#endif
            }

            clang::InitializePreprocessor(compiler.getPreprocessor(), preprocessor_options, header_search_options, frontend_options);
        }

        compiler.getSourceManager().createMainFileID(compiler.getFileManager().getFile(main_file_path.native()));

        compiler.getDiagnosticClient().BeginSourceFile(
            compiler.getLangOpts(),
            &compiler.getPreprocessor()
        );

        CXXInfoCollector info_collector(compiler.getASTContext());
        TSWriter         writer(output_dir);
        RootDeclHandler  root_decl_handler(compiler.getDiagnosticClient(), info_collector, writer, target_pkg, headers);

        clang::ParseAST(
            compiler.getPreprocessor(),
            &root_decl_handler,
            compiler.getASTContext(),
            false,
            clang::TU_Complete,
            nullptr,
            true
        );

        compiler.getDiagnosticClient().EndSourceFile();

        return compiler.getDiagnosticClient().getNumErrors() == 0;
    }
}

} } } }
