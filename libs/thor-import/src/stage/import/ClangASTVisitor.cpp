/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iostream>
#include <string>

#include <boost/assert.hpp>
#include <boost/noncopyable.hpp>

#include <clang/AST/Decl.h>
#include <clang/AST/DeclCXX.h>
#include <clang/AST/DeclTemplate.h>

#include "language/stage/import/ClangASTVisitor.h"
#include "language/stage/import/CXXInfoCollector.h"

namespace zillians { namespace language { namespace stage { namespace import {

namespace {

template<
    typename clang_type,
    bool (CXXInfoCollector::* enter_func)(const clang_type&),
    void (CXXInfoCollector::* leave_func)(const clang_type&)
>
class AutoScope : boost::noncopyable {
public:
    AutoScope(CXXInfoCollector& info_collector, const clang_type& clang_decl)
        : info_collector(info_collector)
        , clang_decl(clang_decl)
        , entered((info_collector.*enter_func)(clang_decl))
    {
    }

    ~AutoScope()
    {
        if(entered)
        {
            (info_collector.*leave_func)(clang_decl);
        }
    }

    bool is_entered() const
    {
        return entered;
    }

private:
    CXXInfoCollector& info_collector;
    const clang_type& clang_decl;
    bool entered;

};

}

ClangASTVisitor::ClangASTVisitor(CXXInfoCollector& info_collector)
    : info_collector(info_collector)
{
}

bool ClangASTVisitor::TraverseNamespaceDecl(clang::NamespaceDecl* ns)
{
    BOOST_ASSERT(ns && "null pointer exception");

    const AutoScope<
        clang::NamespaceDecl,
        &CXXInfoCollector::enter_ns,
        &CXXInfoCollector::leave_ns
    > auto_scope(info_collector, *ns);

    if(auto_scope.is_entered())
    {
        return BaseVisitor::TraverseNamespaceDecl(ns);
    }
    else
    {
        return true;
    }
}

bool ClangASTVisitor::TraverseCXXRecordDecl(clang::CXXRecordDecl* record)
{
    BOOST_ASSERT(record && "null pointer exception");

    if(record->isAnonymousStructOrUnion())
    {
        return BaseVisitor::TraverseCXXRecordDecl(record);
    }
    else if(record->isClass() || record->isStruct())
    {
        const AutoScope<
            clang::CXXRecordDecl,
            &CXXInfoCollector::enter_cls,
            &CXXInfoCollector::leave_cls
        > auto_scope(info_collector, *record);

        if(auto_scope.is_entered())
        {
            return BaseVisitor::TraverseCXXRecordDecl(record);
        }
    }
    else if(record->isUnion())
    {
        if(info_collector.register_union(*record))
        {
            return BaseVisitor::TraverseCXXRecordDecl(record);
        }
    }

    return true;
}

bool ClangASTVisitor::TraverseClassTemplateSpecializationDecl(clang::ClassTemplateSpecializationDecl* specialization)
{
    return true;
}

bool ClangASTVisitor::VisitFieldDecl(clang::FieldDecl* field)
{
    BOOST_ASSERT(field && "null pointer exception");

    info_collector.register_field(*field);

    return BaseVisitor::VisitFieldDecl(field);
}

bool ClangASTVisitor::VisitEnumDecl(clang::EnumDecl* e)
{
    BOOST_ASSERT(e && "null pointer exception");

    info_collector.register_enum(*e);

    return BaseVisitor::VisitEnumDecl(e);
}

bool ClangASTVisitor::VisitTypedefDecl(clang::TypedefDecl* type_def)
{
    info_collector.register_typedef(*type_def);

    return BaseVisitor::VisitTypedefDecl(type_def);
}

bool ClangASTVisitor::VisitFunctionDecl(clang::FunctionDecl* function)
{
    BOOST_ASSERT(function && "null pointer exception");

    info_collector.register_function(*function);

    return BaseVisitor::VisitFunctionDecl(function);
}

} } } }
