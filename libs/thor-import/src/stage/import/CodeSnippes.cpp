/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/stage/import/CodeSnippes.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace code_snippes {

const char* TO_STRING =
R"THOR-IMPORT(
#include <clocale>
#include <cwchar>
#include <string>
#include <locale>

namespace {

std::string to_string_$suffix$(const ::thor::lang::String& str_$suffix$)
{
    std::string result_$suffix$;

    const std::wstring& data_$suffix$ = *str_$suffix$.data;

    if (!data_$suffix$.empty())
    {
        typedef std::codecvt<wchar_t, char, std::mbstate_t> codecvt_$suffix$;

        const std::locale locale_$suffix$("");
        auto& facet_$suffix$ = std::use_facet<codecvt_$suffix$>(locale_$suffix$);

        result_$suffix$.resize(data_$suffix$.size() * facet_$suffix$.max_length(), '\0');

        const wchar_t*const from_begin_$suffix$ = data_$suffix$.c_str();
        const wchar_t*const from_end_$suffix$   = from_begin_$suffix$ + data_$suffix$.size();
        const wchar_t*      from_next_$suffix$  = nullptr;

        char*const to_begin_$suffix$ = &result_$suffix$[0];
        char*const to_end_$suffix$   = to_begin_$suffix$ + result_$suffix$.size();
        char*      to_next_$suffix$  = nullptr;

        std::mbstate_t mb_$suffix$ = std::mbstate_t();
        const auto convert_result_$suffix$ = facet_$suffix$.out(
            mb_$suffix$,
            from_begin_$suffix$, from_end_$suffix$, from_next_$suffix$,
              to_begin_$suffix$,   to_end_$suffix$,   to_next_$suffix$
        );

        result_$suffix$.resize(static_cast<std::size_t>(std::distance(to_begin_$suffix$, to_next_$suffix$)));
    }

    return result_$suffix$;
}

}
)THOR-IMPORT";

} } } } }
