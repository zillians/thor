/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxGetSetGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_getter_array_size_decl_grammar<iterator>::cxx_getter_array_size_decl_grammar() : cxx_getter_array_size_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::at_c;
    using boost::phoenix::construct;

    start = size.alias();

    size =
           lit(CXX_INT64_NAME)
        << ' '
        << optinal_scope[_1 = construct<boost::fusion::vector<ts_class*, bool>>
                              (
                                  helpers::ts_decl_member_of(at_c<0>(_val)),
                                  at_c<1>(_val)
                              )
                        ]
        << "get_"
        << string       [_1 = helpers::ts_decl_wrapped_name(at_c<0>(_val))]
        << "_size()"
    ;
}

template<typename iterator>
cxx_getter_array_size_def_grammar<iterator>::cxx_getter_array_size_def_grammar() : cxx_getter_array_size_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = size.alias();

    size =
           size_decl[_1 = construct<boost::fusion::vector<ts_getset*, bool>>(_val, true)] << eol
        << size_body[_1 = _val]
    ;

    size_body =
           '{' << eol
        << "    static_assert(" << size_expression[_1 = _val] << " <= static_cast<std::make_unsigned< " << CXX_INT64_NAME << ">::type>(std::numeric_limits< " << CXX_INT64_NAME << ">::max()), \"array index is out of language limitation\");" << eol
        << "    return static_cast< " << CXX_INT64_NAME << ">(" << size_expression[_1 = _val] << ");" << eol
        << '}' << eol
    ;

    size_expression =
           "sizeof("
        << size_array[_1 = _val]
        << ") / sizeof(*("
        << size_array[_1 = _val]
        << "))"
    ;

    size_array =
           lit(CXX_VARIABLE_IMPL_NAME)
        << "->"
        << string[_1 = helpers::ts_decl_wrapped_name(_val)]
    ;
}

template<typename iterator>
cxx_getter_decl_grammar<iterator>::cxx_getter_decl_grammar() : cxx_getter_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::at_c;
    using boost::phoenix::construct;

    start = getter.alias();

    getter =
           type           [_1 = helpers::ts_getset_type(at_c<0>(_val))]
        << ' '
        << optinal_scope  [_1 = construct<boost::fusion::vector<ts_class*, bool>>
                                (
                                    helpers::ts_decl_member_of(at_c<0>(_val)),
                                    at_c<1>(_val)
                                )
                          ]
        << "get_"
        << string         [_1 = helpers::ts_decl_wrapped_name(at_c<0>(_val))]
        << '('
        << extra_arguments[_1 = at_c<0>(_val)]
        << ')'
    ;

    extra_arguments =
          eps(helpers::ts_getset_has_getter_array_element(_val)) << extra_arguments_array_element
        | eps
    ;

    extra_arguments_array_element =
           lit(CXX_INT64_NAME)
        << " index_"
        << CXX_LOCAL_VARIABLE_SUFFIX
    ;
}

template<typename iterator>
cxx_getter_def_grammar<iterator>::cxx_getter_def_grammar() : cxx_getter_def_grammar::base_type(start), type(false)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = getter.alias();

    getter =
           getter_decl[_1 = construct<boost::fusion::vector<ts_getset*, bool>>(_val, true)] << eol
        << getter_body[_1 = _val]
    ;

    getter_field =
           lit(CXX_VARIABLE_IMPL_NAME)
        << "->"
        << string[_1 = helpers::ts_decl_wrapped_name(_val)]
        << -(
                 eps(helpers::ts_getset_has_getter_array_element(_val))
              << "[index_" << CXX_LOCAL_VARIABLE_SUFFIX << ']'
            )
    ;

    getter_body =
           '{' << eol
        << (
               eps(helpers::ts_getset_has_getter_by_pass   (_val)) << getter_body_impl_by_pass
             | eps(helpers::ts_getset_has_getter_address_of(_val)) << getter_body_impl_address_of
           )
        << '}' << eol
    ;

    getter_body_impl_by_pass =
        "    return " << getter_field << ';' << eol
    ;

    // Code should be like the following psuedo code
    //
    // field_wrapper_class* wrapper_class::get()
    // {
    //     auto* temp = field_wrapper_class::create();
    //     temp->set(&impl->field, false);
    //     return temp;
    // }
    getter_body_impl_address_of =
           lit("    auto* temp_")
        << CXX_LOCAL_VARIABLE_SUFFIX
        << " = "
        << type[_1 = helpers::ts_getset_type(_val)]
        << "::"
        << CXX_FUNCTION_CREATOR_PIMPL_NAME
        << "();"
        << eol
        << "    temp_"
        << CXX_LOCAL_VARIABLE_SUFFIX
        << "->"
        << CXX_FUNCTION_SET_PIMPL_NAME
        << "(&"
        << getter_field[_1 = _val]
        << ", false);"
        << eol
        << "    return temp_"
        << CXX_LOCAL_VARIABLE_SUFFIX
        << ';'
        << eol
    ;
}

template<typename iterator>
cxx_setter_decl_grammar<iterator>::cxx_setter_decl_grammar() : cxx_setter_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::at_c;
    using boost::phoenix::construct;

    start = setter.alias();

    setter =
           "void "
        << optinal_scope  [_1 = construct<boost::fusion::vector<ts_class*, bool>>
                                (
                                    helpers::ts_decl_member_of(at_c<0>(_val)),
                                    at_c<1>(_val)
                                )
                          ]
        << "set_"
        << string         [_1 = helpers::ts_decl_wrapped_name(at_c<0>(_val))]
        << '('
        << type           [_1 = helpers::ts_getset_type(at_c<0>(_val))]
        << ' '
        << "new_"
        << string         [_1 = helpers::ts_decl_wrapped_name(at_c<0>(_val))]
        << '_'
        << CXX_LOCAL_VARIABLE_SUFFIX
        << extra_arguments[_1 = at_c<0>(_val)]
        << ')'
    ;

    extra_arguments =
          eps(helpers::ts_getset_has_getter_array_element(_val)) << extra_arguments_array_element
        | eps
    ;

    extra_arguments_array_element =
           lit(", ")
        << CXX_INT64_NAME
        << " index_"
        << CXX_LOCAL_VARIABLE_SUFFIX
    ;
}

template<typename iterator>
cxx_setter_def_grammar<iterator>::cxx_setter_def_grammar() : cxx_setter_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = setter.alias();

    setter =
           setter_decl[_1 = construct<boost::fusion::vector<ts_getset*, bool>>(_val, true)] << eol
        << setter_body[_1 = _val]
    ;

    setter_field =
           lit(CXX_VARIABLE_IMPL_NAME)
        << "->"
        << string[_1 = helpers::ts_decl_wrapped_name(_val)]
        << -(
                 eps(helpers::ts_getset_has_getter_array_element(_val))
              << "[index_" << CXX_LOCAL_VARIABLE_SUFFIX << ']'
            )
    ;

    setter_param =
           "new_"
        << string[_1 = helpers::ts_decl_wrapped_name(_val)]
        << '_'
        << CXX_LOCAL_VARIABLE_SUFFIX
    ;

    setter_body =
           '{' << eol
        << (
               eps(helpers::ts_getset_has_setter_by_pass    (_val)) << setter_body_impl_by_pass
             | eps(helpers::ts_getset_has_setter_copy_assign(_val)) << setter_body_impl_copy_assign
           )
        << '}' << eol
    ;

    setter_body_impl_by_pass =
           "    "
        << setter_field[_1 = _val]
        << " = "
        << setter_param[_1 = _val]
        << ';'
        << eol
    ;

    setter_body_impl_copy_assign =
           "    "
        << setter_field[_1 = _val]
        << " = *"
        << setter_param[_1 = _val]
        << "->"
        << CXX_FUNCTION_GET_PIMPL_NAME
        << "();"
        << eol
    ;
}

template<typename iterator>
cxx_getset_decl_grammar<iterator>::cxx_getset_decl_grammar() : cxx_getset_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::eol;

    using boost::phoenix::construct;

    start = getset.alias();

    getset =
           -(eps(helpers::ts_getset_has_getter           (_val)) << "    " << getter           [_1 = construct<boost::fusion::vector<ts_getset*, bool>>(_val, false)] << ';' << eol)
        << -(eps(helpers::ts_getset_has_getter_array_size(_val)) << "    " << getter_array_size[_1 = construct<boost::fusion::vector<ts_getset*, bool>>(_val, false)] << ';' << eol)
        << -(eps(helpers::ts_getset_has_setter           (_val)) << "    " << setter           [_1 = construct<boost::fusion::vector<ts_getset*, bool>>(_val, false)] << ';' << eol)
        << eol
    ;
}

template<typename iterator>
cxx_getset_def_grammar<iterator>::cxx_getset_def_grammar() : cxx_getset_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    using boost::phoenix::construct;

    start = getset.alias();

    getset =
           -(eps(helpers::ts_getset_has_getter           (_val)) << getter           [_1 = _val] << eol)
        << -(eps(helpers::ts_getset_has_getter_array_size(_val)) << getter_array_size[_1 = _val] << eol)
        << -(eps(helpers::ts_getset_has_setter           (_val)) << setter           [_1 = _val] << eol)
        << eol
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_getter_array_size_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_getter_array_size_def_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_getter_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_getter_def_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_setter_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_setter_def_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_getset_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_getset_def_grammar)

} } } } }
