/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/TsPimplGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_pimpl_grammar<iterator>::ts_pimpl_grammar() : ts_pimpl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::lit;

    start = pimpl.alias();

    pimpl =
           creator
        << eol
        << variables
    ;

    creator =
           "    private static function "
        << lit(CXX_FUNCTION_CREATOR_PIMPL_NAME)
        << "():"
        << identifier[_1 = helpers::ts_decl_id(_val)]
        << eol
        << "    {" << eol
        << "        return new " << identifier[_1 = helpers::ts_decl_id(_val)] << "();" << eol
        << "    }" << eol
    ;

    variables =
           lit("    private var internal_1_") << CXX_MEMBER_VARIABLE_SUFFIX << ": int8;" << eol
        << lit("    private var internal_2_") << CXX_MEMBER_VARIABLE_SUFFIX << ": thor.unmanaged.ptr_<int8>;" << eol
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_pimpl_grammar)

} } } } }
