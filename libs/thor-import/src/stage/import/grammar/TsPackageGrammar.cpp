/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/TsPackageGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_package_grammar<iterator>::ts_package_grammar()
    : ts_package_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    start = package.alias();

    package =
           "import thor.lang;"      << eol
        << "import thor.unmanaged;" << eol
        << eol
        << (*import)[_1 = helpers::ts_package_refered_packages(_val)]
        << eol
        << (
             *(
                  cls
                | function
              )
           )[_1 = helpers::ts_package_declarations(_val)]
    ;

    cls =
        cls_impl << eol
    ;

    function =
        function_impl << eol
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_package_grammar)

} } } } }
