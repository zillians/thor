/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/TsClassGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_class_grammar<iterator>::ts_class_grammar() : ts_class_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;

    start = cls.alias();

    cls =
           "@native" << eol
        << "class " << identifier[_1 = helpers::ts_decl_id(_val)] << eol
        << "    extends " << TS_THOR_LANG_OBJECT_NAME << eol
        << '{' << eol
        << (*(function << eol))[_1 = helpers::ts_class_constructors(_val)]
        << (*(function << eol))[_1 = helpers::ts_class_destructors (_val)]
        << (*(getset   << eol))[_1 = helpers::ts_class_getsets     (_val)]
        << pimpl[_1 = _val]
        << '}' << eol
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_class_grammar)

} } } } }
