
/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"
#include "language/stage/import/grammar/TsGetSetGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
ts_getter_array_size_grammar<iterator>::ts_getter_array_size_grammar() : ts_getter_array_size_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = size.alias();

    size =
           "    @native" << eol
        << "    public function get_"
        << string[_1 = helpers::ts_decl_wrapped_name(_val)]
        << "_size():"
        << TS_INT64_NAME
        << ';'
    ;
}

template<typename iterator>
ts_getter_grammar<iterator>::ts_getter_grammar() : ts_getter_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = getter.alias();

    getter =
           "    @native" << eol
        << "    public function get_"
        << string         [_1 = helpers::ts_decl_wrapped_name(_val)]
        << '('
        << extra_arguments[_1 = _val]
        << "):"
        << type           [_1 = construct<boost::fusion::vector<ts_type, ts_package*>>
                                (
                                    helpers::ts_getset_type(_val),
                                    helpers::ts_decl_get_package(_val)
                                )
                          ]
        << ';'
    ;

    extra_arguments =
          eps(helpers::ts_getset_has_getter_array_element(_val)) << extra_arguments_array_element
        | eps
    ;

    extra_arguments_array_element =
           lit("index_")
        << CXX_LOCAL_VARIABLE_SUFFIX
        << ':'
        << lit(TS_INT64_NAME)
    ;
}

template<typename iterator>
ts_setter_grammar<iterator>::ts_setter_grammar() : ts_setter_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::lit;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = setter.alias();

    setter =
           "    @native" << eol
        << "    public function set_"
        << string         [_1 = helpers::ts_decl_wrapped_name(_val)]
        << "(new_"
        << string         [_1 = helpers::ts_decl_wrapped_name(_val)]
        << ':'
        << type           [_1 = construct<boost::fusion::vector<ts_type, ts_package*>>
                                (
                                    helpers::ts_getset_type(_val),
                                    helpers::ts_decl_get_package(_val)
                                )
                          ]
        << extra_arguments[_1 = _val]
        << "):void;"
    ;

    extra_arguments =
          eps(helpers::ts_getset_has_getter_array_element(_val)) << extra_arguments_array_element
        | eps
    ;

    extra_arguments_array_element =
           lit(", index_")
        << CXX_LOCAL_VARIABLE_SUFFIX
        << ':'
        << TS_INT64_NAME
    ;
}

template<typename iterator>
ts_getset_grammar<iterator>::ts_getset_grammar() : ts_getset_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    start = getset.alias();

    getset =
           -(eps(helpers::ts_getset_has_getter           (_val)) << getter           [_1 = _val] << eol)
        << -(eps(helpers::ts_getset_has_getter_array_size(_val)) << getter_array_size[_1 = _val] << eol)
        << -(eps(helpers::ts_getset_has_setter           (_val)) << setter           [_1 = _val] << eol)
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_getter_array_size_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_getter_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_setter_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(ts_getset_grammar)

} } } } }
