/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/phoenix_function.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace helpers {

std::string cxx_name_impl::to_string(ts_primitive primitive)
{
    switch(primitive)
    {
    case ts_primitive::VOID   : return CXX_VOID_NAME;
    case ts_primitive::BOOL   : return CXX_BOOL_NAME;
    case ts_primitive::INT8   : return CXX_INT8_NAME;
    case ts_primitive::INT16  : return CXX_INT16_NAME;
    case ts_primitive::INT32  : return CXX_INT32_NAME;
    case ts_primitive::INT64  : return CXX_INT64_NAME;
    case ts_primitive::FLOAT32: return CXX_FLOAT32_NAME;
    case ts_primitive::FLOAT64: return CXX_FLOAT64_NAME;
    default                   : return CXX_INVALID_TYPE_NAME;
    }
}

std::string cxx_name_impl::to_string(ts_builtin builtin)
{
    switch(builtin)
    {
    case ts_builtin::C_STRING : return CXX_THOR_LANG_STRING_NAME;
    case ts_builtin::C_WSTRING: return CXX_THOR_LANG_STRING_NAME;
    default                   : return CXX_INVALID_TYPE_NAME;
    }
}

std::string ts_name_impl::to_string(ts_primitive primitive)
{
    switch(primitive)
    {
    case ts_primitive::VOID   : return TS_VOID_NAME;
    case ts_primitive::BOOL   : return TS_BOOL_NAME;
    case ts_primitive::INT8   : return TS_INT8_NAME;
    case ts_primitive::INT16  : return TS_INT16_NAME;
    case ts_primitive::INT32  : return TS_INT32_NAME;
    case ts_primitive::INT64  : return TS_INT64_NAME;
    case ts_primitive::FLOAT32: return TS_FLOAT32_NAME;
    case ts_primitive::FLOAT64: return TS_FLOAT64_NAME;
    default                   : return TS_INVALID_TYPE_NAME;
    }
}

std::string ts_name_impl::to_string(ts_builtin builtin)
{
    switch(builtin)
    {
    case ts_builtin::C_STRING : return TS_THOR_LANG_STRING_NAME;
    case ts_builtin::C_WSTRING: return TS_THOR_LANG_STRING_NAME;
    default                   : return TS_INVALID_TYPE_NAME;
    }
}

boost::phoenix::function<replace_non_alnum_impl>                                            replace_non_alnum;
boost::phoenix::function<cxx_name_impl>                                                     cxx_name;
boost::phoenix::function<ts_name_impl>                                                      ts_name;
boost::phoenix::function<ts_type_is_none_impl>                                              ts_type_is_none;
boost::phoenix::function<ts_type_is_primitive_impl>                                         ts_type_is_primitive;
boost::phoenix::function<ts_type_is_builtin_impl>                                           ts_type_is_builtin;
boost::phoenix::function<ts_type_is_class_impl>                                             ts_type_is_class;
boost::phoenix::function<ts_type_get_primitive_impl>                                        ts_type_get_primitive;
boost::phoenix::function<ts_type_get_builtin_impl>                                          ts_type_get_builtin;
boost::phoenix::function<ts_type_get_class_impl>                                            ts_type_get_class;
boost::phoenix::function<ts_identifier_name_impl>                                           ts_identifier_name;
boost::phoenix::function<ts_decl_scope_impl>                                                ts_decl_scope;
boost::phoenix::function<ts_decl_member_of_impl>                                            ts_decl_member_of;
boost::phoenix::function<ts_decl_id_impl>                                                   ts_decl_id;
boost::phoenix::function<ts_decl_wrapped_name_impl>                                         ts_decl_wrapped_name;
boost::phoenix::function<ts_decl_get_package_impl>                                          ts_decl_get_package;
boost::phoenix::function<ts_function_result_type_impl>                                      ts_function_result_type;
boost::phoenix::function<ts_function_is_result_wrapping_impl<ts_function::BY_PASS>>         ts_function_is_result_wrapping_by_pass;
boost::phoenix::function<ts_function_is_result_wrapping_impl<ts_function::COPY_CREATE>>     ts_function_is_result_wrapping_copy_create;
boost::phoenix::function<ts_function_parameters_impl>                                       ts_function_parameters;
boost::phoenix::function<ts_function_is_constructor_impl>                                   ts_function_is_constructor;
boost::phoenix::function<ts_function_is_destructor_impl>                                    ts_function_is_destructor;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter              >>    ts_getset_has_getter;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_by_pass      >>    ts_getset_has_getter_by_pass;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_address_of   >>    ts_getset_has_getter_address_of;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_array_element>>    ts_getset_has_getter_array_element;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_getter_array_size   >>    ts_getset_has_getter_array_size;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter              >>    ts_getset_has_setter;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter_by_pass      >>    ts_getset_has_setter_by_pass;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter_copy_assign  >>    ts_getset_has_setter_copy_assign;
boost::phoenix::function<ts_getset_condition_impl<&ts_getset::has_setter_array_element>>    ts_getset_has_setter_array_element;
boost::phoenix::function<ts_getset_type_impl>                                               ts_getset_type;
boost::phoenix::function<ts_class_constructors_impl>                                        ts_class_constructors;
boost::phoenix::function<ts_class_destructors_impl>                                         ts_class_destructors;
boost::phoenix::function<ts_class_getsets_impl>                                             ts_class_getsets;
boost::phoenix::function<ts_parameter_type_impl>                                            ts_parameter_type;
boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::BY_PASS  >>            ts_parameter_is_wrapping_by_pass;
boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::VALUE    >>            ts_parameter_is_wrapping_value;
boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::REFERENCE>>            ts_parameter_is_wrapping_reference;
boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::POINTER  >>            ts_parameter_is_wrapping_pointer;
boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::C_STRING >>            ts_parameter_is_wrapping_c_string;
boost::phoenix::function<ts_parameter_is_wrapping_impl<ts_parameter::C_WSTRING>>            ts_parameter_is_wrapping_c_wstring;
boost::phoenix::function<ts_package_declarations_impl>                                      ts_package_declarations;
boost::phoenix::function<ts_package_refered_packages_impl>                                  ts_package_refered_packages;
boost::phoenix::function<ts_package_refered_package_pairs_impl>                             ts_package_refered_package_pairs;
boost::phoenix::function<ts_package_has_used_builtins_impl<ts_builtin::C_STRING>>           ts_package_has_used_builtins_c_string;
boost::phoenix::function<ts_package_depth_impl>                                             ts_package_depth;
boost::phoenix::function<ts_package_get_common_parent_impl>                                 ts_package_get_common_parent;
boost::phoenix::function<ts_package_get_wrapped_includes_impl>                              ts_package_get_wrapped_includes;

} } } } }
