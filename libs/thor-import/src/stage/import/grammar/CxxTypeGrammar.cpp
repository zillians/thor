/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxTypeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_type_grammar<iterator>::cxx_type_grammar(bool append_star/* = true*/) : cxx_type_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::string;

    start = type.alias();

    type =
          cls
        | primitive
        | builtin
        | eps(false)
    ;

    if(append_star)
    {
        cls =
            qualified_identifier << '*'
        ;
    }
    else
    {
        cls =
            qualified_identifier
        ;
    }

    primitive =
        string[_1 = helpers::cxx_name(_val)]
    ;

    builtin =
           string[_1 = helpers::cxx_name(_val)]
        << (
               eps(_val == ts_builtin::C_STRING || _val == ts_builtin::C_WSTRING) << '*'
             | eps
           )
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_type_grammar)

} } } } }
