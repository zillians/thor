/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/fusion/container/vector.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/Constants.h"
#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxClassGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_class_fwd_grammar<iterator>::cxx_class_fwd_grammar()
    : cxx_class_fwd_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;

    start = cls.alias();

    cls =
        "class " << identifier[_1 = helpers::ts_decl_id(_val)] << ';' << eol
    ;
}

template<typename iterator>
cxx_class_decl_grammar<iterator>::cxx_class_decl_grammar() : cxx_class_decl_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;
    using boost::spirit::karma::string;

    using boost::phoenix::construct;

    start = cls.alias();

    cls =
           "class " << identifier[_1 = helpers::ts_decl_id(_val)] << eol
        << "    : public " << CXX_THOR_LANG_OBJECT_NAME << eol
        << '{' << eol
        << functions[_1 = helpers::ts_class_constructors(_val)]
        << functions[_1 = helpers::ts_class_destructors (_val)]
        << (*getset)[_1 = helpers::ts_class_getsets     (_val)]
        << pimpl    [_1 = _val]
        << "};" << eol
    ;

    functions =
          (
               "public:" << eol
            << +("    " << function << ';' << eol)
            << eol
          )
        | eps
    ;

    function =
        function_impl[_1 = construct<boost::fusion::vector<ts_function*, bool>>(_val, false)]
    ;

    pimpl =
           "public:" << eol
        << "    " << pimpl_getter [_1 = construct<boost::fusion::vector<ts_class*, bool>>(_val, false)] << ';' << eol
        << "    " << pimpl_setter [_1 = construct<boost::fusion::vector<ts_class*, bool>>(_val, false)] << ';' << eol
        << "    " << pimpl_creator[_1 = _val] << ';' << eol
        << "private:" << eol
        << pimpl_variables
        << eol
    ;

    pimpl_variables =
           "    bool " << eps << CXX_VARIABLE_OWN_IMPL_NAME << ';' << eol
        << "    " << string[_1 = helpers::ts_decl_wrapped_name(_val)] << "* " << CXX_VARIABLE_IMPL_NAME << ';' << eol
    ;
}

template<typename iterator>
cxx_class_def_grammar<iterator>::cxx_class_def_grammar() : cxx_class_def_grammar::base_type(start)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;

    start = cls.alias();

    cls =
           (*function)[_1 = helpers::ts_class_constructors(_val)]
        << (*function)[_1 = helpers::ts_class_destructors (_val)]
        << (*getset  )[_1 = helpers::ts_class_getsets     (_val)]
        << pimpl_getter[_1 = _val]
        << pimpl_setter[_1 = _val]
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_class_fwd_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_class_decl_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_class_def_grammar)

} } } } }
