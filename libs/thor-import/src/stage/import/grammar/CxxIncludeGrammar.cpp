/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <string>

#include <boost/spirit/include/karma.hpp>
#include <boost/spirit/include/phoenix.hpp>

#include "language/stage/import/TSInfo.h"
#include "language/stage/import/grammar/CxxIncludeGrammar.h"
#include "language/stage/import/grammar/ExpectedIterator.h"
#include "language/stage/import/grammar/Helpers.h"
#include "language/stage/import/grammar/IdentifierGrammar.h"

namespace zillians { namespace language { namespace stage { namespace import { namespace grammar {

template<typename iterator>
cxx_include_grammar<iterator>::cxx_include_grammar(const std::string& header_name)
    : cxx_include_grammar::base_type(start)
    , header_name(header_name)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_a;
    using boost::spirit::karma::_val;
    using boost::spirit::karma::eol;
    using boost::spirit::karma::eps;

    using boost::phoenix::at_c;
    using boost::phoenix::construct;

    start = include.alias();

    include =
           "#include \""
        << relative_path
        << header_name
        << "\""
        << eol
    ;

    relative_path =
           eps[_a = helpers::ts_package_get_common_parent(at_c<0>(_val), at_c<1>(_val))]
        << relative_backward[_1 = helpers::ts_package_depth(at_c<0>(_val)) - helpers::ts_package_depth(_a)]
        << relative_forward [_1 = construct<pair_type>(_a, at_c<1>(_val))]
    ;

    relative_backward =
           eps[_a = 0u]
        << *(
                 eps(_a != _val)
              << "../"
              << eps[++_a]
            )
    ;

    relative_forward =
        -(
              eps(at_c<0>(_val) != at_c<1>(_val))
           << relative_forward[_1 = construct<pair_type>(at_c<0>(_val), helpers::ts_decl_scope(at_c<1>(_val)))]
           << identifier      [_1 = helpers::ts_decl_id(at_c<1>(_val))]
           << '/'
         )
    ;
}

template<typename iterator>
cxx_refers_grammar<iterator>::cxx_refers_grammar(const std::string& header_name)
    : cxx_refers_grammar::base_type(start)
    , include(header_name)
{
    using boost::spirit::karma::_1;
    using boost::spirit::karma::_val;

    start = refers.alias();

    refers =
        refers_impl[_1 = helpers::ts_package_refered_package_pairs(_val)]
    ;

    refers_impl =
        *include
    ;
}

TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_include_grammar)
TS_IMPORT_GRAMMAR_INSTANTIATE_TEMPLATES(cxx_refers_grammar )

} } } } }
