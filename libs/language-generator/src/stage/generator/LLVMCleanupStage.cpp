/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "language/context/ParserContext.h"
#include "language/context/GeneratorContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/ASTNodeHelper.h"

#include "language/stage/generator/context/DebugInfoContext.h"
#include "language/stage/generator/context/SynthesizedAllocaThisPointerContext.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedObjectLayoutContext.h"
#include "language/stage/generator/context/SynthesizedTypeContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/context/SynthesizedVirtualFunctionContext.h"
#include "language/stage/generator/context/SynthesizedVTableContext.h"
#include "language/stage/generator/LLVMCleanupStage.h"

namespace zillians { namespace language { namespace stage {

LLVMCleanupStage::LLVMCleanupStage()
{ }

LLVMCleanupStage::~LLVMCleanupStage()
{ }

const char* LLVMCleanupStage::name()
{
    return "LLVM IR Cleanup Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> LLVMCleanupStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options();

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options();

    return std::make_pair(option_desc_public, option_desc_private);
}

bool LLVMCleanupStage::parseOptions(po::variables_map& vm)
{
    return true;
}

bool LLVMCleanupStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    GeneratorContext& generator_context = getGeneratorContext();

    if(!generator_context.active_config)
    	return true;

    // create visitor to walk through the entire tree and generate instructions accordingly
    if(getParserContext().tangle)
    {
    	tree::ASTNodeHelper::foreachApply<tree::ASTNode>(*getParserContext().tangle, [](tree::ASTNode& node) {
    		DebugInfoProgramContext::reset(&node);
    		DebugInfoContext::reset(&node);
    		DebugInfoTypeContext::reset(&node);
    		SynthesizedAllocaThisPointerContext::reset(&node);
    		SynthesizedFunctionContext::reset(&node);
    		SynthesizedTypeContext::reset(&node);
    		SynthesizedValueContext::reset(&node);
    		SynthesizedGuardValueContext::reset(&node);
    		SynthesizedBlockContext::reset(&node);
    		IntermediateValueContext::reset(&node);
    		SynthesizedFinalizeBlockContext::reset(&node);
    		SynthesizedVirtualFunctionContext::reset(&node);
    		SynthesizedVTableContext::reset(&node);
    	});
    }

    return true;
}

bool LLVMCleanupStage::hasProgress()
{
	return getGeneratorContext().active_config != NULL;
}

} } }
