/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <functional>

#include <boost/algorithm/string/join.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "core/Common.h"
#include "utility/Foreach.h"

#include "language/tree/ASTNodeFactory.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/generator/detail/CppSourceGenerator.h"

namespace zillians { namespace language { namespace stage {

using namespace language::tree;

static Declaration* getCanonicalDecl(TemplatedIdentifier* node)
{
    ASTNode* tidParent = node->parent;
    BOOST_ASSERT(isa<TypeSpecifier>(tidParent) || isa<ClassDecl>(tidParent) || isa<FunctionDecl>(tidParent));

    Type* resolved_type{ASTNodeHelper::getCanonicalType(tidParent)};

    // ClassType & EnumType => ClassDecl & EnumDecl
    if(Declaration* class_enum_decl = resolved_type->getAsDecl())
    {
        return class_enum_decl;
    }
    // FunctionType => follow getCanonicalType to get FunctionDecl
    else
    {
        if(FunctionDecl* parent_func_decl = cast<FunctionDecl>(tidParent))
            return parent_func_decl;

        ASTNode* func_symbol = ASTNodeHelper::getCanonicalSymbol(node);
        FunctionDecl* func_decl = cast<FunctionDecl>(func_symbol);
        BOOST_ASSERT(func_decl != nullptr);
        return func_decl;
    }

    UNREACHABLE_CODE();
    return nullptr;
}

CppSourceGenerator::CppSourceGenerator(bool cuda_mode) : cuda_mode(cuda_mode), output(nullptr)
{ }

void CppSourceGenerator::setWrite(Stream *new_stream)
{
    output = new_stream;
}

static Declaration* getTemplateDecl(TemplatedIdentifier* node)
{
    Declaration* decl = getCanonicalDecl(node);
    BOOST_ASSERT(decl != NULL);
    if(InstantiatedFrom::get(decl) != NULL)
    {
        decl = cast<Declaration>(InstantiatedFrom::get(decl));
        BOOST_ASSERT(decl != NULL);
    }
    return decl;
}

static Declaration* getInstantiationDecl(TemplatedIdentifier* node)
{
    Declaration* decl = getCanonicalDecl(node);
    return decl;
}

std::vector<Declaration*> CppSourceGenerator::sortDeclByInheritence(std::set<Declaration*>& classDecls, std::set<BaseDerivePair>& baseDerivedPairs)
{
    std::vector<Declaration*> sortedClassDecl;
    std::set<Declaration*>    allClasses = classDecls;
    std::set<Declaration*>    picked;
    while(!allClasses.empty())
    {
        auto locate = std::find_if(allClasses.begin(), allClasses.end(), [&baseDerivedPairs, &picked](Declaration* a) -> bool {
            for(auto i = baseDerivedPairs.begin(); i != baseDerivedPairs.end(); ++i)
            {
                if(picked.count(i->first) == 0 && i->second == a)
                {
                    return false;
                }
            }
            return true;
        });
        picked.insert(*locate);
        sortedClassDecl.push_back(*locate);
        allClasses.erase(locate);
    }
    return sortedClassDecl;
}

// public :
// protected :
// private :
void CppSourceGenerator::genVisibility(Declaration::VisibilitySpecifier::type v)
{
    switch(v)
    {
    case Declaration::VisibilitySpecifier::PUBLIC:
        *output << L"public : " ;
        break;
    case Declaration::VisibilitySpecifier::PROTECTED:
        *output << L"protected : " ;
        break;
    case Declaration::VisibilitySpecifier::PRIVATE:
        *output << L"private : " ;
        break;
    // TODO what is DEFAULT?
    case Declaration::VisibilitySpecifier::DEFAULT:
        *output << L"private : " ;
        break;
    }
}

// thor::lang::Bar<...>
//             ^^^^^^^^
void CppSourceGenerator::genNonNSQualifiedName(const bool addStar, Identifier* id)
{
    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
    {
        *output << tid->id->toString() << L"<";
        zillians::for_each_and_pitch(
            tid->templated_type_list,
            [this](decltype(tid->templated_type_list)::const_reference type) {
                if(type->specialized_type)
                {
                    auto argument_type = type->specialized_type->getCanonicalType();
                    genNamespaceQualifiedType(argument_type->isRecordType(), type->specialized_type);
                }
                else
                {
                    *output << type->name->toString();
                }
            },
            [this]() { *output << L", "; }
        );
        *output << L">";
        if(addStar) *output << L"*";
    }
    else if(isa<NestedIdentifier>(id))
    {
        UNIMPLEMENTED_CODE();
    }
    else if(isa<SimpleIdentifier>(id))
    {
        *output << id->toString();
        if(addStar) *output << L"*";
    }
    else
    {
        UNIMPLEMENTED_CODE();
    }

}

void CppSourceGenerator::genNamespaceQualifier(ASTNode* node)
{
    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(node);
    if(!packages.empty())
        *output << L" ";
    for(auto* package : packages)
    {
        *output << package->id->name << L"::";
    }
}

void CppSourceGenerator::genNamespaceQualifiedType(const bool addStar, TypeSpecifier* type)
{
    if (auto*const type_func = cast<FunctionSpecifier>(type))
        genNamespaceQualifiedType(addStar, type_func);
    else if (auto*const type_primitive = cast<PrimitiveSpecifier>(type))
        genNamespaceQualifiedType(addStar, type_primitive);
    else if (auto*const type_named = cast<NamedSpecifier>(type))
        genNamespaceQualifiedType(addStar, type_named);
    else
        UNREACHABLE_CODE();
}

void CppSourceGenerator::genNamespaceQualifiedType(const bool addStar, FunctionSpecifier* type)
{
    genNamespaceQualifiedType(addStar, type->getReturnType());
    *output << L"(";
    zillians::for_each_and_pitch(
        type->getParameterTypes(),
        [this,addStar](TypeSpecifier* type) {
            genNamespaceQualifiedType(addStar, type);
        },
        [this]() { *output << L", "; }
    );
    *output << L")";
}

void CppSourceGenerator::genNamespaceQualifiedType(const bool addStar, NamedSpecifier* type)
{
    Type* node = type->getCanonicalType();
    // if type is TypenameDecl of a templated id, node will be NULL, and just print the name
    if(node == nullptr)
    {
        *output << type->toString();
        return;
    }

    genNamespaceQualifier(node);
    if(node->isPrimitiveType())
    {
        *output << node->toString();
    }
    else if(EnumDecl* enum_decl = node->getAsEnumDecl())
    {
        auto* id = enum_decl->name;
        genNonNSQualifiedName(false, id);
    }
    else if(auto* class_decl = node->getAsClassDecl())
    {
        auto* id = class_decl->name;
        genNonNSQualifiedName(addStar, id);
    }
    else
    {
        auto* id = type->getName();
        genNonNSQualifiedName(false, id);
    }
}

void CppSourceGenerator::genNamespaceQualifiedType(const bool addStar, PrimitiveSpecifier* type)
{
    Type* t = type->getCanonicalType();
    genNamespaceQualifier(t);
    *output << type->toString();
}

// static
void CppSourceGenerator::genStatic(bool isStatic)
{
    if(isStatic)
        *output << L"static ";
}

// static
void CppSourceGenerator::genVirtual(bool isVirtual)
{
    if(isVirtual)
        *output << L"virtual ";
}

// private Foo<Bar<...>> w1;
void CppSourceGenerator::genDataMember(VariableDecl* var)
{
    genVisibility(var->visibility);
    genStatic(var->is_static); *output << L" ";
    genNamespaceQualifiedType(AddStar(true), var->type); *output << L" ";
    BOOST_ASSERT(isa<SimpleIdentifier>(var->name));
    *output << var->name->toString(); *output << L" ;" << std::endl;
}

// template < typename T >
// class Foo
void CppSourceGenerator::genCppClassName(Identifier* id)
{
    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
    {
        *output << L"// " << tid->toString() << std::endl;
        *output << L"template <";
        zillians::for_each_and_pitch(
            tid->templated_type_list,
            [this](decltype(tid->templated_type_list)::const_reference type) {
                *output << L"typename " << type->name->toString();
            },
            [this]() { *output << L", "; }
        );
        *output << L">" << std::endl;
        *output << L"class " << tid->id->toString();
    }
    else if(isa<NestedIdentifier>(id))
    {
        UNIMPLEMENTED_CODE();
    }
    else if(isa<SimpleIdentifier>(id))
    {
        *output << L"class " << id->toString();
    }
}

// template < typename T, ... >
void CppSourceGenerator::genCppMemberFunctionTemplateQuaifier(Identifier* id)
{
    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
    {
        *output << L"    // " << tid->toString() << std::endl;
        *output << L"    template <";
        zillians::for_each_and_pitch(
            tid->templated_type_list,
            [this](decltype(tid->templated_type_list)::const_reference type) {
                *output << L"typename " << type->name->toString();
            },
            [this]() { *output << L", "; }
        );
        *output << L">";
    }
}

// Foo
void CppSourceGenerator::genCppSimpleName(Identifier* id)
{
    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
    {
        std::wstring name = tid->id->toString();
        if (name == L"new")
        {
            genCppSimpleName(tid->id);
        }
        else
        {
            *output << name;
        }
    }
    else if(isa<NestedIdentifier>(id))
    {
        UNIMPLEMENTED_CODE();
    }
    else if(isa<SimpleIdentifier>(id))
    {
        std::wstring name = id->toString();
        if(name == L"new")
        {
            ClassDecl* classDecl = id->getOwner<ClassDecl>();
            genCppSimpleName(classDecl->name);
        }
        else if(name == L"delete")
        {
            ClassDecl* classDecl = id->getOwner<ClassDecl>();
            *output << L"~";
            genCppSimpleName(classDecl->name);
        }
        else
        {
            *output << name;
        }
    }
}

// int a1
void CppSourceGenerator::genParameter(VariableDecl* var)
{
    //os << var->type->toString();
    genNamespaceQualifiedType(AddStar(true), var->type);
    *output << L" ";
    *output << var->name->toString();
}

// int a1, int a2
void CppSourceGenerator::genParameters(std::vector<VariableDecl*>& parameters)
{
    zillians::for_each_and_pitch(
        parameters,
        [this](std::remove_reference<decltype(parameters)>::type::const_reference param) {
            genParameter(param);
        },
        [this]() { *output << L", "; }
    );
}

// public int func(...) { ... }
void CppSourceGenerator::genMemberFunction(FunctionDecl* func)
{
    // do not generate functions which return multiple values
    if( isa<MultiSpecifier>(func->type) )
        return;

    genVisibility(func->visibility);
    genCppMemberFunctionTemplateQuaifier(func->name); *output << std::endl;
    *output << L"    "; genStatic(func->is_static); genVirtual(func->is_virtual);
    if(!isCtorOrDtor(func))
    {
        Type* resolved_type = func->type->getCanonicalType();
        AddStar add_star = resolved_type->isRecordType();
        genNamespaceQualifiedType(add_star, func->type);
        *output << L" ";
    }
    genCppSimpleName(func->name);
    *output << L"(";
    genParameters(func->parameters);
    *output << L");";
    *output << std::endl;
}

// : public Base
void CppSourceGenerator::genBases(NamedSpecifier* base, std::vector<NamedSpecifier*>& implements)
{
    if(base != nullptr || !implements.empty())
    {
        *output << L" :";
    }

    if(base != nullptr)
    {
        *output << L" public ";
        genNamespaceQualifiedType(AddStar(false), base);
    }

    if(base != nullptr && !implements.empty())
        *output << L", ";

    zillians::for_each_and_pitch(
        implements,
        [this](std::remove_reference<decltype(implements)>::type::const_reference impl){
            *output << L" public ";
            genNamespaceQualifiedType(AddStar(false), impl);
        },
        [this](){
            *output << L", ";
        }
    );
}

// namespace xxx {
void CppSourceGenerator::openCppNamespace(Declaration* decl)
{
    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(decl);
    zillians::for_each_and_pitch(
        packages,
        [this](decltype(packages)::const_reference package){
            *output << L"namespace " << package->id->name << L" {";
        },
        [this](){ *output << L" "; }
    );
    *output << std::endl;
}

// } // namespace xxx
void CppSourceGenerator::closeCppNamespace(Declaration* decl)
{
    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(decl);
    for(auto count : zero_to(packages.size()))
    {
        *output << L"} ";
    }
    *output << L"// namespace";
    for(auto* package : packages)
    {
        *output << L" " << package->id->name;
    }
    *output << std::endl;
}

// template < typename T >
// class Foo {
//     ...
//     ...
// };
void CppSourceGenerator::genClassDecl(ClassDecl* classDecl)
{
    // namespace
    openCppNamespace(classDecl);

    // title
    genCppClassName(classDecl->name);
    // inheretent base
    genBases(classDecl->base, classDecl->implements);

    *output << L" {" << std::endl;
    // data member
    *output << L"    // data member" << std::endl;
    for(auto& attribute : classDecl->member_variables)
    {
        *output << L"    ";
        genDataMember(attribute);
    }
    // function
    *output << L"    // member function" << std::endl;
    for(auto& method : classDecl->member_functions)
    {
        *output << L"    ";
        genMemberFunction(method);
    }
    // end class
    *output << L"};" << std::endl;

    // end namespace
    closeCppNamespace(classDecl);
    *output << std::endl;
}

void CppSourceGenerator::genPreIncludeForwardDeclaration(std::set<Declaration*>& classDecls)
{
    std::set<Declaration*> generated;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Pre-Include Forward declaration" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;
    for(auto* decl : classDecls)
    {
        ClassDecl* classDecl = cast<ClassDecl>(decl);
        if(generated.find(classDecl) == generated.end())
        {
            generated.insert(classDecl);
            openCppNamespace(classDecl);
            genCppClassName(classDecl->name);
            *output << L";" << std::endl;
            closeCppNamespace(classDecl);
        }
    }
    *output << std::endl;
}

// namespame thor {
// class A;
// } // thor
// ^^^^^^^^^^^^^^^^
void CppSourceGenerator::genForwardDeclaration(std::set<Declaration*>& classDecls)
{
    std::set<Declaration*> generated;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Forward declaration" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;
    for(auto* decl : classDecls)
    {
        ClassDecl* classDecl = cast<ClassDecl>(decl);
        if(generated.find(classDecl) == generated.end())
        {
            generated.insert(classDecl);
            openCppNamespace(classDecl);
            genCppClassName(classDecl->name);
            *output << L";" << std::endl;
            closeCppNamespace(classDecl);
        }
    }
    *output << std::endl;
}

// enum class ??? : (int32|int64) { ... };
void CppSourceGenerator::genEnumDefinitions(std::set<EnumDecl*>& enumDecls)
{
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Enum definition" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;

    boost::range::for_each(enumDecls, [this](EnumDecl* enum_){
        // already has definition in system bundle, skip
        if(enum_->findAnnotation(L"system"))
            return;

        openCppNamespace(enum_);

        auto enum_name = enum_->name->getSimpleId()->name;
        auto underlying_type_is_int64 = EnumDeclManglingContext::get(enum_)->is_long_literal;
        *output << L"enum class " << enum_name << L" : thor::" << (underlying_type_is_int64 ? L"int64" : L"int32") << std::endl;
        *output << L"{" << std::endl;

        auto literal_postfix = (underlying_type_is_int64 ?  L"L" : L"");
        zillians::for_each_and_pitch(
            enum_->values,
            [this,literal_postfix](VariableDecl* value){
                auto value_name = value->name->getSimpleId()->name;
                auto literal = EnumIdManglingContext::get(value)->value;

                *output << L"    " << value_name << L" = " << literal << literal_postfix;
            },
            [this](){ *output << "," << std::endl; }
        );

        *output << std::endl;
        *output << L"};" << std::endl;

        closeCppNamespace(enum_);
    });
}

// class A {...};
// class B {...};
// class C {...};
void CppSourceGenerator::genClassDecls(std::vector<Declaration*>& classDecls)
{
    std::set<Declaration*> generated;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Class declaration" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;
    for(auto* decl : classDecls)
    {
        ClassDecl* classDecl = cast<ClassDecl>(decl);
        ClassDecl* templateClassDecl = classDecl;
        if(InstantiatedFrom::get(classDecl) != NULL)
            templateClassDecl = cast<ClassDecl>(InstantiatedFrom::get(classDecl));
        if(generated.find(templateClassDecl) == generated.end())
        {
            generated.insert(templateClassDecl);
            genClassDecl(templateClassDecl);
        }
    }
}

// #include <cinttypes>
// typedef std::int8_t int8;
// typedef std::int16_t int16;
// typedef std::int32_t int32;
// typedef std::int64_t int64;
// typedef float float32;
// typedef double float64;
void CppSourceGenerator::genPrimitiveType()
{
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Primitive types" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;
    *output << L"#include <functional>" << std::endl;
    *output << L"template < typename T > struct AddPointerNonPrimitive { typedef typename std::conditional<std::is_class<T>::value, T, T*>::type type; };" << std::endl;
}

// template class Foo::Foo<NS::Bar>;
void CppSourceGenerator::genClassExplicitInstantiation(std::set<Declaration*>& instantiationSet)
{
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Explicit class instantiation" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;

    for(auto* decl : instantiationSet)
    {
        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(decl->name);
        BOOST_ASSERT(tid != NULL);

        *output << L"template class ";
        std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(getTemplateDecl(tid));
        for(auto* package : packages)
            *output << package->id->name << L"::";

        *output << tid->id->toString();
        *output << L"<";
        zillians::for_each_and_pitch(
            tid->templated_type_list,
            [this](decltype(tid->templated_type_list)::const_reference type){
                if(type->specialized_type)
                {
                    auto argument_type = type->specialized_type->getCanonicalType();
                    genNamespaceQualifiedType(argument_type->isRecordType(), type->specialized_type);
                }
                else
                {
                    *output << type->name->toString();
                }
            },
            [this](){ *output << L", "; }
        );
        *output << L">;" << std::endl;
    }

    *output << std::endl;
}

void CppSourceGenerator::genClassNameQualifiedFunctionName(FunctionDecl* funcDecl)
{
    // class name(if member function)
    ClassDecl* classDecl = funcDecl->getOwner<ClassDecl>();
    std::wstring class_name;
    if(classDecl)
    {
        genNamespaceQualifier(classDecl);
        if(isa<TemplatedIdentifier>(classDecl->name))
        {
            auto class_tid = cast<TemplatedIdentifier>(classDecl->name);
            *output << classDecl->name->getSimpleId()->toString();

            // append resolved type name to it
            *output << L"<";
            for (auto i = class_tid->templated_type_list.begin(); i != class_tid->templated_type_list.end(); ++i)
            {
                if (i != class_tid->templated_type_list.begin())
                    *output << ", ";
                genNamespaceQualifiedType(AddStar(true), (*i)->specialized_type);
            }
            *output << L">::";
        }
        else
        {
            BOOST_ASSERT(isa<SimpleIdentifier>(classDecl->name));
            *output << classDecl->name->toString() << L"::";
        }
    }

    // function name
    if (funcDecl->isConstructor())
    {
        // constructor should output class name
        *output << class_name;
    }
    else if(funcDecl->isDestructor())
    {
        // constructor should output class name
        *output << L"~" << class_name;
    }
    else
    {
        *output << funcDecl->name->toString();
    }
}

// template class Foo::Foo<NS::Bar>;
void CppSourceGenerator::genFunctionExplicitInstantiation(std::set<Declaration*>& instantiationSet)
{
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Explicit function instantiation" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;

    for(auto& inst : instantiationSet)
    {
        openCppNamespace(inst);

        FunctionDecl* funcDecl = cast<FunctionDecl>(inst);
        BOOST_ASSERT(funcDecl != NULL);
        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(funcDecl->name);
        BOOST_ASSERT(tid != NULL);

        if(funcDecl->name->toString() == L"__cxa_delete")
            continue;

        *output << L"template ";

        // no need to generate return type for constructor
        if (!funcDecl->isConstructor())
        {
            genNamespaceQualifiedType(AddStar(true), funcDecl->type);
            *output << L" ";
        }

        // class name(if member function)
        ClassDecl* classDecl = funcDecl->getOwner<ClassDecl>();
        std::wstring class_name;
        if(classDecl)
        {
            if(isa<TemplatedIdentifier>(classDecl->name))
            {
                auto tid = cast<TemplatedIdentifier>(classDecl->name);
                class_name = tid->id->toString();
                *output << class_name;

                // append resolved type name to it
                *output << L"<";
                for (decltype(tid->templated_type_list.size()) i = 0; i < tid->templated_type_list.size(); i++)
                {
                    auto typename_decl = tid->templated_type_list[i];
                    genNamespaceQualifiedType(AddStar(true), typename_decl->specialized_type);

                    if (i != tid->templated_type_list.size() - 1)
                        *output << ", ";
                }
                *output << L">::";
            }
            else
            {
                BOOST_ASSERT(isa<SimpleIdentifier>(classDecl->name));
                *output << classDecl->name->toString() << L"::";
            }
        }

        // function name
        if (funcDecl->isConstructor())
        {
            // constructor should output class name
            *output << class_name;
        }
        else
        {
            *output << tid->id->toString();
        }

        *output << L"<";
        zillians::for_each_and_pitch(
            tid->templated_type_list,
            [this](decltype(tid->templated_type_list)::const_reference type){
                genNamespaceQualifiedType(AddStar(true), type->specialized_type);
            },
            [this](){ *output << L", "; }
        );
        *output << L">(";
        genParameters(funcDecl->parameters);
        *output << L");" << std::endl;

        closeCppNamespace(inst);
    }

    *output << std::endl;
}

// #include "..."
void CppSourceGenerator::genIncludeImplementation(std::set<Declaration*>& nativeClasses)
{
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << L"// Include implementation" << std::endl;
    *output << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
    *output << std::endl;
    for(auto* decl : nativeClasses)
    {
        ASTNode* value = decl->findAnnotationValue({L"native", L"include"});
        if(!value) continue;

        StringLiteral* literal = cast<StringLiteral>(value);
        if(!literal) continue;

        *output << L"#include \"" << literal->value << L"\"" << std::endl;
        // TODO throw exception when it's not ready?
    }
    *output << std::endl;
    *output << L"using namespace thor::lang;" << std::endl;
}



bool CppSourceGenerator::isCtorOrDtor(FunctionDecl* func)
{
    if (func->isConstructor()) return true;
    if (func->isDestructor()) return true;

    return false;
}
} } }
