/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <iomanip>

#include <boost/algorithm/cxx11/all_of.hpp>

#include <llvm/Support/Program.h>

#include "utility/Functional.h"
#include "utility/Process.h"

#include "language/ThorConfiguration.h"
#include "language/context/ParserContext.h"
#include "language/context/ResolverContext.h"
#include "language/context/GeneratorContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/tree/ASTNodeHelper.h"

#include "language/stage/generator/CppGeneratorStage.h"
#include "language/stage/generator/detail/CppSourceGenerator.h"

namespace zillians { namespace language { namespace stage {

using namespace language::tree;

//////////////////////////////////////////////////////////////////////////////
// Forward declaration & typedef
//////////////////////////////////////////////////////////////////////////////

//typedef std::pair<Declaration*, Declaration*> BaseDerivePair;
//typedef bool WithInTemplateQuate;
//typedef bool AddStar;

//static void genNamespaceQualifiedType(const bool addStar, TypeSpecifier* type, std::wostream& os);
//static void genCppClassName(Identifier* id, std::wostream& os);

//////////////////////////////////////////////////////////////////////////////
// static functions
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// helper

using SourceGenerator = CppSourceGenerator;

static Declaration* getCanonicalDecl(TemplatedIdentifier* node)
{
    ASTNode* tidParent = node->parent;
    BOOST_ASSERT(isa<TypeSpecifier>(tidParent) || isa<ClassDecl>(tidParent) || isa<FunctionDecl>(tidParent));

    Type* resolved_type{ASTNodeHelper::getCanonicalType(tidParent)};

    // ClassType & EnumType => ClassDecl & EnumDecl
    if(Declaration* class_enum_decl = resolved_type->getAsDecl())
    {
        return class_enum_decl;
    }
    // FunctionType => follow getCanonicalType to get FunctionDecl
    else
    {
        if(FunctionDecl* parent_func_decl = cast<FunctionDecl>(tidParent))
            return parent_func_decl;

        ASTNode* func_symbol = ASTNodeHelper::getCanonicalSymbol(node);
        FunctionDecl* func_decl = cast<FunctionDecl>(func_symbol);
        BOOST_ASSERT(func_decl != nullptr);
        return func_decl;
    }

    UNREACHABLE_CODE();
    return nullptr;
}

static Declaration* getTemplateDecl(TemplatedIdentifier* node)
{
    Declaration* decl = getCanonicalDecl(node);
    BOOST_ASSERT(decl != NULL);
    if(InstantiatedFrom::get(decl) != NULL)
    {
        decl = cast<Declaration>(InstantiatedFrom::get(decl));
        BOOST_ASSERT(decl != NULL);
    }
    return decl;
}

static Declaration* getInstantiationDecl(TemplatedIdentifier* node)
{
    Declaration* decl = getCanonicalDecl(node);
    return decl;
}

static bool compile(const ThorBuildConfiguration& config, const boost::filesystem::path& cpp_file, const boost::filesystem::path& obj_file)
{
    BOOST_ASSERT(hasGeneratorContext() && "generator context is required!");

    const auto& generator_context = getGeneratorContext();

    llvm::sys::Path native_compiler = llvm::sys::Program::FindProgramByName(THOR_CPP_NATIVE_COMPILER);
    if (native_compiler.isEmpty())
    {
        std::cerr << "Error: failed to locate " << THOR_CPP_NATIVE_COMPILER << std::endl;
        return false;
    }

    std::string cmd = native_compiler.str();

#if defined (__PLATFORM_LINUX__) || defined (__PLATFORM_MAC__)
    cmd += " -c -fPIC -std=c++0x -o " + obj_file.string() + " " + cpp_file.string();

    if (generator_context.emit_debug_info)
        cmd += " -g";

    if (generator_context.opt_level > 0)
        cmd += " -O" + std::to_string(generator_context.opt_level);

    for(auto& bundle : config.manifest.deps.bundles)
    {
        cmd += " -I" + (config.extract_bundle_path / bundle.name / "src").string();
    }

    cmd += " -I" + config.source_path.string() ;
#ifdef __PLATFORM_MAC__
    cmd += " -I/usr/local/include";
#endif
#endif

#ifdef __PLATFORM_WINDOWS__
    UNIMPLEMENTED_CODE();
#endif

    if(config.verbose)
    {
        std::cerr << "[CppGenerator] invokes : '" << cmd << "'" <<  std::endl;
    }

    int return_code = zillians::system(cmd);
    if(return_code != 0)
    {
        std::cerr << "Error: return code from " << THOR_CPP_NATIVE_COMPILER << ": " << return_code << std::endl;
    }

    return (return_code == 0);
}

//////////////////////////////////////////////////////////////////////////////
// collector

/**
 * Return the set of native Class/Function declaration
 */
template<typename T>
static std::set<Declaration*> getNative()
{
    std::set<Declaration*> result;
    ASTNode* tangle = getParserContext().tangle;
    ASTNodeHelper::foreachApply<T>(*tangle, [&result](T& node){
        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(node.name);
        if(tid == NULL)
            return;
        if(!tid->type == TemplatedIdentifier::Usage::FORMAL_PARAMETER)
            return;
        if(InstantiatedFrom::get(&node) != NULL)
            return;
        if(!ASTNodeHelper::hasNativeLinkage(&node)){
            return;
        }
        result.insert(&node);
    });
    return result;

}

/**
 * Return all enumerations under tangle
 */
static std::set<EnumDecl*> getEnumDecls()
{
    std::set<EnumDecl*> result;
    ASTNode* tangle = getParserContext().tangle;
    ASTNodeHelper::foreachApply<EnumDecl>(*tangle, [&result](EnumDecl& node){
        result.insert(&node);
    });
    return result;
}

static bool collectTypeSpecifier(TypeSpecifier* type, std::set<Declaration*>& result);

static bool collectTypeSpecifier(FunctionSpecifier* type, std::set<Declaration*>& result)
{
    BOOST_ASSERT(type != NULL);

    bool hasAdd = false;

    hasAdd |= collectTypeSpecifier(type->getReturnType(), result);

    for(auto& param_type : type->getParameterTypes())
        hasAdd |= collectTypeSpecifier(param_type, result);

    return hasAdd;
}

static bool collectTypeSpecifier(NamedSpecifier* type, std::set<Declaration*>& result)
{
    BOOST_ASSERT(type != NULL);

    Type* node = type->getCanonicalType();

    // if it's enum declaration, don't add
    // TODO handle enum type export
    if(node->isEnumType())
        return false;

    // if the node is a template type id 'T' in the class, don't add
    if(node->isTypenameType())
        return false;

    if(node->isPrimitiveType())
    {
        return false;
    }

    bool hasAdd = false;

    Declaration* decl = node->getAsDecl();
    if(isa<TemplatedIdentifier>(decl->name))
    {
        // template decl
        TemplatedIdentifier* templateId = cast<TemplatedIdentifier>(decl->name);
        Declaration* templateDecl = getInstantiationDecl(templateId);
        if(result.find(templateDecl) == result.end())
        {
            result.insert(templateDecl);
            hasAdd = true;
        }
        // instantiate type args
        TemplatedIdentifier* tid = decl->name->getTemplateId();
        BOOST_ASSERT(tid != NULL);
        for(auto* typenameDecl : tid->templated_type_list)
        {
            if(typenameDecl->specialized_type != NULL)
            {
                if(collectTypeSpecifier(typenameDecl->specialized_type, result))
                    hasAdd = true;
            }
        }
    }
    else
    {
        if(result.find(decl) == result.end())
        {
            result.insert(decl);
            hasAdd = true;
        }
    }

    return hasAdd;
}

/*
 * We need to recursively trace into the TypeSpecifier. Because of
 *
 *     var w : Vector < Foo < Bar < Aion, int32 > > > ;
 *
 * not only 'Foo' need to be generated, but also 'Bar' and 'Aion',
 * This is done by collectTypeSpecifier() call itself recursively.
 */
static bool collectTypeSpecifier(TypeSpecifier* type, std::set<Declaration*>& result)
{
    BOOST_ASSERT(type != NULL);

    if (isa<PrimitiveSpecifier, MultiSpecifier>(type))
        return false;

    if (auto*const type_func = cast<FunctionSpecifier>(type))
        return collectTypeSpecifier(type_func, result);

    if (auto*const type_named = cast<NamedSpecifier>(type))
        return collectTypeSpecifier(type_named, result);

    UNREACHABLE_CODE();
    return false;
}

static bool isUnderUninstantiatedTree(ASTNode* node)
{
    node = node->parent;
    while(node)
    {
        if(isa<ClassDecl>(node) || isa<FunctionDecl>(node))
        {
            Declaration* decl = cast<Declaration>(node);
            if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(decl->name))
            {
                if(!tid->isFullySpecialized())
                {
                    return true;
                }
            }
        }
        node = node->parent;
    };
    return false;
}

template<typename T>
static std::set<Declaration*> getNativeDeclsTypeArgsAndInstantiation(std::set<Declaration*>& nativeDecls, std::set<Declaration*>& instantiationSet)
{
    std::set<Declaration*> result;
    ASTNode* tangle = getParserContext().tangle;
    ASTNodeHelper::foreachApply<TemplatedIdentifier>(*tangle, [&result, &nativeDecls, &instantiationSet](TemplatedIdentifier& node){
        if(node.type == TemplatedIdentifier::Usage::FORMAL_PARAMETER && node.isFullySpecialized() && !isUnderUninstantiatedTree(&node))
        {
            Declaration* decl = cast<Declaration>(node.parent);
            if(decl == NULL) return;
            if(ASTNode* inst_from = InstantiatedFrom::get(decl))
            {
                Declaration* templateDecl = cast<Declaration>(inst_from);
                BOOST_ASSERT(templateDecl != NULL);
                if(nativeDecls.find(templateDecl) != nativeDecls.end())
                {
                    for(auto* typeSpecifier : node.templated_type_list)
                    {
                        TypeSpecifier* specializedType = typeSpecifier->specialized_type;
                        collectTypeSpecifier(specializedType, result);
                    }
                    Declaration* instDecl = getInstantiationDecl(&node);
                    //Declaration* instDecl = decl;
                    T* classOrFuncDecl = cast<T>(instDecl);
                    BOOST_ASSERT(classOrFuncDecl != NULL);
                    instantiationSet.insert(classOrFuncDecl);
                }
            }
        }
    });
    return result;
}

// collect function arg & return type
static std::set<Declaration*> getFunctionUsedTypes(FunctionDecl* funcDecl, std::set<Declaration*>& result)
{
    // return type
    TypeSpecifier* type = funcDecl->type;
    if(type != NULL)
    {
        collectTypeSpecifier(type, result);
    }
    // args
    for(auto* arg : funcDecl->parameters)
    {
        TypeSpecifier* argType = arg->type;
        collectTypeSpecifier(argType, result);
    }

    return result;
}

static std::set<Declaration*> getNativeFunctionUsedTypes(std::set<Declaration*>& nativeFunctions)
{
    std::set<Declaration*> result;
    for(auto* decl : nativeFunctions)
    {
        FunctionDecl* func = cast<FunctionDecl>(decl);
        BOOST_ASSERT(func != NULL);

        getFunctionUsedTypes(func, result);
    }
    return result;
}

// collect data member & base class
static std::set<Declaration*> getExtentedNativeClassesTypeArgs(std::set<Declaration*>& nativeClassesTypeArgs, std::set<SourceGenerator::BaseDerivePair>& baseDerivedPairs)
{
    bool hasAdd = false;
    std::set<Declaration*> result = nativeClassesTypeArgs;
    do
    {
        hasAdd = false;
        for(auto* decl : result)
        {
            BOOST_ASSERT(isa<ClassDecl>(decl));
            //result.insert(*i);
            // class
            if(ClassDecl* classDecl = cast<ClassDecl>(decl))
            {
                // base class
                TypeSpecifier* base = classDecl->base;
                if(base != NULL)
                {
                    if(collectTypeSpecifier(base, result))
                    {
                        hasAdd = true;
                    }
                    Type* node = base->getCanonicalType();
                    ClassDecl* baseClassDecl = node->getAsClassDecl();
                    BOOST_ASSERT(baseClassDecl != NULL);
                    baseDerivedPairs.insert(std::make_pair(baseClassDecl, classDecl));
                }

                // base interfaces
                std::vector<NamedSpecifier*>& interfaces = classDecl->implements;
                for(auto* interface : interfaces)
                {
                    if(collectTypeSpecifier(interface, result))
                    {
                        hasAdd = true;
                    }
                    Type* node = interface->getCanonicalType();
                    ClassDecl* baseClassDecl = node->getAsClassDecl();
                    BOOST_ASSERT(baseClassDecl != NULL);
                    baseDerivedPairs.insert(std::make_pair(baseClassDecl, classDecl));
                }

                // data member
                for(auto* varDecl : classDecl->member_variables)
                {
                    TypeSpecifier* type = varDecl->type;
                    if(collectTypeSpecifier(type, result))
                        hasAdd = true;
                }

                // member function
                for(auto* funcDecl : classDecl->member_functions)
                {
                    // return type
                    TypeSpecifier* type = funcDecl->type;
                    if(type != NULL)
                    {
                        if(collectTypeSpecifier(type, result))
                            hasAdd = true;
                    }
                    // args
                    for(auto* arg : funcDecl->parameters)
                    {
                        TypeSpecifier* argType = arg->type;
                        if(collectTypeSpecifier(argType, result))
                            hasAdd = true;
                    }
                }
            }
            // interface
            else
            {
            }
        }
    } while (hasAdd) ;

    return result;
}

////////////////////////////////////////////////////////////////////////////////
//// generator
//
//static std::vector<Declaration*> sortDeclByInheritence(std::set<Declaration*>& classDecls, std::set<BaseDerivePair>& baseDerivedPairs)
//{
//    std::vector<Declaration*> sortedClassDecl;
//    std::set<Declaration*>    allClasses = classDecls;
//    std::set<Declaration*>    picked;
//    while(!allClasses.empty())
//    {
//        auto locate = std::find_if(allClasses.begin(), allClasses.end(), [&baseDerivedPairs, &picked](Declaration* a) -> bool {
//            for(auto i = baseDerivedPairs.begin(); i != baseDerivedPairs.end(); ++i)
//            {
//                if(picked.count(i->first) == 0 && i->second == a)
//                {
//                    return false;
//                }
//            }
//            return true;
//        });
//        picked.insert(*locate);
//        sortedClassDecl.push_back(*locate);
//        allClasses.erase(locate);
//    }
//    return sortedClassDecl;
//}
//
//// public :
//// protected :
//// private :
//static void genVisibility(Declaration::VisibilitySpecifier::type v, std::wostream& os)
//{
//    switch(v)
//    {
//    case Declaration::VisibilitySpecifier::PUBLIC:
//        os << L"public : " ;
//        break;
//    case Declaration::VisibilitySpecifier::PROTECTED:
//        os << L"protected : " ;
//        break;
//    case Declaration::VisibilitySpecifier::PRIVATE:
//        os << L"private : " ;
//        break;
//    // TODO what is DEFAULT?
//    case Declaration::VisibilitySpecifier::DEFAULT:
//        os << L"private : " ;
//        break;
//    }
//}
//
//// thor::lang::Bar<...>
////             ^^^^^^^^
//static void genNonNSQualifiedName(const bool addStar, Identifier* id, std::wostream& os)
//{
//    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
//    {
//        os << tid->id->toString() << L"<";
//        foreach(i, tid->templated_type_list)
//        {
//            if(i != tid->templated_type_list.begin())
//                os << L", ";
//
//            if( (*i)->specialized_type )
//                genNamespaceQualifiedType(AddStar(false), (*i)->specialized_type, os);
//            else
//                os << (*i)->name->toString();
//        }
//        os << L">";
//        if(addStar) os << L"*";
//    }
//    else if(isa<NestedIdentifier>(id))
//    {
//        UNIMPLEMENTED_CODE();
//    }
//    else if(isa<SimpleIdentifier>(id))
//    {
//        os << id->toString();
//        if(addStar) os << L"*";
//    }
//    else
//    {
//        UNIMPLEMENTED_CODE();
//    }
//
//}
//
//static void genNamespaceQualifier(ASTNode* node, std::wostream& os)
//{
//    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(node);
//    if(!packages.empty())
//        os << L" ::";
//    foreach(i, packages)
//    {
//        os << (*i)->id->name << L"::";
//    }
//}
//
//static void genNamespaceQualifiedType(const bool addStar, TypeSpecifier* type, std::wostream& os)
//{
//    if (type->isFunctionType())
//    {
//        FunctionType* function_type = type->getFunctionType();
//
//        genNamespaceQualifiedType(addStar, function_type->return_type, os);
//        os << L"(";
//        foreach(i, function_type->parameter_types)
//        {
//            genNamespaceQualifiedType(addStar, *i, os);
//            if(!is_end_of_foreach(i, type->referred.function_type->parameter_types))
//                os << L", ";
//        }
//        os << L")";
//    }
//    else if (type->isPrimitiveType())
//    {
//        os << type->toString();
//    }
//    else if (type->isUnspecified())
//    {
//        ASTNode* node = type->getCanonicalType();
//        // if type is TypenameDecl of a templated id, node will be NULL, and just print the name
//        if(isa<TypenameDecl>(node))
//        {
//            os << L"typename AddPointerNonPrimitive<" << type->toString() << L">::type";
//            return;
//        }
//
//        genNamespaceQualifier(node, os);
//        if(TypeSpecifier* t = cast<TypeSpecifier>(node))
//        {
//            if(t->isPrimitiveType())
//            {
//                os << t->toString();
//            }
//        }
//        else if(ClassDecl* classDecl = cast<ClassDecl>(node))
//        {
//            Identifier* id = classDecl->name;
//            genNonNSQualifiedName(addStar, id, os);
//        }
//        else
//        {
//            Identifier* id = type->getUnspecified();
//            genNonNSQualifiedName(addStar, id, os);
//        }
//    }
//    else
//    {
//        UNREACHABLE_CODE();
//    }
//}
//
//// static
//static void genStatic(bool isStatic, std::wostream& os)
//{
//    if(isStatic)
//        os << L"static ";
//}
//
//// private Foo<Bar<...>> w1;
//static void genDataMember(VariableDecl* var, std::wostream& os)
//{
//    genVisibility(var->visibility, os);
//    genStatic(var->is_static, os); os << L" ";
//    genNamespaceQualifiedType(AddStar(true), var->type, os); os << L" ";
//    BOOST_ASSERT(isa<SimpleIdentifier>(var->name));
//    os << var->name->toString(); os << L" ;" << std::endl;
//}
//
//// template < typename T >
//// class Foo
//static void genCppClassName(Identifier* id, std::wostream& os)
//{
//    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
//    {
//        os << L"// " << tid->toString() << std::endl;
//        os << L"template <";
//        foreach(i, tid->templated_type_list)
//        {
//            if(i != tid->templated_type_list.begin()) os << L", ";
//            os << L"typename " << (*i)->name->toString();
//        }
//        os << L">" << std::endl;
//        os << L"class " << tid->id->toString();
//    }
//    else if(isa<NestedIdentifier>(id))
//    {
//        UNIMPLEMENTED_CODE();
//    }
//    else if(isa<SimpleIdentifier>(id))
//    {
//        os << L"class " << id->toString();
//    }
//}
//
//// template < typename T, ... >
//static void genCppMemberFunctionTemplateQuaifier(Identifier* id, std::wostream& os)
//{
//    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
//    {
//        os << L"    // " << tid->toString() << std::endl;
//        os << L"    template <";
//        foreach(i, tid->templated_type_list)
//        {
//            if(i != tid->templated_type_list.begin()) os << L", ";
//            os << L"typename " << (*i)->name->toString();
//        }
//        os << L">";
//    }
//}
//
//// Foo
//static void genCppSimpleName(Identifier* id, std::wostream& os)
//{
//    if(TemplatedIdentifier* tid = cast<TemplatedIdentifier>(id))
//    {
//		std::wstring name = tid->id->toString();
//		if (name == L"new")
//		{
//			genCppSimpleName(tid->id, os);
//		}
//		else
//		{
//	        os << name;
//		}
//    }
//    else if(isa<NestedIdentifier>(id))
//    {
//        UNIMPLEMENTED_CODE();
//    }
//    else if(isa<SimpleIdentifier>(id))
//    {
//        std::wstring name = id->toString();
//        if(name == L"new")
//        {
//            ClassDecl* classDecl = id->getOwner<ClassDecl>();
//            genCppSimpleName(classDecl->name, os);
//        }
//        else if(name == L"delete")
//        {
//            ClassDecl* classDecl = id->getOwner<ClassDecl>();
//            os << L"~";
//            genCppSimpleName(classDecl->name, os);
//        }
//        else
//        {
//            os << name;
//        }
//    }
//}
//
//// int a1
//static void genParameter(VariableDecl* var, std::wostream& os)
//{
//    //os << var->type->toString();
//    genNamespaceQualifiedType(AddStar(true), var->type, os);
//    os << L" ";
//    os << var->name->toString();
//}
//
//// int a1, int a2
//static void genParameters(std::vector<VariableDecl*>& parameters, std::wostream& os)
//{
//    foreach(i, parameters)
//    {
//        if(i != parameters.begin())
//            os << L", ";
//        genParameter(*i, os);
//    }
//}
//
//bool isCtorOrDtor(FunctionDecl* func)
//{
//	if (func->isConstructor()) return true;
//    if (func->isDestructor()) return true;
//
//    return false;
//}
//
//// public int func(...) { ... }
//static void genMemberFunction(FunctionDecl* func, std::wostream& os)
//{
//    genVisibility(func->visibility, os);
//    genCppMemberFunctionTemplateQuaifier(func->name, os); os << std::endl;
//    os << L"    "; genStatic(func->is_static, os);
//    if(!isCtorOrDtor(func))
//    {
//		ASTNode* resolved_type = func->type->getCanonicalType();
//		AddStar add_star = isa<ClassDecl>(resolved_type);
//        genNamespaceQualifiedType(add_star, func->type, os);
//        os << L" ";
//    }
//    genCppSimpleName(func->name, os);
//    os << L"(";
//    genParameters(func->parameters, os);
//    os << L");";
//    os << std::endl;
//}
//
//// : public Base
//static void genBases(TypeSpecifier* base, std::vector<TypeSpecifier*>& implements, std::wostream& os)
//{
//    if(base != NULL || !implements.empty())
//    {
//        os << L" :";
//    }
//
//    if(base != NULL)
//    {
//        os << L" public ";
//        genNamespaceQualifiedType(AddStar(false), base, os);
//    }
//
//    foreach(i, implements)
//    {
//        if(base != NULL || i != implements.begin())
//        {
//            os << L", ";
//        }
//        os << L" public ";
//        genNamespaceQualifiedType(AddStar(false), *i, os);
//    }
//}
//
//// namespace xxx {
//static void openCppNamespace(Declaration* decl, std::wostream& os)
//{
//    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(decl);
//    foreach(i, packages)
//    {
//        if(i != packages.begin())
//            os << L" ";
//        os << L"namespace " << (*i)->id->name << L" {";
//    }
//    os << std::endl;
//}
//
//// } // namespace xxx
//static void closeCppNamespace(Declaration* decl, std::wostream& os)
//{
//    std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(decl);
//    foreach(i, packages)
//    {
//        os << L"} ";
//    }
//    os << L"// namespace";
//    foreach(i, packages)
//    {
//        os << L" " << (*i)->id->name;
//    }
//    os << std::endl;
//}
//
//// template < typename T >
//// class Foo {
////     ...
////     ...
//// };
//static void genClassDecl(ClassDecl* classDecl, std::wostream& os)
//{
//    // namespace
//    openCppNamespace(classDecl, os);
//
//    // title
//    genCppClassName(classDecl->name, os);
//    // inheretent base
//    genBases(classDecl->base, classDecl->implements, os);
//
//    os << L" {" << std::endl;
//    // data member
//    os << L"    // data member" << std::endl;
//    foreach(i, classDecl->member_variables)
//    {
//        os << L"    ";
//        genDataMember(*i, os);
//    }
//    // function
//    os << L"    // member function" << std::endl;
//    foreach(i, classDecl->member_functions)
//    {
//        os << L"    ";
//        genMemberFunction(*i, os);
//    }
//    // end class
//    os << L"};" << std::endl;
//
//    // end namespace
//    closeCppNamespace(classDecl, os);
//    os << std::endl;
//}
//
//static void genPreIncludeForwardDeclaration(std::set<Declaration*>& classDecls, std::wostream& os)
//{
//    std::set<Declaration*> generated;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Pre-Include Forward declaration" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//    foreach(i, classDecls)
//    {
//        Declaration* decl = *i;
//        ClassDecl* classDecl = cast<ClassDecl>(decl);
//        if(generated.find(classDecl) == generated.end())
//        {
//            generated.insert(classDecl);
//            openCppNamespace(classDecl, os);
//            genCppClassName(classDecl->name, os);
//            os << L";" << std::endl;
//            closeCppNamespace(classDecl, os);
//        }
//    }
//    os << std::endl;
//}
//
//// namespame thor {
//// class A;
//// } // thor
//// ^^^^^^^^^^^^^^^^
//static void genForwardDeclaration(std::set<Declaration*>& classDecls, std::wostream& os)
//{
//    std::set<Declaration*> generated;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Forward declaration" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//    foreach(i, classDecls)
//    {
//        Declaration* decl = *i;
//        ClassDecl* classDecl = cast<ClassDecl>(decl);
//        if(generated.find(classDecl) == generated.end())
//        {
//            generated.insert(classDecl);
//            openCppNamespace(classDecl, os);
//            genCppClassName(classDecl->name, os);
//            os << L";" << std::endl;
//            closeCppNamespace(classDecl, os);
//        }
//    }
//    os << std::endl;
//}
//
//// class A {...};
//// class B {...};
//// class C {...};
//static void genClassDecls(std::vector<Declaration*>& classDecls, std::wostream& os)
//{
//    std::set<Declaration*> generated;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Class declaration" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//    foreach(i, classDecls)
//    {
//        Declaration* decl = *i;
//        ClassDecl* classDecl = cast<ClassDecl>(decl);
//        ClassDecl* templateClassDecl = classDecl;
//        if(InstantiatedFrom::get(classDecl) != NULL)
//            templateClassDecl = cast<ClassDecl>(InstantiatedFrom::get(classDecl));
//        if(generated.find(templateClassDecl) == generated.end())
//        {
//            generated.insert(templateClassDecl);
//            genClassDecl(templateClassDecl, os);
//        }
//    }
//}
//
//// #include <cinttypes>
//// typedef std::int8_t int8;
//// typedef std::int16_t int16;
//// typedef std::int32_t int32;
//// typedef std::int64_t int64;
//// typedef float float32;
//// typedef double float64;
//static void genPrimitiveType(std::wostream& os)
//{
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Primitive types" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//    os << L"#include <functional>" << std::endl;
//    os << L"template < typename T > struct AddPointerNonPrimitive { typedef typename std::conditional<std::is_class<T>::value, T, T*>::type type; };" << std::endl;
//}
//
//// template class Foo::Foo<NS::Bar>;
//static void genClassExplicitInstantiation(std::set<Declaration*>& instantiationSet, std::wostream& os)
//{
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Explicit class instantiation" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//
//    std::set<std::wstring> lines;
//    foreach(i, instantiationSet)
//    {
//        std::wostringstream oss;
//        Declaration* decl = *i;
//        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(decl->name);
//        BOOST_ASSERT(tid != NULL);
//
//        oss << L"template class ";
//        std::vector<Package*> packages = ASTNodeHelper::getParentScopePackages(getTemplateDecl(tid));
//        foreach(j, packages)
//            oss << (*j)->id->name << L"::";
//        oss << tid->id->toString();
//        oss << L"<";
//        foreach(j, tid->templated_type_list)
//        {
//            if(j != tid->templated_type_list.begin())
//                oss << L", ";
//            genNamespaceQualifiedType(AddStar(false), (*j)->specialized_type, oss);
//        }
//        oss << L">;" << std::endl;
//        lines.insert(oss.str());
//    }
//
//    foreach(i, lines)
//        os << *i;
//
//    os << std::endl;
//}
//
//// template class Foo::Foo<NS::Bar>;
//static void genFunctionExplicitInstantiation(std::set<Declaration*>& instantiationSet, std::wostream& os)
//{
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Explicit function instantiation" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//
//    std::set<std::wstring> lines;
//    foreach(i, instantiationSet)
//    {
//        std::wostringstream oss;
//        openCppNamespace(*i, oss);
//
//        FunctionDecl* funcDecl = cast<FunctionDecl>(*i);
//        BOOST_ASSERT(funcDecl != NULL);
//        TemplatedIdentifier* tid = cast<TemplatedIdentifier>(funcDecl->name);
//        BOOST_ASSERT(tid != NULL);
//
//        if(funcDecl->name->toString() == L"__cxa_delete")
//            continue;
//
//        oss << L"template ";
//
//		// no need to generate return type for constructor
//		if (!funcDecl->isConstructor())
//		{
//	        genNamespaceQualifiedType(AddStar(true), funcDecl->type, oss);
//	        oss << L" ";
//		}
//
//        // class name(if member function)
//		ClassDecl* classDecl = funcDecl->getOwner<ClassDecl>();
//		std::wstring class_name;
//        if(classDecl)
//        {
//        	if(isa<TemplatedIdentifier>(classDecl->name))
//        	{
//				auto tid = cast<TemplatedIdentifier>(classDecl->name);
//				class_name = tid->id->toString();
//				oss << class_name;
//
//				// append resolved type name to it
//				oss << L"<";
//				for (decltype(tid->templated_type_list.size()) i = 0; i < tid->templated_type_list.size(); i++)
//				{
//					auto typename_decl = tid->templated_type_list[i];
//					genNamespaceQualifiedType(false, typename_decl->specialized_type, oss);
//
//					if (i != tid->templated_type_list.size() - 1)
//						oss << ", ";
//				}
//				oss << L">::";
//        	}
//        	else
//        	{
//        		BOOST_ASSERT(isa<SimpleIdentifier>(classDecl->name));
//        		oss << classDecl->name->toString() << L"::";
//        	}
//        }
//
//        // function name
//		if (funcDecl->isConstructor())
//		{
//			// constructor should output class name
//			oss << class_name;
//		}
//		else
//		{
//	        oss << tid->id->toString();
//		}
//
//        oss << L"<";
//        foreach(j, tid->templated_type_list)
//        {
//            if(j != tid->templated_type_list.begin())
//                oss << L", ";
//
//            genNamespaceQualifiedType(AddStar(false), (*j)->specialized_type, oss);
//        }
//        oss << L">(";
//        genParameters(funcDecl->parameters, oss);
//        oss << L");" << std::endl;
//
//        closeCppNamespace(*i, oss);
//        lines.insert(oss.str());
//    }
//
//    foreach(i, lines)
//        os << *i;
//
//    os << std::endl;
//}
//
//// #include "..."
//static void genIncludeImplementation(std::set<Declaration*>& nativeClasses, std::wostream& os)
//{
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << L"// Include implementation" << std::endl;
//    os << L"//////////////////////////////////////////////////////////////////////////////" << std::endl;
//    os << std::endl;
//    foreach(i, nativeClasses)
//    {
//        ASTNode* primary_literal = ASTNodeHelper::findAnnotationValue(*i, {L"native", L"include"});
//        if(!primary_literal) continue;
//
//        StringLiteral* literal = ASTNodeHelper::castPrimaryExprStringLiteral(primary_literal);
//        if(!literal) continue;
//
//        os << L"#include \"" << literal->value << L"\"" << std::endl;
//        // TODO throw exception when it's not ready?
//    }
//    os << std::endl;
//	os << L"using namespace thor::lang;" << std::endl;
//}

//////////////////////////////////////////////////////////////////////////////
// member functions
//////////////////////////////////////////////////////////////////////////////

const char* CppGeneratorStage::name()
{
    return "Cpp Code Generation Stage";
}

std::pair<shared_ptr<po::options_description>, shared_ptr<po::options_description>> CppGeneratorStage::getOptions()
{
    shared_ptr<po::options_description> option_desc_public(new po::options_description());
    shared_ptr<po::options_description> option_desc_private(new po::options_description());

    option_desc_public->add_options()
        ("project-path", po::value<std::string>(), "set the project root path")
        ("build-path", po::value<std::string>(), "set the build directory")
        ("dump-graphviz", po::bool_switch(), "dump AST in graphviz format")
        ("dump-graphviz-dir", po::value<std::string>(), "dump AST in graphviz format")
        ("extract-bundle-path", po::value<std::string>(), "Explicitly designate the path to find dependent bundle AST. (For JIT bundle handling only)")
    ;

    for(auto& option : option_desc_public->options()) option_desc_private->add(option);

    option_desc_private->add_options()
    ;

    return std::make_pair(option_desc_public, option_desc_private);
}

bool CppGeneratorStage::parseOptions(po::variables_map& vm)
{
    if (vm.count("project-path"))
    {
        config.load(vm["project-path"].as<std::string>(), true, true);
    }
    else
    {
        config.loadDefault();
    }

    if (vm.count("build-path"))
        config.build_path_override = vm["build-path"].as<std::string>();

    if (vm.count("verbose"))
        config.verbose = true;
    else
        config.verbose = false;

    if (vm.count("extract-bundle-path"))
    {
        config.extract_bundle_path_override = vm["extract-bundle-path"].as<std::string>();
    }

    config.dump_graphviz = vm["dump-graphviz"].as<bool>();
    if(vm.count("dump-graphviz-dir") > 0)
    {
        // TODO remove the dump dir because it's always override
        config.dump_graphviz_dir = vm["dump-graphviz-dir"].as<std::string>();
    }

    return true;
}

template < typename T >
static void dump(T& decls, const std::wstring& title)
{
    std::wostringstream woss;
    woss << title << L" has " << decls.size() << L" Declarations" << std::endl;
    LOG4CXX_DEBUG(LoggerWrapper::GeneratorStage, woss.str());
    for(auto i = decls.begin(); i != decls.end(); ++i)
    {
        Declaration* decl = *i;
        woss.str(L"");
        woss << L"    " << std::left << std::setw(16) << decl->instanceName() << decl->name->toString() << std::endl;
        LOG4CXX_DEBUG(LoggerWrapper::GeneratorStage, woss.str());
    }
}

static void dump_pair(std::set<SourceGenerator::BaseDerivePair>& base_derives, const std::wstring& title)
{
    std::wostringstream woss;
    woss << title << L" has " << base_derives.size() << L" Base-Derived pairs" << std::endl;
    for(auto& base_derive : base_derives)
    {
        Declaration* base    = base_derive.first;
        Declaration* derived = base_derive.second;
        woss.str(L"");
        woss << L"    " << std::left << std::setw(16) << base->instanceName() << base->name->toString() << L" <= " << std::setw(16) << derived->instanceName() << derived->name->toString() << std::endl;
        LOG4CXX_DEBUG(LoggerWrapper::GeneratorStage, woss.str());
    }
}

bool CppGeneratorStage::execute(bool& continue_execution)
{
    UNUSED_ARGUMENT(continue_execution);

    if(!config.loadDefault(false, false, true))
    {
        std::cerr << "Error: failed to load project configuration" << std::endl;
        return false;
    }
    config.createDirectoriesIfNecessary();

    if(config.dump_graphviz)
    {
        boost::filesystem::path p = config.dump_graphviz_dir / "cpp-generation.dot";
        ASTNodeHelper::visualize(getParserContext().tangle, p.string());
    }

    std::set<SourceGenerator::BaseDerivePair>       baseDerivedPairs;
    // collect native class
    std::set<Declaration*>         nativeClasses                 = getNative<ClassDecl>();
    std::set<Declaration*>         nativeFunctions               = getNative<FunctionDecl>();
    std::set<Declaration*>         nativeFuncArgAndReturn        = getNativeFunctionUsedTypes(nativeFunctions);

    dump(nativeClasses              , L"nativeClasses                    ");
    dump(nativeFunctions            , L"nativeFunctions                  ");
    dump(nativeFuncArgAndReturn     , L"nativeFuncArgAndReturn           ");

    // collect type arg
    std::set<Declaration*>         classInstantiationSet;
    std::set<Declaration*>         functionInstantiationSet;
    std::set<Declaration*>         nativeClassesTypeArgs         = getNativeDeclsTypeArgsAndInstantiation<ClassDecl>(nativeClasses, classInstantiationSet);
    std::set<Declaration*>         nativeFuncsTypeArgs           = getNativeDeclsTypeArgsAndInstantiation<FunctionDecl>(nativeFunctions, functionInstantiationSet);

    BOOST_ASSERT( boost::algorithm::all_of(classInstantiationSet, InstantiatedFrom::get) );
    BOOST_ASSERT( boost::algorithm::all_of(functionInstantiationSet, InstantiatedFrom::get) );

    std::set<Declaration*>         nativeTypeArgs                = nativeClassesTypeArgs;

    nativeTypeArgs.insert(nativeFuncsTypeArgs.begin(), nativeFuncsTypeArgs.end());
    nativeTypeArgs.insert(nativeFuncArgAndReturn.begin(), nativeFuncArgAndReturn.end());

    dump(nativeClassesTypeArgs      , L"nativeClassesTypeArgs            ");
    dump(nativeFuncsTypeArgs        , L"nativeFuncsTypeArgs              ");
    dump(nativeTypeArgs             , L"nativeTypeArgs                   ");

    SourceGenerator generator;

    std::set<Declaration*>         extendedNativeClassesTypeArgs = getExtentedNativeClassesTypeArgs(nativeTypeArgs, baseDerivedPairs);
    std::vector<Declaration*>      sortedClassDecl               = generator.sortDeclByInheritence(extendedNativeClassesTypeArgs, baseDerivedPairs);
    std::set<EnumDecl*>            enumDecls                     = getEnumDecls();

    dump(extendedNativeClassesTypeArgs, L"extendedNativeClassesTypeArgs");
    dump_pair(baseDerivedPairs        , L"baseDerivedPairs");
    dump(sortedClassDecl              , L"sortedClassDecl           ");

    auto newEnd = std::remove_if(sortedClassDecl.begin(), sortedClassDecl.end(), [](Declaration* decl){
        return decl->hasAnnotation(L"native") || decl->hasAnnotation(L"system");
    });
    sortedClassDecl.erase(newEnd, sortedClassDecl.end());

    // no need to instantiate
    if(classInstantiationSet.empty() && functionInstantiationSet.empty())
        return true;

    // open file and write results to it
    const std::string temporary_file_name = "temporary_template_instantiation";
    auto cpp_file_path = config.build_path / (temporary_file_name + THOR_EXTENSION_CPP);

    std::wofstream cpp_file(cpp_file_path.string(), std::ios::trunc);
    generator.setWrite(&cpp_file);

    auto _= zillians::invoke_on_destroy([&generator](){
        generator.setWrite(nullptr);
    });

    generator.genPrimitiveType();
    generator.genPreIncludeForwardDeclaration(nativeFuncArgAndReturn);
    generator.genIncludeImplementation(nativeClasses);
    generator.genIncludeImplementation(nativeFunctions);
    generator.genForwardDeclaration(extendedNativeClassesTypeArgs);
    generator.genEnumDefinitions(enumDecls);
    generator.genClassDecls(sortedClassDecl);
    generator.genClassExplicitInstantiation(classInstantiationSet);
    generator.genFunctionExplicitInstantiation(functionInstantiationSet);

    auto obj_file_path = config.build_path / (temporary_file_name + THOR_EXTENSION_OBJ);
    if(!compile(config, cpp_file_path, obj_file_path))
    {
        std::cerr << "Error: failed to generate and compile C++ declarations" << std::endl;
        return false;
    }

    return true;
}

} } }
