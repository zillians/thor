/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/StringUtil.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/visitor/LLVMGeneratorStageStructPreambleVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {


LLVMGeneratorStageStructPreambleVisitor::LLVMGeneratorStageStructPreambleVisitor(llvm::LLVMContext& context, llvm::Module& module) :
    mGenerateForwardDeclaration(true), mContext(context), mModule(module), mBuilder(context), mHelper(context, mModule, mBuilder)
{
    REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
}

void LLVMGeneratorStageStructPreambleVisitor::generate(ASTNode& node)
{
    revisit(node);
}

void LLVMGeneratorStageStructPreambleVisitor::generate(ClassDecl& node)
{
    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    llvm::StructType* llvm_struct_type = mHelper.getStructType(node, mGenerateForwardDeclaration);

    if(llvm_struct_type == nullptr)
    {
        terminateRevisit();
        return;
    }

    revisit(node);
}

void LLVMGeneratorStageStructPreambleVisitor::generateForwardDeclaration(bool forward_decl_only) 
{
    mGenerateForwardDeclaration = forward_decl_only;
}


} } } }

