/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include "utility/StringUtil.h"
#include "language/GlobalConstant.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/GeneratorContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedVTableContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/visitor/LLVMGeneratorStageFunctionPreambleVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace language::tree;

LLVMGeneratorStageFunctionPreambleVisitor::LLVMGeneratorStageFunctionPreambleVisitor(llvm::LLVMContext& context, llvm::Module& module) :
    mContext(context), mModule(module), mBuilder(context), mHelper(context, module, mBuilder)
{
    REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
}

void LLVMGeneratorStageFunctionPreambleVisitor::generate(ASTNode& node)
{
    revisit(node);
}

void LLVMGeneratorStageFunctionPreambleVisitor::generate(Tangle& node)
{
    // generate PTX intrinsics if necessary
    if(getGeneratorContext().active_config->arch.is_cuda())
    {

    }

    revisit(node);
}

void LLVMGeneratorStageFunctionPreambleVisitor::generate(ClassDecl& node)
{
    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    revisit(node);
}

void LLVMGeneratorStageFunctionPreambleVisitor::generate(FunctionDecl& node)
{
    // we don't generate code for non-fully-specialized functions
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!GET_SYNTHESIZED_LLVM_FUNCTION(&node))
    {
        if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
            return;

        ClassDecl* class_decl = node.getOwner<ClassDecl>();

        // No need to generate virtual function declaration
        if (class_decl && class_decl->is_interface) return;

        // Check if it is member function, if so, we need to insert *this* pointer before other parameters
        const auto&      has_this           = node.is_member && !node.is_static;
              auto*const llvm_function_type = mHelper.getFunctionType(node);

        // try to resolve function type
        if(llvm_function_type == nullptr)
        {
            BOOST_ASSERT(false && "failed to generate LLVM function object");
            terminateRevisit();
            return;
        }

        llvm::GlobalValue::LinkageTypes linkage = llvm::GlobalValue::ExternalLinkage;
        if(node.is_member)
        {
            if(isa<TemplatedIdentifier>(node.getOwner<ClassDecl>()->name) || isa<TemplatedIdentifier>(node.name))
            {
                // TODO following code might broke while there is "native
                // specilaized class template", once class `Atomic` is added
                // back to the system bundle. The bug might presense.
                linkage = llvm::GlobalValue::WeakAnyLinkage;
            }
        }
        else
        {
            if(isa<TemplatedIdentifier>(node.name))
            {
                // TODO following code might broke while there is "native
                // specilaized class template", once class `Atomic` is added
                // back to the system bundle. The bug might presense.
                linkage = llvm::GlobalValue::WeakAnyLinkage;
            }
        }

        // TODO we should provide some generator name manging helper
        auto*const llvm_function = llvm::Function::Create(llvm_function_type, linkage, NameManglingContext::get(&node)->mangled_name, &mModule);

        // for GPU function, set the GPU device calling convention
        if(getGeneratorContext().active_config->arch.is_cuda())
        {
            llvm_function->setCallingConv(llvm::CallingConv::PTX_Device);
        }

        if(!llvm_function)
        {
            BOOST_ASSERT(false && "failed to generate LLVM function object");
            terminateRevisit();
            return;
        }

        // set function parameter names
        int index = 0;
        for(llvm::Function::arg_iterator i = llvm_function->arg_begin(); i != llvm_function->arg_end(); ++i)
        {
            std::string mangled_name;
            if (i == llvm_function->arg_begin() && has_this)
            {
                // the first parameter is *this* pointer, if it's member function
                mangled_name = "this";
            }
            else
            {
                mangled_name = NameManglingContext::get(node.parameters[index])->mangled_name;
                ++index;
            }
            i->setName(mangled_name);
        }

        // associate the LLVM function object with AST FunctionDecl object
        SET_SYNTHESIZED_LLVM_FUNCTION(&node, llvm_function);
    }
}

} } } }
