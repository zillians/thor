/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cstdint>

#include <vector>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/iterator_range.hpp>


#include <llvm/Config/config.h>
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/IR/InlineAsm.h>
    #include <llvm/IR/Attributes.h>
#else
    #include <llvm/InlineAsm.h>
    #include <llvm/Attributes.h>
#endif

#include "utility/Functional.h"
#include "utility/RangeUtil.h"

#include "language/context/ResolverContext.h"
#include "language/context/TransformerContext.h"
#include "language/GlobalConstant.h"
#include "language/logging/LoggerWrapper.h"

#include "language/context/ManglingStageContext.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/visitor/NodeInfoVisitor.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/generator/context/SynthesizedAllocaThisPointerContext.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/context/SynthesizedVirtualFunctionContext.h"
#include "language/stage/generator/context/SynthesizedVTableContext.h"
#include "language/stage/generator/visitor/LLVMGeneratorStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace language::tree;

static bool isFloatType(Expression& node)
{
    Type* node_resolved = node.getCanonicalType();
    if(!node_resolved)
        return false;
    return node_resolved->isFloatType();
}

static bool is_non_struct_lvalue(llvm::Value* v)
{
    if(!v->getType()->isPointerTy())
        return false;

    if(llvm::cast<llvm::PointerType>(v->getType())->getElementType()->isStructTy())
        return false;

    if(llvm::ConstantPointerNull::classof(v))
        return false;

    return true;
}

LLVMGeneratorStageVisitor::LLVMGeneratorStageVisitor(llvm::LLVMContext& context, llvm::Module& module) :
    mContext(context), mModule(module), mBuilder(context), mHelper(context, module, mBuilder)
{
    mFunctionContext.function            = nullptr;
    mFunctionContext.entry_block         = nullptr;
    mFunctionContext.return_block        = nullptr;
    mFunctionContext.alloca_insert_point = nullptr;
    mFunctionContext.mask_insertion = false;

    mAtomic.inAtomicBlock = false;
    mAtomic.tx            = nullptr;
    mAtomic.tm_begin      = nullptr;
    mAtomic.tm_commit     = nullptr;
    mAtomic.a_setjmp      = nullptr;
    mAtomic.a_stm_selt    = nullptr;
    mAtomic.ty_TxThread   = nullptr;
    mAtomic.ty_JmpBuf     = nullptr;

    REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
}

void LLVMGeneratorStageVisitor::generate(ASTNode& node)
{
    revisit(node);
}

void LLVMGeneratorStageVisitor::generate(NormalBlock& node)
{
    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    revisit(node);
}

void LLVMGeneratorStageVisitor::generate(AtomicBlock& node)
{
    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    mAtomic.setInAtomicBlock();
    blockInitialize();
    revisit(node);
    blockFinalize();
    mAtomic.clearInAtomicBlock();
}

void LLVMGeneratorStageVisitor::generate(NumericLiteral& node)
{
    if(hasValue(node)) return;

    revisit(node);

    llvm::Value* result = nullptr;
    switch(node.primitive_kind)
    {
        case PrimitiveKind::BOOL_TYPE:
            result = llvm::ConstantInt::get(llvm::IntegerType::getInt8Ty(mContext), node.value.b, false); break;
        case PrimitiveKind::INT8_TYPE:
            result = llvm::ConstantInt::get(llvm::IntegerType::getInt8Ty(mContext), node.value.i8, false); break;
        case PrimitiveKind::INT16_TYPE:
            result = llvm::ConstantInt::get(llvm::IntegerType::getInt16Ty(mContext), node.value.i16, false); break;
        case PrimitiveKind::INT32_TYPE:
            result = llvm::ConstantInt::get(llvm::IntegerType::getInt32Ty(mContext), node.value.i32, false); break;
        case PrimitiveKind::INT64_TYPE:
            result = llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(mContext), node.value.i64, false); break;
        case PrimitiveKind::FLOAT32_TYPE:
            result = llvm::ConstantFP::get(llvm::Type::getFloatTy(mContext), node.value.f32); break;
        case PrimitiveKind::FLOAT64_TYPE:
            result = llvm::ConstantFP::get(llvm::Type::getDoubleTy(mContext), node.value.f64); break;
        default:
            break;
    }

    if(!result)
    {
        BOOST_ASSERT(false && "invalid LLVM interpretation");
        terminateRevisit();
    }

    SET_SYNTHESIZED_LLVM_VALUE(&node, result);
}

void LLVMGeneratorStageVisitor::generate(ObjectLiteral& node)
{
    if(hasValue(node)) return;

    revisit(node);

    switch (node.type)
    {
    case ObjectLiteral::LiteralType::SUPER_OBJECT:
    case ObjectLiteral::LiteralType::THIS_OBJECT:
    {
        // Since they are called in member function, we could safely assume there exists *this* pointer.
        // We need to save the *this* value into Synthesized LLVM Context.
        FunctionDecl* function_decl = node.getOwner<FunctionDecl>();

        BOOST_ASSERT(function_decl->is_member && "using super/this in non-member functions");

        if (function_decl->is_static && node.type == ObjectLiteral::LiteralType::SUPER_OBJECT)
            break;

        BOOST_ASSERT(!function_decl->is_static && "incorrect use of super/this in static functions");

        ClassDecl* class_decl = node.getOwner<ClassDecl>();
        ClassDecl* target_class_decl = ResolvedType::get(&node)->getAsClassDecl();

        llvm::Value* llvm_this_value = loadThisPointer(*function_decl);
        llvm::Value* adjust_this_pointer = mHelper.adjustPointer(llvm_this_value, class_decl, target_class_decl);

        SET_SYNTHESIZED_LLVM_VALUE(&node, adjust_this_pointer);
        break;
    }
    case ObjectLiteral::LiteralType::NULL_OBJECT:
    {
        Type* resolved = node.getCanonicalType();
        BOOST_ASSERT(resolved && "should resolved to thor.lang.Object");
        ClassDecl* resolved_class = resolved->getAsClassDecl();

        llvm::StructType* llvm_struct_type = mHelper.getStructType(*resolved_class);

        llvm::Constant* null_pointer = llvm::ConstantPointerNull::get(llvm_struct_type->getPointerTo());
        SET_SYNTHESIZED_LLVM_VALUE(&node, null_pointer);
        break;
    }
    default:
    {
        BOOST_ASSERT(false && "Not implemented");
        break;
    }
    }
}

void LLVMGeneratorStageVisitor::generate(StringLiteral& node)
{
    if(hasValue(node)) return;

    revisit(node);
}

void LLVMGeneratorStageVisitor::generate(TypeIdLiteral& node)
{
    const auto&  global_offset_name = mHelper.getGlobalOffsetNameOfType(node.tangle_uuid);
    llvm::Type*  global_offset_type = llvm::Type::getInt64Ty(mContext);

    llvm::Value* global_offset      = mModule.getOrInsertGlobal(global_offset_name, global_offset_type);
    llvm::Value* adjusted_symbol_id = mBuilder.CreateAdd(
        mBuilder.CreateLoad(global_offset),
        llvm::ConstantInt::getSigned(llvm::Type::getInt64Ty(mContext), node.local_id)
    );

    SET_SYNTHESIZED_LLVM_VALUE(&node, adjusted_symbol_id);
}

void LLVMGeneratorStageVisitor::generate(SymbolIdLiteral& node)
{
    const auto&  global_offset_name = mHelper.getGlobalOffsetNameOfSymbol(node.tangle_uuid);
    llvm::Type*  global_offset_type = llvm::Type::getInt64Ty(mContext);

    llvm::Value* global_offset      = mModule.getOrInsertGlobal(global_offset_name, global_offset_type);
    llvm::Value* adjusted_symbol_id = mBuilder.CreateAdd(
        mBuilder.CreateLoad(global_offset),
        llvm::ConstantInt::getSigned(llvm::Type::getInt64Ty(mContext), node.local_id)
    );

    SET_SYNTHESIZED_LLVM_VALUE(&node, adjusted_symbol_id);
}

void LLVMGeneratorStageVisitor::generate(FunctionIdLiteral& node)
{
    const auto&  global_offset_name = mHelper.getGlobalOffsetNameOfFunction(node.tangle_uuid);
    llvm::Type*  global_offset_type = llvm::Type::getInt64Ty(mContext);

    llvm::Value* global_offset      = mModule.getOrInsertGlobal(global_offset_name, global_offset_type);
    llvm::Value* adjusted_symbol_id = mBuilder.CreateAdd(
        mBuilder.CreateLoad(global_offset),
        llvm::ConstantInt::getSigned(llvm::Type::getInt64Ty(mContext), node.local_id)
    );

    SET_SYNTHESIZED_LLVM_VALUE(&node, adjusted_symbol_id);
}

void LLVMGeneratorStageVisitor::generate(Tangle& node)
{
    generateAtomicDataStructures();

    revisit(node);
}

void LLVMGeneratorStageVisitor::generate(Source& node)
{
    // create LLVM global variables to store VTT and other global variables in thor
    //      if(!node.is_imported)
    {
        revisit(node);
    }
}

void LLVMGeneratorStageVisitor::generate(ClassDecl& node)
{
    // TODO handle structure generation
    visit(*node.name);

    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    revisit(node);
}

void LLVMGeneratorStageVisitor::generate(FunctionDecl& node)
{
    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if (node.hasAnnotation(L"native") || node.hasAnnotation(L"system"))
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    // if it's an imported function and is not a template, we should ignore it
    // (for imported template function, we should generate it as a weak symbol)
    if(node.getOwner<Source>()->is_imported)
    {
        if(node.is_member)
        {
            if(!isa<TemplatedIdentifier>(node.getOwner<ClassDecl>()->name) && !isa<TemplatedIdentifier>(node.name))
                return;
        }
        else
        {
            if(!isa<TemplatedIdentifier>(node.name))
                return;
        }
    }

    if(node.block == nullptr)
        return;

    if(isFunctionVisited(node))
        return;

    // create function signature (if necessary) and emit prologue of function
    if(!startFunction(node))
    {
        terminate();
        return;
    }

    // create alloca for all parameters
    if(!allocateParameters(node))
    {
        terminate();
        return;
    }

    // special function code generation will insert here
    specialFunctionHandling(node);

    // visit all children
    revisit(node);

    // emit epilogue of function
    if(!finishFunction(node))
    {
        terminate();
        return;
    }
}

void LLVMGeneratorStageVisitor::generate(VariableDecl& node)
{
    BOOST_ASSERT(node.parent && "null pointer exception");

    if (node.isParameter())
    {
        // skip initializer
        // default value should be handled in caller, not callee

        if(node.getAnnotations()) visit(*node.getAnnotations());
        if(node.name            ) visit(*node.name            );
        if(node.type            ) visit(*node.type            );
    }
    else
    {
        revisit(node);
    }

    if(node.hasOwner<FunctionDecl>())
    {
        // function parameter & local variable
        generateLocalVariable(node);
    }
}

void LLVMGeneratorStageVisitor::generate(DeclarativeStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    revisit(node);

    propagate(&node, node.declaration);
}

void LLVMGeneratorStageVisitor::generate(BranchStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    revisit(node);

    llvm::Value* result = nullptr;

    switch(node.opcode)
    {
        case BranchStmt::OpCode::BREAK:
            {
                ASTNode* iterative_node = node.getOwner<IterativeStmt>();
                llvm::BasicBlock* finalize_block = GET_SYNTHESIZED_LLVM_FINALIZE_BLOCK(iterative_node);
                BOOST_ASSERT(finalize_block && "Does not have finalize block");
                result = mBuilder.CreateBr(finalize_block);
                break;
            }
        case BranchStmt::OpCode::CONTINUE:
            {
                llvm::BasicBlock* jump_to_block = nullptr;
                ASTNode* iterative_node = node.getOwner<IterativeStmt>();
                if (isa<WhileStmt>(iterative_node))
                {
                    auto whileloop_node = cast<WhileStmt>(iterative_node);
                    jump_to_block = GET_SYNTHESIZED_LLVM_BLOCK(whileloop_node->cond);
                }
                else if (isa<ForStmt>(iterative_node))
                {
                    auto forloop_node = cast<ForStmt>(iterative_node);
                    jump_to_block = GET_SYNTHESIZED_LLVM_BLOCK(forloop_node->step);
                }
                else
                {
                    BOOST_ASSERT(false && "Not implement!");
                }
                result = mBuilder.CreateBr(jump_to_block);
                break;
            }
        case BranchStmt::OpCode::RETURN:
            {
                // return (expression);
                if(node.result)
                {
                    // single return value
                    if( !isa<TieExpr>(node.result) )
                    {
                        llvm::Value* read_value = getReadValue(node, *node.result);

                        BOOST_ASSERT(read_value && "invalid LLVM value for return instruction");

                        // If the return type is void, we don't have return value
                        if ( !mFunctionContext.return_values.empty() )
                        {
                            llvm::StoreInst* inst = mBuilder.CreateStore( read_value, mFunctionContext.return_values.front() );
                            ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
                        }

                        result = mBuilder.CreateBr(mFunctionContext.return_block);
                    }
                    // multiple return values
                    else
                    {
                        auto multiple_exprs = cast<TieExpr>(node.result)->tied_expressions;

                        // read values from expressions and store them into the variables to return
                        auto return_value = mFunctionContext.return_values.begin();
                        for( auto expr : multiple_exprs )
                        {
                            BOOST_ASSERT( return_value != mFunctionContext.return_values.end() && "mismatched # of expressions and return types" );

                            llvm::Value* read_value = getReadValue(node, *expr);

                            BOOST_ASSERT( read_value && "invalid LLVM value for return instruction" );

                            auto instruction = mBuilder.CreateStore( read_value, *return_value++ );
                            ADD_INTERMEDIATE_LLVM_VALUE( &node, instruction );
                        }

                        result = mBuilder.CreateBr( mFunctionContext.return_block );
                    }
                }
                // return;
                else
                {
                    result = mBuilder.CreateBr(mFunctionContext.return_block);
                }
                setBlockInsertionMask();
                break;
            }
    }

    if(!result)
    {
        BOOST_ASSERT(false && "invalid LLVM interpretation");
        terminateRevisit();
    }

    SET_SYNTHESIZED_LLVM_VALUE(&node, result);
}

void LLVMGeneratorStageVisitor::generate(ExpressionStmt& node)
{
    if(hasValue(node)) return;

    // special handling for static variable initialization
    // TODO: Note that the following implemenations cannot apply to ARM (according to clang implementation)
    VariableDecl* static_variable_decl = nullptr;
    if (ASTNode* ref = SplitReferenceContext::get(&node))
    {
        if (auto variable_decl = cast<VariableDecl>(ref))
        {
            if (variable_decl->is_static && variable_decl->hasOwner<FunctionDecl>())
            {
                static_variable_decl = variable_decl;
            }
        }
    }

    // create condition check
    llvm::BasicBlock* current_block = mBuilder.GetInsertBlock();
    llvm::Function* llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(node.getOwner<FunctionDecl>());
    llvm::BasicBlock* cxa_guard_acquire_end_block = nullptr;
    llvm::Value* guard_value = nullptr;
    if (static_variable_decl)
    {
        // create block for later use
        llvm::BasicBlock* cxa_guard_acquire_block = createBasicBlock("guard.acquire.check", llvm_function);
        llvm::BasicBlock* cxa_guard_init_block = createBasicBlock("guard.init", llvm_function);
        cxa_guard_acquire_end_block = createBasicBlock("guard.acquire.end", llvm_function);

        // Atomic load with Acquire order
        guard_value = GET_SYNTHESIZED_LLVM_GUARD_VALUE(static_variable_decl);
        llvm::LoadInst* load_inst = mBuilder.CreateLoad(mBuilder.CreateBitCast(guard_value, mBuilder.getInt8PtrTy()));
        load_inst->setAlignment(1);
        load_inst->setAtomic(llvm::Acquire);

        // check whether the first byte of the guard value is initialized
        llvm::Value* is_uninitialized = mBuilder.CreateIsNull(load_inst, "guard.uninitialized");

        // Check if the first byte of the guard variable is zero, which means not initialized
        createCondBr(is_uninitialized, cxa_guard_acquire_block, cxa_guard_acquire_end_block);
        enterBasicBlock(cxa_guard_acquire_block);

        // generate codes for __cxa_guard_acquire
        llvm::Value* llvm_result = mBuilder.CreateCall(generateGuardAcquire(), guard_value);
        llvm::Value* is_initialized = mBuilder.CreateIsNull(llvm_result, "static_variable.initialized");
        createCondBr(is_initialized, cxa_guard_acquire_end_block, cxa_guard_init_block);
        enterBasicBlock(cxa_guard_init_block);

        // well, we need to add the static local variable address to root set
        Type* resolved_type = static_variable_decl->type->getCanonicalType();
        if (resolved_type && resolved_type->isRecordType())
        {
            callAddToGlobalRootSet(GET_SYNTHESIZED_LLVM_VALUE(static_variable_decl));
        }

        // re-arrange the block so that the created blocks locate after the current block
        cxa_guard_acquire_end_block->moveAfter(current_block);
        cxa_guard_init_block->moveAfter(current_block);
        cxa_guard_acquire_block->moveAfter(current_block);
    }

    revisit(node);

    if (static_variable_decl)
    {
        // finalization
        mBuilder.CreateCall(generateGuardRelease(), guard_value);
        enterBasicBlock(cxa_guard_acquire_end_block);
    }

    propagate(&node, node.expr);
}

void LLVMGeneratorStageVisitor::generate(IfElseStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    // TODO simplify the blocks by removing or merging unnecessary blocks
    // generate code into blocks
    std::vector<llvm::BasicBlock*> llvm_blocks;
    {
        // for if branch
        {
            llvm::BasicBlock* cond = createBasicBlock("if.eval", mFunctionContext.function);
            SET_SYNTHESIZED_LLVM_BLOCK(node.if_branch->cond, cond);

            // jump from current block to the condition evaluation block
            enterBasicBlock(cond, true);
            visit(*node.if_branch->cond);

            llvm::BasicBlock* eval_last_active_block = mBuilder.GetInsertBlock();

            llvm::BasicBlock* block = createBasicBlock("if.then", mFunctionContext.function);

            // because we create linkage among blocks on our own in this if/else structure, so we don't emit branch instruction automatically
            enterBasicBlock(block, false);
            visit(*node.if_branch->block);

            llvm::BasicBlock* last_active_block = mBuilder.GetInsertBlock();
            SET_SYNTHESIZED_LLVM_BLOCK(node.if_branch->block, last_active_block);

            llvm_blocks.push_back(cond);
            llvm_blocks.push_back(eval_last_active_block);
            llvm_blocks.push_back(block);
            llvm_blocks.push_back(last_active_block);
        }

        // for elif branch
        {
            for(Selection* elseif_branch: node.elseif_branches)
            {
                llvm::BasicBlock* cond = createBasicBlock("elif.eval", mFunctionContext.function);
                SET_SYNTHESIZED_LLVM_BLOCK(elseif_branch->cond, cond);

                enterBasicBlock(cond, false);
                visit(*elseif_branch->cond);

                llvm::BasicBlock* eval_last_active_block = mBuilder.GetInsertBlock();

                llvm::BasicBlock* block = createBasicBlock("elif.then", mFunctionContext.function);

                enterBasicBlock(block, false);
                visit(*elseif_branch->block);

                llvm::BasicBlock* last_active_block = mBuilder.GetInsertBlock();
                SET_SYNTHESIZED_LLVM_BLOCK(elseif_branch->block, last_active_block);

                llvm_blocks.push_back(cond);
                llvm_blocks.push_back(eval_last_active_block);
                llvm_blocks.push_back(block);
                llvm_blocks.push_back(last_active_block);
            }
        }

        // for else branch
        if(node.else_block)
        {
            llvm::BasicBlock* block = createBasicBlock("else.then", mFunctionContext.function);

            enterBasicBlock(block, false);
            visit(*node.else_block);

            llvm::BasicBlock* last_active_block = mBuilder.GetInsertBlock();
            SET_SYNTHESIZED_LLVM_BLOCK(node.else_block, last_active_block);

            llvm_blocks.push_back(block);
            llvm_blocks.push_back(last_active_block);
        }

        // for the last block
        {
            llvm::BasicBlock* block = createBasicBlock("if.finalized", mFunctionContext.function);
            llvm_blocks.push_back(block);
        }
    }

    // create linkage among blocks
    {
        // for if branch
        {

            if(!isBlockTerminated(llvm_blocks[0]))
            {
                mBuilder.SetInsertPoint(llvm_blocks[0]); // [0] is the 'condition evaluation block for if branch'
                llvm::Value* read_value = getReadValue(*node.if_branch->cond, *node.if_branch->cond);
                createCondBr(read_value, llvm_blocks[2], llvm_blocks[4]); // [1] is the 'then block for if branch', [2] could be the 'condition evaluation block for first elif branch', or the 'else block', or the 'end block'
            }

            if(!isBlockTerminated(llvm_blocks[1]))
            {
                mBuilder.SetInsertPoint(llvm_blocks[1]); // [0] is the 'condition evaluation block for if branch'
                llvm::Value* read_value = getReadValue(*node.if_branch->cond, *node.if_branch->cond);
                createCondBr(read_value, llvm_blocks[2], llvm_blocks[4]); // [2] is the 'then block for if branch', [4] could be the 'condition evaluation block for first elif branch', or the 'else block', or the 'end block'
            }

            if(!isBlockTerminated(llvm_blocks[2]))
            {
                mBuilder.SetInsertPoint(llvm_blocks[2]); // [2] is the first block of 'then block for if branch'
                mBuilder.CreateBr(llvm_blocks.back()); // [back()] is the 'end block'
            }

            if(!isBlockTerminated(llvm_blocks[3]))
            {
                mBuilder.SetInsertPoint(llvm_blocks[3]); // [2] is the last block of 'then block for if branch'
                mBuilder.CreateBr(llvm_blocks.back()); // [back()] is the 'end block'
            }
        }

        // for all elif branch
        int index = 4;
        {
            for(Selection* elseif_branch: node.elseif_branches)
            {
                if(!isBlockTerminated(llvm_blocks[index]))
                {
                    mBuilder.SetInsertPoint(llvm_blocks[index]);
                    llvm::Value* read_value = getReadValue(*(elseif_branch->cond), *(elseif_branch->cond));
                    createCondBr(read_value, llvm_blocks[index+2], llvm_blocks[index+4]);
                }

                if(!isBlockTerminated(llvm_blocks[index+1]))
                {
                    mBuilder.SetInsertPoint(llvm_blocks[index]);
                    llvm::Value* read_value = getReadValue(*(elseif_branch->cond), *(elseif_branch->cond));
                    createCondBr(read_value, llvm_blocks[index+2], llvm_blocks[index+4]);
                }

                if(!isBlockTerminated(llvm_blocks[index+2]))
                {
                    mBuilder.SetInsertPoint(llvm_blocks[index+2]);
                    mBuilder.CreateBr(llvm_blocks.back());
                }

                if(!isBlockTerminated(llvm_blocks[index+3]))
                {
                    mBuilder.SetInsertPoint(llvm_blocks[index+3]);
                    mBuilder.CreateBr(llvm_blocks.back());
                }

                index += 4;
            }
        }

        // for else branch
        if(node.else_block)
        {
            if(!isBlockTerminated(llvm_blocks[index+1]))
            {
                mBuilder.SetInsertPoint(llvm_blocks[index+1]);
                mBuilder.CreateBr(llvm_blocks.back());
            }
            index += 2;
        }

        // for the last block
        {
            enterBasicBlock(llvm_blocks.back(), false);
            index += 1;
        }

        BOOST_ASSERT(index == (int)llvm_blocks.size());
    }
}

void LLVMGeneratorStageVisitor::generate(ForStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    // create the finalized block beforehand
    llvm::BasicBlock* init_block = createBasicBlock("for.init", mFunctionContext.function);
    SET_SYNTHESIZED_LLVM_BLOCK(node.init, init_block);

    llvm::BasicBlock* cond_block = createBasicBlock("for.cond", mFunctionContext.function);
    SET_SYNTHESIZED_LLVM_BLOCK(node.cond, cond_block);

    llvm::BasicBlock* step_block = createBasicBlock("for.step", mFunctionContext.function);
    SET_SYNTHESIZED_LLVM_BLOCK(node.step, step_block);

    llvm::BasicBlock* action_block = createBasicBlock("for.action", mFunctionContext.function);
    SET_SYNTHESIZED_LLVM_BLOCK(node.block, action_block);

    llvm::BasicBlock* finalized_block = createBasicBlock("for.finalized", mFunctionContext.function);
    SET_SYNTHESIZED_LLVM_FINALIZE_BLOCK(&node, finalized_block);

    // jump from current block to the condition evaluation block
    enterBasicBlock(init_block);
    if(node.init) visit(*node.init);

    // after the init block, the next block is condition block
    enterBasicBlock(cond_block);
    visit(*node.cond);

    // conditional branch to either action block or finalized block
    llvm::Value* llvm_cond = GET_SYNTHESIZED_LLVM_VALUE(node.cond);
    createCondBr(llvm_cond, action_block, finalized_block);

    enterBasicBlock(action_block);
    if(node.block) visit(*node.block);

    // after the action block, the next block is step block
    enterBasicBlock(step_block);
    if(node.step) visit(*node.step);

    // after the step block, the next block is the condition block (since we've completed the code generation for condition block, here we just simply make a branch to it)
    enterBasicBlock(cond_block);

    // enter finalized block
    enterBasicBlock(finalized_block);
}

void LLVMGeneratorStageVisitor::generate(ForeachStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    //      if(node.if_branch.cond) user_visitor->
    //      if(node.if_branch.block) user_visitor->visit(*node.if_branch.block);
    //      foreach(i, node.elseif_branches)
    //      {
    //          if(i->cond) user_visitor->visit(*i->cond);
    //          if(i->block) user_visitor->visit(*i->block);
    //      }
    //      if(node.else_block) user_visitor->visit(*node.else_block);
    //
    //      if(node.annotations) user_visitor->visit(*node.annotations);

    // emit the preamble and prepare blocks
}

void LLVMGeneratorStageVisitor::generate(WhileStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    // emit the preamble and prepare blocks
    if(node.style == WhileStmt::Style::WHILE)
    {
        llvm::BasicBlock* cond_block = createBasicBlock("while.eval", mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_BLOCK(node.cond, cond_block);

        llvm::BasicBlock* action_block = createBasicBlock("while.action", mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_BLOCK(node.block, action_block);

        llvm::BasicBlock* finalized_block = createBasicBlock("while.finalized", mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_FINALIZE_BLOCK(&node, finalized_block);

        // jump from current block to the condition evaluation block
        enterBasicBlock(cond_block);
        visit(*node.cond);

        llvm::BasicBlock* cond_last_active_block = mBuilder.GetInsertBlock(); // as new basic block can be created last visitation, we need to store the last active basic block
        SET_SYNTHESIZED_LLVM_BLOCK(node.cond, cond_last_active_block);

        // conditional branch to either action block or finalized block
        llvm::Value* read_value = getReadValue(node, *node.cond);
        createCondBr(read_value, action_block, finalized_block);

        enterBasicBlock(action_block);
        if(node.block) visit(*node.block);

        llvm::BasicBlock* action_last_active_block = mBuilder.GetInsertBlock(); // as new basic block can be created last visitation, we need to store the last active basic block
        SET_SYNTHESIZED_LLVM_BLOCK(node.block, action_last_active_block);

        // after the action block, the next block is the conditional block (since we've completed the code generation for condition block, here we just simply make a branch to it)
        enterBasicBlock(cond_block);

        // enter finalized block
        enterBasicBlock(finalized_block);
    }
    else if(node.style == WhileStmt::Style::DO_WHILE)
    {
        llvm::BasicBlock* action_block = createBasicBlock("while.action", mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_BLOCK(node.block, action_block);

        llvm::BasicBlock* cond_block = createBasicBlock("while.eval", mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_BLOCK(node.cond, cond_block);

        llvm::BasicBlock* finalized_block = createBasicBlock("while.finalized", mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_FINALIZE_BLOCK(&node, finalized_block);

        // jump from current block to the condition evaluation block
        enterBasicBlock(action_block);
        if(node.block) visit(*node.block);

        llvm::BasicBlock* action_last_active_block = mBuilder.GetInsertBlock(); // as new basic block can be created last visitation, we need to store the last active basic block
        SET_SYNTHESIZED_LLVM_BLOCK(node.block, action_last_active_block);

        // conditional branch to either action block or finalized block
        enterBasicBlock(cond_block);
        visit(*node.cond);

        llvm::BasicBlock* cond_last_active_block = mBuilder.GetInsertBlock(); // as new basic block can be created last visitation, we need to store the last active basic block
        SET_SYNTHESIZED_LLVM_BLOCK(node.cond, cond_last_active_block);

        llvm::Value* read_value = getReadValue(node, *node.cond);
        createCondBr(read_value, action_block, finalized_block);

        // enter finalized block
        enterBasicBlock(finalized_block);
    }
}

void LLVMGeneratorStageVisitor::generate(SwitchStmt& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    // create the final block beforehand since it's required by CreateSwitch()
    llvm::BasicBlock* finalized_block = createBasicBlock("switch.finalized", mFunctionContext.function);
    llvm::BasicBlock* default_case_block = createBasicBlock("switch.case.default", mFunctionContext.function);

    // create the initial block
    llvm::BasicBlock* init_block = createBasicBlock("switch.init", mFunctionContext.function);
    enterBasicBlock(init_block, true);

    visit(*node.node);

    llvm::Value* read_value = getReadValue(node, *node.node);
    llvm::SwitchInst* switch_inst = mBuilder.CreateSwitch(read_value, default_case_block, (node.default_block) ? (node.cases.size() +1) : node.cases.size());

    std::vector<llvm::BasicBlock*> case_blocks;
    int index = 0;
    for(Selection* case_: node.cases)
    {
        std::ostringstream case_block_name; case_block_name << "switch.case." << index;
        llvm::BasicBlock* case_block = createBasicBlock(case_block_name.str(), mFunctionContext.function);
        SET_SYNTHESIZED_LLVM_BLOCK(&(*case_->block), case_block);

        // BUG SOLVED: the enter basic block will resetBlockInsertionMask(), so we need to call it first or any following visit will have no effect (return by isBlockInsertionMasked())
        // The problem is visit(*i->block) may have BranchStmt (return) in it which will block any code generation until we call enterBasicBlock() again.
        enterBasicBlock(case_block, false);

        visit(*case_->cond);
        llvm::Value* cond = getReadValue(*case_->cond, *case_->cond);
        BOOST_ASSERT(llvm::isa<llvm::ConstantInt>(cond) && "condition in switch statement must be constant");

        visit(*case_->block);

        llvm::BasicBlock* case_last_active_block = mBuilder.GetInsertBlock();

        case_blocks.push_back(case_block);
        case_blocks.push_back(case_last_active_block);

        switch_inst->addCase(llvm::cast<llvm::ConstantInt>(cond), case_block);

        ++index;
    }

    // enter default block
    if(node.default_block)
    {
        // if there's default block, we should visit the default block and have all instructions inserted into default_case_block
        enterBasicBlock(default_case_block, false);
        visit(*node.default_block);

        llvm::BasicBlock* case_last_active_block = mBuilder.GetInsertBlock();
        SET_SYNTHESIZED_LLVM_BLOCK(node.default_block, default_case_block);

        case_blocks.push_back(default_case_block);
        case_blocks.push_back(case_last_active_block);
    }
    else
    {
        // if there's no default block, we simply push the default case block to the list
        case_blocks.push_back(default_case_block);
        case_blocks.push_back(default_case_block);
    }

    BOOST_ASSERT(case_blocks.size() % 2 == 0);

    // create linkage among case blocks
    for(decltype(case_blocks.size()) i=0;i<case_blocks.size();i++)
    {
        if(!isBlockTerminated(case_blocks[i]))
        {
            mBuilder.SetInsertPoint(case_blocks[i]);
            mBuilder.CreateBr(finalized_block);
        }
    }

    // enter finalized block
    enterBasicBlock(finalized_block);
}

void LLVMGeneratorStageVisitor::generate(IdExpr& node)
{
    if(node.getOwner<Annotation>())
        return;

    if(hasValue(node))
        return;

    if(isBlockInsertionMasked())
        return;

    revisit(node);

    propagate(&node, ASTNodeHelper::getCanonicalSymbol(&node));

    // Special handling if the primary expression locates in the non-static function
    // and access member function or member variable. If the following conditions hold,
    // we could consider we are in the case.
    // 1. Check if the primary expression belongs to non-static member functions
    // 2. Check if the resolved symbol of the primary expression is the direct child of ClassDecl
    FunctionDecl* function_decl = node.getOwner<FunctionDecl>();
    if (function_decl && function_decl->is_static == false)
    {
        ASTNode* resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&node);
        if (resolved_symbol && resolved_symbol->parent &&
                isa<ClassDecl>(resolved_symbol->parent) &&
                !isStatic(resolved_symbol) )
        {
            // Well, we gonna retrieve this pointer from the first parameter of the function
            llvm::Value* llvm_this_pointer = loadThisPointer(*function_decl);
            BOOST_ASSERT(llvm_this_pointer && "Missing the first argument in member function, should be *this* pointer");

            if (FunctionDecl* ast_function = cast<FunctionDecl>(resolved_symbol))
            {
                // TODO: The following codes seem to be similar to MemberExpr. Refactor it!
                ClassDecl* ast_class =cast<ClassDecl>(resolved_symbol->parent);

                llvm::Value* virtual_function = nullptr;

                if (ast_function->is_virtual)
                    virtual_function = getVirtualFunction(*ast_class, *ast_function, llvm_this_pointer);

                // According to the resolved ast_function, we need to adjust the this pointer
                ClassDecl* source_class = function_decl->getOwner<ClassDecl>();
                llvm_this_pointer = mHelper.adjustPointer(llvm_this_pointer, source_class, ast_class);

                SET_SYNTHESIZED_VIRTUAL_FUNCTION(&node, llvm_this_pointer, virtual_function);
            }
            else if (VariableDecl* ast_variable = cast<VariableDecl>(resolved_symbol))
            {
                // Retrieve the object layout from the class declaration
                ClassDecl* class_decl = cast<ClassDecl>(resolved_symbol->parent);
                SynthesizedObjectLayoutContext* object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(class_decl);

                BOOST_ASSERT(object_layout->member_attributes.count(ast_variable) != 0);
                auto index = object_layout->member_attributes[ast_variable].get<0>();
                llvm::Value* member_ptr = mBuilder.CreateStructGEP(llvm_this_pointer, index);

                SET_SYNTHESIZED_LLVM_VALUE(&node, member_ptr);
            }
        }
    }
}

void LLVMGeneratorStageVisitor::generate(UnaryExpr& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    revisit(node);

    llvm::Value* result = nullptr;
    llvm::Value* temp   = nullptr;

    llvm::Value* read_value  = getReadValue (node, *node.node);
    llvm::Value* write_value = (node.opcode >= UnaryExpr::OpCode::INCREMENT && node.opcode <= UnaryExpr::OpCode::DECREMENT) ? getWriteValue(node, *node.node) : nullptr;

    switch(node.opcode)
    {
        case UnaryExpr::OpCode::INCREMENT:
            if(mAtomic.isInAtomicBlock())
            {
                auto llvm_value_tx = mBuilder.CreateLoad(mAtomic.tx);

                if(read_value->getType()->isIntegerTy()) temp = mBuilder.CreateAdd (read_value, llvm::ConstantInt::get(read_value->getType(), 1, false));
                else                                     temp = mBuilder.CreateFAdd(read_value, llvm::ConstantFP ::get(read_value->getType(), 1));

                mBuilder.CreateCall3(mAtomic.getWriteFunction(&node), write_value, temp, llvm_value_tx);
                result = write_value;
            }
            else
            {
                if(read_value->getType()->isIntegerTy()) temp = mBuilder.CreateAdd (read_value, llvm::ConstantInt::get(read_value->getType(), 1, false));
                else                                     temp = mBuilder.CreateFAdd(read_value, llvm::ConstantFP ::get(read_value->getType(), 1));

                mBuilder.CreateStore(temp, write_value);
                result = write_value;
            }
            break;
        case UnaryExpr::OpCode::DECREMENT:
            if(mAtomic.isInAtomicBlock())
            {
                auto llvm_value_tx = mBuilder.CreateLoad(mAtomic.tx);

                if(read_value->getType()->isIntegerTy()) temp = mBuilder.CreateSub (read_value, llvm::ConstantInt::get(read_value->getType(), 1, false));
                else                                     temp = mBuilder.CreateFSub(read_value, llvm::ConstantFP ::get(read_value->getType(), 1));

                mBuilder.CreateCall3(mAtomic.getWriteFunction(&node), write_value, temp, llvm_value_tx);
                result = write_value;
            }
            else
            {
                if(read_value->getType()->isIntegerTy()) temp = mBuilder.CreateSub (read_value, llvm::ConstantInt::get(read_value->getType(), 1, false));
                else                                     temp = mBuilder.CreateFSub(read_value, llvm::ConstantFP ::get(read_value->getType(), 1));

                mBuilder.CreateStore(temp, write_value);
                result = write_value;
            }
            break;
        case UnaryExpr::OpCode::BINARY_NOT:
            result = mBuilder.CreateNot(read_value); break;
        case UnaryExpr::OpCode::LOGICAL_NOT:
            {
                auto llvm_value = mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_EQ,
                        read_value,
                        llvm::ConstantInt::get(read_value->getType(), 0, false) );
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;
        case UnaryExpr::OpCode::ARITHMETIC_NEGATE:
            result = (isFloatType(node)) ? mBuilder.CreateFNeg(read_value) : mBuilder.CreateNeg(read_value); break;
        case UnaryExpr::OpCode::NEW:
            break;
        case UnaryExpr::OpCode::NOOP:
            // since the getValue() may add the llvm value to the intermediate set and for noop unary expression there's no "intermediate value"
            // we should remove the intermediate value from intermediate set explicitly
            // TODO should be refactored somehow?
            REMOVE_INTERMEDIATE_LLVM_VALUE(&node, read_value);
            result = read_value;
            break;
        default:
            result = nullptr;
            break;
    }

    if(!result)
    {
        BOOST_ASSERT(false && "invalid LLVM interpretation");
        terminateRevisit();
    }

    SET_SYNTHESIZED_LLVM_VALUE(&node, result);
}

void LLVMGeneratorStageVisitor::generate(UnpackExpr& node)
{
    visit( *node.call );

    // get function return values
    llvm::Value* read_value = getReadValue(node, *node.call);

    using Index = unsigned;
    Index element_index = 0;
    for ( auto identifier : node.unpack_list )
    {
        auto element_value = mBuilder.CreateExtractValue( read_value,
                                                          llvm::ArrayRef<Index>(element_index++) );

        llvm::Value* write_value = getWriteValue(node, *identifier);

        mBuilder.CreateStore( element_value, write_value );
    }
}

void LLVMGeneratorStageVisitor::generate(BinaryExpr& node)
{

    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))
        return;

    // do short circuit evaluation if meet following operations
    if( node.opcode == BinaryExpr::OpCode::LOGICAL_AND || node.opcode == BinaryExpr::OpCode::LOGICAL_OR )
    {
        shortCircuitEval( node );
        return;
    }

    revisit( node );

    llvm::Value* lhs_read_value = (node.opcode != BinaryExpr::OpCode::ASSIGN) ? getReadValue(node, *node.left) : nullptr;
    llvm::Value* rhs_read_value = getReadValue(node, *node.right);

    bool is_atomic_assign                 = mAtomic.isInAtomicBlock() && node.isAssignment();
    llvm::LoadInst* llvm_load_tx          = nullptr;
    llvm::Value*    llvm_binary_op_result = nullptr;
    llvm::Value*    result                = nullptr;

    if(is_atomic_assign)
        llvm_load_tx = mBuilder.CreateLoad(mAtomic.tx);
    else
        llvm_load_tx = nullptr;

    switch(node.opcode)
    {
        // Assignments Operations
        case BinaryExpr::OpCode::ASSIGN:
            llvm_binary_op_result = rhs_read_value;
            break;
        case BinaryExpr::OpCode::RSHIFT_ASSIGN:
            llvm_binary_op_result = mBuilder.CreateShl(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::LSHIFT_ASSIGN:
            llvm_binary_op_result = mBuilder.CreateAShr(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::ADD_ASSIGN:
            llvm_binary_op_result = (isFloatType(*node.left)) ? mBuilder.CreateFAdd(lhs_read_value, rhs_read_value)
                                                              : mBuilder.CreateAdd(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::SUB_ASSIGN:
            llvm_binary_op_result = (isFloatType(*node.left)) ? mBuilder.CreateFSub(lhs_read_value, rhs_read_value)
                                                              : mBuilder.CreateSub(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::MUL_ASSIGN:
            llvm_binary_op_result = (isFloatType(*node.left)) ? mBuilder.CreateFMul(lhs_read_value, rhs_read_value)
                                                              : mBuilder.CreateMul(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::DIV_ASSIGN:
            llvm_binary_op_result = (isFloatType(*node.left)) ? mBuilder.CreateFDiv(lhs_read_value, rhs_read_value)
                                                              : mBuilder.CreateUDiv(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::MOD_ASSIGN:
            llvm_binary_op_result = (isFloatType(*node.left)) ? mBuilder.CreateFRem(lhs_read_value, rhs_read_value)
                                                              : mBuilder.CreateURem(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::AND_ASSIGN:
            llvm_binary_op_result = mBuilder.CreateAnd(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::OR_ASSIGN:
            llvm_binary_op_result = mBuilder.CreateOr(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::XOR_ASSIGN:
            llvm_binary_op_result = mBuilder.CreateXor(lhs_read_value, rhs_read_value);
            break;

            // Arithmetic Operations
        case BinaryExpr::OpCode::ARITHMETIC_ADD:
            result = (isFloatType(*node.left)) ? mBuilder.CreateFAdd(lhs_read_value, rhs_read_value)
                                               : mBuilder.CreateAdd(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::ARITHMETIC_SUB:
            result = (isFloatType(*node.left)) ? mBuilder.CreateFSub(lhs_read_value, rhs_read_value)
                                               : mBuilder.CreateSub(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::ARITHMETIC_MUL:
            result = (isFloatType(*node.left)) ? mBuilder.CreateFMul(lhs_read_value, rhs_read_value)
                                               : mBuilder.CreateMul(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::ARITHMETIC_DIV:
            result = (isFloatType(*node.left)) ? mBuilder.CreateFDiv(lhs_read_value, rhs_read_value)
                                               : mBuilder.CreateSDiv(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::ARITHMETIC_MOD:
            result = (isFloatType(*node.left)) ? mBuilder.CreateFRem(lhs_read_value, rhs_read_value)
                                               : mBuilder.CreateSRem(lhs_read_value, rhs_read_value);
            break;

            // Arithmetic Bitwise Operations
        case BinaryExpr::OpCode::BINARY_AND:
            result = mBuilder.CreateAnd(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::BINARY_OR:
            result = mBuilder.CreateOr(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::BINARY_XOR:
            result = mBuilder.CreateXor(lhs_read_value, rhs_read_value);
            break;
        case BinaryExpr::OpCode::BINARY_LSHIFT:
            result = mBuilder.CreateShl(lhs_read_value, rhs_read_value);
            break;
            break;
        case BinaryExpr::OpCode::BINARY_RSHIFT:
            // We use AShr since all the primitive type which allowed to be shifted are signed
            result = mBuilder.CreateAShr(lhs_read_value, rhs_read_value);
            break;

            // Logical Comparison
        case BinaryExpr::OpCode::COMPARE_EQ:
            {
                auto llvm_value = (isFloatType(*node.left)) ? mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_UEQ, lhs_read_value, rhs_read_value)
                                                            : mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_EQ, lhs_read_value, rhs_read_value);
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;
        case BinaryExpr::OpCode::COMPARE_NE:
            {
                auto llvm_value = (isFloatType(*node.left)) ? mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_UNE, lhs_read_value, rhs_read_value)
                                                            : mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_NE, lhs_read_value, rhs_read_value);
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;
        case BinaryExpr::OpCode::COMPARE_GT:
            {
                auto llvm_value = (isFloatType(*node.left)) ? mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_UGT, lhs_read_value, rhs_read_value)
                                                            : mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_SGT, lhs_read_value, rhs_read_value);
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;
        case BinaryExpr::OpCode::COMPARE_LT:
            {
                auto llvm_value = (isFloatType(*node.left)) ? mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_ULT, lhs_read_value, rhs_read_value)
                                                            : mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_SLT, lhs_read_value, rhs_read_value);
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;
        case BinaryExpr::OpCode::COMPARE_GE:
            {
                auto llvm_value = (isFloatType(*node.left)) ? mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_UGE, lhs_read_value, rhs_read_value)
                                                            : mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_SGE, lhs_read_value, rhs_read_value);
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;
        case BinaryExpr::OpCode::COMPARE_LE:
            {
                auto llvm_value = (isFloatType(*node.left)) ? mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_ULE, lhs_read_value, rhs_read_value)
                                                            : mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_SLE, lhs_read_value, rhs_read_value);
                result = mBuilder.CreateZExt(llvm_value, llvm::Type::getInt8Ty(mContext));
            }
            break;

            // Range Operator
        case BinaryExpr::OpCode::RANGE_ELLIPSIS:
            BOOST_ASSERT(false && "not yet implemented");
            break;

        default:
            BOOST_ASSERT(false && "not yet implemented");
            result = nullptr; break;
    }

    // Assign operator calculation result
    if(node.isAssignment())
    {
        BOOST_ASSERT(result == nullptr);
        llvm::Value* lhs_write_value = node.isAssignment() ? getWriteValue(node, *node.left) : nullptr;
        bool is_global_or_member = llvm::isa<llvm::GetElementPtrInst>(lhs_write_value) ||
                                   llvm::isa<llvm::GlobalVariable   >(lhs_write_value);
        if(is_atomic_assign && is_global_or_member)
        {
            mBuilder.CreateCall3(mAtomic.getWriteFunction(node.right), lhs_write_value, llvm_binary_op_result, llvm_load_tx);
        }
        else
        {
            mBuilder.CreateStore(llvm_binary_op_result, lhs_write_value);
        }

        result = lhs_write_value;
    }

    if(!result)
    {
        BOOST_ASSERT(false && "invalid LLVM interpretation");
        terminateRevisit();
    }

    SET_SYNTHESIZED_LLVM_VALUE(&node, result);
}

void LLVMGeneratorStageVisitor::generate(TernaryExpr& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    llvm::BasicBlock* cond_block = createBasicBlock("ternary.eval", mFunctionContext.function);
    llvm::BasicBlock* true_block = createBasicBlock("ternary.true", mFunctionContext.function);
    llvm::BasicBlock* false_block = createBasicBlock("ternary.false", mFunctionContext.function);
    llvm::BasicBlock* final_block = createBasicBlock("ternary.finalized", mFunctionContext.function);

    // Remember the last active block so that we could correctly insert the phi node with correct coming block
    llvm::BasicBlock* last_active_true_block = nullptr;
    llvm::BasicBlock* last_active_false_block = nullptr;

    using getter_type               = llvm::Value* (LLVMGeneratorStageVisitor:: *)(ASTNode& , ASTNode&);
    const auto&        is_rvalue    = node.isRValue();
    const getter_type& value_getter = is_rvalue ? &LLVMGeneratorStageVisitor::getReadValue : &LLVMGeneratorStageVisitor::getWriteValue;

    // jump from current block to the condition evaluation block
    enterBasicBlock(cond_block);
    {
        visit(*node.cond);

        // conditional branch to either action block or finalized block
        llvm::Value* read_value = getReadValue(node, *node.cond);

        llvm::BranchInst* inst = createCondBr(read_value, true_block, false_block);
        ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
    }

    // try entering true block and get the value
    enterBasicBlock(true_block);
    llvm::Value* true_value = nullptr;
    if(node.true_node)
    {
        visit(*node.true_node);

        true_value = (this->*value_getter)(node, *node.true_node);

        last_active_true_block = mBuilder.GetInsertBlock();
        llvm::BranchInst* inst = mBuilder.CreateBr(final_block);
        ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
    }

    // try entering true block and get the value
    enterBasicBlock(false_block);
    llvm::Value* false_value = nullptr;
    if(node.false_node)
    {
        visit(*node.false_node);

        false_value = (this->*value_getter)(node, *node.false_node);

        last_active_false_block = mBuilder.GetInsertBlock();
        llvm::BranchInst* inst = mBuilder.CreateBr(final_block);
        ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
    }

    // try enter finalized block and create branch from true block
    enterBasicBlock(final_block);
    {
        llvm::PHINode* phi = mBuilder.CreatePHI(true_value->getType(), 2);
        phi->addIncoming(true_value, last_active_true_block);
        phi->addIncoming(false_value, last_active_false_block);

        SET_SYNTHESIZED_LLVM_VALUE(&node, phi);
        SET_SYNTHESIZED_LLVM_BLOCK(&node, final_block);
    }
}

void LLVMGeneratorStageVisitor::generate(CallExpr& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    revisit(node);

    FunctionDecl* function_decl = cast<FunctionDecl>(ASTNodeHelper::getCanonicalSymbol(node.node));
    BOOST_ASSERT(function_decl && "calling non-invokable value");

    // Fetch this pointer if it is member expression
    SynthesizedVirtualFunctionContext* vfunc_context = GET_SYNTHESIZED_VIRTUAL_FUNCTION(node.node);

    std::vector<llvm::Value*> arguments;
    if (!function_decl->is_static && vfunc_context && vfunc_context->llvm_this_pointer)
        arguments.push_back(vfunc_context->llvm_this_pointer);

    for(auto param : node.parameters)
    {
        llvm::Value* read_value = getReadValue(node, *param);
        arguments.push_back(read_value);
    }

    // virtual function handling
    llvm::Value* llvm_function = nullptr;
    if (function_decl->is_virtual && !isSuperMember(*node.node))
    {
        llvm_function = vfunc_context->llvm_virtual_function;
    }
    else
    {
        llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(function_decl);
    }
    BOOST_ASSERT(llvm_function && "invalid LLVM function object");

    llvm::ArrayRef<llvm::Value*> llvm_arguments(arguments);
    llvm::Value* result = mBuilder.CreateCall(llvm_function, llvm_arguments);

    SET_SYNTHESIZED_LLVM_VALUE(&node, result);
    // TODO depending on the LHS, if it's a directly-invokable function, just get the function prototype and invoke
    // TODO if it's not a directly-invokable function, which can be a class member function, pass this pointer to that function and make the call
    // TODO if it's a class member function and it's virtual, we have different calling convention here
}

void LLVMGeneratorStageVisitor::generate(CastExpr& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    revisit(node);

    llvm::Value* llvm_result = nullptr;
    llvm::Value* read_value = getReadValue(node, *node.node);

    Type* fromType = node.node->getCanonicalType();
    Type* toType   = node.getCanonicalType();

    if( (fromType->isPrimitiveType() || fromType->isEnumType()) &&
        (toType  ->isPrimitiveType() || toType  ->isEnumType()))
    {
        PrimitiveSpecifier* fromPrimitive = nullptr;
        PrimitiveSpecifier* toPrimitive = nullptr;

        if(fromType->isEnumType())
        {
            EnumDeclManglingContext* ctx = EnumDeclManglingContext::get(fromType->getAsEnumDecl());
            fromPrimitive = getParserContext().tangle->internal->getPrimitiveTypeSpecifier((ctx->is_long_literal) ? PrimitiveKind::INT64_TYPE : PrimitiveKind::INT32_TYPE);
        }
        else
            fromPrimitive = getParserContext().tangle->internal->getPrimitiveTypeSpecifier(fromType->getAsPrimitiveType()->getKind());

        if(toType->isEnumType())
        {
            EnumDeclManglingContext* ctx = EnumDeclManglingContext::get(toType->getAsEnumDecl());
            toPrimitive = getParserContext().tangle->internal->getPrimitiveTypeSpecifier((ctx->is_long_literal) ? PrimitiveKind::INT64_TYPE : PrimitiveKind::INT32_TYPE);
        }
        else
            toPrimitive = getParserContext().tangle->internal->getPrimitiveTypeSpecifier(toType->getAsPrimitiveType()->getKind());

        const bool need_ext           = toPrimitive->getKind().byteSize() >  fromPrimitive->getKind().byteSize();
        const bool need_truc          = toPrimitive->getKind().byteSize() <  fromPrimitive->getKind().byteSize();
        const bool bitsize_mismatched = toPrimitive->getKind().byteSize() != fromPrimitive->getKind().byteSize();

        UNUSED_ARGUMENT(bitsize_mismatched);

        llvm::Type* llvm_cast_type = mHelper.getType(*toPrimitive);

        if (toPrimitive->getKind().isBoolType() && fromPrimitive->getKind().isIntegerType())
        {
            // casting from integer to bool
            llvm::Value* llvm_zero = llvm::Constant::getNullValue(read_value->getType());
            llvm::Value* llvm_i1bool = mBuilder.CreateICmp(llvm::CmpInst::Predicate::ICMP_NE, read_value, llvm_zero);
            llvm_result = mBuilder.CreateZExt(llvm_i1bool, llvm_cast_type);
        }
        else if (toPrimitive->getKind().isBoolType() && fromPrimitive->getKind().isFloatType())
        {
            // casting from float to bool
            llvm::Value* llvm_zero = llvm::Constant::getNullValue(read_value->getType());
            llvm::Value* llvm_i1bool = mBuilder.CreateFCmp(llvm::CmpInst::Predicate::FCMP_UNE, read_value, llvm_zero);
            llvm_result = mBuilder.CreateZExt(llvm_i1bool, llvm_cast_type);
        }
        else if(toPrimitive->getKind().isIntegerType() &&
                (fromPrimitive->getKind().isIntegerType() || fromPrimitive->getKind().isBoolType()))
        {
            // casting from integer to integer
            if(need_truc)
            {
                llvm_result = mBuilder.CreateTrunc(read_value, llvm_cast_type);
            }
            else if(need_ext)
            {
                llvm_result = mBuilder.CreateSExt(read_value, llvm_cast_type);
            }
            else
            {
                // the source the destination are all integer with same size, no-op
                llvm_result = read_value;
            }
        }
        else if(toPrimitive->getKind().isIntegerType() && fromPrimitive->getKind().isFloatType())
        {
            // casting from float to integer
            llvm_result = mBuilder.CreateFPToSI(read_value, llvm_cast_type);
        }
        else if(toPrimitive->getKind().isFloatType() &&
            (fromPrimitive->getKind().isIntegerType() || fromPrimitive->getKind().isBoolType()))
        {
            // casting from integer to float
            llvm_result = mBuilder.CreateSIToFP(read_value, llvm_cast_type);
        }
        else if(toPrimitive->getKind().isFloatType() && fromPrimitive->getKind().isFloatType())
        {
            // casting from float to float
            if(need_truc)
            {
                llvm_result = mBuilder.CreateFPTrunc(read_value, llvm_cast_type);
            }
            else if(need_ext)
            {
                llvm_result = mBuilder.CreateFPExt(read_value, llvm_cast_type);
            }
            else
            {
                // the source the destination are all float with same size, no-op
                llvm_result = read_value;
            }
        }
        else
        {
            // the rest of types can be both object types or function types or void type, no casting required
            if( (toPrimitive->getKind() == PrimitiveKind::VOID_TYPE && fromPrimitive->getKind() == PrimitiveKind::VOID_TYPE) )
            {
                llvm_result = read_value;
            }
            else
            {
                BOOST_ASSERT(false && "invalid primitive type conversion for LLVM generator stage");
            }
        }
    }
    else
    {
        Type* resolved = node.getCanonicalType();

        // cast target is a ClassDecl
        if (resolved->isRecordType())
        {
            // Handle class casted (down-casting or up-casting)
            ClassDecl* target_cast_class = resolved->getAsClassDecl();
            Type* resolved_source = node.node->getCanonicalType();

            if (llvm::ConstantPointerNull::classof(read_value))
            {
                // special handling null case, if the source is a null, we don't need to adjust the pointer
                llvm::StructType* target_struct_type = mHelper.getStructType(*target_cast_class);
                llvm_result = mBuilder.CreateBitCast(read_value, target_struct_type->getPointerTo());
            }
            else if (ClassDecl* fromSourceClass = resolved_source->getAsClassDecl())
            {
                // cast source is ClassDecl
                llvm::StructType* target_struct_type = mHelper.getStructType(*target_cast_class);

                BOOST_ASSERT(target_struct_type && "no target structure type!");

                llvm::BasicBlock* adjust_block = createBasicBlock("cast.adjust", mFunctionContext.function);
                llvm::BasicBlock* null_block = createBasicBlock("cast.null", mFunctionContext.function);
                llvm::BasicBlock* final_block = createBasicBlock("cast.final", mFunctionContext.function);

                llvm::Value* is_null = mBuilder.CreateIsNull(read_value);
                createCondBr(is_null, null_block, adjust_block);

                llvm::Value* null_result = nullptr;
                llvm::Value* adjusted_result = nullptr;

                enterBasicBlock(null_block);
                {
                    null_result = llvm::ConstantPointerNull::get(target_struct_type->getPointerTo());

                    mBuilder.CreateBr(final_block);
                }

                enterBasicBlock(adjust_block);
                {
                    BOOST_ASSERT((node.method == CastExpr::CastMethod::STATIC || node.method == CastExpr::CastMethod::DYNAMIC) && "invalid cast method");

                    if(node.method == CastExpr::CastMethod::STATIC)
                    {
                        adjusted_result = mHelper.adjustPointer(read_value, fromSourceClass, target_cast_class);
                    }
                    else
                    {
                        ClassDecl* object_class = node.getOwner<Tangle>()->findThorLangObject();

                        BOOST_ASSERT(object_class && "no thor.lang.Object");

                        llvm::StructType* object_struct_type = mHelper.getStructType(*object_class);

                        BOOST_ASSERT(object_struct_type && "no object structure type!");

                        llvm::PointerType* pi8 = llvm::Type::getInt8PtrTy(mContext);
                        llvm::PointerType* pi64 = llvm::Type::getInt64PtrTy(mContext);
                        llvm::PointerType* ppi64 = llvm::PointerType::getUnqual(pi64);

                        llvm::Value* vptr = mBuilder.CreateBitCast(read_value, ppi64, "vptr");
                        llvm::Value* vtable = mBuilder.CreateLoad(vptr, "vtable");
                        llvm::Value* offset = mBuilder.CreateLoad(mBuilder.CreateConstGEP1_64(vtable, -2), "offset");

                        llvm::Value* object = mBuilder.CreateGEP(mBuilder.CreateBitCast(vptr, pi8), offset, "obj");
                        llvm::Value* target_type_id = mHelper.get_type_id(*target_cast_class);
                        llvm::Function* dyn_cast_impl = generateDynCastImpl();

                        llvm::Value* target = mBuilder.CreateCall2(dyn_cast_impl, object, target_type_id);

                        adjusted_result = mBuilder.CreateBitCast(target, target_struct_type->getPointerTo());
                    }

                    mBuilder.CreateBr(final_block);
                }

                enterBasicBlock(final_block);
                {
                    llvm::PHINode* phi = mBuilder.CreatePHI(target_struct_type->getPointerTo(), 2);

                    phi->addIncoming(null_result, null_block);
                    phi->addIncoming(adjusted_result, adjust_block);

                    llvm_result = phi;
                }
            }
            else
            {
                BOOST_ASSERT(false && "Unknown casting");
            }
        }
        else
        {
            // for function type or unspecified type, they should be all compatible anyway, so no conversion is necessary
            // TODO confirm this
            llvm_result = read_value;
        }
    }

    if(llvm_result)
    {
        SET_SYNTHESIZED_LLVM_VALUE(&node, llvm_result);
    }
    else
    {
        BOOST_ASSERT(false && "invalid AST for LLVM generator stage");
    }
}

void LLVMGeneratorStageVisitor::generate(MemberExpr& node)
{
    if(hasValue(node)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    revisit(node);
    // TODO otherwise, the node is resolved to a local variable, if the RHS is a member variable, use object resolver to resolve correct offset
    // TODO if RHS is a member function, use object resolver to resolve correct function pointer (if it's a non-virtual function)
    // TODO if it's virtual function, resolve to correct table index using VTable
    Type* resolved_type = node.node->getCanonicalType();
    if(!resolved_type) return;

    if(ClassDecl* class_decl = resolved_type->getAsClassDecl())
    {
        SynthesizedObjectLayoutContext* object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(class_decl);

        // Get *this* pointer
        llvm::Value* this_value = getReadValue(node, *node.node);

        ASTNode* resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&node);
        if (FunctionDecl* ast_function = cast<FunctionDecl>(resolved_symbol))
        {
            llvm::Value* virtual_function = nullptr;

            // If the function is static, there is no *this* pointer to consider.
            if (ast_function->is_static)
                return;

            // According to the resolved ast_function, we need to adjust the this pointer
            ClassDecl* target_class = ast_function->getOwner<ClassDecl>();
            this_value = mHelper.adjustPointer(this_value, class_decl, target_class);

            if (ast_function->is_virtual)
            {
                virtual_function = getVirtualFunction(*target_class, *ast_function, this_value);
            }

            // TODO: Well, kind of strange. For member function, we need *this*, but the member function is not always virtual.
            // However, we still store *this* into virtual function context. Need to create another context for *this*?? Or rename virtual function context?
            SET_SYNTHESIZED_VIRTUAL_FUNCTION(&node, this_value, virtual_function);
        }
        else if (VariableDecl* ast_variable = cast<VariableDecl>(resolved_symbol))
        {
            // if the variable is static, variable is not on the object instance
            if(ast_variable->is_static)
                return;

            // Retrieve the object layout from the class declaration
            BOOST_ASSERT(object_layout->member_attributes.count(ast_variable) != 0);
            auto index = object_layout->member_attributes[ast_variable].get<0>();
            llvm::Value* member_ptr = mBuilder.CreateStructGEP(this_value, index);

            SET_SYNTHESIZED_LLVM_VALUE(&node, member_ptr);
        }
    }
    else if(EnumDecl* enum_decl = resolved_type->getAsEnumDecl())
    {
        // get the numeric literal for the enumeration constant
        if(isa<IdExpr>(node.node))
        {
            EnumIdManglingContext* enum_id_ctx = EnumIdManglingContext::get(node.node);
            if(enum_id_ctx)
            {
                llvm::Value* enum_literal = nullptr;

                if(EnumDeclManglingContext::get(enum_decl)->is_long_literal)
                    enum_literal = llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(mContext), (int64)enum_id_ctx->value, false);
                else
                    enum_literal = llvm::ConstantInt::get(llvm::IntegerType::getInt32Ty(mContext), (int32)enum_id_ctx->value, false);

                SET_SYNTHESIZED_LLVM_VALUE(&node, enum_literal);
            }
        }
    }
    else
    {
        UNIMPLEMENTED_CODE();
    }

    // TODO: Handle static member variable
}

void LLVMGeneratorStageVisitor::generate(BlockExpr& node)
{
    revisit(node);

    if(node.block->objects.size() > 0)
    {
        ExpressionStmt* last_stmt = cast<ExpressionStmt>(*node.block->objects.rbegin());
        propagate(&node, last_stmt);
    }
}

void LLVMGeneratorStageVisitor::generate(SystemCallExpr& node)
{
    revisit(node);

    using boost::adaptors::indirected;

    llvm::Value* value_system_call = nullptr;

    // TODO implement expanding for corresponding system call
    switch (node.call_type)
    {
    case SystemCallExpr::CallType::ASYNC:
    {
        BOOST_ASSERT(node.parameters.size() >= 2 && "at least function id and result pointer should be in system call's parameter");

        auto*const ret_ptr_expr = node.parameters[0];
        auto*const func_id_expr = node.parameters[1];

        NOT_NULL(func_id_expr);

        if (node.parameters.size() > 2)
            value_system_call = generateAsyncCallParams(node, ret_ptr_expr, *func_id_expr, boost::make_iterator_range(node.parameters, 2, 0) | indirected);
        else
            value_system_call = generateAsyncCallNoParams(node, ret_ptr_expr, *func_id_expr);

        break;
    }
    default:
        UNIMPLEMENTED_CODE();
        break;
    }

    if (value_system_call != nullptr)
        SET_SYNTHESIZED_LLVM_VALUE(&node, value_system_call);
}

llvm::Function* LLVMGeneratorStageVisitor::getRootsetAdder()
{
    auto*const llvm_type_void = mHelper.getType(PrimitiveKind::VOID_TYPE);
    auto*const llvm_type_int8 = mHelper.getType(PrimitiveKind::INT8_TYPE);

    auto*const llvm_type_adder = llvm::FunctionType::get(llvm_type_void, {llvm_type_int8->getPointerTo()}, false);
    auto*const llvm_decl_adder = mModule.getOrInsertFunction(global_constant::ROOTSET_ADDER, llvm_type_adder);

    return llvm::cast<llvm::Function>(llvm_decl_adder);
}

llvm::Function* LLVMGeneratorStageVisitor::getAsyncInvocationAdder()
{
    auto*const llvm_type_i8  = mHelper.getType(PrimitiveKind::INT8_TYPE);
    auto*const llvm_type_i64 = mHelper.getType(PrimitiveKind::INT64_TYPE);

    llvm::Type* arg_types[] = {llvm_type_i64, llvm_type_i8->getPointerTo()};

    auto*const llvm_type_adder = llvm::FunctionType::get(llvm_type_i64, arg_types, false);
    auto*const llvm_decl_adder = mModule.getOrInsertFunction(global_constant::INVOCATION_ADDER, llvm_type_adder);

    return llvm::cast<llvm::Function>(llvm_decl_adder);
}

llvm::Function* LLVMGeneratorStageVisitor::getAsyncInvocationReserver()
{
    auto*const llvm_type_i64  = mHelper.getType(PrimitiveKind::INT64_TYPE);

    llvm::Type* arg_types[] = {llvm_type_i64};

    auto*const llvm_type_reserver = llvm::FunctionType::get(llvm_type_i64, arg_types, false);
    auto*const llvm_decl_reserver = mModule.getOrInsertFunction(global_constant::INVOCATION_RESERVER, llvm_type_reserver);

    return llvm::cast<llvm::Function>(llvm_decl_reserver);
}

llvm::Function* LLVMGeneratorStageVisitor::getAsyncInvocationCommiter()
{
    auto*const llvm_type_bool = mHelper.getType(PrimitiveKind:: BOOL_TYPE);
    auto*const llvm_type_i64  = mHelper.getType(PrimitiveKind::INT64_TYPE);

    llvm::Type* arg_types[] = {llvm_type_i64};

    auto*const llvm_type_commiter = llvm::FunctionType::get(llvm_type_bool, arg_types, false);
    auto*const llvm_decl_commiter = mModule.getOrInsertFunction(global_constant::INVOCATION_COMMITER, llvm_type_commiter);

    return llvm::cast<llvm::Function>(llvm_decl_commiter);
}

llvm::Function* LLVMGeneratorStageVisitor::getAsyncInvocationAborter()
{
    auto*const llvm_type_bool = mHelper.getType(PrimitiveKind::BOOL_TYPE);
    auto*const llvm_type_i64  = mHelper.getType(PrimitiveKind::INT64_TYPE);

    llvm::Type* arg_types[] = {llvm_type_i64};

    auto*const llvm_type_aborter = llvm::FunctionType::get(llvm_type_bool, arg_types, false);
    auto*const llvm_decl_aborter = mModule.getOrInsertFunction(global_constant::INVOCATION_ABORTER, llvm_type_aborter);

    return llvm::cast<llvm::Function>(llvm_decl_aborter);
}

llvm::Function* LLVMGeneratorStageVisitor::getAsyncInvocationAppender(const Type& type)
{
    const char* appender_name   = nullptr;
    llvm::Type* llvm_param_type = nullptr;

    if (type.isRecordType())
    {
        appender_name   = global_constant::INVOCATION_APPENDER_INT8P;
        llvm_param_type = llvm::Type::getInt8PtrTy(mContext);
    }
    else
    {
        PrimitiveKind kind = PrimitiveKind::VOID_TYPE;

        if (const auto* type_primitive = type.getAsPrimitiveType())
            kind = type_primitive->getKind();
        else if (const auto*const decl_enum = type.getAsEnumDecl())
            kind = decl_enum->getUnderlyingType();
        else
            UNREACHABLE_CODE();

        switch (kind)
        {
        case PrimitiveKind::BOOL_TYPE   : appender_name = global_constant::INVOCATION_APPENDER_BOOL ; break;
        case PrimitiveKind::INT8_TYPE   : appender_name = global_constant::INVOCATION_APPENDER_INT8 ; break;
        case PrimitiveKind::INT16_TYPE  : appender_name = global_constant::INVOCATION_APPENDER_INT16; break;
        case PrimitiveKind::INT32_TYPE  : appender_name = global_constant::INVOCATION_APPENDER_INT32; break;
        case PrimitiveKind::INT64_TYPE  : appender_name = global_constant::INVOCATION_APPENDER_INT64; break;
        case PrimitiveKind::FLOAT32_TYPE: appender_name = global_constant::INVOCATION_APPENDER_FLT32; break;
        case PrimitiveKind::FLOAT64_TYPE: appender_name = global_constant::INVOCATION_APPENDER_FLT64; break;
        default                         : UNREACHABLE_CODE();                                       ; return nullptr;
        }

        llvm_param_type = mHelper.getType(kind);
    }

    auto*const  llvm_type_bool       = mHelper.getType(PrimitiveKind:: BOOL_TYPE);
    auto*const  llvm_type_i64        = mHelper.getType(PrimitiveKind::INT64_TYPE);
    llvm::Type* appender_arg_types[] = {llvm_type_i64, llvm_type_i64, llvm_param_type};

    auto*const llvm_type_appender = llvm::FunctionType::get(llvm_type_i64, appender_arg_types, false);
    auto*const llvm_decl_appender = mModule.getOrInsertFunction(appender_name, llvm_type_appender);

    return llvm::cast<llvm::Function>(llvm_decl_appender);
}

llvm::Function* LLVMGeneratorStageVisitor::getAsyncInvocationRetPtrSetter()
{
    auto*const llvm_type_bool = mHelper.getType(PrimitiveKind::BOOL_TYPE);
    auto*const llvm_type_i8   = mHelper.getType(PrimitiveKind::INT8_TYPE);
    auto*const llvm_type_i64  = mHelper.getType(PrimitiveKind::INT64_TYPE);

    llvm::Type* ret_ptr_setter_arg_types[] = {llvm_type_i64, llvm_type_i8->getPointerTo()};

    auto*const llvm_type_ret_ptr_setter = llvm::FunctionType::get(llvm_type_bool, ret_ptr_setter_arg_types, false);
    auto*const llvm_decl_ret_ptr_setter = mModule.getOrInsertFunction(global_constant::INVOCATION_RET_PTR_SETTER, llvm_type_ret_ptr_setter);

    return llvm::cast<llvm::Function>(llvm_decl_ret_ptr_setter);
}

llvm::Value* LLVMGeneratorStageVisitor::generateAsyncCallParamThis(const FunctionDecl& callee, Expression& callee_this_expr)
{
    BOOST_ASSERT(callee.is_member && !callee.is_static && "callee is not a non-static member function");
    BOOST_ASSERT(callee.hasOwner<ClassDecl>() && "member function not under class declaration");

    auto*const callee_owner_decl      = callee.getOwner<ClassDecl>();
    auto*const callee_owner_llvm_type = mHelper.getStructType(*callee_owner_decl, true);

    const auto*const this_expr_type = callee_this_expr.getCanonicalType();

    BOOST_ASSERT(this_expr_type != nullptr && "no canonical type");
    BOOST_ASSERT(this_expr_type->isRecordType() && "expression provides returns non-object!");

          auto*const this_expr_decl      = this_expr_type->getAsClassDecl();
    const auto*const this_expr_llvm_type = mHelper.getStructType(*this_expr_decl);

    BOOST_ASSERT(callee_owner_llvm_type != nullptr && "no struct type for specific class declaration");
    BOOST_ASSERT(   this_expr_llvm_type != nullptr && "no struct type for specific class declaration");

    auto*const            this_value = getReadValue(callee_this_expr, callee_this_expr);
    llvm::Value* adjusted_this_value = nullptr;

    if (callee_this_expr.isThisLiteral() || callee_this_expr.isNullLiteral())
    {
        adjusted_this_value = mHelper.adjustPointer(this_value, this_expr_decl, callee_owner_decl);
    }
    else
    {
        auto*const  null_block = createBasicBlock("cast.null" , mFunctionContext.function);
        auto*const  cast_block = createBasicBlock("cast.cast" , mFunctionContext.function);
        auto*const final_block = createBasicBlock("cast.final", mFunctionContext.function);

        auto*const is_null_cond_value = mBuilder.CreateIsNull(this_value);

        mBuilder.CreateCondBr(is_null_cond_value, null_block, cast_block);

        enterBasicBlock(null_block, false);
        auto*const   null_this_value = llvm::Constant::getNullValue(callee_owner_llvm_type->getPointerTo());
        mBuilder.CreateBr(final_block);

        enterBasicBlock(cast_block, false);
        auto*const casted_this_value = mHelper.adjustPointer(this_value, this_expr_decl, callee_owner_decl);
        mBuilder.CreateBr(final_block);

        enterBasicBlock(final_block);
        auto*const phi_this_value = mBuilder.CreatePHI(callee_owner_llvm_type->getPointerTo(), 2);

        phi_this_value->addIncoming(  null_this_value, null_block);
        phi_this_value->addIncoming(casted_this_value, cast_block);

        adjusted_this_value = phi_this_value;
    }

    return mBuilder.CreateBitCast(adjusted_this_value, llvm::Type::getInt8PtrTy(mContext));
}

llvm::Value* LLVMGeneratorStageVisitor::generateAsyncCallParamNonThis(Expression& param_expr)
{
    auto* param_value = getReadValue(param_expr, param_expr);

    if (param_value->getType()->isPointerTy())
        param_value = mBuilder.CreateBitCast(param_value, llvm::Type::getInt8PtrTy(mContext));

    return param_value;
}

llvm::Value* LLVMGeneratorStageVisitor::generateAsyncCallReturnPtrSet(llvm::Value* inv_id, Expression* assignee_expr)
{
    if (assignee_expr == nullptr)
        return llvm::ConstantInt::get(mHelper.getType(PrimitiveKind::BOOL_TYPE), 1);

    auto*const assignee_value = getWriteValue(*assignee_expr, *assignee_expr);
    BOOST_ASSERT(assignee_value != nullptr && "invalid assignee!? please verify S1 has refused invalid assignee");

    auto*const return_ptr_setter = getAsyncInvocationRetPtrSetter();
    auto*const return_ptr        = mBuilder.CreateBitCast(assignee_value, llvm::Type::getInt8PtrTy(mContext));

    auto*const call = mBuilder.CreateCall2(return_ptr_setter, inv_id, return_ptr);

    return call;
}

template<typename RangeType>
llvm::Value* LLVMGeneratorStageVisitor::generateAsyncCallParams(SystemCallExpr& system_call, Expression* assignee_expr, Expression& func_id_expr, const RangeType& parameters)
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;
    using boost::make_iterator_range;

    const auto*const decl_callee = cast_or_null<FunctionDecl>(ASTNodeHelper::getCanonicalSymbol(&func_id_expr));

    BOOST_ASSERT(decl_callee != nullptr && "expression for function id does not resolved to callee");

    auto*const value_func_id = getReadValue(func_id_expr, func_id_expr);
    NOT_NULL(value_func_id);

    std::vector<llvm::Value*> parameter_values;

    if (decl_callee->is_member && !decl_callee->is_static)
    {
        // 'this' need to manually adjusted for member function calls
        parameter_values.emplace_back(generateAsyncCallParamThis(*decl_callee, parameters.front()));

        boost::push_back(
            parameter_values,
              make_iterator_range(parameters, 1, 0)
            | transformed(std::bind(&LLVMGeneratorStageVisitor::generateAsyncCallParamNonThis, this, std::placeholders::_1))
        );
    }
    else
    {
        boost::push_back(
            parameter_values,
              parameters
            | transformed(std::bind(&LLVMGeneratorStageVisitor::generateAsyncCallParamNonThis, this, std::placeholders::_1))
        );
    }

    auto*const block_begin    = createBasicBlock("async.begin"     , mFunctionContext.function);
    auto*      block_append   = createBasicBlock("async.append.beg", mFunctionContext.function);
    auto*const block_commit   = createBasicBlock("async.commit"    , mFunctionContext.function);
    auto*const block_ret_ptr  = createBasicBlock("async.ret_ptr"   , mFunctionContext.function);
    auto*const block_rootset  = createBasicBlock("async.rootset"   , mFunctionContext.function);
    auto*const block_abort    = createBasicBlock("async.abort"     , mFunctionContext.function);
    auto*const block_finalize = createBasicBlock("async.finalize"  , mFunctionContext.function);

    enterBasicBlock(block_begin);

    auto*const decl_reserver     = getAsyncInvocationReserver();
    auto*const value_inv_id      = mBuilder.CreateCall(decl_reserver, value_func_id);
    auto*const value_is_reserved = mBuilder.CreateICmpSGE(value_inv_id, llvm::ConstantInt::getSigned(value_inv_id->getType(), 0));

    mBuilder.CreateCondBr(value_is_reserved, block_append, block_finalize);

    enterBasicBlock(block_append, false);
    {
        llvm::Value* value_offset = llvm::ConstantInt::getSigned(llvm::Type::getInt64Ty(mContext), 0);

        for (const auto& pair : make_zip_range(parameters, parameter_values))
        {
            Expression&       parameter = boost::get<0>(pair);
            llvm::Value*const value     = boost::get<1>(pair);

            auto*const parameter_type  = parameter.getCanonicalType();
            auto*const decl_appender   = getAsyncInvocationAppender(*parameter_type);

            auto*const value_new_offset  = mBuilder.CreateCall3(decl_appender, value_inv_id, value_offset, value);
            auto*const value_is_appended = mBuilder.CreateICmpSGE(value_new_offset, llvm::ConstantInt::getSigned(value_new_offset->getType(), 0));
            auto*const block_next        = createBasicBlock("async.append.next", mFunctionContext.function);

            mBuilder.CreateCondBr(value_is_appended, block_next, block_abort);

            block_append = block_next;
            value_offset = value_new_offset;

            enterBasicBlock(block_append, false);
        }
    }
    mBuilder.CreateBr(block_ret_ptr);

    enterBasicBlock(block_ret_ptr);
    {
        auto*const is_success_value    = generateAsyncCallReturnPtrSet(value_inv_id, assignee_expr);
        auto*const is_success_value_i1 = mBuilder.CreateTrunc(is_success_value, llvm::Type::getInt1Ty(mContext));

        mBuilder.CreateCondBr(is_success_value_i1, block_commit, block_abort);
    }

    enterBasicBlock(block_commit);
    {
        auto*const decl_commiter       = getAsyncInvocationCommiter();
        auto*const is_success_value    = mBuilder.CreateCall(decl_commiter, value_inv_id);
        auto*const is_success_value_i1 = mBuilder.CreateTrunc(is_success_value, llvm::Type::getInt1Ty(mContext));

        mBuilder.CreateCondBr(is_success_value_i1, block_rootset, block_abort);
    }

    enterBasicBlock(block_rootset);
    {
        auto*const decl_adder = getRootsetAdder();

        for (auto*const parameter_value : parameter_values)
        {
            auto*const parameter_type = parameter_value->getType();

            if (!parameter_type->isPointerTy())
                continue;

            mBuilder.CreateCall(decl_adder, parameter_value);
        }
    }
    mBuilder.CreateBr(block_finalize);

    enterBasicBlock(block_abort);
    {
        auto*const llvm_type_i64 = llvm::Type::getInt64Ty(mContext);
        auto*const decl_aborter  = getAsyncInvocationAborter();

        mBuilder.CreateCall(decl_aborter, value_inv_id);
    }
    mBuilder.CreateBr(block_finalize);

    enterBasicBlock(block_finalize);

    auto*const llvm_type_id = value_inv_id->getType();
    auto*const result       = mBuilder.CreatePHI(llvm_type_id, 2, "async_id");

    result->addIncoming(llvm::ConstantInt::get(llvm_type_id, -1), block_begin  );
    result->addIncoming(value_inv_id                            , block_rootset);
    result->addIncoming(llvm::ConstantInt::get(llvm_type_id, -1), block_abort  );

    return result;
}

llvm::Value* LLVMGeneratorStageVisitor::generateAsyncCallNoParams(SystemCallExpr& system_call, Expression* assignee_expr, Expression& func_id_expr)
{
    auto*const   decl_adder         = getAsyncInvocationAdder();
    auto*const   llvm_type_i1       = llvm::Type::getInt1Ty(mContext);
    auto*const   llvm_type_i8_ptr   = llvm::Type::getInt8PtrTy(mContext);
    llvm::Value* value_assignee_ptr = nullptr;

    if (assignee_expr != nullptr)
    {
        auto*const assignee_value = getWriteValue(*assignee_expr, *assignee_expr);
        BOOST_ASSERT(assignee_value != nullptr && "invalid assignee!? please verify S1 has refused invalid assignee");

        value_assignee_ptr = mBuilder.CreateBitCast(assignee_value, llvm_type_i8_ptr);
    }
    else
    {
        value_assignee_ptr = llvm::ConstantPointerNull::get(llvm_type_i8_ptr);
    }

    auto*const value_func_id = getReadValue(func_id_expr, func_id_expr);
    auto*const call_expr     = mBuilder.CreateCall2(decl_adder, value_func_id, value_assignee_ptr);

    return call_expr;
}

bool LLVMGeneratorStageVisitor::isSuperMember(ASTNode& node)
{
    bool is_super = false;
    auto member_expr = cast<MemberExpr>(&node);
    if (member_expr)
    {
        ObjectLiteral* literal = cast<ObjectLiteral>(member_expr->node);
        is_super = (literal && literal->isSuperLiteral());
    }
    return is_super;
}

bool LLVMGeneratorStageVisitor::isStatic(ASTNode* resolved_symbol)
{
    BOOST_ASSERT(isa<FunctionDecl>(resolved_symbol) || isa<VariableDecl>(resolved_symbol) );
    if (isa<FunctionDecl>(resolved_symbol)) return cast<FunctionDecl>(resolved_symbol)->is_static;
    if (isa<VariableDecl>(resolved_symbol)) return cast<VariableDecl>(resolved_symbol)->is_static;
    UNREACHABLE_CODE(); return false;
}

void LLVMGeneratorStageVisitor::specialFunctionHandling(FunctionDecl& node)
{
    if (node.isConstructor())
    {
        // Before the constructor initialization, we need to call base constructor first
        // The following codes should be placed after allocateParameters(), since we need the this pointer alloca
        callBaseConstructor(node);
    }
    else if (node.is_global_init && node.arch.is_x86() /* TODO implement GC on CUDA target to remove this condition */)
    {
        if(node.isGlobal()) // for global variable initialization, add variables of non-primitive types into GC root set
        {
            Source* source = node.getOwner<Source>();
            for (auto* decl : source->declares)
            {
                if (!isa<VariableDecl>(decl)) continue;

                auto global_var = cast<VariableDecl>(decl);

                if(!node.arch.is_any(global_var->arch))
                    continue;

                Type* resolved_type = global_var->type->getCanonicalType();
                if (resolved_type && resolved_type->isRecordType())
                {
                    callAddToGlobalRootSet(GET_SYNTHESIZED_LLVM_VALUE(global_var));
                }
            }
        }
        else // for class static member initialization
        {
            BOOST_ASSERT(boost::algorithm::starts_with(node.name->toString(), L"__static_init"));

            ClassDecl* class_decl = node.getOwner<ClassDecl>();
            for (auto* var : class_decl->member_variables)
            {
                Type* resolved_type = var->type->getCanonicalType();
                if (resolved_type && resolved_type->isRecordType() && var->is_static && node.arch.is_any(var->arch))
                {
                    callAddToGlobalRootSet(GET_SYNTHESIZED_LLVM_VALUE(var));
                }
            }
        }
    }
}

void LLVMGeneratorStageVisitor::callAddToGlobalRootSet(llvm::Value* llvm_value)
{
    llvm::Function* llvm_function = generateAddToGlobalRootSet();
    llvm::Value* address_of_variable = mBuilder.CreateBitCast( llvm_value, llvm::Type::getInt8PtrTy(mContext), "address_of_var");
    llvm::ArrayRef<llvm::Value*> llvm_arguments(address_of_variable);
    mBuilder.CreateCall(llvm_function, llvm_arguments);
}

llvm::Function* LLVMGeneratorStageVisitor::generateAddToGlobalRootSet()
{
    llvm::FunctionType* llvm_function_type = llvm::FunctionType::get(llvm::Type::getVoidTy(mContext), llvm::ArrayRef<llvm::Type*>(llvm::Type::getInt8PtrTy(mContext)), false /*not variadic*/);
    // thor.lang.__addToGlobalRootSet(int8*)
    return llvm::cast<llvm::Function>(mModule.getOrInsertFunction("_ZN4thor4lang20__addToGlobalRootSetEPa", llvm_function_type));
}

llvm::Function* LLVMGeneratorStageVisitor::generateGuardAcquire()
{
    llvm::FunctionType* llvm_function_type = llvm::FunctionType::get(llvm::Type::getInt32Ty(mContext), llvm::ArrayRef<llvm::Type*>(llvm::Type::getInt64PtrTy(mContext)), false /*not variadic*/);
    return llvm::cast<llvm::Function>(mModule.getOrInsertFunction("__cxa_guard_acquire", llvm_function_type));
}

llvm::Function* LLVMGeneratorStageVisitor::generateGuardRelease()
{
    llvm::FunctionType* llvm_function_type = llvm::FunctionType::get(llvm::Type::getVoidTy(mContext), llvm::ArrayRef<llvm::Type*>(llvm::Type::getInt64PtrTy(mContext)), false /*not variadic*/);
    return llvm::cast<llvm::Function>(mModule.getOrInsertFunction("__cxa_guard_release", llvm_function_type));
}

llvm::Function* LLVMGeneratorStageVisitor::generateDynCastImpl()
{
    llvm::Type* arg_types[] = {
        llvm::Type::getInt8PtrTy(mContext),
        llvm::Type::getInt64Ty(mContext),
    };

    llvm::FunctionType* llvm_function_type = llvm::FunctionType::get(llvm::Type::getInt8PtrTy(mContext), llvm::ArrayRef<llvm::Type*>(arg_types), false /*not variadic*/);
    return llvm::cast<llvm::Function>(mModule.getOrInsertFunction(global_constant::DYN_CAST_IMPL, llvm_function_type));
}

llvm::Value* LLVMGeneratorStageVisitor::loadThisPointer(FunctionDecl& node)
{
    BOOST_ASSERT(node.is_member && "It's not member function");

    llvm::Value* llvm_alloca_this = GET_SYNTHESIZED_ALLOCA_THIS_POINTER(&node);
    llvm::Value* llvm_this_pointer = mBuilder.CreateLoad(llvm_alloca_this);

    BOOST_ASSERT(llvm_this_pointer && "Missing the first argument in member function, should be *this* pointer");

    return llvm_this_pointer;
}

void LLVMGeneratorStageVisitor::callDefaultFunction(FunctionDecl& from_function, ClassDecl& to_class, std::wstring function_name)
{
    // TODO: Given a proper name. Here means default constructor and destructor with the properties that there exists no parameters.
    ClassDecl* from_class = from_function.getOwner<ClassDecl>();
    llvm::Value* this_pointer = loadThisPointer(from_function);

    FunctionDecl* function = nullptr;
    for (auto* method : to_class.member_functions)
    {
        if (method->name->toString() == function_name && method->parameters.size() == 0)
        {
            function = method;
            break;
        }
    }
    BOOST_ASSERT(function && "No specific function found!");
    llvm::Function* llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(function);

    llvm::Value* adjust_this_pointer = mHelper.adjustPointer(this_pointer, from_class, &to_class);

    llvm::ArrayRef<llvm::Value*> llvm_arguments(&adjust_this_pointer, 1);
    mBuilder.CreateCall(llvm_function, llvm_arguments);
}

void LLVMGeneratorStageVisitor::callBaseDestructor(FunctionDecl& dtor_node)
{
    ClassDecl* class_decl = dtor_node.getOwner<ClassDecl>();
    BOOST_ASSERT(class_decl);

    if (!!class_decl->base && !class_decl->is_interface)
    {
        ClassDecl* base = class_decl->base->getCanonicalType()->getAsClassDecl();
        callDefaultFunction(dtor_node, *base, L"delete");
    }
}

void LLVMGeneratorStageVisitor::implementDeleteDestructor(FunctionDecl& dtor_node)
{
    // Call my complete destructor first
    ClassDecl* class_decl = dtor_node.getOwner<ClassDecl>();
    BOOST_ASSERT(class_decl);

    if (!class_decl->is_interface)
    {
        callDefaultFunction(dtor_node, *class_decl, L"delete");
    }

    // TODO: call delete operator later
}

void LLVMGeneratorStageVisitor::callBaseConstructor(FunctionDecl& ctor_node)
{
    // Examine the implementation of the current constructor, if there exists super(), we don't need to generate it.
    bool manual_call_base = false;

    ASTNodeHelper::foreachApply<MemberExpr>(
        ctor_node,
        [&manual_call_base](MemberExpr& node) {
            if (!isa<ObjectLiteral>(node.node)) return;
            if (!isa<SimpleIdentifier>(node.member)) return;

            ObjectLiteral*    object_literal = cast<ObjectLiteral>(node.node);
            SimpleIdentifier* identifier     = cast<SimpleIdentifier>(node.member);

            if (object_literal->type != ObjectLiteral::LiteralType::SUPER_OBJECT) return;

            // Now, we make sure it is super call, so we only need to make sure it calls to constructor
            if (identifier->name == L"new")
                manual_call_base = true;
        }
    );

    if (manual_call_base) return;

    // Well, since we only have one extended class, just call its parent
    ClassDecl* class_decl = ctor_node.getOwner<ClassDecl>();
    BOOST_ASSERT(class_decl);

    if (!!class_decl->base && !class_decl->is_interface)
    {
        ClassDecl* base = class_decl->base->getCanonicalType()->getAsClassDecl();
        callDefaultFunction(ctor_node, *base, L"new");
    }
}

void LLVMGeneratorStageVisitor::initializeVTT(FunctionDecl& ctor_node)
{
    //std::cout << "[giggle] Initialize virtual function table pointers" << std::endl;
    ClassDecl* class_decl = ctor_node.getOwner<ClassDecl>();
    BOOST_ASSERT(class_decl);

    if (!class_decl->hasVirtual())
        return;

    // Initialize virtual function pointers
    SynthesizedVTableContext* vtable_context = GET_SYNTHESIZED_VTABLE(class_decl);
    SynthesizedObjectLayoutContext* object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(class_decl);

    llvm::Value* this_pointer = loadThisPointer(ctor_node);
    BOOST_ASSERT(this_pointer && "Missing the first argument in member function, should be *this* pointer");

    llvm::PointerType* int8PtrTy = llvm::Type::getInt8PtrTy(mContext);

    //std::cout << "[giggle] number of vptr = " << object_layout->vptr_index.size() << std::endl;
    for (uint32 i = 0; i < object_layout->vptr_index.size(); i++)
    {
        // Get the vptr address in object layout
        uint32 index = object_layout->vptr_index[i];
        llvm::Value* pointer_to_vptr = mBuilder.CreateStructGEP(this_pointer, index);
        llvm::Value* pointer_to_vptr_casted = mBuilder.CreateBitCast( pointer_to_vptr, int8PtrTy->getPointerTo()->getPointerTo(), "addr_of_vptr");

        // Get the address of the first function in virtual table
        uint32 subtable_index = vtable_context->subtable_start[i];
        llvm::Value* pointer_to_subtable_addr = mBuilder.CreateConstInBoundsGEP2_64(vtable_context->virtual_table, 0, subtable_index);

        // translate the address if necessary through intrinsic
        if(vtable_context->virtual_table->getType()->getAddressSpace() != pointer_to_vptr_casted->getType()->getPointerAddressSpace())
        {
            llvm::Value* raw_ptr = mBuilder.CreateBitCast(pointer_to_subtable_addr, llvm::Type::getInt8PtrTy(mContext, NVPTX_AS_CONST));
            llvm::Value* translated_raw_ptr = mBuilder.CreateCall(getAddrSpaceConversionIntrinsics(NVPTX_AS_CONST, NVPTX_AS_GENERIC), llvm::ArrayRef<llvm::Value*>(raw_ptr));
            pointer_to_subtable_addr = mBuilder.CreateBitCast(translated_raw_ptr, llvm::Type::getInt8PtrTy(mContext)->getPointerTo(NVPTX_AS_GENERIC));
        }

        // Save the address of subtable to the vptr
        mBuilder.CreateStore(pointer_to_subtable_addr, pointer_to_vptr_casted);
    }
}

llvm::Value* LLVMGeneratorStageVisitor::getVirtualFunction(ClassDecl& ast_class, FunctionDecl& ast_function, llvm::Value* this_value)
{
    BOOST_ASSERT(!ast_function.is_static);
    SynthesizedVTableContext* vtable_context = GET_SYNTHESIZED_VTABLE(&ast_class);

    // virtual function handling, get the vptr first
    llvm::FunctionType* llvm_function_type = mHelper.getFunctionType(ast_function);

    const auto index_result = ast_class.getVirtualIndex(ast_function);
    BOOST_ASSERT(index_result.second && "function is not recognized as virtual function!?");
    uint32 index = index_result.first;

    llvm::Value* vptr = mBuilder.CreateBitCast(this_value, llvm_function_type->getPointerTo()->getPointerTo()->getPointerTo());
    llvm::Value* vtable = mBuilder.CreateLoad(vptr, "vtable");

    llvm::Value* vfn = mBuilder.CreateConstInBoundsGEP1_64(vtable, index, "vfn");
    llvm::Value* virtual_function = mBuilder.CreateLoad(vfn);

    return virtual_function;
}

// this is for NVVM only, which has several built-in intrinsics to perform address space conversion
llvm::Function* LLVMGeneratorStageVisitor::getAddrSpaceConversionIntrinsics(unsigned from, unsigned to, int point_to_bitsize)
{
    if(from == to) return nullptr;

    auto address_space_to_name = [](unsigned as) -> const char* {
        switch(as) {
            case NVPTX_AS_GENERIC: return "gen";
            case NVPTX_AS_GLOBAL: return "global";
            case NVPTX_AS_CONST_NOT_GEN: return "constant";
            case NVPTX_AS_SHARED: return "shared";
            case NVPTX_AS_LOCAL: return "local";
            case NVPTX_AS_CONST: return "constant";
            default: UNIMPLEMENTED_CODE(); return nullptr;
        }
    };

    if(from != 0 && to != 0)
    {
        UNIMPLEMENTED_CODE();
        return nullptr;
    }

    char name[128]; sprintf(name, "llvm.nvvm.ptr.%s.to.%s.p%di%d.p%di%d", address_space_to_name(from), address_space_to_name(to), to, point_to_bitsize, from, point_to_bitsize);

    llvm::FunctionType* llvm_intrinsic_type = llvm::FunctionType::get(
            llvm::Type::getIntNPtrTy(mContext, point_to_bitsize, to),
            llvm::ArrayRef<llvm::Type*>(llvm::Type::getIntNPtrTy(mContext, point_to_bitsize, from)),
            false /*not variadic*/);

    return llvm::cast<llvm::Function>(mModule.getOrInsertFunction(name, llvm_intrinsic_type));
}

llvm::BasicBlock* LLVMGeneratorStageVisitor::createBasicBlock(llvm::StringRef name, llvm::Function* parent, llvm::BasicBlock* before)
{
    return llvm::BasicBlock::Create(mContext, name, parent, before);
}

void LLVMGeneratorStageVisitor::enterBasicBlock(llvm::BasicBlock* block, bool emit_branch_if_necessary)
{
    if(emit_branch_if_necessary && !mBuilder.GetInsertBlock()->getTerminator())
    {
        mBuilder.CreateBr(block);
    }

    mBuilder.SetInsertPoint(block);

    resetBlockInsertionMask();
}

llvm::AllocaInst* LLVMGeneratorStageVisitor::createAlloca(VariableDecl* node, llvm::Type* type, const llvm::Twine& name)
{
    llvm::AllocaInst* llvm_alloca_inst = nullptr;
    if(mBuilder.isNamePreserving())
        llvm_alloca_inst = new llvm::AllocaInst(type, 0, name, mFunctionContext.alloca_insert_point);
    else
        llvm_alloca_inst = new llvm::AllocaInst(type, 0, "", mFunctionContext.alloca_insert_point);

    return llvm_alloca_inst;
}

bool LLVMGeneratorStageVisitor::createAlloca(VariableDecl& ast_variable)
{
    if(GET_SYNTHESIZED_LLVM_VALUE(&ast_variable))
        return true;

    llvm::Type* llvm_variable_type = mHelper.getType(*ast_variable.type);

    if(llvm_variable_type == nullptr)
        return false;

    //      llvm_variable_type->dump();

    llvm::AllocaInst* llvm_alloca_inst = createAlloca(&ast_variable, llvm_variable_type, NameManglingContext::get(&ast_variable)->mangled_name);
    SET_SYNTHESIZED_LLVM_VALUE(&ast_variable, llvm_alloca_inst);

    return true;
}

bool LLVMGeneratorStageVisitor::startFunction(FunctionDecl& ast_function)
{
    // TODO simplify the blocks by removing or merging unnecessary blocks
    BOOST_ASSERT(!mFunctionContext.function);
    BOOST_ASSERT(!mFunctionContext.entry_block);
    BOOST_ASSERT(!mFunctionContext.return_block);
    BOOST_ASSERT(!mFunctionContext.alloca_insert_point);

    llvm::Function* llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(&ast_function);
    if(!llvm_function)
    {
        BOOST_ASSERT(false && "invalid LLVM function object");
        terminateRevisit();
        return false;
    }

    // create basic entry blocks
    mFunctionContext.entry_block = createBasicBlock("entry", llvm_function);
    mBuilder.SetInsertPoint(mFunctionContext.entry_block);

    // create dummy alloca insertion point (from clang & foster)
    {
        llvm::Value *undef = llvm::UndefValue::get(llvm::Type::getInt32Ty(mContext));
        mFunctionContext.alloca_insert_point = new llvm::BitCastInst(undef, llvm::Type::getInt32Ty(mContext), "", mFunctionContext.entry_block);
        if(mBuilder.isNamePreserving())
            mFunctionContext.alloca_insert_point->setName("allocapt");
    }

    // create return block
    mFunctionContext.return_block = createBasicBlock("return", llvm_function);
    SET_SYNTHESIZED_LLVM_BLOCK(ast_function.block, mFunctionContext.return_block);

    // allocate return value if necessary
    if (const auto*const multi_type = cast<MultiSpecifier>(ast_function.type))
    {
        // multiple return values
        mFunctionContext.return_values = {};
        uint32_t return_count = 0;
        for( auto type : multi_type->getTypes() )
        {
            llvm::Type* llvm_type = mHelper.getType(*type);
            if( !(llvm_type && !llvm_type->isVoidTy()) )
                break;
            auto && name = "retval" + std::to_string(return_count++);
            mFunctionContext.return_values.push_back( new llvm::AllocaInst(llvm_type, nullptr, name, mFunctionContext.alloca_insert_point) );
        }
    }
    else
    {
        // single return value
        llvm::Type* llvm_type = mHelper.getType(*ast_function.type);
        if(llvm_type && !llvm_type->isVoidTy())
        {
            mFunctionContext.return_values = { new llvm::AllocaInst(llvm_type, 0, "retval", mFunctionContext.alloca_insert_point) };
        }
        else
        {
            mFunctionContext.return_values = {};
        }
    }

    // set current function
    mFunctionContext.function = llvm_function;

    return true;
}

bool LLVMGeneratorStageVisitor::allocateParameters(FunctionDecl& ast_function)
{
    llvm::Function* llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(&ast_function);
    if(!llvm_function)
    {
        BOOST_ASSERT(false && "invalid LLVM function object");
        terminateRevisit();
        return false;
    }

    int index = 0;
    for(llvm::Function::arg_iterator it_arg = llvm_function->arg_begin(); it_arg != llvm_function->arg_end(); ++it_arg)
    {
        // We need to know if this one is member function, if so, we need to skip the first parameter (which is *this*)
        if (it_arg == llvm_function->arg_begin() && ast_function.is_member && !ast_function.is_static )
        {
            llvm::Value* llvm_this_pointer = llvm_function->arg_begin();

            llvm::AllocaInst* this_alloc = createAlloca(nullptr, llvm_this_pointer->getType(), "this_alloca");
            mBuilder.CreateStore(llvm_this_pointer, this_alloc);
            SET_SYNTHESIZED_ALLOCA_THIS_POINTER(&ast_function, this_alloc);
            continue;
        }

        //Identifier* parameter_identifier = ast_function.parameters[index]->name;

        llvm::Type* t = mHelper.getType(*ast_function.parameters[index]->type);
        if(t && !t->isVoidTy())
        {
            llvm::AllocaInst* parameter_alloc = createAlloca(ast_function.parameters[index], t, NameManglingContext::get(ast_function.parameters[index])->mangled_name + "_parameter_alloca");
            mBuilder.CreateStore(&(*it_arg), parameter_alloc);

            // store the alloca in the parameter identifier
            SET_SYNTHESIZED_LLVM_VALUE(ast_function.parameters[index], parameter_alloc);
            ++index;
        }
        else
        {
            BOOST_ASSERT(false && "writing to read-only LLVM type");
            return false;
        }
    }

    return true;
}

bool LLVMGeneratorStageVisitor::finishFunction(FunctionDecl& ast_function)
{
    UNUSED_ARGUMENT(ast_function);

    // Special handling for the following functions
    if (ast_function.isConstructor())
    {
        // After the constructor implementation, we need to initialize VTT
        initializeVTT(ast_function);
    }
    else if (ast_function.isDestructor())
    {
        // complete destructor
        // calling base complete destructor
        callBaseDestructor(ast_function);
    }
    else if (ast_function.name->toString() == L"__cxa_delete")
    {
        // delete destructor
        // calling my complete destructor and then calling delete operator
        implementDeleteDestructor(ast_function);
    }

    enterBasicBlock(mFunctionContext.return_block);

    // returns void
    if( mFunctionContext.return_values.empty() )
    {
        mBuilder.CreateRetVoid();
    }
    // at least one return value
    else
    {
        switch( mFunctionContext.return_values.size() )
        {
        // single return value
        case 1:
            BOOST_ASSERT(!mFunctionContext.return_values.front()->getType()->isVoidTy());
            mBuilder.CreateRet(mBuilder.CreateLoad(mFunctionContext.return_values.front()));
            break;
        // multiple return values
        default:
            auto && return_values = mFunctionContext.return_values;
            // load return values
            std::vector< llvm::Value* > loaded_values;
            for( auto && return_value : return_values )
            {
                BOOST_ASSERT( !return_value->getType()->isVoidTy() );
                loaded_values.push_back( mBuilder.CreateLoad(return_value) );
            }

            mBuilder.CreateAggregateRet( loaded_values.data(), loaded_values.size() );
            break;
        }
    }

    if(ast_function.is_global_init)
    {
        mGlobalCtors.push_back(this->mFunctionContext.function);
    }

    mFunctionContext.alloca_insert_point->eraseFromParent();
    mFunctionContext.alloca_insert_point = nullptr;

    // TODO simplify the block if there're few instructions by merging return and entry block

    // TODO add return instruction for void return function

    // TODO does this cause memory leak? don't know yet
    mFunctionContext.function = nullptr;
    mFunctionContext.entry_block = nullptr;
    mFunctionContext.return_block = nullptr;

    return true;
}

void LLVMGeneratorStageVisitor::generateLocalVariable(VariableDecl& var)
{
    // here we only generate AllocaInst for AST variables within function
    // those sit in global scope or class member scope will be stored in some other object and access through game object API
    // (all global variables are stored in a single game object, which is assembled by compiler)

    if(hasValue(var)) return;
    if(isBlockInsertionMasked() || isBlockTerminated(currentBlock()))   return;

    createAlloca(var);

    BOOST_ASSERT(!var.initializer && "the initializer should be null because we have restructure stage applied");
}

void LLVMGeneratorStageVisitor::shortCircuitEval(BinaryExpr& node)
{
    BOOST_ASSERT( node.opcode == BinaryExpr::OpCode::LOGICAL_AND || node.opcode == BinaryExpr::OpCode::LOGICAL_OR );

    llvm::BasicBlock* cond_block = createBasicBlock("binary.eval", mFunctionContext.function);
    llvm::BasicBlock* true_block = createBasicBlock("binary.true", mFunctionContext.function);
    llvm::BasicBlock* false_block = createBasicBlock("binary.false", mFunctionContext.function);
    llvm::BasicBlock* final_block = createBasicBlock("binary.finalized", mFunctionContext.function);

    // Remember the last active block so that we could correctly insert the phi node with correct coming block
    llvm::BasicBlock* last_active_true_block = nullptr;
    llvm::BasicBlock* last_active_false_block = nullptr;

    Expression* condition = node.left;
    Expression* run_if_evaluated_true = (node.opcode == BinaryExpr::OpCode::LOGICAL_AND) ? node.right : condition;
    Expression* run_if_evaluated_false  = (node.opcode == BinaryExpr::OpCode::LOGICAL_AND) ? condition : node.right;

    // jump from current block to the condition evaluation block
    enterBasicBlock(cond_block);
    {
        visit(*condition);

        // conditional branch to either action block or finalized block
        llvm::Value* cond_value = getReadValue(node, *condition);
        llvm::BranchInst* inst = createCondBr(cond_value, true_block, false_block);
        ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
    }

    // try entering true block and get the value
    enterBasicBlock(true_block);
    llvm::Value* true_value = nullptr;
    if( run_if_evaluated_true )
    {
        if( condition != run_if_evaluated_true )
            visit(*run_if_evaluated_true);

        true_value = getReadValue(node, *run_if_evaluated_true);

        last_active_true_block = mBuilder.GetInsertBlock();
        llvm::BranchInst* inst = mBuilder.CreateBr(final_block);
        ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
    }

    // try entering true block and get the value
    enterBasicBlock(false_block);
    llvm::Value* false_value = nullptr;
    if( run_if_evaluated_false )
    {
        if( condition != run_if_evaluated_false )
            visit(*run_if_evaluated_false);

        false_value = getReadValue(node, *run_if_evaluated_false);

        last_active_false_block = mBuilder.GetInsertBlock();
        llvm::BranchInst* inst = mBuilder.CreateBr(final_block);
        ADD_INTERMEDIATE_LLVM_VALUE(&node, inst);
    }

    // try enter finalized block and create branch from true block
    enterBasicBlock(final_block);
    {
        llvm::PHINode* phi = mBuilder.CreatePHI(true_value->getType(), 2);
        phi->addIncoming(true_value, last_active_true_block);
        phi->addIncoming(false_value, last_active_false_block);

        SET_SYNTHESIZED_LLVM_VALUE(&node, phi);
        SET_SYNTHESIZED_LLVM_BLOCK(&node, final_block);
    }
}

llvm::BranchInst* LLVMGeneratorStageVisitor::createCondBr(llvm::Value* value, llvm::BasicBlock* true_block, llvm::BasicBlock* false_block)
{
    // firstly, we need to truncate the value to i1 if needed
    llvm::Value* trunc_value = mBuilder.CreateTrunc(value, llvm::Type::getInt1Ty(mContext));

    return mBuilder.CreateCondBr(trunc_value, true_block, false_block);
}

llvm::Value* LLVMGeneratorStageVisitor::getAtomicReadValue(ASTNode& node, llvm::Value* read_value)
{
    llvm::LoadInst* load_tx = mBuilder.CreateLoad(mAtomic.tx);
    llvm::CallInst* call = mBuilder.CreateCall2(mAtomic.getReadFunction(&node), read_value, load_tx);
    read_value = call;
    return read_value;
}

llvm::Value* LLVMGeneratorStageVisitor::getNonAssignedReadValue(ASTNode& attach, ASTNode& node, llvm::Value* read_value)
{
    llvm::Value* result = read_value;
    // non-struct variable (primitive variable????)
    if(is_non_struct_lvalue(result))
    {
        result = mBuilder.CreateLoad(result);
        ADD_INTERMEDIATE_LLVM_VALUE(&attach, result);
    }
    return result;
}

llvm::Value* LLVMGeneratorStageVisitor::getAssignedReadValue(
    ASTNode& attach,
    ASTNode& node,
    llvm::Value* read_value)
{
    llvm::Value* result = read_value;
    llvm::Value* pointer_operand = llvm::cast<llvm::StoreInst>(result)->getPointerOperand();
    result = mBuilder.CreateLoad(pointer_operand);
    return result;
}

llvm::Value* LLVMGeneratorStageVisitor::getNonVariableReadValue(
    ASTNode& attach,
    ASTNode& node,
    llvm::Value* read_value)
{
    llvm::Value* result = read_value;

    ASTNode* resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&node);
    if(resolved_symbol == nullptr) {
        return result;
    }

    // enum member
    if(isa<VariableDecl>(resolved_symbol) && isa<EnumDecl>(resolved_symbol->parent))
    {
        bool is_long_literal = EnumDeclManglingContext::get(resolved_symbol->parent)->is_long_literal;
        result = llvm::ConstantInt::get(llvm::IntegerType::get(mContext, is_long_literal ? 64 : 32), EnumIdManglingContext::get(resolved_symbol)->value, true);
    }
    // static-member
    else
    {
        result = GET_SYNTHESIZED_LLVM_VALUE(resolved_symbol);
        if(result != nullptr)
        {
            if(result->getType()->isPointerTy())
            {
                result = mBuilder.CreateLoad(result);
                ADD_INTERMEDIATE_LLVM_VALUE(&attach, result);
            }
        }
        else
        {
            // nested value resolve
            return getReadValue(attach, *resolved_symbol);
        }
    }
    return result;
}

static bool is_atomic_read(ASTNode& node, llvm::Value* read_value, const LLVMGeneratorStageVisitor::AtomicCtxS& atomic_context)
{
    // only member variable and global/static variable need atomic monitor
    const bool is_member           = read_value != nullptr && llvm::isa<llvm::GetElementPtrInst>(read_value);
    const bool is_global           = read_value != nullptr && llvm::isa<llvm::GlobalVariable   >(read_value);
    const bool is_primitive        = ASTNodeHelper::getCanonicalType(&node) != nullptr && ASTNodeHelper::getCanonicalType(&node)->isPrimitiveType();
    const bool is_member_or_global = is_member || is_global;
    const bool need_atomic_read    = atomic_context.isInAtomicBlock() && is_member_or_global && is_primitive;
    return need_atomic_read;
}

llvm::Value* LLVMGeneratorStageVisitor::getWriteValue(ASTNode& attach, ASTNode& node)
{
    llvm::Value* read_value  = GET_SYNTHESIZED_LLVM_VALUE(&node);

    // not-assigned yet non-struct variable
    if(read_value)
        return read_value;

    // can not be enum member
    ASTNode* resolved_symbol = ASTNodeHelper::getCanonicalSymbol(&node);
    BOOST_ASSERT_MSG(!isa<VariableDecl>(resolved_symbol) || !isa<EnumDecl>(resolved_symbol->parent), "getWriteValue() on enum member");

    // member (static member?)
    read_value = GET_SYNTHESIZED_LLVM_VALUE(resolved_symbol);

    if(read_value == nullptr) {
        return getWriteValue(attach, *resolved_symbol);
    } else if(read_value->getType()->isPointerTy()) {
        return read_value;
    } else {
        BOOST_ASSERT_MSG(0, "getWriteValue() on r-value");
        return nullptr;
    }
}

llvm::Value* LLVMGeneratorStageVisitor::getReadValue(
    ASTNode& attach,
    ASTNode& node)
{
    // this function has 4 main branch:
    //
    // 1. atomic read (atomic write is handled outside of this function)
    //
    //    atomic -> a = b + c;
    //                  ^   ^
    //
    // 2. not-assigned non-struct variable & literal
    //    2.1. not-assigned non-struct variable
    //
    //         var a : int32 = 0;
    //         a = a + 1;
    //             ^
    //
    //    2.2. literal
    //
    //         a = a + 1;
    //                 ^
    //
    // 3. assigned variable
    //
    //    a = a + 1;
    //    ^
    //
    // 4. non-variable
    //    4.1. enum-member
    //
    //         enum EEE { x, y }
    //         EEE.x;
    //         ^^^
    //
    //    4.2. static-member
    //
    //         class Foo { public static function f() : void {} }
    //         Foo::f();
    //         ^^^

    llvm::Value* result = GET_SYNTHESIZED_LLVM_VALUE(&node);

    // 1. atomic read (atomic write is handled outside of this function)
    if(is_atomic_read(node, result, mAtomic))
    {
        return getAtomicReadValue(node, result);
    }
    // 2. not-assigned non-struct variable & literal
    else if(result && !llvm::isa<llvm::StoreInst>(result))
    {
        return getNonAssignedReadValue(attach, node, result);
    }
    // 3. assigned variable
    else if (result && llvm::isa<llvm::StoreInst>(result))
    {
        return getAssignedReadValue(attach, node, result);
    }
    // 4. non-variable
    else
    {
        return getNonVariableReadValue(attach, node, result);
    }

    return result;
}

bool LLVMGeneratorStageVisitor::isFunctionParameter(ASTNode& node)
{
    if(!isa<SimpleIdentifier>(&node))
        return false;

    if(!node.parent)
        return false;

    if(!isa<FunctionDecl>(node.parent))
        return false;

    return true;
}

int LLVMGeneratorStageVisitor::getFunctionParameterIndex(ASTNode& node)
{
    if(!isa<SimpleIdentifier>(&node))
        return -1;

    if(!node.parent)
        return -1;

    if(!isa<FunctionDecl>(node.parent))
        return -1;

    SimpleIdentifier* p = cast<SimpleIdentifier>(&node);
    FunctionDecl* f = cast<FunctionDecl>(node.parent);

    int index = 0;
    for(auto* param : f->parameters)
    {
        if(param->name == p) break;
        ++index;
    }

    if(index >= (int)f->parameters.size())
        return -1;

    return index;
}

FunctionDecl* LLVMGeneratorStageVisitor::getContainingFunction(ASTNode& node)
{
    if(!isa<SimpleIdentifier>(&node))
        return nullptr;

    if(!node.parent)
        return nullptr;

    return cast<FunctionDecl>(node.parent);
}

bool LLVMGeneratorStageVisitor::hasValue(ASTNode& ast_node)
{
    if(GET_SYNTHESIZED_LLVM_VALUE(&ast_node))
        return true;
    else
        return false;
}

bool LLVMGeneratorStageVisitor::isResolved(ASTNode& ast_node)
{
    if(ASTNodeHelper::getCanonicalSymbol(&ast_node))
        return true;
    else
        return false;
}

bool LLVMGeneratorStageVisitor::isFunctionVisited(FunctionDecl& ast_function)
{
    llvm::Function* llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(&ast_function);
    if(!llvm_function)
        return false;

    if(llvm_function->getBasicBlockList().size() == 0 )
        return false;

    return true;
}

bool LLVMGeneratorStageVisitor::isBlockInsertionMasked()
{
    return mFunctionContext.mask_insertion;
}

void LLVMGeneratorStageVisitor::setBlockInsertionMask()
{
    mFunctionContext.mask_insertion = true;
}

void LLVMGeneratorStageVisitor::resetBlockInsertionMask()
{
    mFunctionContext.mask_insertion = false;
}

llvm::BasicBlock* LLVMGeneratorStageVisitor::currentBlock()
{
    return mBuilder.GetInsertBlock();
}

bool LLVMGeneratorStageVisitor::isBlockTerminated(llvm::BasicBlock* block)
{
    return (block->getTerminator()) ? true : false;
}

bool LLVMGeneratorStageVisitor::propagate(ASTNode* to, ASTNode* from)
{
    if(!from || !to) return false;

    llvm::Value* existing_value = GET_SYNTHESIZED_LLVM_VALUE(to);
    if(!existing_value)
    {
        llvm::Value* value = GET_SYNTHESIZED_LLVM_VALUE(from);
        if(value)
        {
            SET_SYNTHESIZED_LLVM_VALUE(to, value);
        }
        else
        {
            if(isa<VariableDecl>(from))
            {
                tree::visitor::NodeInfoVisitor node_info_visitor;
                node_info_visitor.visit(*from);
                std::wstring from_info = node_info_visitor.stream.str();
                node_info_visitor.visit(*to);
                std::wstring to_info = node_info_visitor.stream.str();

                LOG4CXX_ERROR(LoggerWrapper::GeneratorStage, L"failed to propagate NULL value from \"" << from_info << "\" to \"" << to_info << L"\"");
            }
        }
    }

    return true;
}

void LLVMGeneratorStageVisitor::blockInitialize()
{
    //////////////////////////////////////////////////////////////////////
    // thread init
    // Currently, we don't have a good place to insert thread_init(),
    // so, we thread_init() before every atomic block
    //////////////////////////////////////////////////////////////////////
    // ; stm::thread_init();
    // call void @_ZN3stm11thread_initEv()
    mBuilder.CreateCall(mAtomic.thread_init);

    //////////////////////////////////////////////////////////////////////
    // block init
    // ; stm::TxThread* tx = (stm::TxThread*)stm::Self;
    // %tx = alloca %"struct.stm::TxThread"*, align 8
    //mAtomic.tx = mBuilder.CreateAlloca(mAtomic.ty_TxThread->getPointerTo()); //tx->setAlignment(8); // TBD
    mAtomic.tx = new llvm::AllocaInst(mAtomic.ty_TxThread->getPointerTo(), 0, "", mFunctionContext.alloca_insert_point);
    // ; jmp_buf _jmpbuf;
    // %tx = alloca %"struct.stm::TxThread"*, align 8
    //llvm::AllocaInst* _jmp_buf = mBuilder.CreateAlloca(mAtomic.ty_JmpBuf, llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(mContext), 1)); //_jmp_buf->setAlignment(16); // TBD
    llvm::AllocaInst* _jmp_buf = new llvm::AllocaInst(mAtomic.ty_JmpBuf, llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(mContext), 1), "", mFunctionContext.alloca_insert_point); //_jmp_buf->setAlignment(16); // TBD
    // ; uint32_t abort_flags = _setjmp (_jmpbuf);
    // %abort_flags = alloca i32, align 4
    //llvm::AllocaInst* abort_flags = mBuilder.CreateAlloca(llvm::IntegerType::getInt32Ty(mContext)); //abort_flags->setAlignment(4); // TBD
    llvm::AllocaInst* abort_flags = new llvm::AllocaInst(llvm::IntegerType::getInt32Ty(mContext), 0, "", mFunctionContext.alloca_insert_point); //abort_flags->setAlignment(4); // TBD
    // %0 = load %"struct.stm::TxThread"** @_ZN3stm4SelfE, align 8
    llvm::LoadInst* li_0 = mBuilder.CreateLoad(mAtomic.a_stm_selt); // liSelf->setAlignment(8);
    // store %"struct.stm::TxThread"* %0, %"struct.stm::TxThread"** %tx, align 8
    mBuilder.CreateStore(li_0, mAtomic.tx);
    // %arraydecay = getelementptr inbounds [1 x %struct.__jmp_buf_tag]* %_jmpbuf, i32 0, i32 0
    llvm::Value* arraydecay = mBuilder.CreateInBoundsGEP(_jmp_buf, llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(mContext), 0));
    // %call = call i32 @_setjmp(%struct.__jmp_buf_tag* %arraydecay) nounwind returns_twice
    llvm::CallInst* call = mBuilder.CreateCall(mAtomic.a_setjmp, arraydecay);
    call->setCanReturnTwice();
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    call->addAttribute(-1, llvm::Attribute::NoUnwind);
#else
    call->addAttribute(-1, llvm::Attributes::get(mContext, llvm::Attributes::NoUnwind));
#endif
    // store i32 %call, i32* %abort_flags, align 4
    mBuilder.CreateStore(call, abort_flags);
    // ; stm::begin(tx, &_jmpbuf, abort_flags);
    // %1 = load %"struct.stm::TxThread"** %tx, align 8
    llvm::LoadInst* li_1 = mBuilder.CreateLoad(mAtomic.tx);
    // %2 = bitcast [1 x %struct.__jmp_buf_tag]* %_jmpbuf to i8*
    llvm::Value* li_2 = mBuilder.CreateBitCast(_jmp_buf, llvm::IntegerType::getInt8PtrTy(mContext));
    // %3 = load i32* %abort_flags, align 4
    llvm::LoadInst* li_3 = mBuilder.CreateLoad(abort_flags);
    // call void @_ZN3stm5beginEPNS_8TxThreadEPvj(%"struct.stm::TxThread"* %1, i8* %2, i32 %3)
    mBuilder.CreateCall3(mAtomic.tm_begin, li_1, li_2, li_3);
    // ; __asm__ volatile ("":::"memory");
    // call void asm sideeffect "", "~{memory},~{dirflag},~{fpsr},~{flags}"() nounwind, !srcloc !0
    // NOTE: actually, it is a memory fence here.(we guess so)
    //       Per SDK's suggestion, I replace the orginal code with IRBuilder::CreateFence() for portability.
    //       I am not 100% sure is the two equivalent, but this pass the test case.
    //       If further test fail, try to fix this.
    mBuilder.CreateFence(llvm::SequentiallyConsistent);
}

void LLVMGeneratorStageVisitor::blockFinalize()
{
    // ; stm::commit(tx);
    // %8 = load %"struct.stm::TxThread"** %tx, align 8
    llvm::LoadInst* li_8 = mBuilder.CreateLoad(mAtomic.tx);
    // call void @_ZN3stm6commitEPNS_8TxThreadE(%"struct.stm::TxThread"* %8)
    mBuilder.CreateCall(mAtomic.tm_commit, li_8);
}

llvm::Function* LLVMGeneratorStageVisitor::genLLVMExternFunc(const std::string& name, llvm::Type* return_type, const std::vector<llvm::Type*>& param_types)
{
    llvm::FunctionType* func_type = nullptr;
    if(param_types.empty())
        func_type = llvm::FunctionType::get(return_type, false /*not variadic*/);
    else
        func_type = llvm::FunctionType::get(return_type, param_types, false /*not variadic*/);
    llvm::Function* func = llvm::Function::Create(func_type, llvm::GlobalValue::LinkageTypes::ExternalLinkage, name, &mModule);
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    func->addFnAttr(llvm::Attribute::NoUnwind);
#else
    func->addFnAttr(llvm::Attributes::NoUnwind);
#endif
    return func;
}

void LLVMGeneratorStageVisitor::generateAtomicDataStructures()
{
    if(getGeneratorContext().active_config->arch.is_not_x86())
        return;

    // tx
    llvm::Type* txTy       = llvm::Type::getInt8Ty(mContext);

    // short name for llvm type
    llvm::Type* llvoid     = llvm::Type::getVoidTy(mContext);
    llvm::Type* llint8     = llvm::Type::getInt8Ty(mContext);
    llvm::Type* llint8p    = llvm::Type::getInt8PtrTy(mContext);
    llvm::Type* llint8pp   = llvm::Type::getInt8PtrTy(mContext)->getPointerTo();
    llvm::Type* llint32    = llvm::Type::getInt32Ty(mContext);
    llvm::Type* llint32p   = llvm::Type::getInt32PtrTy(mContext);
    llvm::Type* llint64    = llvm::Type::getInt64Ty(mContext);
    llvm::Type* llint64p   = llvm::Type::getInt64PtrTy(mContext);
    llvm::Type* llfloat32  = llvm::Type::getFloatTy(mContext);
    llvm::Type* llfloat32p = llvm::Type::getFloatPtrTy(mContext);
    llvm::Type* llfloat64  = llvm::Type::getDoubleTy(mContext);
    llvm::Type* llfloat64p = llvm::Type::getDoublePtrTy(mContext);

    // TxThread
    mAtomic.ty_TxThread = llvm::Type::getInt8Ty(mContext);

    // jmp_buf
    std::vector<llvm::Type*> sig_type_list = { llvm::ArrayType::get(llvm::IntegerType::getInt64Ty(mContext), 16) };
    llvm::StructType* sigset_t = llvm::StructType::create(mContext, sig_type_list, "struct.__sigset_t");
    std::vector<llvm::Type*> buf_type_list = { llvm::ArrayType::get(llvm::IntegerType::getInt64Ty(mContext), 8), llvm::IntegerType::getInt32Ty(mContext), sigset_t };
    llvm::StructType* jmp_buf_tag = llvm::StructType::create(mContext, buf_type_list, "struct.__jmp_buf_tag");
    mAtomic.ty_JmpBuf = jmp_buf_tag;

    std::vector<llvm::Type*> handlerFnTyParams { txTy->getPointerTo() };
    llvm::FunctionType* handlerFnTy = llvm::FunctionType::get(llvoid, handlerFnTyParams, false/*not variadic*/);

    // extern functions                         mangled name                                , return type, parameters ...
    mAtomic.sys_init                              = genLLVMExternFunc("_ZN3stm8sys_initEPFvPNS_8TxThreadEE"       , llvoid     , { handlerFnTy->getPointerTo()                                             });
    mAtomic.thread_init                           = genLLVMExternFunc("_ZN3stm11thread_initEv"                    , llvoid     , {                                                                         });
    mAtomic.thread_shutdown                       = genLLVMExternFunc("_ZN3stm15thread_shutdownEv"                , llvoid     , {                                                                         });
    mAtomic.sys_shutdown                          = genLLVMExternFunc("_ZN3stm12sys_shutdownEv"                   , llvoid     , {                                                                         });
    // atomic block functions
    mAtomic.tm_begin                              = genLLVMExternFunc("_ZN3stm5beginEPNS_8TxThreadEPvj"           , llvoid     , { txTy->getPointerTo()       , llint8p             , llint32              });
    mAtomic.tm_commit                             = genLLVMExternFunc("_ZN3stm6commitEPNS_8TxThreadE"             , llvoid     , { txTy->getPointerTo()                                                    });
    mAtomic.a_setjmp                              = genLLVMExternFunc("_setjmp"                                   , llint32    , { jmp_buf_tag->getPointerTo()                                             });
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    mAtomic.a_setjmp->addFnAttr(llvm::Attribute::ReturnsTwice);
#else
    mAtomic.a_setjmp->addFnAttr(llvm::Attributes::ReturnsTwice);
#endif

    // read/write functions
    mAtomic.tm_read [PrimitiveKind::BOOL_TYPE   ] = genLLVMExternFunc("_ZN3stm8stm_readIbEET_PS1_PNS_8TxThreadE"  , llint8     , { llint8p                    , txTy->getPointerTo()                       });
    mAtomic.tm_read [PrimitiveKind::INT8_TYPE   ] = genLLVMExternFunc("_ZN3stm8stm_readIaEET_PS1_PNS_8TxThreadE"  , llint8     , { llint8p                    , txTy->getPointerTo()                       });
    mAtomic.tm_read [PrimitiveKind::INT32_TYPE  ] = genLLVMExternFunc("_ZN3stm8stm_readIiEET_PS1_PNS_8TxThreadE"  , llint32    , { llint32p                   , txTy->getPointerTo()                       });
    mAtomic.tm_read [PrimitiveKind::INT64_TYPE  ] = genLLVMExternFunc("_ZN3stm8stm_readIlEET_PS1_PNS_8TxThreadE"  , llint64    , { llint64p                   , txTy->getPointerTo()                       });
    mAtomic.tm_read [PrimitiveKind::FLOAT32_TYPE] = genLLVMExternFunc("_ZN3stm8stm_readIfEET_PS1_PNS_8TxThreadE"  , llfloat32  , { llfloat32p                 , txTy->getPointerTo()                       });
    mAtomic.tm_read [PrimitiveKind::FLOAT64_TYPE] = genLLVMExternFunc("_ZN3stm8stm_readIdEET_PS1_PNS_8TxThreadE"  , llfloat64  , { llfloat64p                 , txTy->getPointerTo()                       });
    mAtomic.tm_read [AtomicCtxS::POINTER_TYPE   ] = genLLVMExternFunc("_ZN3stm8stm_readIdEET_PS1_PNS_8TxThreadE"  , llint8p    , { llint8pp                   , txTy->getPointerTo()                       });

    mAtomic.tm_write[PrimitiveKind::BOOL_TYPE   ] = genLLVMExternFunc("_ZN3stm9stm_writeIbEEvPT_S1_PNS_8TxThreadE", llvoid     , { llint8p                    ,llint8               ,  txTy->getPointerTo()});
    mAtomic.tm_write[PrimitiveKind::INT8_TYPE   ] = genLLVMExternFunc("_ZN3stm9stm_writeIaEEvPT_S1_PNS_8TxThreadE", llvoid     , { llint8p                    ,llint8               ,  txTy->getPointerTo()});
    mAtomic.tm_write[PrimitiveKind::INT32_TYPE  ] = genLLVMExternFunc("_ZN3stm9stm_writeIiEEvPT_S1_PNS_8TxThreadE", llvoid     , { llint32p                   ,llint32              ,  txTy->getPointerTo()});
    mAtomic.tm_write[PrimitiveKind::INT64_TYPE  ] = genLLVMExternFunc("_ZN3stm9stm_writeIlEEvPT_S1_PNS_8TxThreadE", llvoid     , { llint64p                   ,llint64              ,  txTy->getPointerTo()});
    mAtomic.tm_write[PrimitiveKind::FLOAT32_TYPE] = genLLVMExternFunc("_ZN3stm9stm_writeIfEEvPT_S1_PNS_8TxThreadE", llvoid     , { llfloat32p                 ,llfloat32            ,  txTy->getPointerTo()});
    mAtomic.tm_write[PrimitiveKind::FLOAT64_TYPE] = genLLVMExternFunc("_ZN3stm9stm_writeIdEEvPT_S1_PNS_8TxThreadE", llvoid     , { llfloat64p                 ,llfloat64            ,  txTy->getPointerTo()});
    mAtomic.tm_write[AtomicCtxS::POINTER_TYPE   ] = genLLVMExternFunc("_ZN3stm9stm_writeIdEEvPT_S1_PNS_8TxThreadE", llvoid     , { llint8p                    ,llint8p              ,  txTy->getPointerTo()});


    // self
    llvm::GlobalVariable* g_val = new llvm::GlobalVariable(mModule, txTy->getPointerTo(), false/*isConstant*/, llvm::GlobalVariable::LinkageTypes::ExternalLinkage, nullptr/*init*/, "_ZN3stm4SelfE");
    g_val->setThreadLocal(true);
    mAtomic.a_stm_selt = g_val;
}

bool LLVMGeneratorStageVisitor::isBeAssigned(Expression& node) {
    BinaryExpr* be = cast<BinaryExpr>(node.parent);
    if(be == nullptr) return false;
    if(be->left != &node) return false;
    return be->opcode == BinaryExpr::OpCode::ASSIGN;
}

} } } }
