/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <boost/filesystem.hpp>

#include <llvm/Config/config.h>
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
    #include <llvm/DIBuilder.h>
#endif

#include "utility/Filesystem.h"
#include "utility/UnicodeUtil.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ParserContext.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/parser/context/SourceInfoContext.h"
#include "language/stage/generator/context/DebugInfoContext.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/context/SynthesizedAllocaThisPointerContext.h"
#include "language/logging/LoggerWrapper.h"
#include "language/stage/generator/visitor/LLVMDebugInfoGeneratorStagePreambleVisitor.h"

using namespace zillians::language::tree;
using zillians::language::tree::visitor::GenericDoubleVisitor;

namespace zillians { namespace language { namespace stage { namespace visitor {

#define COMPANY_INFORMATION "1.0 Thor Compiler (Zillians Inc)"

LLVMDebugInfoGeneratorStagePreambleVisitor::LLVMDebugInfoGeneratorStagePreambleVisitor(llvm::LLVMContext& context, llvm::Module& current_module, llvm::DIBuilder& factory)
    : mHelper(context, current_module, mBuilder)
      , mBuilder(context)
      , context(context)
      , current_module(current_module)
      , factory(factory)
{
    REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
}

LLVMDebugInfoGeneratorStagePreambleVisitor::~LLVMDebugInfoGeneratorStagePreambleVisitor()
{
}

void LLVMDebugInfoGeneratorStagePreambleVisitor::generate(ASTNode& node)
{
    revisit(node);
}

void LLVMDebugInfoGeneratorStagePreambleVisitor::generate(Package& node)
{
    // skip root package
    if (node.isRoot())
    {
        revisit(node);
        return;
    }

    // Well, our package is similar with the name space in C++.
    // For each file in Package, we need to create a namespace associated with it.

    // empty set
    llvm::DICompileUnit unit;
    llvm::DIFile file;

    DebugInfoContext* parent_debug_info = NULL;

    if (node.parent)
    {
        parent_debug_info = DebugInfoContext::get(node.parent);
    }

    llvm::DINameSpace name_space;
    if (parent_debug_info)
    {
        name_space = factory.createNameSpace(parent_debug_info->context, ws_to_s(node.id->toString().c_str()), file, 0);
    }
    else
    {
        llvm::DIDescriptor scope;
        name_space = factory.createNameSpace(scope, ws_to_s(node.id->toString().c_str()), file, 0);
    }

    DebugInfoContext::set(&node, new DebugInfoContext(unit, file, name_space));
    revisit(node);
}

void LLVMDebugInfoGeneratorStagePreambleVisitor::generate(Source& node)
{
//      if(!node.is_imported)
    {
        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, __PRETTY_FUNCTION__);

        // Create compile units
        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Program> Ori path: " << node.filename);
        boost::filesystem::path absolute_filepath = Filesystem::normalize_path(node.filename);
        std::string absolute_folder = absolute_filepath.parent_path().generic_string();
        std::string absolute_filename = absolute_filepath.generic_string();
        std::string filename = absolute_filepath.filename().generic_string();

        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Program> absolute folder: " << absolute_folder);
        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Program> absolute filename: " << absolute_filename);
        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Program> filename: " << filename);

        factory.createCompileUnit(llvm::dwarf::DW_LANG_C_plus_plus,
            llvm::StringRef(absolute_filename.c_str()), llvm::StringRef(absolute_folder.c_str()), llvm::StringRef(COMPANY_INFORMATION),
            /*optimized*/false, /*flags*/llvm::StringRef(""), /*runtime version*/0);

        llvm::DIFile file = factory.createFile(llvm::StringRef(filename.c_str()), llvm::StringRef(absolute_folder.c_str()));

        llvm::DICompileUnit compile_unit(factory.getCU());
        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Program> compile_unit: " << compile_unit);
        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Program> file: " << file);

        // generate namespace
        Package* package = cast<Package>(node.parent);
        if (package->isRoot())
        {
            DebugInfoContext::set(&node, new DebugInfoContext(compile_unit, file, file));
        }
        else
        {
            DebugInfoContext::set(&node, new DebugInfoContext(compile_unit, file, DebugInfoContext::get(package)->context));
        }

        revisit(node);
    }
}

void LLVMDebugInfoGeneratorStagePreambleVisitor::generate(ClassDecl& node)
{
    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    // currently we don't support GPU code debugging
    if(getGeneratorContext().active_config->arch.is_gpu())
        return;

    DebugInfoContext* source_debug_context = DebugInfoContext::get(node.getOwner<Source>());

    // Well, generate a class type without looking its members
    // We will make it complete once in later visitor

    //llvm::DIType class_type_info = factory.createTemporaryType(source_debug_context->file);
    SourceInfoContext* source_info_context = SourceInfoContext::get(&node);
    llvm::DIType class_type_info = factory.createForwardDecl(
        llvm::dwarf::DW_TAG_structure_type,
        llvm::StringRef(ws_to_s(node.name->toString()).c_str()),
        DebugInfoContext::get(node.parent)->context,
        source_debug_context->file,
        source_info_context->line
        );
    DebugInfoContext::set(&node, new DebugInfoContext(source_debug_context->compile_unit, source_debug_context->file, class_type_info));
}
}}}}

