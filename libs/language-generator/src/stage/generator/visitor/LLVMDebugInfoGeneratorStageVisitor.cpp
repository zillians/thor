/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <unordered_set>

#include <boost/filesystem.hpp>

#include <llvm/Value.h>

#include "utility/Filesystem.h"
#include "utility/UnicodeUtil.h"
#include "language/context/ManglingStageContext.h"
#include "language/context/ParserContext.h"
#include "language/tree/visitor/GenericDoubleVisitor.h"
#include "language/tree/ASTNodeHelper.h"
#include "language/logging/LoggerWrapper.h"
#include "language/stage/generator/detail/LLVMHelper.h"
#include "language/stage/parser/context/SourceInfoContext.h"
#include "language/stage/generator/context/DebugInfoContext.h"
#include "language/stage/generator/context/SynthesizedFunctionContext.h"
#include "language/stage/generator/context/SynthesizedValueContext.h"
#include "language/stage/generator/context/SynthesizedAllocaThisPointerContext.h"
#include "language/stage/generator/visitor/LLVMDebugInfoGeneratorStageVisitor.h"

namespace zillians { namespace language { namespace stage { namespace visitor {

using namespace language::tree;

LLVMDebugInfoGeneratorStageVisitor::LLVMDebugInfoGeneratorStageVisitor(llvm::LLVMContext& context, llvm::Module& current_module, llvm::DIBuilder& factory)
    : mHelper(context, current_module, mBuilder)
    , mBuilder(context)
    , context(context)
    , current_module(current_module)
    , factory(factory)
{
    REGISTER_ALL_VISITABLE_ASTNODE(generateInvoker);
}

LLVMDebugInfoGeneratorStageVisitor::~LLVMDebugInfoGeneratorStageVisitor()
{
    // Call this function to generated all deferred debug information
    factory.finalize();
}

void LLVMDebugInfoGeneratorStageVisitor::generate(ASTNode& node)
{
    /**
     * Generic implementation:
     *
     * 1. Propagate the debug info from parent node to child node
     * 2. Insert default debug information (line, column, context)
     *
     * Usually, this function is good for the leaf nodes. If the node is not leaf, we need to specify the context manually, like function, block.
     */
    LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "Handle General Node");

    // This one should be the first, since the following calls, insertDebugLocation and insertDebugLocationForIntermediateValues,
    // may depend on the lexical block created in it.
    insertDebugLocationForBlock(node);

    insertDebugLocation(node);
    insertDebugLocationForIntermediateValues(node);
    revisit(node);
}

void LLVMDebugInfoGeneratorStageVisitor::generate(FunctionType& node)
{
    // skip FunctionType
    // TODO: think more carefully if we need to handle it or not
}

void LLVMDebugInfoGeneratorStageVisitor::generate(Source& node)
{
    // Debug info for source node had been built in preamble stage
    revisit(node);
}

void LLVMDebugInfoGeneratorStageVisitor::generate(CallExpr& node)
{
    insertDebugLocation(node);
}

void LLVMDebugInfoGeneratorStageVisitor::generate(TypeSpecifier& node)
{
    if (DebugInfoTypeContext::get(&node))
        return;

    uint32 bits = 0;
    uint32 alignment = 0;
    llvm::dwarf::dwarf_constants encoding;
    llvm::DIType debug_info_type;

    bool result = createDIType(node, debug_info_type, bits, alignment, encoding);
    BOOST_ASSERT(result && "Fail to create DIType for specific type");
    if (result)
    {
        DebugInfoTypeContext::set(&node, new DebugInfoTypeContext(debug_info_type, bits, alignment, encoding));
    }
}

void LLVMDebugInfoGeneratorStageVisitor::generate(EnumDecl& node)
{
    if (DebugInfoContext::get(&node))
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    DebugInfoContext* parent_context = DebugInfoContext::get(node.parent);
    SourceInfoContext* source_info_context = SourceInfoContext::get(&node);

    // construct each enum element
    uint64 member_size = 0;
    uint64 member_align = 0;
    std::vector<llvm::Value*> elements;
    for(auto* value : node.values)
    {
        elements.push_back(factory.createEnumerator(
                                ws_to_s(value->name->toString().c_str()), 
                                EnumIdManglingContext::get(value)->value));
        member_size = mHelper.getTypeSizeInBits(GET_SYNTHESIZED_LLVM_VALUE(value)->getType());
        member_align = mHelper.getTypeAlignmentInBits(GET_SYNTHESIZED_LLVM_VALUE(value)->getType());
    }

    // create enum declaratoin debug info
    // TODO createEnumerationType requires additional argument for ClassTy and Flags
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 2
    llvm::DIType class_ty;
    //unsigned flags = 0;
    llvm::DIType enum_type = factory.createEnumerationType(
            parent_context->context, 									// Scope
            llvm::StringRef(ws_to_s(node.name->toString()).c_str()),	// Union name
            parent_context->file,										// File
            source_info_context->line,									// Line
            member_size,												// Member size
            member_align,												// Member alignment
            factory.getOrCreateArray(llvm::ArrayRef<llvm::Value*>(elements)),	// Elements
            class_ty                                                    // Class type
            );
#else
    llvm::DIType enum_type = factory.createEnumerationType(
            parent_context->context, 									// Scope
            llvm::StringRef(ws_to_s(node.name->toString()).c_str()),	// Union name
            parent_context->file,										// File
            source_info_context->line,									// Line
            member_size,												// Member size
            member_align,												// Member alignment
            factory.getOrCreateArray(llvm::ArrayRef<llvm::Value*>(elements))	// Elements
            );
#endif
    DebugInfoContext::set(&node, new DebugInfoContext(parent_context->compile_unit, parent_context->file, enum_type));
}

void LLVMDebugInfoGeneratorStageVisitor::generate(ClassDecl& node)
{
    // we don't generate code for non-fully-specialized classes
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

//  if(ASTNodeHelper::hasNativeLinkage(&node))
//      return;

    llvm::StructType* llvm_struct_type = mHelper.getStructType(node);
    BOOST_ASSERT(llvm_struct_type && "The class does not has struct type");

    SourceInfoContext* source_info_context = SourceInfoContext::get(&node);
    DebugInfoContext* parent_context = DebugInfoContext::get(node.parent);

    // Generate member variable debug info types
    std::vector<llvm::Value*> elements;
    createClassMembersDebugInfo(node, elements, parent_context);

    llvm::DIType class_type_info = factory.createClassType(
            parent_context->context,                        // Scope
            llvm::StringRef(ws_to_s(node.name->toString()).c_str()),            // Name
            parent_context->file,                                               // File
            source_info_context->line,                                          // LineNo
            mHelper.getTypeSizeInBits(llvm_struct_type),                        // SizeInBits
            mHelper.getTypeAlignmentInBits(llvm_struct_type),                   // AlignInBits
            0,                                                                  // OffsetInBits
            llvm::DIType::FlagObjcClassComplete,                                // Flags
            llvm::DIType(),                                                     // DerivedFrom  (Ref. clang)
            factory.getOrCreateArray(llvm::ArrayRef<llvm::Value*>(elements)),   // Elements
            NULL,                                                               // VTableHolder
            NULL                                                                // TemplateParms
            );

    // Replace the original one
    DebugInfoContext* context = DebugInfoContext::get(&node);
    context->context->replaceAllUsesWith(class_type_info);
    DebugInfoContext::set(&node, new DebugInfoContext(parent_context->compile_unit, parent_context->file, class_type_info));

    for(auto* method : node.member_functions) generate(*method);
}

bool LLVMDebugInfoGeneratorStageVisitor::createMultiType(MultiSpecifier& node, llvm::DIType& type,
                                                         uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding)
{
    // setup basic settings
    bits = mHelper.getPointerSizeInBits();
    alignment = mHelper.getPointerABIAlignment();
    encoding = llvm::dwarf::DW_TAG_pointer_type;

    // get llvm struct type equivalence of this multiple type node
    llvm::Type *struct_type = mHelper.getType(node);
    BOOST_ASSERT( struct_type && "cannot get llvm type from multiple type node" );

    // get informations from source files
    auto source = SourceInfoContext::get( node.parent ); // same as function declaration
    auto source_info = DebugInfoContext::get( node.getOwner<Source>() );

    // create llvm debug info types for anonymous struct members
    std::vector< llvm::Value* > elements;
    unsigned element_index = 0;
    for( auto member_type : node.getTypes() )
    {
        // get variable debug type info
        generate( *member_type );
        auto member_type_info = DebugInfoTypeContext::get( member_type );
        BOOST_ASSERT( member_type_info && "context of anonymous struct member cannot be empty" );

        // get member llvm type and its debug type info
        llvm::Type *member_llvm_type = mHelper.getType(*member_type);
        BOOST_ASSERT( member_llvm_type && "cannot get llvm type from multiple member's type" );

        auto member_name = "m" + std::to_string(element_index);
        auto member_info = factory.createMemberType(
                source_info->context,                                                      // Scope
                llvm::StringRef(member_name),                                              // Name
                source_info->file,                                                         // File
                source->line,                                                              // LineNo (I don't care where it was declared in the class)
                mHelper.getTypeSizeInBits(member_llvm_type),                               // SizeInBits
                mHelper.getTypeAlignmentInBits(member_llvm_type),                          // AlignInBits
                mHelper.getStructElementOffset(llvm::cast<llvm::StructType>(struct_type),  // OffsetInBits
                                               element_index) * 8,
                0,                                                                         // Flags
                member_type_info->type                                                     // Parent type
        );
        ++element_index;
        // add member_info into elements
        elements.push_back( member_info );
    }

    // create llvm debug info type for anonymous structure
    auto struct_type_info = factory.createStructType(
            source_info->context,                                               // Scope
            llvm::StringRef("anonymous"),                                       // Name
            source_info->file,                                                  // File
            source->line,                                                       // LineNo
            mHelper.getTypeSizeInBits(struct_type),                             // SizeInBits
            mHelper.getTypeAlignmentInBits(struct_type),                        // AlignInBits
            llvm::DIType::FlagObjcClassComplete,                                // Flags
#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
            llvm::DIType{},                                                     // DerivedFrom
#endif
            factory.getOrCreateArray(llvm::ArrayRef<llvm::Value*>(elements))    // Elements
            );

    DebugInfoContext::set( &node, new DebugInfoContext(source_info->compile_unit,
                                                       source_info->file,
                                                       struct_type_info) );

    return true;
}

void LLVMDebugInfoGeneratorStageVisitor::generate(FunctionDecl& node)
{
    // we don't generate code for non-fully-specialized functions
    if(isa<TemplatedIdentifier>(node.name) && !cast<TemplatedIdentifier>(node.name)->isFullySpecialized())
        return;

    if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
        return;

    if(node.getOwner<Source>()->is_imported)
    {
        if(node.is_member)
        {
            if(!isa<TemplatedIdentifier>(node.getOwner<ClassDecl>()->name) && !isa<TemplatedIdentifier>(node.name))
                return;
        }
        else
        {
            if(!isa<TemplatedIdentifier>(node.name))
                return;
        }
    }

    // Neglect all without source info context
    SourceInfoContext* source_info_context = SourceInfoContext::get(&node);
    if (!source_info_context)
        return;

    if(ASTNodeHelper::hasNativeLinkage(&node))
        return;

    // Skip functions in interface 
    ClassDecl* class_decl = node.getOwner<ClassDecl>();
    if (node.is_member && class_decl->is_interface) return;

    NameManglingContext* mangling = NameManglingContext::get(&node);
    Source* source = node.getOwner<Source>();
    DebugInfoContext* file_context = DebugInfoContext::get(source);
    LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage,"<Function> (" << source->filename << ") " << ws_to_s(node.name->toString()) << ", " << mangling->mangled_name);

    // generate return type debug information
    generate(*node.type);
    llvm::DIType subroutine_type = createSubroutineType(node, file_context->file);

    // Create DISubprogram for the function
    llvm::Function* llvm_function = GET_SYNTHESIZED_LLVM_FUNCTION(&node);
    BOOST_ASSERT(llvm_function && "Well, the llvm function is NULL");

    llvm::DISubprogram subprogram = factory.createFunction(
            DebugInfoContext::get(node.parent)->context,
            llvm::StringRef(ws_to_s(node.name->toString()).c_str()),
            llvm::StringRef(mangling->mangled_name.c_str()),
            file_context->file,
            source_info_context->line,
            subroutine_type,
            false, //bool isLocalToUnit,
            true, //bool isDefinition,
            source_info_context->line,
            llvm::DIDescriptor::FlagPrototyped, // TODO: Decide the flag
            false, //bool isOptimized = false
            llvm_function,
            NULL, // Function template parameters
            NULL // I suppose this one is for forward declaration node
            );

    DebugInfoContext::set(&node, new DebugInfoContext(file_context->compile_unit, file_context->file, subprogram));

    // Well, special treat for *this* pointer
    bool has_this_pointer = (node.is_member && !node.is_static);
    if (has_this_pointer)
    {
        // Create *this* pointer type
        auto function_debug_info = DebugInfoContext::get(&node);
        auto function_source_info = SourceInfoContext::get(&node);

        uint32 dummy;
        llvm::dwarf::dwarf_constants dummy_encoding;
        llvm::DIType this_pointer_type;
        bool result = createStructPointerType(*class_decl, this_pointer_type, dummy, dummy, dummy_encoding);
        BOOST_ASSERT(result && "Fail to create pointer to struct");

        if (result)
        {
            createVariableDebugInfoDeclare(
                    llvm_function->arg_begin()->getName(),
                    llvm::dwarf::DW_TAG_arg_variable,
                    this_pointer_type,
                    GET_SYNTHESIZED_ALLOCA_THIS_POINTER(&node),
                    &llvm_function->getEntryBlock(),
                    function_debug_info,
                    function_source_info,
                    1);
        }
    }

    // Generate block and parameters debug info
    int arg_base = (has_this_pointer) ? (2) : (1);
    for (decltype(node.parameters.size()) i = 0; i < node.parameters.size(); i++)
    {
        // generate debug information of parameters' type
        generateVariableDebugInfo(*node.parameters[i], llvm::dwarf::DW_TAG_arg_variable, i+arg_base);
    }
    if(node.block) generate(*node.block);
}

void LLVMDebugInfoGeneratorStageVisitor::generate(VariableDecl& node)
{
    // If the variable declartion is placed after the branch statement, the generator
    // stage will not generate llvm values for these nodes. So neglect those without 
    // llvm value. Though we may silently fail to detect those nodes which in somehow
    // does not attach the llvm value onto the node.
    if (!GET_SYNTHESIZED_LLVM_VALUE(&node)) return;

    if(node.isGlobal())
    {
        if(!LLVMHelper::isCurrentCodeGenerationTarget(&node))
            return;

        // generate global variable
        generateGlobalVariableDebugInfo(node);
    }
    else if (node.is_static)
    {
        // generate static variable
        generateStaticVariableDebugInfo(node, false);
    }
    else if (node.hasOwner<FunctionDecl>())
    {
        // generate local variable
        generateVariableDebugInfo(node, llvm::dwarf::DW_TAG_auto_variable);
    }
    else
    {
        UNREACHABLE_CODE();
    }
}

bool LLVMDebugInfoGeneratorStageVisitor::createDIType(TypeSpecifier& node, llvm::DIType& type, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding)
{
    if (isa<NamedSpecifier>(node))
    {
        Type* resolved_type = node.getCanonicalType();
        if (resolved_type->isRecordType())
        {
            ClassDecl* class_decl = resolved_type->getAsClassDecl();
            return createStructPointerType(*class_decl, type, bits, alignment, encoding);
        }
        else if(resolved_type->isEnumType())
        {
            if(EnumDeclManglingContext::get(resolved_type->getAsEnumDecl())->is_long_literal)
                return createPrimitiveType(PrimitiveKind(PrimitiveKind::INT64_TYPE), type, bits, alignment, encoding);
            else
                return createPrimitiveType(PrimitiveKind(PrimitiveKind::INT32_TYPE), type, bits, alignment, encoding);
        }
        else if (resolved_type->isPrimitiveType())
        {
            //TypeSpecifier* type_specifier = cast<TypeSpecifier>(resolved_type);
            Internal* internal = getParserContext().tangle->internal;
            TypeSpecifier* type_specifier = internal->getPrimitiveTypeSpecifier(resolved_type->getAsPrimitiveType()->getKind());
            return createDIType(*type_specifier, type, bits, alignment, encoding);
        }
        else
        {
            BOOST_ASSERT(false && "unsupported type");
        }
    }
    /// TODO: add debug information for multiple type
    else if (auto*const node_multi = cast<MultiSpecifier>(&node))
    {
        return createMultiType(*node_multi, type, bits, alignment, encoding);
    }
    else if (auto*const node_primitive = cast<PrimitiveSpecifier>(&node))
    {
        return createPrimitiveType(node_primitive->getKind(), type, bits, alignment, encoding);
    }
    else
    {
        BOOST_ASSERT(false && "Unsupported type");
    }

    return false;
}

void LLVMDebugInfoGeneratorStageVisitor::createClassMembersDebugInfo(ClassDecl& node, std::vector<llvm::Value*>& elements, DebugInfoContext* debuginfo_context)
{
    if (node.is_interface) return;

    for(auto* attribute : node.member_variables)
    {
        if (attribute->is_static)
        {
            // special handling for class static member
            generateStaticVariableDebugInfo(*attribute, true);
        }
        else
        {
            elements.push_back(createMemberVariableDebugInfo(*attribute, debuginfo_context));
        }
    }

    if (node.base)
    {
        ClassDecl* base = node.base->getCanonicalType()->getAsClassDecl();
        createClassMembersDebugInfo(*base, elements, debuginfo_context);
    }
}

llvm::DIType LLVMDebugInfoGeneratorStageVisitor::createSubroutineType(FunctionDecl& node, llvm::DIFile file)
{
    DebugInfoTypeContext* return_type = DebugInfoTypeContext::get(node.type);

    // Create subrountine type
    std::vector<llvm::Value*> elements;

    elements.push_back(return_type->type);

    // TODO: By observing clang, I found that even I give lots of parameters, it only generates return type. Parameters seem to lose.
    // So, temporarily we only put the return type.

    llvm::DIArray parameter_types = factory.getOrCreateArray(llvm::ArrayRef<llvm::Value*>(elements));
    return factory.createSubroutineType(file, parameter_types);
}

llvm::DIType LLVMDebugInfoGeneratorStageVisitor::createMemberVariableDebugInfo(VariableDecl& node, DebugInfoContext* context)
{
    // Get variable debug type info
    generate(*node.type);
    DebugInfoTypeContext* variable_debug_typeinfo = DebugInfoTypeContext::get(node.type);

    // get class information
    ClassDecl* class_decl = node.getOwner<ClassDecl>();
    auto object_layout = GET_SYNTHESIZED_OBJECTLAYOUT(class_decl);
    llvm::StructType* llvm_struct_type = mHelper.getStructType(*class_decl);

    BOOST_ASSERT(object_layout->member_attributes.count(&node) > 0 && "No specific member variable found");
    auto offset_in_bytes = object_layout->member_attributes[&node].get<1>();

    llvm::DIType member_variable_info = factory.createMemberType(
            context->context,                                           // Scope
            llvm::StringRef(ws_to_s(node.name->toString()).c_str()),    // Name
            context->file,                                              // File
            0,                                                          // LineNo (I don't care where it was declared in the class)
            variable_debug_typeinfo->bits,                              // SizeInBits
            variable_debug_typeinfo->alignment,                         // AlignInBits
            offset_in_bytes * 8,                                        // OffsetInBits
            0,                                                          // Flags
            variable_debug_typeinfo->type                               // Parent type
    );

    return member_variable_info;
}

void LLVMDebugInfoGeneratorStageVisitor::generateGlobalVariableDebugInfo(VariableDecl& node)
{
    // note that we may generate implicit global variables such as function id or symbol id. For those global
    // variables, we may not have source information. In this case, give a default line number.
    LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, __PRETTY_FUNCTION__);
    BOOST_ASSERT(node.parent && "Variable declaration has no parent!");

    DebugInfoContext* parent_debug_info = DebugInfoContext::get(node.parent);
    SourceInfoContext* source_info = SourceInfoContext::get(&node);

    generate(*node.type);
    DebugInfoTypeContext* type_info = DebugInfoTypeContext::get(node.type);

    llvm::GlobalVariable* llvm_global_value = llvm::cast<llvm::GlobalVariable>(GET_SYNTHESIZED_LLVM_VALUE(&node));
    llvm::DIGlobalVariable global_variable = factory.createStaticVariable(
                    parent_debug_info->context,
                    llvm::StringRef(ws_to_s(node.name->toString()).c_str()),
                    llvm::StringRef(NameManglingContext::get(&node)->mangled_name),
                    parent_debug_info->file,
                    source_info->line,
                    type_info->type,
                    true,
                    llvm_global_value);

    DebugInfoContext::set(&node, new DebugInfoContext(
            parent_debug_info->compile_unit, parent_debug_info->file, // inherit from parent node
            global_variable));
}

void LLVMDebugInfoGeneratorStageVisitor::generateStaticVariableDebugInfo(VariableDecl& node, bool is_class_data_member)
{
    LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, __PRETTY_FUNCTION__);
    BOOST_ASSERT(node.parent && "Variable declaration has no parent!");

    DebugInfoContext* parent_debug_info = NULL;
    bool is_local_to_unit = false;
    if (is_class_data_member)
    {
        parent_debug_info = DebugInfoContext::get(node.getOwner<ClassDecl>());
    }
    else
    {
        is_local_to_unit = true;
        parent_debug_info = DebugInfoContext::get(node.getOwner<FunctionDecl>());
    }
    SourceInfoContext* source_info = SourceInfoContext::get(&node);

    generate(*node.type);
    DebugInfoTypeContext* type_info = DebugInfoTypeContext::get(node.type);

    llvm::DIGlobalVariable static_variable = factory.createStaticVariable(
                    parent_debug_info->context,
                    llvm::StringRef(ws_to_s(node.name->toString()).c_str()),
                    llvm::StringRef(StaticVariableManglingContext::get(&node)->mangled_name),
                    parent_debug_info->file,
                    source_info->line,
                    type_info->type,
                    is_local_to_unit,
                    GET_SYNTHESIZED_LLVM_VALUE(&node));

    DebugInfoContext::set(&node, new DebugInfoContext(
            parent_debug_info->compile_unit, parent_debug_info->file, // inherit from parent node
            static_variable));
}

void LLVMDebugInfoGeneratorStageVisitor::generateVariableDebugInfo(VariableDecl& node, llvm::dwarf::llvm_dwarf_constants variable_type, int argument_position)
{
    LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, __PRETTY_FUNCTION__);
    BOOST_ASSERT(node.parent && "Variable declaration has no parent!");

    DebugInfoContext* parent_debug_info = DebugInfoContext::get(node.parent);
    SourceInfoContext* source_info = SourceInfoContext::get(&node);

    LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Variable> parent context: " << parent_debug_info->context);

    // generate type debug information
    generate(*node.type);
    DebugInfoTypeContext* type_info = DebugInfoTypeContext::get(node.type);

    llvm::Value* llvm_value = GET_SYNTHESIZED_LLVM_VALUE(&node);
    llvm::BasicBlock* llvm_block = llvm::cast<llvm::Instruction>(llvm_value)->getParent();
    llvm::DIVariable variable = createVariableDebugInfoDeclare(
            llvm::StringRef(ws_to_s(node.name->toString()).c_str()),
            variable_type,
            type_info->type,
            llvm_value,
            llvm_block,
            parent_debug_info, source_info, argument_position);

    DebugInfoContext::set(&node, new DebugInfoContext(
            parent_debug_info->compile_unit, parent_debug_info->file, // inherit from parent node
            variable));

    // Check if the variable has initialization
    insertDebugLocationForIntermediateValues(node);
}

llvm::DIVariable LLVMDebugInfoGeneratorStageVisitor::createVariableDebugInfoDeclare(llvm::StringRef name, llvm::dwarf::llvm_dwarf_constants variable_type, llvm::DIType type, llvm::Value* llvm_value,
        llvm::BasicBlock* block, DebugInfoContext* debug_info, SourceInfoContext* source_info, int argument_position)
{
    // TODO: Need to decide the type
    BOOST_ASSERT( (source_info->line >> 24) == 0 && "To much lines!! Only allowed 16777215 lines");
    uint32 offset = (argument_position << 24) + source_info->line;
    llvm::DIVariable variable = factory.createLocalVariable(variable_type, debug_info->context, name, debug_info->file, offset, type);

    llvm::Instruction* variable_inst = factory.insertDeclare(llvm_value, variable, block);
    llvm::MDNode* scope = debug_info->context;
    variable_inst->setDebugLoc(llvm::DebugLoc::get(source_info->line, source_info->column, scope));

    return variable;
}

bool LLVMDebugInfoGeneratorStageVisitor::getPrimitiveTypeInfo(PrimitiveKind kind, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding)
{
    bits = 0; alignment = 0; encoding = llvm::dwarf::DW_ATE_unsigned;

    switch (kind)
    {
    case PrimitiveKind::BOOL_TYPE:
    {
        bits = 8; alignment = 8; encoding = llvm::dwarf::DW_ATE_boolean;
        break;
    }
    case PrimitiveKind::INT8_TYPE:
    {
        bits = 8; alignment = 8; encoding = llvm::dwarf::DW_ATE_signed;
        break;
    }
    case PrimitiveKind::INT16_TYPE:
    {
        bits = 16; alignment = 16; encoding = llvm::dwarf::DW_ATE_signed;
        break;
    }
    case PrimitiveKind::FLOAT32_TYPE:
    {
        bits = 32; alignment = 32; encoding = llvm::dwarf::DW_ATE_float;
        break;
    }
    case PrimitiveKind::INT32_TYPE:
    {
        bits = 32; alignment = 32; encoding = llvm::dwarf::DW_ATE_signed;
        break;
    }
    case PrimitiveKind::FLOAT64_TYPE:
    {
        bits = 64; alignment = 64; encoding = llvm::dwarf::DW_ATE_float;
        break;
    }
    case PrimitiveKind::INT64_TYPE:
    {
        bits = 64; alignment = 64; encoding = llvm::dwarf::DW_ATE_signed;
        break;
    }
    case PrimitiveKind::VOID_TYPE:
    {
        return false;
    }
    default:
        BOOST_ASSERT(false && "Unknown basic kind");
        return false;
    }
    return true;
}

bool LLVMDebugInfoGeneratorStageVisitor::createStructPointerType(ClassDecl& class_decl, llvm::DIType& debug_info_type,
                                                                 uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding)
{
    // Setup basic settings
    bits = mHelper.getPointerSizeInBits();
    alignment = mHelper.getPointerABIAlignment();
    encoding = llvm::dwarf::DW_TAG_pointer_type;

    // Check if we have the specific pointer to the struct type already
    llvm::StructType* llvm_struct_type = mHelper.getStructType(class_decl);
    BOOST_ASSERT(llvm_struct_type && "Struct type is NULL");

    // Create the structure pointer type and store it
    auto class_debug_info = DebugInfoContext::get(&class_decl);
    llvm::DIType class_type_info = static_cast<llvm::DIType>(class_debug_info->context);
    debug_info_type = factory.createPointerType(class_type_info, mHelper.getPointerSizeInBits(), mHelper.getPointerABIAlignment());

    return true;
}

bool LLVMDebugInfoGeneratorStageVisitor::createPrimitiveType(PrimitiveKind kind, llvm::DIType& debug_info_type, uint32& bits, uint32& alignment, llvm::dwarf::dwarf_constants& encoding)
{
    bool result = getPrimitiveTypeInfo(kind, bits, alignment, encoding);
    if (!result)
    {
        debug_info_type = llvm::DIType();
        return true;
    }

    debug_info_type = factory.createBasicType(
            llvm::StringRef(ws_to_s(kind.toString().c_str())),
            bits, alignment, encoding);

    return true;
}

void LLVMDebugInfoGeneratorStageVisitor::insertDebugLocationForBlock(ASTNode& node)
{
    llvm::BasicBlock* block = llvm::dyn_cast_or_null<llvm::BasicBlock>(GET_SYNTHESIZED_LLVM_BLOCK(&node));

    if (block)
    {
        // Retrieve parent node debug information, since we need its context
        DebugInfoContext* parent_debug_info = DebugInfoContext::get(node.parent);
        SourceInfoContext* source_info = SourceInfoContext::get(&node);

        LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, "<Block> line: " << source_info->line << " column: " << source_info->column);
        llvm::DILexicalBlock function_block = factory.createLexicalBlock(
                parent_debug_info->context, parent_debug_info->file, source_info->line, source_info->column);

        DebugInfoContext::set(&node, new DebugInfoContext(
                parent_debug_info->compile_unit, parent_debug_info->file,   // inherit from parent node
                function_block));

        // Insert the position for the last command
        llvm::TerminatorInst* terminate_inst = block->getTerminator();
        terminate_inst->setDebugLoc(llvm::DebugLoc::get(source_info->line, source_info->column, parent_debug_info->context));
    }
}

DebugInfoContext* LLVMDebugInfoGeneratorStageVisitor::getOrInheritDebugInfo(ASTNode& node)
{
    DebugInfoContext* debug_info = DebugInfoContext::get(&node);
    if (!debug_info && node.parent)
    {
        // The current node does not have debug info, so it must inherit from the parent
        DebugInfoContext* parent_debug_info = DebugInfoContext::get(node.parent);
        if (parent_debug_info)
        {
            DebugInfoContext::set(&node, new DebugInfoContext(*parent_debug_info));
            debug_info = DebugInfoContext::get(&node);
        }
    }

    return debug_info;
}

void LLVMDebugInfoGeneratorStageVisitor::insertDebugLocation(ASTNode& node)
{
    // Get debug information context first
    auto debug_info = getOrInheritDebugInfo(node);

    // Attach debug info to specific llvm instruction
    if (debug_info)
    {
        SourceInfoContext* source_info = SourceInfoContext::get(&node);
        llvm::Value* llvm_value = GET_SYNTHESIZED_LLVM_VALUE(&node);
        if (llvm_value)
        {
            llvm::Instruction* llvm_inst = llvm::dyn_cast<llvm::Instruction>(llvm_value);
            if (llvm_inst)
            {
                LOG4CXX_DEBUG(LoggerWrapper::DebugInfoGeneratorStage, __FUNCTION__ << ": " << node.instanceName() << " @" << llvm_inst << "(" << source_info->line << ", " << source_info->column << ") with scope: " << (llvm::MDNode*)debug_info->context);
                llvm_inst->setDebugLoc(llvm::DebugLoc::get(source_info->line, source_info->column, debug_info->context));
            }
        }
    }
}

void LLVMDebugInfoGeneratorStageVisitor::insertDebugLocationForIntermediateValues(ASTNode& node)
{
    auto debug_info = getOrInheritDebugInfo(node);

    std::unordered_set<llvm::Value*> values = GET_INTERMEDIATE_LLVM_VALUES(&node);
    SourceInfoContext* source_info = SourceInfoContext::get(&node);

    for(auto& value : values)
    {
        llvm::Instruction* inst = llvm::dyn_cast<llvm::Instruction>(value);
        inst->setDebugLoc(llvm::DebugLoc::get(source_info->line, source_info->column, debug_info->context));
    }
}

}}}}

