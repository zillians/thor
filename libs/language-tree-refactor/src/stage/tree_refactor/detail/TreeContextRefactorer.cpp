/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <climits>

#include <deque>
#include <functional>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/all_of.hpp>
#include <boost/algorithm/cxx11/any_of.hpp>
#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/set_of.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/contains.hpp>
#include <boost/mpl/push_back.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/size_t.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include <boost/range/algorithm/lexicographical_compare.hpp>
#include <boost/range/algorithm_ext/erase.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/empty.hpp>
#include <boost/range/end.hpp>
#include <boost/range/iterator_range.hpp>

#include "language/context/ManglingStageContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/UniqueName.h"
#include "language/tree/basic/Annotations.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/Declaration.h"
#include "language/tree/declaration/FunctionDecl.h"
#include "language/tree/module/Source.h"
#include "language/tree/module/Tangle.h"
#include "language/tree/visitor/RecursiveASTVisitor.h"

#include "language/stage/serialization/detail/ASTSerializationCommon.h"
#include "language/stage/tree_refactor/detail/TreeContextRefactorer.h"
#include "language/stage/tree_refactor/detail/UniqueNameCacher.h"

namespace zillians { namespace language { namespace stage { namespace visitor { namespace detail {

using relinkable_contexts = RelinkableSerializeContexts;
using  droppable_contexts = DroppableSerializeContexts;

namespace {

// ensure function is the last one,
// this make tree insertion easier since
// template instantiated class will be
// inserted before underlying member
/// function instantiation
constexpr int get_normalized_unqiue_name_category(tree::unique_name_category_t category) noexcept
{
    return  category == tree::unique_name_category_t::PACKAGE  ? 0 :
            category == tree::unique_name_category_t::CLASS    ? 1 :
            category == tree::unique_name_category_t::ENUM     ? 2 :
            category == tree::unique_name_category_t::FUNCTION ? 3 :
            INT_MIN + static_cast<int>(category);
}

int get_normalized_unqiue_name_category(const tree::unique_name_t& unique_name)
{
    const auto& category = tree::get_unique_name_category(unique_name);

    return get_normalized_unqiue_name_category(category);
}

std::string get_filename_of(const tree::Source& source)
{
    const auto& file_path = boost::filesystem::path(source.filename).filename();

    return file_path.generic_string();
}

std::string get_filename_of(const tree::Declaration& decl)
{
    const auto*const source = decl.getOwner<tree::Source>();
    BOOST_ASSERT(source != nullptr && "declaration is not under any source");

    return get_filename_of(*source);
}

struct unique_name_less_comparator
{
    bool operator()(const tree::Declaration* lhs, const tree::Declaration* rhs) const
    {
        NOT_NULL(lhs);
        NOT_NULL(rhs);

        const auto& lhs_unique_name = lhs->getUniqueName();
        const auto& rhs_unique_name = rhs->getUniqueName();

        const auto& lhs_category    = get_normalized_unqiue_name_category(lhs_unique_name);
        const auto& rhs_category    = get_normalized_unqiue_name_category(rhs_unique_name);

        if (lhs_category < rhs_category)
            return true;
        if (lhs_category > rhs_category)
            return false;

        return boost::lexicographical_compare(lhs_unique_name, rhs_unique_name);
    }
};

std::pair<bool, std::vector<tree::Declaration*>> resolve_conflict_decl_impl(tree::Declaration& kept_decl, tree::Declaration& conflict_decl)
{
    BOOST_ASSERT(kept_decl.rtype() == conflict_decl.rtype() && "category of nodes are different!");
    if (kept_decl.rtype() != conflict_decl.rtype())
        return {false, {}};

    if (kept_decl.arch != conflict_decl.arch)
        return {false, {}};

    if (auto*const conflict_annotations = conflict_decl.getAnnotations())
    {
        auto*const     kept_annotations =     kept_decl.getAnnotations();

        if (kept_annotations == nullptr)
        {
            conflict_decl.setAnnotations(nullptr);
                kept_decl.setAnnotations(conflict_annotations);
        }
        else
        {
            if (!kept_annotations->merge(*conflict_annotations))
                return {false, {}};
        }
    }

    if (auto*const conflict_decl_cls = tree::cast<tree::ClassDecl>(&conflict_decl))
    {
        std::vector<tree::Declaration*> dangling_decls;

        boost::push_back(dangling_decls, conflict_decl_cls->member_functions);
        conflict_decl_cls->member_functions.clear();

        return {true, std::move(dangling_decls)};
    }
    else
    {
        return {true, {}};
    }
}

template<typename derived_type>
class fallback_links_visitor : public tree::visitor::RecursiveASTVisitor<derived_type>
{
    using base_visitor = tree::visitor::RecursiveASTVisitor<derived_type>;

    friend base_visitor;

private:
    bool traverseSource(tree::Source& node)
    {
        // ignore imported packages
        if (node.is_imported)
            return true;

        return base_visitor::traverseSource(node);
    }

    bool visitASTNode(tree::ASTNode& node)
    {
        fallback_relinkable_contexts<relinkable_contexts>(node);

        return base_visitor::visitASTNode(node);
    }

    bool visitTangle(tree::Tangle& node)
    {
        fallback_link_data_to_id_map(node, node.data_to_func_id);
        fallback_link_data_to_id_map(node, node.data_to_type_id);

        return base_visitor::visitTangle(node);
    }

    bool visitExpression(tree::Expression& node)
    {
        fallback_link_resolved_type(node);

        return base_visitor::visitExpression(node);
    }

    bool visitDeclaration(tree::Declaration& node)
    {
        fallback_link_resolved_type(node);

        return base_visitor::visitDeclaration(node);
    }

    bool visitClassDecl(tree::ClassDecl& node)
    {
        fallback_link_virtual_table(node);

        return base_visitor::visitClassDecl(node);
    }

    bool visitTypeSpecifier(tree::TypeSpecifier& node)
    {
        fallback_link_resolved_type(node);

        return base_visitor::visitTypeSpecifier(node);
    }

    bool visitIdentifier(tree::Identifier& node)
    {
        fallback_link_resolved_type(node);

        return base_visitor::visitIdentifier(node);
    }

    bool visitType(tree::Type& node)
    {
        fallback_link_resolved_type(node);

        return base_visitor::visitType(node);
    }

private:
    template<typename contexts>
    void fallback_relinkable_contexts_impl(tree::ASTNode&, boost::mpl::size_t<0>) {}

    template<typename contexts, std::size_t remain_count>
    void fallback_relinkable_contexts_impl(tree::ASTNode& node, boost::mpl::size_t<remain_count>)
    {
        using current_context = typename boost::mpl::at_c<
            contexts,
            boost::mpl::size<contexts>::value - remain_count
        >::type;

        look_into(node, static_cast<const current_context*const>(nullptr));

        fallback_relinkable_contexts_impl<contexts>(node, boost::mpl::size_t<remain_count - 1>());
    }

    template<typename contexts>
    void fallback_relinkable_contexts(tree::ASTNode& node)
    {
        fallback_relinkable_contexts_impl<contexts>(node, boost::mpl::size_t<boost::mpl::size<contexts>::value>());
    }

    template<
        typename T,
        typename std::enable_if<
            boost::mpl::contains<boost::mpl::vector<ResolvedPackage, InstantiatedFrom, DefaultValueFrom>, T>::value
        >::type * = nullptr
    >
    void look_into(tree::ASTNode& node, const T*const)
    {
        auto*const context = node.get<T>();

        if (context == nullptr)
            return;

        fallback_link(node, context->value);
    }

    void look_into(tree::ASTNode& node, const ResolvedSymbol*const)
    {
        auto*const context = node.get<ResolvedSymbol>();

        if (context == nullptr)
            return;

        auto& node_relinkable = context->value;

        BOOST_ASSERT(node_relinkable.is_ptr() && "fallback more than once!?");

        if (node_relinkable == nullptr)
            return;

        BOOST_ASSERT(
            (tree::isa<tree::Declaration, tree::StringLiteral>(node_relinkable)) &&
            "resolved symbol could set to declaration or string literal only"
            ", please take care of cross bundle issue if there is new target node"
        );

        auto decl_relinkable = tree::make_relinkable(tree::cast<tree::Declaration>(node_relinkable));

        // StringLiteral should not be fallbacked
        if (decl_relinkable == nullptr)
            return;

        if (fallback_link(node, decl_relinkable))
            node_relinkable = std::move(decl_relinkable);
    }

    void look_into(tree::ASTNode& node, const InstantiatedAs*const)
    {
        auto*const context = node.get<InstantiatedAs>();

        if (context == nullptr)
            return;

        for (auto i = boost::begin(context->value), iend = boost::end(context->value); i != iend; )
        {
            auto  key   = i->first;
            auto& value = i->second;

            if (value.is_ptr())
                fallback_link(node, value);

            bool is_key_fallbacked = false;

            for (auto& sub_key : key)
                // there is case that sub key is inserted during iteration
                if (sub_key.is_ptr() && fallback_link(node, sub_key))
                    is_key_fallbacked = true;

            if (is_key_fallbacked)
            {
                context->value.emplace(std::move(key), std::move(value));
                i = context->value.erase(i);
            }
            else
            {
                ++i;
            }
        }
    }

    template<typename node_type>
    bool fallback_link(tree::ASTNode& node, tree::relinkable_ptr<node_type>& relinkable)
    {
        BOOST_ASSERT(relinkable.is_ptr() && "fallbacking non pointer!?");

        if (relinkable == nullptr)
            return false;

        if (!need_fallback(node, *relinkable))
            return false;

        relinkable = relinkable->getUniqueName();

        return true;
    }

    template<typename node_type>
    bool fallback_link_resolved_type(node_type& node)
    {
        auto& relinkable = node.resolved_type;

        return fallback_link(node, relinkable);
    }

    template<typename node_type, typename value_type>
    bool fallback_link_data_to_id_map(tree::ASTNode& node, std::map<tree::relinkable_ptr<const node_type>, value_type>& data_to_id)
    {
              bool  has_fallback_any = false;
              auto  i                = boost::begin(data_to_id);
        const auto& iend             = boost::end  (data_to_id);

        while (i != iend)
        {
                  auto  key               = i->first;
            const auto& is_key_fallbacked = key.is_ptr() && fallback_link(node, key);

            if (is_key_fallbacked)
            {
                data_to_id.emplace(std::move(key), std::move(i->second));

                i                = data_to_id.erase(i);
                has_fallback_any = true;
            }
            else
            {
                ++i;
            }
        }

        return has_fallback_any;
    }

    bool fallback_link_virtual_table(tree::ClassDecl& node)
    {
        bool  has_fallback_any = false;
        auto& vtable           = node.vtable;

        for (auto& segment : vtable.segments)
        {
            has_fallback_any |= fallback_link(node, segment.source_class);

            for (auto& entry : segment.entries)
            {
                for (auto& parameter_type : entry.signature.parameter_types)
                    has_fallback_any |= fallback_link(node, parameter_type);

                for (auto& overriden_decl : entry.overriden_decls)
                    has_fallback_any |= fallback_link(node, overriden_decl);
            }
        }

        for (auto i = boost::begin(vtable.override_states), iend = boost::end(vtable.override_states); i != iend; )
        {
            auto key               = i->first;
            bool is_key_fallbacked = false;

            for (auto& parameter_type : key.parameter_types)
                // there is case that sub key is inserted during iteration
                if (parameter_type.is_ptr() && fallback_link(node, parameter_type))
                    is_key_fallbacked = true;

            if (is_key_fallbacked)
            {
                vtable.override_states.emplace(std::move(key), std::move(i->second));
                i = vtable.override_states.erase(i);
                has_fallback_any = true;
            }
            else
            {
                ++i;
            }
        }

        for (auto i = boost::begin(vtable.function_indices), iend = boost::end(vtable.function_indices); i != iend; )
        {
            auto key               = i->first;
            bool is_key_fallbacked = key.is_ptr() && fallback_link(node, key);

            if (is_key_fallbacked)
            {
                vtable.function_indices.emplace(std::move(key), std::move(i->second));
                i = vtable.function_indices.erase(i);
                has_fallback_any = true;
            }
            else
            {
                ++i;
            }
        }

        return has_fallback_any;
    }

    bool need_fallback(tree::ASTNode& node, const tree::ASTNode& referred_node) const
    {
        const auto& derived = base_visitor::getDerived();

        return derived.need_fallback_impl(node, referred_node);
    }
};

class fallback_cross_bundle_links_visitor : public fallback_links_visitor<fallback_cross_bundle_links_visitor>
{
    using base_visitor = fallback_links_visitor<fallback_cross_bundle_links_visitor>;
    friend base_visitor;

private:
    bool need_fallback_impl(tree::ASTNode&, const tree::ASTNode& referred_node) const
    {
        const auto*const source = referred_node.getOwner<tree::Source>();

        if (source == nullptr)
            return true;
        else
            return source->is_imported;
    }
};

class fallback_cross_function_links_visitor : public fallback_links_visitor<fallback_cross_function_links_visitor>
{
    using base_visitor = fallback_links_visitor<fallback_cross_function_links_visitor>;
    friend base_visitor;

private:
    bool need_fallback_impl(tree::ASTNode& node, const tree::ASTNode& referred_node) const
    {
        // possible referred node
        // 1. type        -> under internal/declaration
        // 2. class       -> under source
        // 3. function    -> under source/class
        // 4. variable    -> under source/class/function
        // 5. package     -> under tangle/package

        // so, if referred node
        // 1. under internal       -> fallback
        // 2. under tangle/package -> fallback
        // 3. under source         -> fallback
        // 4. under function       -> fallback if node is not under the same function
        // 5. under class          -> fallback if node is under function or not under the same class

        const auto*const referred_node_owner = referred_node.findFromThisToParents<tree::ClassDecl, tree::FunctionDecl>();

        if (referred_node_owner == nullptr)
            return true;

        const auto*const node_owner = node.findFromThisToParents<tree::ClassDecl, tree::FunctionDecl>();
        BOOST_ASSERT(node_owner != nullptr && "node should under class or function by current implementation");

        return referred_node_owner != node_owner;
    }
};

// FIXME verify declarations produced by flow, lambda, async (remote or not) has correctly set 'may_conflict' flag.
class may_conflict_visitor : public tree::visitor::RecursiveASTVisitor<may_conflict_visitor>
{
    using base_visitor = tree::visitor::RecursiveASTVisitor<may_conflict_visitor>;
    friend base_visitor;

public:
    bundled_ast::grouped_conflict_decl_map_type steal_grouped_may_conflict_decls()
    {
        return std::move(grouped_may_conflict_decls);
    }

    bool traverse(tree::ASTNode& node)
    {
        const auto& need_traverse = tree::isa<
            tree::Tangle   ,
            tree::Package  ,
            tree::Source   ,
            tree::ClassDecl
        >(node);

        if (!need_traverse)
            return true;

        return base_visitor::traverse(node);
    }

private:
    bool traverseTangle(tree::Tangle& node)
    {
        if (auto*const root = node.root)
            if (!traverse(*root))
                return false;

        return walkUpFromTangle(node);
    }

    bool traversePackage(tree::Package& node)
    {
        for (auto*const source : node.sources)
            if (source != nullptr)
                if (!traverse(*source))
                    return false;

        for (auto*const child : node.children)
            if (child != nullptr)
                if (!traverse(*child))
                    return false;

        return walkUpFromPackage(node);
    }

    bool traverseSource(tree::Source& node)
    {
        for (auto*const declare : node.declares)
            if (declare != nullptr)
                if (!traverse(*declare))
                    return false;

        if (!collect(node.declares))
            return false;

        return walkUpFromSource(node);
    }

    bool traverseClassDecl(tree::ClassDecl& node)
    {
        if (!collect(node.member_functions))
            return false;

        return walkUpFromClassDecl(node);
    }

    template<typename range_type>
    bool collect(range_type& decls)
    {
        for (auto*& decl : decls)
        {
            NOT_NULL(decl);

            if (!decl->mayConflict())
                continue;

            grouped_may_conflict_decls[get_filename_of(*decl)].emplace_back(decl);
            decl->parent = nullptr;
            decl         = nullptr;
        }

        boost::remove_erase(decls, nullptr);

        return true;
    }

private:
    bundled_ast::grouped_conflict_decl_map_type grouped_may_conflict_decls;
};

class drop_context_visitor : public tree::visitor::RecursiveASTVisitor<drop_context_visitor>
{
    using base_visitor = tree::visitor::RecursiveASTVisitor<drop_context_visitor>;
    friend base_visitor;

private:
    bool visitASTNode(tree::ASTNode& node)
    {
        drop_contexts<droppable_contexts>(node);

        return base_visitor::visitASTNode(node);
    }

    template<typename contexts>
    void drop_contexts_impl(tree::ASTNode&, boost::mpl::size_t<0>) {}

    template<typename contexts, std::size_t remain_count>
    void drop_contexts_impl(tree::ASTNode& node, boost::mpl::size_t<remain_count>)
    {
        using current_context = typename boost::mpl::at_c<
            contexts,
            boost::mpl::size<contexts>::value - remain_count
        >::type;

        node.reset<current_context>();

        drop_contexts_impl<contexts>(node, boost::mpl::size_t<remain_count - 1>());
    }

    template<typename contexts>
    void drop_contexts(tree::ASTNode& node)
    {
        drop_contexts_impl<contexts>(node, boost::mpl::size_t<boost::mpl::size<contexts>::value>());
    }
};

// TODO speed up by ignoring useless traversal
template<typename map_type>
class dirty_declaration_insertion_visitor : public tree::visitor::RecursiveASTVisitor<dirty_declaration_insertion_visitor<map_type>>
{
    using base_visitor = tree::visitor::RecursiveASTVisitor<dirty_declaration_insertion_visitor<map_type>>;
    friend base_visitor;

public:
    explicit dirty_declaration_insertion_visitor(map_type&& mapping)
        : mapping(std::move(mapping))
    {
    }

    bool is_done() const
    {
        return mapping.empty();
    }

private:
    bool traverseSource(tree::Source& node)
    {
        if (!try_insert_back(node))
            return false;

        return base_visitor::traverseSource(node);
    }

    bool traverseClassDecl(tree::ClassDecl& node)
    {
        if (!try_insert_back(node))
            return false;

        return base_visitor::traverseClassDecl(node);
    }

private:
    template<typename node_type>
    bool try_insert_back(node_type& node)
    {
        using boost::adaptors::indirected;
        using boost::adaptors::map_values;

        const auto& first_level_key = get_first_level_key(node);

        if (first_level_key.second == nullptr)
            return false;

        const auto& first_level_pos = mapping.find({first_level_key.first, *first_level_key.second});

        if (first_level_pos == mapping.end())
            return true;

        const auto& second_level_key = get_second_level_key(node);

        if (second_level_key.empty())
            return false;

              auto& grouped_decls = first_level_pos->second.right;
        const auto& decls_info    = grouped_decls.equal_range(second_level_key);

        if (boost::empty(decls_info))
            return true;

        for (tree::Declaration& decl : decls_info | map_values | indirected)
            if (!insert_into(node, decl))
                return false;

        grouped_decls.erase(boost::begin(decls_info), boost::end(decls_info));

        if (grouped_decls.empty())
            mapping.erase(first_level_pos);

        return true;
    }

    std::pair<int, const tree::unique_name_t*> get_first_level_key(const tree::Source& node)
    {
        auto*const package = node.getOwner<tree::Package>();
        BOOST_ASSERT(package != nullptr && "source is not under package");

        if (package == nullptr)
            return {get_normalized_unqiue_name_category(tree::unique_name_category_t::NONE), nullptr};

        const auto& unique_name = package->getUniqueName();
        const auto& category    = get_normalized_unqiue_name_category(tree::unique_name_category_t::PACKAGE);

        return {category, &unique_name};
    }

    std::pair<int, const tree::unique_name_t*> get_first_level_key(const tree::ClassDecl& node)
    {
        const auto& unique_name = node.getUniqueName();
        const auto& category    = get_normalized_unqiue_name_category(tree::unique_name_category_t::CLASS);

        return {category, &unique_name};
    }

    std::string get_second_level_key(const tree::Source& node)
    {
        return get_filename_of(node);
    }

    std::string get_second_level_key(const tree::ClassDecl& node)
    {
        auto*const source = node.getOwner<tree::Source>();
        BOOST_ASSERT(source != nullptr && "class is not under source");

        if (source == nullptr)
            return {};

        return get_filename_of(*source);
    }

    bool insert_into(tree::Source& node, tree::Declaration& decl)
    {
        using boost::adaptors::transformed;

        const auto& has_duplicated = boost::algorithm::any_of(
            node.declares | transformed(std::mem_fn(&tree::Declaration::getUniqueName)),
            [&decl](const tree::unique_name_t& another_name)
            {
                return another_name == decl.getUniqueName();
            }
        );
        BOOST_ASSERT(!has_duplicated && "duplicated declaration!");
        if (has_duplicated)
            return false;

        if (auto*const func_decl = tree::cast<tree::FunctionDecl>(&decl))
            if (func_decl->is_member)
                return false;

        if (auto*const var_decl = tree::cast<tree::VariableDecl>(&decl))
            if (var_decl->is_member)
                return false;

        decl.parent = &node;
        node.declares.emplace_back(&decl);

        return true;
    }

    bool insert_into(tree::ClassDecl& node, tree::Declaration& decl)
    {
        auto*const func_decl = tree::cast<tree::FunctionDecl>(&decl);

        if (func_decl == nullptr)
            return false;

        if (!func_decl->is_member)
            return false;

        const auto& duplicated_pos = boost::find_if(
            node.member_functions,
            [&decl](const tree::Declaration* another_decl)
            {
                return another_decl->getUniqueName() == decl.getUniqueName();
            }
        );

        if (duplicated_pos == boost::end(node.member_functions))
        {
            func_decl->parent = &node;
            node.member_functions.emplace_back(func_decl);

            return true;
        }

        const auto& merge_result   = resolve_conflict_decl_impl(**duplicated_pos, decl);
        const auto& is_merged      = merge_result.first;
        const auto& dangling_decls = merge_result.second;

        BOOST_ASSERT(is_merged && "merge failure");
        BOOST_ASSERT(dangling_decls.empty() && "function should not produce dangling decls");

        if (!is_merged || !dangling_decls.empty())
            return false;

        return true;
    }

private:
    map_type mapping;
};

class unique_name_cacher_visitor : public tree::visitor::RecursiveASTVisitor<unique_name_cacher_visitor>
{
    using base_visitor = tree::visitor::RecursiveASTVisitor<unique_name_cacher_visitor>;
    friend base_visitor;

public:
    explicit unique_name_cacher_visitor(tree::Internal& internal)
        : cacher(internal)
    {
    }

    unique_name_cacher steal_cacher()
    {
        return std::move(cacher);
    }

    bool traverse(tree::ASTNode& node)
    {
        const auto& need_traversal = tree::isa<
            tree::Tangle ,
            tree::Package,
            tree::Source ,

            tree::Declaration,
            tree::Type       ,

            tree::TemplatedIdentifier
        >(node);

        if (need_traversal)
            return base_visitor::traverse(node);
        else
            return true;
    }

private:
    bool traverseTangle(tree::Tangle& node)
    {
        if (auto*const root = node.root)
            if (!traverse(*root))
                return false;

        return walkUpFromTangle(node);
    }

    bool traversePackage(tree::Package& node)
    {
        for (auto*const source : node.sources)
            if (source != nullptr)
                if (!traverse(*source))
                    return false;

        for (auto*const child : node.children)
            if (child != nullptr)
                if (!traverse(*child))
                    return false;

        return walkUpFromPackage(node);
    }

    bool traverseSource(tree::Source& node)
    {
        for (auto*const declare : node.declares)
            if (declare != nullptr)
                if (!traverse(*declare))
                    return false;

        return walkUpFromSource(node);
    }

    bool traverseClassDecl(tree::ClassDecl& node)
    {
        if (auto*const name = node.name)
            if (!traverse(*name))
                return false;

        for (auto*const member_function : node.member_functions)
            if (member_function != nullptr)
                if (!traverse(*member_function))
                    return false;

        for (auto*const member_variable : node.member_variables)
            if (member_variable != nullptr)
                if (!traverse(*member_variable))
                    return false;

        if (auto*const type = node.getType())
            if (!traverse(*type))
                return false;

        return walkUpFromClassDecl(node);
    }

    bool traverseFunctionDecl(tree::FunctionDecl& node)
    {
        if (auto*const name = node.name)
            if (!traverse(*name))
                return false;

        for (auto*const parameter : node.parameters)
            if (parameter != nullptr)
                if (!traverse(*parameter))
                    return false;

        return walkUpFromFunctionDecl(node);
    }

    bool traverseTypedefDecl(tree::TypedefDecl& node)
    {
        if (auto*const type = node.getType())
            if (!traverse(*type))
                return false;

        return walkUpFromTypedefDecl(node);
    }

    bool traverseTypenameDecl(tree::TypenameDecl& node)
    {
        if (auto*const type = node.getType())
            if (!traverse(*type))
                return false;

        return walkUpFromTypenameDecl(node);
    }

    bool traverseVariableDecl(tree::VariableDecl& node)
    {
        return walkUpFromVariableDecl(node);
    }

    bool traverseTemplatedIdentifier(tree::TemplatedIdentifier& node)
    {
        for (auto*const decl : node.templated_type_list)
            if (decl != nullptr)
                if (!traverse(*decl))
                    return false;

        return walkUpFromTemplatedIdentifier(node);
    }

    bool visitDeclaration(tree::Declaration& node)
    {
        if (!node.getUniqueName().empty())
        {
            const auto& is_registered = cacher.register_decl(node);

            BOOST_ASSERT(is_registered && "duplicated declaration!?");
            if (!is_registered)
                return false;
        }

        return base_visitor::visitDeclaration(node);
    }

    bool visitPackage(tree::Package& node)
    {
        if (!node.getUniqueName().empty())
        {
            const auto& is_registered = cacher.register_package(node);

            BOOST_ASSERT(is_registered && "duplicated declaration!?");
            if (!is_registered)
                return false;
        }

        return base_visitor::visitPackage(node);
    }

    bool visitType(tree::Type& node)
    {
        if (!node.getUniqueName().empty())
        {
            const auto& is_registered = cacher.register_type(node);

            BOOST_ASSERT(is_registered && "duplicated declaration!?");
            if (!is_registered)
                return false;
        }

        return base_visitor::visitType(node);
    }

private:
    unique_name_cacher cacher;
};

class rebuild_links_visitor : public tree::visitor::RecursiveASTVisitor<rebuild_links_visitor>
{
    using base_visitor = tree::visitor::RecursiveASTVisitor<rebuild_links_visitor>;
    friend base_visitor;

public:
    rebuild_links_visitor(const unique_name_cacher&  cacher) : cacher(          cacher ), failure_count(0) {}
    rebuild_links_visitor(      unique_name_cacher&& cacher) : cacher(std::move(cacher)), failure_count(0) {}

    bool     has_failure      () const noexcept { return failure_count != 0; }
    unsigned get_failure_count() const noexcept { return failure_count     ; }

private:
    bool visitASTNode(tree::ASTNode& node)
    {
        follow_contexts<relinkable_contexts>(node);

        return base_visitor::visitASTNode(node);
    }

    bool visitTangle(tree::Tangle& node)
    {
        rebuild_link_data_to_id_map(node, node.data_to_func_id);
        rebuild_link_data_to_id_map(node, node.data_to_type_id);

        return base_visitor::visitTangle(node);
    }

    bool visitExpression(tree::Expression& node)
    {
        rebuild_link_resolved_type(node);

        return base_visitor::visitExpression(node);
    }

    bool visitDeclaration(tree::Declaration& node)
    {
        rebuild_link_resolved_type(node);

        return base_visitor::visitDeclaration(node);
    }

    bool visitClassDecl(tree::ClassDecl& node)
    {
        rebuild_link_virtual_table(node);

        return base_visitor::visitClassDecl(node);
    }

    bool visitTypeSpecifier(tree::TypeSpecifier& node)
    {
        rebuild_link_resolved_type(node);

        return base_visitor::visitTypeSpecifier(node);
    }

    bool visitIdentifier(tree::Identifier& node)
    {
        rebuild_link_resolved_type(node);

        return base_visitor::visitIdentifier(node);
    }

    bool visitType(tree::Type& node)
    {
        rebuild_link_resolved_type(node);

        return base_visitor::visitType(node);
    }

private:
    template<typename contexts>
    void follow_contexts_impl(tree::ASTNode&, boost::mpl::size_t<0>) {}

    template<typename contexts, std::size_t remain_count>
    void follow_contexts_impl(tree::ASTNode& node, boost::mpl::size_t<remain_count>)
    {
        using current_context = typename boost::mpl::at_c<
            contexts,
            boost::mpl::size<contexts>::value - remain_count
        >::type;

        look_into(node, static_cast<const current_context*const>(nullptr));

        follow_contexts_impl<contexts>(node, boost::mpl::size_t<remain_count - 1>());
    }

    template<typename contexts>
    void follow_contexts(tree::ASTNode& node)
    {
        follow_contexts_impl<contexts>(node, boost::mpl::size_t<boost::mpl::size<contexts>::value>());
    }

    template<
        typename T,
        typename std::enable_if<
            boost::mpl::contains<boost::mpl::vector<ResolvedPackage, InstantiatedFrom, DefaultValueFrom>, T>::value
        >::type * = nullptr
    >
    void look_into(tree::ASTNode& node, const T*const)
    {
        auto*const context = node.get<T>();

        if (context == nullptr)
            return;

        rebuild_link(node, context->value);
    }

    void look_into(tree::ASTNode& node, const ResolvedSymbol*const)
    {
        auto*const context = node.get<ResolvedSymbol>();

        if (context == nullptr)
            return;

        auto& node_relinkable = context->value;

        if (!node_relinkable.is_unique_name())
            return;

        tree::relinkable_ptr<tree::Declaration> decl_relinkable{node_relinkable.get_unique_name()};

        if (rebuild_link(node, decl_relinkable))
            node_relinkable = decl_relinkable;
    }

    void look_into(tree::ASTNode& node, const InstantiatedAs*const)
    {
        auto*const context = node.get<InstantiatedAs>();

        if (context == nullptr)
            return;

        for (auto i = boost::begin(context->value), iend = boost::end(context->value); i != iend; )
        {
            auto  key   = i->first;
            auto& value = i->second;

            rebuild_link(node, value);

            bool is_key_rebuilt = true;

            for (auto& sub_key : key)
                if (rebuild_link(node, sub_key))
                    is_key_rebuilt = true;

            if (is_key_rebuilt)
            {
                context->value.emplace(std::move(key), std::move(value));
                i = context->value.erase(i);
            }
            else
            {
                ++i;
            }
        }
    }

    template<typename node_type>
    bool rebuild_link_resolved_type(node_type& node)
    {
        auto& relinkable = node.resolved_type;

        return rebuild_link(node, relinkable);
    }

    bool rebuild_link_virtual_table(tree::ClassDecl& node)
    {
        bool  has_rebuild_any = false;
        auto& vtable          = node.vtable;

        for (auto& segment : vtable.segments)
        {
            has_rebuild_any |= rebuild_link(node, segment.source_class);

            for (auto& entry : segment.entries)
            {
                for (auto& parameter_type : entry.signature.parameter_types)
                    has_rebuild_any |= rebuild_link(node, parameter_type);

                for (auto& overriden_decl : entry.overriden_decls)
                    has_rebuild_any |= rebuild_link(node, overriden_decl);
            }
        }

        for (auto i = boost::begin(vtable.override_states), iend = boost::end(vtable.override_states); i != iend; )
        {
            auto key              = i->first;
            bool is_key_rebuilded = false;

            for (auto& parameter_type : key.parameter_types)
                // there is case that sub key is inserted during iteration
                if (parameter_type.is_ptr() && rebuild_link(node, parameter_type))
                    is_key_rebuilded = true;

            if (is_key_rebuilded)
            {
                vtable.override_states.emplace(std::move(key), std::move(i->second));
                i = vtable.override_states.erase(i);
                has_rebuild_any = true;
            }
            else
            {
                ++i;
            }
        }

        for (auto i = boost::begin(vtable.function_indices), iend = boost::end(vtable.function_indices); i != iend; )
        {
            auto key              = i->first;
            bool is_key_rebuilded = key.is_ptr() && rebuild_link(node, key);

            if (is_key_rebuilded)
            {
                vtable.function_indices.emplace(std::move(key), std::move(i->second));
                i = vtable.function_indices.erase(i);
                has_rebuild_any = true;
            }
            else
            {
                ++i;
            }
        }

        return has_rebuild_any;
    }

    template<typename node_type, typename value_type>
    bool rebuild_link_data_to_id_map(tree::ASTNode& node, std::map<tree::relinkable_ptr<const node_type>, value_type>& data_to_id)
    {
              bool  has_rebuilt_any = false;
              auto  i               = boost::begin(data_to_id);
        const auto& iend            = boost::end  (data_to_id);

        while (i != iend)
        {
                  auto  key            = i->first;
            const auto& is_key_rebuilt = key.is_unique_name() && rebuild_link(node, key);

            if (is_key_rebuilt)
            {
                data_to_id.emplace(std::move(key), std::move(i->second));

                i               = data_to_id.erase(i);
                has_rebuilt_any = true;
            }
            else
            {
                ++i;
            }
        }

        return has_rebuilt_any;
    }

    template<typename node_type>
    bool rebuild_link(tree::ASTNode& node, tree::relinkable_ptr<node_type>& relinkable)
    {
        if (!relinkable.is_unique_name())
            return false;

        auto*const link = get_or_create_link(relinkable);

        if (link == nullptr)
            return ++failure_count, false;

        relinkable = link;

        return true;
    }

    template<typename decl_node, typename std::enable_if<std::is_convertible<decl_node*, const tree::Declaration*>::value>::type * = nullptr>
    decl_node* get_or_create_link(const tree::relinkable_ptr<decl_node>& relinkable)
    {
        const auto&      unique_name = relinkable.get_unique_name();
              auto*const        decl = cacher.lookup_decl(unique_name);
              auto*const casted_decl = tree::cast_or_null<decl_node>(decl);

        return casted_decl;
    }

    tree::Package* get_or_create_link(const tree::relinkable_ptr<tree::Package>& relinkable)
    {
        const auto&      unique_name = relinkable.get_unique_name();
              auto*const pkg         = cacher.lookup_package(unique_name);

        return pkg;
    }

    template<typename type_node, typename std::enable_if<std::is_convertible<type_node*, const tree::Type*>::value>::type * = nullptr>
    type_node* get_or_create_link(const tree::relinkable_ptr<type_node>& relinkable)
    {
        const auto&      unique_name = relinkable.get_unique_name();
              auto*const        type = cacher.lookup_type(unique_name);
              auto*const casted_type = tree::cast_or_null<type_node>(type);

        return casted_type;
    }

private:
    unique_name_cacher cacher;
    unsigned           failure_count;
};

bundled_ast::grouped_conflict_decl_map_type collect_decls_may_conflict(tree::Tangle& tangle)
{
    may_conflict_visitor visitor;

    const auto& is_success = visitor.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failed!");

    return visitor.steal_grouped_may_conflict_decls();
}

void fallback_cross_bundle_links(tree::Tangle& tangle)
{
    fallback_cross_bundle_links_visitor visitor;

    const auto& is_success = visitor.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failed!");
}

template<typename range_type>
void fallback_cross_function_links(const range_type& grouped_decls)
{
    using boost::adaptors::indirected;

    fallback_cross_function_links_visitor visitor;

    for (auto& decls : grouped_decls)
    {
        for (auto& decl : decls | indirected)
        {
            const auto& is_success = visitor.traverse(decl);
            BOOST_ASSERT(is_success && "traversal failed!");
        }
    }
}

template<typename range_type>
void drop_contexts(tree::Tangle& tangle, const range_type& grouped_may_conflict_decls)
{
    using boost::adaptors::indirected;

    drop_context_visitor visitor;

    const auto& is_success = visitor.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failed!");

    for (auto& may_conflict_decls : grouped_may_conflict_decls)
    {
        for (auto& may_conflict_decl : may_conflict_decls | indirected)
        {
            const auto& is_success = visitor.traverse(may_conflict_decl);
            BOOST_ASSERT(is_success && "traversal failed!");
        }
    }
}

void remove_imported_sources(tree::Tangle& tangle)
{
    class imported_sources_removal_visitor : public tree::visitor::RecursiveASTVisitor<imported_sources_removal_visitor>
    {
        using base_visitor = tree::visitor::RecursiveASTVisitor<imported_sources_removal_visitor>;
        friend base_visitor;

    private:
        bool traverseTangle(tree::Tangle& node)
        {
            const auto& is_success = base_visitor::traverseTangle(node);

            if (!is_success)
                return false;

            NOT_NULL(node.root);
            BOOST_ASSERT(
                (
                    node.root->hasChild() ||
                    node.root->hasSource()
                ) && "empty tree!? there should be at least one source..."
            );

            return true;
        }

        bool traversePackage(tree::Package& node)
        {
            const auto& is_success = base_visitor::traversePackage(node);

            if (!is_success)
                return false;

            boost::remove_erase_if(node.children, [](const tree::Package*const pkg) { return !pkg->hasChild() && !pkg->hasSource(); });
            boost::remove_erase_if(node.sources , std::mem_fn(&tree::Source::isImported)                                             );

            return true;
        }

    } visitor;

    const auto& is_success = visitor.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failed!");
}

template<typename range_type>
tree::Tangle* merge_tree(const range_type& tangles)
{
    BOOST_ASSERT(!tangles.empty() && "no any tangle!");

    tree::Tangle*const tangle        = &tangles.front();
    const auto&        other_tangles = boost::make_iterator_range(tangles, 1, 0);

    for (auto& other_tangle : other_tangles)
        other_tangle.markImported(true);

    const auto& is_success = boost::algorithm::all_of(other_tangles, std::bind(&tree::Tangle::merge, tangle, std::placeholders::_1));

    if (!is_success)
        return nullptr;

    return tangle;
}

/**
 * refuse if any of the following condition holds
 *  - not under package or class
 *  - conflict and different kind of declaration
 *  - conflict and from different sources
 *  - conflict and fail to merge
 */
template<typename map_type>
std::pair<bool, std::vector<tree::Declaration*>> resolve_conflict_decl(const std::string& source_name, tree::Declaration& may_conflict_decl, map_type& grouped_non_conflicted_decls)
{
    const auto& unique_name       = may_conflict_decl.getUniqueName();
          auto  owner_unique_name = tree::extract_owner_unique_name(unique_name);
    const auto& owner_category    = tree::get_unique_name_category(owner_unique_name);

    constexpr auto category_pkg = tree::unique_name_category_t::PACKAGE;
    constexpr auto category_cls = tree::unique_name_category_t::CLASS  ;

    BOOST_ASSERT(
        (
            owner_category == category_cls ||
            owner_category == category_pkg
        ) && "declaration (may conflict) is not under package or class!?"
    );

    // global declarations or member functions could cause conflicts by design
    if (owner_category != category_pkg && owner_category != category_cls)
        return {false, {}};

    auto  normalized_owner_category = get_normalized_unqiue_name_category(owner_category);
    auto& non_conflicted_decls      = grouped_non_conflicted_decls[{std::move(normalized_owner_category), std::move(owner_unique_name)}].left;

    const auto& insertion_result = non_conflicted_decls.insert({&may_conflict_decl, source_name});
    const auto& conflict_info    = *insertion_result.first ;
    const auto& is_inserted      =  insertion_result.second;

    if (is_inserted)
        return {true, {}};

          auto*const kept_decl           = conflict_info.first ;
    const auto&      another_source_name = conflict_info.second;

    if (another_source_name != source_name)
        return {false, {}};

    return resolve_conflict_decl_impl(*kept_decl, may_conflict_decl);
}

template<typename range_type>
bool merge_and_insert_conflict_decls(tree::Tangle& tangle, const range_type& grouped_may_conflict_decls_groups)
{
    using boost::adaptors::transformed;

    BOOST_ASSERT(!grouped_may_conflict_decls_groups.empty() && "may be one may conflict declaration range, even if it's empty!");

    using owner_key_type   = std::pair<
                                 int                , // normalized category
                                 tree::unique_name_t  // unique name
                             >;
    using owner_value_type = boost::bimaps::bimap<
                                 boost::bimaps::     set_of<tree::Declaration*, unique_name_less_comparator>, // declarations
                                 boost::bimaps::multiset_of<std::string                                    >  // source name
                             >;
    using owner_map_type   = std::map<owner_key_type, owner_value_type>;

    owner_map_type grouped_non_conflicted_decls;

    for (const auto& grouped_may_conflict_decls : grouped_may_conflict_decls_groups)
    {
        for (const auto& may_conflict_decls_info : grouped_may_conflict_decls)
        {
            const auto&                                 source_name        = std::get<0>(may_conflict_decls_info);
            const auto&                                 may_conflict_decls = std::get<1>(may_conflict_decls_info);
            std::deque<std::vector<tree::Declaration*>> all_dangling_decls;

            const auto& conflict_merger = [&grouped_non_conflicted_decls, &source_name, &all_dangling_decls](tree::Declaration*const may_conflict_decl)
            {
                NOT_NULL(may_conflict_decl);
                if (may_conflict_decl == nullptr)
                    return false;

                      auto  resolve_result = resolve_conflict_decl(source_name, *may_conflict_decl, grouped_non_conflicted_decls);
                const auto& is_success     = resolve_result.first;
                      auto& dangling_decls = resolve_result.second;

                if (!is_success)
                    return false;

                if (!dangling_decls.empty())
                    all_dangling_decls.emplace_back(std::move(dangling_decls));

                return true;
            };

            if (!boost::algorithm::all_of(may_conflict_decls, conflict_merger))
                return false;

            for (; !all_dangling_decls.empty(); all_dangling_decls.pop_front())
            {
                const auto& dangling_decls = all_dangling_decls.front();

                if (!boost::algorithm::all_of(dangling_decls, conflict_merger))
                    return false;
            }
        }
    }

    dirty_declaration_insertion_visitor<owner_map_type> visitor(std::move(grouped_non_conflicted_decls));

    const auto& is_success = visitor.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failure");

    if (!is_success)
        return false;

    if (!visitor.is_done())
        return false;

    return true;
}

unique_name_cacher create_unique_name_cache(tree::Tangle& tangle)
{
    NOT_NULL(tangle.internal);

    unique_name_cacher_visitor visitor(*tangle.internal);

    const auto& is_success = visitor.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failed!");

    return visitor.steal_cacher();
}

bool rebuild_fallback_links(tree::Tangle& tangle, unique_name_cacher&& cacher)
{
    rebuild_links_visitor rebuilder(std::move(cacher));

    const auto& is_success = rebuilder.traverse(tangle);
    BOOST_ASSERT(is_success && "traversal failed!");

    return rebuilder.has_failure();
}

}

bundled_ast::bundled_ast()
    : tangle(nullptr)
    , grouped_may_conflict_decls()
{
}

bundled_ast::bundled_ast(tree::Tangle* tangle, const grouped_conflict_decl_map_type&  grouped_may_conflict_decls) : tangle(tangle), grouped_may_conflict_decls(          grouped_may_conflict_decls ) {}
bundled_ast::bundled_ast(tree::Tangle* tangle,       grouped_conflict_decl_map_type&& grouped_may_conflict_decls) : tangle(tangle), grouped_may_conflict_decls(std::move(grouped_may_conflict_decls)) {}

bundled_ast bundled_ast::create(tree::Tangle& tangle)
{
    using boost::adaptors::map_values;

    auto grouped_may_conflict_decls = collect_decls_may_conflict(tangle);

    fallback_cross_bundle_links(tangle);
    fallback_cross_function_links(grouped_may_conflict_decls | map_values);

    remove_imported_sources(tangle);

    drop_contexts(tangle, grouped_may_conflict_decls | map_values);

    return {&tangle, std::move(grouped_may_conflict_decls)};
}

bundled_ast bundled_ast::create_empty()
{
    return {nullptr, {}};
}

bundled_ast bundled_ast::create_dummy(tree::Tangle& tangle)
{
    return {&tangle, {}};
}

tree::Tangle* bundled_ast::merge(const std::deque<bundled_ast>& asts)
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    auto*const tangle = merge_tree(asts | transformed(std::mem_fn(&bundled_ast::get_tangle)) | indirected);

    if (tangle == nullptr)
        return nullptr;

    if (!merge_and_insert_conflict_decls(*tangle, asts | transformed(std::mem_fn(&bundled_ast::get_grouped_may_conflict_decls))))
        return nullptr;

    auto cacher = create_unique_name_cache(*tangle);

    if (rebuild_fallback_links(*tangle, std::move(cacher)))
        return nullptr;

    return tangle;
}

auto bundled_ast::get_tangle                    () const noexcept -> tree::Tangle*                         { return tangle                    ; }
auto bundled_ast::get_grouped_may_conflict_decls() const noexcept -> const grouped_conflict_decl_map_type& { return grouped_may_conflict_decls; }

} } } } }
