/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>
#include <cstdint>

#include <deque>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "framework/ExecutorRemote.h"
#include "framework/ExecutorX86.h"
#include "framework/x86_detail/DomainManager.h"
#include "framework/x86_detail/Kernel.h"
#include "framework/x86_detail/Launcher.h"
#include "framework/x86_detail/ObjectManager.h"
#include "framework/x86_detail/ObjectTracker.h"
#include "framework/x86_detail/RuntimeInfo.h"

#include "thor/lang/Language.h"

namespace zillians { namespace framework { namespace proxies { namespace x86 {

namespace runtime_info = x86_detail::runtime_info;

const std::vector<std::wstring>& get_arguments()
{
    auto*const executor = runtime_info::get_executor();

    return executor->get_arguments();
}

void exit(std::int32_t exit_code)
{
    auto*const launch = runtime_info::get_launcher();

    launch->exit(exit_code);
}

void* allocate_object(std::size_t size, std::int64_t type_id)
{
    auto*const obj_manager = runtime_info::get_object_manager();

    return obj_manager->allocate(size, type_id);
}

void destroy_objects(void* ptr)
{
    using container_type = std::vector<thor::lang::Object*>;

    const std::unique_ptr<container_type> raii_ptr(reinterpret_cast<container_type*>(*reinterpret_cast<void**>(ptr)));
    auto*const                            obj_manager = runtime_info::get_object_manager();

    for (auto*const obj : *raii_ptr)
    {
        assert(obj != nullptr && "null pointer exception");

        obj->~Object();
    }

    obj_manager->deallocate(raii_ptr->size(), reinterpret_cast<void**>(raii_ptr->data()));
}

void* dyn_cast(void* ptr, std::int64_t target_type_id)
{
    auto*const obj_manager = runtime_info::get_object_manager();

    return obj_manager->dyn_cast(ptr, target_type_id);
}

std::pair<const wchar_t*, std::size_t> get_string(std::int64_t str_id)
{
    auto*const kern = runtime_info::get_kernel();

    return kern->get_string(str_id);
}

std::pair<std::unique_ptr<std::deque<std::int8_t>>, std::int64_t> steal_replication_data()
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->steal_replication_data();
}

std::int64_t add_invocation(std::int64_t func_id, void* ret_ptr)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->add_invocation(func_id, ret_ptr);
}

std::int64_t add_invocation(std::int64_t func_id, std::int64_t session_id, std::unique_ptr<std::deque<std::int8_t>>&& replication_data)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->add_invocation(func_id, session_id, std::move(replication_data));
}

std::int64_t reserve_invocation(std::int64_t func_id)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->reserve_invocation(func_id);
}

bool commit_invocation(std::int64_t inv_id)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->commit_invocation(inv_id);
}

bool abort_invocation(std::int64_t inv_id)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->abort_invocation(inv_id);
}

bool set_invocation_return_ptr(std::int64_t inv_id, void* ptr)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->set_invocation_return_ptr(inv_id, ptr);
}

#define APPEND_INVOCATION_PARAMETER_DEF(value_type)                                                      \
    std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, value_type value) \
    {                                                                                                    \
        auto*const launcher = runtime_info::get_launcher();                                              \
                                                                                                         \
        return launcher->append_invocation_parameter(inv_id, offset, value);                             \
    }

APPEND_INVOCATION_PARAMETER_DEF(bool        )
APPEND_INVOCATION_PARAMETER_DEF(std::int8_t )
APPEND_INVOCATION_PARAMETER_DEF(std::int16_t)
APPEND_INVOCATION_PARAMETER_DEF(std::int32_t)
APPEND_INVOCATION_PARAMETER_DEF(std::int64_t)
APPEND_INVOCATION_PARAMETER_DEF(float       )
APPEND_INVOCATION_PARAMETER_DEF(double      )
APPEND_INVOCATION_PARAMETER_DEF(void*       ) // for thor.lang.Object

#undef APPEND_INVOCATION_PARAMETER_DEF

bool postpone_invocation_dependency_to(std::int64_t inv_id)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->postpone_invocation_dependency_to(inv_id);
}

bool set_invocation_depend_on_count(std::int64_t inv_id, int32 count)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->set_invocation_depend_on_count(inv_id, count);
}

bool set_invocation_depend_on(std::int64_t inv_id, std::int64_t depend_on_inv_id)
{
    auto*const launcher = runtime_info::get_launcher();

    return launcher->set_invocation_depend_on(inv_id, depend_on_inv_id);
}

std::int64_t get_current_invocation_function_id()
{
    return x86_detail::launcher::get_current_invocation_function_id();
}

std::int64_t get_current_invocation_session_id()
{
    return x86_detail::launcher::get_current_invocation_session_id();
}

const char* get_current_invocation_parameters()
{
    return x86_detail::launcher::get_current_invocation_parameters();
}

std::int64_t get_type_id(::thor::lang::Object* obj)
{
    assert(obj != nullptr && "null pointer exception");

    const auto*const header = x86_detail::tracker_header_t::from_object(obj);

    return header->type_id;
}

remote_adaptor_t* get_remote_adaptor(std::int64_t func_id)
{
    auto*const kern = runtime_info::get_kernel();

    return kern->get_remote_adaptor(func_id);
}

object_creator_t* get_object_creator(std::int64_t type_id)
{
    auto*const kern = runtime_info::get_kernel();

    return kern->get_object_creator(type_id);
}

object_serializer_t* get_object_serializer(std::int64_t type_id)
{
    auto*const kern = runtime_info::get_kernel();

    return kern->get_object_serializer(type_id);
}

object_deserializer_t* get_object_deserializer(std::int64_t type_id)
{
    auto*const kern = runtime_info::get_kernel();

    return kern->get_object_deserializer(type_id);
}

std::int64_t gc_get_generation_id()
{
    auto*const obj_manager = runtime_info::get_object_manager();
    auto&      gc          = obj_manager->get_gc();

    return gc.get_epoch().real_value();
}

void gc_get_active_objects(thor::container::Vector<thor::lang::Object*>& objs)
{
    auto*const obj_manager = runtime_info::get_object_manager();

    obj_manager->get_active_objects(objs);
}

void gc_shared_root_set_add(void* ptr, x86_detail::object_manager* object_manager)
{
    if (ptr == nullptr)
        return;

    auto& gc = (object_manager == nullptr ? runtime_info::get_object_manager()->get_gc()
                                          : object_manager->get_gc());

    gc.shared_root_set_add(reinterpret_cast<void**>(ptr));
}

void gc_shared_root_set_remove(void* ptr, x86_detail::object_manager* object_manager)
{
    if (ptr == nullptr)
        return;

    auto& gc = (object_manager == nullptr ? runtime_info::get_object_manager()->get_gc()
                                          : object_manager->get_gc());

    gc.shared_root_set_remove(reinterpret_cast<void**>(ptr));
}

void gc_global_root_set_add(void* ptr)
{
    if (ptr == nullptr)
        return;

    auto*const obj_manager = runtime_info::get_object_manager();
    auto&      gc          = obj_manager->get_gc();

    gc.global_root_set_add(reinterpret_cast<void**>(ptr));
}

void gc_global_root_set_remove(void* ptr)
{
    if (ptr == nullptr)
        return;

    auto*const obj_manager = runtime_info::get_object_manager();
    auto&      gc          = obj_manager->get_gc();

    gc.global_root_set_remove(reinterpret_cast<void**>(ptr));
}

zillians::UUID domain_listen(const std::wstring& endpoint, ::thor::lang::Domain::ConnCallback* conn_cb, ::thor::lang::Domain::ConnCallback* disconn_cb, ::thor::lang::Domain::ErrCallback* err_cb)
{
    auto*const domain_manager = runtime_info::get_domain_manager();

    return domain_manager->listen(endpoint, conn_cb, disconn_cb, err_cb);
}

zillians::UUID domain_connect(const std::wstring& endpoint, ::thor::lang::Domain::ConnCallback* conn_cb, ::thor::lang::Domain::ConnCallback* disconn_cb, ::thor::lang::Domain::ErrCallback* err_cb)
{
    auto*const domain_manager = runtime_info::get_domain_manager();

    return domain_manager->connect(endpoint, conn_cb, disconn_cb, err_cb);
}

bool domain_cancel(const zillians::UUID& connection_id)
{
    auto*const domain_manager = runtime_info::get_domain_manager();

    return domain_manager->cancel(connection_id);
}

::thor::lang::Domain* get_domain_object(std::int64_t session_id)
{
    auto*const domain_manager = runtime_info::get_domain_manager();

    return domain_manager->find(session_id);
}

::thor::lang::Domain* add_domain_object(std::int64_t session_id, ::thor::lang::Domain* domain)
{
    auto*const domain_manager = runtime_info::get_domain_manager();

    return domain_manager->insert(session_id, domain);
}

} } } } // namespace zillians::framework::proxies::x86
