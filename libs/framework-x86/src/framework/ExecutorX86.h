/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_EXECUTOR_X86_H_
#define ZILLIANS_FRAMEWORK_EXECUTOR_X86_H_

#include <cstdint>

#include <deque>
#include <future>
#include <memory>
#include <string>

#include "framework/Executor.h"

namespace zillians { namespace framework {

namespace x86_detail {

class kernel;
class launcher;
class object_manager;
class domain_manager;

}

struct invocation_request;

class executor_x86 : public executor_rt
{
public:
     executor_x86(std::int32_t new_id, bool new_multi_thread);
    ~executor_x86();

    bool is_multi_thread() const noexcept;

    virtual bool initialize() override;
    virtual bool finalize() override;

    virtual const char* get_binary_file_suffix() const override;

    virtual void                             set_arguments(const std::vector<std::wstring>& new_args) override;
            const std::vector<std::wstring>& get_arguments() const noexcept; // in different architectures, arguments may be stored in different way, do not use virtual

    virtual bool call(const std::string& func_name) override;
    virtual bool call(std::int64_t func_id) override;
    virtual bool call(std::int64_t session_id, invocation_request&& request) override;

    virtual bool call_and_wait(std::int64_t       func_id  ) override;
    virtual bool call_and_wait(const std::string& func_name) override;

    virtual int  get_exit_code() override;
            void set_exit_code(std::int32_t new_exit_code); // in different architectures, exit_code needs different way to be set, do not use virtual

    x86_detail::domain_manager& get_domain_manager() const;

protected:
    virtual void do_work() override;

private:
    std::vector<std::wstring> args;

    std::promise<int> exit_code;
    bool              multi_thread;

    std::unique_ptr<x86_detail::kernel>         kern;
    std::unique_ptr<x86_detail::object_manager> obj_manager;
    std::unique_ptr<x86_detail::launcher>       launcher;
    std::unique_ptr<x86_detail::domain_manager> domain_manager;
};

} }

#endif /* ZILLIANS_FRAMEWORK_EXECUTOR_X86_H_ */
