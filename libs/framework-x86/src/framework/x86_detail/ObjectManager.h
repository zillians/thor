/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_OBJECTMANAGER_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_OBJECTMANAGER_H_

#include <cstddef>
#include <cstdint>

#include <deque>
#include <memory>
#include <mutex>
#include <thread>

#include <boost/noncopyable.hpp>

#include <log4cxx/logger.h>

#include "framework/x86_detail/GarbageCollector.h"
#include "framework/x86_detail/ObjectTracker.h"

#include "thor/container/Vector.h"
#include "thor/lang/Language.h"

namespace zillians { namespace language { namespace tree {

struct ClassDecl;

} } }

namespace zillians { namespace framework {

class executor_x86;

namespace x86_detail {

class kernel;

class object_manager : boost::noncopyable
{
public:
    explicit  object_manager(executor_x86& new_executor, kernel& new_kern);
             ~object_manager();

    bool is_verbose() const noexcept;
    void set_verbose(bool new_verbose = true) noexcept;

    void*   allocate(std::size_t size, std::int64_t type_id);
    void  deallocate(std::size_t count, void** ptrs);

    void* dyn_cast(void* ptr, std::int64_t target_type_id) const;
    void* dyn_cast(void* ptr, const language::tree::ClassDecl& target_decl) const;

    void               get_active_objects(thor::container::Vector<thor::lang::Object*>& objs);
    garbage_collector& get_gc() noexcept;

    void do_gc();

private:
    bool          verbose;
    executor_x86* executor;
    kernel*       kern;

    std::mutex                             trackers_guard;
    std::deque<std::unique_ptr<tracker_t>> trackers;

    std::unique_ptr<garbage_collector> gc;

    log4cxx::LoggerPtr logger;
};

}

} }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_OBJECTMANAGER_H_ */
