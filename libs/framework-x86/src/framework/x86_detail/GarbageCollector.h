/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_GARBAGECOLLECTOR_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_GARBAGECOLLECTOR_H_

#include <memory>
#include <mutex>
#include <thread>
#include <vector>

#include <boost/noncopyable.hpp>

#include <log4cxx/logger.h>

#include <tbb/concurrent_hash_map.h>
#include <tbb/tick_count.h>

#include "framework/x86_detail/ObjectTracker.h"

#include "thor/lang/Language.h"

namespace zillians { namespace framework { namespace x86_detail {


class kernel;
class object_manager;

using shared_track_map_t = tbb::concurrent_hash_map<tracker_header_t*, unsigned>;
using global_track_map_t = tbb::concurrent_hash_map<void**           , unsigned>;

struct gc_context_t
{
    shared_track_map_t* shared_root_set = nullptr;
    global_track_map_t* global_root_set = nullptr;
};

class garbage_collector : boost::noncopyable
{
public:
     garbage_collector(kernel& new_kern, object_manager& new_obj_manager);
    ~garbage_collector();

    epoch_t                                           get_epoch() const noexcept;
    std::unique_ptr<std::vector<thor::lang::Object*>> steal_sweeped_objs() noexcept;

    void trigger(tracker_t& tracker);

    void shared_root_set_add   (void** ptr);
    void shared_root_set_remove(void** ptr);

    void global_root_set_add   (void** ptr);
    void global_root_set_remove(void** ptr);

private:
    kernel*         kern;
    object_manager* obj_manager;

    epoch_t epoch;

    shared_track_map_t shared_root_set;
    global_track_map_t global_root_set;

    gc_context_t                     context;
    tbb::tick_count                  last_sweep_time;
    std::thread                      sweeper;
    std::mutex                       sweeper_guard;
    std::mutex                       sweeped_objs_guard;
    std::vector<thor::lang::Object*> sweeped_objs;

    log4cxx::LoggerPtr logger;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_GARBAGECOLLECTOR_H_ */
