#!/usr/bin/env python
#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

import subprocess as sp

if __name__ == '__main__':
    exe_clang = '/path/to/clang++'
    file_cpp  = 'Kernel.gd.cpp'
    file_ir   = 'Kernel.gd.s'
    buffer_ir = None

    def clang_compile():
        if sp.call([exe_clang, '-std=c++11', '-S', '-emit-llvm', file_cpp]) != 0:
            raise RuntimeError, "fail to invoke clang"

    def load_ir():
        global buffer_ir

        with open(file_ir, 'r') as ir:
            buffer_ir = ir.read()

    def save_ir():
        global buffer_ir

        with open(file_ir, 'w') as ir:
            ir.write(buffer_ir)

    class replacer(object):
        def __init__(self, old_value, new_value):
            self.old_value = old_value
            self.new_value = new_value

        def __call__(self):
            global buffer_ir

            buffer_ir = buffer_ir.replace(self.old_value, self.new_value)

    class prepender(object):
        def __init__(self, line):
            self.line = line

        def __call__(self):
            global buffer_ir

            buffer_ir = self.line + '\n' + buffer_ir

    class appender(object):
        def __init__(self, line):
            self.line = line

        def __call__(self):
            global buffer_ir

            buffer_ir = buffer_ir + self.line + '\n'

    actions = (clang_compile    ,
               load_ir          ,
               replacer('sw.bb6', 'sw.domain_on_error'     ), replacer('i64 6, label', 'i64 $__domain_on_error__$  , label'      ),
               replacer('sw.bb5', 'sw.domain_on_disconnect'), replacer('i64 5, label', 'i64 $__domain_on_disconnect__$  , label' ),
               replacer('sw.bb4', 'sw.domain_on_connect'   ), replacer('i64 4, label', 'i64 $__domain_on_connect__$  , label'    ),
               replacer('sw.bb3', 'sw.global_initialize'   ), replacer('i64 3, label', 'i64 $__global_initialization__$  , label'),
               replacer('sw.bb2', 'sw.system_initialize'   ), replacer('i64 2, label', 'i64 $__system_initialization__$  , label'),
               replacer('sw.bb1', 'sw.remote_invocation'   ), replacer('i64 1, label', 'i64 $__remote_invocation__$  , label'    ),
               replacer('sw.bb' , 'sw.destroy_objects'     ), replacer('i64 0, label', 'i64 $__object__destruction__$, label'    ),
               prepender('R"SKELETON_CODE('),
               appender (')SKELETON_CODE"'),
               save_ir)

    for action in actions:
        action()
