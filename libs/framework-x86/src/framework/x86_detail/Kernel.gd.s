R"SKELETON_CODE(
; ModuleID = 'Kernel.gd.cpp'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

define void @_Z15global_dispatchxPKcPc(i64 %function_id, i8* %parameter_ptr, i8* %store_to) uwtable {
entry:
  %function_id.addr = alloca i64, align 8
  %parameter_ptr.addr = alloca i8*, align 8
  %store_to.addr = alloca i8*, align 8
  store i64 %function_id, i64* %function_id.addr, align 8
  store i8* %parameter_ptr, i8** %parameter_ptr.addr, align 8
  store i8* %store_to, i8** %store_to.addr, align 8
  %0 = load i64* %function_id.addr, align 8
  switch i64 %0, label %sw.epilog [
    i64 $__object__destruction__$, label %sw.destroy_objects
    i64 $__remote_invocation__$  , label %sw.remote_invocation
    i64 $__system_initialization__$  , label %sw.system_initialize
    i64 $__global_initialization__$  , label %sw.global_initialize
    i64 $__domain_on_connect__$  , label %sw.domain_on_connect
    i64 $__domain_on_disconnect__$  , label %sw.domain_on_disconnect
    i64 $__domain_on_error__$  , label %sw.domain_on_error
  ]

sw.destroy_objects:                                            ; preds = %entry
  %1 = load i8** %parameter_ptr.addr, align 8
  call void @_ZN8zillians9framework7proxies3x8615destroy_objectsEPv(i8* %1)
  br label %sw.epilog

sw.remote_invocation:                                           ; preds = %entry
  call void @_ZN4thor4lang15__remoteInvokerEv()
  br label %sw.epilog

sw.system_initialize:                                           ; preds = %entry
  call void @_ZN4thor4lang12__initializeEv()
  br label %sw.epilog

sw.global_initialize:                                           ; preds = %entry
  br label %sw.epilog

sw.domain_on_connect:                                           ; preds = %entry
  call void @_ZN4thor4lang17__domainOnConnectEv()
  br label %sw.epilog

sw.domain_on_disconnect:                                           ; preds = %entry
  call void @_ZN4thor4lang20__domainOnDisconnectEv()
  br label %sw.epilog

sw.domain_on_error:                                           ; preds = %entry
  call void @_ZN4thor4lang15__domainOnErrorEv()
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %sw.domain_on_error, %sw.domain_on_disconnect, %sw.domain_on_connect, %sw.global_initialize, %sw.system_initialize, %sw.remote_invocation, %sw.destroy_objects
  ret void
}

declare void @_ZN8zillians9framework7proxies3x8615destroy_objectsEPv(i8*)

declare void @_ZN4thor4lang15__remoteInvokerEv()

declare void @_ZN4thor4lang12__initializeEv()

declare void @_ZN4thor4lang17__domainOnConnectEv()

declare void @_ZN4thor4lang20__domainOnDisconnectEv()

declare void @_ZN4thor4lang15__domainOnErrorEv()
)SKELETON_CODE"
