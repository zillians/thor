/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>

#include <functional>
#include <iterator>
#include <string>
#include <tuple>
#include <vector>

#include <boost/algorithm/cxx11/none_of.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/combine.hpp>
#include <boost/tuple/tuple.hpp>

#include <llvm/Config/config.h>
#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/IRBuilder.h>

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
#include <llvm/IR/DataLayout.h>
#else
#include <llvm/DataLayout.h>
#endif

#include "language/Architecture.h"
#include "language/GlobalConstant.h"
#include "language/context/ManglingStageContext.h"
#include "language/tree/ASTNode.h"
#include "language/tree/basic/Type.h"
#include "language/tree/declaration/FunctionDecl.h"

#include "framework/detail/GlobalOffsetsComputor.h"

#include "framework/x86_detail/GlobalDispatcherGenerator.h"

namespace zillians { namespace language { namespace tree {

class ClassDecl;

} } }

namespace zillians { namespace framework { namespace x86_detail {

using namespace language::tree;

global_dispatcher_generator::global_dispatcher_generator(
    const log4cxx::LoggerPtr&       new_logger        ,
    const detail::global_offsets_t& new_global_offsets,
    llvm::LLVMContext&              new_context       ,
    llvm::IRBuilder<>&              new_builder       ,
    llvm::Module&                   new_module
)
    : detail::global_dispatcher_generator(new_logger, language::Architecture::x86(), new_global_offsets)
    , context(new_context)
    , builder(new_builder)
    , module(new_module)
    , helper(new_context, new_module, new_builder)
    , layout(new_module.getDataLayout())
    , skeleton_sw(nullptr)
    , skeleton_sw_block_epilog(nullptr)
    , skeleton_sw_block_system_init(nullptr)
    , skeleton_sw_block_global_init(nullptr)
    , skeleton_alloca_function_id(nullptr)
    , skeleton_alloca_parameter_ptr(nullptr)
    , skeleton_alloca_store_to(nullptr)
    , skeleton_func_global_dispatch(nullptr)
    , skeleton_sw_block_system_init_original_size(0)
{
}

llvm::Function* global_dispatcher_generator::get_global_dispatcher() const noexcept
{
    return skeleton_func_global_dispatch;
}

bool global_dispatcher_generator::load_skeleton()
{
#define SYMBOL_VAR_NAME(store_to)                                                            \
    BOOST_PP_CAT(skeleton_, store_to)

#define EXPECT_IMPL(expr, store_to, overwrite_error_msg, null_ptr_error_msg)                 \
    assert(SYMBOL_VAR_NAME(store_to) == nullptr && overwrite_error_msg);                     \
    SYMBOL_VAR_NAME(store_to) = (expr);                                                      \
    assert(SYMBOL_VAR_NAME(store_to) && null_ptr_error_msg)

#define EXPECT_FUNCTION(name, store_to)                                                      \
    EXPECT_IMPL(                                                                             \
        module.getFunction(name),                                                            \
        store_to,                                                                            \
        "found multiple function \"" name "\"",                                              \
        "function \"" name "\" empty"                                                        \
    )

#define EXPECT_VALUE(value, store_to)                                                        \
    EXPECT_IMPL(                                                                             \
        value,                                                                               \
        store_to,                                                                            \
        "found multiple value for \"" #value "\"",                                           \
        "value \"" #value "\" empty"                                                         \
    )

    EXPECT_FUNCTION("_Z15global_dispatchxPKcPc", func_global_dispatch);

    for (llvm::Instruction& inst : skeleton_func_global_dispatch->getEntryBlock())
    {
        auto*const alloca_inst = llvm::dyn_cast<llvm::AllocaInst>(&inst);

        if (alloca_inst == nullptr)
            continue;

        const auto& alloca_inst_name = alloca_inst->getName();

        if (alloca_inst_name == "function_id.addr")
        {
            EXPECT_VALUE(alloca_inst, alloca_function_id);
        }
        else if (alloca_inst_name == "parameter_ptr.addr")
        {
            EXPECT_VALUE(alloca_inst, alloca_parameter_ptr);
        }
        else if (alloca_inst_name == "store_to.addr")
        {
            EXPECT_VALUE(alloca_inst, alloca_store_to);
        }
    }

    for (llvm::BasicBlock& block: *skeleton_func_global_dispatch)
    {
        for (llvm::Instruction& inst: block)
        {
            if (auto*const inst_sw = llvm::dyn_cast<llvm::SwitchInst>(&inst))
            {
                EXPECT_VALUE(inst_sw, sw);
                break;
            }
        }

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 1
        const auto& block_name = block.getName();
#else
        const auto& block_name = block.getNameStr();
#endif

        if (block_name == "sw.epilog")
        {
            EXPECT_VALUE(&block, sw_block_epilog);
        }

        else if (block_name == "sw.system_initialize")
        {
            EXPECT_VALUE(&block, sw_block_system_init);
            skeleton_sw_block_system_init_original_size = block.size();
        }

        else if (block_name == "sw.global_initialize")
        {
            EXPECT_VALUE(&block, sw_block_global_init);
        }
    }

#undef EXPECT_VALUE
#undef EXPECT_GLOBAL_VAR
#undef EXPECT_FUNCTION
#undef EXPECT_IMPL
#undef SYMBOL_VAR_NAME

    const bool is_all_found =
        skeleton_sw                   != nullptr &&
        skeleton_sw_block_epilog      != nullptr &&
        skeleton_sw_block_system_init != nullptr &&
        skeleton_sw_block_global_init != nullptr &&
        skeleton_alloca_function_id   != nullptr &&
        skeleton_alloca_parameter_ptr != nullptr &&
        skeleton_alloca_store_to      != nullptr &&
        skeleton_func_global_dispatch != nullptr
    ;

    if (!is_all_found)
        LOG4CXX_ERROR(logger, L"not all required symbol is not found in global dispatcher's skeleton");

    return is_all_found;
}

bool global_dispatcher_generator::set_global_offset(const std::string& name, std::int64_t offset)
{
    // insert instructions in front of all the instructions which are already in block when loading
    builder.SetInsertPoint(
        std::prev(skeleton_sw_block_system_init->end(), skeleton_sw_block_system_init_original_size)
    );

    auto*const llvm_type  = llvm::IntegerType::getInt64Ty(context);
    auto*const llvm_value = llvm::ConstantInt::getSigned(llvm_type, offset);
    auto*const llvm_var   = module.getOrInsertGlobal(name, llvm_type);

    builder.CreateStore(llvm_value, llvm_var);

    return true;
}

bool global_dispatcher_generator::add_global_init(const FunctionDecl& func_decl)
{
    const auto*const mangle_context = language::NameManglingContext::get(&func_decl);
    assert(mangle_context != nullptr && "null pointer exception");

    builder.SetInsertPoint(std::prev(skeleton_sw_block_global_init->end()));

    const auto&      mangled_name = mangle_context->mangled_name;
          auto*const llvm_type    = llvm::FunctionType::get(llvm::Type::getVoidTy(context), false);
          auto*const llvm_func    = llvm::cast<llvm::Function>(module.getOrInsertFunction(mangled_name, llvm_type));

    builder.CreateCall(llvm_func);

    return true;
}

bool global_dispatcher_generator::add_exported_function(boost::iterator_range<detail::func_mapping_t::right_const_iterator> exported_func_with_ids)
{
    using boost::adaptors::map_keys;
    using boost::adaptors::map_values;

    assert(!exported_func_with_ids.empty() && "no exported function!");
    assert(boost::algorithm::none_of_equal(exported_func_with_ids | map_keys, nullptr) && "null pointer exception");

    const auto&      exported_func    = *exported_func_with_ids.front().first;
          auto*const invocation_block = create_invocation_block(exported_func);

    for (const auto& id : exported_func_with_ids | map_values)
    {
        const auto&      case_block_name  = "switch.case." + std::to_string(id);
              auto*const case_block       = llvm::BasicBlock::Create(context, case_block_name, skeleton_func_global_dispatch, nullptr);
              auto*const case_value       = llvm::ConstantInt::get(llvm::IntegerType::getInt64Ty(context), id, false);

        skeleton_sw->addCase(case_value, case_block);

        builder.SetInsertPoint(case_block);
        builder.CreateBr(invocation_block);
    }

    return true;
}

bool global_dispatcher_generator::finish()
{
    return true;
}

llvm::Value* global_dispatcher_generator::create_invocation_param(llvm::Value* ptr, unsigned idx)
{
    auto*const element_ptr   = builder.CreateStructGEP(ptr, idx);
    auto*const element_value = builder.CreateLoad(element_ptr);

    return element_value;
}

std::vector<llvm::Value*> global_dispatcher_generator::create_invocation_params(const FunctionDecl& exported)
{
    using boost::adaptors::transformed;

    const auto*const exported_type = helper.getFunctionType(exported);

    std::vector<llvm::Type*> llvm_param_types{exported_type->param_begin(), exported_type->param_end()};

    NOT_NULL(skeleton_alloca_parameter_ptr);

    auto*const llvm_dummy_struct_type = llvm::StructType::get(context, llvm_param_types, true /* packed */);
    auto*const llvm_dummy_value_ptr   = builder.CreateLoad(skeleton_alloca_parameter_ptr);
    auto*const llvm_dummy_value       = builder.CreateBitCast(llvm_dummy_value_ptr, llvm_dummy_struct_type->getPointerTo());

    const auto& parameters = zero_to(llvm_param_types.size())
                           | transformed(std::bind(&global_dispatcher_generator::create_invocation_param, this, llvm_dummy_value, std::placeholders::_1))
                           ;

    return {parameters.begin(), parameters.end()};
}

llvm::BasicBlock* global_dispatcher_generator::create_invocation_block(const FunctionDecl& exported)
{
    const auto*const ctx_name_mangling = language::NameManglingContext::get(&exported);

    assert(ctx_name_mangling != nullptr && "no mangled name of exported function");

    const auto&      invocation_block_name = "do_" + ctx_name_mangling->mangled_name;
          auto*const invocation_block      = llvm::BasicBlock::Create(context, invocation_block_name, skeleton_func_global_dispatch, nullptr);

    builder.SetInsertPoint(invocation_block);

    const auto&         parameters    = create_invocation_params(exported);
    llvm::FunctionType* function_type = helper.getFunctionType(exported);
    llvm::Value*        function      = nullptr;

    assert(function_type != nullptr && "cannot get exported function type");

    if (exported.is_virtual)
    {
        assert(isa<ClassDecl>(exported.parent) && "global virtual function!?");
        assert(!parameters.empty() && "member function call without 'this'!?");

        const auto*const owner_class  = cast<ClassDecl>(exported.parent);
        const auto&      index_result = owner_class->getVirtualIndex(exported);
        const auto&      index        = index_result.first;
              auto*const this_value   = parameters.front();

        assert(index_result.second && "specified function is not recognized as virtual");

        auto*const vptr   = builder.CreateBitCast(this_value, function_type->getPointerTo()->getPointerTo()->getPointerTo());
        auto*const vtable = builder.CreateLoad(vptr, "vtable");
        auto*const vfn    = builder.CreateConstInBoundsGEP1_64(vtable, index, "vfn");

        function = builder.CreateLoad(vfn);
    }
    else
    {
        assert(module.getFunction(ctx_name_mangling->mangled_name) == nullptr && "trying to create an existed exported function.");
        function = llvm::Function::Create(function_type, llvm::GlobalValue::ExternalLinkage, ctx_name_mangling->mangled_name, &module);
    }

    NOT_NULL(exported.type);

    auto*const result_type = helper.getType(*exported.type);
    auto*const result      = builder.CreateCall(function, parameters);

    for (const auto& entry : boost::combine(boost::make_iterator_range(function_type->param_begin(), function_type->param_end()), parameters))
    {
        auto*const type  = llvm::dyn_cast<llvm::PointerType>(boost::get<0>(entry));
        auto*const value =                                   boost::get<1>(entry) ;

        if (type == nullptr)
            continue;

        assert(type->getNumContainedTypes() == 1 && "pointer type contains more than one pointee!?");
        auto*const pointee_type = type->getContainedType(0);

        if (!pointee_type->isStructTy())
            continue;

        create_root_set_removal_call(value);
    }

    if (!result->getType()->isVoidTy())
    {
        /*
         * produce equivalent code compared to the following
         * @code
         * if (store_to != nullptr)
         *     *reinterpret_cast<decltype(result)*>(store_to) = result;
         * @endcode
         */
        const auto& store_to_block_name = invocation_block_name + ".store_to";
        auto*const  store_to_block      = llvm::BasicBlock::Create(context, store_to_block_name, skeleton_func_global_dispatch, nullptr);

        auto*const  has_store_to        = builder.CreateIsNotNull(builder.CreateLoad(skeleton_alloca_store_to));

        builder.CreateCondBr(has_store_to, store_to_block, skeleton_sw_block_epilog);

        builder.SetInsertPoint(store_to_block);

        auto*const store_to_casted = builder.CreateBitCast(builder.CreateLoad(skeleton_alloca_store_to), result_type->getPointerTo(), "store_to.casted");
        builder.CreateStore(result, store_to_casted);
    }

    builder.CreateBr(skeleton_sw_block_epilog);

    return invocation_block;
}

llvm::Value* global_dispatcher_generator::create_root_set_removal_call(llvm::Value* value)
{
    llvm::Type*const         void_type = llvm::Type::getVoidTy(context);
    llvm::Type*const         int8_type = llvm::Type::getInt8PtrTy(context);
    llvm::FunctionType*const func_type = llvm::FunctionType::get(void_type, {int8_type}, false);
    llvm::Function*const     func_decl = llvm::cast<llvm::Function>(module.getOrInsertFunction(language::global_constant::ROOTSET_REMOVER, func_type));

    llvm::Value*const int8_value = builder.CreateBitCast(value, int8_type);

    return builder.CreateCall(func_decl, int8_value);
}

llvm::StructType* global_dispatcher_generator::get_struct_type(ClassDecl& cls_decl)
{
    llvm::StructType* struct_type = helper.getStructType(cls_decl, true);

    assert(struct_type != nullptr && "cannot get type of class declaration");

    return struct_type;
}

llvm::StructType* global_dispatcher_generator::get_struct_type(const Type& canonical_type)
{
    auto*const cls_decl = canonical_type.getAsClassDecl();

    if (cls_decl == nullptr)
        return nullptr;

    return get_struct_type(*cls_decl);
}

} } }
