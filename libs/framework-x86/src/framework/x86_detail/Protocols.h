/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_PROTOCOLS_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_PROTOCOLS_H_

#include <cstddef>
#include <cstdint>

#include <array>
#include <atomic>
#include <deque>
#include <functional>
#include <memory>

#include <boost/noncopyable.hpp>
#include <boost/variant/variant.hpp>

namespace zillians { namespace framework { namespace x86_detail {

struct invocation_header_t : boost::noncopyable
{
    invocation_header_t() noexcept;
    invocation_header_t(invocation_header_t&& ref) noexcept;
    invocation_header_t& operator=(invocation_header_t&& ref) noexcept;

    std::int64_t function_id;
    std::int64_t  session_id;

    bool valid;

    std::unique_ptr<std::atomic<unsigned>>   depend_on_counter;
    std::atomic<unsigned>*                 be_depended_counter;

    void*                  store_to;
    std::function<void()>* done_notifier;
};

namespace {

constexpr std::size_t SIZE_INVOCATION_PAYLOAD   = 128;
constexpr std::size_t SIZE_INVOCATION_HEADER    = sizeof(invocation_header_t);
constexpr std::size_t SIZE_INVOCATION_PARAMETER = SIZE_INVOCATION_PAYLOAD - SIZE_INVOCATION_HEADER;

}

struct invocation_t : invocation_header_t
{
    using raw_data_t = std::array<std::int8_t, SIZE_INVOCATION_PARAMETER>;

    struct remote_data_t
    {
        std::unique_ptr<std::deque<std::int8_t>> data      ; // replication data
        std::int64_t                             adaptor_id;
    };

    invocation_t() = default;
    invocation_t(invocation_t&&) = default;
    invocation_t& operator=(invocation_t&&) = default;

    boost::variant<
           raw_data_t,
        remote_data_t
    > payload;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_PROTOCOLS_H_ */
