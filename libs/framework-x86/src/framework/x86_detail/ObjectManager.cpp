/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>
#include <cstdint>

#include <algorithm>
#include <mutex>
#include <thread>

#include <boost/algorithm/cxx11/none_of.hpp>
#include <boost/range/end.hpp>

#include <log4cxx/level.h>
#include <log4cxx/logger.h>

#include "utility/MemoryUtil.h"

#include "language/tree/declaration/ClassDecl.h"

#include "framework/x86_detail/GarbageCollector.h"
#include "framework/x86_detail/ObjectManager.h"
#include "framework/x86_detail/ObjectTracker.h"
#include "framework/x86_detail/Kernel.h"

#include "thor/container/Vector.h"
#include "thor/lang/Language.h"

namespace zillians { namespace framework {

class executor_x86;
    
namespace x86_detail {

object_manager::object_manager(executor_x86& new_executor, kernel& new_kern)
    : verbose(false)
    , executor(&new_executor)
    , kern(&new_kern)
    , trackers_guard()
    , trackers()
    , gc(make_unique<garbage_collector>(new_kern, *this))
    , logger(log4cxx::Logger::getLogger("framework.x86.object_manager"))
{
    trackers.emplace_back(make_unique<tracker_t>());

    set_verbose(false);
}

object_manager::~object_manager()
{
    gc = nullptr;
}

bool object_manager::is_verbose() const noexcept
{
    return verbose;
}

void object_manager::set_verbose(bool new_verbose/* = true*/) noexcept
{
    verbose = new_verbose;

    logger->setLevel(verbose ? log4cxx::Level::getInfo() : log4cxx::Level::getError());
}

void* object_manager::allocate(std::size_t size, std::int64_t type_id)
{
    assert(!trackers.empty() && "no any tracker!?");
    assert(boost::algorithm::none_of_equal(trackers, nullptr) && "null pointer exception");
    assert(gc != nullptr && "null poitner exception");

    while (true)
    {
        std::unique_lock<std::mutex> trackers_lock(trackers_guard);
        const auto&                  tracker = trackers.back();

        trackers_lock.unlock();

        auto*const ptr = tracker->allocate(size, gc->get_epoch(), type_id);
        if (ptr != nullptr)
            return ptr;

        if (trackers_lock.try_lock())
            trackers.emplace_back(make_unique<tracker_t>(*tracker));
        else
            std::this_thread::yield();
    }

    assert(false && "unreachable code!");

    return nullptr;
}

void object_manager::deallocate(std::size_t count, void** ptrs)
{
    const std::lock_guard<std::mutex> trackers_lock(trackers_guard);

    auto& tracker = trackers.back();

    std::for_each(
        ptrs, ptrs + count,
        [&tracker](void* ptr)
        {
            return tracker->deallocate(ptr);
        }
    );
}

void* object_manager::dyn_cast(void* ptr, std::int64_t target_type_id) const
{
    assert(kern != nullptr && "null pointer exception");

    const auto*const target_decl = kern->get_class_decl(target_type_id);

    if (target_decl == nullptr)
    {
        LOG4CXX_ERROR(logger, L"cannot find target class declaration from id (" << target_type_id << L")");

        return nullptr;
    }

    return dyn_cast(ptr, *target_decl);
}

void* object_manager::dyn_cast(void* ptr, const language::tree::ClassDecl& target_decl) const
{
    assert(ptr  != nullptr && "null pointer exception");
    assert(kern != nullptr && "null pointer exception");

    const auto&      source_type_id = tracker_header_t::from_object(ptr)->type_id;
    const auto*const source_decl    = kern->get_class_decl(source_type_id);

    if (source_decl == nullptr)
    {
        LOG4CXX_ERROR(logger, L"cannot find source class declaration from id (" << source_type_id << L")");

        return nullptr;
    }

    const auto& object_layout     = kern->get_object_layout(*source_decl);
    const auto& class_offset      = object_layout.class_offset;
    const auto& target_offset_pos = class_offset.find(&target_decl);

    if (target_offset_pos == boost::const_end(class_offset))
        return nullptr;

    const auto& target_offset = target_offset_pos->second.second;

    return reinterpret_cast<std::int8_t*>(ptr) + target_offset;
}

void object_manager::get_active_objects(thor::container::Vector<thor::lang::Object*>& objs)
{
    std::unique_lock<std::mutex> trackers_lock(trackers_guard);
    const auto&                  tracker = trackers.back();

    assert(tracker != nullptr && "null pointer exception");

    for (const auto*const header : *tracker)
    {
        assert(header != nullptr && "null pointer exception");

        if (!header->is_valid)
            continue;

        objs.pushBack(reinterpret_cast<thor::lang::Object*>(header->object_ptr));
    }
}

garbage_collector& object_manager::get_gc() noexcept
{
    assert(gc != nullptr && "null pointer exception");

    return *gc;
}

void object_manager::do_gc()
{
    assert(trackers.back() != nullptr && "null pointer exception");

    auto& tracker = *trackers.back();

    gc->trigger(tracker);
}

}

} }
