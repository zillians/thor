/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>

#include <vector>

#include <boost/coroutine/stack_context.hpp>

#include "framework/x86_detail/StackPool.h"

namespace zillians { namespace framework { namespace x86_detail {

stack_pool::stack_pool()
    : stack_size(8192 * 1024)
    , allocator()
#if(BOOST_VERSION >= 105500)
    , contexts()
#else
    , stack_ptrs()
#endif
{
}

stack_pool::stack_pool(std::size_t concurrency)
    : stack_pool() // object concstruction is succeeded if delegated ctor is completed.
                   // That is, dtor will be call even if current ctor is thrown.
                   // This ensure the stack will be released even if exception is thron during stack allocation.
{
#if(BOOST_VERSION >= 105500)
    contexts.reserve(concurrency);
#else
    stack_ptrs.reserve(concurrency);
#endif

    for (; concurrency != 0; --concurrency)
    {
#if(BOOST_VERSION >= 105500)
        boost::coroutines::stack_context context;
        allocator.allocate(context, stack_size);
        contexts.emplace_back(std::move(context));
#else
        stack_ptrs.emplace_back(allocator.allocate(stack_size));
#endif
    }
}

stack_pool::~stack_pool()
{
#if(BOOST_VERSION >= 105500)
    for (auto& context : contexts)
        allocator.deallocate(context);

    contexts.clear();
#else
    for (auto*const stack_ptr : stack_ptrs)
        allocator.deallocate(stack_ptr, stack_size);

    stack_ptrs.clear();
#endif
}

std::size_t stack_pool::get_concurrency() const noexcept
{
#if(BOOST_VERSION >= 105500)
    return contexts.size();
#else
    return stack_ptrs.size();
#endif
}

std::pair<std::size_t, void*> stack_pool::get_stack(std::size_t index) const
{
#if(BOOST_VERSION >= 105500)
    assert(contexts.size() >= index && "index out of range");

    const auto& context = contexts[index];

    return {context.size, context.sp};
#else
    assert(stack_ptrs.size() < index && "index out of range");

    return {stack_size, stack_ptrs[index]};
#endif
}

} } }
