/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>

#include <memory>
#include <ostream>
#include <type_traits>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/bind.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/range/algorithm_ext/push_back.hpp> 
#include <boost/variant.hpp>

#include "framework/ExecutorRemote.h"
#include "framework/x86_detail/DomainEventHandler.h"
#include "framework/x86_detail/DomainManager.h"
#include "framework/x86_detail/GarbageCollector.h"
#include "framework/x86_detail/ObjectManager.h"
#include "network/Listener.h"
#include "network/Session.h"
#include "network/SessionFactory.h"
#include "utility/MemoryUtil.h"
#include "utility/ProtocolParser.h"
#include "utility/UUIDUtil.h"
#include "utility/UnicodeUtil.h"

#include "framework/x86_detail/RuntimeInfo.h"

namespace zillians { namespace framework { namespace x86_detail {

namespace {

template<std::size_t offset>
std::size_t append_parameters_impl(std::deque<std::int8_t>&) noexcept
{
    return offset;
}

template<std::size_t offset, typename CurrentType, typename ...RemainTypes>
std::size_t append_parameters_impl(std::deque<std::int8_t>& parameters, CurrentType&& current, RemainTypes&&... remains)
{
    using                 real_current_type = typename std::remove_reference<CurrentType>::type;
    constexpr std::size_t real_current_size = sizeof(real_current_type);

    static_assert(std::is_trivial<real_current_type>::value, "you cannot insert non-standard-layout value into invocation");
    static_assert(sizeof(std::int8_t) == sizeof(char), "std::int8_t & char should have same size");

    const auto* source = reinterpret_cast<const char*>(&current);
    boost::push_back(parameters, boost::make_iterator_range(source, source + sizeof(current)));

    return append_parameters_impl<offset + real_current_size>(parameters, std::forward<RemainTypes>(remains)...);
}

template<typename CurrentType, typename ...RemainTypes>
std::size_t set_parameters(std::deque<std::int8_t>& parameters, CurrentType&& current, RemainTypes&&... remains)
{
    return append_parameters_impl<0>(parameters, std::forward<CurrentType>(current), std::forward<RemainTypes>(remains)...);
}

} // anonymous namespace

domain_manager::domain_manager(
    executor_remote&      remote,
    object_manager&       obj_manager,
    domain_event_handler& event_handler
)
    : remote(remote)
    , obj_manager(obj_manager)
    , event_handler(event_handler)
    , session_factory()
{
}

domain_manager::~domain_manager()
{
}

// static method
UUID domain_manager::listen(
    const std::wstring&        wide_endpoint,
    domain_conn_callback_type* on_connect,
    domain_conn_callback_type* on_disconnect,
    domain_err_callback_type*  on_error
)
{
    UUID connection_id;
    bool add_uuid_success = false;

    std::tie(connection_id, add_uuid_success) = add_request(request_type::LISTEN, on_connect, on_disconnect, on_error);
    assert(add_uuid_success && "UUID collision occurs");

    const auto& endpoint = ws_to_s(wide_endpoint);
    ProtocolParser<std::string::const_iterator> parser;

    if (!parser.parse(endpoint.begin(), endpoint.end()) || !parser.port)
    {
        return notify_error(connection_id, domain_error_type::INVALID_ENDPOINT_SPEC);
    }

    const auto& transport = parser.transport;
    const auto& port      = parser.port;

    if (transport == "tcp" || transport == "tcp4")
    {
        return listen_impl(network::listener::TCP_V4, connection_id, *port);
    }
    else if (transport == "tcp6")
    {
        return listen_impl(network::listener::TCP_V6, connection_id, *port);
    }

    return notify_error(connection_id, domain_error_type::UNSUPPORTED_TRANSPORT);
}

// static method
UUID domain_manager::connect(
    const std::wstring&        wide_endpoint,
    domain_conn_callback_type* on_connect,
    domain_conn_callback_type* on_disconnect,
    domain_err_callback_type*  on_error
)
{
    UUID connection_id;
    bool add_uuid_success = false;

    std::tie(connection_id, add_uuid_success) = add_request(request_type::CONNECT, on_connect, on_disconnect, on_error);
    assert(add_uuid_success && "UUID collision occurs");

    const auto& endpoint = ws_to_s(wide_endpoint);
    ProtocolParser<std::string::const_iterator> parser;

    if (!parser.parse(endpoint.begin(), endpoint.end()) || !parser.port)
    {
        return notify_error(connection_id, domain_error_type::INVALID_ENDPOINT_SPEC);
    }

    const auto& transport = parser.transport;
    const auto& address   = parser.address;
    const auto& port      = parser.port;

    if (transport == "tcp" || transport == "tcp4")
    {
        return connect_impl(network::session::TCP_V4, connection_id, address, *port);
    }
    else if (transport == "tcp6")
    {
        return connect_impl(network::session::TCP_V6, connection_id, address, *port);
    }

    return notify_error(connection_id, domain_error_type::UNSUPPORTED_TRANSPORT);
}

UUID domain_manager::listen_impl(network::listener::transport_type type, const UUID& connection_id, std::uint16_t port)
{
    auto* const listener = create_listener_for(connection_id);
    assert(listener != nullptr && "failed on creating listener");

    auto error = listener->listen(type, port);
    if (error)
    {
        return notify_error(connection_id, domain_error_type::UNKNOWN_ERROR);
    }

    listener->async_accept(
        session_factory.create_session(),
        boost::bind(&domain_manager::handle_accepted, this, connection_id, _1, _2)
    );

    return connection_id;
}

UUID domain_manager::connect_impl(network::session::transport_type type, const UUID& connection_id, const std::string& address, std::uint16_t port)
{
    auto* const session = create_session_for(connection_id);
    assert(session != nullptr && "failed on creating session");

    session->async_connect(
        type, address, port,
        boost::bind(&domain_manager::handle_connected, this, connection_id, _1, _2)
    );

    return connection_id;
}

std::pair<UUID, bool> domain_manager::add_request(
    int                        type,
    domain_conn_callback_type* on_connect,
    domain_conn_callback_type* on_disconnect,
    domain_err_callback_type*  on_error
)
{
    auto connection_id = UUID::random();

    request_type request{
        type,
        proxies::x86::make_root_set_ptr(on_connect   , &obj_manager),
        proxies::x86::make_root_set_ptr(on_disconnect, &obj_manager),
        proxies::x86::make_root_set_ptr(on_error     , &obj_manager)
    };
    decltype(requests)::accessor accessor;
    const auto result = requests.insert(
        accessor,
        {connection_id, std::move(request)}
    );

    return {std::move(connection_id), result};
}

UUID domain_manager::notify_connect(const UUID& connection_id, network::session* session)
{
    auto* request = get_request(connection_id);
    assert(request != nullptr && "fatal error, invalid connection id");

    std::deque<std::int8_t> parameters;
    set_parameters(
        parameters,
        remote.get_session_id(session), connection_id, request->on_connect.get()
    );

    event_handler.on_connect(std::move(parameters));
    
    return connection_id;
}

UUID domain_manager::notify_disconnect(const UUID& connection_id, network::session* session)
{
    const auto*const request = get_request(connection_id);
    assert(request != nullptr && "fatal error, invalid connection id");

    std::deque<std::int8_t> parameters;
    set_parameters(
        parameters,
        remote.get_session_id(session), connection_id, request->on_disconnect.get()
    );
    obj_manager.get_gc().shared_root_set_add(reinterpret_cast<void**>(request->on_disconnect.get()));

    bool success_cleanup = close(connection_id) && drop_request(connection_id);
    assert(success_cleanup && "can not remove failure connection request");

    event_handler.on_disconnect(std::move(parameters));

    return connection_id;
}

UUID domain_manager::notify_error(const UUID& connection_id, domain_error_type error)
{
    const auto*const request = get_request(connection_id);
    assert(request != nullptr && "fatal error, invalid connection id");

    std::deque<std::int8_t> parameters;
    set_parameters(parameters,
        connection_id, error, request->on_disconnect.get()
    );
    obj_manager.get_gc().shared_root_set_add(reinterpret_cast<void**>(request->on_error.get()));

    bool success_cleanup = close(connection_id) && drop_request(connection_id);
    assert(success_cleanup && "can not remove failure connection request");

    event_handler.on_error(std::move(parameters));

    return connection_id;
}

bool domain_manager::cancel(const UUID& connection_id)
{
    return close(connection_id);
}

bool domain_manager::drop_request(const UUID& connection_id)
{
    decltype(requests)::const_accessor accessor;

    if (!requests.find(accessor, connection_id))
        return false;

    return requests.erase(accessor);
}

int domain_manager::get_request_type(const UUID& connection_id)
{
    const auto& request = get_request(connection_id);
    if (request == nullptr)
        return request_type::NOT_EXIST;

    return request->type;
}

auto domain_manager::get_request(const UUID& connection_id) -> const request_type*
{
    decltype(requests)::const_accessor accessor;
    if (!requests.find(accessor, connection_id))
        return nullptr;

    return &accessor->second;
}

network::session* domain_manager::create_session_for(const UUID& connection_id)
{
    decltype(uuid_to_session)::accessor accessor;
    if (!uuid_to_session.insert(accessor, {connection_id, session_factory.create_session()}))
        return nullptr;

    return accessor->second;
}

network::listener* domain_manager::create_listener_for(const UUID& connection_id)
{
    decltype(uuid_to_listener)::accessor accessor;
    if (!uuid_to_listener.insert(accessor, {connection_id, session_factory.create_listener()}))
        return nullptr;

    return accessor->second;
}

network::session* domain_manager::get_session(const UUID& connection_id) const
{
    decltype(uuid_to_session)::const_accessor accessor;
    if (!uuid_to_session.find(accessor, connection_id))
        return nullptr;

    return accessor->second;
}

network::listener* domain_manager::get_listener(const UUID& connection_id) const
{
    decltype(uuid_to_listener)::const_accessor accessor;
    if (!uuid_to_listener.find(accessor, connection_id))
        return nullptr;

    return accessor->second;
}

bool domain_manager::close(const UUID& connection_id)
{
    const auto request_type = get_request_type(connection_id);

    /// TODO: notify disconnect here

    switch (request_type)
    {
    case request_type::LISTEN:
        {
            // false if no such connection
            auto* listener = remove_listener(connection_id);
            return listener != nullptr && !listener->close();
        }
    case request_type::CONNECT:
        {
            // false if no such connection
            auto* session = remove_session(connection_id);
            return session != nullptr && !session->close() && remote.remove(session);
        }
    default:
        // should not reach here
        break;
    }

    assert(false && "should not reach here");
    return false;
}

void domain_manager::handle_accepted(const UUID& connection_id, network::listener* listener, const boost::system::error_code& error)
{
    if (error)
    {
        //std::cerr << "error occurs when accepting: " << error.message() << std::endl;
        listener->cancel_accept();
    }
    else
    {
        auto*const session = listener->steal_session();

        bool success_add_session = remote.add(session);
        assert(success_add_session && "cannot set local id for session(maybe it already has one?)");

        remote.receive_invocation_request(session, boost::bind(&domain_manager::handle_read_data, this, connection_id, _1, _2, _3));

        notify_connect(connection_id, session);
    }

    if (error == boost::asio::error::operation_aborted)
    {
        return;
    }

    listener->async_accept(
        session_factory.create_session(),
        boost::bind(&domain_manager::handle_accepted, this, connection_id, _1, _2)
    );
}

void domain_manager::handle_connected(const UUID& connection_id, network::session* session, const boost::system::error_code& error)
{
    if (error)
    {
        close(connection_id);
        notify_error(connection_id, domain_error_type::UNKNOWN_ERROR);
        return;
    }

    bool success_add_session = remote.add(session);
    assert(success_add_session && "cannot set local id for session(maybe it already has one?)");

    remote.receive_invocation_request(session, boost::bind(&domain_manager::handle_read_data, this, connection_id, _1, _2, _3));

    notify_connect(connection_id, session);
}

void domain_manager::handle_read_data(
    const UUID& connection_id,
    network::session* session,
    const boost::system::error_code& error,
    std::size_t /* unused */
)
{
    if (error)
    {
        notify_disconnect(connection_id, session);
        return;
    }

    remote.receive_invocation_request(session, boost::bind(&domain_manager::handle_read_data, this, connection_id, _1, _2, _3));
}

network::session* domain_manager::remove_session(const UUID& connection_id)
{
    decltype(uuid_to_session)::const_accessor accessor;
    if (!uuid_to_session.find(accessor, connection_id))
        return nullptr;

    auto*const session = accessor->second;
    uuid_to_session.erase(accessor);
    return session;
}

network::listener* domain_manager::remove_listener(const UUID& connection_id)
{
    decltype(uuid_to_listener)::const_accessor accessor;
    if (!uuid_to_listener.find(accessor, connection_id))
        return nullptr;

    auto*const listener = accessor->second;
    uuid_to_listener.erase(accessor);
    return listener;
}

auto domain_manager::find(std::int64_t session_id) const -> domain_type*
{
    decltype(session_id_to_domain)::const_accessor accessor;
    if (session_id_to_domain.find(accessor, session_id))
    {
        return accessor->second.get();
    }

    return nullptr;
}

auto domain_manager::insert(std::int64_t session_id, domain_type* domain) -> domain_type*
{
    decltype(session_id_to_domain)::accessor accessor;
    bool success = session_id_to_domain.insert(
        accessor,
        {session_id, proxies::x86::make_root_set_ptr(domain, &obj_manager)}
    );

    if (!success)
    {
        assert(false && "failed to insert domain for such session id");
        return nullptr;
    }

    return accessor->second.get();
}

} } } // namespace zillians::framework::x86_detail
