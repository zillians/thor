/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_RUNTIMEINFO_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_RUNTIMEINFO_H_

namespace zillians { namespace framework {

class executor_x86;

namespace x86_detail {

class kernel;
class launcher;
class object_manager;
class domain_manager;

// functions in following namespace work on thread local storage.
// TAKE CARE!
namespace runtime_info {

void set_thread_info(unsigned thread_index, executor_x86* executor, kernel* kern, object_manager* obj_manager, launcher* launch);

unsigned        get_thread_index();
executor_x86*   get_executor();
kernel*         get_kernel();
launcher*       get_launcher();
object_manager* get_object_manager();
domain_manager* get_domain_manager();

} } } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_RUNTIMEINFO_H_ */
