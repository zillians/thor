/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <limits>

#include "framework/ExecutorX86.h"
#include "framework/x86_detail/RuntimeInfo.h"

namespace zillians { namespace framework {

class executor_x86;

namespace x86_detail {

class kernel;
class launcher;
class object_manager;

namespace runtime_info {

namespace {

struct
{
    unsigned        thread_index = std::numeric_limits<unsigned>::max();
    executor_x86*   executor     = nullptr;
    kernel*         kern         = nullptr;
    object_manager* obj_manager  = nullptr;
    launcher*       launch       = nullptr;
} thread_local thread_info;

}

void set_thread_info(unsigned thread_index, executor_x86* executor, kernel* kern, object_manager* obj_manager, launcher* launch)
{
    thread_info.thread_index = thread_index;
    thread_info.executor     = executor    ;
    thread_info.kern         = kern        ;
    thread_info.obj_manager  = obj_manager ;
    thread_info.launch       = launch      ;
}

unsigned        get_thread_index  () { return thread_info.thread_index             ; }
executor_x86*   get_executor      () { return thread_info.executor                 ; }
kernel*         get_kernel        () { return thread_info.kern                     ; }
launcher*       get_launcher      () { return thread_info.launch                   ; }
object_manager* get_object_manager() { return thread_info.obj_manager              ; }
domain_manager* get_domain_manager() { return &get_executor()->get_domain_manager(); }

} } } }
