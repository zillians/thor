/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>

#include <condition_variable>
#include <limits>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <boost/algorithm/cxx11/any_of.hpp>
#include <boost/range/adaptor/indirected.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/equal.hpp>
#include <boost/range/begin.hpp>
#include <boost/range/end.hpp>
#include <boost/swap.hpp>

#include <tbb/tick_count.h>

#include <log4cxx/level.h>
#include <log4cxx/logger.h>

#include "utility/MemoryUtil.h"

#include "language/tree/ASTNodeHelper.h"
#include "language/tree/basic/Type.h"
#include "language/tree/declaration/ClassDecl.h"
#include "language/tree/declaration/VariableDecl.h"
#include "language/tree/module/Package.h"
#include "language/tree/module/Tangle.h"

#include "framework/x86_detail/GarbageCollector.h"
#include "framework/x86_detail/Kernel.h"
#include "framework/x86_detail/ObjectManager.h"
#include "framework/x86_detail/ObjectTracker.h"

#include "thor/lang/GarbageCollectable.h"
#include "thor/lang/Language.h"

namespace zillians { namespace framework { namespace x86_detail {

using language::tree::ClassDecl;

namespace {

constexpr unsigned GC_TRIGGER_INTERVAL = 2000;

void* cast_to_thor_lang_object(void* ptr)
{
    assert(ptr != nullptr && "null pointer exception");

    const auto**const vptr   = reinterpret_cast<const std::int64_t**>(ptr);
    const auto*const  vtable = *vptr;
    const auto        offset = vtable[-2];

    return reinterpret_cast<std::int8_t*>(ptr) + offset;
}

const std::wstring& get_package_name(const language::tree::Package& pkg)
{
    assert(pkg.id != nullptr && "null pointer exception");

    return pkg.id->name;
}

bool is_unmanaged_ptr(const ClassDecl& decl)
{
    using boost::adaptors::indirected;
    using boost::adaptors::transformed;

    assert(decl.name != nullptr && "null pointer exception");

    if (decl.name->getTemplateId() == nullptr)
        return false;

    if (decl.name->getSimpleId()->name != L"ptr_")
        return false;

    const auto& packages = language::tree::ASTNodeHelper::getParentScopePackages(&decl);

    const auto&                    names   = packages | indirected | transformed(&get_package_name);
    static const wchar_t* expected_names[] = {L"thor", L"unmanaged"};

    return boost::equal(names, expected_names);
}

std::vector<tracker_header_t*> get_gc_roots(const gc_context_t& context)
{
    using boost::adaptors::indirected;
    using boost::adaptors::map_keys;

    const auto&                    roots_range = *context.shared_root_set | map_keys;
    std::vector<tracker_header_t*> roots       = {boost::const_begin(roots_range), boost::const_end(roots_range)};

    for (auto*const ptr : *context.global_root_set | map_keys | indirected)
    {
        if (ptr == nullptr)
            continue;

        auto*const obj_ptr        = cast_to_thor_lang_object(ptr);
        auto*const object_header  = object_header_t::from_object(obj_ptr);
        auto*const tracker_header = object_header->tracker_ptr;

        assert(object_header->magic_number == object_header_t::MAGIC_VALUE && "Impossible!! Should match to magic number!!");

        roots.push_back(tracker_header);
    }

    return std::move(roots);
}

template<typename set_type>
void mark_object_recursively(log4cxx::LoggerPtr logger, epoch_t epoch, const kernel& kern, const object_manager& obj_manager, const ClassDecl* collectable_decl, tracker_header_t& header, set_type& marked_headers);

template<typename set_type>
void mark_object_members(log4cxx::LoggerPtr logger, epoch_t epoch, const kernel& kern, const object_manager& obj_manager, const ClassDecl* collectable_decl, const ClassDecl& decl, tracker_header_t& header, set_type& marked_headers)
{
    LOG4CXX_DEBUG(logger, L"[Marker] " << __FUNCTION__ << L": " << decl.name->toString());

    const auto& layout = kern.get_object_layout(decl);

    for (const auto& member : layout.member_attributes)
    {
        assert(member.first != nullptr && "null pointer exception");

        const auto*const member_var = member.first;
        const auto*const decl       = member_var->getCanonicalType()->getAsClassDecl();

        if (decl == nullptr)
            continue;

        if (is_unmanaged_ptr(*decl))
            continue;

        const auto& member_attrs  = member.second;
        const auto& member_offset = member_attrs.get<1>();
        const auto& member_size   = member_attrs.get<2>();

        assert(member_size && "member size must not be zero!!!");

        auto* const object_ptr = header.object_ptr;
        void**const member_ptr = reinterpret_cast<void**>(reinterpret_cast<std::int8_t*>(object_ptr) + member_offset);

        LOG4CXX_DEBUG(logger, L"[Marker] object address: " << object_ptr            );
        LOG4CXX_DEBUG(logger, L"[Marker] member name   : " << decl->name->toString());
        LOG4CXX_DEBUG(logger, L"[Marker] member offset : " << member_offset         );
        LOG4CXX_DEBUG(logger, L"[Marker] member size   : " << member_size           );
        LOG4CXX_DEBUG(logger, L"[Marker] member address: " << member_ptr            );

        if (*member_ptr == nullptr)
            continue;

        auto*const member_obj_ptr = cast_to_thor_lang_object(*member_ptr);
        auto*const member_header  = tracker_header_t::from_object(member_obj_ptr);
        assert(member_header != nullptr && "null pointer exception");

        mark_object_recursively(logger, epoch, kern, obj_manager, collectable_decl, *member_header, marked_headers);
    }
}

template<typename set_type>
void mark_object_data(log4cxx::LoggerPtr logger, epoch_t epoch, const kernel& kern, const object_manager& obj_manager, const ClassDecl* collectable_decl, tracker_header_t& header, set_type& marked_headers)
{
    if (collectable_decl == nullptr)
        return;

    auto*const collectable = reinterpret_cast<thor::lang::GarbageCollectable*>(obj_manager.dyn_cast(header.object_ptr, *collectable_decl));

    if (collectable == nullptr)
        return;

    thor::lang::CollectableObject contained_objects;
    collectable->getContainedObjects(&contained_objects);

    LOG4CXX_DEBUG(logger, "[Marker] " << __FUNCTION__ << ": found " << contained_objects.objects.size() << " objects inside");

    for (auto*const ptr : contained_objects.objects)
    {
        if (ptr == nullptr)
            continue;

              auto*const obj_ptr        = cast_to_thor_lang_object(ptr);
              auto*const obj_header     = object_header_t::from_object(obj_ptr);
              auto*const tracker_header = obj_header->tracker_ptr;
        const auto&      magic_number   = obj_header->magic_number;

        if (magic_number != object_header_t::MAGIC_VALUE)
            continue;

        assert(tracker_header->object_ptr == obj_ptr && "Integrity check failed");

        mark_object_recursively(logger, epoch, kern, obj_manager, collectable_decl, *tracker_header, marked_headers);
    }
}

template<typename set_type>
void mark_object_recursively(log4cxx::LoggerPtr logger, epoch_t epoch, const kernel& kern, const object_manager& obj_manager, const ClassDecl* collectable_decl, tracker_header_t& header, set_type& marked_headers)
{
    {
        const auto& insertion_result = marked_headers.insert(&header);
        const auto& is_new_header    = insertion_result.second;

        if (!is_new_header)
            return;
    }

    const auto*const class_decl = kern.get_class_decl(header.type_id);
    if (class_decl == nullptr)
    {
        assert(false && "Cannot find the specific type id");
        return;
    }

    if (epoch.marker() == header.epoch)
    {
        LOG4CXX_DEBUG(
            logger,
            L"[Marker] *Raise* object:  " << static_cast<void*>(header.object_ptr) << L" (" << class_decl->name->toString() << L") epoch advanced: new " << epoch.mutator().normalized_value()
        );

        header.epoch = epoch.mutator();
    }

    mark_object_members(logger, epoch, kern, obj_manager, collectable_decl, *class_decl, header, marked_headers);
    mark_object_data(logger, epoch, kern, obj_manager, collectable_decl, header, marked_headers);
}

void mark_objects(log4cxx::LoggerPtr logger, epoch_t epoch, const kernel& kern, const object_manager& obj_manager, const gc_context_t& context)
{
    LOG4CXX_DEBUG(logger, "[marker] " << __FUNCTION__);

          auto                  roots            = get_gc_roots(context);
    const auto*const            collectable_decl = kern.get_tangle()->findClass(L"thor.lang", L"GarbageCollectable");
    std::set<tracker_header_t*> marked_headers;

    for (auto*const root : roots)
    {
        assert(root != nullptr && "null pointer exception");

        mark_object_recursively(logger, epoch, kern, obj_manager, collectable_decl, *root, marked_headers);
    }
}

std::vector<thor::lang::Object*> sweep_objects(log4cxx::LoggerPtr logger, epoch_t epoch, const kernel& kern, const object_manager& obj_manager, tracker_t& tracker)
{
    std::vector<thor::lang::Object*> collected_objs;

    for (auto*const header : tracker)
    {
        assert(header != nullptr && "null pointer exception");

        if (!header->is_valid)
            continue;

        if (header->epoch != epoch.sweeper())
            continue;

        LOG4CXX_DEBUG(
            logger,
            L"[Sweeper] Remove garbage: " << header->object_ptr << L" (" <<
            (
                [header, &kern]
                {
                    const auto*const class_decl = kern.get_class_decl(header->type_id);
                    assert(class_decl != nullptr && "null pointer exception");

                    return class_decl->name->toString();
                }
            )() << L")"
        );

        collected_objs.emplace_back(reinterpret_cast<thor::lang::Object*>(header->object_ptr));
        header->is_valid = false;
    }

    LOG4CXX_DEBUG(logger, L"[Sweeper] =================> Recycle object count: " << collected_objs.size());

    return std::move(collected_objs);
}

template<typename map_type>
typename map_type::key_type adjust_root_set_key(void** ptr)
{
    return ptr;
}

template<>
shared_track_map_t::key_type adjust_root_set_key<shared_track_map_t>(void** ptr)
{
    assert(ptr != nullptr && "null pointer exception");

    auto*const obj_ptr = cast_to_thor_lang_object(ptr);
    auto*const header  = tracker_header_t::from_object(obj_ptr);

    return header;
}

template<typename map_type, typename key_compatible_type>
void inc_mapped_value(map_type& map, key_compatible_type&& key)
{
    typename map_type::accessor accessor;

    if (!map.insert(accessor, {std::forward<key_compatible_type>(key), 1u}))
        ++accessor->second;
}

template<typename map_type, typename key_compatible_type>
void dec_mapped_value(map_type& map, const key_compatible_type& key)
{
    typename map_type::accessor accessor;

    const auto& is_found = map.find(accessor, key);
    assert(is_found && "removing not presented object, there may be some non-paired add-to-root-set/remove-from-root-set call");

    if (--accessor->second == 0)
        map.erase(accessor);
}

template<typename map_type>
void root_set_add_impl(map_type& map, void** ptr)
{
    if (ptr == nullptr)
        return;

    auto*const adjusted_ptr = adjust_root_set_key<map_type>(ptr);
    assert(adjusted_ptr != nullptr && "null pointer exception");

    inc_mapped_value(map, adjusted_ptr);
}

template<typename map_type>
void root_set_remove_impl(map_type& map, void** ptr)
{
    if (ptr == nullptr)
        return;

    auto*const adjusted_ptr = adjust_root_set_key<map_type>(ptr);
    assert(adjusted_ptr != nullptr && "null pointer exception");

    dec_mapped_value(map, adjusted_ptr);
}

}

garbage_collector::garbage_collector(kernel& new_kern, object_manager& new_obj_manager)
    : kern(&new_kern)
    , obj_manager(&new_obj_manager)
    , epoch(3)
    , shared_root_set()
    , global_root_set()
    , context()
    , last_sweep_time(tbb::tick_count::now())
    , sweeper()
    , sweeper_guard()
    , sweeped_objs_guard()
    , sweeped_objs()
    , logger(log4cxx::Logger::getLogger("framework.x86.garbage_collector"))
{
    context.shared_root_set = &shared_root_set;
    context.global_root_set = &global_root_set;

    logger->setLevel(log4cxx::Level::getError());
}

garbage_collector::~garbage_collector()
{
    if (sweeper.joinable())
        sweeper.join();
}

epoch_t garbage_collector::get_epoch() const noexcept
{
    return epoch;
}

std::unique_ptr<std::vector<thor::lang::Object*>> garbage_collector::steal_sweeped_objs() noexcept
{
    std::lock_guard<std::mutex> lock(sweeped_objs_guard);

    if (sweeped_objs.empty())
        return nullptr;

    auto stealed_objs = make_unique<std::vector<thor::lang::Object*>>();

    boost::swap(sweeped_objs, *stealed_objs);

    return std::move(stealed_objs);
}

void garbage_collector::trigger(tracker_t& tracker)
{
    assert(kern        != nullptr && "null pointer exception");
    assert(obj_manager != nullptr && "null pointer exception");

          auto                   now      = tbb::tick_count::now();
    const auto&                  interval = (now - last_sweep_time).seconds() * 1000;
    std::unique_lock<std::mutex> lock(sweeper_guard, std::defer_lock);

    const auto& should_run =
        // FIXME need more advanced policy on GC trigger interval
        // interval > GC_TRIGGER_INTERVAL &&
        lock.try_lock()
    ;

    if (!should_run)
        return;

    if (sweeper.joinable())
        sweeper.join();

    mark_objects(logger, epoch, *kern, *obj_manager, context);

    std::condition_variable cond;
    const auto              new_epoch = epoch.next();

    LOG4CXX_DEBUG(logger, L"mutator: " << epoch.mutator().normalized_value());
    LOG4CXX_DEBUG(logger, L"marker : " << epoch.marker ().normalized_value());
    LOG4CXX_DEBUG(logger, L"sweeper: " << epoch.sweeper().normalized_value());

    epoch           = new_epoch;
    last_sweep_time = std::move(now);
    sweeper         = std::thread(
        [this, new_epoch, &cond, &tracker]
        {
            std::unique_lock<std::mutex> lock(sweeper_guard);
            cond.notify_one();

            assert(sweeper.get_id() == std::this_thread::get_id() && "sweeper is not current thread!?");

            auto new_sweeped_objs = sweep_objects(logger, new_epoch, *kern, *obj_manager, tracker);

            {
                std::lock_guard<std::mutex> lock(sweeped_objs_guard);

                sweeped_objs = std::move(new_sweeped_objs);
            }
        }
    );

    cond.wait(lock);
}

void garbage_collector::shared_root_set_add(void** ptr)
{
    root_set_add_impl(shared_root_set, ptr);
}

void garbage_collector::shared_root_set_remove(void** ptr)
{
    root_set_remove_impl(shared_root_set, ptr);
}

void garbage_collector::global_root_set_add(void** ptr)
{
    root_set_add_impl(global_root_set, ptr);
}

void garbage_collector::global_root_set_remove(void** ptr)
{
    root_set_remove_impl(global_root_set, ptr);
}

} } }
