/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_LAUNCHER_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_LAUNCHER_H_

#include <cstdint>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <boost/asio/io_service.hpp>
#include <boost/noncopyable.hpp>
#include <boost/range/irange.hpp>

#include <log4cxx/logger.h>

#include "framework/x86_detail/Protocols.h"
#include "framework/x86_detail/StackPool.h"
#include "framework/x86_detail/DomainEventHandler.h"

namespace zillians { namespace framework {

class executor_x86;

namespace x86_detail {

class kernel;
class object_manager;

class launcher : boost::noncopyable, public domain_event_handler
{
public:
     launcher(executor_x86& new_executor, kernel& new_kern, object_manager& new_obj_manager);
    ~launcher();

    bool is_verbose() const noexcept;
    void set_verbose(bool new_verbose = true) noexcept;

    bool launch_batch();

private:
    template<typename range_type>
    bool launch_range(const range_type& range);

    static void launch_one(std::intptr_t ptr);

public:
    // add/run invocations from threads which are NOT MANAGED by launcher
    bool add_invocation_from_external(const std::string& func_name);
    bool add_invocation_from_external(std::int64_t       func_id  );

    bool run_invocation_from_external(const std::string& func_name);
    bool run_invocation_from_external(std::int64_t       func_id  );

    std::int64_t push_invocation_from_external(invocation_t&& invocation);

public:
    std::int64_t     add_invocation(std::int64_t func_id, void* ret_ptr);
    std::int64_t     add_invocation(std::int64_t func_id, std::int64_t session_id, std::unique_ptr<std::deque<std::int8_t>>&& replication_data);
    std::int64_t reserve_invocation(std::int64_t func_id);
    bool          commit_invocation(std::int64_t inv_id);
    bool           abort_invocation(std::int64_t inv_id);

    bool set_invocation_return_ptr(std::int64_t inv_id, void* ptr);

    template<typename value_type>
    std::int64_t append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, value_type value);

    bool postpone_invocation_dependency_to(std::int64_t inv_id);
    bool set_invocation_depend_on_count(std::int64_t inv_id, std::int32_t count);
    bool set_invocation_depend_on(std::int64_t inv_id, std::int64_t depend_on_inv_id);

    static std::int64_t get_current_invocation_function_id();
    static std::int64_t get_current_invocation_session_id();
    static const char*  get_current_invocation_parameters();

    std::int64_t push_invocation(invocation_t&& invocation);

    std::pair<std::unique_ptr<std::deque<std::int8_t>>, std::int64_t> steal_replication_data();

    // override interface from 'domain_event_handler'
    virtual void    on_connect(std::deque<std::int8_t>&& parameters) override;
    virtual void on_disconnect(std::deque<std::int8_t>&& parameters) override;
    virtual void      on_error(std::deque<std::int8_t>&& parameters) override;

private:
    void prepare_invocations();
    void delay_invocation(invocation_t& invocation);

private:
    std::deque<invocation_t>& get_next_invocations();
    invocation_t*             get_committed_invocation(std::int64_t inv_id);
    invocation_t*             get_reserved_invocation(std::int64_t inv_id);

public:
    void exit(std::int32_t exit_code);

private:
    bool          verbose;
    executor_x86* executor;
    kernel*       kern;

    stack_pool                          stacks;
    boost::asio::io_service             service;
    std::vector<std::thread>            workers;
    std::vector<std::thread>::size_type unfinished_count;
    std::mutex                          unfinished_guard;
    std::condition_variable             unfinished_cond;

    std::atomic<bool>                    is_exited;
    std::deque<std::deque<invocation_t>> invocations_batches;

    std::vector<std::deque<invocation_t>> next_invocations_batches;

    std::mutex               invocations_from_external_guard;
    std::deque<invocation_t> invocations_from_external;

    log4cxx::LoggerPtr logger;
};

}

} }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_LAUNCHER_H_ */
