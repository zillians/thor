/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_GLOBAL_DISPATCHER_GENERATOR_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_GLOBAL_DISPATCHER_GENERATOR_H_

#include <cstdint>

#include <string>
#include <vector>

#include <llvm/Config/config.h>
#include <llvm/LLVMContext.h>
#include <llvm/Module.h>
#include <llvm/IRBuilder.h>

#if LLVM_VERSION_MAJOR >= 3 && LLVM_VERSION_MINOR >= 3
#include <llvm/IR/DataLayout.h>
#else
#include <llvm/DataLayout.h>
#endif

#include "language/tree/ASTNodeFwd.h"
#include "language/stage/generator/detail/LLVMHelper.h"

#include "framework/detail/GlobalDispatcherGenerator.h"
#include "framework/detail/GlobalOffsetsComputor.h"

namespace zillians { namespace language { namespace tree {

class ClassDecl;
class FunctionDecl;
class Type;

} } }

namespace zillians { namespace framework { namespace x86_detail {

class global_dispatcher_generator : public detail::global_dispatcher_generator
{
public:
    global_dispatcher_generator(
        const log4cxx::LoggerPtr&       new_logger        ,
        const detail::global_offsets_t& new_global_offsets,
        llvm::LLVMContext&              new_context       ,
        llvm::IRBuilder<>&              new_builder       ,
        llvm::Module&                   new_module
    );

    llvm::Function* get_global_dispatcher() const noexcept;

protected:
    virtual bool load_skeleton() override;

    virtual bool set_global_offset(const std::string& name, std::int64_t offset) override;

    virtual bool add_global_init(const FunctionDecl& func_decl) override;
    virtual bool add_exported_function(boost::iterator_range<detail::func_mapping_t::right_const_iterator> exported_func_with_ids) override;

    virtual bool finish() override;

private:
    llvm::Value*              create_invocation_param(llvm::Value* ptr, unsigned idx);
    std::vector<llvm::Value*> create_invocation_params(const language::tree::FunctionDecl& exported);
    llvm::BasicBlock*         create_invocation_block(const language::tree::FunctionDecl& exported);

    llvm::Value* create_root_set_removal_call(llvm::Value* value);

    llvm::StructType* get_struct_type(language::tree::ClassDecl& cls_decl);
    llvm::StructType* get_struct_type(const language::tree::Type& canonical_type);

private:
    llvm::LLVMContext&                   context;
    llvm::IRBuilder<>&                   builder;
    llvm::Module&                        module;
    language::stage::visitor::LLVMHelper helper;
    llvm::DataLayout                     layout;

    llvm::SwitchInst* skeleton_sw                  ;
    llvm::BasicBlock* skeleton_sw_block_epilog     ;
    llvm::BasicBlock* skeleton_sw_block_system_init;
    llvm::BasicBlock* skeleton_sw_block_global_init;
    llvm::AllocaInst* skeleton_alloca_function_id  ;
    llvm::AllocaInst* skeleton_alloca_parameter_ptr;
    llvm::AllocaInst* skeleton_alloca_store_to     ;
    llvm::Function*   skeleton_func_global_dispatch;

    std::size_t skeleton_sw_block_system_init_original_size;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_GLOBAL_DISPATCHER_GENERATOR_H_ */
