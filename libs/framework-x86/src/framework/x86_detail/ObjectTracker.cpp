/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstdlib>

#include <algorithm>

#include <boost/lockfree/queue.hpp>
#include <boost/range/iterator_range.hpp>

#include "framework/x86_detail/ObjectTracker.h"

namespace zillians { namespace framework { namespace x86_detail {

namespace {

constexpr unsigned INITIAL_OBJECT_COUNT = 1024;

}

tracker_header_t* tracker_header_t::from_object(void* ptr)
{
    auto*const obj_header = object_header_t::from_object(ptr);
    assert(obj_header != nullptr && "null pointer exception");

    return obj_header->tracker_ptr;
}

void* object_header_t::get_payload_ptr()
{
    return this + 1;
}

std::size_t object_header_t::get_size(std::size_t payload_size)
{
    return sizeof(object_header_t) + payload_size;
}

object_header_t* object_header_t::from_object(void* ptr)
{
    auto*const          byte_ptr = reinterpret_cast<std::int8_t*>(ptr);
    auto*const offseted_byte_ptr = byte_ptr - sizeof(object_header_t);
    auto*const    obj_header_ptr = reinterpret_cast<object_header_t*>(offseted_byte_ptr);

    return obj_header_ptr;
}

tracker_t::tracker_t()
    : total_count(INITIAL_OBJECT_COUNT)
    , headers(nullptr)
    , allocator(nullptr)
{
    headers   = new tracker_header_t*[total_count];
    allocator = new boost::lockfree::queue<tracker_header_t*>(total_count);

    for (tracker_header_t*& header : boost::make_iterator_range(headers, headers + total_count))
    {
        header = new tracker_header_t;

        const auto& pushed = allocator->push(header);

        assert(pushed && "cannot not push to lockfree queue!?");
    }
}

tracker_t::tracker_t(tracker_t& parent)
    : total_count(parent.total_count * 2)
    , headers(nullptr)
    , allocator(nullptr)
{
    headers   = new tracker_header_t*[total_count];
    allocator = parent.allocator;

    std::copy(parent.headers, parent.headers + parent.total_count, headers);

    for (tracker_header_t*& header : boost::make_iterator_range(headers + parent.total_count, headers + total_count))
    {
        header = new tracker_header_t;

        const auto& pushed = allocator->push(header);

        assert(pushed && "cannot not push to lockfree queue!?");
    }
}

tracker_t::~tracker_t()
{
    total_count = 0;
    allocator   = nullptr;

    for (tracker_header_t*& header : boost::make_iterator_range(headers, headers + total_count))
    {
        delete header;
        header = nullptr;
    }
}

void* tracker_t::allocate(std::size_t size, epoch_t epoch, std::int64_t type_id)
{
    tracker_header_t* tracker_header = nullptr;

    if (!allocator->pop(tracker_header))
        return nullptr;

    assert(tracker_header != nullptr && "null pointer exception");

    auto*const ptr = std::malloc(object_header_t::get_size(size));
    if (ptr == nullptr)
    {
        const auto& pushed = allocator->push(tracker_header);
        assert(pushed && "cannot push back to allocator");
        return nullptr;
    }

    auto*const obj_header  = reinterpret_cast<object_header_t*>(ptr);
    auto*const payload_ptr = obj_header->get_payload_ptr();

    obj_header->magic_number = object_header_t::MAGIC_VALUE;
    obj_header->tracker_ptr  = tracker_header;

    tracker_header->epoch      = epoch;
    tracker_header->type_id    = type_id;
    tracker_header->object_ptr = payload_ptr;
    tracker_header->is_valid   = true;

    return payload_ptr;
}

void tracker_t::deallocate(void* ptr)
{
    assert(ptr != nullptr && "null pointer exception");

    auto*const     obj_header = object_header_t::from_object(ptr);
    auto*const tracker_header = obj_header->tracker_ptr;

    assert(tracker_header != nullptr && "null pointer exception");

    if (tracker_header->object_ptr != ptr)
    {
        assert(false && "integrity check failed!");
        return;
    }

         obj_header->magic_number = 0;
    *tracker_header               = tracker_header_t();

    std::free(obj_header);

    const auto& pushed = allocator->push(tracker_header);
    assert(pushed && "unable to push header back");
}

tracker_header_t** tracker_t::begin() noexcept { return headers              ; }
tracker_header_t** tracker_t::end  () noexcept { return headers + total_count; }

} } }
