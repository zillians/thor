/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#ifndef ZILLIANS_FRAMEWORK_X86_DETAIL_OBJECTTRACKER_H_
#define ZILLIANS_FRAMEWORK_X86_DETAIL_OBJECTTRACKER_H_

#include <cstddef>
#include <cstdint>

#include <limits>

#include <boost/lockfree/queue.hpp>

namespace zillians { namespace framework { namespace x86_detail {

struct object_header_t;

class epoch_t
{
public:
                       epoch_t(                      ) = delete;
    explicit constexpr epoch_t(std::int64_t new_value) : value(new_value) {}

    epoch_t next() const noexcept
    {
        constexpr auto     epoch_max_value = std::numeric_limits<decltype(value)>::max() / 3 * 3;
        const std::int64_t epoch_plus_one  = value + 1;

        if (epoch_plus_one >= epoch_max_value)
            return epoch_t(3);
        else
            return epoch_t(epoch_plus_one);
    }

    constexpr std::int64_t       real_value() const noexcept { return value    ; }
    constexpr std::int64_t normalized_value() const noexcept { return value % 3; }

    constexpr epoch_t mutator() const noexcept { return epoch_t(value    ); }
    constexpr epoch_t marker () const noexcept { return epoch_t(value - 1); }
    constexpr epoch_t sweeper() const noexcept { return epoch_t(value - 2); }

private:
    std::int64_t value;
};

inline bool operator==(const epoch_t& lhs, const epoch_t& rhs) noexcept { return lhs.normalized_value() == rhs.normalized_value(); }
inline bool operator!=(const epoch_t& lhs, const epoch_t& rhs) noexcept { return lhs.normalized_value() != rhs.normalized_value(); }

struct tracker_header_t
{
    bool          is_valid   = false;
    epoch_t       epoch      = epoch_t(0);
    std::uint64_t type_id    = std::numeric_limits<std::uint64_t>::max();
    void*         object_ptr = nullptr;;

    static tracker_header_t* from_object(void* ptr);
};

struct object_header_t
{
    static const std::int64_t MAGIC_VALUE = 0xFFEEFFAABBEE9933;

    std::int64_t      magic_number;
    tracker_header_t* tracker_ptr;

    void* get_payload_ptr();

    static std::size_t      get_size(std::size_t payload_size);
    static object_header_t* from_object(void* ptr);
};

class tracker_t
{
public:
              tracker_t();
    explicit  tracker_t(tracker_t& parent);
             ~tracker_t();

    void*   allocate(std::size_t size, epoch_t epoch, std::int64_t type_id);
    void  deallocate(void* ptr);

    tracker_header_t** begin() noexcept;
    tracker_header_t** end  () noexcept;

private:
    std::size_t                                total_count;
    tracker_header_t**                         headers;
    boost::lockfree::queue<tracker_header_t*>* allocator;
};

} } }

#endif /* ZILLIANS_FRAMEWORK_X86_DETAIL_OBJECTTRACKER_H_ */
