/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>
#include <cstring>

#include <deque>
#include <exception>
#include <functional>
#include <iterator>
#include <mutex>
#include <limits>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include <boost/asio/io_service.hpp>
#include <boost/context/fcontext.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/algorithm/for_each.hpp>
#include <boost/range/algorithm/generate.hpp>
#include <boost/range/irange.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/variant/get.hpp>

#include <log4cxx/level.h>
#include <log4cxx/logger.h>

#include "utility/Foreach.h"
#include "utility/Functional.h"
#include "utility/MemoryUtil.h"

#include "language/SystemFunction.h"

#include "framework/ExecutorX86.h"
#include "framework/ExecutorRemote.h"

#include "framework/x86_detail/Kernel.h"
#include "framework/x86_detail/Launcher.h"
#include "framework/x86_detail/ObjectManager.h"
#include "framework/x86_detail/RuntimeInfo.h"

namespace zillians { namespace framework { namespace x86_detail {

namespace {

template<typename container_type>
std::vector<boost::iterator_range<typename container_type::iterator>> split_invocations(container_type& invocations, typename container_type::size_type worker_count)
{
    assert(invocations.size() > 0 && "no task");
    assert(worker_count       > 0 && "no worker");

    using task_container_type = std::vector<boost::iterator_range<typename container_type::iterator>>;

    const auto&                        invocation_count      = invocations.size();
    typename container_type::size_type group_count           = 0;
    typename container_type::size_type task_count_each_group = 0;
    typename container_type::size_type dangling_count        = 0;

    if (invocation_count > worker_count)
    {
        group_count           = worker_count;
        task_count_each_group = invocation_count / worker_count;
        dangling_count        = invocation_count % worker_count;
    }
    else
    {
        group_count           = invocation_count;
        task_count_each_group = 1;
        dangling_count        = 0;
    }

    assert(group_count * task_count_each_group + dangling_count == invocation_count && "group tasks count setting failure!");

    using task_range_type = boost::iterator_range<typename container_type::iterator>;

    std::vector<task_range_type> tasks(group_count);
    auto                         begin = invocations.begin();

    boost::generate(
        tasks,
        [&invocations, &begin, task_count_each_group, &dangling_count]
        {
            const auto& task_count_current_group = dangling_count ? task_count_each_group + 1 : task_count_each_group;
                  auto  next_begin               = std::next(begin, task_count_current_group);
                  auto  result                   = boost::make_iterator_range(begin, next_begin);

            if (dangling_count)
                --dangling_count;

            begin = std::move(next_begin);

            return result;
        }
    );

    assert(begin == invocations.end() && "did not assign all tasks!?");

    return tasks;
}

// implementor of launcher's overrided interfaces from 'domain_event_handler'
std::int64_t push_domain_event(x86_detail::launcher* launcher, std::int64_t system_function_id, std::deque<std::int8_t>&& parameters)
{
    invocation_t invocation;

    invocation.valid       = true;
    invocation.function_id = system_function_id;
    invocation.session_id  = -1;

    auto& raw_data     = boost::get<invocation_t::raw_data_t>(invocation.payload);
    auto* raw_data_ptr = raw_data.data();

    boost::copy(parameters, raw_data_ptr);

    return launcher->push_invocation_from_external(std::move(invocation));
}

struct invocation_state_t
{
    boost::context::fcontext_t* worker;
    boost::context::fcontext_t* caller;
    invocation_t*               invocation;

    static invocation_state_t get_invalid()
    {
        return {
            nullptr,
            nullptr,
            nullptr
        };
    }
};

thread_local invocation_state_t invocation_state = invocation_state_t::get_invalid();

} // anonymous namespace


launcher::launcher(executor_x86& new_executor, kernel& new_kern, object_manager& new_obj_manager)
    : verbose(false)
    , executor(&new_executor)
    , kern(&new_kern)
    , stacks(new_executor.is_multi_thread() ? std::thread::hardware_concurrency() : 1u)
    , service()
    , workers()
    , unfinished_count(0)
    , unfinished_guard()
    , unfinished_cond()
    , is_exited(false)
    , invocations_batches()
    , next_invocations_batches(new_executor.is_multi_thread() ? std::thread::hardware_concurrency() : 1u)
    , invocations_from_external_guard()
    , invocations_from_external()
    , logger(log4cxx::Logger::getLogger("framework.x86.launcher"))
{
    const auto& concurrency = stacks.get_concurrency();

    workers.reserve(concurrency);

    for (const auto index : zero_to(concurrency))
    {
        workers.emplace_back(
            [this, index, &new_obj_manager]
            {
                boost::asio::io_service::work dummy_work(service);

                runtime_info::set_thread_info(index, executor, kern, &new_obj_manager, this);

                while (true)
                {
                    try
                    {
                        service.run();

                        break;
                    }
                    catch (const std::exception& e)
                    {
                        LOG4CXX_ERROR(logger, "exception e: " << e.what());
                    }
                }
            }
        );
    }

    set_verbose(false);
}

launcher::~launcher()
{
    service.stop();

    for (auto& worker : workers)
        if (worker.joinable())
            worker.join();

    workers.clear();
    next_invocations_batches.clear();
}

bool launcher::is_verbose() const noexcept
{
    return verbose;
}

void launcher::set_verbose(bool new_verbose/* = true*/) noexcept
{
    verbose = new_verbose;

    logger->setLevel(verbose ? log4cxx::Level::getInfo() : log4cxx::Level::getError());
}

bool launcher::launch_batch()
{
    assert(unfinished_count == 0 && "re-launching!");

    prepare_invocations();

    if (invocations_batches.empty())
        return false;

          auto& invocations = invocations_batches.front();
    const auto& ranges      = split_invocations(invocations, workers.size());

    assert(stacks.get_concurrency() >= ranges.size() && "amount of range is out of concurrency");

    if (logger->isDebugEnabled())
    {
        for (const auto& invocation : invocations)
        {
            const auto&      func_id             = invocation.function_id;
            const auto&      depend_on_counter   = invocation.depend_on_counter;
            const auto*const be_depended_counter = invocation.be_depended_counter;

            LOG4CXX_DEBUG(
                logger,
                   L"found invocation: "
                << L"func_id=" << func_id
                << L", depend_on=" << (depend_on_counter   == nullptr ? L"none" : std::to_wstring(*depend_on_counter  ))
                << L", be_depend=" << (be_depended_counter == nullptr ? L"none" : std::to_wstring(*be_depended_counter))
            );
        }
    }

    {
        std::unique_lock<std::mutex> lock(unfinished_guard);
        unfinished_count = ranges.size();

        for (const auto range : ranges)
        {
            service.post(
                [this, range]
                {
                    const auto& finished_marker = invoke_on_destroy(
                        [this]
                        {
                            LOG4CXX_TRACE(logger, L"ranged launch finished");

                            {
                                std::lock_guard<std::mutex> lock(unfinished_guard);

                                --unfinished_count;
                                unfinished_cond.notify_one();
                            }

                            LOG4CXX_TRACE(logger, L"ranged launch finished with notification");
                        }
                    );

                    assert(!range.empty() && "launch without task!");

                    launch_range(range);
                }
            );
        }

        unfinished_cond.wait(
            lock,
            [this]
            {
                LOG4CXX_TRACE(logger, L"test for unfinished count: " << unfinished_count);

                return unfinished_count == 0;
            }
        );

        LOG4CXX_TRACE(logger, L"all finished");
    }

    invocations_batches.pop_front();

    return true;
}

template<typename range_type>
bool launcher::launch_range(const range_type& range)
{
    if (is_exited)
        return false;

    boost::context::fcontext_t caller;

    for (auto& invocation : range)
    {
        assert(invocation.valid            && "invalid invocation!?");

        if (invocation.depend_on_counter != nullptr)
        {
            if (*invocation.depend_on_counter != 0)
            {
                LOG4CXX_DEBUG(logger, L"delay invocation for function: " << invocation.function_id);

                delay_invocation(invocation);

                continue;
            }
            else
            {
                invocation.depend_on_counter = nullptr;
            }
        }

        const auto&      thread_index = runtime_info::get_thread_index();
        const auto&      stack        = stacks.get_stack(thread_index);
        const auto&      stack_size   = stack.first;
              void*const stack_ptr    = stack.second;
              auto*const fcontext     = boost::context::make_fcontext(stack_ptr, stack_size, &launcher::launch_one);

        invocation_state = invocation_state_t
        {
            fcontext,
            &caller,
            &invocation
        };

        LOG4CXX_DEBUG(logger, L"invoking function: " << invocation.function_id);

        const auto& ret = boost::context::jump_fcontext(&caller, fcontext, reinterpret_cast<std::intptr_t>(this));

        LOG4CXX_DEBUG(logger, L"invoked function: " << invocation.function_id);

        if (invocation.be_depended_counter != nullptr)
            --*invocation.be_depended_counter;

        if (const auto*const done_notifier = invocation.done_notifier)
        {
            try
            {
                done_notifier->operator()();
            }
            catch (const std::exception& e)
            {
                LOG4CXX_ERROR(logger, L"exception thrown in notifier passed from outside");
            }
        }

        if (ret != 0)
            return false;

        if (is_exited)
            return false;
    }

    return true;
}

void launcher::launch_one(std::intptr_t ptr)
{
    auto*const this_ptr = reinterpret_cast<launcher*>(ptr);

    assert(this_ptr       != nullptr && "null pointer exception");
    assert(this_ptr->kern != nullptr && "null pointer exception");

    auto*const global_dispatcher = this_ptr->kern->get_global_dispatcher();

    assert(global_dispatcher != nullptr && "null pointer exception");

    try
    {
        auto*const invocation = invocation_state.invocation;

        assert(invocation_state.invocation != nullptr && "null pointer exception");

        const auto&      function_id       = invocation->function_id;
        const auto*const raw_data_array    = boost::get<invocation_t::raw_data_t>(&invocation->payload);
        const auto*const raw_data          = raw_data_array == nullptr ? nullptr : reinterpret_cast<const char*>(raw_data_array->data());
              auto*const store_to          = reinterpret_cast<char*>(invocation->store_to);

        LOG4CXX_INFO(this_ptr->logger, L"run function[" << function_id << L"]");

        global_dispatcher(function_id, raw_data, store_to);
    }
    catch (const std::exception& e)
    {
        LOG4CXX_FATAL(this_ptr->logger, "caught exception with message: " << e.what());
    }
    catch (...)
    {
        LOG4CXX_FATAL(this_ptr->logger, "caught exception not derived from std::exception!");
    }

    assert(invocation_state.worker != nullptr && "null pointer exception");
    assert(invocation_state.caller != nullptr && "null pointer exception");

    boost::context::jump_fcontext(invocation_state.worker, invocation_state.caller, 0);

    UNREACHABLE_CODE();
}

bool launcher::add_invocation_from_external(const std::string& func_name)
{
    const auto& func_id = kern->get_function_id(func_name);

    if (func_id == -1)
        return false;

    return add_invocation_from_external(func_id);
}

bool launcher::add_invocation_from_external(std::int64_t func_id)
{
    assert(func_id >= 0 && "invalid function id");

    invocation_t invocation;

    invocation.valid       = true;
    invocation.session_id  = -1;
    invocation.function_id = func_id;

    push_invocation_from_external(std::move(invocation));

    return true;
}

bool launcher::run_invocation_from_external(const std::string& func_name)
{
    const auto& func_id = kern->get_function_id(func_name);

    if (func_id == -1)
        return false;

    return run_invocation_from_external(func_id);
}

bool launcher::run_invocation_from_external(std::int64_t func_id)
{
    assert(func_id >= 0 && "invalid function id");

    std::mutex              guard;
    std::condition_variable cond;
    std::function<void()>   notifier = [&guard, &cond]
                                       {
                                           std::lock_guard<std::mutex> lock(guard);

                                           cond.notify_one();
                                       };

    std::unique_lock<std::mutex> lock(guard);

    {
        invocation_t invocation;

        invocation.valid         = true;
        invocation.session_id    = -1;
        invocation.function_id   = func_id;
        invocation.done_notifier = &notifier;

        push_invocation_from_external(std::move(invocation));
    }

    cond.wait(lock);

    return true;
}

std::int64_t launcher::push_invocation_from_external(invocation_t&& invocation)
{
    std::lock_guard<std::mutex> lock(invocations_from_external_guard);

    invocations_from_external.emplace_back(std::move(invocation));

    const auto& inv_id = invocations_from_external.size() - 1;
    assert(inv_id <= std::numeric_limits<std::int64_t>::max() && "ran out of memory should before integer overflow...");

    return static_cast<std::int64_t>(inv_id);
}

std::int64_t launcher::add_invocation(std::int64_t func_id, void* ret_ptr)
{
    invocation_t invocation;

    invocation.valid       = true;
    invocation.session_id  = -1;
    invocation.function_id = func_id;
    invocation.store_to    = ret_ptr;

    return push_invocation(std::move(invocation));
}

std::int64_t launcher::add_invocation(std::int64_t func_id, std::int64_t session_id, std::unique_ptr<std::deque<std::int8_t>>&& replication_data)
{
    assert(replication_data != nullptr && "null pointer exception");

    invocation_t invocation;

    invocation.valid       = true;
    invocation.session_id  = session_id;
    invocation.function_id = language::system_function::remote_invocation_id;
    invocation.payload     = invocation_t::remote_data_t{std::move(replication_data), func_id};

    return push_invocation(std::move(invocation));
}

std::int64_t launcher::reserve_invocation(std::int64_t func_id)
{
    invocation_t invocation;

    invocation.function_id = func_id;
    invocation.session_id  = -1;
    invocation.valid       = false;

    return push_invocation(std::move(invocation));
}

bool launcher::commit_invocation(std::int64_t inv_id)
{
    auto*const invocation = get_reserved_invocation(inv_id);

    if (invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, "modifying committed invocation!?");
        return false;
    }

    invocation->valid = true;

    return true;
}

bool launcher::abort_invocation(std::int64_t inv_id)
{
    auto*const invocation = get_reserved_invocation(inv_id);

    if (invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, "modifying committed invocation!?");
        return false;
    }

    return true;
}

bool launcher::set_invocation_return_ptr(std::int64_t inv_id, void* ptr)
{
    auto*const invocation = get_reserved_invocation(inv_id);

    if (invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, "modifying committed invocation!?");
        return false;
    }

    invocation->store_to = ptr;

    return true;
}

template<typename value_type>
std::int64_t launcher::append_invocation_parameter(std::int64_t inv_id, std::int64_t offset, value_type value)
{
    if (offset < 0)
    {
        LOG4CXX_ERROR(logger, "invalid offset");
        return -1;
    }

    auto*const invocation = get_reserved_invocation(inv_id);

    if (invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, "modifying committed invocation!?");
        return -1;
    }

              auto&      raw_data     = boost::get<invocation_t::raw_data_t>(invocation->payload);
              auto*const raw_data_ptr = raw_data.data();
    constexpr auto       value_size   = sizeof(value);
    assert(value_size <= raw_data.size() && "parameter buffer is too small for specifc type!");

    if (offset > raw_data.size() - value_size)
        return -1;

    std::memcpy(raw_data_ptr + offset, &value, value_size);

    return static_cast<std::int64_t>(offset + value_size);
}

template std::int64_t launcher::append_invocation_parameter<bool        >(std::int64_t, std::int64_t, bool        );
template std::int64_t launcher::append_invocation_parameter<std::int8_t >(std::int64_t, std::int64_t, std::int8_t );
template std::int64_t launcher::append_invocation_parameter<std::int16_t>(std::int64_t, std::int64_t, std::int16_t);
template std::int64_t launcher::append_invocation_parameter<std::int32_t>(std::int64_t, std::int64_t, std::int32_t);
template std::int64_t launcher::append_invocation_parameter<std::int64_t>(std::int64_t, std::int64_t, std::int64_t);
template std::int64_t launcher::append_invocation_parameter<float       >(std::int64_t, std::int64_t, float       );
template std::int64_t launcher::append_invocation_parameter<double      >(std::int64_t, std::int64_t, double      );
template std::int64_t launcher::append_invocation_parameter<void*       >(std::int64_t, std::int64_t, void*       );

bool launcher::postpone_invocation_dependency_to(std::int64_t inv_id)
{
    auto*const postpone_to_invocation = get_committed_invocation(inv_id);

    if (postpone_to_invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, L"modifying not yet committed or activated invocation!");
        return false;
    }

    assert(postpone_to_invocation->be_depended_counter == nullptr && "overwriting dependency!");
    assert(postpone_to_invocation->store_to            == nullptr && "overwriting dependency!");

    auto*const current_invocation = invocation_state.invocation;
    assert(current_invocation != nullptr && "null pointer exception");

    postpone_to_invocation->be_depended_counter = current_invocation->be_depended_counter;
        current_invocation->be_depended_counter = nullptr;

    postpone_to_invocation->store_to = current_invocation->store_to;
        current_invocation->store_to = nullptr;

    return true;
}

bool launcher::set_invocation_depend_on_count(std::int64_t inv_id, std::int32_t count)
{
    using  atomic_type = decltype(invocation_t::depend_on_counter)::element_type;
    using counter_type = decltype(std::declval<atomic_type>().load());

    assert(0 <= count && count <= std::numeric_limits<counter_type>::max() && "count is ran out of range");

    auto*const invocation = get_committed_invocation(inv_id);

    if (invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, L"modifying not yet committed or activated invocation!");
        return false;
    }

    assert(invocation->depend_on_counter == nullptr && "overwriting dependency!");

    invocation->depend_on_counter = make_unique<atomic_type>(static_cast<counter_type>(count));

    return true;
}

bool launcher::set_invocation_depend_on(std::int64_t inv_id, std::int64_t depend_on_inv_id)
{
    auto*const           invocation = get_committed_invocation(          inv_id);
    auto*const depend_on_invocation = get_committed_invocation(depend_on_inv_id);

    if (invocation == nullptr || depend_on_invocation == nullptr)
    {
        LOG4CXX_ERROR(logger, L"modifying not yet committed or activated invocation!");
        return false;
    }

    depend_on_invocation->be_depended_counter = invocation->depend_on_counter.get();

    return true;
}

std::int64_t launcher::get_current_invocation_function_id()
{
    const auto*const invocation = invocation_state.invocation;
    assert(invocation != nullptr && "invocation statte should be initialized before use");

    return invocation->function_id;
}

std::int64_t launcher::get_current_invocation_session_id()
{
    const auto*const invocation = invocation_state.invocation;
    assert(invocation != nullptr && "invocation statte should be initialized before use");

    return invocation->session_id;
}

const char* launcher::get_current_invocation_parameters()
{
    const auto*const invocation = invocation_state.invocation;
    assert(invocation != nullptr && "invocation statte should be initialized before use");

    const auto& raw_data = boost::get<invocation_t::raw_data_t>(invocation->payload);

    return reinterpret_cast<const char*>(raw_data.data());
}

std::int64_t launcher::push_invocation(invocation_t&& invocation)
{
    const auto& index = runtime_info::get_thread_index();
    assert(index < next_invocations_batches.size() && "index our of range, but this should not happened by design");

    auto& next_invocations_batch = next_invocations_batches[index];

    next_invocations_batch.emplace_back(std::move(invocation));

    const auto& inv_id = next_invocations_batch.size() - 1;
    assert(inv_id <= std::numeric_limits<std::int64_t>::max() && "ran out of memory should before integer overflow...");

    return static_cast<std::int64_t>(inv_id);
}

std::pair<std::unique_ptr<std::deque<std::int8_t>>, std::int64_t> launcher::steal_replication_data()
{
    auto*const invocation  = invocation_state.invocation;
    auto*const remote_data = boost::get<invocation_t::remote_data_t>(&invocation->payload);

    if (remote_data == nullptr)
    {
        LOG4CXX_ERROR(logger, L"no replication data");
        return {nullptr, -1};
    }

    return {std::move(remote_data->data), remote_data->adaptor_id};
}

void launcher::prepare_invocations()
{
    if (!invocations_batches.empty())
        return;

    invocations_batches.emplace_back();

    const auto& merge_new_invocations = [this](std::deque<invocation_t>& new_invocations, bool accept_all)
    {
        using boost::adaptors::filtered;

        const auto& new_valid_invocations = new_invocations
                                          | filtered(std::mem_fn(&invocation_t::valid))
                                          ;

        if (new_valid_invocations.empty())
            return;

        auto& invocations_batch = invocations_batches.back();
        for (auto& invocation : new_valid_invocations)
        {
            if (accept_all || invocation.session_id == -1)
            {
                invocations_batch.insert(invocations_batch.end(), std::move(invocation));
            }
            else
            {
                auto& remote_data = boost::get<invocation_t::remote_data_t>(invocation.payload);
                auto& executor_remote = executor->get_executor_remote();
                executor_remote.send_invocation_request(
                    remote_data.adaptor_id,
                    invocation.session_id,
                    std::move(*remote_data.data)
                );
            }
        }

        new_invocations.clear();
    };

    boost::for_each(next_invocations_batches, std::bind(merge_new_invocations, std::placeholders::_1, false));

    {
        std::lock_guard<std::mutex> lock(invocations_from_external_guard);

        merge_new_invocations(invocations_from_external, true/* accept all */);
    }

    if (invocations_batches.front().empty())
        invocations_batches.pop_front();
}

void launcher::delay_invocation(invocation_t& invocation)
{
    assert(boost::get<invocation_t::raw_data_t>(&invocation.payload) != nullptr && "only invocation with raw data could be delayed");

    auto& next_invocations = get_next_invocations();

    next_invocations.emplace_back(std::move(invocation));
}

std::deque<invocation_t>& launcher::get_next_invocations()
{
    assert(next_invocations_batches.size() > runtime_info::get_thread_index() && "index out of range");

    return next_invocations_batches[runtime_info::get_thread_index()];
}

invocation_t* launcher::get_committed_invocation(std::int64_t inv_id)
{
    auto& next_invocations = get_next_invocations();
    assert(0 <= inv_id && inv_id < next_invocations.size() && "index ouf of range!?");

    auto& invocation = next_invocations[inv_id];

    if (!invocation.valid)
        return nullptr;

    return &invocation;
}

invocation_t* launcher::get_reserved_invocation(std::int64_t inv_id)
{
    auto& next_invocations = get_next_invocations();
    assert(0 <= inv_id && inv_id < next_invocations.size() && "index ouf of range!?");

    auto& invocation = next_invocations[inv_id];

    if (invocation.valid)
        return nullptr;

    return &invocation;
}

void launcher::exit(std::int32_t exit_code)
{
    is_exited = true;

    executor->set_exit_code(exit_code);

    boost::context::jump_fcontext(invocation_state.worker, invocation_state.caller, reinterpret_cast<std::intptr_t>(1l));
}

void launcher::on_connect(std::deque<std::int8_t>&& parameters)
{
    if (SIZE_INVOCATION_PARAMETER < parameters.size())
    {
        LOG4CXX_ERROR(logger, "received data size is too large.");
        return;
    }

    push_domain_event(
        this,
        language::system_function::domain_on_connect_id,
        std::move(parameters)
    );
}

void launcher::on_disconnect(std::deque<std::int8_t>&& parameters)
{
    if (SIZE_INVOCATION_PARAMETER < parameters.size())
    {
        LOG4CXX_ERROR(logger, "received data size is too large.");
        return;
    }

    push_domain_event(
        this,
        language::system_function::domain_on_disconnect_id,
        std::move(parameters)
    );
}

void launcher::on_error(std::deque<std::int8_t>&& parameters)
{
    if (SIZE_INVOCATION_PARAMETER < parameters.size())
    {
        LOG4CXX_ERROR(logger, "received data size is too large.");
        return;
    }

    push_domain_event(
        this,
        language::system_function::domain_on_error_id,
        std::move(parameters)
    );
}

} } }
