/**
 *
 * Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
 *
 * This file is part of Thor.
 * Thor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * Thor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Thor.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to develop any commercial services or closed-source products with
 * Thor, to adapt sources of Thor in your own projects without
 * disclosing sources, purchasing a commercial license is mandatory.
 *
 * For more information, please contact Zillians, Inc.
 * <thor@zillians.com>
 *
 */

#include <cassert>
#include <cstdint>
#include <cstring>

#include <string>
#include <algorithm>

#include <boost/swap.hpp>

#include <log4cxx/consoleappender.h>
#include <log4cxx/patternlayout.h>

#include "utility/MemoryUtil.h"

#include "language/SystemFunction.h"
#include "language/ThorConfiguration.h"

#include "framework/Executor.h"
#include "framework/ExecutorRemote.h"
#include "framework/ExecutorX86.h"
#include "framework/InvocationRequest.h"

#include "framework/x86_detail/DomainManager.h"
#include "framework/x86_detail/Kernel.h"
#include "framework/x86_detail/Launcher.h"
#include "framework/x86_detail/ObjectManager.h"
#include "framework/x86_detail/Protocols.h"

namespace zillians { namespace framework {

namespace {

void push_obj_destroy_invocation_if_any(x86_detail::object_manager& obj_manager, x86_detail::launcher& launcher)
{
    using x86_detail::invocation_t;

    auto       objs     = obj_manager.get_gc().steal_sweeped_objs();
    auto*const objs_ptr = objs.get();

    if (objs_ptr == nullptr)
        return;

    invocation_t objs_destroy_invocation;
    auto&        objs_destroy_invocation_params     = boost::get<invocation_t::raw_data_t>(objs_destroy_invocation.payload);
    auto*const   objs_destroy_invocation_params_ptr = objs_destroy_invocation_params.data();

    objs_destroy_invocation.function_id = language::system_function::object_destruction_id;
    objs_destroy_invocation.session_id  = -1;

    std::memcpy(objs_destroy_invocation_params_ptr, &objs_ptr, sizeof(objs_ptr));

    launcher.push_invocation_from_external(std::move(objs_destroy_invocation));

    objs.release();
}

}

executor_x86::executor_x86(std::int32_t new_id, bool new_multi_thread)
    : executor_rt(new_id, "framework.x86.executor")
    , args()
    , exit_code()
    , multi_thread(new_multi_thread)
    , kern(nullptr)
    , obj_manager(nullptr)
    , launcher(nullptr)
    , domain_manager(nullptr)
{
    // sample logged message with the following pattern layout:
    // ERROR [logger.name] [namespace::class_name::function_name] MESSAGE
    log4cxx::PatternLayoutPtr pattern_layout       = new log4cxx::PatternLayout("%-5p [%c] [%C::%M] %m%n");
    auto                      framework_x86_logger = logger->getParent();

    assert(framework_x86_logger != nullptr && "null pointer exception");

    framework_x86_logger->removeAllAppenders();
    framework_x86_logger->addAppender(new log4cxx::ConsoleAppender(pattern_layout));
}

executor_x86::~executor_x86() = default; // generate dtor here since header have only forward declaration (for kerner/launcher)

bool executor_x86::is_multi_thread() const noexcept
{
    return multi_thread;
}

bool executor_x86::initialize()
{
    if (!executor_rt::initialize())
        return false;

    auto new_kern = make_unique<x86_detail::kernel>(
        ast_path                                                 ,
        export_type == EXPORT_ALL || export_type == EXPORT_SERVER, /* enable server */
        export_type == EXPORT_ALL || export_type == EXPORT_CLIENT  /* enable client */
    );

    if (verbose)
        new_kern->set_verbose(true);

    if (!new_kern->initialize(ast_path, dll_path, dep_pathes))
    {
        LOG4CXX_ERROR(logger, "fail to initialize x86 kernel");

        return false;
    }

    auto new_obj_manager    = make_unique<x86_detail::object_manager>(*this, *new_kern                                 );
    auto new_launcher       = make_unique<x86_detail::launcher      >(*this, *new_kern, *new_obj_manager               );
    auto new_domain_manager = make_unique<x86_detail::domain_manager>(*executor_remote, *new_obj_manager, *new_launcher);

    if (verbose)
    {
        new_obj_manager->set_verbose(true);
        new_launcher   ->set_verbose(true);
    }

    boost::swap(kern          , new_kern          );
    boost::swap(obj_manager   , new_obj_manager   );
    boost::swap(launcher      , new_launcher      );
    boost::swap(domain_manager, new_domain_manager);

    return true;
}

bool executor_x86::finalize()
{
    assert(
        (
            (kern == nullptr && obj_manager == nullptr && launcher == nullptr) ||
            (kern != nullptr && obj_manager != nullptr && launcher != nullptr)
        ) && "corrupted state!"
    );

    if (kern == nullptr)
        return false;

    domain_manager = nullptr;
    launcher       = nullptr;
    obj_manager    = nullptr;

    if (!kern->finalize())
        LOG4CXX_ERROR(logger, "fail to finalize x86 kernel");

    kern = nullptr;

    return executor_rt::finalize();
}

const char* executor_x86::get_binary_file_suffix() const
{
    return language::THOR_EXTENSION_SO;
}

void executor_x86::set_arguments(const std::vector<std::wstring>& new_args)
{
    args = new_args;
}

const std::vector<std::wstring>& executor_x86::get_arguments() const noexcept
{
    return args;
}

bool executor_x86::call(std::int64_t func_id)
{
    assert(launcher != nullptr && "not yet initialized!?");

    return launcher->add_invocation_from_external(func_id);
}

bool executor_x86::call(const std::string& func_name)
{
    assert(launcher != nullptr && "not yet initialized!?");

    return launcher->add_invocation_from_external(func_name);
}

bool executor_x86::call(std::int64_t session_id, invocation_request&& request)
{
    using namespace x86_detail;

    invocation_t invocation;

    invocation.valid       = true;
    invocation.function_id = language::system_function::remote_invocation_id;
    invocation.session_id  = session_id;

    invocation.payload = invocation_t::remote_data_t{
        make_unique<std::deque<std::int8_t>>(std::move(request.parameters)),
        request.function_id
    };

    return launcher->push_invocation_from_external(std::move(invocation));
}

bool executor_x86::call_and_wait(std::int64_t func_id)
{
    assert(launcher != nullptr && "not yet initialized!?");

    return launcher->run_invocation_from_external(func_id);
}

bool executor_x86::call_and_wait(const std::string& func_name)
{
    assert(launcher != nullptr && "not yet initialized!?");

    return launcher->run_invocation_from_external(func_name);
}

x86_detail::domain_manager& executor_x86::get_domain_manager() const
{
    return *domain_manager;
}

int executor_x86::get_exit_code()
{
    auto future = exit_code.get_future();

    return future.get();
}

void executor_x86::set_exit_code(std::int32_t new_exit_code)
{
    exit_code.set_value(new_exit_code);
}

void executor_x86::do_work()
{
    assert(launcher    != nullptr && "not yet initialized!?");
    assert(obj_manager != nullptr && "not yet initialized!?");

    launcher->launch_batch();
    obj_manager->do_gc();

    push_obj_destroy_invocation_if_any(*obj_manager, *launcher);
}

} } // namespace zillians::framework
