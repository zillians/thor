#
# Copyright (C) 2014 Zillians, Inc. <http://www.zillians.com/>
#
# This file is part of Thor.
# Thor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
#
# Thor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Thor.  If not, see <http://www.gnu.org/licenses/>.
#
# If you want to develop any commercial services or closed-source products with
# Thor, to adapt sources of Thor in your own projects without
# disclosing sources, purchasing a commercial license is mandatory.
#
# For more information, please contact Zillians, Inc.
# <thor@zillians.com>
#

INCLUDE_DIRECTORIES(
    "${CMAKE_CURRENT_SOURCE_DIR}/src"
    ${THOR_SYSTEM_BUNDLE_INCLUDE_DIR}
    ${TBB_INCLUDE_DIR}/
)

ADD_DEFINITIONS(${LLVM_CPPFLAGS})

ADD_LIBRARY(thor-framework-x86
    src/framework/ExecutorX86.cpp
    src/framework/RegisterX86.cpp
    src/framework/proxies/X86.cpp
    src/framework/x86_detail/DomainManager.cpp
    src/framework/x86_detail/GarbageCollector.cpp
    src/framework/x86_detail/GlobalDispatcherGenerator.cpp
    src/framework/x86_detail/Kernel.cpp
    src/framework/x86_detail/Launcher.cpp
    src/framework/x86_detail/ObjectManager.cpp
    src/framework/x86_detail/ObjectTracker.cpp
    src/framework/x86_detail/Protocols.cpp
    src/framework/x86_detail/RuntimeInfo.cpp
    src/framework/x86_detail/StackPool.cpp
)

TARGET_LINK_LIBRARIES(thor-framework-x86
    ${LLVM_ALL_LIBS}
    ${LLVM_LDFLAGS}
    thor-common-utility
    thor-framework
)
