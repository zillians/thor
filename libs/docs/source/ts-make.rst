.. _thor-make:

=====================
thor-make
=====================

Source dependency and tangle
=================================================
For a Thor source dependency like:

.. digraph:: g

    rankdir = "LR";
    node[fontname="monospace"];
    "A"[shape="record" label="a.t|import b;\nimport c;"];
    "B"[shape="record" label="b.t|import d;"];
    "C1"[shape="record" label="c1.t|import c2;\nimport d;"];
    "C2"[shape="record" label="c2.t|import c1;\nimport d;"];
    "D"[shape="record" label="d.t"];
    "A" -> "B";
    "A" -> "C1";
    "C1" -> "C2";
    "C2" -> "C1";
    "B" -> "D";
    "C1" -> "D";

we can have a tangle graph:

.. digraph:: g

    rankdir = "LR";
    node[fontname="monospace"];
    "A"[shape="record" label="A|a.t"];
    "B"[shape="record" label="B|b.t"];
    "C"[shape="record" label="C|c1.t\nc2.t"];
    "D"[shape="record" label="D|d.t"];
    "A" -> "B";
    "A" -> "C";
    "B" -> "D";
    "C" -> "D";

and a build order should be (smaller build first):

    +---+---+---+---+
    | A | B | C | D |
    +---+---+---+---+
    | 2 | 1 | 1 | 0 |
    +---+---+---+---+

So we can have a Thread-Buliding-Block(TBB) flow graph like this:

.. digraph:: g

    rankdir = "LR";
    node[fontname="monospace"];
    "D"[shape="record" label="D|d.t"];
    "B"[shape="record" label="B|b.t"];
    "C"[shape="record" label="C|c1.t\nc2.t"];
    "A"[shape="record" label="A|a.t"];
    "D" -> "B";
    "D" -> "C";
    "B" -> "A";
    "C" -> "A";

...which **is the transpose graph** of the tangle graph.
The tangle D must be build at the very first.
And tangle A build at last.

Our goal is to generate TBB flow graph like this:

.. digraph:: g

    rankdir = "LR";
    size = "8, 8";
    fontname = "monospace";
    node[fontname="monospace"];
    "D" [shape="record" label="BraodCast node D|thor-compile d.t"];
    "B" [shape="record" label="Function node B|thor-compile b.t"];
    "C" [shape="record" label="Function node C|thor-compile c1.t c2.t"];
    "JA"[shape="record" label="<f0>Join node JA|<f1> \<input 1\>|<f2> \<input 2\> "];
    "A" [shape="record" label="Function node A|thor-compile a.t"];
    "D" -> "B";
    "D" -> "C";
    "B" -> "JA":f1;
    "C" -> "JA":f2;
    "JA" -> "A";

Invoking ``thor-compile``
===================================================

``thor-make`` while invoke following four commands:

::

    thor-compile d.t --emit-ast=5d50e909767553b30916afd1cd2f7b931059b808_d.ast --emit-llvm=5d50e909767553b30916afd1cd2f7b931059b808_d.bc
    thor-compile c1.t c2.t 5d50e909767553b30916afd1cd2f7b931059b808_d.ast --emit-ast=7afef7fd6295e79721601ac6cc702a14176b38ce_c1_c2.ast --emit-llvm=7afef7fd6295e79721601ac6cc702a14176b38ce_c1_c2.llvm
    thor-compile b.t 5d50e909767553b30916afd1cd2f7b931059b808_d.ast --emit-ast=d4ba13091eccce66e6190c8d5c611051c72b87bf_b.ast --emit-llvm=d4ba13091eccce66e6190c8d5c611051c72b87bf_b.llvm
    thor-compile a.t 7afef7fd6295e79721601ac6cc702a14176b38ce_c1_c2.ast d4ba13091eccce66e6190c8d5c611051c72b87bf_b.ast --emit-ast=35d24a6127575e673c66d8040efa3337deaa985c_a.ast --emit-llvm=35d24a6127575e673c66d8040efa3337deaa985c_a.llvm

Each command will output two files, one ``.ast`` file which contains the
interface of the source files, another ``.bc`` file contains the generated llvm
code.

The format of the output file name has two part, the first part is the
hash of all the input source file, the second part the stem name of all the
input source file. for example: ``7afef7fd6295e79721601ac6cc702a14176b38ce_c1_c2``
is the combination of ``7afef7fd6295e79721601ac6cc702a14176b38ce`` and ``c1_c2``,
and ``7afef7fd6295e79721601ac6cc702a14176b38ce`` is the hash of ``c1 c2``.


``JoinFunctionModule``
=================================================

Since the ``tbb::flow::join_node`` can only specify its input number
with compile-time constant expression. and the binding of input port have to be
specified with a compile-time constant. and the number of input can not be
larger than 9, it is very difficult to generate a TBB flow graph dynamically in
run time according to a file.

we have to design a wrapper class ``JoinFunctionModule`` to simplify the job.
``JoinFunctionModule`` will generate ``tbb::flow::join_node`` hierarchy(with
a ``tbb::flow::function_node`` attached) automatically, and can support
infinite input ports, and user don't have to assign the id of input port while
assign edge to the join node.

The usage would like:

.. code-block:: c++

    tbb::flow::graph g;

    // create vertex
    tbb::flow::broadcast_node<int> start;
    zillians::JoinFunctionModule d(g, compile("d.t"));
    zillians::JoinFunctionModule b(g, compile("b.t"));
    zillians::JoinFunctionModule c(g, compile("c1.t c2.t"));
    zillians::JoinFunctionModule a(g, compile("a.t"), 2);

    // add broadcast edge
    tbb::flow::make_edge(start, d.getNextInputPort());

    // add edges
    tbb::flow::make_edge(d.getOutputPort(), b.getNextInputPort());
    tbb::flow::make_edge(d.getOutputPort(), c.getNextInputPort());
    tbb::flow::make_edge(b.getOutputPort(), a.getNextInputPort());
    tbb::flow::make_edge(c.getOutputPort(), a.getNextInputPort());

    // check all meta nodes input had been set.
    assert(a.verifyInput());
    assert(b.verifyInput());
    assert(c.verifyInput());
    assert(d.verifyInput());

    // trigger the build
    start.try_put(1);
    g.wait_all_end();

or while reading from serialized boost::graph file...

.. code-block:: c++

    tbb::flow::graph g;

    // create vertex
    tbb::flow::broadcast_node<int> start;
    std::vector<zillians::JoinFunctionModule> v;
    foreach(vertex, tangleGraph.vertices) {
        zillians::JoinFunctionModule m(g, compile(vertex), in_degree(g, vertex));
        v.push_back(m);
    }

    // add broadcast edge
    foreach(vertex, tangleGraph.verticesWithLevel0) {
        tbb::flow::make_edge(start, vertex.getNextInputPort());
    }
        
    // add edges
    foreach(edge, tangleGraph.edges) {
        tbb::flow::make_edge(v[edge.source].getOutputPort(), v[edge.target].getNextInputPort());
    }

    // check all meta nodes input had been set.
    foreach(n, v) {
        assert(g.verifyInput());
    }

    // trigger the build
    start.try_put(1);
    g.wait_all_end();

Detail of the Construction ``JoinFunctionModule``.
===================================================

TBB has attribute type on every node. Every node has its input type and output
type. One node's input type must be the type of the output type of its source
node. We can not bind edge between two node which the types are not match.

The output type of ``tbb::flow::join`` is ``std::tuple<...>``, the result is
the combination of its input.  If we connect two join nodes directly, like:

.. digraph:: g

    rankdir = "LR";
    size = "9, 9";
    node[fontname="monospace"];
    edge[fontname="monospace"];
    i0[color=white label=""];
    i1[color=white label=""];
    i2[color=white label=""];
    i3[color=white label=""];
    o0[color=white label=""];
    A[shape="rectangle" label="Join"];
    B[shape="rectangle" label="Join"];
    C[shape="rectangle" label="Join"];
    i0 -> A[label="T0"];
    i1 -> A[label="T1"];
    i2 -> B[label="T2"];
    i3 -> B[label="T3"];
    A  -> C[label="tuple<T0, T1>"];
    B  -> C[label="tuple<T2, T3>"];
    C  -> o0[label="tuple<tuple<T0, T1>, tuple<T2, T3>>"];

The result type of a ``JoinFunctionMoudle`` will be very complicated.  So we
have to adapt a **type folding node** after the join node to collapse the output
type, after all, we don't care about the return value of the node at all.

.. digraph:: g

    rankdir = "LR";
    size = "9, 9";
    node[fontname="monospace"];
    edge[fontname="monospace"];
    i0[color=white label=""];
    i1[color=white label=""];
    i2[color=white label=""];
    i3[color=white label=""];
    o0[color=white label=""];
    A[shape="rectangle" label="Join"];
    B[shape="rectangle" label="Join"];
    C[shape="rectangle" label="Join"];
    fA[height=0.25, width=0.25, label=""];
    fB[height=0.25, width=0.25, label=""];
    fC[height=0.25, width=0.25, label=""];
    i0 -> A[label="T"];
    i1 -> A[label="T"];
    i2 -> B[label="T"];
    i3 -> B[label="T"];
    A  -> fA[label="tuple<T, T>"]; fA -> C [label="T"];
    B  -> fB[label="tuple<T, T>"]; fB -> C [label="T"];
    C  -> fC[label="tuple<T, T>"]; fC -> o0 [label="T"];

Therefore the internal structure of ``JoinFunctionModule`` might like:

.. digraph:: g

    rankdir = "LR";
    size = "9, 8";

    node[fontname = "monospace"];
    subgraph cluster_meta {
        label = "JoinFunctionModule";
        fontname = "monospace";
        style = filled;
        color = lightgrey;
        node [shape = rectangle, style = filled, color = white];
        subgraph cluster_join_level1 {
            label = "JoinLevel<5>";
            color = black;
            fillcolor = grey;
            J0[label = "Join"];
            J1[label = "Join"];
            J2[label = "Join"];
            O0[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            O1[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            O2[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            J0 -> O0;
            J1 -> O1;
            J2 -> O2;
        }
        subgraph cluster_join_level2 {
            label = "JoinLevel<3>";
            color = black;
            fillcolor = grey;
            J01[label = "Join"];
            J02[label = "Join"];
            O01[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            O02[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            J01 -> O01;
            J02 -> O02;
        }
        subgraph cluster_join_level3 {
            label = "JoinLevel<2>";
            color = black;
            fillcolor = grey;
            J11 [label = "Join"];
            O11[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            J11 -> O11;
        }
        node[height=0.25, width=0.5, fontsize=8];
        input_port0 -> J0;
        input_port1 -> J0;
        input_port2 -> J1;
        input_port3 -> J1;
        input_port4 -> J2;
        Function[label="Function\n(output_port)"];

        O0  -> J01;
        O1  -> J01;
        O2  -> J02;
        O01 -> J11;
        O02 -> J11;

        O11 -> Function;
    }
    i0[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i1[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i2[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i3[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i4[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];

    i0 -> input_port0 ;
    i1 -> input_port1 ;
    i2 -> input_port2 ;
    i3 -> input_port3 ;
    i4 -> input_port4 ;

    O[color=white, label=" "];
    Function -> O;

Notice that, since the number of input port of a join node has been assigned in
compile time, and must larget then 1. To simplify the implementation, we make
all join nodes have 2 input ports, like:

.. digraph:: g

    rankdir = "LR";
    size = "9, 8";

    node[fontname = "monospace"];
    subgraph cluster_meta {
        label = "JoinFunctionModule";
        fontname = "monospace";
        style = filled;
        color = lightgrey;
        node [shape = rectangle, style = filled, color = white];
        subgraph cluster_join_level1 {
            label = "JoinLevel<5>";
            color = black;
            fillcolor = grey;
            J0[label = "Join"];
            J1[label = "Join"];
            J2[label = "Join"];
            O0[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            O1[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            O2[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            J0 -> O0;
            J1 -> O1;
            J2 -> O2;
        }
        subgraph cluster_join_level2 {
            label = "JoinLevel<3>";
            color = black;
            fillcolor = grey;
            J01[label = "Join"];
            J02[label = "Join"];
            O01[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            O02[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            J01 -> O01;
            J02 -> O02;
        }
        subgraph cluster_join_level3 {
            label = "JoinLevel<2>";
            color = black;
            fillcolor = grey;
            J11 [label = "Join"];
            O11[shape = circle, label = "", fontsize=8, height=0.25, width=0.25];
            J11 -> O11;
        }
        node[height=0.25, width=0.5, fontsize=8];
        input_port0 -> J0;
        input_port1 -> J0;
        input_port2 -> J1;
        input_port3 -> J1;
        input_port4 -> J2 [color = red];
        input_port4 -> J2 [color = red];
        Function[label="Function\n(output_port)"];

        O0  -> J01;
        O1  -> J01;
        O2  -> J02 [color = red];
        O2  -> J02 [color = red];
        O01 -> J11;
        O02 -> J11;

        O11 -> Function;
    }
    i0[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i1[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i2[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i3[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];
    i4[color=white, height=0.2, shape=rectangle, fontsize=8, label=" "];

    i0 -> input_port0 ;
    i1 -> input_port1 ;
    i2 -> input_port2 ;
    i3 -> input_port3 ;
    i4 -> input_port4 ;

    O[color=white, label=" "];
    Function -> O;

To do
===================================================

- Cache

  Currently the ``thor-make`` does not support cache, each make will invoke the compile of all source files.
